  
<?php
return [
    /*
    |--------------------------------------------------------------------------
    | 
    |--------------------------------------------------------------------------
    |
    | 
    | 
    | 
    |
    */
    // index.blade
    'ana' => 'Home Page',


    //survey.blade
    'anket' => 'Survey',
    'anket_yok' => 'No Survey Found',
    'anket_yok_des' => 'Our System Do Not Have A Survey To Fill Please Contact Us To Solve This Issue',
    'anket_bolum' => 'this survey have :number section',
    'koşullu_secim' => 'Conditional Selection',
    'sorular' => 'Question',
    'sonraki' => 'Next',
    'onceki' => 'Previous',
    'kaydet_ve_bitir' => 'Save And End',
    'gonder' => 'Submit',
    'bilgi' => 'Information',
    'anket_bilgi' => 'There is an unfilled questionnaire for the Kit with code :code  .. Please Fill',
    'hatali_url' => 'Bad URL',
    'anket_dolduruldu' => 'Thank you for completing the questionnaire for the kit with code :code',
];