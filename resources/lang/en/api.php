<?php

return [

    /*
    |--------------------------------------------------------------------------
    | API Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'genel_hata' => 'An error occurred, please try again',
    'kit_hata' => 'This Kit does not belong to you',
    'kit_error_desc' => 'The code you entered is not in our records.',
    /*
    |--------------------------------------------------------------------------
    | AuthController
    |--------------------------------------------------------------------------
    */
    'login_successfully' => 'Successful Login',
    'login_unsuccessful' => 'The user information you entered is incorrect',

    /*
    |--------------------------------------------------------------------------
    | PasswordResetController
    |--------------------------------------------------------------------------
    */
    'send_reset_password' => 'A reset link has been sent to your email address.',
    'reset_password_error' => 'The e-mail address you entered is not in our records.',
    'reset_password_done' => 'Password reset done successfully',

    /*
    |--------------------------------------------------------------------------
    | UserRegisterKitController
    |--------------------------------------------------------------------------
    */
    'register_unccessful' => 'The user information you entered is incorrect',
    'register_successfully' => 'Your kit registration has been successful',

    /*
    |--------------------------------------------------------------------------
    | UserController
    |--------------------------------------------------------------------------
    */
    'already_verified_email' => 'This email has already been verified',

    'verification_email_send' => 'Your verification code has been sent to the email you entered. Please check',
    'verification_email_done' => 'Your e-mail address has been verified.',
    'verification_email_expired' => 'The code you entered has expired.',
    'verification_email_error' => 'The code you entered did not match.',

    'survey_information' => 'You must fill in the questionnaire for the :code coded kit.',
    'no_survey_desc' => 'There is no questionnaire to be filled in our system. Please contact us to resolve this issue.',

    'no_result_yet' => 'Your analysis results have not been uploaded yet.',

    'no_kit_suggestion' => 'Your suggestions are not yet prepared.',

    'no_resource' => 'No resources were found.',

    'send_pdf' => 'Your ENBIOSIS Microbiome Analysis report and Nutrient Scores will be sent to your email address in a few minutes.',
    'no_file' => 'File not found.',

    'iyi_Skor' => 'My Good Scores',
    'orta_Skor' => 'My Average Scores',
    'kotu_Skor' => 'My Scores I Need to Improve',

    /*
    |--------------------------------------------------------------------------
    | DietitianController
    |--------------------------------------------------------------------------
    */
    'no_result_found' => 'No matching clients found...',
];
