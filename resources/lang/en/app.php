  
<?php
return [
    /*
    |--------------------------------------------------------------------------
    | 
    |--------------------------------------------------------------------------
    |
    | 
    | 
    | 
    |
    */
    'satinal' => 'BUY NOW',
    'dil' => 'LANGUAGE',
    'flag' => 'gb',
    'anasafya' => 'HOME',
    'hakkimizda' => 'ABOUT US',
    'bilim' => 'SCIENCE',
    'enbiosis_test' => 'ENBIOSIS TEST',
    'shop' => 'BUY NOW',
    'sss' => 'F.A.Q',
    'iletisim' => 'CONTACT',
    'login' => 'LOGIN',
    'register_kit' => 'REGISTER KIT',
    'cart' => 'Cart',
    'header_6' => 'More Information',

    //footer
    'urunlerimiz' => 'OUR PRODUCTS',
    'sistem_work' => 'HOW DOES THE SYSTEM WORKS ? ',
    'mikro_bes' => 'MICROBIOTA and NUTRITION',
    'adres' => 'ADDRESS',

    //first setion
    'header_1' => 'YOUR GUT TELLS THE MOST APPROPRIATE',
    'header_2' => 'DIET FOR YOU',
    'header_3' => 'Order Now',
    'header_4' => 'Using the state-of-the-art sequencing method, we detect the bacteria that make up the intestinal microbiome.',
    'header_5' => 'What exactly are we looking for? Is the most appropriate healthy diet for people in general or the most suitable healthy diet for ourselves',

    //desc section
    'desc_1' => 'Welcome to your gut world.',
    'desc_2' => 'Trillions of bacteria directly affect your digestive system.',
    'desc_3' => 'It\'s time to listen to your microorganisms that play a significant role in the well-being of your body!',
    'desc_4' => 'Individual Diet Design and the use of microbiome data are the parts that are missing in today\'s technology.',
    'desc_5' => 'The aim of enbiosis is to transform the individuality of each individual, including biochemical parameters, into a personalized and feasible plan. Just test it and watch your microbiome over time, it\'s your choice.',

    // en-carousel
    'carousel_1_1' => 'Ideal Deight',
    'carousel_1_2' => 'You can reach your ideal weight with enbiosis! We provide specific nutrients for specific bacteria.',

    'carousel_2_1' => 'Digestive System',
    'carousel_2_2' => 'You can regulate your digestive system. It is in the hands of the microbiome to live a life free from bloating, abdominal pain, diarrhea, and constipation!',

    'carousel_3_1' => 'Sleep Quality',
    'carousel_3_2' => 'You can improve your sleep quality. We can help you sleep better with bacteria that contribute to the melatonin that regulates the sleep-wake cycle.',

    'carousel_4_1' => 'Energy',
    'carousel_4_2' => 'You can increase your energy level and be more productive during the day.',

    'carousel_5_1' => 'Mental Clarity',
    'carousel_5_2' => 'Human gut microbiome plays a key role in the transmission of signals to the brain. You can influence what you think with your intestines.',

    'carousel_6_1' => 'More',
    'carousel_6_2' => 'We\'re always with you as enbiosis family when you have any complaints. Your gut microbiome will give us all the answers we need',

    //How Does the System Work?
    'system_1_1' => 'Taking Your Microbiome Sample',
    'system_1_2' => 'Take your gut microbiome sample easily by following the steps written in the kit we sent to you. Put the test tube in the envelope inside the kit and post it to us free of charge.',

    'system_2_1' => 'Laboratory Analysis',
    'system_2_2' => 'Your microbiome sample is received by Erciyes University Genome and Stem Cell Center Laboratory and sequenced with 16S rRNA-nucleotide sequencing technology. The data obtained are analyzed by enbiosis, an artificial intelligence system.',

    'system_3_1' => 'Reporting and Dietary Recommendations',
    'system_3_2' => 'You can access your reports showing the status of your microbiome and your personalized nutritional recommendations created specifically for you from the enbiosis mobile app.',

    //Technology
    'teknoloji' => 'Technology',

    'teknoloji_1' => 'Artificial intelligence transfers the ability of human beings to think logically, to learn and to apply what they have learned to new situations to the machines. With the help of artificial intelligence technology, we obtain much more data more quickly and clearly than the human mind can analyze. Artificial intelligence helps researchers accomplish challenging tasks faster and accelerates medical research. It is a solution to the concept of ‘time’, which is the biggest problem of our time and which we cannot supply in any way.',
    'teknoloji_2' => 'We\'ve created an artificial intelligence algorithm specializing in valuable data collected from studies around the world. It contains data about thousands of bacteria and nutrients, as well as information that has been generated with thousands of accumulated experiences, which can\'t even be found in the literature. In this system, we create personalized diet lists based on artificial intelligence as a result of our analyzes and specifically ensure that the person consumes the most appropriate food in the most accurate way.',

    // MERAK EDİLENLER
    'merak' => 'Frequently asked questions',

    'merak_1_1' => 'What do you do with microbiome analysis? Why it is necessary?',
    'merak_1_2' => 'Your microbiome analysis shows your reaction to food. Every person gives different reactions to the same food. This analysis is necessary in order to balance your blood sugar levels and create a healthy and personalized diet list for you. ',

    'merak_2_1' => ' Who should take this test?',
    'merak_2_2' => 'A great population is in question.  Any person aged 16 and above can take this test. Those who are in the prediabetes stage, have any acute-chronic illness, weight and sleep problems, have autoimmune disorders, and who are healthy but want to have detailed information about their inner world in case of any possible diseases can take this test.',

    'merak_3_1' => 'How does the system work?',
    'merak_3_2' => 'First, users should subscribe by downloading the mobile application or via our website. This customized program starts after a few questions about the individual\'s health history and general diet habits are asked. Stool samples taken using the sample collection kit provided to the users are sent to Erciyes University Genome and Stem Cell Center. This laboratory is the first and largest genome and stem cell research center of Turkey. Here, the microbial DNA of the sample is analyzed, individual microbiome profiles are obtained, and the results are shared with the user through the mobile app with exclusive confidentiality. Which one of the thousands of foods is suitable for you is shown through the application with scoring. This program, which regulates the body mass index of the individual in a healthy way, is created with the dietary recommendation algorithms according to the individual’s microbiome profile.',

    'merak_4_1' => 'How can we get the kit? How do we prepare the sample?',
    'merak_4_2' => 'You can give order easily via enbiosis mobile app or website. You can collect your sample at home at any time in less than two minutes, following the steps shown on the box. Your kit includes everything you need.  Then, send your sample back to our address on the prepaid envelope attached.',

    'merak_5_1' => 'Is there any possibility of germ contamination from anywhere while preparing the sample? How stable is my sample when I rotate it in the tube after collected it? Is it really possible to send it at room temperature? Do bacteria start to grow? Will my microbiome profile be affected by shipping?',
    'merak_5_2' => 'Don’t worry. Your sampling tube contains DNA stabilizing liquid that kills microorganisms in the case of contact and protects the released microbial DNA from degradation. Any possible infectious agent dies before shipment. The said liquid is a special component designed to stabilize and protect your sample. When you transfer your sample from the swab to the sampling tube, the growth of microorganisms stops. This is very important because we want to know exactly what it looks like when you collect your sample. If your microorganisms are allowed to grow, the contents of the tube will change until they arrive at our laboratory. We ensure that they come to our laboratory without being affected by any conditions during transportation.',

    'merak_x' => 'CLICK HERE FOR MORE',

    //Hakkimizde Page

    'NEDEN_MİKROBİYOM?' => 'Why microbiome?',

    'hak_page_1' => 'Homo sapiens have been evolving and interacting for thousands of years with friendly microorganisms in the body. Specifically, metabolites nucleic acids produced by living microorganisms in the gut and even genetic materials are transported into the human systemic circulation and lead to activation of inactive genes through epigenetic mechanisms, which can benefit health, lead to the development of various diseases and evolution. <br><br> The microbiome of each person is individual. Microbiome varies greatly from person to person and affects blood sugar levels differently with its response to food. We obtain detailed information about the activities, diversity, and metabolites produced by the microorganisms in your body through our microbiome analysis and we plan your personalized nutrition program based on this information.',

    'MİSYONUMUZ' => 'Mission',
    'MİSYONUMUZ_1' => 'To promote the science of microbiome and to make it beneficial for human beings. To improve the quality of life of people with personalized diet programs suitable for microbiome.',

    'VİZYONUMUZ' => 'Our Vision',
    'VİZYONUMUZ_1' => 'To create personalized health technologies by targeting next-generation technologies Our aim is to make human health development a solution by giving priority to the principles of ethics of health sciences and to present it to the masses.',

    'degerlerimiz' => 'Our Values',
    'degerlerimiz_1' => 'Adhering to the principles and values of health ethics and commitment to science and new technologies.',

    //EKİBİMİZ
    'EKİBİMİZ' => 'Our Team',

    'ekip_1' => 'AI-SOLT Chairman of the Board',
    'ekip_2' => 'AI-SOLT Co-Founder',
    'ekip_3' => 'Academician - Bioinformatics Specialist',
    'ekip_4' => 'Academician- Molecular Microbiology Specialist',
    'ekip_5' => 'Solution Partner',

        // Bilim page

    'bilim_1' => 'In a study conducted by the Weizmann Institute of Science on 800 healthy volunteers (Cell, 19 November 2015), it has been observed that different people react to the same foods with different blood glucose levels. Successful results were obtained in 100 people who were included in the study later. Information about the physical activity, sleep patterns, drug use, and anthropometric data of the individuals were collected and then, stool samples were analyzed with microbiome test. Thanks to the artificial intelligence algorithms developed, blood sugar responses of individuals to foods have been predicted successfully. In the study, general dietary recommendations have been reported to provide limited benefit in individuals.',
    'bilim_2' => 'The microbiome of every human is unique. The characteristics of the bacteria are encoded in their genes. Our goal is to change the players undertaking our body functions in a positive direction and to increase the number of tasks they undertake. We detect bacteria that make up the gut microbiome using advanced molecular genetic analyzes. The main process is to classify all the bacteria in the gut according to their genetic structure and to analyze how many bacteria are there in each class in the population. As it has many advantages such as having sufficient database, being present in all bacteria, being protected functionally, and having well-protected and heterogeneous regions, we use the 16S_rRNA-nucleotide-sequencing method, among the DNA sequencing technologies, in the analysis of stool sample.',

    'bilim_row_1_1' => 'Personalized Health',
    'bilim_row_1_2' => 'Health begins with how your body processes food. Gut microbiome is directly related to your health.',

    'bilim_row_2_1' => 'Personalized Diet',
    'bilim_row_2_2' => 'Significant differences in blood sugar levels observed among different people consuming the same food emphasize why personalized dietary preferences are more feasible than universal dietary recommendations.',

    'bilim_row_3_1' => '16S rRNA-nucleotide sequence',
    'bilim_row_3_2' => 'We determine the composition and diversity of the gut microbiome at the genetic level by using high-throughput 16S rRNA-nucleotide sequencing methods. ',

    'bilim_row_4_1' => 'Artificial Intelligence Algorithms',
    'bilim_row_4_2' => 'We train our artificial intelligence algorithms with high-quality peer-reviewed scientific literature studies that are carefully selected, expert information from our science team, biological tests, and client feedbacks.',

    'bilim_row_5_1' => ' Erciyes University Genome and Stem Cell Center',
    'bilim_row_5_2' => 'The gut microbiome is analyzed by 100% local scientists at Erciyes University Genome and Stem Cell Center Laboratory in accordance with the most recent studies and in light of science. ',

    'bilim_row_6_1' => 'Science Team',
    'bilim_row_6_2' => 'Microbiology specialist Assoc. Prof. Dr.  Dr. Aycan Gündoğdu, who worked in Australia for 7 years in this field, and Bioinformatics Engineer Dr. Özkan Ufuk Nalbantoğlu, who worked in the USA for 10 years in this field, have brought their experience in science and technology to our country.',


    //enbiosis TEST page

    'test_1' => '<b>The company is established in Turkey and is based on a personalized algorithmic diet. With enbiosis, we train artificial intelligence algorithms with machine learning and deep learning. We use the latest technology sequencing method, as well as the advanced statistical techniques, for the microorganisms in the sample.</b><br/><br/> This test provides personalized nutritional recommendations that help the body show its highest performance and it further analyzes the activity of microorganisms in your microbiome. enbiosis helps you to determine gut activity related to specific areas of interest. We give you status reports on many issues including digestive system activity, sleep quality, intestinal mobility, microbiome age, immune system, and antibiotic damage. All test results and personalized nutritional recommendations are sent to you through a mobile network specific to you, with detailed descriptions. What we are aiming at with enbiosis is to provide a healthy gut and a personalized diet program.</p>',

    'test_2_1' => '<span>Your Personalized Microbiome <br/>Sequencing Report</span>',
    'test_2_2' => '<span>The most appropriate foods for you</span>',
    'test_2_3' => '<span>The most appropriate snacks for you</span>',
    'test_2_4' => '<span>Your personalized food scores</span>',
    'test_2_5' => '<span>Your personalized health</span>',
    'test_2_6' => '<span>Weekly feedback by enbiosis dietitian </span>',

    // SSS page

    's-s-s' => 'FAQs',

    's_title' => 'Frequently asked questions',
    's_dec' => '',

    's_1_1' => 'What are the concepts of microbiota and microbiome?',
    's_1_2' => 'We host trillions of microscopic tiny creatures in our bodies such as bacteria, fungi, and viruses with whom we share our food, drink, stress, smile, and diseases. These creatures have spread to many parts of our bodies and have been accompanying us from the moment we exist. We call the group created by these creatures ‘microbiota’. We collectively describe the habitat, genomes and interaction conditions of these tiny organisms as \'microbiomes\'.',

    's_2_1' => 'Why are some diets good for some people but not for others?',
    's_2_2' => 'Through the studies we have carried out, we have observed that we asked the wrong question. The situation is not as superficial as a low-carbohydrate, low-fat and sugar-free life. This is why we cannot reach everyone at the rate we want with general healthy information. The right question is \'Which diet is the most appropriate one for me?\' The answer is in your gut.',

    's_3_1' => 'What do you do with microbiome analysis? Why it is necessary?',
    's_3_2' => 'Your microbiome analysis shows your reaction to food. Every person gives different reactions to the same food. This analysis is necessary in order to balance your blood sugar levels and create a healthy and personalized diet list for you. ',

    's_4_1' => 'who should take this test?',
    's_4_2' => 'A great population is in question.  Any person aged 16 and above can take this test. Those who are in the prediabetes stage, have any acute-chronic illness, weight and sleep problems, have autoimmune disorders, and who are healthy but want to have detailed information about their inner world in case of any possible diseases can take this test.',

    's_5_1' => 'What\'s the solution?',
    's_5_2' => 'In this system, we create personalized nutrition recommendations based on artificial intelligence as a result of our analyzes and specifically ensure that the person consumes the most appropriate food in the most accurate way. We can eliminate the problems of individuals with specific nutrients that we provide for specific microorganisms. ',

    's_6_1' => 'How does the system work?',
    's_6_2' => 'First, users should subscribe by downloading the mobile application or via our website. This customized program starts after a few questions about the individual\'s health history and general diet habits are asked. Stool samples taken using the sample collection kit provided to the users are sent to Erciyes University Genome and Stem Cell Center. This laboratory is the first and largest genome and stem cell research center of Turkey. Here, the microbial DNA of the sample is analyzed, individual microbiome profiles are obtained, and the results are shared with the user through the mobile app with exclusive confidentiality. Which one of the thousands of foods is suitable for you is shown through the application with scoring. This program, which regulates the body mass index of the individual in a healthy way, is created with the dietary recommendation algorithms according to the individual’s microbiome profile.',

    's_7_1' => 'How can we get the kit? How do we prepare the sample?',
    's_7_2' => 'You can give order easily via enbiosis mobile app or website. You can collect your sample at home at any time in less than two minutes, following the steps shown on the box. Your kit includes everything you need.  Then, send your sample back to our address on the prepaid envelope attached.',

    's_8_1' => 'Is there any possibility of germ contamination from anywhere while preparing the sample? How stable is my sample when I rotate it in the tube after collected it? Is it really possible to send it at room temperature? Do bacteria start to grow? Will my microbiome profile be affected by shipping?',
    's_8_2' => 'Don’t worry. Your sampling tube contains DNA stabilizing liquid that kills microorganisms in the case of contact and protects the released microbial DNA from degradation. Any possible infectious agent dies before shipment. The said liquid is a special component designed to stabilize and protect your sample. When you transfer your sample from the swab to the sampling tube, the growth of microorganisms stops. This is very important because we want to know exactly what it looks like when you collect your sample. If your microorganisms are allowed to grow, the contents of the tube will change until they arrive at our laboratory. We ensure that they come to our laboratory without being affected by any conditions during transportation.	',

    's_9_1' => 'How often should I take this test?',
    's_9_2' => '30 days is sufficient to suppress the harmful activities of the microbiome and increase its beneficial activities and to provide a more permanent effect on your microbiome and your overall health. You are the most basic measure of yourselves. We recommend you to repeat the test several times a year or as often as necessary to monitor the changes in your functional active microbiome.',

    's_10_1' => 'Are there any situations where I need to repeat the test, except for periodical analyses?',
    's_10_2' => 'You should repeat the test if more than six months have passed since your last test, you have received antibiotic treatment and more than 48 hours of inpatient treatment in any health center, you have undergone any operation, you have traveled abroad, and you are under too much stress.',

    's_11_1' => 'Does the microbiome change? Is the microbiome of every human the same?',
    's_11_2' => 'The microbiome of every human is unique. The characteristics of the bacteria are encoded in their genes. These bacteria seem to be basically the same, however, they transfer their tasks to different players over time. Microbiome core does not change, but its surrounding environment varies according to the person and lifestyle.  The thing we call the core is the function. This function occurs continuously in our bodies. Our main goal is to change the players who undertake this function in the best way and to place good players who undertake multiple tasks. ',

    's_12_1' => 'What kind of tasks does microbiome undertake? Why is it that important?',
    's_12_2' => 'Homo sapiens have been evolving and interacting for thousands of years with friendly microorganisms in the body. Specifically, metabolites nucleic acids produced by living microorganisms in the gut and even genetic materials are transported into the human systemic circulation and lead to activation of inactive genes through epigenetic mechanisms, which can benefit health, lead to the development of various diseases and evolution. Gut microbiota is a complex ecosystem that provides important functions to the host in a healthy state by affecting metabolism, modulating the immune system, and protecting the body against pathogens. The key to a healthy and long life is to have a healthy gut structure. Therefore, there are numerous benefits of knowing the content of the individual gut microbiome. ',

    's_13_1' => 'Which analysis method do you use?',
    's_13_2' => 'We use high-throughput 16S rRNA-nucleotide sequence in these tests. Thanks to new (metagenomic) techniques, our perspective on the world of tiny creatures has changed. We carry as many microbial cells as our own cells. We detect bacteria that make up the gut microbiome using advanced molecular genetic analyzes. We are able to carry out the process completely, including bacteria whose name or function we don’t know. The main process is to classify all the bacteria in the gut according to their genetic structure and to analyze how many bacteria are there in each class in the population. In the stool sample, we can easily access this analysis through the 16S_rRNA sequencing method, which is the latest technology DNA sequencing method. ',

    's_14_1' => 'Will you send my data to abroad? Is the system safe?',
    's_14_2' => 'Here, we are working on a great project for the use of a 100% native, 100% scientific and 100% innovative technology, which is produced by our scientists, by the end-users. The microbial DNA of the sample is analyzed at Erciyes University Genome and Stem Cell Center, personal microbiome profiles are obtained and the reports are shared with the users through the mobile app through a safe and user-specific network.',

    's_15_1' => 'How does artificial intelligence contribute to the system ?',
    's_15_2' => 'Thanks to artificial intelligence, we can discover which foods are ideal for you. Thus, we aim for optimal health. Our artificial intelligence engine performs multiple analyses of the data included in our extensive database. High-quality peer-reviewed scientific literature studies that are carefully selected, expert information from our science team, biological tests, and client feedbacks train our artificial intelligence machine. With the help of artificial intelligence technology, we obtain much more data more quickly and clearly than the human mind can analyze.  Artificial intelligence has been proved to be useful in medicine and in many fields and has made a breakthrough in today\'s technology. Artificial intelligence transfers the ability of human beings to think logically, to learn and to apply what they have learned to new situations to the machines.  It helps researchers accomplish challenging tasks faster and accelerates medical research. Artificial intelligence is a solution to the concept of ‘time’, which is the biggest problem of our era and which we cannot supply in any way.',

    's_16_1' => 'Does this system require a dietitian?',
    's_16_2' => 'We\'ve created a robot specializing in valuable data collected from studies around the world. It contains data about thousands of bacteria and nutrients, as well as information that has been generated with thousands of accumulated experiences, which can\'t even be found in the literature. We would like to provide a system that takes the minor jobs from the professionals and takes them to the controlling process faster. In the work we do as a draft, we focus on the players who created the final product and provide the proper raw material. Moreover, we don\'t just leave the whole process to the machine of course; you are constantly in touch with our enbiosis dietitians both online and face-to-face. In addition to the detailed report, your personalized diet list created by our enbiosis dietitians allows you to plan your day in an easier and more balanced way. You will be guided by your enbiosis dietitian on issues such as composition, portion, balance, variety, recommendation, sufficiency, reliability and meal times.  Following the reporting stage, you will be followed continuously through the mobile application and face-to-face interviews and feedbacks will be provided weekly. During this process, we also provide psychological motivation support. You should go to your enbiosis dietitian',

    's_17_1' => 'Does it matter which day I collect stool samples? I haven\'t made any major changes in my diet recently, but I eat different foods every day. Does my microbiome change from one day to the next? Is there a recommended day to collect the sample?',
    's_17_2' => ' It doesn\'t matter which day you take the sample or if there are small changes in your daily diet. We know that there are no major changes in a person\'s microbiome unless major dietary changes are made. However, other studies have shown that major dietary changes, changes in health, or participation in international travel change your microbiome. In such cases, it would be useful to do multiple tests to see how the microbiome will change.',

    's_18_1' => 'On the kit you\'ve sent, it is written that I should take the sample from the toilet paper. Does it matter which feces piece I take from this toilet paper? If I take 2 samples from the same toilet paper and send them for testing, do the microbiome profiles of these 2 samples look the same?',
    's_18_2' => 'Yes, two samples taken from the same toilet paper have been found to be almost identical. ',

    's_19_1' => 'How many times can this process be repeated? How will these microbiome profiles look if you process the same sample multiple times? What happens if you process the same sample three times? Do you obtain the same results every time or do these three samples look different each time they are processed? ',
    's_19_2' => ' Three samples taken from the same person are very similar, independently from reagents or equipment used and persons performing the test in the laboratory. Since each human being has its own unique microbiome, we can clearly distinguish these samples. You are the criterion. Within the scope of the abovementioned conditions, we recommend that the test should be repeated several times a year and your microorganism functions should be checked.',

    's_20_1' => 'What happens if you process the same sample hundreds of times? Do all the results still look the same?',
    's_20_2' => 'Even if they are processed more than 300 times, the results will be very similar.',


    //iletisim page

    'iletisim_bilgiler' => 'Our Address Information',
    'iletisim_tel' => 'Phone',
    'iletisim_mail' => 'E-posta',

    'iletisim_gec' => 'Get in touch',
    'il_ad_soy' => 'Name And Surname',
    'il_email' => 'E-Mail',
    'il_mesaj ' => 'Subject',
    'il_me' => 'Your Letter...',
    'il_gonder' => 'COMPLETE ORDER',


    //product page

    'EnBiosis_Product' => 'Enbiosis Product',
    'EnBiosis_Products' => 'Enbiosis Products',
    'sepete_ekle' => 'Add To Cart',
    'price' => 'Price',

    // caart page

    'Shopping_Cart' => 'Shopping Cart',
    'Product' => 'Product',
    'Quantity' => 'Quantity',
    'Subtotal' => 'Subtotal',
    'Continue_Shopping' => 'Continue Shopping',
    'Total' => 'Total',
    'Check_Out' => 'Check Out',

    //Checkout Page

    'Billing_address' => 'Billing address',
    'Name' => 'Full Name',
    'Email' => 'Email',
    'Payment' => 'Payment',
    'Name_on_card' => 'Name on card',
    'Name_on_card_desc' => 'Full name as displayed on card',
    'Credit_card_number' => 'Credit card number',
    'Expiration_year' => 'Expiration year',
    'Expiration_Month' => 'Expiration Month',

    //odem
    'odeme' => 'Payment',
    'odeme_hata' => 'An error occurred',
    'odem_hata_des' => 'Failed to process payment, please try again',

    'Tamamlandı_des' => 'Payment process completed successfully, some Email will be sent to you after',
    'Tamamlandı' => 'Payment completed',

    'kupon' => 'coupon',
    'submit' => 'SUBMIT',
    'Errors' => 'Errors',
    'coupon_hata' => 'coupon code Not matched',
    'coupon_hata_des' => 'The coupon code you entered is not in our records',
    'tebrikler' => 'Congratulations',
    'tebrikler_des' => 'The coupon code you enter gives you a special :number % discount',
    'indirim' => 'discount',
    'sil_kopun' => 'Remove Coupon ',
    'coupon_sil' => 'Coupon Removed',
    'coupon_sil_des' => 'Coupon Code in payment Form Successfully Removed',
    'coupon_hata_eslesme' => 'coupon code Error matching',
    'coupon_hata_eslesme_des' => 'coupon discount price changed if you think there is an error please contact us',
    'Close' => 'Close',
    'de_code' => 'Dietetian Name (optional)',
	'de_evet' => 'Yes',
	'de_hayir' => 'No',
    'cart_delete' => 'Product Removed',
    'cart_delete_des' => 'Product Removed successfully From Cart',
	'change_quantity' => "Change",
    'cart_update' => 'Cart Updated',
    'cart_update_des' => 'Product Quantity in Cart Successfully Updated',
    'menu' => 'MENU',

    'cart_created' => 'Product Added',
    'cart_created_des' => 'Product Successfully Added To Cart',

    //register-kit
    'register-kit' => 'Register Kit',
    'kit_code' => 'Kit Code',
    'kit_hata' => 'Kit Code Not matched',
    'kit_hata_des' => 'The Kit Code you entered is not in our records',
    'genel_hata' => 'An error occurred, please try again.',
    'address' => 'Address',
    'üye_mi' => 'Are you a member? Login',
    'cinsiyet' => 'Gender',
    'lütfen_seçin' => 'Please select',
    'erkek' => 'Male',
    'kadin' => 'Female',
    'belirtilmemiş' => 'Not Specified',
    'odeme_sekli' => 'Payment Method',
    'tek_cekim' => 'Cash',
    '2_taksit' => '2 Installment',
    '3_taksit' => '3 Installment',
    'taksit' => 'Installment'


];