<?php

return [
        /*
        |--------------------------------------------------------------------------
        |  checkout page
        |--------------------------------------------------------------------------
        */
        /*
        |--------------------------------------------------------------------------
        |  page Titles
        |--------------------------------------------------------------------------
        */
        'page_title'        => 'Mikrobiyota Testi ile Kendini Keşfet | Enbiosis',
        'page_description'  => 'BEDENİNDE HAYAT BULAN SAVAŞÇILARI KEŞFETMENİN ZAMANI GELDİ!',
    
        /*
        |--------------------------------------------------------------------------
        |  section header
        |--------------------------------------------------------------------------
        */
        'header_title'  => 'اشتري',
        'breadcrumb'    => ['الصفحة الرئيسية', 'اشتري'],
    
        'Mikrobiyom Analizi' => 'Mikrobiyom Analizi',
        'Kişiselleştirilmiş Beslenme Rehberi' => 'Kişiselleştirilmiş Beslenme Rehberi',
        'Diyetisyen desteği' => 'Diyetisyen desteği',
    
        'mikrobiyom-saglik-paketi' => 'حزمة الصحة لتحليل بكتيريا الأمعاء',
        'mikrobiyom-saglik-paketi-premium' => 'حزمة الصحة بلس لتحليل بكتيريا الأمعاء',
    
        'desc' => 'Bağırsaklarında trilyonlarca bakteri var, bu bakterileri keşfederek sorunların kaynağına inmek ve kişiselleştirilmiş sağlık yolculuğuna adım atmak artık senin elinde!',
    
        'box' => [
            'Bağırsak bakteri ekosisteminin analizi',
            'Vücudunun ihtiyaç duyduğu besinlerin raporu',
            '6 haftalık diyetisyen takibi'
        ],
         /*
        |--------------------------------------------------------------------------
        |  section KARGO
        |--------------------------------------------------------------------------
        */
        'kargo_adesi' => ['عنوان', 'الشحن'],
    
        'adi_soyadi' => 'الاسم والكنية',
    
        'e_posta' => 'البريد الإلكتروني',
    
        'tel_num' => 'رقم التلفون',
    
        'ulke' => 'الدولة',

        'il' => 'المدينة',
    
    
        'ilce' => 'الحي',
    
        'adres' => 'العنوان الكامل',
    
        //  /*
        // |--------------------------------------------------------------------------
        // |  section FATURA
        // |--------------------------------------------------------------------------
        // */
        // 'yes_no' => 'Fatura adresi kargo adresiyle aynı mı ?',
        // 'yes' => 'Evet',
        // 'no' => 'Hayır',
    
        // 'fatura_adesi' => ['FATURA', 'ADRESİ'],
        // 'fadres' => 'Fatura Adresiniz',
        // 'fadres_place' => 'Fatura Adresiniz',
    
         /*
        |--------------------------------------------------------------------------
        |  section ODEME
        |--------------------------------------------------------------------------
        */
        'odeme' => 'الدفع',
    
        'kart_sahibi' => 'اسم صاحب الكرت',
        'kart_num' => 'رقم الكرت',
        'kart_son_kullanma_tarihi' => 'انتهاء الصلاحية',
        'ay' => 'شهر',
        'yil' => 'سنة',
        'cvc' => 'cvc',
    
        'sozlesme' => [
            'أوافق على <u> اتفاقية البيع عن بعد </u>.',
            'أقبل <u> نموذج الإخطار المسبق </u>',
            'أقبل <u> نموذج التوضيح </u>.'
        ],
        'gonder' => 'اكمل الدفع',

         /*
        |--------------------------------------------------------------------------
        |  section sidebar
        |--------------------------------------------------------------------------
        */
    
        'product_name' => 'ENBIOSIS MİKROBİYOM ANALİZİ',
        'fiyat' => 'Fiyat',
        'indirimli_fiyat' => 'İndirimli Fiyat',
        'miktar_degistir' => 'Miktarı değiştir',
        'degistir' => 'DEĞİŞTİR',
    
        'taksit' => 'Taksit Seçeneği',
        'taksitler' => 'Taksit Seçenekleri',
        'aylik_odeme' => 'Aylık Ödeme',
        'toplam_tutar' => 'Toplam Tutar',
        'Tek Çekim' => 'Tek Çekim',
        '2 Taksit' => '2 Taksit',
        '3 Taksit' => '3 Taksit',
        '6 Taksit' => '6 Taksit',
    
    
        'kupon_kullan' => 'استخدم قسيمة تخفيض',
        'kupon_kodu' => 'القسيمة',
        'sil' => 'إزالة',
        'sorgula' => 'استعلام',
        'indirim_uygulanmistir' => 'تم تطبيق قسيمة التخفيض بقيمة',
    
        'de_var_mi' => 'Referans Kodunuz Var mı ?',
    
        'diyetisyen_ismi' => 'Referans Kodunuz',
    
    
    ];