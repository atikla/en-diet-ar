
<?php
return [
    /*
    |--------------------------------------------------------------------------
    |  Public layout
    |--------------------------------------------------------------------------
    */
    // 'bulten'                => 'Bizden Haberdar Olmak İster Misiniz?',
    // 'bulten_desc'           => 'ENBIOSIS ve Mikrobiyom ile ilgili güncel haberleri takip etmek için bültenimize abone olabilirsiniz.',
    // 'kayit'                 => 'اشترك',
    // 'urunlerimiz'           => 'ÇÖZÜMLERİMİZ',
    // 'mikrobiyom'            => 'MİKROBİYOM NEDİR?',
    // 'mikrobiyom_footer'     => 'MİKROBİYOM',
    // 'hakkimizda'            => 'HAKKIMIZDA',
    'satin_al'              => 'اشتري',
    'kit_kayit'             => 'تسجيل العينة',
    'giris_yap'             => 'تسجيل الدخول',
    // 's_s_s'                 => 'S.S.S',
    // 'iletisim'              => 'İLETİŞİM',
    // 'bize_ulasin'           => 'BİZE ULAŞIN',
    // 'turkiye_ofisi'         => 'Türkiye Ofisi',
    // 'turkiye_ofisi_desc'    => 'Maslak 1453 T4-B/91<br>Sarıyer/İstanbul',
    // 'uae_dubai_ofisi'       => 'UAE Dubai Ofisi',
    // 'uae_dubai_ofisi_desc'  => 'Park Avenue Building Office: 205 Dubai Silicon Oasis / UAE',
    // 'telefon'               => 'Telefon',
    // 'e_posta'               => 'E-posta',
    // 'cookie_title'          => 'Çerez Kullanımı',
    // 'cookie_desc'           => 'Sana daha iyi bir alışveriş deneyimi sunabilmek için çerezler kullanıyoruz. Detaylı bilgi için <a href="' . route('cookies-policy') . '">Çerezler Politikamızı</a>  inceleyebilirsin.',
    // 'cookie_ok'             => 'Tamam',
    // 'cookie_more'           => 'Detaylı bilgi',

];
