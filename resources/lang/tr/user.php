  
<?php
return [
    /*
    |--------------------------------------------------------------------------
    | 
    |--------------------------------------------------------------------------
    |
    | 
    | 
    | 
    |
    */
    // index.blade
    'ana' => 'Ana Safya',


    //survey.blade
    'anket' => 'Anket',
    'anket_yok' => 'Anket Bulunamadı',
    'anket_yok_des' => 'Sistemimizde doldurulacak bir Anket yok Bu sorunu çözmek için lütfen bizimle iletişime geçin.',
    'anket_bolum' => 'bu anketin :number bölümü var',
    'koşullu_secim' => 'Koşullu Seçim',
    'sorular' => 'Sorular',
    'sonraki' => 'Sonraki',
    'onceki' => 'Önceki',
    'kaydet_ve_bitir' => 'Kaydet ve bitir',
    'gonder' => 'Gönder',
    'bilgi' => 'Bilgilendirme ',
    'anket_bilgi' => ':code kodlu kit için anket doldurmanız gerekmektedir.',
    'hatali_url' => 'Hatalı Url',
    'anket_dolduruldu' => ':code kodlu kit için anketi doldurduğunuz için teşekkür ederiz ',
    'bilgilendirme' => 'Bilgilendirme '

];