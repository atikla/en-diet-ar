<?php

return [

    /*
    |--------------------------------------------------------------------------
    | API Language Lines
    |--------------------------------------------------------------------------
    */

    'genel_hata' => 'Hata oluştu lutfen yeniden deneyiniz',
    'kit_hata' => 'Bu Kit size ait değil',
    'kit_error_desc' => 'Girdiğiniz kod kayıtlarımızda bulunmamaktadır.',
    /*
    |--------------------------------------------------------------------------
    | AuthController
    |--------------------------------------------------------------------------
    */
    'login_successfully' => 'Başarılı Giriş',
    'login_unsuccessful' => 'Girdiğiniz kullanıcı bilgileri hatalıdır',

    /*
    |--------------------------------------------------------------------------
    | PasswordResetController
    |--------------------------------------------------------------------------
    */
    'send_reset_password' => 'E-posta adresinize bir sıfırlama bağlantısı gönderildi.',
    'reset_password_error' => 'Girdiğiniz e-posta kayıtlarımızda bulunmamaktadır',
    'reset_password_done' => 'Password reset done successfully',

    /*
    |--------------------------------------------------------------------------
    | UserRegisterKitController
    |--------------------------------------------------------------------------
    */
    'register_unccessful' => 'Girdiğiniz kullanıcı bilgileri hatalıdır',
    'register_successfully' => 'Kit kaydınız başarılı bir şekilde gerçekleşti',

    /*
    |--------------------------------------------------------------------------
    | UserController
    |--------------------------------------------------------------------------
    */
    'already_verified_email' => 'Bu e-posta zaten doğrulanmış',

    'verification_email_send' => 'Doğrulama kodunuz girmiş olduğunuz e-postaya gönderildi. Lütfen kontrol ediniz',
    'verification_email_done' => 'E-posta adresiniz doğrulandı.',
    'verification_email_expired' => 'Girdiğiniz kod süresi dolmuş.',
    'verification_email_error' => 'Girdiğiniz kod süresi dolmuş.',

    'survey_information' => ':code kodlu kit için anket doldurmanız gerekmektedir.',
    'no_survey_desc' => 'Sistemimizde doldurulacak bir Anket yok Bu sorunu çözmek için lütfen bizimle iletişime geçin.',

    'no_result_yet' => 'Analiz sonuçlarınız henüz yüklenmedi.',

    'no_kit_suggestion' => 'Önerileriniz henüz hazırlanmadı.',

    'no_resource' => 'Kaynak bulunamadı.',

    'send_pdf' => 'ENBIOSIS Mikrobiyom Analiz raporunuz ve Besin Skorlarınız birkaç dakika içinde girdiğiniz e-posta adresine gönderilecektir',
    'no_file' => 'Dosya bulunamadı.',

    'iyi_Skor' => 'İyi Olan Skorlarım',
    'orta_Skor' => 'Ortalama Skorlarım',
    'kotu_Skor' => 'Düzeltmem Gereken Skorlarım',

    /*
    |--------------------------------------------------------------------------
    | DietitianController
    |--------------------------------------------------------------------------
    */
    'no_result_found' => 'Eşleşen danışan bulunamadı...',
];
