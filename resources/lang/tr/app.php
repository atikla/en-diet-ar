
<?php
return [
    /*
    |--------------------------------------------------------------------------
    |
    |--------------------------------------------------------------------------
    |
    |
    |
    |
    |
    */

	'satinal' => 'SATIN AL',
    // nav And Haeder
    'dil' => 'DİL',
    'flag' => 'tr',
    'anasafya' => 'ANASAYFA',
    'hakkimizda' => 'HAKKIMIZDA',
    'bilim' => 'BİLİM',
    'enbiosis_test' => 'ENBİOSİS TEST',
    'shop' => 'SATIN AL',
    'sss' => 'S.S.S',
    'iletisim' => 'İLETİŞİM',
    'login' => 'GİRİŞ YAP',
    'register_kit' => 'KİT KAYIT',
    'cart' => 'SEPET',
    'header_6' => 'Detayli Bilgi',

    //footer
    'urunlerimiz' => 'ÜRÜNLERİMİZ',
    'sistem_work' => 'SİSTEM NASIL ÇALIŞIR?',
    'mikro_bes' => 'MİKROBİYOTA VE BESLENME',
    'adres' => 'ADRES',

    //first setion
    'header_1' => 'KENDİNİ KEŞFET',
    'header_2' => 'BESLENMENE HÜKMET',
    'header_3' => 'Sipariş Ver',
    'header_4' => 'Son teknoloji dizileme yöntemini kullanarak, bağırsak mikrobiyomunu oluşturan bakterileri saptamaktayız.',
    'header_5' => 'Biz tam olarak ne arıyoruz? İnsanlar için genel en uygun sağlıklı diyeti mi yoksa kendimiz için en uygun, sağlıklı diyeti mi ?',

    //desc section
    'desc_1' => 'Bağırsak dünyana hoş geldin.',
    'desc_2' => 'Trilyonlarca bakteri doğrudan sindirim sistemini etkiliyor. ',
    'desc_3' => 'Vücudunun iyiliğinde muazzam bir rol üstlenen mikroorganizmalarını dinleme vakti!',
    'desc_4' => 'Bireysel Diyet Tasarımı ve mikrobiyom verilerinin kullanımı bugünkü teknolojide eksik kalan parçamız.',
    'desc_5' => 'Enbiosis, biyokimyasal parametreler de dahil olmak üzere her bireyin özgünlüğünü kişiselleştirilmiş ve uygulanabilir bir plana dönüştürmeyi hedefliyor.  Sadece test et ve zaman içinde mikrobiyomunu izle, seçim senin.',

    // en-carousel
    'carousel_1_1' => 'İDEAL KİLO',
    'carousel_1_2' => 'Enbiosis ile ideal kilona gelebilirsin! Spesifik bakterilere spesifik besinler sağlıyoruz.',

    'carousel_2_1' => 'SİNDİRİM SİSTEMİ',
    'carousel_2_2' => 'Sindirim sistemini düzenleyebilirsin. Şişkinlik, karın ağrısı, ishal ve kabızlıktan uzak bir hayat mikrobiyomun elinde!',

    'carousel_3_1' => 'UYKU KALİTESİ',
    'carousel_3_2' => 'Uyku kaliteni arttırabilirsin. Uyku-uyanıklık döngüsünü düzenleyen melatonin hormonuna katkı sağlayan bakteri türleriyle daha iyi uyumana yardımcı olabiliriz.',

    'carousel_4_1' => 'ENERJİ',
    'carousel_4_2' => 'Enerji seviyeni yükseltebilir, gün içerisinde daha iyi verim sağlayabilirsin.',

    'carousel_5_1' => 'ZİHİNSEL BERRAKLIK',
    'carousel_5_2' => 'İnsan bağırsak mikrobiyomu beyine sinyal göndermede anahtar rol oynar. Bağırsaklarınla düşündüklerini etkileyebilirsin.',

    'carousel_6_1' => 'DAHA FAZLASI',
    'carousel_6_2' => 'Herhangi bir şikayetinde Enbiosis olarak yanınızdayız. Mikrobiyom bize gereken tüm cevapları verecektir.',

    //How Does the System Work?
    'system_1_1' => 'Mikrobiyom Örneğinizin Alınması',
    'system_1_2' => 'Size gönderilen kitteki adımları izleyerek bağırsak mikrobiyom örneğinizi kendiniz kolayca alın. Deney tüpünü kitin içerisindeki zarfa koyarak ödemesiz olarak postaya verin',

    'system_2_1' => 'Laboratuvar Analizi',
    'system_2_2' => 'Mikrobiyom örneğiniz Erciyes Üniversitesi Genom ve Kök Hücre Merkezi Laboratuvarı tarafından teslim alınır ve 16S rRNA-nükleotit- dizileme teknolojisi ile dizilenir. Elde edilen veriler bir yapay zeka sistemi olan Enbiosis tarafından analiz edilir.',

    'system_3_1' => 'Raporlama ve Diyet Önerileriniz',
    'system_3_2' => 'Mikrobiyom durumunu gösteren raporlarınıza ve size özgü oluşturulmuş kişiselleştirilmiş beslenme önerilerinize Enbiosis mobil uygulamasından erişin.',

    //Technology
    'teknoloji' => 'TEKNOLOJİ',

    'teknoloji_1' => 'Yapay zeka, insanların mantıklı düşünebilme, öğrenebilme ve öğrendiklerini yeni durumlara uygulama kabiliyetlerini makinelere aktarmaktadır. Yapay zeka teknolojisi ile insan aklının analiz edemeyeceği çoklukta verileri çok daha hızlı ve net bir şekilde elde ediyoruz. Yapay zeka, araştırmacıların zorlu görevlerini daha hızlı gerçekleştirmelerine yardımcı olarak tıbbi araştırmayı hızlandırmaktadır. Çağımızın en büyük problemi olan ve hiçbir şeklide tedarik edemediğimiz ‘zaman’ kavramına bir çözümdür.',
    'teknoloji_2' => 'Dünya genelinde yapılan çalışmalarla toparlanmış çok kıymetli verilerden uzmanlaşmış bir yapay zeka algoritması oluşturduk. Bünyesinde binlerce bakteri ve binlerce besin verisi, binlerce biriken tecrübe ile oluşturulmuş. Kurduğumuz bu sistemde, yaptığımız analizler sonucu yapay zeka tabanlı kişiselleştirilmiş diyet listeleri oluşturuyor ve spesifik olarak bireyin en doğru şekilde beslenmesini sağlıyoruz.',

    // MERAK EDİLENLER
    'merak' => 'MERAK EDİLENLER',

    'merak_1_1' => 'Mikrobiyom analizi ile ne yapıyorsunuz? Neden gerekli? ',
    'merak_1_2' => 'Mikrobiyom analiziniz bize yiyeceklere olan tepkinizi göstermektedir. Her insanın aynı yiyeceğe olan tepkisi farklıdır. Kan şekerinizi dengelemek, sizin için sağlıklı kişiselleştirilmiş bir diyet listesini tam anlamıyla oluşturabilmek için bu analiz gereklidir.',

    'merak_2_1' => 'Bu testi kimler yaptırmalı? ',
    'merak_2_2' => 'Büyük bir popülasyon söz konusu. 16 yaş üzeri herkes bu testi yaptırabilir. Prediyabet aşamasında olanlar, herhangi bir akut-kronik rahatsızlığı olanlar, kilo problemi yaşayanlar, uyku problemi yaşayanlar, otoimmün rahatsızlığı olanlar veya sağlıklı olup da olası ihtimallere karşı iç dünyaları hakkında ayrıntılı bilgi sahibi olmak isteyenler yaptırabilir.',

    'merak_3_1' => 'Sistem nasıl çalışıyor?',
    'merak_3_2' => 'Öncelikle kullanıcılar mobil uygulamayı/web indirip abone olmaktadır. Bu kişisel program, bireyin sağlık geçmişini ve genel diyet alışkanlıklarını sorduktan sonra başlamaktadır. Kullanıcılara verilen numune toplama kitiyle alınan dışkı örnekleri, Erciyes Üniversitesi Genom ve Kök Hücre Merkezine gönderilmektedir. Bu laboratuvar Türkiye’nin alanında ilk ve en büyük genom ve kök hücre araştırma merkezidir. Burada numunenin mikrobiyal DNA\'sı analiz edilerek, kişisel mikrobiyom profilleri elde edilmekte ve raporlar kullanıcı ile özel bir gizlilikle paylaşılmaktadır. Binlerce adet besinden hangisinin sana ne kadar uygun olduğu skorlama ile uygulama üzerinden gösterilmektedir. Bireyin vücut kitle indeksini sağlıklı bir şekilde düzenleyen bu program, kişinin mikrobiyom profiline göre diyet öneri algoritmaları ile üretilmektedir.',

    'merak_4_1' => 'Kiti nasıl temin edebiliriz? Numuneyi nasıl hazırlayacağız?',
    'merak_4_2' => 'Enbiosis web indirin ve kolayca sipariş edin. Numunenizi istediğiniz zaman evde iki dakikadan az bir sürede, kutu üzerinde gösterilen adımları izleyerek alabilirsiniz. Kitiniz ihtiyacınız olan her şeyi içerir.  Daha sonra numunenizi ekteki ön ödemeli zarftaki adresimize geri gönderin.',

    'merak_5_1' => 'Numuneyi hazırlarken herhangi bir yerden mikrop bulaşma ihtimali var mı? Aldıktan sonra tüpte döndürdüğümde numunem ne kadar stabildir? Gerçekten oda sıcaklığında gönderebilir miyim? Bazı bakteriler büyümeye başlar mı? Mikrobiyom profilim nakliye sürecinde değişecek mi?',
    'merak_5_2' => 'Endişelenme. Numune alma tüpünüz, temasta mikroorganizmalarını öldüren ve açığa çıkan mikrobiyal DNA’yı parçalamaya karşı koruyan DNA stabilize edici sıvı içerir. Herhangi bir olası bulaşıcı ajan postalamadan önce ölmüştür. Sıvı, numunenizi stabilize etmek ve korumak için tasarlanmış özel bir bileşendir. Numunenizi swabdan örnekleme tüpüne aktardığınız anda, içindeki mikroorganizmaların büyümesi durur. Bu çok önemlidir, çünkü numunenizi topladığınız anda neye benzediğini tam olarak bilmek isteriz. Mikroorganizmalarınızın büyümeye devam etmesine izin verilirse, tüpün içeriği laboratuvarımıza gelene kadar çok farklı olacaktır. Nakliye sürecinde herhangi bir koşuldan etkilenmeden laboratuvarımıza gelmektedir.',

    'merak_x' => 'DAHA FAZLASI İÇİN TIKLAYINIZ',


    //Hakkimizde Page

    'NEDEN_MİKROBİYOM?' => 'NEDEN MİKROBİYOM?',

    'hak_page_1' => 'Homo sapiens, binlerce yıldır vücudunuzdaki dost mikroorganizmalarla birlikte ve etkileşim halinde evrimleşmektedir. Özellikle bağırsakta yaşayan mikroorganizmaların ürettikleri metabolitler ve hatta genetik materyaller insan sistemik dolaşımına katılmakta, epigenomik mekanizmalarla inaktif genlerin aktive olmasına, böylelikle sağlıklılığın sürmesine, birtakım hastalıkların oluşmamasına ve evrime neden olmaktadır.<br/><br/> Her insanın mikrobiyomu kişiye özeldir. Mikrobiyom insandan insana büyük değişiklik göstermekte ve yiyeceğe olan tepkisiyle kan şekerini farklı etkilemektedir. Yaptığımız mikrobiyom analizine göre vücudunuzdaki mikroorganizmaların faaliyetleri, çeşitliliği ve ürettikleri metabolitler hakkında ayrıntılı bilgi sahibi olmakta ve buna dayanarak kişiselleştirilmiş beslenme programınızı planlamaktayız.',

    'MİSYONUMUZ' => 'MİSYONUMUZ',
    'MİSYONUMUZ_1' => 'Mikrobiyom bilimini ilerletmek ve insanlara faydalı kılmaktır. Mikrobiyoma uygun kişiselleştirilmiş beslenme programları ile insanların yaşam kalitesini arttırmaktır.',

    'VİZYONUMUZ' => 'VİZYONUMUZ',
    'VİZYONUMUZ_1' => 'Yeni nesil teknolojileri hedef alarak kişiselleştirilmiş sağlık teknolojileri üretmektir. Amacımız insan sağlığı gelişimini sağlık bilimleri etiği ilkelerine öncelik vererek bir çözüm haline getirmek ve kitlelere sunmaktır.',

    'degerlerimiz' => 'DEĞERLERİMİZ',
    'degerlerimiz_1' => 'Sağlık etiği ilke ve değerlerine bağlı kalmak, bilime ve yeni teknolojilere bağlılık.',

    //EKİBİMİZ
    'EKİBİMİZ' => 'EKİBİMİZ',

    'ekip_1' => 'AI-SOLT YÖNETİM KURULU BAŞKANI',
    'ekip_2' => 'AI-SOLT KURUCU ORTAĞI',
    'ekip_3' => 'AKADEMİSYEN - BİYOİNFORMATİK UZMANI',
    'ekip_4' => 'AKADEMİSYEN - MOLEKÜLER MİKROBİYOLOJİ UZMANI',
    'ekip_5' => 'ÇÖZÜM ORTAĞI',

    // Bilim page

    'bilim_1' => 'Weizmann Fen Bilimleri Enstitüsü, 800 sağlıklı gönüllü birey üzerinde yaptıkları bir çalışmada (Cell,19 Kasım 2015) farklı insanların aynı gıdalara farklı kan şekeri ile tepki verdiğini gözlemlemiştir. Çalışma, daha sonra katılan 100 katılımcıda da başarılı sonuçlanmıştır. Kişilerin fiziksel aktiviteleri, uyku düzenleri, ilaç kullanma durumları, antropometrik verileri toplanmış ve daha sonra dışkı analizleri mikrobiyom testi ile analiz edilmiştir. Geliştirilen yapay zeka algoritmaları sayesinde, bireylerin besinlere verecekleri kan şekeri tepkileri başarılı bir şekilde öngörülmüştür. Yapılan çalışmada, genel diyet önerilerinin bireylerde sınırlı yarar sağladığı belirtilmiştir.',
    'bilim_2' => 'Her insanın mikrobiyomu kendine özgüdür. Bakterilerin karakteristik özellikleri genlerinde kodlanmıştır. Amacımız, vücudumuzda gerçekleşen fonksiyonları üstlenen oyuncuları iyi yönde değiştirmek ve üstlendikleri görev sayısını arttırmaktır. Bizler gelişmiş moleküler genetik analizler kullanılarak, bağırsak mikrobiyomunu oluşturan bakterileri saptamaktayız. Yaptığımız işlem bağırsaktaki tüm bakterileri genetik yapısına göre sınıflandırıp popülasyon içerisinde her sınıftan ne kadar bakteri olduğunu analiz etmektir. Yeterli veritabanına sahip olması, bütün bakterilerde bulunması, fonksiyonel olarak korunması, üzerinde iyi korunmuş ve heterojen bölgeler içermesi gibi avantajlarından dolayı dışkı örneğinde, DNA sıralama teknolojilerinden 16S rRNA-nükleotit-dizileme yöntemini kullanmaktayız.',

    'bilim_row_1_1' => 'KİŞİSELLEŞTİRİLMİŞ SAĞLIK',
    'bilim_row_1_2' => 'Sağlık, vücudunuzun yiyeceği nasıl işlediği ile başlar. Bağırsak mikrobiyomu sağlığınızla doğrudan ilişkilidir.',

    'bilim_row_2_1' => 'KİŞİSELLEŞTİRİLMİŞ DİYET',
    'bilim_row_2_2' => 'Aynı yemekleri tüketen farklı insanlar arasında, kan şekeri seviyelerindeki önemli farklılıklar, insan sağlığı konusunda neden kişiselleştirilmiş beslenme tercihlerinin evrensel diyet tavsiyelerinden daha olası olduğunu vurgular.',

    'bilim_row_3_1' => '16S rRNA-NÜKLEOTİT DİZİMİ',
	'bilim_row_3_2' => 'Bağırsak mikrobiyom kompozisyon ve çeşitliliğini genetik seviyede belirlemek için yüksek çıktılı 16S rRNA-nükleotit dizileme yöntemleri kullanıyoruz. ',

    'bilim_row_4_1' => 'YAPAY ZEKA ALGORİTMALARI',
    'bilim_row_4_2' => 'Dikkatle seçilmiş yüksek kaliteli hakemli bilimsel literatür, bilim ekibimizden gelen uzman bilgiler, biyolojik testler ve danışan geri bildirimleri ile yapay zeka algoritmalarımızı eğitiyoruz.',

    'bilim_row_5_1' => 'ERCİYES ÜNİVERSİTESİ GENOM VE KÖK HÜCRE MERKEZİ',
    'bilim_row_5_2' => 'Bilimin ışığında, güncel çalışmalara dayanarak bağırsak mikrobiyomu, Erciyes Üniversitesi Genom ve Kök Hücre Merkezi Laboratuvarı’nda %100 yerli bilim insanlarımız eşliğinde analiz ediliyor. ',

    'bilim_row_6_1' => 'BİLİM EKİBİ',
    'bilim_row_6_2' => 'Avusturalya’da 7 yıl bu alanda çalışan mikrobiyoloji uzmanı Doç. Dr. Aycan Gündoğdu ve 10 yıl Amerika’da bu alanda çalışan Biyoinformatik Mühendisi Dr. Özkan Ufuk Nalbantoğlu edinmiş oldukları bilimsel ve teknolojik tecrübeyi ülkemize getirmiştir. ',


    //enbiosis TEST page

    'test_1' => '<b>Türkiye’de kurulan kişiselleştirilmiş algoritmik diyet tabanlı bir şirkettir. Enbiosis ile yapay zeka algoritmalarını machine learning ve deep learning ile eğitiyor ve gelişmiş istatistiksel teknikleri de kullanarak numunendeki mikroorganizmaları son teknoloji dizileme yöntemi ile sıralıyoruz.</b><br/><br/>Bu test vücudunun en yüksek performansta çalışmasını sağlayan kişiselleştirilmiş beslenme önerileri ile mikrobiyomunda bulunan mikroorganizmaların aktivitesini analiz ediyor. Enbiosis, ilgilendiğiniz belirli alanlarla ilgili bağırsak aktivitesini belirlemeye yardımcı olur. Sana; sindirim sistemi etkinliği, uyku kalitesi, bağırsak hareketliliği, mikrobiyom yaşı, bağışıklık sistemi ve antibiyotik hasarı gibi birçok başlık hakkında durum raporları veriyoruz. Tüm test sonuçların ve kişiselleştirilmiş beslenme önerilerin ayrıntılı açıklamalarıyla birlikte mobil uygulama üzerinden sana özel bir ağ yardımıyla iletiliyor. Enbiosis ile hedeflediğimiz nokta, sağlıklı bir bağırsak ile kişiselleştirilmiş beslenme programındır.',

    'test_2_1' => '<span>KİŞİSELLEŞTİRİLMİŞ MİKROBİYOM<br/>SIRALAMA RAPORUN</span>',
    'test_2_2' => '<span>SENİN İÇİN EN UYGUN BESİNLER</span>',
    'test_2_3' => '<span>SENİN İÇİN EN UYGUN ATIŞTIRMALIKLAR</span>',
    'test_2_4' => '<span>KİŞİSELLEŞTİRİLMİŞ BESİN SKORLARIN</span>',
    'test_2_5' => '<span>KİŞİSELLEŞTİRİLMİŞ SAĞLIĞIN</span>',
    'test_2_6' => '<span>ENBİOSİS DİYETİSYENİN TARAFINDAN HAFTALIK<br/>GERİ BİLDİRİM</span>',

    // SSS page

    's-s-s' => 'S S S',

    's_title' => 'Sıkça Sorulan Sorular',
    's_dec' => 'en-Biosis hakkında tüm merak ettikleriniz...',

    's_1_1' => 'Mikrobiyota ve mikrobiyom kavramları nedir?',
    's_1_2' => 'Vücudumuzun birçok noktasına yayılmış ve var olduğumuz andan itibaren bize eşlik eden, yediğimizi, içtiğimizi, stresimizi, gülüşümüzü, hastalığımızı paylaştığımız trilyonlarca bakteri, mantar, virüs gibi mikroskobik minik canlılara sahibiz ve bunların oluşturduğu topluluğa ‘mikrobiyota’ diyoruz. Bu minik canlıların var olduğu habitatı, genomları ve içerisinde bulundukları çevreyle etkileşim koşullarını topluca ‘mikrobiyom’ şeklinde nitelendiriyoruz.',

    's_2_1' => 'Neden bazı diyetler bazı insanlara iyi gelirken bazılarına iyi gelmiyor?',
    's_2_2' => 'Biz yanlış soruyu sorduğumuzu yaptığımız çalışmalarla gözlemledik. Olay az karbonhidratlı, az yağlı, şekerden uzak bir hayat kadar yüzeysel değil. Beslenme ile ilgili genellemelerle herkese istediğimiz oranda ulaşamayışımız bu sebeptendir. Doğru soru ‘Bana en uygun diyet hangisidir’. Cevabı da bağırsakta.',

    's_3_1' => 'Mikrobiyom analizi ile ne yapıyorsunuz? Neden gerekli? ',
    's_3_2' => 'Mikrobiyom analiziniz bize yiyeceklere olan tepkinizi göstermektedir. Her insanın aynı yiyeceğe olan tepkisi farklıdır. Kan şekerinizi dengelemek, sizin için sağlıklı kişiselleştirilmiş bir diyet listesini tam anlamıyla oluşturabilmek için bu analiz gereklidir.',

    's_4_1' => 'Bu testi kimler yaptırmalı? ',
    's_4_2' => 'Büyük bir popülasyon söz konusu.  16 yaş üzeri herkes bu testi yaptırabilir. Prediyabet aşamasında olanlar, herhangi bir akut-kronik rahatsızlığı olanlar, kilo problemi yaşayanlar, uyku problemi yaşayanlar, otoimmün rahatsızlığı olanlar veya sağlıklı olup da olası ihtimallere karşı iç dünyaları hakkında ayrıntılı bilgi sahibi olmak isteyenler yaptırabilir.',

    's_5_1' => 'Çözüm nedir?',
    's_5_2' => 'Bizler kurduğumuz bu sistemde, yaptığımız analizler sonucu yapay zeka tabanlı kişiselleştirilmiş beslenme önerileri oluşturuyor ve spesifik olarak bireyin en doğru şeklide beslenmesini sağlıyoruz. Spesifik mikroorganizmalara sağladığımız spesifik besinlerle bireylerin problemlerini çözebiliyoruz.',

    's_6_1' => 'Sistem nasıl çalışıyor?',
    's_6_2' => 'Öncelikle kullanıcılar mobil uygulamayı/web indirip abone olmaktadır. Bu kişisel program, bireyin sağlık geçmişini ve genel diyet alışkanlıklarını sorduktan sonra başlamaktadır. Kullanıcılara verilen numune toplama kitiyle alınan dışkı örnekleri, Erciyes Üniversitesi Genom ve Kök Hücre Merkezine gönderilmektedir. Bu laboratuvar Türkiye’nin alanında ilk ve en büyük genom ve kök hücre araştırma merkezidir. Burada numunenin mikrobiyal DNA\'sı analiz edilerek, kişisel mikrobiyom profilleri elde edilmekte ve raporlar kullanıcı ile  özel bir gizlilikle paylaşılmaktadır. Binlerce adet besinden hangisinin sana ne kadar uygun olduğu skorlama ile uygulama üzerinden gösterilmektedir. Bireyin vücut kitle indeksini sağlıklı bir şekilde düzenleyen bu program, kişinin mikrobiyom profiline göre diyet öneri algoritmaları ile üretilmektedir.',

    's_7_1' => 'Kiti nasıl temin edebiliriz? Numuneyi nasıl hazırlayacağız?',
    's_7_2' => 'Enbiosis mobil aplikasyonu/web indirin ve kolayca sipariş edin. Numunenizi istediğiniz zaman evde iki dakikadan az bir sürede, kutu üzerinde gösterilen adımları izleyerek alabilirsiniz. Kitiniz ihtiyacınız olan her şeyi içerir.  Daha sonra numunenizi ekteki ön ödemeli zarftaki adresimize geri gönderin.',

    's_8_1' => 'Numuneyi hazırlarken herhangi bir yerden mikrop bulaşma ihtimali var mı? Gerçekten oda sıcaklığında gönderebilir miyim? Bazı bakteriler büyümeye başlar mı? Mikrobiyom profilim nakliye sürecinde değişecek mi?',
    's_8_2' => 'Endişelenme. Numune alma tüpünüz, temasta mikroorganizmalarını öldüren ve açığa çıkan mikrobiyal DNA’yı parçalamaya karşı koruyan DNA stabilize edici sıvı içerir. Herhangi bir olası bulaşıcı ajan postalamadan önce ölmüştür. Sıvı, numunenizi stabilize etmek ve korumak için tasarlanmış özel bir bileşendir. Numunenizi swabdan örnekleme tüpüne aktardığınız anda, içindeki mikroorganizmaların büyümesi durur. Bu çok önemlidir, çünkü numunenizi topladığınız anda neye benzediğini tam olarak bilmek isteriz. Mikroorganizmalarınızın büyümeye devam etmesine izin verilirse, tüpün içeriği laboratuvarımıza gelene kadar çok farklı olacaktır. Nakliye sürecinde herhangi bir koşuldan etkilenmeden laboratuvarımıza gelmektedir.',

    's_9_1' => 'Bu testi ne sıklıkla yaptırmam gerekiyor?',
    's_9_2' => '6 hafta mikrobiyomun zararlı aktivitelerinin bastırılması ve yararlıların artışında, mikrobiyomunuza ve genel sağlığınıza daha kalıcı bir etki sağlamak için yeterli bir zaman sunar. Sizler kendinizin en temel ölçütüsünüz. Fonksiyonel aktif mikrobiyom değişikliklerinizi takip etmek için yılda birkaç kez veya gerektiği sıklıkta tekrar test etmenizi öneririz.',

    's_10_1' => 'Periyodik analizler dışında testi tekrarlamam gereken durumlar var mı?',
    's_10_2' => 'Son testinizden bu yana altı aydan uzun bir süre geçtiyse, antibiyotik tedavisi aldıysanız, herhangi bir sağlık merkezinde 48 saatten fazla yatarak tedavi gördüyseniz ya da herhangi bir operasyon geçirdiyseniz, ülke dışına seyahat ettiyseniz, çok fazla stres altındaysanız bu testi tekrar etmelisiniz.',

    's_11_1' => 'Mikrobiyom değişir mi? Her insanın mikrobiyomu aynı mıdır?',
    's_11_2' => 'Her insanın mikrobiyomu kendine özgüdür. Bakterilerin karakteristik özellikleri genlerinde kodlanmıştır. Bu bakteriler temelde aynı gibi gözükse de zamanla yaptıkları görevleri farklı oyunculara devrederler. Mikrobiyom koru değişmez, çevresi kişiye ve yaşam tarzına göre değişiklikler gösterir.  Kor dediğimiz olay fonksiyon olayıdır. Vücudumuzda bu fonksiyon sürekli gerçekleşmektedir. Bizim asıl amacımız bu fonksiyonu üstlenen oyuncuları iyi yönde değiştirmektir. Bir yerine birden fazla görevi üstlenen iyi oyuncuları yerleştirmektir.',

    's_12_1' => 'Mikrobiyom ne gibi görevler üstlenmiştir? Neden bu kadar önemli?',
    's_12_2' => 'Homo sapiens, binlerce yıldır vücudundaki dost mikroorganizmalarla birlikte ve etkileşim halinde evrimleşmektedir. Özellikle bağırsakta yaşayan mikroorganizmaların ürettikleri metabolitler ve hatta genetik materyaller insan sistemik dolaşımına katılmakta, epigenomik mekanizmalarla inaktif genlerin aktive olmasına, böylelikle sağlığın sürdürülebilir olmasına, birtakım hastalıkların oluşmamasına ve evrime neden olmaktadır. Bağırsak mikrobiyotası, sağlıklı bir durumda, metabolizmayı etkileyerek, bağışıklık sistemini modüle ederek ve patojenlere karşı koruyarak konakçıya önemli işlevler sağlayan karmaşık bir ekosistemdir. Sağlıklı ve uzun yaşamın anahtarı sağlıklı bir bağırsak yapısıdır. O nedenle kişisel bağırsak mikrobiyomunun içeriğinin bilinmesinde sayısız yarar vardır.',

    's_13_1' => 'Hangi analiz yöntemini kullanıyorsunuz?',
    's_13_2' => 'Bu çalışmalarda, yüksek çıktılı 16S_rRNA-nükleotid-dizimi kullanılmaktadır. Yeni (metagenomik) teknikler sayesinde, minicanlılar dünyasına bakış açımız hepten değişmiştir. Vücudumuzda kendi hücrelerimiz kadar mikrobiyal hücre taşıyoruz. Bizler gelişmiş moleküler genetik analizler kullanılarak, bağırsak mikrobiyomunu oluşturan bakterileri saptamaktayız. İsmini, işlevini bilmediğimiz bakteriler dahil işlem yapabilmekteyiz. Asıl yaptığımız işlem bağırsaktaki tüm bakterileri genetik yapısına göre sınıflandırıp popülasyon içerisinde her sınıftan ne kadar bakteri olduğunu analiz etmektir. Dışkı örneğinde, son teknoloji DNA sekanslama yöntemi olan 16S_rRNA dizileme yöntemiyle çok rahat bir şekilde bu analize erişebilmekteyiz.',

    's_14_1' => 'Benim verilerim yurtdışına gidecek mi? Sistem güvenli mi?',
    's_14_2' => 'Burada bizim bilim insanlarımızca üretilen %100 yerli, %100 bilimsel ve %100 inovatif bir teknolojinin son kullanıcıya yönelik kullanımı üzerinde muazzam bir projede çalışıyoruz. Numunenin mikrobiyal DNA\'sı Erciyes Üniversitesi Genom ve Kök Hücre Merkezi’nde analiz edilerek, kişisel mikrobiyom profilleri elde edilmekte ve raporlar kullanıcı ile güvenli ve sadece kendilerine özgü bir ağ yardımıyla mobil uygulama üzerinden paylaşılmaktadır.',

    's_15_1' => 'Yapay zeka sisteme nasıl bir katkı sağlıyor?',
    's_15_2' => 'Yapay zeka sayesinde, sizin için hangi gıdaların ideal olduğunu keşfedebiliyoruz. Böylece optimal sağlığı hedefliyoruz. Yapay zeka motorumuz, geniş bilgi veritabanızmızdaki verilerin çoklu analizini gerçekleştirir. Dikkatle seçilmiş yüksek kaliteli hakemli bilimsel literatür, bilim ekibimizden gelen uzman bilgiler, biyolojik testler ve danışan geri bildirimleri yapay zeka makinemizi eğitmektedir. Yapay zeka teknolojisi ile insan aklının analiz edemeyeceği çoklukta verileri çok daha hızlı ve net bir şeklide elde ediyoruz.  Yapay zeka tıpta ve birçok alanda yararlı olduğunu kanıtlamış ve günümüz teknolojisinde bir çığır açmıştır. Yapay zeka, insanların mantıklı düşünebilme, öğrenebilme ve öğrendiklerini yeni durumlara uygulama kabiliyetlerini makinelere aktarmaktadır.  Araştırmacıların zorlu görevlerini daha hızlı gerçekleştirmelerine yardımcı olarak tıbbi araştırmayı hızlandırmaktadır. Yapay zeka, çağımızın en büyük problemi olan ve hiçbir şeklide tedarik edemediğimiz ‘zaman’ kavramına bir çözümdür. Hasta Güvenliği Dergisi’nde yapılan bir araştırmaya göre yılda 400.000’den fazla kişi hastanelerde erken ölmektedir. Yapay zeka ile gelişen teknoloji ve sağladığı zaman tasarrufu sayesinde erken müdahale ile daha çok insan hayatı kurtarma söz konusudur. Algoritma hastaları sürekli olarak izleyebilmektedir. Bu yüzden geliştirilen algoritmalar oldukça yararlı ve pratiktir.',

    's_16_1' => 'Bu sistemde diyetisyene gerek var mı?',
    's_16_2' => 'Dünya genelinde yapılan çalışmalarla toparlanmış çok kıymetli verilerden uzmanlaşmış bir robot oluşturduk. Bünyesinde binlerce bakteri ve binlerce besin verisi, binlerce biriken tecrübe ile oluşturulmuş. Meslekleri küçük işlerden kurtarıp sonuca ve kontrole daha hızlı götüren bir sistem kazandırmak istiyoruz. Taslak olarak yaptığımız işte son ürünü ortaya çıkaran oyunculara bakıyor ve doğru ham maddeyi veriyoruz. Bununla beraber tüm olayı tabii ki sadece makineye bırakmıyor, Enbiosis diyetisyenlerimizle sürekli olarak online ve yüz yüze iletişimde kalıyorsunuz. Ayrıntılı raporunuzun yanında Enbiosis diyetisyenlerimizin size vermiş olduğu kişiselleştirilmiş diyet listelerinizle gününüzü daha kolay ve dengeli planlıyorsunuz. Kompozisyon, porsiyon, denge, çeşitlilik, öneri, yeterlilik, güvenirlik, öğün saatleri gibi konularda Enbiosis diyetisyeniniz tarafından yönlendirileceksiniz.  Raporlama aşamasından sonra takibiniz sürekli mobil uygulama ve yüz yüze olacak şekilde yapılacak ve geri bildirimler haftalık olarak sunulacaktır. Bu süreçte aynı zamanda psikolojik olarak motivasyon desteği de mevcuttur.',

    's_17_1' => 'Hangi gün dışkı örneği aldığım önemli mi? Son zamanlarda diyetlerimde büyük değişiklikler yapmadım, ancak her gün farklı bir şeyler yiyorum. Mikrobiyomum bir günden diğer güne çok  değişiyor mu? Örnek almak için en iyi gün var mı ? ',
    's_17_2' => 'Hangi günde numune aldığın veya günlük diyetinde küçük değişiklikler olması önemli değildir; büyük diyet değişiklikleri yapılmadığı sürece bir kişinin mikrobiyomunda majör değişiklikler olmadığını biliyoruz. Bununla birlikte, diğer çalışmalar, büyük diyet değişiklikleri yaparsanız , sağlığınızda değişiklikler olursa veya uluslararası seyahatlere katılırsanız , mikrobiyomunuzun değişeceğini göstermiştir . Bu gibi durumlarda mikrobiyomun nasıl değişeceğini görmek için çoklu testler yapmanız yararlı olacaktır.',

    's_18_1' => 'Gönderdiğiniz kit tuvalet kağıdından örnek almamı gerektiriyor. Bu tuvalet kağıdından hangi dışkı parçasını aldığım önemli mi? Aynı tuvalet kağıdından 2 numune alıp test için gönderirsem, bu 2 numunenin mikrobiyom profilleri aynı görünür mü? ',
    's_18_2' => 'Evet, aynı tuvalet kağıdından alınan iki örneğin neredeyse aynı sonuçları verdiği tespit edildi',

    's_19_1' => 'Bu işlem ne kadar tekrarlanabilir? Aynı örneği birden çok kez işlerseniz, bu mikrobiyom profilleri nasıl görünecektir? Aynı örneği üç kez işleseniz ne olur? Aynı sonuçları alıyor musunuz ya da bu üç kopya her işlediğinizde çok farklı görünüyor mu? ',
    's_19_2' => 'Aynı kişiden alınan üç kopya numunenin hepsi, kullanılan reaktiflerden veya ekipmandan ve laboratuvarda testi yapan bireylerden bağımsız olarak birbirine çok benzerdir. Her insanın kendine özgü mikrobiyomu olduğundan, bu örnekleri açıkça ayırt edebiliriz. Ölçüt sizsiniz. Yukarda saydığımız koşullar çerçevesinde yılda birkaç defa testi tekrarlayıp mikroorganizma fonksiyonlarınızın kontrolünü önermekteyiz.',

    's_20_1' => 'Aynı numuneyi yüzlerce kez işleseniz ne olur? Sonuçların hepsi hala aynı mı görünür?',
    's_20_2' => '300 defadan fazla işleme tabi tutulsalar bile birbirlerine çok benzeyeceklerdir.',


    //iletisim page

    'iletisim_bilgiler' => 'İletişim Bilgilerimiz',
    'iletisim_tel' => 'Telefon',
    'iletisim_mail' => 'E-posta',

    'iletisim_gec' => 'İLETİŞİME GEÇİN',
    'il_ad_soy' => 'Adınız ve Soyadınız',
    'il_email' => 'E-Posta Adresiniz',
    'il_mesaj ' => 'Mesaj Başlığı',
    'il_me' => 'Mesajınız',
    'il_gonder' => 'SİPARİŞİ TAMAMLA',


    //product page

    'EnBiosis_Product' => 'Enbiosis Ürün',
    'EnBiosis_Products' => 'Enbiosis Ürünler',
    'sepete_ekle' => 'Sepete Ekle',
    'price' => 'Fiyat',

    // cart page

    'Shopping_Cart' => 'Benim Sepetim',
    'Product' => 'Ürün',
    'Quantity' => 'Adet',
    'Subtotal' => 'Ara Toplam',
    'Continue_Shopping' => 'Alışverişe devam et',
    'Total' => 'Toplam',
    'Check_Out' => 'Alışverişi Tamamla',

    //Checkout Page

    'Billing_address' => 'Fatura Adresi',
    'Name' => 'Adı ve soyadı',
    'Email' => 'E-postanız',
    'Payment' => 'Ödeme Bilgileri',
    'Name_on_card' => 'Kart Üzerindeki İsim',
    'Name_on_card_desc' => 'Kartta Göründüğü Gibi',
    'Credit_card_number' => 'Kart Numarası',
    'Expiration_year' => 'Son Kullanma Yılı',
    'Expiration_Month' => 'Son Kullanma Tarihi',


    //odem
    'odeme' => 'Ödeme',
    'odeme_hata' => 'Bir hata oluştu.',
    'odem_hata_des' => 'Ödeme işlemi gerçekleştirilemedi.Lütfen tekrar deneyiniz.',
    'Tamamlandı_des' => 'Ödeme işlemi başarıyla tamamlandı. Biraz sonra size bir e-posta gönderilecektir.',
    'Tamamlandı' => ' Ödeme işlemi tamamlandı.',
    'kupon' => 'kupon',
    'submit' => 'SORGULA',
    'Errors' => 'hatalar',
    'coupon_hata' => 'kupon kodu Eşleşmedi',
    'coupon_hata_des' => 'Girdiğiniz kupon kodu kayıtlarımızda bulunmamaktadır.',
    'tebrikler' => 'tebrikler',
    'tebrikler_des' => 'Girdiğiniz kupon kodu size özel :number % indirim sağlar.',
    'indirim' => 'indirim',
    'sil_kopun' => 'Kuponu Kaldır',
    'coupon_sil' => 'Kupon Kaldırıldı',
    'coupon_sil_des' => 'Ödeme formundaki kupon kodu başarıyla kaldırıldı.',
    'coupon_hata_eslesme' => 'Kupon kodu eşleşirken hata oluştu',
    'coupon_hata_eslesme_des' => 'Kupon indirim fiyati değiştirilmiştir.Bir hata olduğunu düşünüyorsanız lütfen bizimle iletişime geçiniz.',
    'Close' => 'kapat',
    'de_code' => 'Diyetisyen İsmi (Opsiyonel)',
	'de_evet' => 'Evet',
	'de_hayir' => 'Hayır',
    'menu' => 'MENÜ',
	'change_quantity' => "Değiştir",

    'cart_delete' => 'Ürün Silindi',
    'cart_delete_des' => 'Ürün Sepetten başarıyla Silindi',

    'cart_update' => 'Ürün Güncellendi',
    'cart_update_des' => 'Sepetteki Ürün Miktarı başarıyla güncellendi',

    'cart_created' => 'Ürün Eklendi',
    'cart_created_des' => 'Ürün sepete başarıyla eklendi',

    //register-kit
    'register-kit' => 'kit kaydı',
    'kit_code' => 'Kit Kodunuz',
    'kit_hata' => 'Kit Code Eşleşmedi',
    'kit_hata_des' => 'Girdiğiniz kod kayıtlarımızda bulunmamaktadır.',
    'genel_hata' => 'hata olustu lutfen yeniden deneyiniz',
    'address' => 'Adres',
    'üye_mi' => 'Üye misin? Giriş yap',
    'cinsiyet' => 'Cinsiyet',
    'lütfen_seçin' => 'Lütfen Seçin',
    'erkek' => 'Erkek',
    'kadin' => 'Kadın',
    'belirtilmemiş' => 'Belirtilmemiş',
    'odeme_sekli' => 'Ödeme Şekli',
    'tek_cekim' => 'Tek Çekim',
    '2_taksit' => '2 Taksit',
    '3_taksit' => '3 Taksit',
    'taksit' => 'Taksit'


];
