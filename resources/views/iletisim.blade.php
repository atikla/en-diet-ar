@extends('layouts.public')
@section('subpageMenu','topmenu--subpage')
@section('footerContact','footer--contact')
@section('pageTitle', __('pages/iletisim.page_title'))
@section('pageDescription', __('pages/iletisim.page_description'))

@section('content')
<div id="new__loading" class=''>
	<div id="new__loading-logo"class=''></div>
</div>
<section class="subpage-header subpage-header--contact">
	<div class="container">
		<div class="row">
			<div class="subpage-header__content">
				<h1 class="subpage-header__title">{{__('pages/iletisim.header_title')}}</h1>
				<div class="subpage-header__seperator"></div>
				<div class="subpage-header__breadcrumb">
					<nav aria-label="breadcrumb">
						<ol itemscope itemtype="https://schema.org/BreadcrumbList"
						class="breadcrumb no-bg-color text-light">
							<li itemprop="itemListElement" itemscope
							itemtype="https://schema.org/ListItem"
							class="breadcrumb-item">
								<a itemprop="item"
								style="color:#007bff"
								href="{{ route('/')}}">
								<span itemprop="name">{{__('pages/iletisim.breadcrumb.0')}}</span>
								</a>
								<meta itemprop="position" content="1"/>
							</li>
							<li itemprop="itemListElement" itemscope
							itemtype="https://schema.org/ListItem"
							class="breadcrumb-item active te">
								<a itemprop="item" 
								style="color:#6c757d"
								href="{{ route('iletisim')}}">
									<span itemprop="name">{{__('pages/iletisim.breadcrumb.1')}}</span>
								</a>
								<meta itemprop="position" content="2"/>
							</li>
						</ol>
					</nav>
				</div>
			</div>
		</div>
	</div>
</section>
@if (app()->getLocale() == 'tr')
<section class="contact-info">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
				<img src="{{ url('/') }}/new/img/contact-info-img.webp" class="contact-info__img"/>
			</div>
			<div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 contact-info__right">
				<h2 class="contact-info__title">{{__('pages/iletisim.main_title')}}</h2>
				<p class="contact-info__desc">{{__('pages/iletisim.main_desc')}}</p>
				<ul>
					<li>
						<img src="{{ url('/') }}/new/img/location-icon.webp" />
						<span>
							<b>{{__('pages/iletisim.turkiye_ofisi')}}:</b> {!! __('pages/iletisim.turkiye_ofisi_desc')!!}<br/>
							<b>{{__('pages/iletisim.uae_dubai_ofisi')}}:</b> {{__('pages/iletisim.uae_dubai_ofisi_desc')}}
						</span>
					</li>
					<li>
						<img src="{{ url('/') }}/new/img/phone-icon.webp" />
						<span>
							<a 	href="tel:+90 850 840 77 93"
								style="color:#696969"
								onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'phone'});">
							+90 (850) 840 77 93
							</a> 
						</span>
					</li>
					<li>
						<img src="{{ url('/') }}/new/img/email-icon.webp" />
							<span>
								<a 	href="mailto:destek@enbiosis.com" 
									style="color:#696969"
									onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'mail'})">
									destek@enbiosis.com
								</a>
								<br>
								<a 	href="mailto:info@enbiosis.com"
									style="color:#696969"
									onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'mail'})">
									info@enbiosis.com
								</a>
							</span>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>
@endif
<section class="contact-form {{ app()->getLocale() != 'tr' ? ' mt-0' : '' }}">
	<h2 class="contact-form__title">{{__('pages/iletisim.bize_ulasin')}}</h2>
	<div class="container">
		{!! Form::open(['method'=>'POST', 'action' => 'GeneralController@sendMail', 'class' => 'row']) !!}
		<div class="form-group col-md-6 contact-form__input">
			
			{!! Form::text('name', null, ['class'=>'form-control', 'id' => 'form_adsoyad', 'placeholder' => __('pages/iletisim.adiniz_ve_soyadiniz')]) !!}
			@error('name')
				<small id="emailHelp" class="form-text text-muted">{{$message}}</small>
			@enderror
		
		</div>

		<div class="form-group col-md-6 contact-form__input">
			
			{!! Form::text('email', null, ['class'=>'form-control', 'id' => 'form_email', 'aria-describedby' => 'emailHelp', 'placeholder' => __('pages/iletisim.e_posta_adresiniz')]) !!}
			@error('email')
				<small id="emailHelp" class="form-text text-danger">{{$message}}</small>
			@enderror
		</div>

		<div class="form-group col-md-12 contact-form__input">

			{!! Form::text('subject', null, ['class'=>'form-control', 'id' => 'form_baslik', 'aria-describedby' => 'emailHelp', 'placeholder' => __('pages/iletisim.mesaj_basligi')]) !!}
			@error('subject')
				<small id="emailHelp" class="form-text text-danger">{{$message}}</small>
			@enderror
		</div>

		<div class="form-group col-md-12 buy__input">

			<textarea name="mesaj" class="form-control" id="form_mesaj" placeholder="{{__('pages/iletisim.mesaj')}}">{{old('mesaj')}}</textarea>
			@error('mesaj')
				<small id="emailHelp" class="form-text text-danger">{{$message}}</small>
			@enderror
		</div>

		<div class="col-md-12 d-flex justify-content-center">

			{!! Form::submit(__('pages/iletisim.gonder'), ['class'=>'link-btn link-btn--orange form-button form-send']) !!}
		
		</div>
		
		{!! Form::close() !!} 
	</div>
</section>
@endsection
@section('scripts')

<script>
	$('form').submit(function (){
		$('#new__loading').addClass("loading");
		$('#new__loading-logo').addClass("loading-logo");
	})
</script>
	
@endsection