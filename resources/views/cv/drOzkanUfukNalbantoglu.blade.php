@extends('layouts.public')
@section('subpageMenu','topmenu--subpage')

@section('pageTitle',__('pages/cv/ozkanUfuk.page_title'))
@section('pageDescription',__('pages/cv/ozkanUfuk.page_description'))

@section('styles')
<!-- font-awesome -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" integrity="sha256-+N4/V/SbAFiW1MPBCXnfnP9QSN3+Keu+NlB+0ev/YKQ=" crossorigin="anonymous"/>
@endsection
@section('content')
<section class="subpage-header subpage-header--microbiome-analysis">
	<div class="container">
		<div class="row">
			<div class="subpage-header__content">
				<h1 class="subpage-header__title">{{__('pages/cv/ozkanUfuk.header_title')}}</h1>
				<div class="subpage-header__seperator"></div>
				<div class="subpage-header__breadcrumb">
                    <nav aria-label="breadcrumb">
						<ol class="breadcrumb no-bg-color text-light">
                          <li class="breadcrumb-item"><a href="{{ route('/')}}">{{__('pages/cv/ozkanUfuk.breadcrumb.0')}}</a></li>
                          <li class="breadcrumb-item"><a href="{{ route('/')}}">{{__('pages/cv/ozkanUfuk.breadcrumb.1')}}</a></li>
						  <li class="breadcrumb-item active" aria-current="page">{{__('pages/cv/ozkanUfuk.breadcrumb.2')}}</li>
						</ol>
					</nav>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="mt-3">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="col-md-12 microbiome__col">
                <div>
                    <span><i>{{__('pages/cv/ozkanUfuk.name')}}</i></span>
                    <img class="m-4" style="float: right;height: 310px;" src="{{ url('/') }}/new/img/ozkan-ufuk-nalbantoglu.webp"/>
                    <p>{{__('pages/cv/ozkanUfuk.desc.0')}}</p>
                    <p>{{__('pages/cv/ozkanUfuk.desc.1')}}</p>
                    <p>{{__('pages/cv/ozkanUfuk.desc.2')}}</p>
                    <p>{{__('pages/cv/ozkanUfuk.interest')}}
                        <i class="fa fa-tag mx-2"></i>{{__('pages/cv/ozkanUfuk.interests.0')}}
                        <i class="fa fa-tag mx-2"></i>{{__('pages/cv/ozkanUfuk.interests.1')}}
                        <i class="fa fa-tag mx-2"></i>{{__('pages/cv/ozkanUfuk.interests.2')}}
                        <i class="fa fa-tag mx-2"></i>{{__('pages/cv/ozkanUfuk.interests.3')}}
                    </p>
                    <div class="mt-3 break">
                        <span><i>{{__('pages/cv/ozkanUfuk.articles')}}</i></span>
                        <div class="mt-3">
                            <a class="pt-2" style="display: inline-block" target="_blank" href="https://www.ncbi.nlm.nih.gov/pubmed/24750812">
                                https://www.ncbi.nlm.nih.gov/pubmed/24750812
                            </a>
                        </div>
                        <div class="mt-3">
                            <a class="pt-2" style="display: inline-block" target="_blank" href="https://ieeexplore.ieee.org/abstract/document/8350295">
                                https://ieeexplore.ieee.org/abstract/document/8350295
                            </a>
                        </div>
                        <div class="mt-3">
                            <a class="pt-2" style="display: inline-block" target="_blank" href="https://www.ncbi.nlm.nih.gov/pubmed/26747442">
                                https://www.ncbi.nlm.nih.gov/pubmed/26747442
                            </a>
                        </div>
                        <div class="mt-3">
                            <a class="pt-2"  style="display: inline-block" target="_blank" href="https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0124906">
                                https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0124906
                            </a>
                        </div>
                        <div class="mt-3">
                            <a class="pt-2" style="display: inline-block" target="_blank" href="https://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-12-41">
                                https://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-12-41
                            </a>
                        </div>
                        <div class="mt-3">
                            <a class="pt-2" style="display: inline-block" target="_blank" href="https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0009007">
                                https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0009007
                            </a>
                        </div>
                        <div class="mt-3">
                            <a class="pt-2" style="display: inline-block" target="_blank" href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2821113/">
                                https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2821113/
                            </a>
                        </div>
                        <div class="mt-3">
                            <a class="pt-2" style="display: inline-block" target="_blank" href="https://link.springer.com/protocol/10.1007/978-1-62703-646-7_1">
                                https://link.springer.com/protocol/10.1007/978-1-62703-646-7_1
                            </a>
                        </div>
                        <div class="mt-3">
                            <a class="pt-2" style="display: inline-block" target="_blank" href="https://www.morganclaypool.com/doi/abs/10.2200/S00360ED1V01Y201105BME04">
                                https://www.morganclaypool.com/doi/abs/10.2200/S00360ED1V01Y201105BME04
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
@endsection
