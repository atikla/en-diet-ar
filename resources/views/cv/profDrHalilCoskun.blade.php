@extends('layouts.public')
@section('subpageMenu','topmenu--subpage')

@section('pageTitle',__('pages/cv/halilCoskun.page_title'))
@section('pageDescription',__('pages/cv/halilCoskun.page_description'))
@section('styles')
<!-- font-awesome -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" integrity="sha256-+N4/V/SbAFiW1MPBCXnfnP9QSN3+Keu+NlB+0ev/YKQ=" crossorigin="anonymous"/>
@endsection

@section('content')
<section class="subpage-header subpage-header--microbiome-analysis">
	<div class="container">
		<div class="row">
			<div class="subpage-header__content">
				<h1 class="subpage-header__title">{{__('pages/cv/halilCoskun.header_title')}}</h1>
				<div class="subpage-header__seperator"></div>
				<div class="subpage-header__breadcrumb">
                    <nav aria-label="breadcrumb">
						<ol class="breadcrumb no-bg-color text-light">
                          <li class="breadcrumb-item"><a href="{{ route('/')}}">{{__('pages/cv/halilCoskun.breadcrumb.0')}}</a></li>
                          <li class="breadcrumb-item"><a href="{{ route('/')}}">{{__('pages/cv/halilCoskun.breadcrumb.1')}}</a></li>
						  <li class="breadcrumb-item active" aria-current="page">{{__('pages/cv/halilCoskun.breadcrumb.2')}}</li>
						</ol>
					</nav>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="mt-3">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="col-md-12 microbiome__col">
                <div>
                    <span><i>{{__('pages/cv/halilCoskun.name')}}</i></span>
                    <img class="m-4" style="float: right;height: 310px;" src="{{ url('/') }}/new/img/halil-coskun.webp"/>
                    <p>{{__('pages/cv/halilCoskun.desc')}}</p>
                    <p>{{__('pages/cv/halilCoskun.interest')}}
                        <i class="fa fa-tag mx-2"></i>{{__('pages/cv/halilCoskun.interests.0')}}
                        <i class="fa fa-tag mx-2"></i>{{__('pages/cv/halilCoskun.interests.1')}}
                        @if (app()->getLocale() != 'tr')
                        <i class="fa fa-tag mx-2"></i>{{__('pages/cv/halilCoskun.interests.2')}}
                        @endif
                    </p>
                    <div class="mt-3 break">
                        <span><i>{{__('pages/cv/halilCoskun.articles')}}</i></span>
                        <div class="mt-3">
                            <a class="pt-2" target="_blank" href="https://link.springer.com/article/10.1007/s00464-016-5215-0">
                                https://link.springer.com/article/10.1007/s00464-016-5215-0
                            </a>
                        </div>
                        <div class="mt-3">
                            <a class="pt-2" target="_blank" href="https://link.springer.com/article/10.1007/s40519-016-0296-2">
                                https://link.springer.com/article/10.1007/s40519-016-0296-2
                            </a>
                        </div>
                        <div class="mt-3">
                            <a class="pt-2" target="_blank" href="https://www.sciencedirect.com/science/article/pii/S0165178115305205">
                                https://www.sciencedirect.com/science/article/pii/S0165178115305205
                            </a>
                        </div>
                        <div class="mt-3">
                            <a class="pt-2" target="_blank" href="https://link.springer.com/article/10.1007/s11695-017-3014-x">
                                https://link.springer.com/article/10.1007/s11695-017-3014-x
                            </a>
                        </div>
                        <div class="mt-3">
                            <a class="pt-2" target="_blank" href="http://lapend.org/jvi.aspx?pdir=less&plng=eng&un=LESS-03522&look4">
                                http://lapend.org/jvi.aspx?pdir=less&plng=eng&un=LESS-03522&look4
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
@endsection
