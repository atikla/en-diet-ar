@extends('layouts.public')
@section('subpageMenu','topmenu--subpage')

@section('pageTitle',__('pages/cv/omerOzkan.page_title'))
@section('pageDescription',__('pages/cv/omerOzkan.page_description'))

@section('styles')
<!-- font-awesome -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" integrity="sha256-+N4/V/SbAFiW1MPBCXnfnP9QSN3+Keu+NlB+0ev/YKQ=" crossorigin="anonymous"/>
@endsection

@section('content')
<section class="subpage-header subpage-header--microbiome-analysis">
	<div class="container">
		<div class="row">
			<div class="subpage-header__content">
				<h1 class="subpage-header__title">{{__('pages/cv/omerOzkan.header_title')}}</h1>
				<div class="subpage-header__seperator"></div>
				<div class="subpage-header__breadcrumb">
                    <ol class="breadcrumb no-bg-color text-light">
                        <li class="breadcrumb-item"><a href="{{ route('/')}}">{{__('pages/cv/omerOzkan.breadcrumb.0')}}</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('/')}}">{{__('pages/cv/omerOzkan.breadcrumb.1')}}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{__('pages/cv/omerOzkan.breadcrumb.2')}}</li>
                      </ol>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="mt-3">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="col-md-12 microbiome__col">
                <div>
                    <span><i>{{__('pages/cv/omerOzkan.name')}}</i></span>
                    <img class="m-4" style="float: right;height: 310px;" src="{{ url('/') }}/new/img/omer-ozkan.webp"/>
                    <p>{{__('pages/cv/omerOzkan.desc.0')}}</p>
                    <p>{{__('pages/cv/omerOzkan.desc.1')}}</p>
                    <p>{{__('pages/cv/omerOzkan.interest')}}
                        <e style="display: inline-block"><i class="fa fa-tag mx-2"></i>{{__('pages/cv/omerOzkan.interests.0')}}</e>
                        <e style="display: inline-block"><i class="fa fa-tag mx-2"></i>{{__('pages/cv/omerOzkan.interests.1')}}</e>
                        <e style="display: inline-block"><i class="fa fa-tag mx-2"></i>{{__('pages/cv/omerOzkan.interests.2')}}</e>
                        <e style="display: inline-block"><i class="fa fa-tag mx-2"></i>{{__('pages/cv/omerOzkan.interests.3')}}</e>
                    </p>
                    <div class="mt-3 break">
                        <span><i>{{__('pages/cv/omerOzkan.articles')}}</i></span>
                        @if (app()->getLocale() == 'tr')
                        <div class="mt-3">
                            <a class="pt-2" target="_blank" href="https://www.capital.com.tr/sirket-panosu/sirket-panosu-haberleri/yapay-zekayla-kisiye-ozel-diyet-listesi-hazirladi">
                                https://www.capital.com.tr/sirket-panosu/sirket-panosu-haberleri/yapay-zekayla-kisiye-ozel-diyet-listesi-hazirladi
                            </a>
                        </div>
                        <div class="mt-3">
                            <a class="pt-2" target="_blank" href="http://www.kobipostasi.net/covide-yapay-zeka-ile-cozum-sonbaharda/">
                                http://www.kobipostasi.net/covide-yapay-zeka-ile-cozum-sonbaharda/
                            </a>
                        </div>
                        <div class="mt-3">
                            <a target="_blank" href="https://uzmanpara.milliyet.com.tr/kap-haberi/basin-bulteni-covide-yapay-zeka-ile-cozum-sonbaharda/1594013/">
                                https://uzmanpara.milliyet.com.tr/kap-haberi/basin-bulteni-covide-yapay-zeka-ile-cozum-sonbaharda/1594013/
                            </a>
                        </div>
                        <div class="mt-3">
                            <a target="_blank" href="https://www.ekonomist.com.tr/girisim-kobi/kisiye-ozel-beslenme-rehberi-hazirladik.html">
                                https://www.ekonomist.com.tr/girisim-kobi/kisiye-ozel-beslenme-rehberi-hazirladik.html
                            </a>
                        </div>
                        @else
                        <div class="mt-3">
                            <a class="pt-2" target="_blank" href="https://medium.com/@enbiosis/biotechnology-export-from-enbiosis-to-europe-dbc796fe866d?source=friends_link&sk=3cdab010fe40b299636755343d199522">
                                https://medium.com/@enbiosis/biotechnology-export-from-enbiosis-to-europe-dbc796fe866d?source=friends_link&sk=3cdab010fe40b299636755343d199522
                            </a>
                        </div>
                        <div class="mt-3">
                            <a class="pt-2" target="_blank" href="https://medium.com/@enbiosis/personalized-treatment-methods-are-now-in-hospitals-d7e152bd984f?source=friends_link&sk=031b1e7b650c3eb79b12398350b3234c">
                                https://medium.com/@enbiosis/personalized-treatment-methods-are-now-in-hospitals-d7e152bd984f?source=friends_link&sk=031b1e7b650c3eb79b12398350b3234c
                            </a>
                        </div>
                        <div class="mt-3">
                            <a target="_blank" href="https://medium.com/@enbiosis/bayer-to-support-health-initiatives-has-prepared-turkey-digital-health-initiatives-sitemap-f34ac432a101?source=friends_link&sk=3e6394741068698abd2d4edaa379202e">
                               https://medium.com/@enbiosis/bayer-to-support-health-initiatives-has-prepared-turkey-digital-health-initiatives-sitemap-f34ac432a101?source=friends_link&sk=3e6394741068698abd2d4edaa379202e
                            </a>
                        </div>
                        @endif
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
@endsection
