@extends('layouts.public')
@section('subpageMenu','topmenu--subpage')

@section('pageTitle',__('pages/cv/hakanAlagozlu.page_title'))
@section('pageDescription',__('pages/cv/hakanAlagozlu.page_description'))
@section('styles')
<!-- font-awesome -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" integrity="sha256-+N4/V/SbAFiW1MPBCXnfnP9QSN3+Keu+NlB+0ev/YKQ=" crossorigin="anonymous"/>
@endsection

@section('content')
<section class="subpage-header subpage-header--microbiome-analysis">
	<div class="container">
		<div class="row">
			<div class="subpage-header__content">
				<h1 class="subpage-header__title">{{__('pages/cv/hakanAlagozlu.header_title')}}</h1>
				<div class="subpage-header__seperator"></div>
				<div class="subpage-header__breadcrumb">
                    <nav aria-label="breadcrumb">
						<ol class="breadcrumb no-bg-color text-light">
                          <li class="breadcrumb-item"><a href="{{ route('/')}}">{{__('pages/cv/hakanAlagozlu.breadcrumb.0')}}</a></li>
                          <li class="breadcrumb-item"><a href="{{ route('/')}}">{{__('pages/cv/hakanAlagozlu.breadcrumb.1')}}</a></li>
						  <li class="breadcrumb-item active" aria-current="page">{{__('pages/cv/hakanAlagozlu.breadcrumb.2')}}</li>
						</ol>
					</nav>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="mt-3">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="col-md-12 microbiome__col">
                <div>
                    <span><i>{{__('pages/cv/hakanAlagozlu.name')}}</i></span>
                    <img class="m-4" style="height: 310px;" src="{{ url('/') }}/new/img/hakanalagozlu.webp"/>
                    <p>{{__('pages/cv/hakanAlagozlu.desc')}}</p>
                    <p>{{__('pages/cv/hakanAlagozlu.interest')}}
                        <i class="fa fa-tag mx-2"></i>{{__('pages/cv/hakanAlagozlu.interests.0')}}
                        <i class="fa fa-tag mx-2"></i>{{__('pages/cv/hakanAlagozlu.interests.1')}}
                        <i class="fa fa-tag mx-2"></i>{{__('pages/cv/hakanAlagozlu.interests.2')}}
                    </p>
                    <div class="mt-3 break">
                        <span><i>{{__('pages/cv/hakanAlagozlu.articles')}}</i></span>
                        <div class="mt-3">
                            <a class="pt-2" target="_blank" href="https://www.turkiyeklinikleri.com/article/en-inflamatuar-bagirsak-hastaliklari-ve-mikrobiyota-83351.html">
                                https://www.turkiyeklinikleri.com/article/en-inflamatuar-bagirsak-hastaliklari-ve-mikrobiyota-83351.html
                            </a>
                        </div>
                        <div class="mt-3">
                            <a class="pt-2" target="_blank" href="https://www.turkiyeklinikleri.com/article/en-bagirsak-mikrobiyotasinin-ve-probiyotiklerin-obezite-uzerine-etkisi-83349.html">
                                https://www.turkiyeklinikleri.com/article/en-bagirsak-mikrobiyotasinin-ve-probiyotiklerin-obezite-uzerine-etkisi-83349.html
                            </a>
                        </div>
                        <div class="mt-3">
                            <a class="pt-2" target="_blank" href="https://link.springer.com/article/10.1007/s10620-005-9006-z">
                                https://link.springer.com/article/10.1007/s10620-005-9006-z
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
@endsection
