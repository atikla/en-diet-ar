@extends('layouts.public')
@section('subpageMenu','topmenu--subpage')

@section('pageTitle',__('pages/cv/aycanGundogdu.page_title'))
@section('pageDescription',__('pages/cv/aycanGundogdu.page_description'))
@section('styles')
<!-- font-awesome -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" integrity="sha256-+N4/V/SbAFiW1MPBCXnfnP9QSN3+Keu+NlB+0ev/YKQ=" crossorigin="anonymous"/>
@endsection

@section('content')
<section class="subpage-header subpage-header--microbiome-analysis">
	<div class="container">
		<div class="row">
			<div class="subpage-header__content">
				<h1 class="subpage-header__title">{{__('pages/cv/aycanGundogdu.header_title')}}</h1>
				<div class="subpage-header__seperator"></div>
				<div class="subpage-header__breadcrumb">
                    <nav aria-label="breadcrumb">
						<ol class="breadcrumb no-bg-color text-light">
                          <li class="breadcrumb-item"><a href="{{ route('/')}}">{{__('pages/cv/aycanGundogdu.breadcrumb.0')}}</a></li>
						  <li class="breadcrumb-item"><a href="{{ route('/')}}">{{__('pages/cv/aycanGundogdu.breadcrumb.1')}}</a></li>
						  <li class="breadcrumb-item active" aria-current="page">{{__('pages/cv/aycanGundogdu.breadcrumb.2')}}</li>
						</ol>
					</nav>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="mt-3">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="col-md-12 microbiome__col">
                <div>
                    <span><i>{{__('pages/cv/aycanGundogdu.name')}}</i></span>
                    <img class="m-4" style="float: right;height: 310px;" src="{{ url('/') }}/new/img/aycan-gundogdu.webp"/>
                    <p>{{__('pages/cv/aycanGundogdu.desc.0')}}</p>
                    <p>{{__('pages/cv/aycanGundogdu.desc.1')}}</p>
                    <p>{{__('pages/cv/aycanGundogdu.desc.2')}}</p>
                    @if (app()->getLocale() == 'tr')
                    <p>{{__('pages/cv/aycanGundogdu.interest')}}
                        <i class="fa fa-tag mx-2"></i>{{__('pages/cv/aycanGundogdu.interests.0')}}
                        <i class="fa fa-tag mx-2"></i>{{__('pages/cv/aycanGundogdu.interests.1')}}
                        <i class="fa fa-tag mx-2"></i>{{__('pages/cv/aycanGundogdu.interests.2')}}
                    </p>
                    <div class="mt-3 break">
                        <span><i>{{__('pages/cv/aycanGundogdu.articles')}}</i></span>
                        <div class="mt-3">
                            <a class="pt-2" target="_blank" href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5506383/">
                                https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5506383/
                            </a>
                        </div>
                        <div class="mt-3">
                            <a class="pt-2" target="_blank" href="http://tmc.dergisi.org/pdf/pdf_TMC_564.pdf ">
                                http://tmc.dergisi.org/pdf/pdf_TMC_564.pdf 
                            </a>
                        </div>
                        <div class="mt-3">
                            <a class="pt-2" target="_blank" href="https://dergipark.org.tr/tr/pub/atk/issue/47876/604749">
                                https://dergipark.org.tr/tr/pub/atk/issue/47876/604749
                            </a>
                        </div>
                        <div class="mt-3">
                            <a class="pt-2" target="_blank" href="https://www.liebertpub.com/doi/full/10.1089/fpd.2019.2651?url_ver=Z39.88-2003&rfr_id=ori:rid:crossref.org&rfr_dat=cr_pub%3dpubmed">
                                https://www.liebertpub.com/doi/full/10.1089/fpd.2019.2651?url_ver=Z39.88-2003&rfr_id=ori:rid:crossref.org&rfr_dat=cr_pub%3dpubmed
                            </a>
                        </div>
                        <div class="mt-3">
                            <a class="pt-2" target="_blank" href="http://www.floradergisi.org/getFileContent.aspx?op=REDPDF&file_name=2015-20-04-167-173.pdf">
                                http://www.floradergisi.org/getFileContent.aspx?op=REDPDF&file_name=2015-20-04-167-173.pdf
                            </a>
                        </div>
                        <div class="mt-3">
                            <a class="pt-2" target="_blank" href="https://www.sciencedirect.com/science/article/pii/S0196655317309549?via%3Dihub">
                                https://www.sciencedirect.com/science/article/pii/S0196655317309549?via%3Dihub
                            </a>
                        </div>
                        <div class="mt-3">
                            <a class="pt-2" target="_blank" href="https://www.liebertpub.com/doi/full/10.1089/mdr.2016.0321?url_ver=Z39.88-2003&rfr_id=ori:rid:crossref.org&rfr_dat=cr_pub%3dpubmed">
                                https://www.liebertpub.com/doi/full/10.1089/mdr.2016.0321?url_ver=Z39.88-2003&rfr_id=ori:rid:crossref.org&rfr_dat=cr_pub%3dpubmed
                            </a>
                        </div>
                        <div class="mt-3">
                            <a class="pt-2" target="_blank" href="https://www.frontiersin.org/articles/10.3389/fmicb.2016.01761/full">
                                https://www.frontiersin.org/articles/10.3389/fmicb.2016.01761/full
                            </a>
                        </div>
                        <div class="mt-3">
                            <a class="pt-2" target="_blank" href="https://pubmed.ncbi.nlm.nih.gov/27433106/">
                                https://pubmed.ncbi.nlm.nih.gov/27433106/
                            </a>
                        </div>
                        <div class="mt-3">
                            <a class="pt-2" target="_blank" href="https://www.escmid.org/escmid_publications/escmid_elibrary/?q=gundogdu&tx_solr%5Bfilter%5D%5B0%5D=author%253AAycan%2BGundogdu&tx_solr%5Bfilter%5D%5B1%5D=entry_type%253AOral%2Bpresentation">
                                https://www.escmid.org/escmid_publications/escmid_elibrary/?q=gundogdu&tx_solr%5Bfilter%5D%5B0%5D=author%253AAycan%2BGundogdu&tx_solr%5Bfilter%5D%5B1%5D=entry_type%253AOral%2Bpresentation
                            </a>
                        </div>
                        <div class="mt-3">
                            <a class="pt-2" target="_blank" href="https://www.klimikdergisi.org/EN/bacteriophage-therapy-an-unforgetten-source-of-cure-131179">
                                https://www.klimikdergisi.org/EN/bacteriophage-therapy-an-unforgetten-source-of-cure-131179
                            </a>
                        </div>
                    </div>
                    @else
                    <div class="mt-3 break">
                        <span><i>{{__('pages/cv/aycanGundogdu.articles')}}</i></span>
                        <div class="mt-3">
                            <a class="pt-2" target="_blank" href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5506383/">
                                https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5506383/
                            </a>
                        </div>
                        <div class="mt-3">
                            <a class="pt-2" target="_blank" href="https://www.liebertpub.com/doi/full/10.1089/fpd.2019.2651?url_ver=Z39.88-2003&rfr_id=ori:rid:crossref.org&rfr_dat=cr_pub%3dpubmed">
                                https://www.liebertpub.com/doi/full/10.1089/fpd.2019.2651?url_ver=Z39.88-2003&rfr_id=ori:rid:crossref.org&rfr_dat=cr_pub%3dpubmed
                            </a>
                        </div>
                        <div class="mt-3">
                            <a class="pt-2" target="_blank" href="http://www.floradergisi.org/getFileContent.aspx?op=REDPDF&file_name=2015-20-04-167-173.pdf">
                                http://www.floradergisi.org/getFileContent.aspx?op=REDPDF&file_name=2015-20-04-167-173.pdf
                            </a>
                        </div>
                        <div class="mt-3">
                            <a class="pt-2" target="_blank" href="https://www.sciencedirect.com/science/article/pii/S0196655317309549?via%3Dihub">
                                https://www.sciencedirect.com/science/article/pii/S0196655317309549?via%3Dihub
                            </a>
                        </div>
                        <div class="mt-3">
                            <a class="pt-2" target="_blank" href="https://www.liebertpub.com/doi/full/10.1089/mdr.2016.0321?url_ver=Z39.88-2003&rfr_id=ori:rid:crossref.org&rfr_dat=cr_pub%3dpubmed">
                                https://www.liebertpub.com/doi/full/10.1089/mdr.2016.0321?url_ver=Z39.88-2003&rfr_id=ori:rid:crossref.org&rfr_dat=cr_pub%3dpubmed
                            </a>
                        </div>
                        <div class="mt-3">
                            <a class="pt-2" target="_blank" href="https://www.frontiersin.org/articles/10.3389/fmicb.2016.01761/full">
                                https://www.frontiersin.org/articles/10.3389/fmicb.2016.01761/full
                            </a>
                        </div>
                        <div class="mt-3">
                            <a class="pt-2" target="_blank" href="https://pubmed.ncbi.nlm.nih.gov/27433106/">
                               https://pubmed.ncbi.nlm.nih.gov/27433106/
                            </a>
                        </div>
                        <div class="mt-3">
                            <a class="pt-2" target="_blank" href="https://www.escmid.org/escmid_publications/escmid_elibrary/?q=gundogdu&tx_solr%5Bfilter%5D%5B0%5D=author%253AAycan%2BGundogdu&tx_solr%5Bfilter%5D%5B1%5D=entry_type%253AOral%2Bpresentation">
                               https://www.escmid.org/escmid_publications/escmid_elibrary/?q=gundogdu&tx_solr%5Bfilter%5D%5B0%5D=author%253AAycan%2BGundogdu&tx_solr%5Bfilter%5D%5B1%5D=entry_type%253AOral%2Bpresentation
                            </a>
                        </div>
                        <div class="mt-3">
                            <a class="pt-2" target="_blank" href="https://www.klimikdergisi.org/EN/bacteriophage-therapy-an-unforgetten-source-of-cure-131179">
                                https://www.klimikdergisi.org/EN/bacteriophage-therapy-an-unforgetten-source-of-cure-131179
                            </a>
                        </div>
                    </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
@endsection
