@extends('layouts.public')
@section('subpageMenu','topmenu--subpage')

@section('pageTitle',__('pages/cv/tarkanKarakan.page_title'))
@section('pageDescription',__('pages/cv/tarkanKarakan.page_description'))

@section('styles')
<!-- font-awesome -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" integrity="sha256-+N4/V/SbAFiW1MPBCXnfnP9QSN3+Keu+NlB+0ev/YKQ=" crossorigin="anonymous"/>
@endsection
@section('content')
<section class="subpage-header subpage-header--microbiome-analysis">
	<div class="container">
		<div class="row">
			<div class="subpage-header__content">
				<h1 class="subpage-header__title">{{__('pages/cv/tarkanKarakan.header_title')}}</h1>
				<div class="subpage-header__seperator"></div>
				<div class="subpage-header__breadcrumb">
                    <nav aria-label="breadcrumb">
						<ol class="breadcrumb no-bg-color text-light">
                          <li class="breadcrumb-item"><a href="{{ route('/')}}">{{__('pages/cv/tarkanKarakan.breadcrumb.0')}}</a></li>
                          <li class="breadcrumb-item"><a href="{{ route('/')}}">{{__('pages/cv/tarkanKarakan.breadcrumb.1')}}</a></li>
						  <li class="breadcrumb-item active" aria-current="page">{{__('pages/cv/tarkanKarakan.breadcrumb.2')}}</li>
						</ol>
					</nav>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="mt-3">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="col-md-12 microbiome__col">
                <div>
                    <span><i>{{__('pages/cv/tarkanKarakan.name')}}</i></span>
                    <img class="m-4" style="float: right;height: 310px;" src="{{ url('/') }}/new/img/tarkankarakan.webp"/>
                    <p>{{__('pages/cv/tarkanKarakan.desc.0')}}</p>
                    <p>{{__('pages/cv/tarkanKarakan.desc.1')}}</p>
                    <p>{{__('pages/cv/tarkanKarakan.desc.2')}}</p>
                    <p>{{__('pages/cv/tarkanKarakan.interest')}}
                        <i class="fa fa-tag mx-2"></i>{{__('pages/cv/tarkanKarakan.interests.0')}}
                        <i class="fa fa-tag mx-2"></i>{{__('pages/cv/tarkanKarakan.interests.1')}}
                        @if (app()->getLocale() != 'tr')
                            <i class="fa fa-tag mx-2"></i>{{__('pages/cv/tarkanKarakan.interests.2')}}
                        @endif
                    </p>
                    <div class="mt-3 break">
                        <span><i>{{__('pages/cv/tarkanKarakan.articles')}}</i></span>
                        <div class="mt-3">
                            <a class="pt-2" target="_blank" href="https://academic.oup.com/ecco-jcc/article/11/8/905/2756096">
                                https://academic.oup.com/ecco-jcc/article/11/8/905/2756096
                            </a>
                        </div>
                        <div class="mt-3">
                            <a class="pt-2" target="_blank" href="https://www.researchgate.net/profile/Yeong_Yeh_Lee/publication/325922573_World_Gastroenterology_Organisation_Global_Guidelines_Diet_and_the_Gut/links/5b2c7c820f7e9b0df5ba646f/World-Gastroenterology-Organisation-Global-Guidelines-Diet-and-the-Gut.pdf">
                                https://www.researchgate.net/profile/Yeong_Yeh_Lee/publication/325922573_World_Gastroenterology_Organisation_Global_Guidelines_Diet_and_the_Gut/links/5b2c7c820f7e9b0df5ba646f/World-Gastroenterology-Organisation-Global-Guidelines-Diet-and-the-Gut.pdf
                            </a>
                        </div>
                        <div class="mt-3">
                            <a target="_blank" href="https://www.turkjgastroenterol.org/content/files/sayilar/301/buyuk/361.pdf">
                                https://www.turkjgastroenterol.org/content/files/sayilar/301/buyuk/361.pdf
                            </a>
                        </div>
                        <div class="mt-3">
                            <a target="_blank" href="https://dergipark.org.tr/tr/pub/gsbdergi/issue/32176/357029">
                                https://dergipark.org.tr/tr/pub/gsbdergi/issue/32176/357029
                            </a>
                        </div>
                        <div class="mt-3">
                            <a target="_blank" href="https://asosindex.com.tr/index.jsp?modul=articles-page&journal-id=18&article-id=2082">
                                https://asosindex.com.tr/index.jsp?modul=articles-page&journal-id=18&article-id=2082
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
@endsection
