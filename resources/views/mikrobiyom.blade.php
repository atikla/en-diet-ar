@extends('layouts.public')
@section('subpageMenu','topmenu--subpage')

@section('pageTitle',__('pages/mikrobiyom.page_title'))
@section('pageDescription',__('pages/mikrobiyom.page_description'))


@section('content')
<section class="subpage-header subpage-header--microbiome">
	<div class="container">
		<div class="row">
			<div class="subpage-header__content">
				<h1 class="subpage-header__title">{{__('pages/mikrobiyom.header_title')}}</h1>
				<div class="subpage-header__seperator"></div>
				<div class="subpage-header__breadcrumb">
					<nav aria-label="breadcrumb">
						<ol itemscope itemtype="https://schema.org/BreadcrumbList"
						class="breadcrumb no-bg-color text-light">
							<li itemprop="itemListElement" itemscope
							itemtype="https://schema.org/ListItem"
							class="breadcrumb-item">
								<a itemprop="item"
								style="color:#007bff"
								href="{{ route('/')}}">
								<span itemprop="name">{{__('pages/mikrobiyom.breadcrumb.0')}}</span>
								</a>
								<meta itemprop="position" content="1"/>
							</li>
							<li itemprop="itemListElement" itemscope
							itemtype="https://schema.org/ListItem"
							class="breadcrumb-item active te">
								<a itemprop="item" 
								style="color:#6c757d"
								href="{{ route('mikrobiyom')}}">
									<span itemprop="name">{{__('pages/mikrobiyom.breadcrumb.1')}}</span>
								</a>
								<meta itemprop="position" content="2"/>
							</li>
						</ol>
					</nav>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="ecosystem">
	<div class="container">
		<div class="row d-flex justify-content-center">
			<div class="col-md-8 ecosystem__quote">
				<span><i>{{__('pages/mikrobiyom.ecosystem_quote_top.0')}}</i></span>
				<span><i>{{__('pages/mikrobiyom.ecosystem_quote_top.1')}}</i></span>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 ecosystem__item">
				<h2 class="ecosystem__title">
					{{__('pages/mikrobiyom.ecosystem_title.0')}}:<br/><b>{{__('pages/mikrobiyom.ecosystem_title.1')}}</b>
				</h2>
				<p class="ecosystem__desc">
					{{__('pages/mikrobiyom.ecosystem_desc.0')}}
					<br/><br/>
					{{__('pages/mikrobiyom.ecosystem_desc.1')}}
				</p>
			</div>
			<div class="col-md-7 ecosystem__item">

				<p class="ecosystem__desc">
					{{-- @if (app()->getLocale() == 'tr')
						Çeşitliliği yüksek ve dengeli  bir mikrobiyom, metabolik sorunlar, otoimmün hastalıklar, sindirim sistemiyle ilişkilendirilmiş hastalıklar ve mental sorunlar başta olmak üzere bir çok hastalığa karşı bizi korur, fizyolojik ve psikolojik iyileşmeye katkı sağlayarak mevcut yaşam kalitemizi arttırır.
						<br/><br/>
					@endif --}}
					<i>
						<b>
							{{__('pages/mikrobiyom.ecosystem_desc.3')}}
						</b>
					</i>
				</p>
			</div>
		</div>
		<div class="row d-flex justify-content-center">
			<div class="col-md-8 ecosystem__quote">
				<span><i>{{__('pages/mikrobiyom.ecosystem_quote_bottom.0')}}</i></span>
				<span><i>{{__('pages/mikrobiyom.ecosystem_quote_bottom.1')}}</i></span>
			</div>
		</div>
	</div>
</section>
<section class="en-carousel"style="margin-bottom:80px;margin-top:80px;">

   <div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="maffects__title">
					{{__('pages/mikrobiyom.carousel_title.0')}}
					<br/>
					<b>
						{{__('pages/mikrobiyom.carousel_title.1')}}
					</b>
				</h2>
			</div>
		</div>
		<div class="row owl-carousel end-carousel owl-theme">
			<div class="item">
				<img src="{{ url('/') }}/new/img/dogumsekli.webp"/>
				<div class="content">
					<h3>{{__('pages/mikrobiyom.carousel_item_title_1')}}</h3>
					<p>{{__('pages/mikrobiyom.carousel_item_desc_1')}}</p>
				</div>
			</div>
			<div class="item">
				<img src="{{ url('/') }}/new/img/annesutu.webp"/>
				<div class="content">
					<h3>{{__('pages/mikrobiyom.carousel_item_title_2')}}</h3>
					<p>{{__('pages/mikrobiyom.carousel_item_desc_2')}}</p>
				</div>
			</div>
			<div class="item">
				<img src="{{ url('/') }}/new/img/beslenme.webp"/>
				<div class="content">
					<h3>{{__('pages/mikrobiyom.carousel_item_title_3')}}</h3>
					<p>{{__('pages/mikrobiyom.carousel_item_desc_3')}}</p>
				</div>
			</div>
			<div class="item">
				<img src="{{ url('/') }}/new/img/ilaclar.webp"/>
				<div class="content">
					<h3>{{__('pages/mikrobiyom.carousel_item_title_4')}}</h3>
					<p>{{__('pages/mikrobiyom.carousel_item_desc_4')}}</p>
				</div>
			</div>
			<div class="item">
				<img src="{{ url('/') }}/new/img/hastalik.webp"/>
				<div class="content">
					<h3>{{__('pages/mikrobiyom.carousel_item_title_5')}}</h3>
					<p>{{__('pages/mikrobiyom.carousel_item_desc_5')}}</p>
				</div>
			</div>
			<div class="item">
				<img src="{{ url('/') }}/new/img/yasamtarzi.webp"/>
				<div class="content">
					<h3>{{__('pages/mikrobiyom.carousel_item_title_6')}}</h3>
					<p>{{__('pages/mikrobiyom.carousel_item_desc_6')}}</p>
				</div>
			</div>
		</div>
   </div>
</section>
@include('include.mic_balance')
{{-- <section class="maffects">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="maffects__title">
					MİKROBİYOM<br/><b>NASIL ŞEKİLLENİR?</b>
				</h1>
			</div>
		</div>
		<div class="row d-flex justify-content-center">
			<div class="col-12 col-md-6 col-lg-4 col-xl-4">
				<div class="maffects__item">
					<img src="{{ url('/') }}/new/img/icons/dogumsekli.webp" width="70"/>
					<h2>Doğum Şekli</h2>
					<p>Vajinal veya sezaryen doğmuş olmanız mikrobiyomunuzdaki ilk kolonizasyonun annenizden mi yoksa dış dünyadan mı aldığınızı etkiler.</p>
				</div>
			</div>
			<div class="col-12 col-md-6 col-lg-4 col-xl-4">
				<div class="maffects__item">
					<img src="{{ url('/') }}/new/img/icons/annesutu.webp" width="70"/>
					<h2>Anne Sütü Alımı</h2>
					<p>Yaşamın ilk 3 ayında anne sütü almış olmak mikrobiyom tür ve çeşitliliği üzerinde önemli etkiye sahiptir.</p>
				</div>
			</div>
			<div class="col-12 col-md-6 col-lg-4 col-xl-4">
				<div class="maffects__item">
					<img src="{{ url('/') }}/new/img/icons/beslenme.webp" width="70"/>
					<h2>Beslenme</h2>
					<p>Katı besinlerle tanışma ile mikrobiyom çeşitliliği artış gösterir ve hayat boyu tercih ettiğiniz besinlerle değişim devam eder.</p>
				</div>
			</div>
			<div class="col-12 col-md-6 col-lg-4 col-xl-4">
				<div class="maffects__item">
					<img src="{{ url('/') }}/new/img/icons/ilac.webp" width="70"/>
					<h2>Kullanılan İlaçlar</h2>
					<p>Başta antibiyotikler olmak üzere kullanılan ilaçların önemli bir kısmı sindirim kanalından geçerken mikrobiyom üzerinde hasar bırakır.</p>
				</div>
			</div>
			<div class="col-12 col-md-6 col-lg-4 col-xl-4">
				<div class="maffects__item">
					<img src="{{ url('/') }}/new/img/icons/hastaliklar.webp" width="70"/>
					<h2>Hastalıklar</h2>
					<p>Geçirilen veya mevcutta olan hastalıklar mikrobiyomun değişiminde rol oynayabilir. Bu değişim özellikle bağışıklık sistemi üzerinde uzun süreli etki yaratabilir.</p>
				</div>
			</div>
			<div class="col-12 col-md-6 col-lg-4 col-xl-4">
				<div class="maffects__item">
					<img src="{{ url('/') }}/new/img/icons/cevrekosullari.webp" width="70"/>
					<h2>Çevre Koşulları ve Yaşam Tarzı</h2>
					<p>Yaşadığınız çevre, günlük yaşantınız, fiziksel aktivite düzeyiniz gibi faktörle mikrobiyomun şekillenmesinde iyi veya kötü rol oynayabilir.</p>
				</div>
			</div>
		</div>
	</div>
</section> --}}

<section class="microbiome-texts">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="microbiome-texts__text">
					<h2>
						{{__('pages/mikrobiyom.microbiome_text.0')}}<br/>
						<b>{{__('pages/mikrobiyom.microbiome_text.1')}}</b>
					</h2>
					<p>
						<b>
							{{__('pages/mikrobiyom.microbiome_text.2')}}
						</b><br/><br/>
					</p>
				</div>

				<div class="microbiome-texts__text">
					<div class="row microbiome-texts__imgrow">
						<div class="col-md-8">
							<p>
								{{__('pages/mikrobiyom.microbiome_text.3')}}
								<br/><br/>
								{{__('pages/mikrobiyom.microbiome_text.4')}}
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@if (app()->getLocale() == 'tr')
<section class="discover discover--microbiome">
	<div class="container">
		<div class="row">
			<div class="col-md-5">
				<h2 class="discover__title">{{__('pages/mikrobiyom.discover_title')}}</h2>
				<p class="discover__desc">
					{{__('pages/mikrobiyom.discover_desc')}}
				</p>
				<a class="link-btn link-btn--orange" href="{{url('/checkout')}}"
				onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'button', Label: '{{slug('satin_al')}}' })">
					{{__('pages/mikrobiyom.satin_al')}}
				</a>
			</div>
		</div>
	</div>
</section>
@endif
@endsection
@section('scripts')
<script type="text/javascript">

	$('.sleep-quality__carousel-list__item').click(function(){
		$('.sleep-quality__carousel-list__item').removeClass('active');
		$(this).addClass('active');
		var target = $(this).data('target');
		$('.sleep-quality__content').removeClass('active');
		$(target).addClass('active');
		$('.sleep-quality').attr('class','sleep-quality sleep-quality--home ' + target.replace('#',''));
	});
	$(window).resize(function(){
		$('.header').height($(window).height());
	});

	$(document).ready(function(){
		$('.end-carousel').owlCarousel({
		    loop:true,
			autoplay:true,
			autoplayTimeout:6000,
		    margin:20,
		    nav:true,
			dots:true,
			navText:['<img src="{{ url("/") }}/new/img/slide-prev.webp"/>','<img src="{{ url("/") }}/new/img/slide-next.webp"/>'],
		    responsive:{
		        0:{
		            items:1,
					nav:true,
					dots:true,
					// navText:['<img src="{{ url("/") }}/new/img/slide-prev.webp"/>','<img src="{{ url("/") }}/new/img/slide-next.webp"/>'],
		        },
		        600:{
		            items:3
		        },
		        1000:{
		            items:5
		        }
		    }
		});
		$('.end-carousel-balance').owlCarousel({
		    loop:true,
			autoplay:true,
			autoplayTimeout:6000,
		    margin:20,
		    nav:true,
			dots:true,
			navText:['<img src="{{ url("/") }}/new/img/slide-prev.webp"/>','<img src="{{ url("/") }}/new/img/slide-next.webp"/>'],
		    responsive:{
		        0:{
		            items:1,
					nav:true,
					dots:true,
					// navText:['<img src="{{ url("/") }}/new/img/slide-prev.webp"/>','<img src="{{ url("/") }}/new/img/slide-next.webp"/>'],
		        },
		        600:{
		            items:2
		        },
		        1000:{
		            items:3
		        }
		    }
		});
	});
</script>
@endsection
