@extends('layouts.public')

@section('content')
    

<section class="header subpage bilim">
	<div class="container">
		<h1 class="title"></h1>

        <h1 class="title"> {{__('app.bilim')}} </h1>
	</div>
</section>
<section class="bilim-content">

    <div class="container">
		<p class="text">
            {{__('app.bilim_1')}}
        <br/><br>
            {{__('app.bilim_2')}}
        </p>
		<div class="row">
			<div class="col-xl-4 col-lg-4 col-md-6 box">
				<img src="{{url('/')}}/img/kisisel-saglik.png" />
				<h2>{{__('app.bilim_row_1_1')}}</h2>
				<p>{{__('app.bilim_row_1_2')}}</p>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-6 box">
				<img src="{{url('/')}}/img/kisisel-diet.png" />
				<h2>{{__('app.bilim_row_2_1')}}</h2>
				<p>{{__('app.bilim_row_2_2')}}</p>
            	</div>
			<div class="col-xl-4 col-lg-4 col-md-6 box">
				<img src="{{url('/')}}/img/rrna.jpg" />
				<h2>{{__('app.bilim_row_3_1')}}</h2>
				<p>{{__('app.bilim_row_3_2')}}</p>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-6 box">
				<img src="{{url('/')}}/img/yapay-zeka.jpg" />
				<h2>{{__('app.bilim_row_4_1')}}</h2>
				<p>{{__('app.bilim_row_4_2')}}</p>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-6 box">
				<img src="{{url('/')}}/img/genkok.jpg" />
				<h2>{{__('app.bilim_row_5_1')}}</h2>
				<p>{{__('app.bilim_row_5_2')}}</p>
			</div>
			<div class="col-xl-4 col-lg-4 col-md-6 box">
				<img src="{{url('/')}}/img/akademisyen.jpg" />
				<h2>{{__('app.bilim_row_6_1')}}</h2>
				<p>{{__('app.bilim_row_6_2')}}</p>
			</div>
		</div>
	</div>
</section>

@endsection