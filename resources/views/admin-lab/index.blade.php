@extends('layouts.admin-lab')

@section('content')

<div class="row justify-content-center px-4">
    <div class="col">
        @include('include.messages')
        @include('include.form-errors')
        <div class="card">
            <div class="card-header">Dashboard</div>

            <div class="card-body">
                You are logged in! <br>
                @if (auth()->user()->email != 'dbeyazgul@enbiosis.com')
                    <button type="button" class="btn btn-outline-success my-2" data-toggle="modal" data-target="#gida_skorlari">
                        Upload Food scores 
                    </button>
                    <button type="button" class="btn btn-outline-success my-2" data-toggle="modal" data-target="#skorlar">
                        Upload Mikrobiyom Analiz
                    </button>
                @endif
            </div>
        </div>
    </div>
</div>

{{-- For Food  --}}
<div class="modal fade" id="gida_skorlari" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Upload Food scores </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
            {!! Form::open(['method'=>'POST', 'action' => 'LaboratoryAdminController@uploadFoodJsonFile', 'files'=>true, 'id' => 'upload_gida_skorlari']) !!}
                <div class="form-group">
                    {!! Form::label('json-file', 'Json File:') !!}
                    {!! Form::file('json-file', [ 'class' => 'ml-5' ]) !!}
                </div>
            {!! Form::close() !!}
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button class="btn btn-primary" onclick="event.preventDefault();document.getElementById('upload_gida_skorlari').submit();">Save changes</button>
        </div>
    </div>
    </div>
</div>
{{-- Upload Mikrobiyom Analiz --}}
<div class="modal fade" id="skorlar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Upload Mikrobiyom Analiz</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
            {!! Form::open(['method'=>'POST', 'action' => 'LaboratoryAdminController@uploadMicrobiomeJsonFile', 'files'=>true, 'id' => 'skorlari']) !!}
                <div class="form-group">
                    {!! Form::label('json-file', 'Json File:') !!}
                    {!! Form::file('json-file', [ 'class' => 'ml-5' ]) !!}
                </div>
            {!! Form::close() !!}
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button class="btn btn-primary" onclick="event.preventDefault();document.getElementById('skorlari').submit();">Save changes</button>
        </div>
    </div>
    </div>
</div>
@endsection
