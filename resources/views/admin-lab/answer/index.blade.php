@extends('layouts.admin-lab')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-dark text-light">Kits</div>

                    <div class="card-body ">
                        You are logged in as Lab Admin You are in Survey Kit Answer Section!
                    </div>
                </div>
                @include('include.messages')
                @include('include.form-errors')
            </div>
        </div>
    </div>
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="text-right">
                    {!! Form::open(['method'=>'POST', 'action' => ['LaboratoryAdminController@search'], 'style'=> 'display:inline']) !!}

                        {!! Form::label('search', 'Kit Code:') !!}
                        {!! Form::text('search', null, ['class'=>'form-control', 'style'=> 'display:inline; width:initial', 'required']) !!}

                        {!! Form::submit('search', ['class'=>'btn btn-outline-success mr-2 mb-1']) !!}

                    {!! Form::close() !!}
                </div>
                <table class="table table-responsive-lg table-striped">
                    <thead class="thead-light">
                    <tr>
                        <th>#</th>
                        <th>kit code</th>
                        <th>kit status</th>
                        <th>Answered at</th>
                        <th>Process</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="1"></td>
                            <td colspan="5"><input class="mr-2 select-all" type="checkbox" value="" onclick="checkAll($ ( this ) )">Selecet All</td>
                        </tr>
                        @if ($kits)
                            @foreach ($kits as $kit)
                            <tr>
                                <td>{{$kit->id}}</td>
                                <td><input class="mr-2 kit-code" type="checkbox" value="{{$kit->kit_code}}" onclick="check($ ( this ) )">{{$kit->kit_code}}</td>
                                <td>{{$kit->kitStatuse}}</td>
                                <td>{{$kit->survey_filled_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}</td>
                                <td>
                                    @if (auth()->user()->email == 'dbeyazgul@enbiosis.com')
                                        <a href="{{route('lab.answer.show', $kit->id)}}" class="btn btn-outline-info mr-2 mb-1" >Show Answer</a>
                                        @if ( $kit->food && $kit->microbiome )
                                            <a href="{{route('lab.pdf.generate', [$kit->kit_code, 'lang' => 'tr'])}}" class=" pdf btn btn-outline-secondary mb-2">
                                                Go To <span class="flag-icon flag-icon-tr"> </span>  Türkçe Reports
                                            </a>
                                            <a href="{{route('lab.pdf.generate', [$kit->kit_code, 'lang' => 'en'])}}" class=" pdf btn btn-outline-secondary mb-2">
                                                    Go To <span class="flag-icon flag-icon-gb"> </span>  English Reports
                                            </a>
                                       @endif
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        @endif
                        <tr>
                            <td colspan="2" class="">
                                {!! Form::open(['method'=>'POST', 'action' => ['LaboratoryAdminController@export'], 'style'=> 'display:inline', 'id' => 'selecet-kits']) !!}
                                    {!! Form::submit('Export Selected Item as Json', ['class'=>'btn btn-outline-info mr-2 mb-1']) !!}
                                    <input type="hidden" name="kits" id="kits">
                                {!! Form::close() !!} 
                            </td>
                            <td colspan="3" class="text-center">
                                {!! Form::open(['method'=>'POST', 'action' => ['LaboratoryAdminController@export', 'all' => true], 'style'=> 'display:inline']) !!}
                                    {!! Form::submit('Export All Item as Json', ['class'=>'btn btn-outline-success mr-2 mb-1']) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="text-center my-4">
                    {{$kits->links()}}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $( document ).ready(function() {
            let con = false;
            $('#selecet-kits').submit( function (e){
                if ( !con ) {
                    e.preventDefault();
                    let array = [];
                    if ( $('.kit-code:checked').length == 0  ) {
                        alert( 'you must at least check one checkbox ' );
                        return 0;
                    } else {
                        let input = '';
                        $( '.kit-code:checked' ).each( function () {
                            input += $( this ).val() + '-';
                        });
                        console.log(input);
                        $('#kits').val(input);
                        console.log( '---------'+$('#kits').val() + '---------');
                        con = true;
                    }
                }
                $('#selecet-kits')[0].submit();
            });
        });
        function checkAll(e) {
            $('.kit-code').prop("checked", e.prop("checked"));
        }
        function check(e) {
            if ( $('.kit-code:checked').length == $('.kit-code').length) {
                $('.select-all').prop("checked", true);
            }else {
                $('.select-all').prop("checked", false);
            }
        }
    </script>
     <script type="text/javascript">
        $(document).ready(function(){
            $('.pdf').click(function (){
                $body.addClass("loading");
            });
        });
    </script>
@endsection
