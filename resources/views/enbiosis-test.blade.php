@extends('layouts.public')

@section('content')
<section class="header subpage test">
	<div class="container">
        <h1 class="title"> {{__('app.enbiosis_test')}}</h1>
	</div>
</section>
<section class="test-content">

    <div class="container">
		<p class="text">
			{!! __('app.test_1')  !!}
		</p>
	</div>

	
</section>
<section class="features">


	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="f-item">
					<img src="{{url('/')}}/img/f-icon-1.png" />
                    {!! __('app.test_2_1') !!}
				</div>
			</div>
			<div class="col-md-6">
				<div class="f-item">
					<img src="{{url('/')}}/img/f-icon-2.png" />
                    {!! __('app.test_2_2') !!}
				</div>
			</div>
			<div class="col-md-6">
				<div class="f-item">
					<img src="{{url('/')}}/img/f-icon-3.png" />
                    {!! __('app.test_2_3') !!}
				</div>
			</div>
			<div class="col-md-6">
				<div class="f-item">
					<img src="{{url('/')}}/img/f-icon-4.png" />
                    {!! __('app.test_2_4') !!}
				</div>
			</div>
			<div class="col-md-6">
				<div class="f-item">
					<img src="{{url('/')}}/img/f-icon-5.png" />
                    {!! __('app.test_2_5') !!}
				</div>
			</div>
			<div class="col-md-6">
				<div class="f-item">
					<img src="{{url('/')}}/img/f-icon-6.png" />
					{!! __('app.test_2_6') !!}
				</div>
			</div>
		</div>
    </div>
</section>
@endsection