@extends('layouts.public')
@section('subpageMenu','topmenu--subpage')
@section('pageTitle', __('pages/checkout.page_title'))
@section('pageDescription', __('pages/checkout.page_description'))
@section('metaTag')
<meta name="robots" content="noindex">
@endsection
@section('styles')
	<style>
		.strike {
			color: #514689 !important;
		}
	</style>
@endsection
@section('content')
<section class="subpage-header subpage-header--checkout">
	<div class="container">
		<div class="row">
			<div class="subpage-header__content">
				<h1 class="subpage-header__title">{{__('pages/checkout.header_title')}}</h1>
				<div class="subpage-header__seperator"></div>
				<div class="subpage-header__breadcrumb">
					<nav aria-label="breadcrumb">
						<ol itemscope itemtype="https://schema.org/BreadcrumbList"
						class="breadcrumb no-bg-color text-light">
							<li itemprop="itemListElement" itemscope
							itemtype="https://schema.org/ListItem"
							class="breadcrumb-item">
								<a itemprop="item"
								style="color:#007bff"
								href="{{ route('/')}}">
								<span itemprop="name">{{__('pages/checkout.breadcrumb.0')}}</span>
								</a>
								<meta itemprop="position" content="1"/>
							</li>
							<li itemprop="itemListElement" itemscope
							itemtype="https://schema.org/ListItem"
							class="breadcrumb-item te">
								<a itemprop="item" 
								style="color:#007bff"
								href="{{ route('checkout')}}">
									<span itemprop="name">{{__('pages/checkout.breadcrumb.1')}}</span>
								</a>
								<meta itemprop="position" content="2"/>
							</li>
							<li itemprop="itemListElement" itemscope
							itemtype="https://schema.org/ListItem"
							class="breadcrumb-item active te">
								<a itemprop="item" 
								style="color:#6c757d"
								href="{{ route('checkout.Campaign', strtolower($coupon->code))}}">
									<span itemprop="name">{{ucwords($coupon->code)}}</span>
								</a>
								<meta itemprop="position" content="2"/>
							</li>
						</ol>
						{{-- <ol class="breadcrumb no-bg-color text-light">
						  <li class="breadcrumb-item"><a href="{{ route('/')}}">{{__('pages/checkout.breadcrumb.0')}}</a></li>
						  <li class="breadcrumb-item active" aria-current="page">{{__('pages/checkout.breadcrumb.1')}}</li>
						</ol> --}}
					</nav>
				</div>
			</div>
		</div>
	</div>
</section>
<section class=" buy__first pricing py-5" style="margin: 0">
	<div class="container">
		<div class="row buy__row justify-content-center">
			<div class="text-center col-12 mb-5">
				<h2 class="triple-box__title" style="color:#e8957e">{{ucwords($coupon->code)}} Kapmanyasına katılarak %{{$coupon->discount}} indirimi uygulanacak</h2>
			</div>
		</div>
		<div class="row buy__row justify-content-center">
			@foreach ($products as $product)
			<div class="col-lg-6 mt-lg-0 mt-3">
				<div class="card mb-5 mb-lg-0" style="height: 100%;background-color: rgba(255, 255, 255, 0.9);">
					<div class="card-body">
						<h6 class="card-price text-center">{{$product->name}}</h6>
						<h6 class="card-price text-center">
							<strike class="">{{$product->price + 500}} TL</strike>
						</h6>
						<h6 class="card-price text-center">
							<strike class="mr-3">{{$product->price}} TL</strike>
							{{ round ( (double) ( ( ( 100 - $coupon->discount ) * $product->price ) / 100 ), 2)}} TL
						</h6>
						<hr>
						<div class="my-1">
							@if ($product->slug == 'mikrobiyom-saglik-paketi')
								<ul class="fa-ul list-style-type text-center" style="list-style-type: none;">
									<li><b>Mikrobiyom Analizi</b></li>
									<li><b>Kişiselleştirilmiş Beslenme Rehberi</b></li>
								</ul>
								<div class="text-center mt-5 mt-checkout mb-3">
									<img src="{{url('/new/img/saglik_kit.webp')}}" alt="" class="mb-2"> <br>
									<a class="link-btn link-btn--orange" href="{{route('checkout.Campaign.index', [$coupon->code, $product->slug])}}">SATIN AL</a>
								</div>
							@elseif($product->slug == 'mikrobiyom-saglik-paketi-premium')
								<ul class="fa-ul list-style-type text-center" style="list-style-type: none;">
									<li><b>Mikrobiyom Analizi</b></li>
									<li><b>Kişiselleştirilmiş Beslenme Rehberi</b></li>
									<li><b>Diyetisyen desteği</b></li>
								</ul>
								<div class="text-center mt-5 mb-3">
									<img src="{{url('/new/img/saglik_kit_premium.webp')}}" alt="" class="mb-2"> <br>
									<a class="link-btn link-btn--orange" href="{{route('checkout.Campaign.index', [$coupon->code, $product->slug])}}">SATIN AL</a>
								</div>
							@endif
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</div>
	</div>
</section>
<section class="triple-box" style="margin: 60px 0 0 0;">
	<div class="container">
		<div class="row">
			<div class="col-12 triple-box__box text-center pb-3">
				<span><i>Bağırsaklarında trilyonlarca bakteri var, bu bakterileri keşfederek sorunların kaynağına inmek ve kişiselleştirilmiş sağlık yolculuğuna adım atmak artık senin elinde!</i></span>
			</div>
			<div class="col-md-4 triple-box__box text-center">
				<img src="{{url('/')}}/new/img/triple-icon1.webp" />
				<span>{{__('pages/urunlerimiz.box_item_1')}}</span>
				<p> Bağırsak bakteri ekosisteminin analizi</p>
			</div>
			<div class="col-md-4 triple-box__box text-center">
				<img src="{{url('/')}}/new/img/triple-icon2.webp" />
				<span>{{__('pages/urunlerimiz.box_item_2')}}</span>
				<p> Vücudunun ihtiyaç duyduğu besinlerin raporu</p>
			</div>
			<div class="col-md-4 triple-box__box text-center">
				<img src="{{url('/')}}/new/img/triple-icon3.webp" />
				<span>{{__('pages/urunlerimiz.box_item_3')}}</span>
				<p> 6 haftalık diyetisyen takibi </p>
			</div>
		</div>
	</div>
</section>
@endsection

@section('scripts')
@endsection
