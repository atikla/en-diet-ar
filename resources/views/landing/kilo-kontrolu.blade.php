@extends('layouts.public')

@section('pageTitle','Mikrobiyota İle Sağlıklı Yaşam | Enbiosis')
@section('pageDescription','ENBIOSIS ile kalıcı kilo kaybı için sana en uygun besinleri öğren')


@section('content')
<section class="header kilo__1" style="">
	<div class="container">
		<div class="row">
			<div class="col-7 col-md-7 col-lg-6 col-xl-6">
				<span class="header__slogan kilo__2"><b>FAZLA KİLOLARINA KALICI ÇÖZÜM BULMANIN ZAMANI GELDİ!</b></span>
				<p class="header__desc">
					Kilo vermek için aç kaldığın, sana uymayan diyetlerle vakit harcadığın zamanların sonu sence de gelmedi mi? Sana en uygun besinleri öğrenerek kalıcı kilo kaybı sağlamak artık ENBIOSIS ile mümkün!
					<br/><br/>
					Hangi besinlerin iyi veya kötü olduğunu bir kenara bırakıp senin için en iyi olana odaklanmanın zamanı geldi!
				</p>
				<a class="link-btn link-btn--orange" href="{{url('/checkout')}}"
				onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'button', Label: '{{slug('landing_page')}}' })">
				Hemen Satın Al</a>
			</div>
		</div>
	</div>
</section>
<section class="header header--mobile" style="background-image: url(/new/img/kilo-mobile.webp);background-color:white;padding-bottom:0;">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-12 col-lg-12 col-xl-12" style="    text-align: center;">
				<h1 class="header__slogan"><b>FAZLA KİLOLARINA KALICI ÇÖZÜM BULMANIN ZAMANI GELDİ!</b></h1>
				<p class="header__desc">
					Kilo vermek için aç kaldığın, sana uymayan diyetlerle vakit harcadığın zamanların sonu sence de gelmedi mi? Sana en uygun besinleri öğrenerek kalıcı kilo kaybı sağlamak artık ENBIOSIS ile mümkün!
					<br/><br/>
					Hangi besinlerin iyi veya kötü olduğunu bir kenara bırakıp senin için en iyi olana odaklanmanın zamanı geldi!
				</p>
				<a class="link-btn link-btn--orange" href="{{url('/checkout')}}"
				onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'button', Label: '{{slug('landing_page')}}' })">
				Hemen Satın Al</a>
			</div>
		</div>
	</div>
</section>

<section class="bg-none">
	<div class="container">
		<div class="row">
			<div class="col-12">
				@include('include.video.kesfet', ['id' => 'kesfet'])
			</div>
		</div>
	</div>
</section>

<section class="mictobiome-balance">
	<section class="microbiome-analysis microbiome-analysis--2">
		<div class="container">
			<div class="row">
				<div class="col-md-12 mictobiome-balance-mobile">
					<img src="/new/img/microbiome-analysis-bg3-mobile.webp" style="width:100%;height:auto;margin-bottom:70px;"/>
				</div>
				<div class="col-md-12 col-lg-6 col-xl-6">
					<h2 class="microbiome-analysis__title bagisiklik__t" style="font-size:36px;">KİLONU YÖNETEN <b>BAKTERİLER</b></h2>
					<p class="microbiome-analysis__desc">
						Bağırsaklarımız trilyonlarca bakteriye ev sahipliği yapmaktadır. Bu mikroskobik canlıların oluşturduğu ekosisteme mikrobiyom denir. Mikrobiyomunda yer alan bakteri gruplarına, onların çeşitliliğine ve dağılımına bakarak kilo almaya yatkınlığın ölçülebilir! Eğer verdiğin kiloları sık sık tekrar alıyorsan artık cevabı buldun demektir. Mikrobiyomundaki bakterilerin dengesini sağlamadan bu probleminden kalıcı olarak kurtulamayacağın ise bir gerçektir. ENBIOSIS Mikrobiyom Analizi ile mikrobiyomundaki seni sabote eden bakterilerin karakterini öğrenebilir ve sadece sana özel olarak hazırlanan Kişiselleştirilmiş Beslenme Rehberi ile mikrobiyomunu yönetebilirsin! Artık bedeninle barış ilan etmenin zamanı geldi!
					</p>
				</div>
			</div>
		</div>
	</section>
</section>
<section class="analyze-report mt-5">
	<section class="analyze-report">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2 class="analyze-report__title">
						ENBIOSISİ <b>DENEYİMLE</b>
					</h2>
				</div>
				<div class="col-md-12 col-lg-6 col-xl-6">
					<div class="analyze-report__item">
						<h3>Mikrobiyom Çeşitliliği:</h3>
						<span>Mikrobiyomunda bulunan bakteri ekosisteminin ne derece farklı cinslerden oluştuğunu ve dağılımlarının ne kadar homojen olduğunu gösterir.</span>
					</div>

					<div class="analyze-report__item">
						<h3>Taksonomik Analiz:</h3>
						<span>Taksonomik analiz, mikrobiyomunda var olan bakterilerin cins, aile ve şube seviyesinde oranlarını gösterir.</span>
					</div>

					<div class="analyze-report__item">
						<h3>Bağırsak Skorları:</h3>
						<span>Bağırsak haraketliliğinden mikrobiyom yaşına, karbonhidrat, yağ, protein metabolizmalarından uyku kalitene kadar 15 farklı modül üzerinden mevcut durumunu gösterir.</span>
					</div>


					<div class="analyze-report__item">
						<h3>Yakın Profiller:</h3>
						<span>Veri tabanımızda yer alan mikrobiyom profilleri doğrultusunda, senin mikrobiyomuna en yakın profillerin yaşam tarzı ve sağlık durumu istatistiklerini gösterir.</span>
					</div>


					<div class="analyze-report__item">
						<h3>Önemli Organizmalar:</h3>
						<span>Mikrobiyom sağlığında ve dengesinde öne çıkan mikroorganizmaların  oranlarını gösterir.</span>
					</div>

				</div>
				<div class="col-md-12 col-lg-6 col-xl-6 iphone-wrapper">
					<div class="device device-ipad-pro device-gold">
						<div class="device-frame">
							@if (app()->getLocale() == 'tr')
								<iframe class="device-content" src="https://app.enbiosis.com/demo/home/tr" style="object-fit: initial;width:100%;height:100%;border:0;"></iframe>
							@else
								<iframe class="device-content" src="https://app.enbiosis.com/demo/home/en" style="object-fit: initial;width:100%;height:100%;border:0;"></iframe>
							@endif
							{{-- <iframe class="device-content" src="https://app.enbiosis.com/demo/home" style="object-fit: initial;width:100%;height:100%;border:0;"></iframe> --}}
						</div>
						<div class="device-stripe"></div>
						<div class="device-header"></div>
						<div class="device-sensors"></div>
						<div class="device-btns"></div>
						<div class="device-power"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@include('include.banner')
<section class="mictobiome-balance">
	<section class="microbiome-analysis microbiome-analysis--2 bagisiklik__5">
		<div class="container">
			<div class="row">
				<div class="col-md-12 mictobiome-balance-mobile">
					<img src="/new/img/microbiome-analysis-bg3-mobile.webp" style="width:100%;height:auto;margin-bottom:70px;"/>
				</div>
				<div class="col-md-12 col-lg-6 col-xl-6">
					<h2 class="microbiome-analysis__title bagisiklik__t" style="font-size:36px;">BESİN SKORLARI İLE <b>MİKROBİYOMUNU</b> YÖNET</h2>
					<p class="microbiome-analysis__desc">
						ENBIOSIS Mikrobiyom Analizi ile mikrobiyom karakterini öğrendikten sonra sıra onları istediğimiz yönde şekillendirmeye gelir. Neyse ki mikrobiyomunu düzene getirmen için tüketmen gereken besinleri biliyoruz! Sahip olduğumuz yapay zeka algoritmaları ile daha iyi bir mikrobiyom profili elde etmen için sıklıkla tüketmen veya sakınman gereken besinleri sana skorlamalarla birlikte sunuyoruz. Geriye kalan tek şey ise ENBIOSIS diyetisyenleri takibinde beslenmeni yönetip kendindeki farkı gözlemlemek!
					</p>
				</div>
			</div>
		</div>
	</section>
</section>
<section class="hprocess">
	<div class="container">
		<div class="row hprocess__howto" style="margin-top:0;">
			<h2 class="hprocess__howtotitle">SÜREÇ NASIL İŞLER?</h2>
			<div class="col-12 mb-3">
				@include('include.video.surec', ['id' => 'surec'])
			</div>
			<div class="col-md-6 col-lg-3 col-xl-3 hprocess__step">
				<div class="hprocess__number">
					<img src="{{ url('/') }}/new/img/icons/surec1.webp" width="50"/>
				</div>
				<h2 class="hprocess__steptitle">ENBIOSIS MİKROBİYOM ANALİZİ SİPARİŞİ</h2>
				<p class="hprocess__stepdesc">
					www.enbiosis.com 'dan sipariş ver, sana gelen kutudaki kod ile web sitesinden kaydını oluştur ve anket sorularını tamamla.
				</p>
			</div>
			<div class="col-md-6 col-lg-3 col-xl-3 hprocess__step">
				<div class="hprocess__number">
					<img src="{{ url('/') }}/new/img/icons/surec2.webp" width="50"/>
				</div>
				<h2 class="hprocess__steptitle">NUMUNE ALIMI</h2>
				<p class="hprocess__stepdesc">
					Kargo ile adresine teslim edilen Mikrobiyom Kitinin içerisinde yer alan adımları takip et ve mikrobiyom analizin için gerekli gaita numuneni kolayca al. Sonrasında numuneni kutuda yer alan adrese ücretsiz bir şekilde gönder.
				</p>
			</div>
			<div class="col-md-6 col-lg-3 col-xl-3 hprocess__step">
				<div class="hprocess__number">
					<img src="{{ url('/') }}/new/img/icons/surec3.webp" width="50"/>
				</div>
				<h2 class="hprocess__steptitle">LABORATUVAR ANALİZİ</h2>
				<p class="hprocess__stepdesc">
					Numunen Metagenom laboratuvarımızda işleme alınarak 16S rRNA yeni nesil dizileme yöntemi ile dizilenir. Elde edilen veriler yapay zeka algoritmalarımız ile ayrıntılı olarak analiz edilir ve raporlandırılır.
				</p>
			</div>
			<div class="col-md-6 col-lg-3 col-xl-3 hprocess__step">
				<div class="hprocess__number">
					<img src="{{ url('/') }}/new/img/icons/surec4.webp" width="50"/>
				</div>
				<h2 class="hprocess__steptitle">ENBIOSIS KİŞİSELLEŞTİRİLMİŞ BESLENME REHBERİ</h2>
				<p class="hprocess__stepdesc">
					Raporların uzmanlarımız tarafından kontrol edildikten sonra sana ulaştırılır. Besin skorların ENBIOSIS diyetisyenleri tarafından yorumlanır ve diyetisyen takibi ile birlikte sana sunulur.
				</p>
			</div>
		</div>
	</div>
</section>
<section class="analyze-report mt-5">
	<div class="container">
		<div class="row analyze-report__besin-skorlari bagisiklik__6">
			<div class="col-md-12 col-lg-6 col-xl-6 d-flex align-items-center">
				<div class="analyze-report__item">
					<h3>Besin Skorları:</h3>
					<span>Mikrobiyom sağlığı büyük bir oranda tükettiğin besinlerden etkilenmektedir. Mikrobiyom profiline göre sana özel hazırlanmış besin skorları ile sağlıklı bir mikrobiyom için tüketmen gereken besinleri keşfet!</span>

					<div class="analyze-report__food-rating">
						<img src="{{url('/')}}/new/img/iyi-besin.webp" width="100"/>
						<div class="content">
							<h4>İşte Senin Besinlerin!</h4>
							<p>
								Sık tüketmen gereken besinleri ifade eder.
							</p>
						</div>
					</div>
					<div class="analyze-report__food-rating">
						<img src="{{url('/')}}/new/img/orta-besin.webp" width="100"/>
						<div class="content">
							<h4>Seninle Uyum İçinde Olan Besinler!</h4>
							<p>
								Dengeli ve çeşitli şekilde tüketmen gereken besinleri ifade eder.
							</p>
						</div>
					</div>
					<div class="analyze-report__food-rating">
						<img src="{{url('/')}}/new/img/kotu-besin.webp" width="100"/>
						<div class="content">
							<h4>Kaçınman Gereken Besinler!</h4>
							<p>
								Daha az tüketmen gereken besinleri ifade eder.
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12 col-lg-6 col-xl-6 iphone-wrapper">
				<div class="device device-ipad-pro device-gold">
					<div class="device-frame">
						<iframe class="device-content" src="https://app.enbiosis.com/demo/foods/tr" style="object-fit: initial;width:100%;height:100%;border:0;"></iframe>
					</div>
					<div class="device-stripe"></div>
					<div class="device-header"></div>
					<div class="device-sensors"></div>
					<div class="device-btns"></div>
					<div class="device-power"></div>
				</div>
			</div>
		</div>
		<div class="row d-flex justify-content-center">
			<div class="col-md-12 analyze-report__quote">
				<i>Raporların uzmanlarımız tarafından kontrol edildikten sonra sana ulaştırılır. Besin skorların ENBIOSIS diyetisyenleri tarafından yorumlanır ve 6 haftalık diyetisyen takibi ile birlikte sana sunulur.</i><br/><br/>
			</div>
		</div>
	</div>
</section>
@include('include.package')
<section class="bilimkurulu">
	<span class="bilimkurulu__title">ENBIOSIS Bilim Kurulu</span>
	<div class="container">
		<div class="row team__row justify-content-center">
			<div class="col-12 col-md-6 col-lg-4 col-xl-4 team__col">
				<a href="{{route('prof-dr-hakan-alagozlu')}}">
					<div class="team__member">
						<img class="team__member__avatar" src="{{ url('/') }}/new/img/hakanalagozlu.webp" />
						<h2 class="team__member__name">Prof. Dr. Hakan Alagözlü</h2>
						<span class="team__member__job team__member__job--2">Gastroenteroloji Uzmanı</span>
						<span class="team__member__job">Gazi Üniversitesi Tıp Fakültesi –  Probiyotik Prebiyotik Derneği kurucu üyesi – Amerika Birleşik Devletleri New York SUNY Üniversitesi –  İstinye Üniversitesi Mikrobiyota çalışmaları –  Mikrobiyota kitabı editörü</span>
					</div>
				</a>
			</div>
			<div class="col-12 col-md-6 col-lg-4 col-xl-4 team__col">
				<a href="{{route('prof-dr-tarkan-karakan')}}">
					<div class="team__member">
						<img class="team__member__avatar" src="{{ url('/') }}/new/img/tarkankarakan.webp" />
						<h2 class="team__member__name">Prof. Dr. Tarkan Karakan</h2>
						<span class="team__member__job team__member__job--2">Gastroenteroloji Uzmanı</span>
						<span class="team__member__job">Hacettepe Üniversitesi Tıp Fakültesi – Johns Hopkins Üniversitesi – Probiyotik Prebiyotik Derneği Başkanı – Gastrointestinal Endoskopi Derneği yönetim kurulu üyesi – WGO Probiotics Guideline Komitesi – TÜBA Beslenme çalışma grubu üyesi – Tarım Bakanlığı Yeni Gıda Destekleri ve Güvenliği Komitesi</span>
					</div>
				</a>
			</div>
			<div class="col-12 col-md-6 col-lg-4 col-xl-4 team__col">
				<a href="{{route('prof-dr-halil-coskun')}}">
					<div class="team__member">
						<img class="team__member__avatar" src="{{ url('/') }}/new/img/halil-coskun.webp" />
						<h2 class="team__member__name">Prof. Dr. Halil Coşkun</h2>
						<span class="team__member__job team__member__job--2">Genel Cerrahi Uzmanı</span>
						<span class="team__member__job">Ankara Üniversitesi Tıp Fakültesi – İstanbul Üniversitesi – ABD Cleveland Tıp Merkezi Bariatrik Enstitüsü - Laparoskopik ve Robotik Obezite – Metabolik Cerrahi –  ABD Weill Cornell Tıp Fakültesi – Bezmi Alem Üniversitesi Genel Cerrahi Anabilim Dalı Kurucu Öğretim Üyesi</span>
					</div>
				</a>
			</div>
		</div>
	</div>
</section>
<section class="discover">
	<div class="container">
		<div class="row">
			<div class="col-md-5">
				<span class="discover__title">MİKROBİYOMUNU KEŞFET!</span>
				<p class="discover__desc">
					Artık hangi besinlerin iyi veya kötü olduğunu bir kenara bırakıp senin için en iyi olana odaklanmanın zamanı geldi.
					<br/><br/>
					ENBIOSIS ile mikrobiyomunu keşfet, beslenmeni yönet, bedenine hükmet!
				</p>

			</div>
		</div>
		<a class="link-btn link-btn--orange" href="{{url('/checkout')}}"
		onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'button', Label: '{{slug('satin_al')}}' })">
		ŞİMDİ DENE</a>
		{{-- <a class="link-btn link-btn--orange" href="{{url('/checkout')}}">ŞİMDİ DENE!</a> --}}
	</div>
</section>
@endsection
