@extends('layouts.public')

@section('pageTitle','Mikrobiyota İle Sağlıklı Yaşam | Enbiosis')
@section('pageDescription','ENBIOSIS Mikrobiyom Analizi ile gıda hassasiyetini yönet')


@section('content')
<section class="header bagirsak__1">
	<div class="container">
		<div class="row">
			<div class="col-7 col-md-7 col-lg-6 col-xl-6">
				<span class="header__slogan bagirsak__2"><b>GLÜTEN HASSASİYETİN Mİ VAR, YOKSA HASSAS OLAN MİKROBİYOMUN MU?
                    </b></span>
				<p class="header__desc">
					Bağırsaklarında yer alan mikroorganizmaları onlara en uygun besinlerle yönetmek, glüten ile ilişkili hastalıkların tedavisinde sana yol gösterir. ENBIOSIS ile mikrobiyom dünyanı keşfedebilir ve tüketmen gereken besinleri öğrenerek sahip olduğun semptomları azaltabilirsin!
                </p>
                <a class="link-btn link-btn--orange" href="{{url('/checkout')}}"
				onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'button', Label: '{{slug('landing_page')}}' })">
				Hemen Satın Al</a>
			</div>
		</div>
	</div>
</section>
<section class="header header--mobile" style="background-image: url(/new/img/bagirsak-mobile.webp);background-color:white;padding-bottom:0;">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-12 col-lg-12 col-xl-12" style="    text-align: center;">
				<h1 class="header__slogan"><b>GLÜTEN HASSASİYETİN Mİ VAR, YOKSA HASSAS OLAN MİKROBİYOMUN MU?</b></h1>
				<p class="header__desc">
					Bağırsaklarında yer alan mikroorganizmaları onlara en uygun besinlerle yönetmek, glüten ile ilişkili hastalıkların tedavisinde sana yol gösterir. ENBIOSIS ile mikrobiyom dünyanı keşfedebilir ve tüketmen gereken besinleri öğrenerek sahip olduğun semptomları azaltabilirsin!
                </p>
                <a class="link-btn link-btn--orange" href="{{url('/checkout')}}"
				onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'button', Label: '{{slug('landing_page')}}' })">
				Hemen Satın Al</a>
			</div>
		</div>
	</div>
</section>

<section class="bg-none">
	<div class="container">
		<div class="row">
			<div class="col-12">
				@include('include.video.kesfet', ['id' => 'kesfet'])
			</div>
		</div>
	</div>
</section>

<section class="mictobiome-balance" >
	<section class="microbiome-analysis microbiome-analysis--2 bagisiklik__3 bg-none">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
                    <h2 class="microbiome-analysis__title bagisiklik__t" style="font-size:36px;">GLÜTEN HASSASİYETİ İLE SAVAŞIRKEN <br><b>BESİN ÇEŞİTLİLİĞİNİN AZALMASINDAN SIKILDIN MI?</b></h2>
					<p class="microbiome-analysis__desc" style="font-size:17px;">
						Sen de buğday, arpa, çavdar gibi tahıllarda bulunan glüteni tükettiğinde şişkinlik, gaz ve diyare gibi problem yaşıyor musun? Çözümü glütensiz diyetlerde aramış hatta marketlerin glütensiz reyonlarında sıkışmış olabilirsin. İşte o zaman bağırsak mikrobiyomunun otoimmün rahatsızlıklardaki rolünü keşfetmenin zamanı geldi!
                    </p>
                    <p class="microbiome-analysis__desc" style="font-size:17px;">
                        Vücudundaki trilyonlarca bakterinin oluşturduğu ekosisteme mikrobiyom denir. Bu bakterilerden bazıları gıda hassasiyetleri ile yakından ilişkilidir. Eğer çölyak hastalığına veya glüten hassasiyetine sahipsen  mikrobiyomunda yer alan bakterilerin dengesi zamanla bozulur. Sağlığın için faydalı bakterilerin sayısı azalırken zararlı bakterilerin sayısı giderek artar. Mikrobiyomunun dengesi bozulduğunda ise vücudunun besinlere verdiği tepki ve yaşadığın semptomların şiddeti artar. ENBIOSIS ile mikrobiyomundaki bakterilerin durumunu öğrenebilir ve mikrobiyom dengeni sağlayıp yaşadığın semptomları azaltmak için hangi besinleri tüketmen gerektiğini keşfedebilirsin!
                    </p>
                    <p class="microbiome-analysis__desc">
                        <a class="link-btn link-btn--orange" href="{{url('/iletisim')}}"
                        onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'button', Label: '{{slug('landing_page')}}' })">
                        Detaylı Bilgi</a>
                    </p>
				</div>
			</div>
		</div>
	</section>
</section>
<section class="mictobiome-balance">
	<section class="microbiome-analysis microbiome-analysis--2 bagisiklik__4 bg-none">
		<div class="container">
            <div class="row" style="justify-content:flex-end;">
				<div class="col-12 text-center">
					<h2 class="microbiome-analysis__title">VÜCUDUNDAKİ KIRMIZI ALARM: <br><b>MİKROBİYOM ÇEŞİTLİLİĞİN AZALIYOR MU?</b></h2>
					<p class="microbiome-analysis__desc">
                        Gereksiz antibiyotik kullanımının artması, değişen yaşam koşulları ve gıda çeşitliliğinin azalması gün geçtikçe sahip olduğun mikrobiyal çeşitliliğin de azalmasına neden olur. Bağırsaklarında sindirim ve bağışıklık faaliyetlerinden sorumlu bu mikroorganizmaların azalması ile tükettiğin bazı besinlere karşı vücudun kırmızı alarm vermeye başlar. Mikrobiyom dengesinin sağlanması, vücudunda besinlere karşı oluşan bağışıklık yanıtının düzenlenmesine ve yaşadığın semptomların azalmasına destek olur.  
					</p>
				</div>
			</div>
		</div>
	</section>
</section>
<section class="nutrition-guide">
	<div class="container">
		<div class="row nutrition-guide__title-row">
			<div class="col-md-12">
				<span class="nutrition-guide__title bagirsak__8"><b>MİKROBİYOMUNA GÖRE BESLEN, VÜCUDUNUN GIDALARA VERDİĞİ TEPKİYİ KONTROL ET </b></span>
				<p class="nutrition-guide__desc">
                    ENBIOSIS ile 3 basit adımda glüten ile ilişkili semptomlarını azaltman için sana özel tüketmen gereken besinleri öğrenebilirsin. Sahip olduğumuz bilim ve teknoloji ile mikrobiyomunu analiz ediyor, dengeli bir mikrobiyom profiline ulaşman için tüketmen gereken besinleri sana sunuyoruz. Sağlıklı bir mikrobiyom; düzenli bir sindirim ve güçlü bir bağışıklık demektir. Vücudunun gıdalara karşı verdiği tepkileri kontrol edebilmek için şimdi mikrobiyomuna göre beslenmenin zamanı!
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 triple-box__box">
				<img src="{{url('/')}}/new/img/triple-icon1.webp" />
				<span>
					Mikrobiyom Analizi İle Kendini Tanı
				</span>
			</div>
			<div class="col-md-4 triple-box__box">
				<img src="{{url('/')}}/new/img/triple-icon2.webp" />
				<span>
					Kişiselleştirilmiş Beslenme Önerilerini ENBIOSIS Diyetisyeni İle Takip Et!
				</span>
			</div>
			<div class="col-md-4 triple-box__box">
				<img src="{{url('/')}}/new/img/triple-icon4.webp" />
				<span>
					Dengeli Mikrobiyom Profiline Ulaş
				</span>
			</div>
		</div>
	</div>
</section>
@include('include.banner', ['mt' => 'm-0'])
<section class="report-item bagirsak__10">
    <div class="container">
        <div class="row mb-2 pb-2">
            <div class="text-center col-12">
                <h2 class="triple-box__title">ENBIOSIS MİKROBİYOM ANALİZ RAPORU İLE BAĞIRSAK SKORLARINI ÖĞREN!</h2>
                </h2>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-6 report-item">
                <div class="report-item__item">
                    <div class="report-item__icon">
                        <img src="/new/landing1/icon5.webp">
                    </div>
                    <div class="report-item__content">
                        <span>Glüten Hassasiyeti</span>
                        <span>
                            Glüten Hassasiyeti Skoru ile mikrobiyomuna dayalı glüten hassasiyet eğilimini öğrenebilirsin.
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-6 report-item">
                <div class="report-item__item">
                    <div class="report-item__icon">
                        <img src="/new/landing1/icon4.webp">
                    </div>
                    <div class="report-item__content">
                        <span>Laktoz Hassasiyeti</span>
                        <span>
                            Laktoz Hassasiyeti Skoru ile mikrobiyomuna dayalı laktoz hassasiyet eğilimini öğrenebilirsin.
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-6 report-item">
                <div class="report-item__item">
                    <div class="report-item__icon">
                        <img src="/new/landing1/icon11.webp">
                    </div>
                    <div class="report-item__content">
                        <span>Otoimmünite İndeksi</span>
                        <span>
                            Otoimmünite İndeksi Skoru ile otoimmün hastalıklara yönelik potansiyel riskini öğrenebilirsin.
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-6 report-item">
                <div class="report-item__item">
                    <div class="report-item__icon">
                        <img src="/new/landing1/icon10.webp">
                    </div>
                    <div class="report-item__content">
                        <span>Antibiyotik Hasarı</span>
                        <span>
                            Antibiyotik Hasarı Skoru ile mikrobiyomunda antibiyotik tüketimine bağlı olarak meydana gelen hasarı öğrenebilirsin.  
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-6 report-item">
                <div class="report-item__item">
                    <div class="report-item__icon">
                        <img src="/new/landing1/icon8.webp">
                    </div>
                    <div class="report-item__content">
                        <span>Probiyotik Mikroorganizmalar</span>
                        <span>
                            Probiyotik Mikroorganizmalar Skoru ile mikrobiyomunda bulunan bu bakteri grubunun varlığı ve miktarı hakkında bilgi sahibi olabilirsin.
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-6 report-item">
                <div class="report-item__item">
                    <div class="report-item__icon">
                        <img src="/new/landing1/icon3.webp">
                    </div>
                    <div class="report-item__content">
                        <span>Vitamin Sentezi</span>
                        <span>
                            Vitamin Sentezi Skoru ile vitamin sentezinden sorumlu bakterilerinin durumunu öğrenebilirsin.
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-6 report-item">
                <div class="report-item__item">
                    <div class="report-item__icon">
                        <img src="/new/landing1/icon9.webp">
                    </div>
                    <div class="report-item__content">
                        <span>Bağırsak Hareketliliği</span>
                        <span>
                            Bağırsak Hareketliliği Skoru ile mikrobiyomunun bağırsak hareketliliğin üzerindeki potansiyel etkisini öğrenebilirsin.
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-6 report-item">
                <div class="report-item__item">
                    <div class="report-item__icon">
                        <img src="/new/landing1/icon7.webp">
                    </div>
                    <div class="report-item__content">
                        <span>Yapay Tatlandırıcı Hasarı</span>
                        <span>
                            Yapay Tatlandırıcı Hasarı Skoru ile mikrobiyomunun yapay tatlandırıcı tüketiminden ne ölçüde etkilendiğini öğrenebilirsin.
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-6 report-item">
                <div class="report-item__item">
                    <div class="report-item__icon">
                        <img src="/new/landing1/icon12.webp">
                    </div>
                    <div class="report-item__content">
                        <span>Uyku Kalitesi</span>
                        <span>
                            Uyku Kalitesi Skoru ile mikrobiyomunun kaliteli bir uyku düzeni için ne kadar elverişli olduğunu öğrenebilirsin.
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-6 report-item">
                <div class="report-item__item">
                    <div class="report-item__icon">
                        <img src="/new/landing1/icon6.webp">
                    </div>
                    <div class="report-item__content">
                        <span>Şeker Tüketimi</span>
                        <span>
                            Şeker tüketiminizin mikrobiyomunuzda ne derecede değişime yol açtığının ölçütüdür.
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="analyze-report">
	<div class="container">
		<div class="row analyze-report__besin-skorlari bagisiklik__6">
            <div class="row mb-2 pb-2">
                <div class="text-center col-12">
                    <h2 class="triple-box__title">ENBIOSIS KİŞİSELLEŞTİRİLMİŞ BESLENME REHBERİ İLE SANA EN UYGUN BESİNLERİ ÖĞREN!</h2>
                    <p class="nutrition-guide__desc">
                        Mikrobiyom dengenin bozulmuş olması vücudunun tükettiğin besinlere karşı tepki göstermesine neden olabilir. Besinlere karşı koyduğun yasaklar, beslenme çeşitliliğini azaltabilir ve mikrobiyom sağlığını olumsuz yönde etkileyebilir. Artık hangi besinlerin sana uygun olup olmadığını keşfetmenin ve beslenmeni yönetmenin zamanı geldi!
                    </p>
                </div>
            </div>
			<div class="col-md-12 col-lg-6 col-xl-6 d-flex align-items-center">
				<div class="analyze-report__item">
                    <h3>Şimdi ENBIOSIS Kişiselleştirilmiş Beslenme Rehberine göz at!</h3>
					<div class="analyze-report__food-rating">
						<img src="{{url('/')}}/new/img/iyi-besin.webp" width="100"/>
						<div class="content">
							<h4>İşte Senin Besinlerin!</h4>
							<p>
                                Mikrobiyom dengeni sağlamak için sık tüketmen gereken besinleri ifade eder.
							</p>
						</div>
					</div>
					<div class="analyze-report__food-rating">
						<img src="{{url('/')}}/new/img/orta-besin.webp" width="100"/>
						<div class="content">
							<h4>Seninle Uyum İçinde Olan Besinler!</h4>
							<p>
                                Mikrobiyom dengeni sağlamak için dengeli ve çeşitli şekilde tüketmen gereken besinleri ifade eder.
							</p>
						</div>
					</div>
					<div class="analyze-report__food-rating">
						<img src="{{url('/')}}/new/img/kotu-besin.webp" width="100"/>
						<div class="content">
							<h4>Kaçınman Gereken Besinler!</h4>
							<p>
                                Mikrobiyom dengeni sağlamak için daha az tüketmen gereken besinleri ifade eder. 
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12 col-lg-6 col-xl-6 iphone-wrapper">
				<div class="device device-ipad-pro device-gold">
					<div class="device-frame">
						<iframe class="device-content" src="https://app.enbiosis.com/demo/foods/tr" style="object-fit: initial;width:100%;height:100%;border:0;"></iframe>
					</div>
					<div class="device-stripe"></div>
					<div class="device-header"></div>
					<div class="device-sensors"></div>
					<div class="device-btns"></div>
					<div class="device-power"></div>
				</div>
			</div>
		</div>
		<div class="row d-flex justify-content-center">
			<div class="col-md-12 analyze-report__quote">
				<i>Gıda Hassasiyetinin sebebi mikrobiyomunda gizli olabilir. Sana özel besinler ile mikrobiyomunu dengele, hassasiyetini azalt!</i><br/><br/>
			</div>
		</div>
	</div>
</section>
@include('include.package')
<section class="hprocess">
	<div class="container">
		<div class="row hprocess__howto" style="margin-top:0;">
			<h2 class="hprocess__howtotitle">SÜREÇ NASIL İŞLER?</h2>
            <div class="col-12 mb-3">
				@include('include.video.surec', ['id' => 'surec'])
			</div>
			<div class="col-md-6 col-lg-3 col-xl-3 hprocess__step">
				<div class="hprocess__number">
					<img src="{{ url('/') }}/new/img/icons/surec1.webp" width="50"/>
				</div>
				<h2 class="hprocess__steptitle">ENBIOSIS MİKROBİYOM ANALİZİ SİPARİŞİ</h2>
				<p class="hprocess__stepdesc">
					www.enbiosis.com 'dan sipariş ver, sana gelen kutudaki kod ile web sitesinden kaydını oluştur ve anket sorularını tamamla.
				</p>
			</div>
			<div class="col-md-6 col-lg-3 col-xl-3 hprocess__step">
				<div class="hprocess__number">
					<img src="{{ url('/') }}/new/img/icons/surec2.webp" width="50"/>
				</div>
				<h2 class="hprocess__steptitle">NUMUNE ALIMI</h2>
				<p class="hprocess__stepdesc">
					Kargo ile adresine teslim edilen Mikrobiyom Kitinin içerisinde yer alan adımları takip et ve mikrobiyom analizin için gerekli gaita numuneni kolayca al. Sonrasında numuneni kutuda yer alan adrese ücretsiz bir şekilde gönder.
				</p>
			</div>
			<div class="col-md-6 col-lg-3 col-xl-3 hprocess__step">
				<div class="hprocess__number">
					<img src="{{ url('/') }}/new/img/icons/surec3.webp" width="50"/>
				</div>
				<h2 class="hprocess__steptitle">LABORATUVAR ANALİZİ</h2>
				<p class="hprocess__stepdesc">
					Numunen Türkiye’nin en kapsamlı araştırma merkezi olan Genom ve Kök Hücre Merkezi’nde işleme alınarak 16S rRNA yeni nesil dizileme yöntemi ile dizilenir. Elde edilen veriler yapay zeka algoritmalarımız ile ayrıntılı olarak analiz edilir ve raporlandırılır.
				</p>
			</div>
			<div class="col-md-6 col-lg-3 col-xl-3 hprocess__step">
				<div class="hprocess__number">
					<img src="{{ url('/') }}/new/img/icons/surec4.webp" width="50"/>
				</div>
				<h2 class="hprocess__steptitle">ENBIOSIS KİŞİSELLEŞTİRİLMİŞ BESLENME REHBERİ</h2>
				<p class="hprocess__stepdesc">
					Raporların uzmanlarımız tarafından kontrol edildikten sonra sana ulaştırılır. Besin skorların ENBIOSIS diyetisyenleri tarafından yorumlanır ve diyetisyen takibi ile birlikte sana sunulur.
				</p>
			</div>
		</div>
	</div>
</section>
<section class="discover">
	<div class="container">
		<div class="row">
			<div class="col-md-5">
				<span class="discover__title">MİKROBİYOMUNU KEŞFET, GIDA HASSASİYETİNİ YÖNET!</span>
				<p class="discover__desc">
					Kişiselleştirilmiş Beslenme Rehberi ile sağlıklı ve sürdürülebilir bir yaşam için tüketmen gereken besinleri öğren!
				</p>

			</div>
		</div>
		<a class="link-btn link-btn--orange" href="{{url('/checkout')}}"
		onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'button', Label: '{{slug('satin_al')}}' })">
		SATIN AL</a>
	</div>
</section>
@endsection
