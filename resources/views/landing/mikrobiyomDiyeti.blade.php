@extends('layouts.public')

@section('pageTitle','Mikrobiyota İle Sağlıklı Yaşam | Enbiosis')
@section('pageDescription','Mikrobiyota nedir? Enbiosis mikrobiyota testi ile size uygun sağlıklı beslenme rehberinize hemen kavuşun!')

@section('content')
<section class="header kisisel__1">
	<div class="container">
		<div class="row">
			<div class="col-7 col-md-7 col-lg-6 col-xl-6">
				<span class="header__slogan bagirsak__2"><b>ONLINE DİYETTE YENİ ÇAĞ: ENBIOSIS MİKROBİYOM DİYETİ İLE SAĞLIĞINI YÖNET!</b></span>
				<p class="header__desc">
                    ENBIOSIS sadece sana özel bir diyet sunmak için laboratuvarı evine getirir. Sen de 6 haftalık online diyet yolculuğunda mikrobiyom dünyanı ve mikrobiyomuna özel skorlanmış 240 adet besini keşfet, sağlığını yönet!
                </p>
                <a class="link-btn link-btn--orange" href="{{url('/checkout')}}"
				onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'button', Label: '{{slug('landing_page')}}' })">
				Hemen Satın Al</a>
			</div>
		</div>
	</div>
</section>
<section class="header header--mobile" style="background-image: url(/new/img/kbeslenme-mobile.webp);background-color:white;padding-bottom:0;">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-12 col-lg-12 col-xl-12" style="    text-align: center;">
				<h1 class="header__slogan"><b>ONLINE DİYETTE YENİ ÇAĞ: ENBIOSIS MİKROBİYOM DİYETİ İLE SAĞLIĞINI YÖNET!</b></h1>
				<p class="header__desc">
                    ENBIOSIS sadece sana özel bir diyet sunmak için laboratuvarı evine getirir. 6 haftalık online diyet yolculuğunda mikrobiyom dünyanı ve sana özel skorlanmış 240 adet besini keşfet, sağlığını yönet! 
                </p>
                <a class="link-btn link-btn--orange" href="{{url('/checkout')}}"
				onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'button', Label: '{{slug('landing_page')}}' })">
				Hemen Satın Al</a>
			</div>
		</div>
	</div>
</section>

<section class="bg-none">
	<div class="container">
		<div class="row">
			<div class="col-12">
				@include('include.video.kesfet', ['id' => 'kesfet'])
			</div>
		</div>
	</div>
</section>

<section class="mictobiome-balance" >
	<section class="microbiome-analysis microbiome-analysis--2 bagisiklik__3 ">
		<div class="container">
			<div class="row">
				<div class="col-md-12 mictobiome-balance-mobile">
					<img src="/new/img/about-1-mobile.webp" style="width:100%;height:auto;margin-bottom:70px;"/>
				</div>
				<div class="col-md-12 col-lg-6 col-xl-6">
                    <h2 class="microbiome-analysis__title bagisiklik__t" style="font-size:36px;"><b>MİKROBİYOM  NEDİR?</b></h2>
                    <p class="microbiome-analysis__desc" style="font-size:17px;">
                        Vücudumuz doğduğumuz andan itibaren bize eşlik eden sağlığımız ve yaşam kalitemiz üzerinde tartışılmaz öneme sahip olan trilyonlarca mikroorganizmaya ev sahipliği yapmaktadır. Bu mikroskobik canlılar ile birlikte genetik materyalleri, çevreyle etkileşimleri ve yaşadıkları habitatın bütünü mikrobiyomu oluşturur. Mikrobiyomun çok büyük kısmı bakterilerden oluşmaktadır. Bu sebeple, bakterilerimizin dağılımı ve çeşitliliği mikrobiyom dengesinde kritik bir öneme sahiptir.
                    </p>
				</div>
			</div>
        </div>
    </section>
</section>
<section class="mictobiome-balance">
	<section class="microbiome-analysis microbiome-analysis--2 bagirsak__11">
		<div class="container">
            <div class="row" style="justify-content:flex-end;">
				<div class="col-md-12 mictobiome-balance-mobile">
					<img src="/new/landing1/bagirsak3-mobile.webp" style="width:100%;height:auto;margin-bottom:70px;"/>
				</div>
				<div class="col-md-12 col-lg-6 col-xl-6">
					<h2 class="microbiome-analysis__title mt-lg-5"><b>SANA EN UYGUN DİYETİ MİKROBİYOMUN SÖYLER!</b></h2>
					<p class="microbiome-analysis__desc">
                        Mikrobiyom dünyası insan genomundan farklı olarak parmak izleri gibi sadece kişiye özeldir. Mikrobiyomun vücudunun besinlere verdiği tepkide önemli rol oynar. Bu sebeple bir kişiye faydalı olan bir besin başka bir kişi için faydalı olmayabilir. ENBIOSIS ile online diyet hizmeti eşliğinde mikrobiyomuna göre beslen, farkı hisset!
					</p>
				</div>
			</div>
		</div>
	</section>
</section>
<section class="mictobiome-balance" >
	
    <section class="microbiome-analysis microbiome-analysis--2 bagisiklik__6">
		<div class="container">
            <div class="row" >
				<div class="col-md-12 mictobiome-balance-mobile">
					<img src="/new/img/microbiome-analysis-bg3-mobile.webp" style="width:100%;height:auto;margin-bottom:70px;"/>
				</div>
				<div class="col-md-12 col-lg-6 col-xl-6">
					<h2 class="microbiome-analysis__title"><b>ENBIOSIS MİKROBİYOMA ÖZEL ONLINE DİYET SERVİSİ </b></h2>
					<p class="microbiome-analysis__desc">
                        ENBIOSIS sana en uygun diyeti bulmak için öncelikle dışkı numunenden alınan mikrobiyom örneğini analiz eder ve mikrobiyomunu sağlıklı hale getirmen için tüketmen gereken besinleri sunar. ENBIOSIS kiti ile mikrobiyom analizin için gerekli olan numuneyi herhangi bir hastaneye, kliniğe gitmene gerek kalmadan evinde kolayca alırsın.  Sonuçlarının çıkmasının ardından 6 hafta boyunca ENBIOSIS diyetisyenleri tarafından takip edilir ve sana özel hazırlanmış mikrobiyom diyeti ile sonuca ulaşırsın! 
					</p>
				</div>
			</div>
		</div>
	</section>
    <div class="container my-lg-3 py-lg-3 my-2 py-2">
        <div class="row my-2 py-2">
            <div class="text-center col-12">
                <h4 style="font-size:30px" class="triple-box__title">3 Kolay Adım İle ENBIOSIS Online Diyete Başla</h4>
            </div>
        </div>
    </div>
    <div class="container pricing my-3">
        <div class="row">
            <div class="col-lg-4">
                <div class="card  mb-lg-0"  style="height: 97%;">
                    <div class="card-body">
                        <div class="step">1</div>
                        <ul class="fa-ul text-center" style="margin-top: 1em;margin-left:0">
                            <li>Evine gönderilen kit yardımı ile gaita numuneni kolayca al.</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 mt-lg-0 mt-5">
                <div class="card mb-lg-0"  style="height: 97%;">
                    <div class="card-body">
                       <div class="step">2</div>
                        <ul class="fa-ul text-center" style="margin-top: 1em;margin-left:0">
                            <li>Numunen ENBIOSIS tarafından analiz edilir ve mikrobiyom sağlığın için tüketmen gereken besinler skorlanır.</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 mt-lg-0 mt-5">
                <div class="card  mb-lg-0"  style="height: 97%;">
                    <div class="card-body">
                        <div class="step">3</div>
                        <ul class="fa-ul text-center" style="margin-top: 1em;margin-left:0">
                            <li>ENBIOSIS diyetisyenleri ile online görüşmelerin başlar, sana özel besinler ile mikrobiyom diyetin oluşturulur.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('include.banner')
<section class="nutrition-guide pb-5">
	<div class="container">
		<div class="row nutrition-guide__title-row">
			<div class="col-md-12">
				<span class="nutrition-guide__title bagirsak__8"><b>ENBIOSIS MİKROBİYOM DİYETİ</b></span>
				<p class="nutrition-guide__desc">
                    Sağlıklı bir mikrobiyoma sahip olabilmen için sana özel besinlerle hazırlanmış mikrobiyom diyeti ve diyetisyen takibi ile sahip olabileceklerin: 
				</p>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-md-6 report-item">
                <div class="report-item__item">
                    <div class="report-__tempitem__icon step step-icon">
                        @include('include.svgs.kilo')
                    </div>
                    <div class="report-item__content report-item__content_width">
                        <span> Kilo Kontrolü </span>
                        <span>
                            Mikrobiyom metabolik regülasyonda koruyucu işlevlere sahiptir. Dengeli ve sağlıklı bir mikrobiyom verilen kiloların kalıcı olmasını sağlar. 
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-6 report-item">
                <div class="report-item__item">
                    <div class="report-__tempitem__icon step step-icon">
                        @include('include.svgs.bagisiklik')
                    </div>
                    <div class="report-item__content report-item__content_width">
                        <span> Güçlü Bağışıklık Sistemi </span>
                        <span>
                            Mikrobiyom hem immün savunmayı uyaran metabolitler salgılayarak hem de bağırsak bariyerini koruyarak vücut savunmasında immün sistemi etkiler. Sağlıklı bir mikrobiyom güçlü bir bağışıklığa sahip olmanı sağlar.
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-6 report-item">
                <div class="report-item__item">
                    <div class="report-__tempitem__icon step step-icon">
                        @include('include.svgs.sindirim')
                    </div>
                    <div class="report-item__content report-item__content_width">
                        <span>Sağlıklı Sindirim Sistemi</span>
                        <span>
                            Mikrobiyom patojen bakterilerin bağırsak çeperine kolonize olmasını engeller, sindirim sistemi hastalıklarına karşı koruyucu rol oynar. Sağlıklı bir mikrobiyom düzenli çalışan sindirim sistemi demektir.
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-6 report-item">
                <div class="report-item__item">
                    <div class="report-__tempitem__icon step step-icon">
                        @include('include.svgs.cilt')
                    </div>
                    <div class="report-item__content report-item__content_width">
                        <span> Cilt Sağlığı</span>
                        <span>
                            Mikrobiyom ürettiği metabolitler ile cilt sağlığının korunmasına ve cilt problerinin azaltılmasına yardımcı olur. Sağlıklı bir mikrobiyom pürüzsüz bir cilt için elzemdir. 
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-6 report-item">
                <div class="report-item__item">
                    <div class="report-__tempitem__icon step step-icon">
                        @include('include.svgs.uyku')
                    </div>
                    <div class="report-item__content report-item__content_width">
                        <span>Uyku Kalitesi</span>
                        <span>
                            Mikrobiyom tarafından sentezlenen veya dolaylı olarak sentezlenmesi sağlanan metabolitler ile uyku kalitesi yakından ilişkilidir. Sağlıklı bir mikrobiyom, uyku kalitesinin arttırılmasını sağlar.
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-6 report-item">
                <div class="report-item__item">
                    <div class="report-__tempitem__icon step step-icon">
                        @include('include.svgs.mental')
                        
                    </div>
                    <div class="report-item__content report-item__content_width">
                        <span>Mental Sağlık</span>
                        <span style="font-weight: 400;font-size:12px">
                            Mikrobiyom ve beyin arasındaki çift yönlü bir iletişim ağı mevcuttur. Mikrobiyomundaki bakteriler ürettikleri metabolitler ile duygularını ve düşüncelerini etkileyebilir. Sağlıklı bir mikrobiyom mental sağlık için önemlidir. 
                        </span>
                    </div>
                </div>
            </div>
		</div>
	</div>
</section>
@include('include.package')
<section class="bilimkurulu">
	<span class="bilimkurulu__title">ENBIOSIS Bilim Kurulu</span>
	<div class="container">
		<div class="row team__row justify-content-center">
			{{-- <div class="col-12 col-md-6 col-lg-3 col-xl-3 team__col">
				<a href="{{route('prof-dr-hakan-alagozlu')}}">
					<div class="team__member">
						<img class="team__member__avatar" src="{{ url('/') }}/new/img/hakanalagozlu.webp" />
						<h2 class="team__member__name">Prof. Dr. Hakan Alagözlü</h2>
						<span class="team__member__job team__member__job--2">Gastroenteroloji Uzmanı</span>
						<span class="team__member__job">Gazi Üniversitesi Tıp Fakültesi –  Probiyotik Prebiyotik Derneği kurucu üyesi – Amerika Birleşik Devletleri New York SUNY Üniversitesi –  İstinye Üniversitesi Mikrobiyota çalışmaları –  Mikrobiyota kitabı editörü</span>
					</div>
				</a>
            </div> --}}
            <div class="col-12 col-md-6 col-lg-4 col-xl-4 team__col">
				<a href="{{route('prof-dr-hakan-alagozlu')}}">
					<div class="team__member">
						<img class="team__member__avatar" src="{{ url('/') }}/new/img/hakanalagozlu.webp" />
						<h2 class="team__member__name">Prof. Dr. Hakan Alagözlü</h2>
						<span class="team__member__job team__member__job--2">Gastroenteroloji Uzmanı</span>
						<span class="team__member__job">Gazi Üniversitesi Tıp Fakültesi –  Probiyotik Prebiyotik Derneği kurucu üyesi – Amerika Birleşik Devletleri New York SUNY Üniversitesi –  İstinye Üniversitesi Mikrobiyota çalışmaları –  Mikrobiyota kitabı editörü</span>
					</div>
				</a>
			</div>
			<div class="col-12 col-md-6 col-lg-4 col-xl-4 team__col">
				<a href="{{route('prof-dr-tarkan-karakan')}}">
					<div class="team__member">
						<img class="team__member__avatar" src="{{ url('/') }}/new/img/tarkankarakan.webp" />
						<h2 class="team__member__name">Prof. Dr. Tarkan Karakan</h2>
						<span class="team__member__job team__member__job--2">Gastroenteroloji Uzmanı</span>
						<span class="team__member__job">Hacettepe Üniversitesi Tıp Fakültesi – Johns Hopkins Üniversitesi – Probiyotik Prebiyotik Derneği Başkanı – Gastrointestinal Endoskopi Derneği yönetim kurulu üyesi – WGO Probiotics Guideline Komitesi – TÜBA Beslenme çalışma grubu üyesi – Tarım Bakanlığı Yeni Gıda Destekleri ve Güvenliği Komitesi</span>
					</div>
				</a>
			</div>
			<div class="col-12 col-md-6 col-lg-4 col-xl-4 team__col">
				<a href="{{route('prof-dr-halil-coskun')}}">
					<div class="team__member">
						<img class="team__member__avatar" src="{{ url('/') }}/new/img/halil-coskun.webp" />
						<h2 class="team__member__name">Prof. Dr. Halil Coşkun</h2>
						<span class="team__member__job team__member__job--2">Genel Cerrahi Uzmanı</span>
						<span class="team__member__job">Ankara Üniversitesi Tıp Fakültesi – İstanbul Üniversitesi – ABD Cleveland Tıp Merkezi Bariatrik Enstitüsü - Laparoskopik ve Robotik Obezite – Metabolik Cerrahi –  ABD Weill Cornell Tıp Fakültesi – Bezmi Alem Üniversitesi Genel Cerrahi Anabilim Dalı Kurucu Öğretim Üyesi</span>
					</div>
				</a>
			</div>
		</div>
	</div>
</section>
<section class="discover">
	<div class="container">
		<div class="row">
			<div class="col-md-5">
				<span class="discover__title">ENBIOSIS İLE ONLINE TAKİPLİ MİKROBİYOM DİYETİNİ KEŞFET</span>
				<p class="discover__desc">
                    Mikrobiyom analizi ile sana en uygun besinleri öğren, 6 haftalık diyetisyen desteği ile sağlığını yönet!
				</p>

			</div>
        </div>
        <a class="link-btn link-btn--orange" href="{{url('/checkout')}}"
		onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'button', Label: '{{slug('landing_page')}}' })">
		SATIN AL</a>
	</div>
</section>
@endsection
