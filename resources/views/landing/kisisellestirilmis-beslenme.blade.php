@extends('layouts.public')

@section('pageTitle','Mikrobiyota İle Sağlıklı Yaşam | Enbiosis')
@section('pageDescription','ENBIOSIS ile sana özel besinleri keşfet')


@section('content')
<section class="header kisisel__1">
	<div class="container">
		<div class="row">
			<div class="col-7 col-md-7 col-lg-6 col-xl-6">
				<span class="header__slogan kisisel__2"><b>SÜRDÜRÜLEBİLİR VE SAĞLIKLI YAŞAMIN ANAHTARI MİKROBİYOMUNDA GİZLİ</b></span>
				<p class="header__desc">
					Mikrobiyomun kimlik kartın gibi sadece sana özgüdür. ENBIOSIS Kişiselleştirilmiş Beslenme Rehberi ile mikrobiyom dengeni sağlayabilir ve sağlıklı bir hayatın sırrını keşfedebilirsin.
				</p>
				<a class="link-btn link-btn--orange" href="{{url('/checkout')}}"
				onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'button', Label: '{{slug('landing_page')}}' })">
				Hemen Satın Al</a>
				{{-- <a class="link-btn link-btn--orange" href="{{url('/iletisim')}}">Bize Ulaşın</a> --}}
			</div>
		</div>
	</div>
</section>
<section class="header header--mobile" style="background-image: url(/new/img/kbeslenme-mobile.webp);background-color:white;padding-bottom:0;">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-12 col-lg-12 col-xl-12" style="    text-align: center;">
				<h1 class="header__slogan"><b>SÜRDÜRÜLEBİLİR VE SAĞLIKLI YAŞAMIN ANAHTARI MİKROBİYOMUNDA GİZLİ</b></h1>
				<p class="header__desc">
					Mikrobiyomun kimlik kartın gibi sadece sana özgüdür. ENBIOSIS Kişiselleştirilmiş Beslenme Rehberi ile mikrobiyom dengeni sağlayabilir ve sağlıklı bir hayatın sırrını keşfedebilirsin.
				</p>
				<a class="link-btn link-btn--orange" href="{{url('/checkout')}}"
				onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'button', Label: '{{slug('landing_page')}}' })">
				Hemen Satın Al</a>
			</div>
		</div>
	</div>
</section>

<section class="bg-none">
	<div class="container">
		<div class="row">
			<div class="col-12">
				@include('include.video.kesfet', ['id' => 'kesfet'])
			</div>
		</div>
	</div>
</section>

<section class="mictobiome-balance" >
	<section class="microbiome-analysis microbiome-analysis--2 kisisel__3" style="">
		<div class="container">

			<div class="row" style="justify-content:flex-end;">
				<div class="col-md-12 mictobiome-balance-mobile">
					<img src="/new/landing1/microbiome_img-mobile.webp" style="width:100%;height:auto;margin-bottom:70px;"/>
				</div>
				<div class="col-md-12 col-lg-5 col-xl-5">
					<h2 class="microbiome-analysis__title bagisiklik__t"  style="font-size:28px;"><b>BEDENİNDEKİ EKOSİSTEM: MİKROBİYOM</b></h2>
					<p class="microbiome-analysis__desc">
						İyi bir uyku kalitesi, güçlü bir bağışıklık sistemi, sağlıklı bir cilt, huzurlu bir bağırsak ve kilo kontrolü… Dengeli bir mikrobiyom profili ile bunların hepsine sahip olabilirsin! Bağırsaklarımız trilyonlarca bakteriye ev sahipliği yapmaktadır. Bu mikroskobik canlılar, onların genetik materyalleri, genom ürünleri ve çevreyle etkileşimleri bir bütün olarak mikrobiyomu oluşturur. <br><br>
						Çeşitliliği yüksek ve dengeli bir mikrobiyom, metabolik sorunlar, otoimmün hastalıklar, sindirim sistemiyle ilişkilendirilmiş hastalıklar ve mental sorunlar başta olmak üzere birçok hastalığa karşı bizi korur, fizyolojik ve psikolojik iyileşmeye katkı sağlayarak mevcut yaşam kalitemizi arttırır. ENBIOSIS Mikrobiyom Analizi ve Kişiselleştirilmiş Beslenme Rehberi ile tüm bunlara ulaşmak artık çok kolay! 
					</p>
				</div>
			</div>
		</div>
	</section>
</section>

<section class="microbiome-texts kisise__4">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="microbiome-texts__text">
					<h2>
						HEPİMİZ BİBİRİMİZDEN FARKLIYSAK AYNI <b style="display: inline-block">BESLENME ÖNERİLERİNİ</b> UYGULAYABİLİR MİYİZ?
					</h2>
				</div>

				<div class="microbiome-texts__text">
					<div class="row microbiome-texts__imgrow">
						<div class="col-md-7">
							<p>
								Mikrobiyom dünyası insan genomundan farklı olarak parmak izleri gibi sadece kişiye özeldir. Mikrobiyomun vücudunun besinlere verdiği tepkide önemli rol oynar. Bu sebeple bir kişiye faydalı olan bir besin başka bir kişi için faydalı olmayabilir. ENBIOSIS Mikrobiyom Analiz Raporu vücudunun hangi besinlere ihtiyaç duyduğunu öğrenmenin ilk adımıdır.  
							</p>
							<p>
								ENBIOSIS Kişiselleştirilmiş Beslenme Rehberi ise bağırsak mikrobiyomunu homojen bir zenginliğe ve çeşitliliğe ulaştırman için daha sık veya daha az tüketmen gereken besinleri sana sunar. Ayrıca sürdürülebilir ve sağlıklı bir yaşam için yol gösterir. Bu sayede sana en uygun besinleri öğrenerek beslenmeni yönetebilirsin. 
							</p>
						</div>
						<div class="col-md-12" style="margin-top:20px;">
							<a class="link-btn link-btn--border" href="{{url('/')}}/Microbiome_Demo_TR.pdf">MİKROBİYOM ANALİZ RAPORU</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="analyze-report kisisel__6">
	<div class="container">
		<div class="row analyze-report__besin-skorlari kisisel__8">
			<div class="col-md-12 col-lg-6 col-xl-6 d-flex align-items-center">
				<div class="analyze-report__item">
					<h3>Besin Skorları:</h3>
					<span>Mikrobiyom sağlığı büyük bir oranda tükettiğin besinlerden etkilenmektedir. Mikrobiyom profiline göre sana özel hazırlanmış besin skorları ile sağlıklı bir mikrobiyom için tüketmen gereken besinleri keşfet!</span>

					<div class="analyze-report__food-rating">
						<img src="{{url('/')}}/new/img/iyi-besin.webp" width="100"/>
						<div class="content">
							<h4>İşte Senin Besinlerin!</h4>
							<p>
								Sık tüketmen gereken besinleri ifade eder.
							</p>
						</div>
					</div>
					<div class="analyze-report__food-rating">
						<img src="{{url('/')}}/new/img/orta-besin.webp" width="100"/>
						<div class="content">
							<h4>Seninle Uyum İçinde Olan Besinler!</h4>
							<p>
								Dengeli ve çeşitli şekilde tüketmen gereken besinleri ifade eder.
							</p>
						</div>
					</div>
					<div class="analyze-report__food-rating">
						<img src="{{url('/')}}/new/img/kotu-besin.webp" width="100"/>
						<div class="content">
							<h4>Kaçınman Gereken Besinler!</h4>
							<p>
								Daha az tüketmen gereken besinleri ifade eder.
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12 col-lg-6 col-xl-6 iphone-wrapper">
				<div class="device device-ipad-pro device-gold">
					<div class="device-frame">
						<iframe class="device-content" src="https://app.enbiosis.com/demo/foods/tr" style="object-fit: initial;width:100%;height:100%;border:0;"></iframe>
					</div>
					<div class="device-stripe"></div>
					<div class="device-header"></div>
					<div class="device-sensors"></div>
					<div class="device-btns"></div>
					<div class="device-power"></div>
				</div>
			</div>
		</div>

	</div>
</section>
<section class="mictobiome-balance" style="display:none;">
	<section class="microbiome-analysis microbiome-analysis--2" style="background-position-x:right;background-image:url(/new/landing1/landing_page_microbiome_img2.webp);">
		<div class="container">
			<div class="row" style="justify-content:flex-end;">
				<div class="col-md-12 mictobiome-balance-mobile">
					<img src="/new/img/microbiome-analysis-bg3-mobile.webp" style="width:100%;height:auto;margin-bottom:70px;"/>
				</div>
				<div class="col-md-12 col-lg-6 col-xl-6">
					<h2 class="microbiome-analysis__title" style="text-align:right;"><b>MİKROBİYOMA</b> UYGUN BESLENME</h2>
					<p class="microbiome-analysis__desc" style="text-align:right;">
						Sağlığın vücudunun besinleri nasıl işlediği ile doğrudan ilişkilidir. Bunu sağlayansa mikrobiyom özgünlüğündür. Popüler diyetler, genel beslenme önerileri senin için iyi, kötü veya senin için nötr olan besinleri bilmen için yeterli değildir. <br/><br/>
						İnsan genetiği bireyden bireye çok farklılık göstermese de mikrobiyom dünyamız bize özgüdür ve besinlere verdiğimiz tepkilerde önemli rol oynar. İşte bu sebepten, herhangi biri için faydalı olan bir besin sizin için aynı faydayı göstermeyebilir.<br/><br/>
						Mikrobiyomun yediğin her besinden etkilenir ancak bunu kendi yararına çekmek mikrobiyom analizi ile artık senin elinde unutma!<br/><br/><br/>
						<a class="link-btn link-btn--orange" href="{{url('/urunlerimiz')}}">SATIN AL</a>
					</p>

				</div>
			</div>
		</div>
	</section>
</section>
<section class="nutrition-guide">
	<div class="container">
		<div class="row nutrition-guide__title-row">
			<div class="col-md-12">
				<span class="nutrition-guide__title" style="display: flex;width: 100%;justify-content: center;"><b>MİKROBİYOM ANALİZİ<br/>&<br/>KİŞİSELLEŞTİRİLMİŞ BESLENME REHBERİ</b></span>
				<p class="nutrition-guide__desc">
					Mikrobiyomun gibi sen de eşsiz ve özelsin, bu sebeple kişiselleştirilmiş beslenme önerilerini hak ediyorsun. ENBIOSIS ile başlayacağın yolculuğunda sana özel sağlıklı ve sürdürülebilir bir yaşam oluşturmak için mikrobiyom dünyanın kapısını aralayacağız. Yolculuğun için gerekli olan tüm bilgileri uzman diyetisyenlerimiz tarafından elde edeceksin. Kalıcı çözüm için adım atma sırası ise artık sende! Yenilenme yolcuğunuza hazır mısın?
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 triple-box__box">
				<img src="{{url('/')}}/new/img/triple-icon1.webp" />
				<span>
					Mikrobiyom Analizi İle Kendini Tanı
				</span>
			</div>
			<div class="col-md-4 triple-box__box">
				<img src="{{url('/')}}/new/img/triple-icon2.webp" />
				<span>
					Sana Sunduğumuz Kişiselleştirilmiş Beslenme Önerilerini Takip Et

				</span>
			</div>
			<div class="col-md-4 triple-box__box">
				<img src="{{url('/')}}/new/img/triple-icon4.webp" />
				<span>
					Dengeli Mikrobiyom Profiline Ulaş Ve Farkı Hisset!
				</span>
			</div>
		</div>

	</div>
</section>
@include('include.banner', ['mt' => 0])
<div class="gen__quote">
	<div class="row d-flex justify-content-center">
		<div class="col-md-8">
			<i>ENBIOSIS Mikrobiyom Analizi ile mikrobiyomunu keşfet, beslenmeni yönet, sağlına hükmet!</i><br><br>
			<a class="link-btn link-btn--orange" href="{{url('/checkout')}}"
			onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'button', Label: '{{slug('landing_page')}}' })">
			ŞİMDİ KEŞFET</a>
		</div>
	</div>
</div>
{{-- <section class="mictobiome-balance" style="display:none;">
	<section class="microbiome-analysis microbiome-analysis--2 kisisel__7">
		<div class="container">
			<div class="row" style="">
				<div class="col-md-12 mictobiome-balance-mobile">
					<img src="/new/img/microbiome-analysis-bg3-mobile.webp" style="width:100%;height:auto;margin-bottom:70px;"/>
				</div>
				<div class="col-md-12 col-lg-8 col-xl-6">
					<h2 class="microbiome-analysis__title"><b>KİŞİSELLEŞTİRİLMİŞ</b> BESLENMENİN ÖNEMİ</h2>
					<p class="microbiome-analysis__desc">
						Her besin her bireyde aynı metabolik ve fizyolojik etkiyi göstermez. Bu besinlerin içeriğindeki bileşenler mikrobiyomunu oluşturan mikroorganizmaların iletişimi ile doğrudan ilişkilidir. Dolayısıyla fizyolojik ve psikolojik sağlığına en uygun beslenme modeli sana özel olmalıdır. Örneğin bir bireyin yediği elma ona iyi gelirken sende aynı faydayı sağlamayabilir. Bunun için mikrobiyomunu tanımalı ve beslenme yönetimini kendi eline almalısın!
					</p>
					<a class="link-btn link-btn--orange" href="{{url('/urunlerimiz')}}">KENDİNİ TANI</a>
				</div>
			</div>
		</div>
	</section>
</section> --}}
<section class="hprocess hprocess--home">
	<div class="container">
		<div class="row hprocess__howto">
			<span class="hprocess__howtotitle">SÜREÇ NASIL İŞLER?</span>
			<div class="col-12 mb-3">
				@include('include.video.surec', ['id' => 'surec'])
			</div>
			<div class="col-md-6 col-lg-3 col-xl-3 hprocess__step">
				<div class="hprocess__number">
					<img src="{{ url('/') }}/new/img/icons/surec1.webp" width="50"/>
				</div>
				<h2 class="hprocess__steptitle">ENBIOSIS MİKROBİYOM ANALİZİ SİPARİŞİ</h2>
				<p class="hprocess__stepdesc">
					www.enbiosis.com 'dan sipariş ver, sana gelen kutudaki kod ile web sitesinden kaydını oluştur ve anket sorularını tamamla.
				</p>
			</div>
			<div class="col-md-6 col-lg-3 col-xl-3 hprocess__step">
				<div class="hprocess__number">
					<img src="{{ url('/') }}/new/img/icons/surec2.webp" width="50"/>
				</div>
				<h2 class="hprocess__steptitle">NUMUNE ALIMI</h2>
				<p class="hprocess__stepdesc">
					Kargo ile adresine teslim edilen Mikrobiyom Kitinin içerisinde yer alan adımları takip et ve mikrobiyom analizin için gerekli gaita numuneni kolayca al. Sonrasında numuneni kutuda yer alan adrese ücretsiz bir şekilde gönder.
				</p>
			</div>
			<div class="col-md-6 col-lg-3 col-xl-3 hprocess__step">
				<div class="hprocess__number">
					<img src="{{ url('/') }}/new/img/icons/surec3.webp" width="50"/>
				</div>
				<h2 class="hprocess__steptitle">LABORATUVAR ANALİZİ</h2>
				<p class="hprocess__stepdesc">
					Numunen Metagenom laboratuvarımızda işleme alınarak 16S rRNA yeni nesil dizileme yöntemi ile dizilenir. Elde edilen veriler yapay zeka algoritmalarımız ile ayrıntılı olarak analiz edilir ve raporlandırılır.
				</p>
			</div>
			<div class="col-md-6 col-lg-3 col-xl-3 hprocess__step">
				<div class="hprocess__number">
					<img src="{{ url('/') }}/new/img/icons/surec4.webp" width="50"/>
				</div>
				<h2 class="hprocess__steptitle">ENBIOSIS KİŞİSELLEŞTİRİLMİŞ BESLENME REHBERİ</h2>
				<p class="hprocess__stepdesc">
					Raporların uzmanlarımız tarafından kontrol edildikten sonra sana ulaştırılır. Besin skorların ENBIOSIS diyetisyenleri tarafından yorumlanır ve diyetisyen takibi ile birlikte sana sunulur.
				</p>
			</div>
		</div>
	</div>
</section>
<section class="bilimkurulu">
	<span class="bilimkurulu__title">ENBIOSIS Bilim Kurulu</span>
	<div class="container">
		<div class="row team__row justify-content-center">
			<div class="col-12 col-md-6 col-lg-4 col-xl-4 team__col">
				<a href="{{route('prof-dr-hakan-alagozlu')}}">
					<div class="team__member">
						<img class="team__member__avatar" src="{{ url('/') }}/new/img/hakanalagozlu.webp" />
						<h2 class="team__member__name">Prof. Dr. Hakan Alagözlü</h2>
						<span class="team__member__job team__member__job--2">Gastroenteroloji Uzmanı</span>
						<span class="team__member__job">Gazi Üniversitesi Tıp Fakültesi –  Probiyotik Prebiyotik Derneği kurucu üyesi – Amerika Birleşik Devletleri New York SUNY Üniversitesi –  İstinye Üniversitesi Mikrobiyota çalışmaları –  Mikrobiyota kitabı editörü</span>
					</div>
				</a>
			</div>
			<div class="col-12 col-md-6 col-lg-4 col-xl-4 team__col">
				<a href="{{route('prof-dr-tarkan-karakan')}}">
					<div class="team__member">
						<img class="team__member__avatar" src="{{ url('/') }}/new/img/tarkankarakan.webp" />
						<h2 class="team__member__name">Prof. Dr. Tarkan Karakan</h2>
						<span class="team__member__job team__member__job--2">Gastroenteroloji Uzmanı</span>
						<span class="team__member__job">Hacettepe Üniversitesi Tıp Fakültesi – Johns Hopkins Üniversitesi – Probiyotik Prebiyotik Derneği Başkanı – Gastrointestinal Endoskopi Derneği yönetim kurulu üyesi – WGO Probiotics Guideline Komitesi – TÜBA Beslenme çalışma grubu üyesi – Tarım Bakanlığı Yeni Gıda Destekleri ve Güvenliği Komitesi</span>
					</div>
				</a>
			</div>
			<div class="col-12 col-md-6 col-lg-4 col-xl-4 team__col">
				<a href="{{route('prof-dr-halil-coskun')}}">
					<div class="team__member">
						<img class="team__member__avatar" src="{{ url('/') }}/new/img/halil-coskun.webp" />
						<h2 class="team__member__name">Prof. Dr. Halil Coşkun</h2>
						<span class="team__member__job team__member__job--2">Genel Cerrahi Uzmanı</span>
						<span class="team__member__job">Ankara Üniversitesi Tıp Fakültesi – İstanbul Üniversitesi – ABD Cleveland Tıp Merkezi Bariatrik Enstitüsü - Laparoskopik ve Robotik Obezite – Metabolik Cerrahi –  ABD Weill Cornell Tıp Fakültesi – Bezmi Alem Üniversitesi Genel Cerrahi Anabilim Dalı Kurucu Öğretim Üyesi</span>
					</div>
				</a>
			</div>
		</div>
	</div>
</section>
@include('include.basin')
<section class="discover">
	<div class="container">
		<div class="row">
			<div class="col-md-5">
				<span class="discover__title" style="font-size:32px;">ENBIOSIS İLE BESLENMENİ YÖNET</span>
				<p class="discover__desc">
					Artık hangi besinlerin iyi veya kötü olduğunu bir kenara bırakıp senin için en iyi olana odaklanmanın zamanı geldi. ENBIOSIS Kişiselleştirilmiş Beslenme Rehberi ile mikrobiyomunu keşfet, beslenmeni yönet!
				</p>
				<a class="link-btn link-btn--orange" href="{{url('/checkout')}}"
				onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'button', Label: '{{slug('satin_al')}}' })">
				SATIN AL</a>
			</div>
		</div>
	</div>
</section>

@endsection
