@extends('layouts.public')

@section('pageTitle','Mikrobiyota İle Sağlıklı Yaşam | Enbiosis')
@section('pageDescription','Sağlıklı bir sindirim sistemi için ENBIOSIS ile bağırsak bakterilerini yönet')


@section('content')
<section class="header bagirsak__1">
	<div class="container">
		<div class="row">
			<div class="col-7 col-md-7 col-lg-6 col-xl-6">
				<span class="header__slogan bagirsak__2"><b>ENBIOSIS İLE SAĞLIKLI BİR SİNDİRİM SİSTEMİNE ULAŞ!</b></span>
				<p class="header__desc">
					Sağlıklı bir sindirim sisteminin anahtarı dengeli bir mikrobiyomda gizlidir. Sana sunulan kişiselleştirilmiş beslenme önerileri ile mikrobiyomunu dengeleyerek sindirim problemlerinden kurtulabilirsin.
				</p>
				<a class="link-btn link-btn--orange" href="{{url('/checkout')}}"
				onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'button', Label: '{{slug('landing_page')}}' })">
				Hemen Satın Al</a>
			</div>
		</div>
	</div>
</section>
<section class="header header--mobile" style="background-image: url(/new/img/bagirsak-mobile.webp);background-color:white;padding-bottom:0;">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-12 col-lg-12 col-xl-12" style="    text-align: center;">
				<h1 class="header__slogan"><b>ENBIOSIS İLE SAĞLIKLI BİR SİNDİRİM SİSTEMİNE ULAŞ!</b></h1>
				<p class="header__desc">
					Sağlıklı bir sindirim sisteminin anahtarı dengeli bir mikrobiyomda gizlidir. Sana sunulan kişiselleştirilmiş beslenme önerileri ile mikrobiyomunu dengeleyerek sindirim problemlerinden kurtulabilirsin.
				</p>
				<a class="link-btn link-btn--orange" href="{{url('/checkout')}}"
				onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'button', Label: '{{slug('landing_page')}}' })">
				Hemen Satın Al</a>
			</div>
		</div>
	</div>
</section>

<section class="bg-none">
	<div class="container">
		<div class="row">
			<div class="col-12">
				@include('include.video.kesfet', ['id' => 'kesfet'])
			</div>
		</div>
	</div>
</section>

<section class="mictobiome-balance" >
	<section class="microbiome-analysis microbiome-analysis--2 bagirsak__3">
		<div class="container">
			<div class="row">
				<div class="col-md-12 mictobiome-balance-mobile">
					<img src="/new/landing1/bagirsak3-mobile.webp" style="width:100%;height:auto;margin-bottom:70px;"/>
				</div>
				<div class="col-md-12 col-lg-6 col-xl-6">
					<h2 class="microbiome-analysis__title bagirsak__4 bagisiklik__t"><b>BAĞIRSAĞIN</b> SAĞLIĞINLA İLGİLİ NE SÖYLÜYOR?</h2>
					<p class="microbiome-analysis__desc">
						Hazımsızlık, gaz, şişkinlik, ishal ve kabızlık gibi problemlerin ile bağırsağın sana bir şeyler anlatmak istiyor olabilir mi? Uzun süredir sindirim problemleri yaşıyor ve bir türlü çözüm bulamıyorsan belki de aradığın cevap bağırsak mikrobiyomundadır. ENBIOSIS Mikrobiyom Analizi ile bağırsaklarının sesini dinle, sana doğru cevabı söyleyecek! <br><br>
						Bağırsaklarımız trilyonlarca bakteriye ev sahipliği yapan ve bu bakteriler aracılığı ile vücudumuzdaki tüm sistemler ile iletişime geçebilen kompleks bir organdır. Bu kompleks organın içerisinde yaşayan mikroskobik canlılar, onların genom ürünleri ve çevreyle etkileşimleri mikrobiyomu oluşturmaktadır. Mikrobiyomun sağlıklı olmadığı sürece sindirim sisteminin düzenli çalışması beklenemez. Bu sebeple bağırsakların, yaşadığın sindirim problemleri ile mikrobiyomuna dikkat etmen gerektiğini söylüyor olabilir!
					</p>
				</div>
			</div>
		</div>
	</section>
</section>
<section class="mictobiome-balance">
	<section class="microbiome-analysis microbiome-analysis--2 bagirsak__5">
		<div class="container">
			<div class="row" style="justify-content:flex-end;">
				<div class="col-md-12 mictobiome-balance-mobile">
					<img src="/new/landing1/bagirsak7-mobile.webp" style="width:100%;height:auto;margin-bottom:70px;"/>
				</div>
				<div class="col-md-12 col-lg-6 col-xl-6">
					<h2 class="microbiome-analysis__title bagirsak__6 bagisiklik__t">SAĞLIKLI BİR <b>SİNDİRİM SİSTEMİ İÇİN MİKROBİYOMUNA GÜVEN</b></h2>
					<p class="microbiome-analysis__desc bagirsak__7">
						ENBIOSIS Mikrobiyom Analizi ve Kişiselleştirilmiş Beslenme Rehberi <b>Çölyak, IBS, Crohn, Laktoz ve Glüten intoleransı</b> gibi mikrobiyom dengesinin bozulduğu rahatsızlıklarda sana tüketebileceğin en uygun besinlerle yol haritası çizer ve problemlerinin hafifletilmesine yardımcı olur!
					</p>
				</div>
			</div>
		</div>
	</section>
</section>
@include('include.banner')
<section class="nutrition-guide">
	<div class="container">
		<div class="row nutrition-guide__title-row">
			<div class="col-md-12">
				<span class="nutrition-guide__title bagirsak__8"><b>MİKROBİYOM ANALİZİ<br/>&<br/>KİŞİSELLEŞTİRİLMİŞ BESLENME REHBERİ</b></span>
				<p class="nutrition-guide__desc">
					Mikrobiyomun gibi sen de eşsiz ve özelsin, bu sebeple kişiselleştirilmiş beslenme önerilerini hak ediyorsun. ENBIOSIS ile başlayacağın yolculuğunda sana özel sağlıklı ve sürdürülebilir bir yaşam oluşturmak için mikrobiyom dünyanın kapısını aralayacağız. Yolculuğun için gerekli olan tüm bilgileri uzman diyetisyenlerimiz tarafından elde edeceksin. Kalıcı çözüm için adım atma sırası ise artık sende! Yenilenme yolcuğunuza hazır mısın?
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 triple-box__box">
				<img src="{{url('/')}}/new/img/triple-icon1.webp" />
				<span>
					Mikrobiyom Analizi İle Kendini Tanı
				</span>
			</div>
			<div class="col-md-4 triple-box__box">
				<img src="{{url('/')}}/new/img/triple-icon2.webp" />
				<span>
					Sana Sunduğumuz Kişiselleştirilmiş Beslenme Önerilerini Takip Et

				</span>
			</div>
			<div class="col-md-4 triple-box__box">
				<img src="{{url('/')}}/new/img/triple-icon4.webp" />
				<span>
					Dengeli Mikrobiyom Profiline Ulaş Ve Farkı Hisset!
				</span>
			</div>
		</div>
	</div>
</section>

<section class="discover discover--microbiome bagirsak__9">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="discover__title" style="text-align:center;font-size:26px;display: block;">ENBIOSIS SİNDİRİM SİSTEMİ HASTALIKLARINA ÇÖZÜM OLABİLİR Mİ?</h2>
			</div>
			<div class="col-md-12">
				<p class="discover__desc" style="text-align:center;font-family: 'PT Serif', sans-serif;">
					<i>ENBIOSIS Mikrobiyom Analizi ve Kişiselleştirilmiş Beslenme Rehberi <b>Çölyak, IBS, Crohn, Laktoz ve Glüten intoleransı</b> gibi mikrobiyom dengesinin bozulduğu rahatsızlıklarda sana tüketebileceğin en uygun besinlerle yol haritası çizer ve problemlerinin hafifletilmesine yardımcı olur!</i>
				</p>
			</div>
			<div class="col-md-12">
				<h2 class="discover__title" style="text-align:center;font-size:26px;display: block;">ENBIOSIS MİKROBİYOM ANALİZİ İLE BAĞIRSAK SKORLARINI KEŞFET</h2>
			</div>
			<div class="col-md-12">
				<p class="discover__desc" style="text-align:center;">
					Düzenli bir sindirim sistemi, mikrobiyom dengesine ulaşmakla gerçekleşir. Kolayca elde edeceğin mikrobiyom analiz raporu sayesinde bağırsağındaki bakterilerin miktarlarını ve dengesini keşfedebilir, bağırsak sağlık skorlarını öğrenebilirsin. İşte onlardan bazıları:
				</p>
			</div>

		</div>
		<section class="report-item bagirsak__10">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-md-6 report-item">
						<div class="report-item__item">
							<div class="report-item__icon">
								<img src="/new/landing1/icon3.webp">
							</div>
							<div class="report-item__content">
								<span>Vitamin Sentezi</span>
								<span>
									Vitamin Sentezi Skoru ile vitamin sentezinden sorumlu bakterilerinin durumunu öğrenebilirsin.
								</span>
							</div>
						</div>
					</div>
					<div class="col-md-6 report-item">
						<div class="report-item__item">
							<div class="report-item__icon">
								<img src="/new/landing1/icon4.webp">
							</div>
							<div class="report-item__content">
								<span>Laktoz Hassasiyeti</span>
								<span>
									Mikrobiyom kaynaklı laktoza karşı hassasiyet riskinizin ölçütüdür.
								</span>
							</div>
						</div>
					</div>
					<div class="col-md-6 report-item">
						<div class="report-item__item">
							<div class="report-item__icon">
								<img src="/new/landing1/icon5.webp">
							</div>
							<div class="report-item__content">
								<span>Glüten Hassasiyeti </span>
								<span>
									Glüten Hassasiyeti Skoru ile mikrobiyomuna dayalı glüten hassasiyet eğilimini öğrenebilirsin.
								</span>
							</div>
						</div>
					</div>
					<div class="col-md-6 report-item">
						<div class="report-item__item">
							<div class="report-item__icon">
								<img src="/new/landing1/icon6.webp">
							</div>
							<div class="report-item__content">
								<span>Şeker Tüketimi</span>
								<span>
									Şeker Tüketim Skoru ile mikrobiyomunun şeker tüketiminden ne ölçüde etkilendiğini öğrenebilirsin.
								</span>
							</div>
						</div>
					</div>
					<div class="col-md-6 report-item">
						<div class="report-item__item">
							<div class="report-item__icon">
								<img src="/new/landing1/icon7.webp">
							</div>
							<div class="report-item__content">
								<span>Yapay Tatlandırıcı Hasarı</span>
								<span>
									Yapay Tatlandırıcı Hasarı Skoru ile mikrobiyomunun yapay tatlandırıcı tüketiminden ne ölçüde etkilendiğini öğrenebilirsin.
								</span>
							</div>
						</div>
					</div>

					<div class="col-md-6 report-item">
						<div class="report-item__item">
							<div class="report-item__icon">
								<img src="/new/landing1/icon8.webp">
							</div>
							<div class="report-item__content">
								<span>Probiyotik Mikroorganizmalar</span>
								<span>
									Probiyotik Mikroorganizmalar Skoru ile mikrobiyomunda bulunan bu bakteri grubunun varlığı ve miktarı hakkında bilgi sahibi olabilirsin.
								</span>
							</div>
						</div>
					</div>
					<div class="col-md-6 report-item">
						<div class="report-item__item">
							<div class="report-item__icon">
								<img src="/new/landing1/icon9.webp">
							</div>
							<div class="report-item__content">
								<span>Bağırsak Hareketliliği</span>
								<span>
									Bağırsak Hareketliliği Skoru ile mikrobiyomunun bağırsak hareketliliğin üzerindeki potansiyel etkisini öğrenebilirsin.
								</span>
							</div>
						</div>
					</div>
					<div class="col-md-6 report-item">
						<div class="report-item__item">
							<div class="report-item__icon">
								<img src="/new/landing1/icon10.webp">
							</div>
							<div class="report-item__content">
								<span>Antibiyotik Hasarı</span>
								<span>
									Antibiyotik Hasarı Skoru ile mikrobiyomunda antibiyotik tüketimine bağlı olarak meydana gelen hasarı öğrenebilirsin.  
								</span>
							</div>
						</div>
					</div>
					<div class="col-md-6 report-item">
						<div class="report-item__item">
							<div class="report-item__icon">
								<img src="/new/landing1/icon11.webp">
							</div>
							<div class="report-item__content">
								<span>Otoimmünite İndeksi</span>
								<span>
									Otoimmünite İndeksi Skoru ile otoimmün hastalıklara yönelik potansiyel riskini öğrenebilirsin.
								</span>
							</div>
						</div>
					</div>
					<div class="col-md-6 report-item">
						<div class="report-item__item">
							<div class="report-item__icon">
								<img src="/new/landing1/icon12.webp">
							</div>
							<div class="report-item__content">
								<span>Uyku Kalitesi</span>
								<span>
									Uyku Kalitesi Skoru ile mikrobiyomunun kaliteli bir uyku düzeni için ne kadar elverişli olduğunu öğrenebilirsin.
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<div class="row d-flex justify-content-center">
			<div class="col-md-8 discover__quote">
				<span><i>Mikrobiyom dünyanı keşfederek sorunlarının kaynağına inmek, sağlığını kontrol eden bakterilerinle tanışmak ve onları yönetmek artık senin elinde!</i></span><br><br>
			</div>
		</div>
	</div>
</section>

<section class="hprocess hprocess--home">
	<div class="container">
		<div class="row hprocess__howto">
			<h1 class="hprocess__howtotitle">{{__('pages/welcome.hprocess_title')}}</h1>
			<div class="col-12 mb-3">
				@include('include.video.surec', ['id' => 'surec'])
			</div>
			<div class="col-md-6 col-lg-3 col-xl-3 hprocess__step">
				<div class="hprocess__number">
					<img src="{{ url('/') }}/new/img/icons/surec1.webp" width="50"/>
				</div>
				<h2 class="hprocess__steptitle">ENBIOSIS MİKROBİYOM ANALİZİ SİPARİŞİ</h2>
				<p class="hprocess__stepdesc">
					www.enbiosis.com 'dan sipariş ver, sana gelen kutudaki kod ile web sitesinden kaydını oluştur ve anket sorularını tamamla.
				</p>
			</div>
			<div class="col-md-6 col-lg-3 col-xl-3 hprocess__step">
				<div class="hprocess__number">
					<img src="{{ url('/') }}/new/img/icons/surec2.webp" width="50"/>
				</div>
				<h2 class="hprocess__steptitle">NUMUNE ALIMI</h2>
				<p class="hprocess__stepdesc">
					Kargo ile adresine teslim edilen Mikrobiyom Kitinin içerisinde yer alan adımları takip et ve mikrobiyom analizin için gerekli gaita numuneni kolayca al. Sonrasında numuneni kutuda yer alan adrese ücretsiz bir şekilde gönder.
				</p>
			</div>
			<div class="col-md-6 col-lg-3 col-xl-3 hprocess__step">
				<div class="hprocess__number">
					<img src="{{ url('/') }}/new/img/icons/surec3.webp" width="50"/>
				</div>
				<h2 class="hprocess__steptitle">LABORATUVAR ANALİZİ</h2>
				<p class="hprocess__stepdesc">
					Numunen Metagenom laboratuvarımızda işleme alınarak 16S rRNA yeni nesil dizileme yöntemi ile dizilenir. Elde edilen veriler yapay zeka algoritmalarımız ile ayrıntılı olarak analiz edilir ve raporlandırılır.
				</p>
			</div>
			<div class="col-md-6 col-lg-3 col-xl-3 hprocess__step">
				<div class="hprocess__number">
					<img src="{{ url('/') }}/new/img/icons/surec4.webp" width="50"/>
				</div>
				<h2 class="hprocess__steptitle">ENBIOSIS KİŞİSELLEŞTİRİLMİŞ BESLENME REHBERİ</h2>
				<p class="hprocess__stepdesc">
					Raporların uzmanlarımız tarafından kontrol edildikten sonra sana ulaştırılır. Besin skorların ENBIOSIS diyetisyenleri tarafından yorumlanır ve diyetisyen takibi ile birlikte sana sunulur.
				</p>
			</div>
		</div>
	</div>
</section>
<section class="bilimkurulu">
	<span class="bilimkurulu__title">ENBIOSIS Bilim Kurulu</span>
	<div class="container">
		<div class="row team__row justify-content-center">
			<div class="col-12 col-md-6 col-lg-4 col-xl-4 team__col">
				<a href="{{route('prof-dr-hakan-alagozlu')}}">
					<div class="team__member">
						<img class="team__member__avatar" src="{{ url('/') }}/new/img/hakanalagozlu.webp" />
						<h2 class="team__member__name">Prof. Dr. Hakan Alagözlü</h2>
						<span class="team__member__job team__member__job--2">Gastroenteroloji Uzmanı</span>
						<span class="team__member__job">Gazi Üniversitesi Tıp Fakültesi –  Probiyotik Prebiyotik Derneği kurucu üyesi – Amerika Birleşik Devletleri New York SUNY Üniversitesi –  İstinye Üniversitesi Mikrobiyota çalışmaları –  Mikrobiyota kitabı editörü</span>
					</div>
				</a>
			</div>
			<div class="col-12 col-md-6 col-lg-4 col-xl-4 team__col">
				<a href="{{route('prof-dr-tarkan-karakan')}}">
					<div class="team__member">
						<img class="team__member__avatar" src="{{ url('/') }}/new/img/tarkankarakan.webp" />
						<h2 class="team__member__name">Prof. Dr. Tarkan Karakan</h2>
						<span class="team__member__job team__member__job--2">Gastroenteroloji Uzmanı</span>
						<span class="team__member__job">Hacettepe Üniversitesi Tıp Fakültesi – Johns Hopkins Üniversitesi – Probiyotik Prebiyotik Derneği Başkanı – Gastrointestinal Endoskopi Derneği yönetim kurulu üyesi – WGO Probiotics Guideline Komitesi – TÜBA Beslenme çalışma grubu üyesi – Tarım Bakanlığı Yeni Gıda Destekleri ve Güvenliği Komitesi</span>
					</div>
				</a>
			</div>
			<div class="col-12 col-md-6 col-lg-4 col-xl-4 team__col">
				<a href="{{route('prof-dr-halil-coskun')}}">
					<div class="team__member">
						<img class="team__member__avatar" src="{{ url('/') }}/new/img/halil-coskun.webp" />
						<h2 class="team__member__name">Prof. Dr. Halil Coşkun</h2>
						<span class="team__member__job team__member__job--2">Genel Cerrahi Uzmanı</span>
						<span class="team__member__job">Ankara Üniversitesi Tıp Fakültesi – İstanbul Üniversitesi – ABD Cleveland Tıp Merkezi Bariatrik Enstitüsü - Laparoskopik ve Robotik Obezite – Metabolik Cerrahi –  ABD Weill Cornell Tıp Fakültesi – Bezmi Alem Üniversitesi Genel Cerrahi Anabilim Dalı Kurucu Öğretim Üyesi</span>
					</div>
				</a>
			</div>
		</div>
	</div>
</section>
@include('include.basin')
<section class="discover">
	<div class="container">
		<div class="row">
			<div class="col-md-5">
				<span class="discover__title">MİKROBİYOMUNU KEŞFET</span>
				<p class="discover__desc">
					Mikrobiyomun gibi sen de eşsiz ve özelsin, bu sebeple problemlerin için kişiselleştirilmiş çözüm önerileni hak ediyorsun. ENBIOISIS, tedavine destek olmak ve yaşam kaliteni arttırmak için her zaman yanında! 
				</p>

			</div>
		</div>
		<a class="link-btn link-btn--orange" href="{{url('/checkout')}}"
		onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'button', Label: '{{slug('satin_al')}}' })">
		ŞİMDİ DENE</a>
	</div>
</section>
@endsection
@section('scripts')
<script type="text/javascript">
	$(document).ready(function(){

		$('.testimonials-slide').owlCarousel({
			loop:true,
			margin:10,
			nav:true,
			navText:['<img src="{{ url("/") }}/new/img/slide-prev.webp"/>','<img src="{{ url("/") }}/new/img/slide-next.webp"/>'],
			dots:false,
			items:1
		})
	});
	$(window).resize(function(){
		//$('.header').height($(window).height());
		$('.mask').height($(window).height());
	});
</script>
@endsection
