@extends('layouts.public')

@section('pageTitle','Mikrobiyota İle Sağlıklı Yaşam | Enbiosis')
@section('pageDescription','ENBIOSIS ile IBS problemine çözüm için sana iyi gelen besinleri öğren')


@section('content')
<section class="header bagirsak__1">
	<div class="container">
		<div class="row">
			<div class="col-7 col-md-7 col-lg-6 col-xl-6">
				<span class="header__slogan bagirsak__2"><b>IBS İLE MÜCADELENDE ARTIK YALNIZ DEĞİLSİN!</b></span>
				<p class="header__desc">
					Hayatının kontrolünü eline almanın zamanı geldi! IBS problemini çözmeye çalışırken hangi besinleri tüketmen gerektiği konusunda yolunu kaybettiysen ENBIOSIS rehberin olmak için senin yanında!
				</p>
				<a class="link-btn link-btn--orange" href="{{url('/checkout')}}"
				onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'button', Label: '{{slug('landing_page')}}' })">
				Hemen Satın Al</a>
			</div>
		</div>
	</div>
</section>
<section class="header header--mobile" style="background-image: url(/new/img/bagirsak-mobile.webp);background-color:white;padding-bottom:0;">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-12 col-lg-12 col-xl-12" style="text-align: center;">
				<h1 class="header__slogan"><b>IBS İLE MÜCADELENDE ARTIK YALNIZ DEĞİLSİN!</b></h1>
				<p class="header__desc">
					Hayatının kontrolünü eline almanın zamanı geldi! IBS problemini çözmeye çalışırken hangi besinleri tüketmen gerektiği konusunda yolunu kaybettiysen ENBIOSIS rehberin olmak için senin yanında!
				</p>
				<a class="link-btn link-btn--orange" href="{{url('/checkout')}}"
				onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'button', Label: '{{slug('landing_page')}}' })">
				Hemen Satın Al</a>
			</div>
		</div>
	</div>
</section>

<section class="bg-none">
	<div class="container">
		<div class="row">
			<div class="col-12">
				@include('include.video.karin', ['id' => 'karin'])
			</div>
		</div>
	</div>
</section>

<section class="mictobiome-balance" >
	<section class="microbiome-analysis microbiome-analysis--2 bagisiklik__3 bg-none">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2 class="microbiome-analysis__title bagisiklik__t" style="font-size:36px;">İLAÇLAR, PROBİYOTİKLER, DİYETLER… <br> PEKİ HANGİSİ SENİN İÇİN DOĞRU?</h2>
					<p class="microbiome-analysis__desc" style="font-size:17px;">
						Dünya nüfusunun yaklaşık %15’i seninle aynı problemleri yaşıyor. Karın ağrısı, değişen dışkılama alışkanlıkları ve sosyal ortamdan uzaklaşma… Bunlar sana tanıdık geldi mi? IBS semptomlarının gerçekleşme sıklığının ve şiddetinin kişiden kişiye değiştiği durumlarda sana sunulan çözüm önerilerinin diğerleri ile aynı olması bu hastalığını tedavi edememe sebebin olabilir. 
					</p>
				</div>
			</div>
		</div>
	</section>
</section>
<section class="mictobiome-balance">
	<section class="microbiome-analysis microbiome-analysis--2 kilo__1">
		<div class="container">
            <div class="row">
				<div class="col-md-12 mictobiome-balance-mobile">
					<img src="/new/img/kilo-mobile.webp" style="width:100%;height:auto;margin-bottom:70px;"/>
				</div>
				<div class="col-md-12 col-lg-6 col-xl-6">
					<h2 class="microbiome-analysis__title">HANGİ BESİNLERİN SANA UYGUN OLDUĞUNUN CEVABI İNTERNETTE DEĞİL <b> BAĞIRSAKLARINDA! </b></h2>
					<p class="microbiome-analysis__desc">
						Bağırsaklarının trilyonlarca bakteriye ev sahipliği yaptığını biliyor muydun? Peki ya bu bakterilerin IBS semptomların ile ilişkili olduğunu? Yapılan araştırmalar IBS hastaları ile sağlıklı bireylerin bağırsaklarında yer alan bakteri türleri arasında ciddi farklılıklar olduğunu bildirmektedir. Yani sancılı karın ağrıların, şişkinlik şikayetlerin ve hayat kaliteni etkileyen bağırsak haraketliliğindeki değişim aslında içinde yaşayan mikroskobik canlıların miktarı, cinsi ve dağılımı ile ilintilidir. Bu canlılarının oluşturduğu ekosistemi yani mikrobiyomunu dengelemek onların hangi besinlere ihtiyacı olduğunu bilmekle mümkündür! 
					</p>
				</div>
			</div>
		</div>
	</section>
</section>

<section class="mictobiome-balance my-lg-3 my-5">
    <div class="container ">
        
    </div>
</section>

<section class="analyze-report">
	<div class="container">
		<div class="row analyze-report__besin-skorlari bagisiklik__6">
			<div class="col-md-12 col-lg-6 col-xl-6 d-flex align-items-center">
				<div class="analyze-report__item">
					<h3>Besin Skorları:</h3>
					<span>Mikrobiyom sağlığı büyük bir oranda tükettiğin besinlerden etkilenmektedir. Mikrobiyom profiline göre sana özel hazırlanmış besin skorları ile sağlıklı bir mikrobiyom için tüketmen gereken besinleri keşfet!</span>

					<div class="analyze-report__food-rating">
						<img src="{{url('/')}}/new/img/iyi-besin.webp" width="100"/>
						<div class="content">
							<h4>İşte Senin Besinlerin!</h4>
							<p>
								Sık tüketmen gereken besinleri ifade eder.
							</p>
						</div>
					</div>
					<div class="analyze-report__food-rating">
						<img src="{{url('/')}}/new/img/orta-besin.webp" width="100"/>
						<div class="content">
							<h4>Seninle Uyum İçinde Olan Besinler!</h4>
							<p>
								Dengeli ve çeşitli şekilde tüketmen gereken besinleri ifade eder.
							</p>
						</div>
					</div>
					<div class="analyze-report__food-rating">
						<img src="{{url('/')}}/new/img/kotu-besin.webp" width="100"/>
						<div class="content">
							<h4>Kaçınman Gereken Besinler!</h4>
							<p>
								Daha az tüketmen gereken besinleri ifade eder.
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12 col-lg-6 col-xl-6 iphone-wrapper">
				<div class="device device-ipad-pro device-gold">
					<div class="device-frame">
						<iframe class="device-content" src="https://app.enbiosis.com/demo/foods/tr" style="object-fit: initial;width:100%;height:100%;border:0;"></iframe>
					</div>
					<div class="device-stripe"></div>
					<div class="device-header"></div>
					<div class="device-sensors"></div>
					<div class="device-btns"></div>
					<div class="device-power"></div>
				</div>
			</div>
		</div>
		<div class="row d-flex justify-content-center">
			<div class="col-md-12 analyze-report__quote">
				<i>ENBIOSIS ile evde kolayca alacağın numunen sayesinde bağırsaklarında neler olup bittiğini öğrenebilir ve bakterilerini optimize edecek besinlerin hangileri olduğunu keşfedebilirsin. ENBIOSIS diyetisyenleri tedavine destek olacak bu süreçte senin yanında!</i><br/><br/>
				<a class="link-btn link-btn--orange" href="{{url('/iletisim')}}"
				onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'button', Label: '{{slug('landing_page')}}' })">
				Bize Ulaşın</a>
			</div>
		</div>
	</div>
</section>
@include('include.banner')
@include('include.package')
<section class="triple-box bagisiklik__7">
	<div class="container">
		<h2 class="triple-box__title">MİKROBİYOM ANALİZİ & KİŞİSELLEŞTİRİLMİŞ BESLENME REHBERİ</h2>
		<p class="triple-box__subtitle">Mikrobiyomun gibi sen de eşsiz ve özelsin! Bu sebeple kişiselleştirilmiş sağlık çözümlerini sen de hak ediyorsun. ENBIOSIS ile başlayacağın bu yolculukta sana özel sağlıklı ve sürdürülebilir bir yaşam oluşturmak için seninle mikrobiyom dünyasının kapısını aralayacağız. Yolculuğun için gerekli olan tüm bilgileri uzman diyetisyenlerimiz tarafından elde edeceksin. Kalıcı çözüm için adım atma sırası ise artık sende!</p>
		<div class="row">
			<div class="col-md-4 triple-box__box">
				<img src="{{url('/')}}/new/img/triple-icon1.webp" />
				<span>Mikrobiyom Analizi İle Kendini Tanı</span>
			</div>
			<div class="col-md-4 triple-box__box">
				<img src="{{url('/')}}/new/img/triple-icon2.webp" />
				<span>Sana Sunduğumuz Kişiselleştirilmiş Beslenme Önerilerini <br>ENBIOSIS Diyetisyeni İle Takip Et</span>
			</div>
			<div class="col-md-4 triple-box__box">
				<img src="{{url('/')}}/new/img/triple-icon3.webp" />
				<span>Dengeli Mikrobiyom Profiline Ulaş Ve Farkı Hisset!</span>
			</div>
		</div>
	</div>
</section>
<section class="hprocess">
	<div class="container">
		<div class="row hprocess__howto" style="margin-top:0;">
			<h2 class="hprocess__howtotitle">SÜREÇ NASIL İŞLER?</h2>
			<div class="col-12 mb-3">
				@include('include.video.surec', ['id' => 'surec'])
			</div>
			<div class="col-md-6 col-lg-3 col-xl-3 hprocess__step">
				<div class="hprocess__number">
					<img src="{{ url('/') }}/new/img/icons/surec1.webp" width="50"/>
				</div>
				<h2 class="hprocess__steptitle">ENBIOSIS MİKROBİYOM ANALİZİ SİPARİŞİ</h2>
				<p class="hprocess__stepdesc">
					www.enbiosis.com 'dan sipariş ver, sana gelen kutudaki kod ile web sitesinden kaydını oluştur ve anket sorularını tamamla.
				</p>
			</div>
			<div class="col-md-6 col-lg-3 col-xl-3 hprocess__step">
				<div class="hprocess__number">
					<img src="{{ url('/') }}/new/img/icons/surec2.webp" width="50"/>
				</div>
				<h2 class="hprocess__steptitle">NUMUNE ALIMI</h2>
				<p class="hprocess__stepdesc">
					Kargo ile adresine teslim edilen Mikrobiyom Kitinin içerisinde yer alan adımları takip et ve mikrobiyom analizin için gerekli gaita numuneni kolayca al. Sonrasında numuneni kutuda yer alan adrese ücretsiz bir şekilde gönder.
				</p>
			</div>
			<div class="col-md-6 col-lg-3 col-xl-3 hprocess__step">
				<div class="hprocess__number">
					<img src="{{ url('/') }}/new/img/icons/surec3.webp" width="50"/>
				</div>
				<h2 class="hprocess__steptitle">LABORATUVAR ANALİZİ</h2>
				<p class="hprocess__stepdesc">
					Numunen Türkiye’nin en kapsamlı araştırma merkezi olan Genom ve Kök Hücre Merkezi’nde işleme alınarak 16S rRNA yeni nesil dizileme yöntemi ile dizilenir. Elde edilen veriler yapay zeka algoritmalarımız ile ayrıntılı olarak analiz edilir ve raporlandırılır.
				</p>
			</div>
			<div class="col-md-6 col-lg-3 col-xl-3 hprocess__step">
				<div class="hprocess__number">
					<img src="{{ url('/') }}/new/img/icons/surec4.webp" width="50"/>
				</div>
				<h2 class="hprocess__steptitle">ENBIOSIS KİŞİSELLEŞTİRİLMİŞ BESLENME REHBERİ</h2>
				<p class="hprocess__stepdesc">
					Raporların uzmanlarımız tarafından kontrol edildikten sonra sana ulaştırılır. Besin skorların ENBIOSIS diyetisyenleri tarafından yorumlanır ve diyetisyen takibi ile birlikte sana sunulur.
				</p>
			</div>
		</div>
	</div>
</section>
<section class="bilimkurulu">
	<span class="bilimkurulu__title">ENBIOSIS Bilim Kurulu</span>
	<div class="container">
		<div class="row team__row justify-content-center">
			<div class="col-12 col-md-6 col-lg-4 col-xl-4 team__col">
				<a href="{{route('prof-dr-hakan-alagozlu')}}">
					<div class="team__member">
						<img class="team__member__avatar" src="{{ url('/') }}/new/img/hakanalagozlu.webp" />
						<h2 class="team__member__name">Prof. Dr. Hakan Alagözlü</h2>
						<span class="team__member__job team__member__job--2">Gastroenteroloji Uzmanı</span>
						<span class="team__member__job">Gazi Üniversitesi Tıp Fakültesi –  Probiyotik Prebiyotik Derneği kurucu üyesi – Amerika Birleşik Devletleri New York SUNY Üniversitesi –  İstinye Üniversitesi Mikrobiyota çalışmaları –  Mikrobiyota kitabı editörü</span>
					</div>
				</a>
			</div>
			<div class="col-12 col-md-6 col-lg-4 col-xl-4 team__col">
				<a href="{{route('prof-dr-tarkan-karakan')}}">
					<div class="team__member">
						<img class="team__member__avatar" src="{{ url('/') }}/new/img/tarkankarakan.webp" />
						<h2 class="team__member__name">Prof. Dr. Tarkan Karakan</h2>
						<span class="team__member__job team__member__job--2">Gastroenteroloji Uzmanı</span>
						<span class="team__member__job">Hacettepe Üniversitesi Tıp Fakültesi – Johns Hopkins Üniversitesi – Probiyotik Prebiyotik Derneği Başkanı – Gastrointestinal Endoskopi Derneği yönetim kurulu üyesi – WGO Probiotics Guideline Komitesi – TÜBA Beslenme çalışma grubu üyesi – Tarım Bakanlığı Yeni Gıda Destekleri ve Güvenliği Komitesi</span>
					</div>
				</a>
			</div>
			<div class="col-12 col-md-6 col-lg-4 col-xl-4 team__col">
				<a href="{{route('prof-dr-halil-coskun')}}">
					<div class="team__member">
						<img class="team__member__avatar" src="{{ url('/') }}/new/img/halil-coskun.webp" />
						<h2 class="team__member__name">Prof. Dr. Halil Coşkun</h2>
						<span class="team__member__job team__member__job--2">Genel Cerrahi Uzmanı</span>
						<span class="team__member__job">Ankara Üniversitesi Tıp Fakültesi – İstanbul Üniversitesi – ABD Cleveland Tıp Merkezi Bariatrik Enstitüsü - Laparoskopik ve Robotik Obezite – Metabolik Cerrahi –  ABD Weill Cornell Tıp Fakültesi – Bezmi Alem Üniversitesi Genel Cerrahi Anabilim Dalı Kurucu Öğretim Üyesi</span>
					</div>
				</a>
			</div>
		</div>
	</div>
</section>
@include('include.basin')
<section class="discover">
	<div class="container">
		<div class="row">
			<div class="col-md-5">
				<span class="discover__title">ENBIOSIS İLE KİŞİSELLEŞTİRİLMİŞ SAĞLIĞI KEŞFET!</span>
				<p class="discover__desc">
					Artık tedavini kişiselleştirerek senin için en iyi olana odaklanma zamanı geldi! ENBIOSIS ile kişiselleştirilmiş sağlığın kapısını arala!
				</p>

			</div>
		</div>
		<a class="link-btn link-btn--orange" href="{{url('/checkout')}}"
		onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'button', Label: '{{slug('satin_al')}}' })">
		SATIN AL</a>
	</div>
</section>
@endsection
