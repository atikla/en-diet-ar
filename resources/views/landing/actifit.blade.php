@extends('layouts.public')

@section('pageTitle','Mikrobiyota İle Sağlıklı Yaşam | Enbiosis')
@section('pageDescription','Mikrobiyota nedir? Enbiosis mikrobiyota testi ile size uygun sağlıklı beslenme rehberinize hemen kavuşun!')

@section('content')
<section class="header kilo__1">
	<div class="container">
		<div class="row">
			<div class="col-7 col-md-7 col-lg-6 col-xl-6">
				<span class="header__slogan bagirsak__2"><b>ENBIOSIS İLE SANA EN UYGUN DİYETİ KEŞFET</b></span>
				<p class="header__desc">
                    Sağlıklı beslenme görecelidir, vücudunun ihtiyaçlarını karşılamak için sana en uygun besinleri öğrenmenin zamanı artık geldi!
				</p>
				<a class="link-btn link-btn--orange" href="{{url('/checkout')}}"
				onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'button', Label: '{{slug('landing_page')}}' })">
				Hemen Satın Al</a>
			</div>
		</div>
	</div>
</section>
<section class="header header--mobile" style="background-image: url(/new/img/kilo-mobile.webp);background-color:white;padding-bottom:0;">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-12 col-lg-12 col-xl-12" style="    text-align: center;">
				<h1 class="header__slogan"><b>ENBIOSIS İLE SANA EN UYGUN DİYETİ KEŞFET</b></h1>
				<p class="header__desc">
                    Sağlıklı beslenme görecelidir, vücudunun ihtiyaçlarını karşılamak için sana en uygun besinleri öğrenmenin zamanı artık geldi!
				</p>
				<a class="link-btn link-btn--orange" href="{{url('/checkout')}}"
				onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'button', Label: '{{slug('landing_page')}}' })">
				Hemen Satın Al</a>
			</div>
		</div>
	</div>
</section>
<section class="mictobiome-balance">
	<section class="microbiome-analysis microbiome-analysis--2 bagirsak__11">
		<div class="container">
            <div class="row" style="justify-content:flex-end;">
				<div class="col-md-12 mictobiome-balance-mobile">
					<img src="/new/landing1/bagirsak3-mobile.webp" style="width:100%;height:auto;margin-bottom:70px;"/>
				</div>
				<div class="col-md-12 col-lg-6 col-xl-6">
					<h2 class="microbiome-analysis__title mt-lg-5"><b>SANA EN UYGUN DİYETİ MİKROBİYOMUN SÖYLER!</b></h2>
					<p class="microbiome-analysis__desc">
                        Mikrobiyom dünyası insan genomundan farklı olarak parmak izleri gibi sadece kişiye özeldir. Mikrobiyomun vücudunun besinlere verdiği tepkide önemli rol oynar. Bu sebeple bir kişiye faydalı olan bir besin başka bir kişi için faydalı olmayabilir. ENBIOSIS ile online diyet hizmeti eşliğinde mikrobiyomuna göre beslen, farkı hisset!
					</p>
				</div>
			</div>
		</div>
	</section>
</section>
@include('include.banner')
<section class="mictobiome-balance" >
	<section class="microbiome-analysis microbiome-analysis--2 bagisiklik__3 ">
		<div class="container">
			<div class="row">
				<div class="col-md-12 mictobiome-balance-mobile">
					<img src="/new/img/about-1-mobile.webp" style="width:100%;height:auto;margin-bottom:70px;"/>
				</div>
				<div class="col-md-12 col-lg-6 col-xl-6">
                    <h2 class="microbiome-analysis__title bagisiklik__t" style="font-size:36px;"><b>MİKROBİYOM NEDİR?</b></h2>
                    <p class="microbiome-analysis__desc" style="font-size:17px;">
                        Vücudumuz doğduğumuz andan itibaren bize eşlik eden sağlığımız ve yaşam kalitemiz üzerinde tartışılmaz öneme sahip olan trilyonlarca mikroorganizmaya ev sahipliği yapmaktadır. Bu mikroskobik canlılar ile birlikte genetik materyalleri, çevreyle etkileşimleri ve yaşadıkları habitatın bütünü mikrobiyomu oluşturur. Mikrobiyomun çok büyük kısmı bakterilerden oluşmaktadır. Bu sebeple, bakterilerimizin dağılımı ve çeşitliliği mikrobiyom dengesinde kritik bir öneme sahiptir.
                    </p>
				</div>
			</div>
        </div>
    </section>
</section>
@include('include.mic_balance')
<section class="mictobiome-balance" >
	<section class="microbiome-analysis microbiome-analysis--2 bagisiklik__3 bg-none">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-12 text-center">
                    <h2 class="microbiome-analysis__title bagisiklik__t text-uppercase" style="font-size:36px;"><b>ENBIOSIS</b> İle Mikrobiyomunu Keşfet, <br>Sağlığına Hükmet!</h2>
                    <p class="microbiome-analysis__desc" style="font-size:17px;">
                        ENBIOSIS sana en uygun diyeti bulmak için öncelikle dışkı numunenden alınan mikrobiyom örneğini analiz eder. Ardından mikrobiyomunu sağlıklı hale getirmen için tüketmen gereken besinleri sahip olduğu besin-bakteri ilişkilerini kullanarak sunar. ENBIOSIS kiti ile mikrobiyom analizin için gerekli olan numuneyi herhangi bir hastaneye, kliniğe gitmene gerek kalmadan evinde kolayca alırsın. Sonuçlarının çıkmasının ardından 6 hafta boyunca ENBIOSIS diyetisyenleri tarafından takip edilir ve sana özel hazırlanmış mikrobiyom diyeti ile sonuca ulaşırsın!
                    </p>
				</div>
			</div>
		</div>
	</section>
</section>
<section class="hprocess hprocess--home">
	<div class="container">
		<div class="row hprocess__howto">
			<h1 class="hprocess__howtotitle">SÜREÇ NASIL İŞLER?</h1>
			<div class="col-12 mb-3">
				@include('include.video.surec', ['id' => 'surec'])
			</div>
			<div class="col-md-6 col-lg-3 col-xl-3 hprocess__step">
				<div class="hprocess__number">
					<img src="{{url('/')}}/new/img/icons/surec1.webp" width="50">
				</div>
				<h2 class="hprocess__steptitle">ENBIOSIS MİKROBİYOM ANALİZİ SİPARİŞİ</h2>
				<p class="hprocess__stepdesc">
					www.enbiosis.com 'dan sipariş ver, sana gelen kutudaki kod ile web sitesinden kaydını oluştur ve anket sorularını tamamla.
				</p>
			</div>
			<div class="col-md-6 col-lg-3 col-xl-3 hprocess__step">
				<div class="hprocess__number">
					<img src="{{url('/')}}/new/img/icons/surec2.webp" width="50">
				</div>
				<h2 class="hprocess__steptitle">NUMUNE ALIMI</h2>
				<p class="hprocess__stepdesc">
					Kargo ile adresine teslim edilen Mikrobiyom Kitinin içerisinde yer alan adımları takip et ve mikrobiyom analizin için gerekli gaita numuneni kolayca al. Sonrasında numuneni kutuda yer alan adrese ücretsiz bir şekilde gönder.
				</p>
			</div>
			<div class="col-md-6 col-lg-3 col-xl-3 hprocess__step">
				<div class="hprocess__number">
					<img src="{{url('/')}}/new/img/icons/surec3.webp" width="50">
				</div>
				<h2 class="hprocess__steptitle">LABORATUVAR ANALİZİ</h2>
				<p class="hprocess__stepdesc">
					Numunen Metagenom laboratuvarımızda işleme alınarak 16S rRNA yeni nesil dizileme yöntemi ile dizilenir. Elde edilen veriler yapay zeka algoritmalarımız ile ayrıntılı olarak analiz edilir ve raporlandırılır.
				</p>
			</div>
			<div class="col-md-6 col-lg-3 col-xl-3 hprocess__step">
				<div class="hprocess__number">
					<img src="{{url('/')}}/new/img/icons/surec4.webp" width="50">
				</div>
				<h2 class="hprocess__steptitle">ENBIOSIS KİŞİSELLEŞTİRİLMİŞ BESLENME REHBERİ</h2>
				<p class="hprocess__stepdesc">
					Raporların uzmanlarımız tarafından kontrol edildikten sonra sana ulaştırılır. Besin skorların ENBIOSIS diyetisyenleri tarafından yorumlanır ve diyetisyen takibi ile birlikte sana sunulur.
				</p>
			</div>
		</div>
	</div>
</section>
@include('include.package')
<section class="discover">
	<div class="container">
		<div class="row">
			<div class="col-md-5">
				<span class="discover__title">ENBIOSIS İLE BESLENMENİ YÖNET</span>
				<p class="discover__desc">
                    Her vücudun ihtiyacı farklıdır, bu sebeple bir kişi için faydalı olan besin senin için olmayabilir. Şimdi vücudunun ihtiyacı olan besinleri öğrenme ve sağlığını yönetme zamanı geldi!
				</p>

			</div>
		</div>
		<a class="link-btn link-btn--orange" href="{{url('/checkout')}}"
		onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'button', Label: '{{slug('satin_al')}}' })">
		SATIN AL</a>
	</div>
</section>
@endsection
@section('scripts')
    <script>
		$(document).ready(function(){
			$('.end-carousel-balance').owlCarousel({
				loop:true,
				autoplay:true,
				autoplayTimeout:6000,
				margin:20,
				nav:true,
				dots:true,
				navText:['<img src="{{ url("/") }}/new/img/slide-prev.webp"/>','<img src="{{ url("/") }}/new/img/slide-next.webp"/>'],
				responsive:{
					0:{
						items:1,
						nav:true,
						dots:true,
						// navText:['<img src="{{ url("/") }}/new/img/slide-prev.webp"/>','<img src="{{ url("/") }}/new/img/slide-next.webp"/>'],
					},
					600:{
						items:2
					},
					1000:{
						items:3
					}
				}
			});
		});
    </script>
@endsection
