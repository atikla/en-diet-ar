@extends('layouts.public')
@section('subpageMenu','topmenu--subpage')
@section('pageTitle', 'For Investors | Enbiosis')
@section('imza')
style = "background-image: url({{url('/')}}/new/img/footer-imza.webp);"	
@endsection
@section('metaTag')
<meta name="robots" content="noindex">
@endsection
{{-- @section('pageDescription', __('pages/hakkimizda.page_description')) --}}
@section('styles')
	<style>
		.content{
			margin-left:30px;
			position: initial !important;
		}
		.content p {
			color: #000 !important;
			font-size: 16px !important;
		}
		.grayscale {
			filter: grayscale(100%);
		}
		.grayscale:hover {
			filter: grayscale(20%);
		}
	</style>
@endsection
@section('content')

<section class="subpage-header subpage-header--investor">
	<div class="container">
		<div class="row">
			<div class="subpage-header__content">
				<h1 class="subpage-header__title">For Investors</h1>
				<div class="subpage-header__seperator"></div>
				<div class="subpage-header__breadcrumb">
					<nav aria-label="breadcrumb">
						<ol itemscope itemtype="https://schema.org/BreadcrumbList"
						class="breadcrumb no-bg-color text-light">
							<li itemprop="itemListElement" itemscope
							itemtype="https://schema.org/ListItem"
							class="breadcrumb-item">
								<a itemprop="item"
								style="color:#007bff"
								href="{{ route('/')}}">
								<span itemprop="name">Home Page</span>
								</a>
								<meta itemprop="position" content="1"/>
							</li>
							<li itemprop="itemListElement" itemscope
							itemtype="https://schema.org/ListItem"
							class="breadcrumb-item active te">
								<a itemprop="item" 
								style="color:#6c757d"
								href="{{ route('yatirimci')}}">
									<span itemprop="name">For Investors</span>
								</a>
								<meta itemprop="position" content="2"/>
							</li>
						</ol>
					</nav>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="microbiome" style="background-color:transparent;text-align: justify">
	<div class="container">
		<div class="row">
			<div class="col-md-12 ecosystem__item text-center">
				<h2 class="maffects__title">
					<b>
						ENBIOSIS Biotechnology
					</b>
				</h2>
				<p class="ecosystem__desc">
					In Nature, one of the most respected scientific journals in the world, it has published that two of the scientific developments that have shaped the last 10 years are microbiome science and artificial intelligence technology. 
					<br/><br/>
					ENBIOSIS with its staff established by bringing together few and successful names in their field aims to advance microbiome science in order to serve human health and to produce personalized health technologies by targeting new generation artificial intelligence technologies. 
				</p>
			</div>
		</div>
	</div>
</section>

<section class="microbiome mt-5 team-web" style="background-color:transparent;">
	<div class="container my-5">
		<div class="row d-flex justify-content-center">
			<div class="col-md-6 microbiome__col">
				<img src="{{url('/')}}/new/img/bayerlogo.webp" width="130"/>
				<div>
					<span><i>2020 Bayer G4A</i></span>
					<p>ENBIOSIS Bıotechnology, became one of the companies eligible to participate in the "Enterprise Acceleration Program" in which Bayer supports initiatives that shape the future.</p>
				</div>
			</div>
			<div class="col-md-6 microbiome__col">
				<img src="{{url('/')}}/new/img/imagine_tomorrow.webp" width="130"/>
				<div>
					<span><i>2020 "Health Initiative of the Year Award"</i></span>
					<p>
						 As Turkey's first and only company in the field we are proud to have taken this award.<br/>
						-Ömer Özkan, CEO
					</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="microbiome mt-5 team-mobile" style="background-color:transparent;">
	<section class="en-carousel mt-0 pb-3">
		<div class="container">
			<div class="row d-flex justify-content-center">
				<div class="row owl-carousel team-carousel owl-theme">
					<div class="item  microbiome__col text-center py-3"  style="display:initial">
						<img src="{{url('/')}}/new/img/bayerlogo.webp" style="width: 160px;margin-left: calc(50% - 80px)"/> <br>
						<div class="text-center">
							<span><i>2020 Bayer G4A</i></span>
							<p>ENBIOSIS Bıotechnology, became one of the companies eligible to participate in the "Enterprise Acceleration Program" in which Bayer supports initiatives that shape the future.</p>
						</div>
					</div>
					<div class="item microbiome__col text-center py-3" style="display:initial">
						<img src="{{url('/')}}/new/img/imagine_tomorrow.webp"style="width: 160px;margin-left: calc(50% - 80px)"/> <br>
						<div class="text-center">
							<span><i>2020 "Health Initiative of the Year Award"</i></span>
							<p>
								As Turkey's first and only company in the field we are proud to have taken this award.<br/>
								-Ömer Özkan, CEO
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>
<section class="en-carousel"style="margin-top:0;background:url({{ url('/') }}/new/img/basindabiz.webp);background-repeat: no-repeat;background-size: cover;">
	<div class="container">
		 <div class="row">
			 <div class="col-md-12">
				 <h2 class="maffects__title">
					 <b>
						{{__('include/basin.title')}}
					 </b>
				 </h2>
			 </div>
		 </div>
		 <div class="row owl-carousel basin-carousel owl-theme">
			<div class="item">
				<img 
				onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'basinda_biz', Label: '{{slug('webrazzi')}}' });
						window.open('https://webrazzi.com/2020/07/01/mikrobiyom-yapay-zeka-kisisellestirilmis-saglik-teknolojisi-enbiosis/', '_blank');"
				src="{{ url('/') }}/new/img/basin/crousal-webrazzi.webp"/>
				<div class="content p-4">
					<p>
						{{__('include/basin.webrazzi')}}
						<a
						target="_blank"
						href="https://webrazzi.com/2020/07/01/mikrobiyom-yapay-zeka-kisisellestirilmis-saglik-teknolojisi-enbiosis/"
						onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'basinda_biz', Label: '{{slug('webrazzi')}}' })">
						{{__('include/basin.read_more')}}</a>
					</p>
					
				</div>
			</div>
			<div class="item">
				<img 
				onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'basinda_biz', Label: '{{slug('capital')}}' })
				window.open('https://www.capital.com.tr/sirket-panosu/sirket-panosu-haberleri/yapay-zekayla-kisiye-ozel-diyet-listesi-hazirladi', '_blank');"
				class="grayscale" src="{{ url('/') }}/new/img/basin/crousal-capital.webp"/>
				<div class="content p-4">
					<p>
						{{__('include/basin.capital')}}
						<a
						target="_blank"
						href="https://www.capital.com.tr/sirket-panosu/sirket-panosu-haberleri/yapay-zekayla-kisiye-ozel-diyet-listesi-hazirladi"
						onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'basinda_biz', Label: '{{slug('capital')}}' })">
						{{__('include/basin.read_more')}}</a>
					</p>
				</div>
			</div>
			<div class="item">
				<img 
				onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'basinda_biz', Label: '{{slug('sabah')}}' });
						window.open('https://www.sabah.com.tr/saglik/2020/08/13/kisiye-ozel-tedavi-yontemleri-artik-hastanelerde', '_blank');" 
				class="grayscale" src="{{ url('/') }}/new/img/basin/crousal-sabah.webp"/>
				<div class="content p-4">
					<p>
						{{__('include/basin.sabah')}}
					   <a
						target="_blank"
						href="https://www.sabah.com.tr/saglik/2020/08/13/kisiye-ozel-tedavi-yontemleri-artik-hastanelerde"
						onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'basinda_biz', Label: '{{slug('sabah')}}' })">
						{{__('include/basin.read_more')}}</a>
					</p>
				</div>
			</div>
			<div class="item">
				<img 
				onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'basinda_biz', Label: '{{slug('egirisim')}}' });
						window.open('https://egirisim.com/2020/07/01/enbiosis-mikrobiyal-organizmalar-ile-insan-sagligini-artirmaya-calisan-yerli-yapay-zeka-girisimi/', '_blank');"
				class="grayscale" src="{{ url('/') }}/new/img/basin/crousal-egitisim.webp"/>
				<div class="content p-4">
					<p>
						{{__('include/basin.egirişim')}}
						<a 
						target="_blank" 
						href="https://egirisim.com/2020/07/01/enbiosis-mikrobiyal-organizmalar-ile-insan-sagligini-artirmaya-calisan-yerli-yapay-zeka-girisimi/"
						onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'basinda_biz', Label: '{{slug('egirisim')}}' })">
						{{__('include/basin.read_more')}}</a>
					</p>
				</div>
			</div>
			<div class="item">
				<img 
				onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'basinda_biz', Label: '{{slug('dunya')}}' });
						window.open('https://www.dunya.com/sirketler/enbiosis-covid-19a-yapay-zeka-ile-cozum-bulmayi-hedefliyor-haberi-473071');"
				class="grayscale" src="{{ url('/') }}/new/img/basin/crousal-dunya.webp"/>
				<div class="content p-4">
					<p>
						{{__('include/basin.dunya')}}
						<a
						target="_blank"
						href="https://www.dunya.com/sirketler/enbiosis-covid-19a-yapay-zeka-ile-cozum-bulmayi-hedefliyor-haberi-473071"
						onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'basinda_biz', Label: '{{slug('dunya')}}' })">
						{{__('include/basin.read_more')}}</a>
					</p>
				</div>
			</div>
		 </div>
	</div>
 </section>
 <section class="team team-mobile">
	<section class="en-carousel mt-0 pb-3">
		<div class="container">
			<div class="row team__row">
				<div class="col-md-12">
					<h2 class="maffects__title">
						<b>
							Our Team
						</b>
					</h2>
					<p class="ecosystem__desc text-center">
						We are team of scientists, physicians, dietitians, and entrepreneurs
					</p>
				</div>
			</div>
			<div class="row owl-carousel team-carousel owl-theme">
				<div class="item">
					<div class="content mx-4">
					<a href="{{route('ceo-omer-ozkan')}}">
						<div class="team__member">
							<img style="width: 210px; height: auto;"class="team__member__avatar" src="{{ url('/') }}/new/img/omer-ozkan.webp" />
							<h2 class="team__member__name">Ömer Özkan</h2>
							<span class="team__member__job team__member__job--2">CEO</span>
							<span class="team__member__job">Entrepreneur - Renewable Energy - Artificial Intelligence - Biotechnology</span>
						</div>
					</a>
					</div>
				</div>
				<div class="item">
					<div class="content mx-4">
					<a href="{{route('dr-ozkan-ufuk-nalbantoglu')}}">
						<div class="team__member">
							<img class="team__member__avatar" src="{{ url('/') }}/new/img/ozkan-ufuk-nalbantoglu.webp" />
							<h2 class="team__member__name">Ph.D. Ufuk Nalbantoğlu</h2>
							<span class="team__member__job team__member__job--2">CTO</span>
							<span class="team__member__job">
								15 years of experience in Microbiome Research - Principal Investigator on projects funded by NIH, NFS and DTRA - World Microbiome Project bioinformatics software architect
							</span>
						</div>
					</a>
					</div>
				</div>
				<div class="item">
					<div class="content mx-4">
					<a href="{{route('doc-dr-aycan-gundogdu')}}">
						<div class="team__member">
							<img class="team__member__avatar" src="{{ url('/') }}/new/img/aycan-gundogdu.webp" />
							<h2 class="team__member__name">Assoc. Prof. Aycan Gündoğdu</h2>
							<span class="team__member__job team__member__job--2">CSO</span>
							<span class="team__member__job">
								Sunshine Coast University-Queensland Department of Health-CSRIO National Laboratories-chief scientist of the Turkish Microbiome Project-member of the European Union Human Microbiome Studies Committee-TUBITAK-member of the SBAG Advisory Board
							</span>
						</div>
					</a>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>
<section class="team team-web">
	<div class="container">
		<div class="row team__row">
			<div class="col-md-12">
				<h2 class="maffects__title">
					<b>
						Our Team
					</b>
				</h2>
				<p class="ecosystem__desc text-center">
					We are team of scientists, physicians, dietitians, and entrepreneurs
				</p>
			</div>
			<div class="col-md-6 col-lg-4 col-xl-4 team__col">
				<a href="{{route('ceo-omer-ozkan')}}">
				<div class="team__member">
					<img style="width: 210px; height: auto;"class="team__member__avatar" src="{{ url('/') }}/new/img/omer-ozkan.webp" />
					<h2 class="team__member__name">Ömer Özkan</h2>
					<span class="team__member__job team__member__job--2">CEO</span>
					<span class="team__member__job">Entrepreneur - Renewable Energy - Artificial Intelligence - Biotechnology</span>
				</div>
				</a>
			</div>
			<div class="col-md-6 col-lg-4 col-xl-4 team__col">
				<a href="{{route('dr-ozkan-ufuk-nalbantoglu')}}">
				<div class="team__member">
					<img class="team__member__avatar" src="{{ url('/') }}/new/img/ozkan-ufuk-nalbantoglu.webp" />
					<h2 class="team__member__name">Ph.D. Ufuk Nalbantoğlu</h2>
					<span class="team__member__job team__member__job--2">CTO</span>
					<span class="team__member__job">
						15 years of experience in Microbiome Research - Principal Investigator on projects funded by NIH, NFS and DTRA - World Microbiome Project bioinformatics software architect
					</span>
				</div>
				</a>
			</div>
			<div class="col-md-6 col-lg-4 col-xl-4 team__col">
				<a href="{{route('doc-dr-aycan-gundogdu')}}">
				<div class="team__member">
					<img class="team__member__avatar" src="{{ url('/') }}/new/img/aycan-gundogdu.webp" />
					<h2 class="team__member__name">Assoc. Prof. Aycan Gündoğdu</h2>
					<span class="team__member__job team__member__job--2">CSO</span>
					<span class="team__member__job">
						Sunshine Coast University-Queensland Department of Health-CSRIO National Laboratories-chief scientist of the Turkish Microbiome Project-member of the European Union Human Microbiome Studies Committee-TUBITAK-member of the SBAG Advisory Board
					</span>
				</div>
				</a>
			</div>
		</div>
	</div>
</section>
<section class="bilimkurulu team-mobile mt-0 pt-0">
	<section class="en-carousel  mt-0 py-3">
		<span class="bilimkurulu__title">{{ucwords(strtolower(__('pages/welcome.bilimkurulu_title')))}}</span>
		<div class="container">
			<div class="row owl-carousel team-carousel owl-theme">
				<div class="item">
					<div class="content mx-4">
					<a href="{{route('prof-dr-tarkan-karakan')}}">
						<div class="team__member">
							<img class="team__member__avatar" src="{{ url('/') }}/new/img/tarkankarakan.webp" />
							<h2 class="team__member__name">Prof. Dr. Tarkan Karakan</h2>
							<span class="team__member__job team__member__job--2">Gastroenterologist</span>
							<span class="team__member__job">
								John Hopkins University – President of Probiotics Association – Board Member of
								Gastrointestinal Endoscopy Association – WGO Probiotics Guideline Committee Member	
							</span>
						</div>
					</a>
					</div>
				</div>
				<div class="item">
					<div class="content mx-4">
					<a href="{{route('prof-dr-halil-coskun')}}">
						<div class="team__member">
							<img class="team__member__avatar" src="{{ url('/') }}/new/img/halil-coskun.webp" />
							<h2 class="team__member__name">Prof. Dr. Halil Coşkun</h2>
							<span class="team__member__job team__member__job--2">General Surgeon</span>
							<span class="team__member__job">
								Cleveland Clinic Bariatric Institute Laparoscopic and Robotic Obesity –Weill Cornell School
								Metabolic Surgeory
							</span>
						</div>
					</a>
					</div>
				</div>
				<div class="item">
					<div class="content mx-4">
					<a href="{{route('prof-dr-hakan-alagozlu')}}">
						<div class="team__member">
							<img class="team__member__avatar" src="{{ url('/') }}/new/img/hakanalagozlu.webp" />
							<h2 class="team__member__name">Prof. Dr. Hakan Alagözlü</h2>
							<span class="team__member__job team__member__job--2">Gastroenterologist</span>
							<span class="team__member__job">
								State University of New York Gastroenterology Department – Board Member of Probiotic
								Association Editor of “Microbiota” Book
							</span>
						</div>
					</a>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>
<section class="bilimkurulu team-web">
	<span class="bilimkurulu__title">{{ucwords(strtolower(__('pages/welcome.bilimkurulu_title')))}}</span>
	<div class="container">
		<div class="row team__row justify-content-center">
			<div class="col-12 col-md-6 col-lg-4 col-xl-4 team__col">
			<a href="{{route('prof-dr-tarkan-karakan')}}">
				<div class="team__member">
					<img class="team__member__avatar" src="{{ url('/') }}/new/img/tarkankarakan.webp" />
					<h2 class="team__member__name">Prof. Dr. Tarkan Karakan</h2>
					<span class="team__member__job team__member__job--2">Gastroenterologist</span>
					<span class="team__member__job">
						John Hopkins University – President of Probiotics Association – Board Member of
						Gastrointestinal Endoscopy Association – WGO Probiotics Guideline Committee Member	
					</span>
				</div>
			</a>
			</div>
			<div class="col-12 col-md-6 col-lg-4 col-xl-4 team__col">
				<a href="{{route('prof-dr-halil-coskun')}}">
				<div class="team__member">
					<img class="team__member__avatar" src="{{ url('/') }}/new/img/halil-coskun.webp" />
					<h2 class="team__member__name">Prof. Dr. Halil Coşkun</h2>
					<span class="team__member__job team__member__job--2">General Surgeon</span>
					<span class="team__member__job">
						Cleveland Clinic Bariatric Institute Laparoscopic and Robotic Obesity –Weill Cornell School
						Metabolic Surgeory
					</span>
				</div>
				</a>
			</div>
			<div class="col-12 col-md-6 col-lg-4 col-xl-4 team__col">
				<a href="{{route('prof-dr-hakan-alagozlu')}}">
				<div class="team__member">
					<img class="team__member__avatar" src="{{ url('/') }}/new/img/hakanalagozlu.webp" />
					<h2 class="team__member__name">Prof. Dr. Hakan Alagözlü</h2>
					<span class="team__member__job team__member__job--2">Gastroenterologist</span>
					<span class="team__member__job">
						State University of New York Gastroenterology Department – Board Member of Probiotic
						Association Editor of “Microbiota” Book
					</span>
				</div>
				</a>
			</div>
		</div>
	</div>
</section>
{{-- <section class="pt-5 text-center">
		<h2 class="maffects__title"><b>Our Roadmap</b></h2>
		<p class="ecosystem__desc">
			We aim to diagnose and treat diseases by balancing human microbiome
		</p>
		<div class="text-center"style="width: 80%; margin-left:10%">
			<img class="my-5 img-responsive" src="{{url('/new/landing1/road_map.webp')}}" alt="">
		 </div>
	</div>
</section> --}}
<section class="microbiome text-center" style="background-color:transparent;">
	<div class="container">
		<div class="row">
			<div class="col-md-12 ecosystem__item">
				<h2 class="maffects__title"><b>Our Roadmap</b></h2>
				<p class="ecosystem__desc">
					We aim to diagnose and treat diseases by balancing human microbiome
				</p>
				<div class="test-center">
					<img class="img-web" style="width: 100%" src="{{url('/new/landing1/road_map.webp')}}" alt="plan-yatirimci">
					<img class="img-mobile" style="width: 100%" src="{{url('/new/landing1/road_map_mobile.webp')}}" alt="plan-yatirimci">
				</div>
			</div>
		</div>
	</div>
</section>
 {{-- <section class="timeline pt-5 text-center"style="background-color: #f5fcff">
	<h2 class="maffects__title"><b>Our Roadmap</b></h2>
	<p class="ecosystem__desc">
		We aim to diagnose and treat diseases by balancing human microbiome
	</p>

	<ol>
		<li>
			<div>
				<time>2018</time>
				<span>Establishment</span>
			</div>
		</li>
		<li>
			<div>
				<time>2019</time>
				<span>Product Devolopment</span><br>
				<span>Beta Tests</span>
			</div>
		</li>
		<li>
			<div>
				<time>2020</time>
				<span>ENBIOSIS Daily <br>(Personalized Prebiotic)</span><br>
				<span>Probiotic Suggestion Engine</span>
			</div>
		</li>
		<li>
			<div>
				<time>2021</time>
				<span>Pregnancy Risks Diagnosis Kit</span><br>
				<span>Personalized Metabolism Diets</span>
			</div>
		</li>
		<li>
			<div>
				<time>2022</time>
				<span>Early Disease Diagnosis Kits</span>
				<span>Bacteriophage Therapy Solutions</span>
				<span>Personal Probiotics</span>
			</div>
		</li>
		<li>
			<div>
				<time>2023</time>
				<span>Personalized Therapy Solutions</span>
			</div>
		</li>
		<li></li>
	</ol>
	<div class="arrows">
	  <button class="arrow arrow__prev disabled" disabled>
		<img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/162656/arrow_prev.svg" alt="prev timeline arrow">
	  </button>
	  <button class="arrow arrow__next">
		<img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/162656/arrow_next.svg" alt="next timeline arrow">
	  </button>
	</div>
  </section>
 <section class ="timeline-mobile-display" style="background-color: #f5fcff">
	<div class="container">
		<div class="row">
			<div class="col-md-12 ecosystem__item">
				<h2 class="maffects__title"><b>Our Roadmap</b></h2>
			<div id="timeline-content">
				<ul class="timeline-mobile">
					<li class="event">
						<h2>2018</h2>
						<h3>Establishment</h3>
					</li>
					<li class="event">
						<h2>2019</h2>
						<h3>Product Devolopment</h3>
						<h3>Beta Tests</h3>
					</li>
					<li class="event">
						<h2>2020</h2>
						<h3>Personelized Nutrition Market Enterance</h3>
						<h3>Personelized Food Supplement Production</h3>
					</li>
					<li class="event">
						<h2>2021</h2>
						<h3>Preterm Birth Diagnosis Kit</h3>
						<h3>Personelized Blood Sugar Nutrition Suggestions</h3>
					</li>
					<li class="event">
						<h2>2022</h2>
						<h3>Early Diagnosis Kit Product</h3>
						<h3>Personalized Bıotherapeutic Production</h3>
						<h3>Personalized Bıotherapeutic Production</h3>
					</li>
					<li class="event">
						<h2>2023</h2>
						<h3>Personalized Therapy Solutions</h3>
					</li>
				</ul>
			  </div>
			</div>
		</div>
	</div>
</section> --}}

<section class="microbiome text-center" style="background-color:transparent;">
	<div class="container">
		<div class="row">
			<div class="col-md-12 ecosystem__item">
				<h2 class="ecosystem__title">
				</h2>
				<h2 class="maffects__title">
					<b>
						ENBIOSIS Biotechnology In Global
					</b>
				</h2>
				<p class="ecosystem__desc">
					In addition to providing personalized diet and dietitian services, ENBIOSIS Biotechnology offers artificial intelligence technology to companies that want to provide similar services by positioning as a global platform.
					<br/><br/>
					Some of the countries where we provide our technological infrastructure:
				</p>
				<div class="test-center">
					<img style="width: 100%" src="{{url('/new/img/yatirimci-map.webp')}}" alt="plan-yatirimci">
				</div>
			</div>
		</div>
	</div>
</section>

<section class="en-carousel mt-3" >
	<div class="container">
		 <div class="row">
			 <div class="col-md-12">
				 <h2 class="maffects__title">
					 <b>
						Our Local References
					 </b>
				 </h2>
			 </div>
		 </div>
		 <div class="row owl-carousel end-carousel owl-theme top-0">
			<div class="item">
				<img src="{{ url('/') }}/new/landing1/ref/ppd.webp"/>
			</div>
			 <div class="item">
				 <img src="{{ url('/') }}/new/landing1/ref/liv.webp"/>
			 </div>
			 <div class="item">
				<img src="{{ url('/') }}/new/landing1/ref/medicana.webp"/>
			</div>
		 </div>
	</div>
</section>
<section class="contact-form my-0">

	<div class="col-md-12">
		<h2 class="maffects__title">
			<b>
				Contact Us
			</b>
		</h2>
		<p class="contact-form__sub_title">
			If you want to get more information about ENBIOSIS Biotechnology, you can fill out the form. We’ll be in touch as soon as possible.
		</p>
		<div class="container">
			{!! Form::open(['method'=>'POST', 'action' => 'GeneralController@sendMail', 'class' => 'row']) !!}
			<div class="form-group col-md-6 contact-form__input">
				
				{!! Form::text('name', null, ['class'=>'form-control', 'id' => 'form_adsoyad', 'placeholder' => __('pages/iletisim.adiniz_ve_soyadiniz', [], 'en')]) !!}
				@error('name')
					<small id="emailHelp" class="form-text text-muted">{{$message}}</small>
				@enderror
			
			</div>

			<div class="form-group col-md-6 contact-form__input">
				
				{!! Form::text('email', null, ['class'=>'form-control', 'id' => 'form_email', 'aria-describedby' => 'emailHelp', 'placeholder' => __('pages/iletisim.e_posta_adresiniz', [], 'en')]) !!}
				@error('email')
					<small id="emailHelp" class="form-text text-danger">{{$message}}</small>
				@enderror
			</div>

			<div class="form-group col-md-12 contact-form__input">

				{!! Form::text('subject', null, ['class'=>'form-control', 'id' => 'form_baslik', 'aria-describedby' => 'emailHelp', 'placeholder' => 'Phone Number']) !!}
				@error('subject')
					<small id="emailHelp" class="form-text text-danger">{{$message}}</small>
				@enderror
			</div>

			<div class="form-group col-md-12 buy__input">

				<textarea name="mesaj" class="form-control" id="form_mesaj" placeholder="{{__('pages/iletisim.mesaj', [], 'en')}}">{{old('mesaj')}}</textarea>
				@error('mesaj')
					<small id="emailHelp" class="form-text text-danger">{{$message}}</small>
				@enderror
			</div>

			<div class="col-md-12 d-flex justify-content-center">

				{!! Form::submit(__('pages/iletisim.gonder', [], 'en'), ['class'=>'link-btn link-btn--orange form-button form-send']) !!}
			
			</div>
			
			{!! Form::close() !!} 
		</div>
	</div>
</section>

@endsection

@section('scripts')
	<script>
		$(document).ready(function(){
			$('.end-carousel').owlCarousel({
				loop:true,
				autoplay:true,
				autoplayTimeout:6000,
				margin:150,
				nav:false,
				dots:false,
					responsive:{
					0:{
						items:1,
						nav:true,
						dots:true,
						navText:['<img src="{{ url("/") }}/new/img/slide-prev.webp"/>','<img src="{{ url("/") }}/new/img/slide-next.webp"/>'],
					},
					600:{
						items:2
					},
					1000:{
						items:3
					}
				}
			});
			$('.basin-carousel').owlCarousel({
				loop:true,
				autoplay:true,
				autoplayTimeout:6000,
				margin:20,
				nav:true,
				navText:['<img src="{{ url("/") }}/new/img/slide-prev-black.webp"/>','<img src="{{ url("/") }}/new/img/slide-next-black.webp"/>'],
					responsive:{
					0:{
						items:1,
						nav:true,
						dots:true,
						navText:['<img src="{{ url("/") }}/new/img/slide-prev-black.webp"/>','<img src="{{ url("/") }}/new/img/slide-next-black.webp"/>'],
					},
					600:{
						items:2,
						dots:true,
					},
					1000:{
						items:4
					}
				}
			});
			$('.team-carousel').owlCarousel({
				loop:true,
				autoplay:false,
				autoplayTimeout:6000,
				margin:20,
				nav:true,
				dots:true,
				navText:['<img src="{{ url("/") }}/new/img/slide-prev.webp"/>','<img src="{{ url("/") }}/new/img/slide-next.webp"/>'],
					responsive:{
					0:{
						items:1,
						nav:true,
						dots:true,
						navText:['<img src="{{ url("/") }}/new/img/slide-prev.webp"/>','<img src="{{ url("/") }}/new/img/slide-next.webp"/>'],
					},
					600:{
						items:2,
						dots:true,
					},
					1000:{
						items:4
					}
				}
			});
		});
	</script>
@endsection
