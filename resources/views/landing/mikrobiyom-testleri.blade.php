@extends('layouts.public')

@section('pageTitle','Mikrobiyota İle Sağlıklı Yaşam | Enbiosis')
@section('pageDescription','ENBIOSIS Mikrobiyom analizi ile bağırsak bakterilerini öğren')


@section('content')
<section class="header mbt__1">
	<div class="container">
		<div class="row">
			<div class="col-7 col-md-7 col-lg-6 col-xl-6">
				<span class="header__slogan mbt__2"><b>ENBIOSIS İLE MİKROBİYOMUNU KEŞFETME ZAMANI GELDİ!</b></span>
				<p class="header__desc">
					Mikrobiyom dünyanı keşfederek sorunlarının kaynağına inmek, sağlığını kontrol eden bakterilerinle tanışmak ve onları yönetmek artık senin elinde!
				</p>
				<a class="link-btn link-btn--orange" href="{{url('/checkout')}}"
				onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'button', Label: '{{slug('landing_page')}}' })">
				Hemen Satın Al</a>
			</div>
		</div>
	</div>
</section>
<section class="header header--mobile" style="background-image: url(/new/img/manaliz-mobile.webp);background-color:white;padding-bottom:0;">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-12 col-lg-12 col-xl-12" style="    text-align: center;">
				<h1 class="header__slogan"><b>ENBIOSIS İLE MİKROBİYOMUNU KEŞFETME ZAMANI GELDİ!</b></h1>
				<p class="header__desc">
					Mikrobiyom dünyanı keşfederek sorunlarının kaynağına inmek, sağlığını kontrol eden bakterilerinle tanışmak ve onları yönetmek artık senin elinde!
				</p>
				<a class="link-btn link-btn--orange" href="{{url('/checkout')}}"
				onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'button', Label: '{{slug('landing_page')}}' })">
				Hemen Satın Al</a>
			</div>
		</div>
	</div>
</section>

<section class="bg-none">
	<div class="container">
		<div class="row">
			<div class="col-12">
				@include('include.video.kesfet', ['id' => 'kesfet'])
			</div>
		</div>
	</div>
</section>

<section class="mictobiome-balance">
	<section class="microbiome-analysis microbiome-analysis--2">
		<div class="container">
			<div class="row">
				<div class="col-md-12 mictobiome-balance-mobile">
					<img src="/new/landing1/mikrobiyom2-mobile.webp" style="width:100%;height:auto;margin-bottom:70px;"/>
				</div>
				<div class="col-md-12 col-lg-6 col-xl-6">
					<h2 class="microbiome-analysis__title"><b>MİKROBİYOM NEDİR ?</b></h2>
					<p class="microbiome-analysis__desc">
						Vücudumuz doğduğumuz andan itibaren bize eşlik eden, sağlığımız ve yaşam kalitemiz üzerinde tartışılmaz öneme sahip trilyonlarca mikroorganizmaya ev sahipliği yapmaktadır. Bu mikroskobik canlılar ile birlikte genetik materyalleri, çevreyle etkileşimleri ve yaşadıkları habitatın bütünü mikrobiyomu oluşturur. Mikrobiyomunun çok büyük kısmı bakterilerden oluşmaktadır. Bu sebeple, bakterilerinin dağılımı ve çeşitliliği mikrobiyom dengesinde kritik bir öneme sahiptir.
					</p>
				</div>
			</div>
		</div>
	</section>
</section>
<section class="discover discover--microbiome mbt__4">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="discover__title bagisiklik__t" style="font-size:30px;">ENBIOSIS <b>MİKROBİYOM</b> ANALİZİ İLE KİŞİSELLEŞTİRİLMİŞ BESLENMENİN GÜCÜ</h2>
			</div>
			<div class="col-md-7">
				<p class="discover__desc">
					Mikrobiyomunun çeşitliliği ve dengesi sağlık durumun hakkında önemli ipuçları vermektedir. Yaşam tarzının ve çevresel faktörlerin etkisi ile zamanla mikrobiyomun değişir ve sana özgü bir yapıya dönüşür. Bu değişimler sağlık durumunu olumlu veya olumsuz yönde etkileyebilir. ENBIOSIS Mikrobiyom Analizi ile mikrobiyom profilinin sağlık durumunu öğrenebilir ve sana sunduğumuz Kişiselleştirilmiş Beslenme Rehberi ile mikrobiyomunu sağlıklı hale getirecek olan besinleri öğrenebilirsin! 
				</p>
			</div>
			<div class="col-md-12">
				<a class="link-btn link-btn--orange" href="{{url('/checkout')}}"
				onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'button', Label: '{{slug('landing_page')}}' })">
				ŞİMDİ DENE</a>
				<a class="link-btn link-btn--border" href="{{url('/')}}/Microbiome_Demo_TR.pdf">DEMO RAPORU</a>
			</div>
		</div>
	</div>
</section>
@include('include.banner')
<section class="discover discover--microbiome mbt__5">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="discover__title" style="font-size:26px;text-align:center;display:block;">ENBIOSIS MİKROBİYOM ANALİZİ VE KİŞİSELLEŞTİRİLMİŞ BESLENME REHBERİ İLE FARKI HİSSEDİN</h2>
			</div>
			<div class="col-md-12">
				<p class="discover__desc" style="text-align:center;">
					Kolayca elde edeceğin Mikrobiyom Analiz Raporu sayesinde bağırsağındaki bakterilerin miktarlarını ve dengesini keşfedebilir, bağırsak skorlarını öğrenebilirsin! İşte onlardan bazıları:
				</p>
			</div>

		</div>
		<section class="report-item mbt__6">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-md-6 report-item">
						<div class="report-item__item">
							<div class="report-item__icon">
								<img src="/new/landing1/icon3.webp">
							</div>
							<div class="report-item__content">
								<span>Vitamin Sentezi</span>
								<span>
									Vitamin Sentezi Skoru ile vitamin sentezinden sorumlu bakterilerinin durumunu öğrenebilirsin.
								</span>
							</div>
						</div>
					</div>
					<div class="col-md-6 report-item">
						<div class="report-item__item">
							<div class="report-item__icon">
								<img src="/new/landing1/icon4.webp">
							</div>
							<div class="report-item__content">
								<span>Laktoz Hassasiyeti</span>
								<span>
									Laktoz Hassasiyeti Skoru ile mikrobiyomuna dayalı laktoz hassasiyet eğilimini öğrenebilirsin.
								</span>
							</div>
						</div>
					</div>
					<div class="col-md-6 report-item">
						<div class="report-item__item">
							<div class="report-item__icon">
								<img src="/new/landing1/icon5.webp">
							</div>
							<div class="report-item__content">
								<span>Glüten Hassasiyeti</span>
								<span>
									Glüten Hassasiyeti Skoru ile mikrobiyomuna dayalı glüten hassasiyet eğilimini öğrenebilirsin.
								</span>
							</div>
						</div>
					</div>
					<div class="col-md-6 report-item">
						<div class="report-item__item">
							<div class="report-item__icon">
								<img src="/new/landing1/icon6.webp">
							</div>
							<div class="report-item__content">
								<span>Şeker Tüketimi</span>
								<span>
									Şeker Tüketim Skoru ile mikrobiyomunun şeker tüketiminden ne ölçüde etkilendiğini öğrenebilirsin.
								</span>
							</div>
						</div>
					</div>
					<div class="col-md-6 report-item">
						<div class="report-item__item">
							<div class="report-item__icon">
								<img src="/new/landing1/icon7.webp">
							</div>
							<div class="report-item__content">
								<span>Yapay Tatlandırıcı Hasarı</span>
								<span>
									Yapay Tatlandırıcı Hasarı Skoru ile mikrobiyomunun yapay tatlandırıcı tüketiminden ne ölçüde etkilendiğini öğrenebilirsin.
								</span>
							</div>
						</div>
					</div>

					<div class="col-md-6 report-item">
						<div class="report-item__item">
							<div class="report-item__icon">
								<img src="/new/landing1/icon8.webp">
							</div>
							<div class="report-item__content">
								<span>Probiyotik Mikroorganizmalar</span>
								<span>
									Probiyotik Mikroorganizmalar Skoru ile mikrobiyomunda bulunan bu bakteri grubunun varlığı ve miktarı hakkında bilgi sahibi olabilirsin.
								</span>
							</div>
						</div>
					</div>
					<div class="col-md-6 report-item">
						<div class="report-item__item">
							<div class="report-item__icon">
								<img src="/new/landing1/icon9.webp">
							</div>
							<div class="report-item__content">
								<span>Bağırsak Hareketliliği</span>
								<span>
									Bağırsak Hareketliliği Skoru ile mikrobiyomunun bağırsak hareketliliğin üzerindeki potansiyel etkisini öğrenebilirsin.
								</span>
							</div>
						</div>
					</div>
					<div class="col-md-6 report-item">
						<div class="report-item__item">
							<div class="report-item__icon">
								<img src="/new/landing1/icon10.webp">
							</div>
							<div class="report-item__content">
								<span>Antibiyotik Hasarı</span>
								<span>
									Antibiyotik Hasarı Skoru ile mikrobiyomunda antibiyotik tüketimine bağlı olarak meydana gelen hasarı öğrenebilirsin.
								</span>
							</div>
						</div>
					</div>
					<div class="col-md-6 report-item">
						<div class="report-item__item">
							<div class="report-item__icon">
								<img src="/new/landing1/icon11.webp">
							</div>
							<div class="report-item__content">
								<span>Otoimmünite İndeksi</span>
								<span>
									Otoimmünite İndeksi Skoru ile otoimmün hastalıklara yönelik potansiyel riskini öğrenebilirsin.
								</span>
							</div>
						</div>
					</div>
					<div class="col-md-6 report-item">
						<div class="report-item__item">
							<div class="report-item__icon">
								<img src="/new/landing1/icon12.webp">
							</div>
							<div class="report-item__content">
								<span>Uyku Kalitesi</span>
								<span>
									Uyku Kalitesi Skoru ile mikrobiyomunun kaliteli bir uyku düzeni için ne kadar elverişli olduğunu öğrenebilirsin.
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<div class="row d-flex justify-content-center">
			<div class="col-md-8 discover__quote">
				<span><i>Mikrobiyom dünyanı keşfederek sorunlarının kaynağına inmek, sağlığını kontrol eden bakterilerinle tanışmak ve onları yönetmek artık senin elinde!</i></span><br><br>
			</div>
		</div>
	</div>
</section>
<section class="microbiome-analysis">
	<div class="container">
		<div class="row">
			<div class="col-md-12 microbiome-analysis-bg-mobile">
				<img src="/new/img/microbiome-analysis-bg-mobile.webp" style="width:100%;height:auto;margin-bottom:70px;"/>
			</div>
			<div class="col-md-12 col-lg-6 col-xl-6">
				<h2 class="microbiome-analysis__title bagisiklik__t" style="font-size:32px;">KİMLER ENBIOSIS MİKROBİYOM ANALİZİ YAPTIRMALIDIR?</h2>
				<div class="microbiome-analysis__quote mbt__9" style="margin-bottom:30px;">
					<span><i>Tüm hastalıklar bağırsakta başlar. Bağırsak hasta ise vücudun geri kalanı da hastadır.</i></span>
					<span><i>Hipokrat</i></span>
				</div>
				<p class="microbiome-analysis__desc">
					Mikrobiyomundaki çeşitliliğin azalması ve dengesinin bozulması ile ortaya çıkan disbiyozis; sindirim sistemi hastalıkları, besin hassasiyetleri, otoimmün hastalıklar, obezite, cilt problemleri, düşük uyku kalitesi ve mental rahatsızlıklar ile ilişkilidir. Sen de bu sağlık problemlerinden muzdaripsen mikrobiyom analizi yaptırmanın zamanı gelmiş olabilir! 
				</p>
			</div>
			<div class="col-md-12 mbt__10" style="margin-top:20px;">
				<a class="link-btn link-btn--orange" href="{{url('/checkout')}}"
				onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'button', Label: '{{slug('landing_page')}}' })">
				ŞİMDİ DENE</a>
				<a class="link-btn link-btn--border" href="{{url('/')}}/Microbiome_Demo_TR.pdf">DEMO RAPORU</a>
			</div>
		</div>
	</div>
</section>

<section class="hprocess hprocess--home mbt__7">
	<div class="container">
		<div class="row hprocess__howto">
			<span class="hprocess__howtotitle">SÜREÇ NASIL İŞLER?</span>
			<div class="col-12 mb-3">
				@include('include.video.surec', ['id' => 'surec'])
			</div>
			<div class="col-md-6 col-lg-3 col-xl-3 hprocess__step">
				<div class="hprocess__number">
					<img src="{{ url('/') }}/new/img/icons/surec1.webp" width="50"/>
				</div>
				<h2 class="hprocess__steptitle">ENBIOSIS MİKROBİYOM ANALİZİ SİPARİŞİ</h2>
				<p class="hprocess__stepdesc">
					www.enbiosis.com 'dan sipariş ver, sana gelen kutudaki kod ile web sitesinden kaydını oluştur ve anket sorularını tamamla.
				</p>
			</div>
			<div class="col-md-6 col-lg-3 col-xl-3 hprocess__step">
				<div class="hprocess__number">
					<img src="{{ url('/') }}/new/img/icons/surec2.webp" width="50"/>
				</div>
				<h2 class="hprocess__steptitle">NUMUNE ALIMI</h2>
				<p class="hprocess__stepdesc">
					Kargo ile adresine teslim edilen Mikrobiyom Kitinin içerisinde yer alan adımları takip et ve mikrobiyom analizin için gerekli gaita numuneni kolayca al. Sonrasında numuneni kutuda yer alan adrese ücretsiz bir şekilde gönder.
				</p>
			</div>
			<div class="col-md-6 col-lg-3 col-xl-3 hprocess__step">
				<div class="hprocess__number">
					<img src="{{ url('/') }}/new/img/icons/surec3.webp" width="50"/>
				</div>
				<h2 class="hprocess__steptitle">LABORATUVAR ANALİZİ</h2>
				<p class="hprocess__stepdesc">
					Numunen Metagenom laboratuvarımızda işleme alınarak 16S rRNA yeni nesil dizileme yöntemi ile dizilenir. Elde edilen veriler yapay zeka algoritmalarımız ile ayrıntılı olarak analiz edilir ve raporlandırılır.
				</p>
			</div>
			<div class="col-md-6 col-lg-3 col-xl-3 hprocess__step">
				<div class="hprocess__number">
					<img src="{{ url('/') }}/new/img/icons/surec4.webp" width="50"/>
				</div>
				<h2 class="hprocess__steptitle">ENBIOSIS KİŞİSELLEŞTİRİLMİŞ BESLENME REHBERİ</h2>
				<p class="hprocess__stepdesc">
					Raporların uzmanlarımız tarafından kontrol edildikten sonra sana ulaştırılır. Besin skorların ENBIOSIS diyetisyenleri tarafından yorumlanır ve diyetisyen takibi ile birlikte sana sunulur.
				</p>
			</div>
		</div>
	</div>
</section>

<section class="nutrition-guide">
	<div class="container">
		<div class="row nutrition-guide__title-row">
			<div class="col-md-12">
				<span class="nutrition-guide__title" style="display: flex;width: 100%;justify-content: center;"><b>MİKROBİYOM ANALİZİ<br/>&<br/>KİŞİSELLEŞTİRİLMİŞ BESLENME REHBERİ</b></span>
				<p class="nutrition-guide__desc">
					ENBIOSIS kapsamlı bir sağlık paketi sunarak sorunun kaynağına iner ve sizin için kalıcı çözüm önerileri oluşturur.
				</p>
			</div>

		</div>
		<div class="row">
			<div class="col-md-4 triple-box__box">
				<img src="{{url('/')}}/new/img/triple-icon1.webp" />
				<span>
					Mikrobiyom Analizi İle Kendini Tanı
				</span>
			</div>
			<div class="col-md-4 triple-box__box">
				<img src="{{url('/')}}/new/img/triple-icon2.webp" />
				<span>
					Sana Sunduğumuz Kişiselleştirilmiş Beslenme Önerilerini Takip Et

				</span>
			</div>
			<div class="col-md-4 triple-box__box">
				<img src="{{url('/')}}/new/img/triple-icon4.webp" />
				<span>
					Dengeli Mikrobiyom Profiline Ulaş Ve Farkı Hisset!
				</span>
			</div>
		</div>
	</div>
</section>
<section class="bilimkurulu">
	<span class="bilimkurulu__title">ENBIOSIS Bilim Kurulu</span>
	<div class="container">
		<div class="row team__row justify-content-center">
			<div class="col-12 col-md-6 col-lg-4 col-xl-4 team__col">
				<a href="{{route('prof-dr-hakan-alagozlu')}}">
					<div class="team__member">
						<img class="team__member__avatar" src="{{ url('/') }}/new/img/hakanalagozlu.webp" />
						<h2 class="team__member__name">Prof. Dr. Hakan Alagözlü</h2>
						<span class="team__member__job team__member__job--2">Gastroenteroloji Uzmanı</span>
						<span class="team__member__job">Gazi Üniversitesi Tıp Fakültesi –  Probiyotik Prebiyotik Derneği kurucu üyesi – Amerika Birleşik Devletleri New York SUNY Üniversitesi –  İstinye Üniversitesi Mikrobiyota çalışmaları –  Mikrobiyota kitabı editörü</span>
					</div>
				</a>
			</div>
			<div class="col-12 col-md-6 col-lg-4 col-xl-4 team__col">
				<a href="{{route('prof-dr-tarkan-karakan')}}">
					<div class="team__member">
						<img class="team__member__avatar" src="{{ url('/') }}/new/img/tarkankarakan.webp" />
						<h2 class="team__member__name">Prof. Dr. Tarkan Karakan</h2>
						<span class="team__member__job team__member__job--2">Gastroenteroloji Uzmanı</span>
						<span class="team__member__job">Hacettepe Üniversitesi Tıp Fakültesi – Johns Hopkins Üniversitesi – Probiyotik Prebiyotik Derneği Başkanı – Gastrointestinal Endoskopi Derneği yönetim kurulu üyesi – WGO Probiotics Guideline Komitesi – TÜBA Beslenme çalışma grubu üyesi – Tarım Bakanlığı Yeni Gıda Destekleri ve Güvenliği Komitesi</span>
					</div>
				</a>
			</div>
			<div class="col-12 col-md-6 col-lg-4 col-xl-4 team__col">
				<a href="{{route('prof-dr-halil-coskun')}}">
					<div class="team__member">
						<img class="team__member__avatar" src="{{ url('/') }}/new/img/halil-coskun.webp" />
						<h2 class="team__member__name">Prof. Dr. Halil Coşkun</h2>
						<span class="team__member__job team__member__job--2">Genel Cerrahi Uzmanı</span>
						<span class="team__member__job">Ankara Üniversitesi Tıp Fakültesi – İstanbul Üniversitesi – ABD Cleveland Tıp Merkezi Bariatrik Enstitüsü - Laparoskopik ve Robotik Obezite – Metabolik Cerrahi –  ABD Weill Cornell Tıp Fakültesi – Bezmi Alem Üniversitesi Genel Cerrahi Anabilim Dalı Kurucu Öğretim Üyesi</span>
					</div>
				</a>
			</div>
		</div>
	</div>
</section>
@include('include.basin')
<section class="discover">
	<div class="container">
		<div class="row">
			<div class="col-md-5">
				<span class="discover__title">MİKROBİYOMUNU KEŞFET</span>
				<p class="discover__desc">
					Sürdürülebilir ve sağlıklı bir yaşamın anahtarı mikrobiyomunda gizli. ENBIOSIS Mikrobiyom Analizi ile bu dünyayı keşfetmek artık senin elinde!
				</p>

			</div>
		</div>
		<a class="link-btn link-btn--orange" href="{{url('/checkout')}}"
		onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'button', Label: '{{slug('landing_page')}}' })">
		ŞİMDİ DENE</a>
	</div>
</section>
@endsection
@section('scripts')
<script type="text/javascript">
	$(document).ready(function(){

		$('.testimonials-slide').owlCarousel({
			loop:true,
			margin:10,
			nav:true,
			navText:['<img src="{{ url("/") }}/new/img/slide-prev.webp"/>','<img src="{{ url("/") }}/new/img/slide-next.webp"/>'],
			dots:false,
			items:1
		})
	});
	$(window).resize(function(){
		//$('.header').height($(window).height());
		$('.mask').height($(window).height());
	});
</script>
@endsection
