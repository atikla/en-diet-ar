<!DOCTYPE html>
<html>
    <head>
        <title>Be right back.</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #B0BEC5;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 3rem;
                margin-bottom: 40px;
            }
            .left {
                text-align: left;
            }
            .none {
                text-decoration: none;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">You are not authorized for this route. </div>
                <div class="title" style="color:red">Yor Are {{auth()->user()->strRole}}</div>
                <ol class="title left">
                    <li><a class="none" href="{{ route('admin.index') }}"> Super Admin       </a> </li>
                    <li><a class="none" href="{{ route('admin.index') }}"> Operation Admin   </a></li>
                    <li><a class="none" href="{{ route('admin.index') }}"> Laboratory Admin  </a> </li>
                    <li>
                        <a class="none" href="{{ route('logout') }}" 
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                        Logout
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                </ol>
            </div>
        </div>
    </body>
    @include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])
</html>
