@if (\Session::has('created'))
    <div id="message" class="col-lg-6 col-md-8 col-sm-12 col-xs-12 px-0 mx-0">
        <div class="card my-3 ">
            <div class="card-header bg-success text-light">info</div>
            <div class="card-body ">
                <ul>
                    <li>{{\Session::get('created')}}</li>
                </ul>
            </div>
        </div>
    </div>
@endif

@if (\Session::has('updated'))
    <div id="message" class="col-lg-6 col-md-8 col-sm-12 col-xs-12 px-0 mx-0">
        <div class="card my-3 ">
            <div class="card-header bg-info text-light">info</div>
            <div class="card-body ">
                <ul>
                    <li>{{\Session::get('updated')}}</li>
                </ul>
            </div>
        </div>
    </div>
@endif

@if (\Session::has('deleted_soft'))
    <div id="message" class="col-lg-6 col-md-8 col-sm-12 col-xs-12 px-0 mx-0">
        <div class="card my-3 ">
            <div class="card-header bg-warning text-dark">info</div>
            <div class="card-body ">
                <ul>
                    <li>{{\Session::get('deleted_soft')}}</li>
                </ul>
            </div>
        </div>
    </div>
@endif

@if (\Session::has('deleted'))
    <div id="message" class="col-lg-6 col-md-8 col-sm-12 col-xs-12 px-0 mx-0">
        <div class="card my-3 ">
            <div class="card-header bg-danger text-light">info</div>
            <div class="card-body ">
                <ul>
                    <li>{{\Session::get('deleted')}}</li>
                </ul>
            </div>
        </div>
    </div>
@endif

@if (\Session::has('restored'))
    <div id="message" class="col-lg-6 col-md-8 col-sm-12 col-xs-12 px-0 mx-0">
        <div class="card my-3 ">
            <div class="card-header bg-info text-light">info</div>
            <div class="card-body ">
                <ul>
                    <li>{{\Session::get('restored')}}</li>
                </ul>
            </div>
        </div>
    </div>
@endif

@if (\Session::has('jsonkit'))
    <div id="message-1" class="col-12 px-0 mx-0">
        <div class="card my-3 ">
            <div class="card-header bg-info text-light">info</div>
            <div class="card-body">
                <div class="row">
                    <div class="col-4">
                        <ul>
                            <li>Added
                                <ol>
                                    @foreach (session('jsonkit')['added'] as $key => $item)
                                        <li>{{ $key }} {{ $item }}</li>
                                    @endforeach
                                </ol>
                            </li>
                        </ul>
                    </div>
                    <div class="col-4">
                        <ul>
                            <li>modified
                                <ol>
                                    @foreach (session('jsonkit')['modified'] as $key => $item)
                                        <li>{{ $key }} {{ $item }}</li>
                                    @endforeach
                                </ol>
                            </li>
                        </ul>
                    </div>
                    <div class="col-4">
                        <ul>
                            <li>denied
                                <ol>
                                    @foreach (session('jsonkit')['denied'] as $key => $item)
                                        <li>{{ $key }} {{ $item }}</li>
                                    @endforeach
                                </ol>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif