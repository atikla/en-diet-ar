@if (!request()->cookie('accept_cookies'))
    <div class="cookie-consent-banner">
        <div style="font-size: 30px; position: fixed; right: 20px;cursor: pointer" onclick="dismis()">&times;</div>
        <div class="cookie-consent-banner__inner">
        <div class="cookie-consent-banner__copy">
            <div class="cookie-consent-banner__header">
               {{__('pages/layout.cookie_title')}}
            </div>
            <div class="cookie-consent-banner__description">
                {!!__('pages/layout.cookie_desc')!!}
            </div>
        </div>
        <div class="cookie-consent-banner__actions">
            <a href="javascript:void(0);" onclick="setcooies();"class="cookie-consent-banner__cta"
            style="padding: 11px 0px;">
                {{__('pages/layout.cookie_ok')}}
            </a>
            <script>
                setTimeout(function(){
                    $(document).ready(function(){
                        $('.cookie-consent-banner').animate({bottom: '0',}, 1500);
                    });
                }, 3000);
                function setcooies() {
                    $.ajax({
                        url: '{{ route('setcookie') }}',
                        method: "POST",
                        data: {_token: '{{ csrf_token() }}'},
                        success: function (response) {
                            $('.cookie-consent-banner').animate({bottom: '-1000px',}, 2500);
                        }
                    });
                }
                function dismis(){
                    $('.cookie-consent-banner').animate({bottom: '-1000px',}, 2500);
                }
            </script>
            {{-- <a href="{{route('cookies-policy')}}" target="_blank" class="cookie-consent-banner__cta cookie-consent-banner__cta--secondary"
            style="padding: 11px 0px;">
                {{__('pages/layout.cookie_more')}}
            </a> --}}
        </div>
        </div>
    </div>
@endif