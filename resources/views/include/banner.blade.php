<section class="mictobiome-balance section_height @if (isset($mt)) mt-0 @endif" style="background-color: #65cde6; color:#FFF; margin-top:3rem; direction: rtl">
	<div class="container">
		<div class="row pb-3" style="padding-top: 20px">
			<div class="col-lg-6">
				<h5>
					لمعرفة معلومات مفصلة أكثر عن En-diet الرجاء الاتصال بنا.
				</h5>
			</div>
			<div class="col-lg-6 text-center" style="direction: ltr">
				<h5>
					<a 	href="tel:+90 850 840 77 93"
					style="color:#FFF">
					+90 (850) 840 77 93
				</a> 
				</h5>
			</div>
		</div>
	</div>
</section>
<section class="mictobiome-balance section_height"  style="background-color: #e8957e; color:#FFF">
	<div class="container">
		<div class="row py-3">
			<div class="col-lg-6 my-auto text-center" style="direction: ltr">
				<div class="footer__socials mb-0 margin_left">
					<ul>
						<li>
							<a href="https://www.facebook.com/enbiosis/" target="_blank">
								<img src="{{ url('/') }}/new/img/facebook-icon.webp" />
							</a>
						</li>
						<li>
							<a href="https://www.instagram.com/enbiosis/" target="_blank">
								<img src="{{ url('/') }}/new/img/instagram-icon.webp" />
							</a>
						</li>
						<li>
							<a href="https://www.youtube.com/channel/UCLmsr81DJ7euqFc_Rgdn4zA" target="_blank">
								<img src="{{ url('/') }}/new/img/youtube-icon.webp" />
							</a>
						</li>
						<li>
							<a href="https://www.linkedin.com/company/enbiosis" target="_blank">
								<img src="{{ url('/') }}/new/img/linkedin-icon.webp" />
							</a>
						</li>
						<li>
							<a href="https://twitter.com/enbiosis" target="_blank">
								<img src="{{ url('/') }}/new/img/twitter-icon.webp" />
							</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="col-lg-6 my-auto">
				<h5>
					لتبقى على اطلاع على آخر التحديثات و التفاصيل الرجاء متابعتنا   
				</h5>
			</div>
		</div>
	</div>
</section>