@if (count($errors) > 0)

    <div id="_enbiosis_errors_enbiosis_"class="col-lg-6 col-md-8 col-sm-12 col-xs-12 px-0 mx-0">
        <div class="card my-3 ">
            <div class="card-header bg-danger text-light">{{__('app.Errors')}}</div>
            <div class="card-body ">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endif
