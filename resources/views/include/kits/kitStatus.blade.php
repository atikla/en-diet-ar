<div class="col-lg-6 col-12 mt-3">
    <div class="card">
        <div class="card-header bg-info text-light">Kit's Status</div>
        <div class="card-body ">
            <p>
                current status <span class="text-info" style="font-size: 1.5rem">{{$kit->kitStatuse}}</span>
            </p>
            <p>
               Change Status
            </p>
            <div class="btn-group" role="group" aria-label="Basic example">
                <button data="{{$kit->kit_code}}" data1="{{$kit->id}}" type="button" class="btn btn-secondary delever" {{$kit->kit_status == 0 && $kit->kit_code ? '' : 'disabled'}}>Delever to user</button>
                <button data="{{$kit->kit_code}}" data1="{{$kit->id}}" type="button" class="btn btn-info text-light received" {{$kit->kit_status == 1 && $kit->kit_code ? '' : 'disabled'}}>received from user</button>
                <button data="{{$kit->kit_code}}" data1="{{$kit->id}}" type="button" class="btn btn-warning text-dark sent" {{$kit->kit_status == 2 && $kit->kit_code ? '' : 'disabled'}}>sent to lab</button>
                <button data="{{$kit->kit_code}}" data1="{{$kit->id}}" type="button" class="btn btn-success text-light upload" {{$kit->kit_status == 3 && $kit->kit_code ? '' : 'disabled'}}>upload results</button>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-6 col-12 mt-3">
    <div class="card">
        <div class="card-header bg-info text-light">Kit's timing</div>
        <div class="card-body ">
            <ol>
            <li>Delevered to user at : {!! $kit->delevered_to_user_at ? $kit->delevered_to_user_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss') : '<span class="text-danger">Not Enterde Yet!</span>' !!}</li>
            <li>Received from user at : {!! $kit->received_from_user_at ? $kit->received_from_user_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss') : '<span class="text-danger">Not Enterde Yet!</span>' !!}</li>
            <li>Send to lab at : {!! $kit->send_to_lab_at ? $kit->send_to_lab_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss') : '<span class="text-danger">Not Enterde Yet!</span>' !!}</li>
            <li>Uploaded at : {!! $kit->uploaded_at ? $kit->uploaded_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss') : '<span class="text-danger">Not Enterde Yet!</span>' !!}</li>
            </ol>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function(){

        $('.delever').click(function(){
            $.ajax({
                url: '{{ route('admin.kits.delever') }}',
                method: "post",
                data: {_token: '{{ csrf_token() }}', kit_code: $( this ).attr('data'), kit_id:$( this ).attr('data1')},
                success: function (response) {
                    window.location.reload();
                },
                error: function (response) {
                    window.location.reload();
                }
            });
        });
        $('.received').click(function(){
            $.ajax({
                url: '{{  route('admin.kits.received') }}',
                method: "post",
                data: {_token: '{{ csrf_token() }}', kit_code: $( this ).attr('data'), kit_id:$( this ).attr('data1')},
                success: function (response) {
                    window.location.reload();

                },
                error: function (response) {
                    window.location.reload();
                }
            });
        });

        $('.sent').click(function(){
            $.ajax({
                url: '{{  route('admin.kits.sent') }}',
                method: "post",
                data: {_token: '{{ csrf_token() }}', kit_code: $( this ).attr('data'), kit_id:$( this ).attr('data1')},
                success: function (response) {

                    window.location.reload();
                },
                error: function (response) {

                    window.location.reload();
                }
            });
        });
        $('.upload').click(function(){
            window.location.href = '{{route('admin.kits.index')}}'
        });
    });
</script>