<div class="col-lg-6 col-12 mt-3">
    <div class="card">
        <div class="card-header bg-info text-light">Kit's Suggestion</div>

        <div class="card-body ">
            <div class="text-right">
                <button type="button" class="header-table btn btn-outline-success mb-2" data-toggle="modal" data-target="#create_suggestion ">
                    Create Suggestion 
                </button>
            </div>
            @if ( $kit->suggestion->count() > 0 )
                <table class="table table-responsive-lg table-striped">
                    <thead class="thead-light">
                        <tr>
                            <th> # </th>
                            <th> title </th>
                            <th> description </th>
                            <th>Created</th>
                            <th>Updated</th>
                            <th>process</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($kit->suggestion as $suggestion)
                            <tr>
                                <td> {{ $loop->iteration }} </td>
                                <td> {{ $suggestion->title }} </td>
                                <td> {!! str_limit( $suggestion->description, 15, '<a href="" data-toggle="modal" data-target="#id_suggestion_' . $suggestion->id . '"> Read More..</a>') !!} </td>
                                <td> {{ $suggestion->created_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss') }} </td>
                                <td> {{ $suggestion->updated_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss') }} </td> 
                                <td>
                                {{-- <button type="button" class="btn btn-outline-success update mr-2 mb-1" data-toggle="modal" data-target="#id_suggestion_{{$suggestion->id}}">
                                    update
                                </button> --}}
                                {!! Form::open(['method'=>'DELETE', 'action' => ['AdminSuggestionController@destroy', $kit->id, $suggestion->id], 'style'=> 'display:inline']) !!}
                                    {!! Form::submit('Delete', ['class'=>'btn btn-outline-danger mr-2 mb-1','onclick'=>"return confirm('Silmek isteğinizden Emin misiniz?')"]) !!}
                                {!! Form::close() !!} 
                                </td>
                                 <!-- Modal -->
                                <div class="modal fade" id="id_suggestion_{{$suggestion->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalCenterTitle">Show Suggestion</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-4 ">
                                                        <p> title </p>
                                                    </div>
                                                    <div class="col-8">
                                                        <p> description</p>
                                                    </div>
                                                    <div class="col-4">
                                                        <p>{{ $suggestion->title }}</p>
                                                    </div>
                                                    <div class="col-8">
                                                        <p>{!! $suggestion->description !!}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <p class="text-danger" > Not suggestion found </p>
            @endif
        </div>
    </div>
</div>
 <!-- Modal -->
 <div class="modal fade" id="create_suggestion" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Create Suggestion</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{-- {!! Form::open(['method'=>'DELETE', 'action' => ['AdminCouponController@destroy', $coupon->id], 'style'=> 'display:inline', 'id' => 'id_form_' . $coupon->code]) !!} --}}
                {!! Form::open(['method'=>'POST', 'action' => ['AdminSuggestionController@store', $kit->id],'style'=> 'display:inline', 'id' => 'id_form_create_suggestion']) !!}
                    <div class="form-group">

                        {!! Form::label('title ', 'Title :') !!}
                        {!! Form::text('title', null, ['class'=>'form-control', 'id' => 'create_title']) !!}
        
                    </div>
        
                    <div class="form-group">
        
                        {!! Form::label('description', 'Description:') !!}
                        {!! Form::textarea('description', null, ['class'=>'form-control', 'id' => 'create_description']) !!}
                        
                    </div>
                {!! Form::close() !!} 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" onclick="
                    event.preventDefault();
                    if ($('#create_title').val()  === '' ) {
                        alert('bazi alanlar doldurulmali');
                        return 0;
                    }
                    document.getElementById('id_form_create_suggestion').submit();
                ">Save Changes
                </button>
            </div>
        </div>
    </div>
</div>
@section('script')
    <script src="https://cdn.ckeditor.com/4.13.0/basic/ckeditor.js"></script>
    <script type="text/javascript">

        $(document).ready(function() {
            $('textarea').each(function(e){
                CKEDITOR.replace( this.id, {language: '{{ app()->getlocale() }}'});
            });
        });

    </script>
@endsection