@if (isMobile())
<section class="en-carousel-balance" style="margin-bottom:80px;background-color: transparent">
	<div class="container">
		 <div class="row mb-5">
			 <div class="col-md-12">
				 <h2 class="sleep-quality__title">
					<b>{{__('pages/welcome.mictobiome_balance')}}</b>
				</h2>
			 </div>
		 </div>
		 <div class="row owl-carousel end-carousel-balance owl-theme">
			 <div class="item">
				 <img src="{{url('/')}}/new/img/kilo-kontrolu.webp"/>
				 <div class="content">
					 <h3>{{__('pages/welcome.kilo_kontrolu_title')}}</h3>
					 <p>{{__('pages/welcome.kilo_kontrolu_desc')}}</p>
				 </div>
			 </div>
			 <div class="item">
				<img src="{{url('/')}}/new/img/uyku-kalitesi.webp"/>
				<div class="content">
					<h3>{{__('pages/welcome.uyku_kalitesi_title')}}</h3>
					<p>{{__('pages/welcome.uyku_kalitesi_desc')}}</p>
				</div>
			</div>
			<div class="item">
				<img src="{{url('/')}}/new/img/mental.webp"/>
				<div class="content">
					<h3>{{__('pages/welcome.mental_saglik_title')}}</h3>
					<p>{{__('pages/welcome.mental_saglik_desc')}}</p>
				</div>
			</div>
			<div class="item">
				<img src="{{url('/')}}/new/img/sindirim-sistemi.webp"/>
				<div class="content">
					<h3>{{__('pages/welcome.sindirim_sistemi_title')}}</h3>
					<p>{{__('pages/welcome.sindirim_sistemi_desc')}}</p>
				</div>
			</div>
			<div class="item">
				<img src="{{url('/')}}/new/img/guclu-bagisiklik.webp"/>
				<div class="content">
					<h3>{{__('pages/welcome.guclu_bagisiklik_sistemi_title')}}</h3>
					<p>{{__('pages/welcome.guclu_bagisiklik_sistemi_desc')}}</p>
				</div>
			</div>
			 <div class="item">
				 <img src="{{url('/')}}/new/img/cilt-sagligi.webp"/>

				 <div class="content">
					 <h3>{{__('pages/welcome.cilt_sagligi_title')}}</h3>
					 <p>{{__('pages/welcome.cilt_sagligi_desc')}}</p>
				 </div>
			 </div>
			 <div class="item">
				 <img src="{{url('/')}}/new/img/daha-fazlasi.webp"/>

				 <div class="content">
					 <h3>{{__('pages/welcome.daha_fazlasi_title')}}</h3>
					 <p>{{__('pages/welcome.daha_fazlasi_desc')}}</p>
				 </div>
			 </div>
		 </div>
	</div>
 </section>
@endif
@if (!isMobile())
<section id="denge" class="sleep-quality sleep-quality--home guclu-bagisiklik-sistemi">
	<div class="container">
		<div class="row">
			<h2 class="sleep-quality__title new_color">
				<b>{{__('pages/welcome.mictobiome_balance')}}</b>
			</h2>

			<div class="col-md-12 col-lg-8 col-xl-8 sleep-quality__col">
				<div class="sleep-quality__content" id="kilo-kontrolu">
					<img src="{{url('/')}}/new/img/kilo-kontrolu.webp" style="width: 100%;height: auto;">
					<h3 class="sleep-quality__content__title">{{__('pages/welcome.kilo_kontrolu_title')}}</h3>
					<p class="sleep-quality__content__desc sleep-quality__content__desc--home">
                        {{__('pages/welcome.kilo_kontrolu_desc')}}
					</p>
				</div>
				<div class="sleep-quality__content" id="uyku-kalitesi">
					<img src="{{url('/')}}/new/img/uyku-kalitesi.webp" style="width: 100%;height: auto;">
					<h3 class="sleep-quality__content__title">{{__('pages/welcome.uyku_kalitesi_title')}}</h3>
					<p class="sleep-quality__content__desc sleep-quality__content__desc--home">
						{{__('pages/welcome.uyku_kalitesi_desc')}}
					</p>
				</div>
				<div class="sleep-quality__content" id="mental-saglik">
					<img src="{{url('/')}}/new/img/mental.webp" style="width: 100%;height: auto;">
					<h3 class="sleep-quality__content__title">{{__('pages/welcome.mental_saglik_title')}}</h3>
					<p class="sleep-quality__content__desc sleep-quality__content__desc--home">
						{{__('pages/welcome.mental_saglik_desc')}}
					</p>
				</div>
				<div class="sleep-quality__content" id="huzurlu-sindirim-sistemi">
					<img src="{{url('/')}}/new/img/sindirim-sistemi.webp" style="width: 100%;height: auto;">
					<h3 class="sleep-quality__content__title">{{__('pages/welcome.sindirim_sistemi_title')}}</h3>
					<p class="sleep-quality__content__desc sleep-quality__content__desc--home">
						{{__('pages/welcome.sindirim_sistemi_desc')}}
					</p>
				</div>
				<div class="sleep-quality__content active" id="guclu-bagisiklik-sistemi">
					<img src="{{url('/')}}/new/img/guclu-bagisiklik.webp" style="width: 100%;height: auto;">
					<h3 class="sleep-quality__content__title">{{__('pages/welcome.guclu_bagisiklik_sistemi_title')}}</h3>
					<p class="sleep-quality__content__desc sleep-quality__content__desc--home">
						{{__('pages/welcome.guclu_bagisiklik_sistemi_desc')}}
					</p>
				</div>
				<div class="sleep-quality__content" id="cilt-sagligi">
					<img src="{{url('/')}}/new/img/cilt-sagligi.webp" style="width: 100%;height: auto;">
					<h3 class="sleep-quality__content__title">{{__('pages/welcome.cilt_sagligi_title')}}</h3>
					<p class="sleep-quality__content__desc sleep-quality__content__desc--home">
						{{__('pages/welcome.cilt_sagligi_desc')}}
					</p>
				</div>
				<div class="sleep-quality__content" id="daha-fazlasi">
					<img src="{{url('/')}}/new/img/daha-fazlasi.webp" style="width: 100%;height: auto;">
					<h3 class="sleep-quality__content__title">{{__('pages/welcome.daha_fazlasi_title')}}</h3>
					<p class="sleep-quality__content__desc sleep-quality__content__desc--home">
						{{__('pages/welcome.daha_fazlasi_desc')}}
					</p>
				</div>
			</div>
			<div class="col-md-12 col-lg-10 col-xl-10 sleep-quality__col">
				<div class="sleep-quality__carousel">
					<div class="sleep-quality__indicator"></div>
				</div>
				<ul class="sleep-quality__carousel-list" id=list>
					<li onclick=
					"replace('guclu-bagisiklik-sistemi', this);
					pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'line', Label: '{{slug(__('pages/mikrobiyom.guclu_bagisiklik_sistemi_title', [], 'tr'))}}' })" 
					class="sleep-quality__carousel-list__item active" data-target="#guclu-bagisiklik-sistemi">
						<div class="triangle"></div>
						<h3>{{__('pages/welcome.guclu_bagisiklik_sistemi_title')}}</h3>
					</li>
					<li onclick=
					"replace('kilo-kontrolu', this);
					pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'line', Label: '{{slug(__('pages/mikrobiyom.kilo_kontrolu_title', [], 'tr'))}}' })"
					class="sleep-quality__carousel-list__item" data-target="#kilo-kontrolu">
						<div class="triangle"></div>
						<h3>{{__('pages/welcome.kilo_kontrolu_title')}}</h3>
					</li>
					<li onclick=
					"replace('mental-saglik', this);
					pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'line', Label: '{{slug(__('pages/mikrobiyom.mental_saglik_title', [], 'tr'))}}' })"
					class="sleep-quality__carousel-list__item" data-target="#mental-saglik">
                        <div class="triangle"></div>
						<h3>{{__('pages/welcome.mental_saglik_title')}}</h3>
					</li>
					<li onclick=
					"replace('uyku-kalitesi', this);
					pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'line', Label: '{{slug(__('pages/mikrobiyom.uyku_kalitesi_title', [], 'tr'))}}' })"
					class="sleep-quality__carousel-list__item" data-target="#uyku-kalitesi">
						<div class="triangle"></div>
						<h3>{{__('pages/welcome.uyku_kalitesi_title')}}</h3>
					</li>
					<li onclick=
					"replace('huzurlu-sindirim-sistemi', this);
					pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'line', Label: '{{slug(__('pages/mikrobiyom.sindirim_sistemi_title', [], 'tr'))}}' })"
					class="sleep-quality__carousel-list__item" data-target="#huzurlu-sindirim-sistemi">
						<div class="triangle"></div>
						<h3>{{__('pages/welcome.sindirim_sistemi_title')}}</h3>
					</li>
					<li onclick=
					"replace('cilt-sagligi', this);
					pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'line', Label: '{{slug(__('pages/mikrobiyom.cilt_sagligi_title', [], 'tr'))}}' })"
					class="sleep-quality__carousel-list__item" data-target="#cilt-sagligi">
						<div class="triangle"></div>
						<h3>{{__('pages/welcome.cilt_sagligi_title')}}</h3>
					</li>
					<li onclick=
					"replace('daha-fazlasi', this);
					pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'line', Label: '{{slug(__('pages/mikrobiyom.daha_fazlasi_title', [], 'tr'))}}' })"
					class="sleep-quality__carousel-list__item" data-target="#daha-fazlasi">
						<div class="triangle"></div>
						<h3>{{__('pages/welcome.daha_fazlasi_title')}}</h3>
					</li>
				</ul>
			</div>
		</div>
    </div>
    <script>
        elArray = [
            'kilo-kontrolu',
            'uyku-kalitesi',
            'mental-saglik',
            'huzurlu-sindirim-sistemi',
            'guclu-bagisiklik-sistemi',
            'cilt-sagligi',
            'daha-fazlasi',
        ]
        function replace(id, el) {
            var section = document.getElementById('denge');
            elArray.forEach(function(item) {
                var element = document.getElementById(item);
                element.classList.remove("active");
                section.classList.remove(item);
            })
            var ul = document.getElementById("list");
            var items = ul.getElementsByTagName("li");
            for (var i = 0; i < items.length; ++i) {
                items[i].classList.remove("active");
            }
            var element = document.getElementById(id);
            element.classList.add("active");
			section.classList.add(id)
            el.classList.add("active");
		}
    </script>
</section>
@endif