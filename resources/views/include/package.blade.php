<section class="microbiome-texts kilo__6 pricing" style="direction: rtl">
	<div class="container">
        <div class="row justify-content-center">
			{{-- <div class="row"> --}}
            <div class="text-center col-12 mb-4">
                <h2 class="triple-box__title" style="color:#e8957e"> تحليل البكتيريا المعوية من  En-diet</h2>
                <p style="color:#e8957e">
                    <b>هل أنت مستعد لبدء رحلتك الصحية باستكشاف عالم البكتيريا في أمعائك؟</b>
                </p>
            </div>
            <div class="col-lg-5 mt-lg-0 mt-3">
                <div class="card mb-5 mb-lg-0 card-not" style="height: 97%;">
                    <div class="card-body">
                        <h6 class="card-price text-center">حزمة الصحة لتحليل بكتيريا الأمعاء</h6>
                        <h6 class="card-price text-center" style="direction: ltr">
                            {{-- <strike class="mr-2">1499 TL</strike> --}}
                            399 $
                        </h6>
						<hr>
						<p class="text-center">
							حان الوقت الآن لاستكشاف عالم البكتيريا في أمعائك والتعرف على أكثر الأطعمة الصحية لنفسك.
						</p>
                        <ul class="fa-ul text-right">
                            <li>تعرف على البكتيريا الموجودة في أمعائك بفضل تقرير تحليل الأمعاء.</li>
                            <li>تعرف على أنسب الأطعمة لك من خلال دليل التغذية المخصص.</li>
                        </ul>
                        <div class="text-center mt-5 mb-3">
                            <a class="link-btn link-btn--orange" href="{{route('checkout.product', 'mikrobiyom-saglik-paketi')}}">قم بالحصول عليها الآن</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 mt-lg-0 mt-3">
                <div class="card mb-5 mb-lg-0 card-not" style="height: 97%;">
                    <div class="card-body">
						<h6 class="card-price text-center">حزمة الصحة بلس لتحليل بكتيريا الأمعاء</h6>
                        <h6 class="card-price text-center" style="direction: ltr">
                            {{-- <strike class="mr-2">1999 TL</strike> --}}
                            499 $
                        </h6>
						<hr>
						<p class="text-center">
							في رحلتك لاكتشاف جسدك أخصائيو التغذية لدينا سيكون بجانبك.
						</p>
						<ul class="fa-ul text-right">
							<li>اكتشف جسدك مع حزمة صحة لتحليل بكتيريا الأمعاء</li>
							<li>تحكم في نظامك الغذائي لمدة 6 أسابيع مع دعم أخصائي تغذية En-diet.</li>
                            {{-- <li><span class="fa-li"><i class="fas fa-check"></i></span>ENBIOSIS diyetisyenleri tarafından kişiselleştirilmiş beslenme rehberine uygun bir şekilde planlanmış 6 haftalık diyet desteği</li> --}}
                        </ul>
                        <div class="text-center mt-5 mt-checkout-paketi mb-3">
                            <a class="link-btn link-btn--orange" href="{{route('checkout.product', 'mikrobiyom-saglik-paketi-premium')}}">قم بالحصول عليها الآن</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>