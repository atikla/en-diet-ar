<div style="display: flex;justify-content: center;width:100%">
    <div class="res-card" style="height:500px;">
        <img class="vid-img video-modal" data-video="https://www.youtube.com/embed/fjrwudz_73s" data-toggle="modal" data-target="#{{ $id }}" src="https://img.youtube.com/vi/ELHmlucbPcM/maxresdefault.jpg" alt="">
        <div class="video-modal play-button" data-video="https://www.youtube.com/embed/fjrwudz_73s" data-toggle="modal" data-target="#{{ $id }}"
            onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'video'})">
        </div>
        <div class="video-desc">Enbiosis İle Mikrobiyomunu Keşfet!</div>
    </div>
    </div>
    <div class="modal fade" id="{{ $id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="embed-container">
                    <iframe width="100%" height="350" src="" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
    