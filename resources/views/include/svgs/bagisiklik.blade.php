<svg style="fill: #fff;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 13.42 15.33" width='40' height='40' class='item-icon'>
    <g id="Layer_2" data-name="Layer 2">
    <g id="_1" data-name="1">
    <path class="cls-1" d="M7.67,10.54H5.75a1,1,0,0,1-1-1v-1h-1a1,1,0,0,1-1-1V5.75a1,1,0,0,1,1-1h1v-1a1,1,0,0,1,1-1H7.67a1,1,0,0,1,1,1v1h1a1,1,0,0,1,.95,1V7.66a1,1,0,0,1-.95,1h-1v1A1,1,0,0,1,7.67,10.54ZM3.83,5.75V7.66H5.27a.48.48,0,0,1,.48.48V9.58H7.67V8.14a.48.48,0,0,1,.48-.48H9.59V5.75H8.15a.48.48,0,0,1-.48-.48V3.83H5.75V5.27a.48.48,0,0,1-.48.48Z"/>
    <path class="cls-1" d="M6.71,15.33a2.57,2.57,0,0,1-.86-.16l-.67-.26A8.12,8.12,0,0,1,0,7.37V2.51A1.43,1.43,0,0,1,.83,1.2,14.23,14.23,0,0,1,6.71,0a14.15,14.15,0,0,1,5.87,1.2,1.43,1.43,0,0,1,.84,1.31V7.37a8.12,8.12,0,0,1-5.18,7.54l-.67.26A2.57,2.57,0,0,1,6.71,15.33ZM6.59,1A13.18,13.18,0,0,0,1.23,2.08.46.46,0,0,0,1,2.5H1V7.37A7.18,7.18,0,0,0,5.53,14l.66.25a1.44,1.44,0,0,0,1,0L7.89,14a7.16,7.16,0,0,0,4.57-6.65V2.51a.49.49,0,0,0-.27-.44A13.37,13.37,0,0,0,6.71,1Z"/>
    </g>
    </g>
</svg>