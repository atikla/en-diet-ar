{{-- @if (isMObile()) --}}
@section('styles')
	<style>
		.content{
			margin-left:30px;
			position: initial !important;
		}
		.content p {
			color: #000 !important;
			font-size: 16px !important;
		}
		.grayscale {
			filter: grayscale(100%);
		}
		.grayscale:hover {
			filter: grayscale(20%);
		}
	</style>
@endsection
{{-- <section class="en-carousel"style="margin-top:0;background:url({{ url('/') }}/new/img/basindabiz.webp);background-repeat: no-repeat;background-size: cover;">
	<div class="container">
		 <div class="row">
			 <div class="col-md-12">
				 <h2 class="maffects__title">
					 <b>
						{{__('pages/welcome.basinda_biz')}}
					 </b>
				 </h2>
			 </div>
		 </div>
		 <div class="row owl-carousel basin-carousel owl-theme">
			<div class="item p-4 text-center">
				<img class="grayscale" src="{{ url('/') }}/new/img/basin/crousal-capital.webp"/>
				<a class="link-btn link-btn--orange" style="border-radius: 46px;font-size:12px;padding: 15px 40px;" target="_blank" href="https://www.capital.com.tr/sirket-panosu/sirket-panosu-haberleri/yapay-zekayla-kisiye-ozel-diyet-listesi-hazirladi"
					onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'basinda_biz', Label: '{{slug('capital')}}' })">
					{{__('pages/welcome.detayli_bilgi')}}
				</a>
			</div>
			<div class="item p-4 text-center">
				<img class="grayscale" src="{{ url('/') }}/new/img/basin/crousal-bloomberg.webp"/>
				<a class="link-btn link-btn--orange" style="border-radius: 46px;font-size:12px;padding: 15px 40px;" target="_blank" href="https://www.youtube.com/watch?v=XZ0eFddE9dE"
					onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'basinda_biz', Label: '{{slug('bloomberg')}}' })">
					{{__('pages/welcome.detayli_bilgi')}}
				</a>
			</div>
			<div class="item p-4 text-center">
				<img class="grayscale" src="{{ url('/') }}/new/img/basin/crousal-webrazzi.webp"/>
				<a class="link-btn link-btn--orange" style="border-radius: 46px;font-size:12px;padding: 15px 40px;" target="_blank" href="https://webrazzi.com/2020/07/01/mikrobiyom-yapay-zeka-kisisellestirilmis-saglik-teknolojisi-enbiosis/"
					onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'basinda_biz', Label: '{{slug('webrazzi')}}' })">
					{{__('pages/welcome.detayli_bilgi')}}
				</a>
			</div>
			<div class="item p-4 text-center">
				<img class="grayscale" src="{{ url('/') }}/new/img/basin/crousal-egitisim.webp"/>
				<a class="link-btn link-btn--orange" style="border-radius: 46px;font-size:12px;padding: 15px 40px;" target="_blank" href="https://egirisim.com/2020/07/01/enbiosis-mikrobiyal-organizmalar-ile-insan-sagligini-artirmaya-calisan-yerli-yapay-zeka-girisimi/"
					onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'basinda_biz', Label: '{{slug('egitisim')}}' })">
					{{__('pages/welcome.detayli_bilgi')}}
				</a>
			</div>
			<div class="item p-4 text-center">
				<img class="grayscale" src="{{ url('/') }}/new/img/basin/crousal-dunya.webp"/>
				<a class="link-btn link-btn--orange" style="border-radius: 46px;font-size:12px;padding: 15px 40px;" target="_blank" href="https://www.dunya.com/sirketler/enbiosis-covid-19a-yapay-zeka-ile-cozum-bulmayi-hedefliyor-haberi-473071"
					onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'basinda_biz', Label: '{{slug('dunya')}}' })">
					{{__('pages/welcome.detayli_bilgi')}}
				</a>
			</div>
			<div class="item p-4 text-center">
				<img class="grayscale" src="{{ url('/') }}/new/img/basin/crousal-sabah.webp"/>
				<a class="link-btn link-btn--orange" style="border-radius: 46px;font-size:12px;padding: 15px 40px;" target="_blank" href="https://www.sabah.com.tr/saglik/2020/08/13/kisiye-ozel-tedavi-yontemleri-artik-hastanelerde"
					onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'basinda_biz', Label: '{{slug('sabah')}}' })">
					{{__('pages/welcome.detayli_bilgi')}}
				</a>
			</div>
		 </div>
	</div>
 </section> --}}
 <section class="en-carousel"style="margin-top:0;background:url({{ url('/') }}/new/img/basindabiz.webp);background-repeat: no-repeat;background-size: cover;">
	<div class="container">
		 <div class="row">
			 <div class="col-md-12">
				 <h2 class="maffects__title">
					 <b>
						{{__('include/basin.title')}}
					 </b>
				 </h2>
			 </div>
		 </div>
		 <div class="row owl-carousel basin-carousel owl-theme">
			 
			<div class="item">
				<img 
				onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'basinda_biz', Label: '{{slug('capital')}}' })
				window.open('https://www.capital.com.tr/sirket-panosu/sirket-panosu-haberleri/yapay-zekayla-kisiye-ozel-diyet-listesi-hazirladi', '_blank');"
				class="grayscale" src="{{ url('/') }}/new/img/basin/crousal-capital.webp"/>
				<div class="content">
					<p>
						{{__('include/basin.capital')}}
						<a
						target="_blank"
						href="https://www.capital.com.tr/sirket-panosu/sirket-panosu-haberleri/yapay-zekayla-kisiye-ozel-diyet-listesi-hazirladi"
						onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'basinda_biz', Label: '{{slug('capital')}}' })">
						{{__('include/basin.read_more')}}</a>
					</p>
				</div>
			</div>
			<div class="item">
				<img
				onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'basinda_biz', Label: '{{slug('bloomberg')}}' });
						window.open('https://www.youtube.com/watch?v=XZ0eFddE9dE')"
				src="{{ url('/') }}/new/img/basin/crousal-bloomberg.webp"/>
				<div class="content">
					<p>
						{{__('include/basin.bloomberg')}}
						<a
						target="_blank"
						href="https://www.youtube.com/watch?v=XZ0eFddE9dE"
						onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'basinda_biz', Label: '{{slug('bloomberg')}}' })">
						{{__('include/basin.read_more')}}</a>
					</p>
					
				</div>
			</div>
			<div class="item">
				<img 
				onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'basinda_biz', Label: '{{slug('webrazzi')}}' });
						window.open('https://webrazzi.com/2020/07/01/mikrobiyom-yapay-zeka-kisisellestirilmis-saglik-teknolojisi-enbiosis/', '_blank');"
				src="{{ url('/') }}/new/img/basin/crousal-webrazzi.webp"/>
				<div class="content">
					<p>
						{{__('include/basin.webrazzi')}}
						<a
						target="_blank"
						href="https://webrazzi.com/2020/07/01/mikrobiyom-yapay-zeka-kisisellestirilmis-saglik-teknolojisi-enbiosis/"
						onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'basinda_biz', Label: '{{slug('webrazzi')}}' })">
						{{__('include/basin.read_more')}}</a>
					</p>
					
				</div>
			</div>
			<div class="item">
				<img 
				onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'basinda_biz', Label: '{{slug('egirisim')}}' });
						window.open('https://egirisim.com/2020/07/01/enbiosis-mikrobiyal-organizmalar-ile-insan-sagligini-artirmaya-calisan-yerli-yapay-zeka-girisimi/', '_blank');"
				class="grayscale" src="{{ url('/') }}/new/img/basin/crousal-egitisim.webp"/>
				<div class="content">
					<p>
						{{__('include/basin.egirişim')}}
						<a 
						target="_blank" 
						href="https://egirisim.com/2020/07/01/enbiosis-mikrobiyal-organizmalar-ile-insan-sagligini-artirmaya-calisan-yerli-yapay-zeka-girisimi/"
						onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'basinda_biz', Label: '{{slug('egirisim')}}' })">
						{{__('include/basin.read_more')}}</a>
					</p>
				</div>
			</div>
			<div class="item">
				<img 
				onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'basinda_biz', Label: '{{slug('dunya')}}' });
						window.open('https://www.dunya.com/sirketler/enbiosis-covid-19a-yapay-zeka-ile-cozum-bulmayi-hedefliyor-haberi-473071');"
				class="grayscale" src="{{ url('/') }}/new/img/basin/crousal-dunya.webp"/>
				<div class="content">
					<p>
						{{__('include/basin.dunya')}}
						<a
						target="_blank"
						href="https://www.dunya.com/sirketler/enbiosis-covid-19a-yapay-zeka-ile-cozum-bulmayi-hedefliyor-haberi-473071"
						onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'basinda_biz', Label: '{{slug('dunya')}}' })">
						{{__('include/basin.read_more')}}</a>
					</p>
				</div>
			</div>
			<div class="item">
				<img 
				onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'basinda_biz', Label: '{{slug('sabah')}}' });
						window.open('https://www.sabah.com.tr/saglik/2020/08/13/kisiye-ozel-tedavi-yontemleri-artik-hastanelerde', '_blank');" 
				class="grayscale" src="{{ url('/') }}/new/img/basin/crousal-sabah.webp"/>
				<div class="content">
					<p>
						{{__('include/basin.sabah')}}
					   <a
						target="_blank"
						href="https://www.sabah.com.tr/saglik/2020/08/13/kisiye-ozel-tedavi-yontemleri-artik-hastanelerde"
						onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'basinda_biz', Label: '{{slug('sabah')}}' })">
						{{__('include/basin.read_more')}}</a>
					</p>
				</div>
			</div>
			@if (app()->getLocale() == 'tr')
				<div class="item">
					<img 
					onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'basinda_biz', Label: '{{slug('sabah')}}' });
							window.open('https://www.haberturk.com/kayseri-haberleri/80643661-turkiyenin-ilk-mikrobiyom-teknoloji-sirketinden-kisisellestirilmis-beslenme-rehberidoc-dr', '_blank');" 
					class="grayscale" src="{{ url('/') }}/new/img/basin/crousal-haberturk.webp"/>
					<div class="content">
						<p>
							{{__('include/basin.haber_turk')}}
						<a
							target="_blank"
							href="https://www.haberturk.com/kayseri-haberleri/80643661-turkiyenin-ilk-mikrobiyom-teknoloji-sirketinden-kisisellestirilmis-beslenme-rehberidoc-dr"
							onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'basinda_biz', Label: '{{slug('sabah')}}' })">
							{{__('include/basin.read_more')}}</a>
						</p>
					</div>
				</div>
			@endif
		 </div>
	</div>
 </section>
{{-- @else
<section class="maffects">
	<span class="maffects__title">{{__('pages/welcome.basinda_biz')}}</span>
	<div class="container" style="max-width: 100%;">
		<div class="row justify-content-center">
			<div class="col-6 col-sm-4 col-md-4 col-lg-3 col-xl-2">
				<div class="maffects__item">
					<img src="{{ url('/') }}/new/img/capitallogo.webp" />
					<a class="link-btn link-btn--orange" style="border-radius: 46px;font-size:12px;padding: 15px 40px;" target="_blank" href="https://www.capital.com.tr/sirket-panosu/sirket-panosu-haberleri/yapay-zekayla-kisiye-ozel-diyet-listesi-hazirladi"
					onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'basinda_biz', Label: '{{slug('capital')}}' })">
						{{__('pages/welcome.detayli_bilgi')}}
					</a>
				</div>
			</div>
			<div class="col-6 col-sm-4 col-md-4 col-lg-3 col-xl-2">
				<div class="maffects__item">
					<img style="margin-top: 35% !important" src="{{ url('/') }}/new/img/bloomberg.webp" />

					<a class="link-btn link-btn--orange" style="border-radius: 46px;font-size:12px;padding: 15px 40px;" target="_blank" href="https://www.youtube.com/watch?v=XZ0eFddE9dE"
					onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'basinda_biz', Label: '{{slug('bloomberg')}}' })">
						{{__('pages/welcome.detayli_bilgi')}}
					</a>
				</div>
			</div>
			<div class="col-6 col-sm-4 col-md-4 col-lg-3 col-xl-2">
				<div class="maffects__item">
					<img style="margin-top: 35% !important" class="mt-lg-5 mt-4" src="{{ url('/') }}/new/img/webrazzi.webp" />

					<a class="link-btn link-btn--orange" style="border-radius: 46px;font-size:12px;padding: 15px 40px;" target="_blank" href="https://webrazzi.com/2020/07/01/mikrobiyom-yapay-zeka-kisisellestirilmis-saglik-teknolojisi-enbiosis/"
					onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'basinda_biz', Label: '{{slug('webrazzi')}}' })">
						{{__('pages/welcome.detayli_bilgi')}}
					</a>
				</div>
			</div>
			<div class="col-6 col-sm-4 col-md-4 col-lg-3 col-xl-2">
				<div class="maffects__item">
					<img style="margin-top: 40% !important" class="mt-lg-5 mt-4" src="{{ url('/') }}/new/img/egirisim.webp" />

					<a class="link-btn link-btn--orange" style="border-radius: 46px;font-size:12px;padding: 15px 40px;" target="_blank" href="https://egirisim.com/2020/07/01/enbiosis-mikrobiyal-organizmalar-ile-insan-sagligini-artirmaya-calisan-yerli-yapay-zeka-girisimi/"
					onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'basinda_biz', Label: '{{slug('egirisim')}}' })">
						{{__('pages/welcome.detayli_bilgi')}}
					</a>
				</div>
			</div>
			<div class="col-6 col-sm-4 col-md-4 col-lg-3 col-xl-2">
				<div class="maffects__item">
					<img style="margin-top: 35%"src="{{ url('/') }}/new/img/dunya.webp" />

					<a class="link-btn link-btn--orange" style="border-radius: 46px;font-size:12px;padding: 15px 40px;" target="_blank" href="https://www.dunya.com/sirketler/enbiosis-covid-19a-yapay-zeka-ile-cozum-bulmayi-hedefliyor-haberi-473071"
					onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'basinda_biz', Label: '{{slug('dunya')}}' })">
						{{__('pages/welcome.detayli_bilgi')}}
					</a>
				</div>
			</div>
			<div class="col-6 col-sm-4 col-md-4 col-lg-3 col-xl-2">
				<div class="maffects__item">
					<img style="margin-top: 35%"src="{{url('/')}}/new/img/sabah.webp" />
					<a class="link-btn link-btn--orange" style="border-radius: 46px;font-size:12px;padding: 15px 40px;" target="_blank" href="https://www.sabah.com.tr/saglik/2020/08/13/kisiye-ozel-tedavi-yontemleri-artik-hastanelerde"
					onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'basinda_biz', Label: '{{slug('sabah')}}' })">
						{{__('pages/welcome.detayli_bilgi')}}
					</a>
				</div>
			</div
		</div>
	</div>
</section>
@endif --}}