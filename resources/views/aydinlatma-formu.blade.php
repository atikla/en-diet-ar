@extends('layouts.public')
@section('pageTitle','ENBİOSİS KİŞİSEL VERİLERİN ELDE EDİLMESİ VE İŞLENMESİ İLE İLGİLİ BİLGİLENDİRME VE AÇIK RIZA FORMU')
@section('metaTag')
<meta name="robots" content="noindex">
@endsection
@section('content')

<section class="subpage-header subpage-header--contact">
	<div class="container">
		<div class="row">
			<div class="subpage-header__content">
				<h1 class="subpage-header__title">نص التوضيح</h1>
				<div class="subpage-header__seperator"></div>

			</div>
		</div>
	</div>
</section>
<section class="bilim-content text-right" style="direction: rtl">

    <div class="container">
		<div class="text">
			<br>شركة Enbiosis Biotechnology Inc. ("Enbiosis") ، بياناتك الشخصية كمشرف على البيانات ضمن نطاق قانون حماية البيانات الشخصية رقم 6698 ("القانون") والتشريعات ذات الصلة ، ضمن الإطار الموضح أدناه ووفقًا للتشريعات الأخرى. تدرك Enbiosis أن بعض البيانات التي ستشاركها معنا هي بيانات شخصية ذات جودة عامة وبعضها بيانات صحية شخصية حساسة وأن أعلى مستوى من الحماية مطلوب. في هذا السياق؛ ما هي البيانات الشخصية التي نجمعها ، وكيف سنعالجها ، ولأي أغراض سنقوم بمعالجتها ، وبأي شروط سيتم نقلها إلى أطراف ثالثة باستخدام "المعلومات ونموذج الموافقة المفتوحة فيما يتعلق بالحصول على البيانات الشخصية لـ Enbiosis ومعالجتها" ("نموذج المعلومات والموافقة") ،
			<br>1. أغراض معالجة البيانات الشخصية
			<br>نقوم بمعالجة بياناتك الشخصية بشكل أساسي لإنشاء ملف تعريف الميكروبيوم الخاص بك باستخدام الأساليب العلمية وبالتالي لتحديد برنامج النظام الغذائي الشخصي الأنسب لملفك الشخصي من خلال اختصاصي التغذية. بعبارة أخرى ، نحتاج إلى معالجة بياناتك الشخصية ، والتي تم سرد تفاصيلها أدناه ، حتى نتمكن من استخراج برنامج النظام الغذائي المخصص الخاص بك باستخدام تطبيق Enbiosis. ما يجب فهمه من معالجة البيانات الشخصية هو معالجة بياناتك بوسائل آلية أو غير آلية ، مثل جمع هذه البيانات وتخزينها واستخدامها وتحليلها الإحصائي ونقلها. مرة أخرى ، إذا واصلت استخدام تطبيق Enbiosis لبياناتك الشخصية ، فيجب مراقبة برنامج النظام الغذائي الخاص بك بشكل دوري ومعالجته لجعل الميكروبات صحية. من ناحية أخرى؛ بالإضافة إلى برنامج النظام الغذائي المخصص لـ Enbiosis ، فإنه يجري أيضًا دراسات إعداد علمية حول تطوير طرق العلاج الشخصية في المستقبل. في هذا السياق؛ يمكن أيضًا استخدام بياناتك الشخصية في مثل هذه الدراسات العلمية بجعلها مجهولة. ومع ذلك؛ لا يوفر Enbiosis حاليًا خدمات علاج شخصية. من أجل إنشاء برنامج غذائي خاص لك ، يجب فحص عينات البراز التي يتم تسليمها إلينا في بيئة معملية. تتم معالجة عينات البراز المأخوذة منك بعد أن يتم إخفاء هويتك بواسطة Enbiosis من خلال إعطائك رقم معرف فريد. يتم نقل عيناتك إلى بيئة المختبر دون إعطاء معلومات تعريفية واضحة (من خلال إعطائك رقم هوية خاص). مرة أخرى نتائج المختبر ، تتم مشاركته مع اختصاصي التغذية برقم المعرف الخاص بك بحيث يمكن إنشاء برنامج التغذية الشخصية الخاص بك بشكل مجهول. تتعاون Enbiosis حاليًا مع مركز الجينوم والخلايا الجذعية بجامعة Erciyes وقد تتعاون مع مختبرات أخرى في البلاد أو في الخارج في المستقبل. بالإضافة إلى ما سبق؛ يمكن معالجة أي بيانات شخصية (بما في ذلك على سبيل المثال لا الحصر البيانات الشخصية ذات الطبيعة الخاصة) التي حصلت عليها Enbiosis للأغراض التالية:
			<br>•	تأكيد الإشارة المرجعية الخاصة بك ،
			<br>•	إجراء البحوث ،
			<br>•	استيفاء المتطلبات القانونية والتنظيمية ،
			<br>•	القيام بأنشطة إدارة المخاطر وتحسين الجودة من قبل أقسام الجودة ونظم المعلومات ،
			<br>•	التسويق والإعلام والاتصال ، وإعطاء معلومات الحملة ومعلومات الحملة عن طريق أقسام مركز الاتصال ، وتصميم وإيصال محتوى خاص ، وفوائد ملموسة وغير ملموسة على قنوات الويب والهاتف المحمول.
			<br>يمكن نقل بياناتك الشخصية التي تم الحصول عليها ومعالجتها وفقًا للتشريعات ذات الصلة إلى المحفوظات المادية و / أو أنظمة المعلومات الخاصة بـ Enbiosis والاحتفاظ بها في كل من البيئة الرقمية والمادية.
			<br>2. الحصول على البيانات الشخصية
			<br>يتم الحصول على بياناتك الصحية الشخصية ذات الجودة الخاصة عن طريق إرسال عينة من البراز تحتوي على الميكروبيوم المعوي مع المجموعة التي نرسلها إليك بالبريد السريع. في المجموعة التي نرسلها إليك ، يتم شرح كيفية أخذ عينة البراز الخاصة بك وتغليفها وإرسالها إلينا خطوة بخطوة. من ناحية أخرى ، بياناتك الصحية الحساسة ؛ يتم الحصول عليها أيضًا كتابيًا من خلال الاستبيان الذي تملأه على تطبيق Enbiosis للهاتف المحمول. وبالإضافة إلى هذه؛ يتم جمع بياناتك الشخصية بواسطتنا من خلال الوسائل الصوتية والمكتوبة والمرئية مثل تطبيق الهاتف المحمول وموقعنا الإلكتروني وأرقام هواتفنا.
			<br>3- البيانات الشخصية التي نجمعها
			<br>يمكن معالجة بياناتك الشخصية الخاصة ، وخاصة بياناتك الصحية ، المدرجة أدناه ، وبياناتك الشخصية العامة بواسطة Enbiosis بطريقة محدودة ومقاسة فيما يتعلق بالأغراض المذكورة أعلاه ، بما في ذلك على سبيل المثال لا الحصر:
			<br>•	جميع أنواع المخرجات الرقمية / المادية التي يتم الحصول عليها من البراز نتيجة للتحليل المختبري ،
			<br>•	جميع المعلومات التي تملأها في الاستبيان المرفق الذي أرسلناه إليك
			<br>o	معلومات عن نمط الحياة (العمر ، المهنة ، الوزن ، الطول ، إلخ) ،
			<br>o	التاريخ الصحي (شكاوى المرض السابقة ، السكري ، الكبد ، الأمعاء ، إلخ)
			<br>o	معلومات الحساسية (الطعام وأنواع الحساسية الأخرى)
			<br>o	معلومات وتاريخ استخدام الأدوية (المضادات الحيوية ولقاح الإنفلونزا والفيتامينات وما إلى ذلك)
			<br>o	عاداتك في الأكل (نوع النظام الغذائي ، المشروبات ، اللحوم ، الخضار ، استهلاك الفاكهة ، إلخ)
			<br>•	معلومات هويتك: اسمك ولقبك.
			<br>•	معلومات الاتصال الخاصة بك: عنوانك ورقم هاتفك وعنوان بريدك الإلكتروني وبيانات الاتصال الأخرى ، وسجلات محادثتك الصوتية التي يحتفظ بها ممثلو العملاء وفقًا لمعايير مركز الاتصال ، وبياناتك الشخصية التي تم الحصول عليها عند الاتصال بنا عبر البريد الإلكتروني أو الخطاب أو أي وسيلة أخرى.
			<br>•	معلوماتك المحاسبية: بياناتك المالية مثل معلومات الفواتير الخاصة بك.
			<br>•	بياناتك الصحية والبيانات الشخصية الأخرى التي ترسلها أو تدخلها من خلال موقع www.enbiosis.com.tr أو تطبيق Enbiosis للهاتف المحمول.
			<br>4. نقل البيانات الشخصية
			<br>بياناتك الشخصية ، مع القانون واللوائح ضمن النطاق وأغراض Enbiosis المذكورة أعلاه ، وشركات التأمين الخاصة ، ووزارة الصحة والوحدات التابعة لها ، ومؤسسة الضمان الاجتماعي ، ونقابة الصيادلة في تركيا وأطراف ثالثة ، وممثلك الذي سمحت له ، والمحامين ، والمستشارين الضريبيين والماليين ومع شركائنا في العمل والأطراف الثالثة الأخرى الذين نتعاون معهم من أجل تطوير أو تنفيذ الخدمات الصحية ، بما في ذلك المؤسسات التنظيمية والإشرافية والسلطات الرسمية للأغراض المذكورة أعلاه. بياناتك الشخصية:
			<br>•	التخزين والأرشفة ودعم تكنولوجيا المعلومات (الخادم ، الاستضافة ، البرنامج ، السحابة) في تركيا وخارجها ، خاصة في دول الاتحاد الأوروبي وأمريكا وإنجلترا ودول منظمة التعاون الاقتصادي والتنمية والهند والصين وروسيا ، في حالة وجود الأهداف المحددة في المادة 5.2 والمادة 6.3 من KVKK. المعلوماتية) ، الأمن ، مركز الاتصال ، إلخ. شركات المجموعة ، شركاء الأعمال ، الشركات الموردة ، البنوك ، المؤسسات المالية ، القانون ، الضرائب ، إلخ. يمكن نقلها إلى الشركات الاستشارية التي يتم تلقي الدعم منها في مجالات مماثلة وإلى الأطراف الأخرى ذات الصلة حيث يكون النقل ضروريًا للأغراض المحددة ؛
			<br>•	شركات التسويق ، وشركات المجموعة ، والجهات الخارجية التي تقدم الدعم التسويقي ، شريطة الحصول على موافقتك الصريحة للأغراض المحددة أعلاه ، في نطاق المادتين 5.1 و 6.2 من KVKK وفي الخارج ، وخاصة دول الاتحاد الأوروبي وأمريكا وإنجلترا ودول منظمة التعاون الاقتصادي والتنمية والهند والصين وروسيا. يمكن نقلها إلى شركات الخدمة (إرسال رسائل بريد إلكتروني ، وإنشاء حملات ، وشركات إعلانية ، وشركات تقدم دعم CRM ، بناءً على موافقتك الصريحة.
			<br>5. الطريقة والسبب القانوني للحصول على البيانات الشخصية
			<br>يتم جمع بياناتك الشخصية في جميع أنواع الوسائط الشفوية والمكتوبة والمرئية والإلكترونية ، للأغراض المذكورة أعلاه وللإطار القانوني لجميع أنواع العمل المدرجة في مجال نشاط Enbiosis ، وضمن هذا النطاق ، يمكن لـ Enbiosis الوفاء بالتزاماتها التعاقدية والقانونية بشكل كامل وصحيح. ومعالجتها. سبب قانوني لجمع بياناتك من هؤلاء الأشخاص ؛
			<br>•	• القانون رقم 6698 بشأن حماية البيانات الشخصية.
			<br>•	القانون الأساسي للخدمات الصحية رقم 3359 ،
			<br>•	• المرسوم بقانون رقم 663 بشأن تنظيم وواجبات وزارة الصحة والمؤسسات التابعة.
			<br>•	• لائحة المستشفيات الخاصة.
			<br>•	• لائحة بيانات الصحة الشخصية ،
			<br>•	• لائحة وزارة الصحة وأحكام التشريعات الأخرى.
			<br>•	لائحة حماية البيانات العامة للاتحاد الأوروبي (GDPR)
			<br>بالإضافة إلى ذلك ، فإن البيانات الشخصية المتعلقة بالصحة والحياة الجنسية ، كما هو مذكور في الفقرة 3 من المادة 6 من القانون ، تبقى سرية فقط لغرض حماية الصحة العامة والطب الوقائي والتشخيص الطبي وخدمات العلاج والرعاية وتخطيط وإدارة الخدمات الصحية والتمويل. يمكن معالجتها من قبل الأشخاص الخاضعين للالتزام أو المؤسسات والمنظمات المصرح لها دون موافقة صريحة من الشخص المعني.
			<br>6- الحقوق والالتزامات المتعلقة بحماية البيانات الشخصية
			<br>وفقًا للقانون والتشريعات ذات الصلة ؛
			<br>•	معرفة ما إذا كانت البيانات الشخصية تتم معالجتها ،
			<br>•	إذا تمت معالجة البيانات الشخصية ، لطلب معلومات بخصوص هذا ،
			<br>•	الوصول إلى البيانات الصحية الشخصية وطلبها ،
			<br>•	تعلم الغرض من معالجة البيانات الشخصية وما إذا كان يتم استخدامها بشكل مناسب لغرضها ،
			<br>•	لمعرفة الأطراف الثالثة التي يتم نقل البيانات الشخصية إليها محليًا أو خارجيًا ،
			<br>•	لطلب تصحيح البيانات الشخصية في حالة المعالجة غير الكاملة أو غير الصحيحة ،
			<br>•	لطلب حذف أو إتلاف البيانات الشخصية ،
			<br>•	في حالة معالجة البيانات الشخصية بشكل غير كامل أو غير دقيق ، لمطالبة الأطراف الثالثة التي يتم نقل البيانات الشخصية إليها بإخطارها بتصحيح و / أو حذف أو إتلاف البيانات الشخصية ،
			<br>•	يحق لك الاعتراض على حدوث نتيجة ضد الشخص نفسه عن طريق تحليل البيانات المعالجة حصريًا من خلال الأنظمة الآلية.
			<br>في حالة ممارستك لواحد أو أكثر من حقوقك المذكورة أعلاه ، يتم إرسال المعلومات ذات الصلة إليك بشكل واضح ومفهوم كتابيًا أو إلكترونيًا ، من خلال معلومات الاتصال التي قدمتها.
			<br>نظرًا لأننا سنعالج بياناتك الشخصية في نطاق العلاقة التعاقدية المبرمة معك وأداء العقد ، فستتم مشاركتها مع الداخل والخارج إذا لزم الأمر ، وإذا طلبت في هذا الاتجاه ، معالجة البيانات الشخصية ، ونقلها ، وحفظها ، والغرض من الاستخدام ، والتصحيح ، والاعتراض ، والحذف ، والتدمير ، إخفاء هويتك ، وإبلاغك بأن لديك الحق في المطالبة بالتعويض في حالة حدوث ضرر ، ولك الحق في التقدم إلينا وفقًا للقانون.
			<br>7. أمن البيانات
			<br>تحمي Enbiosis بياناتك الشخصية بالامتثال الكامل لجميع ضوابط الأمان الفنية والإدارية التي يجب اتخاذها وفقًا لمعايير وإجراءات أمن المعلومات. يتم توفير التدابير الأمنية المعنية على مستوى مناسب للمخاطر المحتملة ، مع مراعاة الإمكانيات التكنولوجية. Enbiosis Biyoteknoloji A.Ş. ، المتحكم في البيانات ، لبياناتك الشخصية التي تتم مشاركتها مع شركتنا. من خلال استخدامه كقوانين وأنظمة.
			<br>8. الشكاوى والتواصل
			<br>تتم حماية بياناتك الشخصية بدقة في نطاق الإمكانيات الفنية والإدارية ويتم توفيرها بمستوى مناسب للمخاطر المحتملة ، مع مراعاة التدابير الأمنية اللازمة والإمكانيات التكنولوجية. يمكنك إرسال طلباتك ضمن نطاق القانون إلى عنوان البريد الإلكتروني kvkk@enbiosis.com أو Maslak Mahallesi TaşyoncıkSk. مسلك 1453 اجلس. شقة 1U T4A T4B. رقم: 1 / B91 ساريير / اسطنبول ، تركيا عن طريق البريد السريع إلى العنوان الموجود على الظرف مع عريضة تحمل توقيعك الأصلي "نطاق القانون بشأن حماية استعلام البيانات الشخصية" يمكنك إرسالها كتابيًا.
			<br>9- وقت معالجة بياناتك الشخصية
			<br>ستتم معالجة البيانات الشخصية التي قمت بمشاركتها مع Enbiosis من خلال القنوات المذكورة في نموذج المعلومات والموافقة هذا وفقًا للتشريعات الخاصة بحماية البيانات الشخصية ، وخاصة KVKK ، والفترات التي تتطلبها التشريعات الأخرى ، طالما لم يتم إلغاء الأغراض المشروعة المذكورة أعلاه.
			<br>10. هوية مراقب البيانات
			<br>Mersis No: 0334097043200001
			<br>العنوان: Maslak Mahallesi TaşyonbasıSk. مسلك 1453 اجلس. شقة 1U T4A T4B. رقم: 1 U / B91 Sarıyer / İstanbul
			<br>E-Mail: kvkk@enbiosis.com لقد قرأت وفهمت المعلومات المذكورة أعلاه واستمارة الموافقة. (التوقيع الذي ستضعه هنا هو فقط لغرض إثبات أننا أوفينا بالتزامنا بالإبلاغ ، وإذا لم تقم بالتوقيع أدناه ، فهذا لا يعني أنك تمنح موافقتك الصريحة على المعاملات التي تمت بموافقة صريحة تحت أي ظرف من الظروف)
			<br>الاسم واللقب:
			<br>التوقيع:
			<br>________________________________________
			<br>ضمن نطاق المادة 3 (ب) من نموذج المعلومات والموافقة أعلاه (= المعاملات التي تتطلب موافقة صريحة في نطاق المادة 5.1 والمادة 6.2 من قانون حماية المستهلك KVKK) والمادة 5 (ب) (= عمليات النقل التي تتطلب موافقتك الصريحة في نطاق المادة 5.1 والمادة 6.2 من KVKK) بواسطة Enbiosis Biotechnology Inc. أقبل بموجب هذا وأعلن وأتعهد بأننا نعطي موافقة صريحة على كل من العمليات (بما في ذلك عمليات النقل) المحددة والخاضعة لموافقة صريحة ، بما في ذلك البيانات الشخصية والبيانات الشخصية المؤهلة الخاصة التي تمت معالجتها حتى الآن وستتم معالجتها بعد ذلك.
			<br>الاسم واللقب
			<br>التوقيع:
			<br>________________________________________
			<br>تأكيد الرسالة الإلكترونية التجارية
			<br>تشمل Enbiosis Biotechnology Corporation (Mersis No: 0334097043200001) الرسائل النصية القصيرة / الرسائل النصية والإخطار الفوري والمكالمات التلقائية والمكالمات الهاتفية والكمبيوتر والهاتف والإنترنت ووسائل التواصل الاجتماعي وشبكات الإعلان عبر الإنترنت ، ومنارة البلوتوث ضمن نطاق القانون رقم 6563 بشأن تنظيم التجارة الإلكترونية. أوافق أيضًا على إرسال رسائل إلكترونية تجارية إليّ عبر الاتصالات والشبكات اللاسلكية العامة والخاصة والفاكس والبريد الإلكتروني / البريد الإلكتروني وجميع أنواع الإخطارات وقنوات الاتصال الإلكترونية الأخرى.
			<br>الاسم - اللقب:
			<br>عنوان البريد الإلكتروني:
			<br>التوقيع:
        </div>
	</div>
</section>

@endsection
