@extends('layouts.public')
@section('styles')
<style>
.border-top { border-top: 1px solid #e5e5e5; }
.border-bottom { border-bottom: 1px solid #e5e5e5; }
.border-top-gray { border-top-color: #adb5bd; }

.box-shadow { box-shadow: 0 .25rem .75rem rgba(0, 0, 0, .05); }

.lh-condensed { line-height: 1.25; }
/* body {
  background: #00b09b;
  background: -webkit-linear-gradient(to right, #00b09b, #96c93d);
  background: linear-gradient(to right, #00b09b, #96c93d);
  min-height: 100vh;
} */

.text-gray {
  color: #aaa;
}
</style>
@endsection
@section('content')
<section class="header" style="height: 30vh"></section>
<section class="hw">
    <div class="container">
        <div class="hw-title">
            <h3><a style="color:#ea502c"href="{{route('shop')}}"> {{__('app.shop')}} </a><i class="fas fa-arrow-right"></i> {{ $product->slug }}</h3>
            <h1>
                {{__('app.EnBiosis_Product')}}
            </h1>
        </div>
    </div>
</section>
<section class="mb-5">
    <div class="container">
        <!-- row-->
        <div class="row">
            <div class="col-lg-12 mx-auto">
            <!-- List group-->
            <ul class="list-group shadow">
                <!-- list group item-->
                    <li class="list-group-item">
                        <!-- Custom content-->
                        <div class="media align-items-lg-center flex-column flex-lg-row p-3">
                            <div class="media-body order-2 order-lg-1 mt-3">
                            <h5 class="mt-0 font-weight-bold mb-2">{{$product->name}}</h5>
                            <p class="font-italic text-muted mb-0 small">{!! $product->description !!}</p>
                            <div class="d-flex align-items-center justify-content-between mt-1">
                                <h6>Price :</h6>
                                <h6 class="font-weight-bold ml-1">
                                    @if ($product->discount_price)
                                        <strike class="text-danger">
                                            {{$product->price}} TL
                                        </strike>
                                        <span class="ml-2 text-success">{{(double) ( ( ( 100 - $product->discount_price ) * $product->price ) / 100 )}} TL</span>
                                    @else
                                        {{$product->price}} TL
                                    @endif
                                </h6>
                                <ul class="list-inline small">
                                <li class="list-inline-item m-0" style="font-size:1rem;"><a style="color:#ea502c" href="{{route('add.cart', $product->id)}}">{{ __('app.sepete_ekle') }}<i style="color:#ea502c" class="mx-2 fas fa-cart-plus"></i> </a></li>
                                </ul>
                            </div>
                            <!-- </div><img style="width:50%" src="{{$product->photo ? url('/') . '/img/private/' . $product->photo->path : url('/') . '/img/private/default.png'}}" alt="Generic placeholder image" width="200" class="ml-lg-5 order-1 order-lg-2"> -->
                            </div><img src="{{$product->photo ? url('/') . '/img/private/' . $product->photo->path : url('/') . '/img/private/default.png'}}" alt="Generic placeholder image" width="200" class="ml-lg-5 order-1 order-lg-2">
                        </div>
                        <!-- End -->
                    </li>
                <!-- End -->
              </ul>
              <!-- End -->
            </div>  
        </div>
    </div>
</section>
@endsection