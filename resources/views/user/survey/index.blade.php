@extends('layouts.app')

@section('content')

<div class="row justify-content-center px-4">
    <div class="col-12">
        {{-- <div class="card">
        <div class="card-header"> {{__('user.ana')}} </div>
            <div class="card-body text-center">
                <div class="btn-group" role="group" aria-label="Basic example">
                    <a type="button" class="btn btn-secondary" href="">  </a>
                    <a type="button" class="btn btn-secondary" href="">  </a>
                    <a type="button" class="btn btn-secondary" href="">  </a>
                </div>
            </div>
        </div> --}}
    </div>
    <div class="col-12 mt-5">
        <div class="card">
            <div class="card-header"> <h2> {{__('user.anket')}} </h2></div>

            <div class="card-body">
                <div class="container py-3 my-3 border rounded shadow-sm">
                    <div class="row">
                        <section class="col-12">
                            <ul class="nav nav-tabs flex-nowrap" role="tablist">
                                <li role="presentation" class="nav-item">
                                    <a href="#step1" class="nav-link active" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1"> 1 </a>
                                </li>
                                @foreach ($surveys->sections as $section)
                                    @if ($loop->last)
                                        <li role="presentation" class="nav-item">
                                            <a href="#step{{$loop->iteration + 1}}" class="nav-link disabled" data-toggle="tab" aria-controls="step{{$loop->iteration + 1}}" role="tab" title="Step {{$loop->iteration + 1}}"> {{$loop->iteration + 1}} </a>
                                        </li>
                                        <li role="presentation" class="nav-item">
                                            <a href="#complete" class="nav-link disabled" data-toggle="tab" aria-controls="complete" role="tab" title="Complete"> {{$loop->iteration + 2}} </a>
                                        </li>
                                    @else
                                        <li role="presentation" class="nav-item">
                                            <a href="#step{{$loop->iteration + 1}}" class="nav-link disabled" data-toggle="tab" aria-controls="step{{$loop->iteration + 1}}" role="tab" title="Step {{$loop->iteration + 1}}"> {{$loop->iteration + 1}} </a>
                                        </li>
                                    @endif
                                @endforeach

                            </ul>
                            {!! Form::open(['method'=>'POST', 'action' => ['UserController@storeSurvey', $kitCode, $kitId], 'style'=> 'display:inline', 'id' => 'id_form_create']) !!}
                                <div class="tab-content py-2">
                                    <div class="tab-pane active" role="tabpanel" id="step1">
                                        <h3>{{$surveys->title}}</h3>
                                        <div class="ml-3">{!! $surveys->description !!}</div>
                                        <p class="ml-3">
                                            {{__('user.anket_bolum', ['number' => $surveys->sections->count()])}}
                                        </p>
                                        <button type="button" class="btn btn-primary next-step-first float-right">{{__('user.sonraki')}}</button>
                                    </div>
                                    {{-- {{dd($surveys->sections->count())}} --}}
                                    @foreach ($surveys->sections as $section)
                                        <div class="tab-pane" role="tabpanel" id="step{{$loop->iteration + 1}}">
                                            <h3>{{ $section->title }}</h3>
                                            <div class="ml-3">{!! $section->description !!}</div>
                                            <div class="ml-3 my-3"><h4>{{__('user.sorular')}}</h4></div>
                                            @foreach ($section->questions as $question)
                                                <div class="ml-lg-5 ml-3 my-2">

                                                    @if ( $question->question_type == 'text' ||  $question->question_type == 'number' || $question->question_type == 'textarea')

                                                    <div class="form-group">

                                                        {!! Form::label('question_id_' . $question->id, $loop->iteration . ' - ' . $question->title) !!}
                                                        {!! Form::text('question_id_' . $question->id, null, ['class'=>'form-control', 'required', 'placeholder' => $question->title]) !!}

                                                    </div>

                                                    {{-- @elseif ( $question->question_type == 'textarea' )

                                                        <div class="form-group">

                                                            {!! Form::label('question_id_' . $question->id, $loop->iteration . ' - ' . $question->title) !!}
                                                            {!! Form::textarea('question_id_' . $question->id, null, ['class'=>'form-control', 'required', 'placeholder' => $question->title]) !!}

                                                        </div> --}}

                                                    @elseif ( $question->question_type == 'date' )

                                                        <div class="form-group">

                                                            {!! Form::label('question_id_' . $question->id, $loop->iteration . ' - ' . $question->title) !!}
                                                            {!! Form::date('question_id_' . $question->id, \Carbon\Carbon::now()->format('Y-m-d'), ['class'=>'form-control', 'required', 'placeholder' => $question->title]) !!}
                                                        </div>

                                                    @elseif ( $question->question_type == 'checkbox' )

                                                        <div class="form-group row">

                                                            <div class="col-sm-4">{{$loop->iteration . ' - ' . $question->title}}</div>
                                                            <div class="col-sm-8">
                                                                @foreach ($question->option_name as $key => $option)
                                                                @php
                                                                    $is_if = 0;
                                                                    if($option['is_if'])
                                                                        $is_if = 1;
                                                                @endphp
                                                                <div class="form-check">

                                                                    {!! Form::checkbox('question_id_' . $question->id . '[]', $key, false, ['isif' => $is_if, 'isifid' => $key, 'data' => $option['is_if'], 'dataid' =>  '' . $question->id . '-' . $key .'']) !!}
                                                                    {!! Form::label('question_id_' . $question->id . '[]', $option['title']) !!}

                                                                </div>
                                                                @endforeach
                                                            </div>

                                                        </div>
                                                    @elseif ( $question->question_type == 'radio' )
                                                        <fieldset class="form-group">
                                                            <div class="row">
                                                                <legend class="col-form-label col-sm-4 pt-0">{{ $loop->iteration . ' - ' . $question->title }}</legend>
                                                                <div class="col-sm-8">
                                                                    @foreach ($question->option_name as $key => $option)
                                                                    @php
                                                                        $is_if = 0;
                                                                        if($option['is_if'])
                                                                            $is_if = 1;
                                                                    @endphp
                                                                        <div class="form-check">
                                                                            {!! Form::radio('question_id_' . $question->id . '[]', $key, false, ['isif' => $is_if, 'isifid' => $key, 'data' => $option['is_if'], 'dataid' =>  '' . $question->id . '-' . $key .'']) !!}
                                                                            {!! Form::label('question_id_' . $question->id . '[]', $option['title']) !!}
                                                                        </div>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                        </fieldset>
                                                    @else
                                                    @endif
                                                </div>
                                            @endforeach

                                            <ul class="float-right">
                                                <li class="list-inline-item">
                                                    <button type="button" class="btn btn-outline-primary prev-step">{{__('user.onceki')}}</button>
                                                </li>
                                                <li class="list-inline-item">
                                                    <button type="button" class="btn btn-primary next-step" data="step{{$loop->iteration + 1}}">{{__('user.sonraki')}}</button>
                                                </li>
                                            </ul>
                                        </div>
                                    @endforeach

                                    <div class="tab-pane" role="tabpanel" id="complete">
                                        <h3>Bütün Aşamalar bitti!</h3>
                                        <p>Cevaplardan eminseniz Bitir'yi tıklayın</p>
                                        <div class="float-right">
                                            {!! Form::submit('Bitir', ['class'=>'btn btn-primary']) !!}
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <input id ="conditionals" type="hidden" name="conditionals">
                            {!! Form::close() !!}
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script src="https://cdn.ckeditor.com/4.13.0/basic/ckeditor.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            let forsubmit = false;
            //Initialize tooltips
            $('.nav-tabs > li a[title]').tooltip();

            //Wizard
            $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                var $target = $(e.target);
                if ($target.parent().hasClass('disabled')) {
                    return false;
                }
            });
            $(".next-step-first").click(function (e) {
                var $active = $('.nav-tabs li>.active');
                $active.parent().next().find('.nav-link').removeClass('disabled');
                nextTab($active);
            });

            $(".next-step").click(function (e) {

                let data = $(this).attr('data');
                let skip = true;
                $("#" + data + " input").each(function() {
                    if ( !$(this).val() ) {
                        // alert("this question " + $(this).attr('placeholder') + "  is required " );
                        skip = false;
                    }
                });
                if( skip ) {
                    var $active = $('.nav-tabs li>.active');
                    $active.parent().next().find('.nav-link').removeClass('disabled');
                    nextTab($active);
                }else {
                    alert('Some Question Is Required');
                }
            });

            $(".prev-step").click(function (e) {
                var $active = $('.nav-tabs li>a.active');
                prevTab($active);
            });

            function nextTab(elem) {
                $(elem).parent().next().find('a[data-toggle="tab"]').click();
            }
            function prevTab(elem) {
                $(elem).parent().prev().find('a[data-toggle="tab"]').click();
            }
            $('textarea').each(function(e){
                CKEDITOR.replace( this.id, {language: '{{ app()->getlocale() }}'});
            });
            $('input[type="checkbox"]').click(function(){
                if($(this).prop('checked') == true){
                   if ( $( this ).attr('isif') == 1 ) {
                       let  newInput    = '<div class="form-group koşullu koşullu_secim-' + $( this ).attr('dataid') + '">';
                            newInput   += '<label for="'+ $(this).prop("id") +'"> ' +  $( this ).attr('data') + ' </label>'
                            newInput   += '<input data="' + $(this).attr("dataid") + '"required="required" placeholder="' +  $( this ).attr('data') +'" type="text" class="form-control">'
                            newInput   += '</div>';
                        $( this ).parent().parent().append(newInput);
                        // console.log($( this ).parent().parent());
                    }
                }
                else if($(this).prop('checked') == false){
                    if ( $( this ).attr('isif') == 1 ) {
                        console.log('.koşullu_secim-' + $( this ).attr('dataid'));
                        $( this ).parent().parent().children('.koşullu_secim-' + $( this ).attr('dataid') ).remove();
                    }
                }
            });

            $('input[type="radio"]').click(function(){
                if($(this).prop('checked') == true){
                   if ( $( this ).attr('isif') == 1 ) {

                       if( $( this ).parent().parent().find('.koşullu_secim').length == 1  ){

                        $( this ).parent().parent().children('.koşullu_secim').remove();

                       }
                        let newInput = '<div class="form-group koşullu koşullu_secim">'
                            + '<label for="'+ $(this).prop("id") +'"> ' + $( this ).attr('data') + ' </label>'
                            + '<input data="' + $(this).attr("dataid") + '" required="required" placeholder="' +  $( this ).attr('data') +'" type="text" class="form-control">'
                            + '</div>';

                        $( this ).parent().parent().append(newInput);
                    }else {
                        if( $( this ).parent().parent().find('.koşullu_secim').length == 1  ){


                            $( this ).parent().parent().children('.koşullu_secim').remove();
                        }
                    }
                }
            });

            $( 'form' ).submit( function (e){

                // console.log('lalal');
                if (!forsubmit) {
                    let obj = {};
                    $( '.koşullu' ).each( function () {
                        let input = $( this ).children('input');
                        let temparray = input.attr('data').split("-");
                        if ( Object.keys( obj ).includes( temparray[0] ) ) {
                            obj[temparray[0]] = {

                                ...obj[temparray[0]],
                                [temparray[1]] : input.val()

                            }
                        } else {

                            obj[temparray[0]] = {
                                [temparray[1]] : input.val()
                            };
                        }
                        
                        forsubmit = true;
                    });
                    // console.log(obj);
                    // $( "#conditionals" ).val(JSON.stringify(obj));
                }
                $( 'form' ).submit()
            });
        });
    </script>
@endsection