@extends('layouts.public')

@section('pageTitle',__('pages/welcome.page_title'))
@section('pageDescription',__('pages/welcome.page_description'))


@section('content')
<section class="header" style="background-image: url({{url('/')}}/new/img/webgorsel-desktop.png);">
	<div class="container">
		<div class="row">
			<div class="col-7 col-md-11 col-lg-7 col-xl-7">
				<h1 class="header__slogan">{{__('pages/welcome.header_slogan.0')}}  {{__('pages/welcome.header_slogan.1')}} <br/><b>{{__('pages/welcome.header_slogan.2')}}</b></h1>
				<p class="header__desc" style="direction:rtl">
					{{__('pages/welcome.header_desc')}}
				</p>
				<a class="link-btn link-btn--orange" href="{{url('/urunlerimiz')}}"
				onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'button', Label: '{{slug(__('pages/welcome.header_slogan.0') . ' ' . __('pages/welcome.header_slogan.1') . ' ' . __('pages/welcome.header_slogan.2'))}}' })">
				{{__('pages/welcome.kesfet')}}</a>
			</div>
		</div>
	</div>
</section>
<section class="header header--mobile" style="background-image: url({{url('/')}}/new/img/webgorsel-mobil.png);
align-items: flex-start;background-size: cover;">
	<div class="container" style="margin-top:160px">
		<div class="row">
			<div class="col-12 col-md-12 col-lg-12 col-xl-12" style="text-align: center;direction: rtl">
				<h1 class="header__slogan">{{__('pages/welcome.header_slogan.0')}}  {{__('pages/welcome.header_slogan.1')}} <b>{{__('pages/welcome.header_slogan.2')}}</b></h1>
				<p class="header__desc">
					{{__('pages/welcome.header_desc')}}
				</p>
				<a class="link-btn link-btn--orange" href="{{url('/urunlerimiz')}}">
					{{__('pages/welcome.kesfet')}}
				</a>
			</div>
		</div>
	</div>
</section>
<section class="microbiome" style="background-color:transparent;text-align: justify; direction: rtl">
	<div class="container">
		<div class="row">
			<div class="col-md-12 ecosystem__item text-center">
				<h2 class="maffects__title">
					<b>
						{{__('pages/welcome.section_1.0.title')}}
					</b>
				</h2>
				<p class="ecosystem__desc">
					{{__('pages/welcome.section_1.0.desc')}}
				</p>
			</div>
			<div class="col-md-12 ecosystem__item text-center">
				<h2 class="maffects__title">
					<b>
						{{__('pages/welcome.section_1.1.title')}}
					</b>
				</h2>
				<p class="ecosystem__desc">
					{{__('pages/welcome.section_1.1.desc')}}
				</p>
			</div> 
		</div>
	</div>
</section>
<section class="hprocess hprocess--home" style="direction: rtl">
	<div class="container">
		<div class="row hprocess__howto mt-0">
			<h1 class="hprocess__howtotitle">{{__('pages/welcome.hprocess_title')}}</h1>
			<div class="col-md-6 col-lg-3 col-xl-3 hprocess__step">
				<div class="hprocess__number">
					<img src="{{ url('/') }}/new/img/icons/surec1.webp" width="50"/>
				</div>
				<h2 class="hprocess__steptitle">{{__('pages/welcome.hprocess_step_title_1')}}</h2>
				<p class="hprocess__stepdesc">
					{!!__('pages/welcome.hprocess_step_desc_1')!!}
				</p>
			</div>
			<div class="col-md-6 col-lg-3 col-xl-3 hprocess__step">
				<div class="hprocess__number">
					<img src="{{ url('/') }}/new/img/icons/surec2.webp" width="50"/>
				</div>
				<h2 class="hprocess__steptitle">{{__('pages/welcome.hprocess_step_title_2')}}</h2>
				<p class="hprocess__stepdesc">
					{{__('pages/welcome.hprocess_step_desc_2')}}
				</p>
			</div>
			<div class="col-md-6 col-lg-3 col-xl-3 hprocess__step">
				<div class="hprocess__number">
					<img src="{{ url('/') }}/new/img/icons/surec3.webp" width="50"/>
				</div>
				<h2 class="hprocess__steptitle">{{__('pages/welcome.hprocess_step_title_3')}}</h2>
				<p class="hprocess__stepdesc">
					{{__('pages/welcome.hprocess_step_desc_3')}}
				</p>
			</div>
			<div class="col-md-6 col-lg-3 col-xl-3 hprocess__step">
				<div class="hprocess__number">
					<img src="{{ url('/') }}/new/img/icons/surec4.webp" width="50"/>
				</div>
				<h2 class="hprocess__steptitle">{{__('pages/welcome.hprocess_step_title_4')}}</h2>
				<p class="hprocess__stepdesc">
					{{__('pages/welcome.hprocess_step_desc_4')}}
				</p>
			</div>
		</div>
	</div>
</section>
@include('include.banner')
@include('include.package')
<section class="mictobiome-balance">
	<section class="microbiome-analysis microbiome-analysis--2">
		<div class="container">
			<div class="row">
				<div class="col-md-12 mictobiome-balance-mobile">
					<img src="{{url('/')}}/new/img/microbiome-analysis-bg3-mobile.webp" style="width:100%;height:auto;margin-bottom:70px;"/>
				</div>
				<div class="col-md-12 col-lg-8 col-xl-6 text-right" style="direction: rtl">
					<h2 class="microbiome-analysis__title"><b>{{__('pages/welcome.mictobiome_balance_title')}}</b></h2>
					<p class="microbiome-analysis__desc">
						{{__('pages/welcome.mictobiome_balance_desc')}}

					</p>
				</div>
			</div>
		</div>
	</section>
</section>
<section class="analyze-report text-right">
	<div class="container">
		<div class="row analyze-report__besin-skorlari">
			<div class="col-md-12 col-lg-6 col-xl-6 d-flex align-items-center">
				<div class="analyze-report__item">
					<h3>{{__('pages/welcome.foods_title')}}</h3>
					<span>{{__('pages/welcome.foods_desc')}}</span>

					<div class="analyze-report__food-rating">
						<img src="{{url('/')}}/new/img/iyi-besin.webp" width="100"/>
						<div class="content pr-2">
							<span>{{__('pages/welcome.foods_item_title_1')}}</span>
							<p>
								{{__('pages/welcome.foods_item_desc_1')}}
							</p>
						</div>
					</div>
					<div class="analyze-report__food-rating">
						<img src="{{url('/')}}/new/img/orta-besin.webp" width="100"/>
						<div class="content pr-2">
							<span>{{__('pages/welcome.foods_item_title_2')}}</span>
							<p>
								{{__('pages/welcome.foods_item_desc_2')}}
							</p>
						</div>
					</div>
					<div class="analyze-report__food-rating">
						<img src="{{url('/')}}/new/img/kotu-besin.webp" width="100"/>
						<div class="content pr-2">
							<span>{{__('pages/welcome.foods_item_title_3')}}</span>
							<p>
								{{__('pages/welcome.foods_item_desc_3')}}
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12 col-lg-6 col-xl-6 iphone-wrapper">
				<div class="device device-ipad-pro device-gold">
					<div class="device-frame">
						<iframe class="device-content" src="https://app.enbiosis.com/demo/foods/tr" style="object-fit: initial;width:100%;height:100%;border:0;"></iframe>
					</div>
					<div class="device-stripe"></div>
					<div class="device-header"></div>
					<div class="device-sensors"></div>
					<div class="device-btns"></div>
					<div class="device-power"></div>
				</div>
			</div>
		</div>
		<div class="row d-flex justify-content-center">
			<div class="col-md-10 analyze-report__quote">
				<i>
					{{__('pages/welcome.report_quote')}}
				</i>
				{{-- <br/><br/>
				<a class="link-btn link-btn--orange" href="{{url('/checkout')}}"
				onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'button', Label: '{{slug('satin_al')}}' })">
					{{__('pages/welcome.simdi_kesfet')}}
				</a> --}}
			</div>
		</div>
	</div>
</section>
@endsection
