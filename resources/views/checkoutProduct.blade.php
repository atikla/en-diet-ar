@extends('layouts.public')
@section('subpageMenu','topmenu--subpage')
@section('pageTitle', __('pages/checkout.page_title'))
@section('pageDescription', __('pages/checkout.page_description'))
@section('metaTag')
<meta name="robots" content="noindex">
@endsection
@section('styles')
	<style>
		input,
		select option,
		textarea {
			text-align: right !important;
		}
	</style>
@endsection
@section('content')
<section class="subpage-header subpage-header--checkout">

	<div class="container">
		<div class="row">
			<div class="subpage-header__content">
				<h1 class="subpage-header__title">{{__('pages/checkout.header_title')}}</h1>
				<div class="subpage-header__seperator"></div>
				<div class="subpage-header__breadcrumb">
					<nav aria-label="breadcrumb">
						<ol itemscope itemtype="https://schema.org/BreadcrumbList"
						class="breadcrumb no-bg-color text-light">
							<li itemprop="itemListElement" itemscope
							itemtype="https://schema.org/ListItem"
							class="breadcrumb-item">
								<a itemprop="item"
								style="color:#007bff"
								href="{{ route('/')}}">
								<span itemprop="name">{{__('pages/checkout.breadcrumb.0')}}</span>
								</a>
								<meta itemprop="position" content="1"/>
							</li>
							<li itemprop="itemListElement" itemscope
							itemtype="https://schema.org/ListItem"
							class="breadcrumb-item active te">
								<a itemprop="item" 
								style="color:#007bff"
								href="{{ route('checkout')}}">
									<span itemprop="name">{{__('pages/checkout.breadcrumb.1')}}</span>
								</a>
								<meta itemprop="position" content="2"/>
                            </li> 
                            <li itemprop="itemListElement" itemscope
							itemtype="https://schema.org/ListItem"
							class="breadcrumb-item active te">
								<a itemprop="item" 
								style="color:#6c757d"
								href="{{ route('checkout.product', $product->slug)}}">
									<span itemprop="name">{{strip_tags(__('pages/checkout.'.$product->slug))}}</span>
								</a>
								<meta itemprop="position" content="3"/>
							</li>
						</ol>
					</nav>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="buy" >
	@php
		$cart = session()->get('cart');
	@endphp
	<div class="container">
		<div class="row buy__row">
			<div class="col-md-12 col-lg-7 col-xl-7 text-right" style="direction: rtl">
				{!! Form::open(['method'=>'POST', 'action' => ['ShopController@CheckOut', $product->slug], 'class' => 'needs-validation', 'id' => 'shop-form']) !!}

				<div class="row">
					<div class="col-md-12">
						<input type="hidden" name="quantity" class="quantity" value="1">
						<h1 class="buy__formtitle" style="margin-top:0;">{{__('pages/checkout.kargo_adesi.0')}} <b> {{__('pages/checkout.kargo_adesi.1')}}</b></h1>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-12 buy__input">
						<label for="exampleInputEmail1">{{__('pages/checkout.adi_soyadi')}}</label>
						{!! Form::text('name', null, ['class'=>'form-control '.($errors->has('name') ? 'error-border':''), 'required']) !!}
						@error('name')
							<small id="emailHelp" class="form-text text-muted">{{$message}}</small>
						@enderror
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-6 buy__input">
						<label for="exampleInputEmail1">{{__('pages/checkout.e_posta')}}</label>
						{!! Form::text('email', null, ['class'=>'form-control '.($errors->has('email') ? 'error-border':''), 'required']) !!}
						@error('email')
							<small id="emailHelp" class="form-text text-muted">{{$message}}</small>
						@enderror
					</div>
					<div class="form-group col-md-6 buy__input">
						<label for="exampleInputEmail1">{{__('pages/checkout.tel_num')}}</label>
						{!! Form::text('phone', null, ['class'=>'form-control '.($errors->has('phone') ? 'error-border':''), 'required']) !!}
						@error('phone')
							<small id="emailHelp" class="form-text text-muted">{{$message}}</small>
						@enderror
					</div>
					<div class="{{'form-group col-md-12 buy__input '.($errors->has('country') ? 'error-border':'')}}">
						<label for="exampleInputEmail1">{{__('pages/checkout.ulke')}}</label>
						{{ Form::select('country', $country, null, ['id' => 'country','class'=>'form-control '.($errors->has('country') ? 'error-border':''), 'required']) }}
						@error('country')
							<small id="emailHelp" class="form-text text-muted">{{$message}}</small>
						@enderror
					</div>
					<div class="{{'form-group col-md-6 buy__input '.($errors->has('city') ? 'error-border':'')}}">
						<label for="exampleInputEmail1">{{__('pages/checkout.il')}}</label>
						{{ Form::select('city', $cities, null, ['id' => 'city','class'=>'form-control '.($errors->has('city') ? 'error-border':''), 'required', 'placeholder' => 'اختر']) }}
						@error('city')
							<small id="emailHelp" class="form-text text-muted">{{$message}}</small>
						@enderror
					</div>
					<div class="form-group col-md-6 buy__input" id="county_div">
						<label for="exampleInputEmail1">{{__('pages/checkout.ilce')}}</label>
					</div>
					{{-- <div class="{{'form-group col-md-6 buy__input '.($errors->has('county') ? 'error-border':'')}}">
						<label for="exampleInputEmail1">{{__('pages/checkout.ilce')}}</label>
						{!! Form::text('county', null, ['class'=>'form-control '.($errors->has('county') ? 'error-border':''), 'required']) !!}
						@error('county')
							<small id="emailHelp" class="form-text text-muted">{{$message}}</small>
						@enderror
					</div> --}}

					<div class="form-group col-md-12 buy__input text-right" >
						<label for="exampleInputEmail1">{{__('pages/checkout.adres')}}</label>
						<textarea class="{{'form-control '.($errors->has('address') ? 'error-border':'')}}" name="address">{{old('address')}}</textarea>
						@error('address')
							<small id="emailHelp" class="form-text text-muted">{{$message}}</small>
						@enderror
					</div>
				</div>
				<div class="row" id="fatura-form"></div>
				<div class="row">
					<div class="col-md-12">
						<h1 class="buy__formtitle"><b>{{__('pages/checkout.odeme')}}</b></h1>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-6 buy__input">
						<label for="exampleInputEmail1">{{__('pages/checkout.kart_sahibi')}}</label>
						{!! Form::text('cc_name', '', ['class'=>'form-control '.($errors->has('cc-name') ? 'error-border':''), 'required']) !!}
						@error('cc_name')
							<small id="emailHelp" class="form-text text-muted">{{$message}}</small>
						@enderror
					</div>
					<div class="form-group col-md-6 buy__input">
						<label for="exampleInputEmail1">{{__('pages/checkout.kart_num')}}</label>
						{!! Form::text('card_number', '', ['class'=>'form-control '.($errors->has('card_number') ? 'error-border':''), 'required']) !!}
						@error('card_number')
							<small id="emailHelp" class="form-text text-muted">{{$message}}</small>
						@enderror
					</div>
				</div>
				<label class="buy__label">{{__('pages/checkout.kart_son_kullanma_tarihi')}}</label>
				<div class="row">

					<div  class="{{'form-group col-md-4 buy__input '.($errors->has('expiration_month') ? 'error-border':'')}}">
						<label for="exampleInputEmail1">{{__('pages/checkout.ay')}}</label>
						<select style="" name="expiration_month" class="{{'error-border form-control '.($errors->has('expiration_month') ? 'error-border':'')}}" id="expiration_month" required>
							@for($i = 1; $i <= 12; ++$i)
								@php
									$old_month = '';
									if ( old('expiration_month') && old('expiration_month') == $i)
										$old_month = 'selected';
								@endphp
								<option {{ $old_month }} value="{{ $i  < 10 ? '0' . $i  : $i }}">{{ $i  < 10 ? '0' . $i  : $i }}</option>
							@endfor
						</select>
						@error('expiration_month')
							<small id="emailHelp" class="form-text text-muted">{{$message}}</small>
						@enderror
					</div>
					<div class="{{'form-group col-md-4 buy__input '.($errors->has('expiration_year') ? 'error-border':'')}}">
						<label for="exampleInputEmail1">{{__('pages/checkout.yil')}}</label>
						<select style="" name="expiration_year" id="expiration_year" class={{'form-control '.($errors->has('expiration_year') ? 'error-border':'')}}>
							@for($i = date('Y'); $i < date('Y') + 20; ++$i)
								@php
									$old_year = '';
									if ( old('expiration_year') && old('expiration_year') == $i)
										$old_year = 'selected';
								@endphp
								<option value="{{$i}}" {{ $old_year }} >{{$i}}</option>
							@endfor
						</select>
						@error('expiration_year')
							<small id="emailHelp" class="form-text text-muted">{{$message}}</small>
						@enderror
					</div>
					<div class="form-group col-md-4 buy__input">
						<label for="exampleInputEmail1">{{__('pages/checkout.cvc')}}</label>
						{!! Form::text('cvc', null, ['style' => 'display:inline','class'=>'form-control '.($errors->has('cvc') ? 'error-border':''), 'required']) !!}
						@error('cvc')
							<small id="emailHelp" class="form-text text-muted">{{$message}}</small>
						@enderror
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-12 buy__input text-right" style="direction: rtl;">
						<ul>
							<li>
								<input {{ old('Mesafeli_Satış_Sözleşmesi') ? 'checked': '' }} type="checkbox" name="Mesafeli_Satış_Sözleşmesi"/> <span class="mr-3"><a href="{{route('mesafeli-satis-sozlesmesi')}}" rel="nofollow" style="{{($errors->has('Mesafeli_Satış_Sözleşmesi') ? 'color:red;':'')}}">{!!__('pages/checkout.sozlesme.0')!!}</a></span>
							</li>
							<li>
								<input {{ old('Ön_Bilgilendirme_Formu') ? 'checked': '' }} type="checkbox" name="Ön_Bilgilendirme_Formu"/> <span class="mr-3"><a href="{{route('on-bilgilendirme-formu')}}" rel="nofollow" style="{{($errors->has('Ön_Bilgilendirme_Formu') ? 'color:red;':'')}}">{!!__('pages/checkout.sozlesme.1')!!}</a></span>
							</li>
							<li>
								<input {{ old('Aydınlatma_Formu') ? 'checked': '' }} type="checkbox" name="Aydınlatma_Formu"/> <span class="mr-3"><a href="{{route('aydinlatma-formu')}}" rel="nofollow" style="{{($errors->has('Aydınlatma_Formu') ? 'color:red;':'')}}">{!!__('pages/checkout.sozlesme.2')!!}</a></span>
							</li>
						</ul>
						<input type="hidden" name="taksit" value="{{ session()->get('instalment') ? session()->get('instalment')[0] : 0}}">
						<input type="hidden" id="de_code" name="de_code" value="">
						<input type="hidden" id="taksit" name="taksit" value="">
						
					</div>
				</div>
				{!! Form::close() !!}
			</div>
			<div class="col-md-12 col-lg-5 col-xl-5 buy__right text-right">
				<div class="row">
					<div class="col-md-12">
						<div class="buy__productname" style="margin-bottom:20px;">
							<span>{!!__('pages/checkout.'.$product->slug)!!}</span>
						</div>
						<div class="buy__productprice">
							@if(!session()->get('coupon'))

							<span style="direction: rtl">
								السعر:
								{{-- <strike>
									<small>{{ '$ ' . ($cart['price'] + 500)}}</small>
								</strike> --}}
								{{ '$ ' . $cart['price'] }} 
							</span>
							@else
							<span style="direction: rtl">
								السعر:
								{{-- <strike>
									<small>{{ '$ ' . ($cart['price'] + 500)}}</small>
								</strike> --}}
								<strike class="mr-2"> {{ '$ ' . $cart['price'] }} </strike>
							</span> <br>
							<span style="direction: rtl">
								السعر المخفض:
								$ {{ round ( (double) ( ( ( 100 - session()->get('coupon') ) * $cart['price'] ) / 100 ), 2) * $cart['quantity']}}
							</span>
							@endif

							{{-- <div class="buy__pricediv">
								<div>
								@if(session()->get('coupon'))
								<span id="text_for_price">
									<b>{{__('pages/checkout.fiyat')}}: </b>
									<span id="text_discound_price" class="mr-2">
										<strike>
												<small>{{$cart['price'] + 500}} x {{ $cart['quantity'] }} = {{ ($cart['price'] + 500) * $cart['quantity'] }}</small>
										</strike>
									</span>
									<strike>
										<span id="orginal_price">
											{{ $cart['price'] }} x {{ $cart['quantity'] }} = {{ $cart['price'] * $cart['quantity'] }}
										</span>TL
									</strike>
								</span><br>
								<span id="text_for_rate_price">
									<b> {{__('pages/checkout.indirimli_fiyat')}} : </b>
									<span id="rate_price">
											{{ round ( (double) ( ( ( 100 - session()->get('coupon') ) * $cart['price'] ) / 100 ), 2) }} x {{ $cart['quantity'] }}
											= {{ round ( (double) ( ( ( 100 - session()->get('coupon') ) * $cart['price'] ) / 100 ), 2) * $cart['quantity']}}
									</span>TL
								</span>
									 @php
										 $total = round ( (double) ( ( ( 100 - session()->get('coupon') ) * $cart['price'] * $cart['quantity'] ) / 100 ), 2) ;
									 @endphp
								@else
									<span id="text_for_price"><b>{{__('pages/checkout.fiyat')}}:</b>
									<span id="text_discound_price" class=" mr-2">
										<strike>
											<small>{{$cart['price'] + 500}} x {{ $cart['quantity'] }} = {{ ($cart['price'] + 500) * $cart['quantity'] }}</small>
										</strike>
									</span>
									<span id="orginal_price"> {{ $cart['price'] }} x {{ $cart['quantity'] }} = {{ $cart['price'] * $cart['quantity'] }} </span> TL 
								</span>
									<br><span id="text_for_rate_price"></span>

									@php
										$total = $cart['price'] * $cart['quantity'];
									@endphp
								@endif
								</div>
							</div> --}}
							{{-- <a href="javascript:void(0);" class="buy__quantitylink">{{__('pages/checkout.miktar_degistir')}}</a> --}}
						</div>
						{{-- {!! Form::open(['method'=>'POST', 'action' => 'ShopController@updateCartProduct', 'class' => 'needs-validation', 'id' => 'updateCart']) !!}
						<div class="input-group mt-3 buy__input buy__quantitydiv">

								<input type="text" value="{{$cart['quantity']}}" class="form-control" name="quantity" id="quantity" aria-describedby="emailHelp" placeholder="Miktar">
								<div class="input-group-append">
									<button class="btn btn-secondary" type="submit">{{__('pages/checkout.degistir')}}</button>
								</div>

						</div>
						{!! Form::close() !!} --}}
					</div>
				</div>
				<a href="javascript:void(0);" class="couponlink buy__couponlink" style="display: inline-block">{{__('pages/checkout.kupon_kullan')}}</a>
				<div class="row">
					<div class="form-group col-md-12 buy__productprice buy__couponforms mt-3">
						@if(session()->get('coupon'))
							<label style="width:72%">%{{session()->get('coupon')}} {{__('pages/checkout.indirim_uygulanmistir')}}</label>
								<input style="width:24%"type="submit" class="btn btn-secondary ml-2" value="{{__('pages/checkout.sil')}}" onclick="removeCoupon()">
						@else
							<div class="input-group mb-3 buy__input">
								<input type="text" class="form-control" name="code" id="code" aria-describedby="emailHelp" placeholder="{{__('pages/checkout.kupon_kodu')}}">
								<div class="input-group-append">
									<input type="submit" class="btn btn-secondary" value="{{__('pages/checkout.sorgula')}}" onclick="checkForCoupon()">
								</div>
							</div>
						@endif
					</div>
				</div>
				<div class="row mt-2">
					<div class="form-group col-md-12 buy__input">
						<div class="mt-3">
							<button class="link-btn link-btn--orange" style="border: none;padding: 10px 30px;" onclick="dopayment()"> {{__('pages/checkout.gonder')}}</button>
							{{-- {!! Form::submit(__('pages/checkout.gonder'), ['class'=>'link-btn link-btn--orange']) !!} --}}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).ready(function() {
		$('#payment_type').select2({
			minimumResultsForSearch: -1
		});
		$('#city').select2({
			minimumResultsForSearch: -1
		});
		$('#product_select').select2({
			minimumResultsForSearch: -1
		})
		$('#expiration_month').select2({
			 minimumResultsForSearch: -1
		});
		$('#expiration_year').select2({
			 minimumResultsForSearch: -1
		});
	});
</script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
	function removeCoupon(){
		$.ajax({
			url: '{{ url('removeCouponFromPaymentForm') }}',
			method: "POST",
			data: {_token: '{{ csrf_token() }}'},
			success: function (response) {
				location.reload();
				// let text = '<div class="input-group mb-3 buy__input">'+
				// 				'<input type="text" class="form-control" name="code" id="code" aria-describedby="emailHelp" placeholder="{{__('pages/checkout.kupon_kodu')}}">'+
				// 				'<div class="input-group-append">'+
				// 					'<input type="submit" class="btn btn-secondary" value="{{__('pages/checkout.sorgula')}}" onclick="checkForCoupon()">'+
				// 				'</div>'+
				// 			'</div>';
				// let text_for_price = '<b>{{__('pages/checkout.fiyat')}}:</b>'+
				// 	'<span id="orginal_price">'
				// 		+'<span id="text_discound_price" class="mr-2">'
				// 			+'<strike>'
				// 					+'<small> ' + ( response.price + 500 ) +' x ' + response.quantity + ' = ' + ( response.price + 500 ) * response.quantity + '</small>'
				// 			+'</strike>'
				// 		+'</span>' 
				// 	+ response.price + ' x ' + response.quantity +' = ' + response.price * response.quantity + ' </span> TL ';
				// $('.buy__couponforms').html(text);
				// $('#text_for_rate_price').text('');
				// $('#text_for_price').html(text_for_price);
				// gettable();

			},
			error: function (response) {
				sweetAlert('Hata Oluştu', 'aaaa', 'error');

			}
		});
	}
	function gettable() {
		$.ajax({
			url: '{{ url('instalmentsTable') }}',
			method: "POST",
			data: {_token: '{{ csrf_token() }}'},
			success: function (response) {
				$('tbody').empty();
				$('tbody').html(response);
				console.log(response);
			}
		});
	}
	function checkForCoupon() {
		let code = $('#code').val();
		$.ajax({
			url: '{{ url('checkForCoupon') }}',
			method: "POST",
			data: {_token: '{{ csrf_token() }}', code: code},
			success: function (response) {
				location.reload();
				// $('.coupon__forms').empty();
				// let text = '<label style="width:72%">%' + response.coupon+ ' {{__('pages/checkout.indirim_uygulanmistir')}}</label>'+
				// 			'<input style="width:24%" type="submit" class="btn btn-secondary ml-2" value="{{__('pages/checkout.sil')}}" onclick="removeCoupon()">';
				// let text_price = '<b>{{__('pages/checkout.fiyat')}} : </b>'
				// 					+'<span id="text_discound_price" class=" mr-2">'
				// 							+'<strike>'
				// 									+'<small> ' + ( response.price + 500 ) +' x ' + response.quantity + ' = ' +  ( response.price + 500 ) * response.quantity + '</small>'
				// 							+'</strike>'
				// 					+'</span>'
				// 					+'<strike>'+
				// 					'<span id="orginal_price">'+
				// 						response.price + ' x ' + response.quantity + ' = ' + response.quantity * response.price +
				// 					'</span> TL</strike>';
				// let text_rate = '<b> {{__('pages/checkout.indirimli_fiyat')}} : </b><span id="rate_price">'+
				// 					( ( ( 100 - response.coupon)  * response.price ) / 100 ).toFixed(2) + ' x ' + response.quantity +
				// 					' = ' +  ( ( ( ( 100 - response.coupon ) * response.price ) / 100 ) * response.quantity ).toFixed(2) +
				// 			'</span>TL';
				// console.log(text_rate);
				// $('.buy__couponforms').empty();
				// $('#text_for_price').empty();
				// $('#text_for_rate_price').empty();
				// $('.buy__couponforms').append(text);
				// $('#text_for_price').append(text_price);
				// $('#text_for_rate_price').append(text_rate);
				// gettable();
			},
			error: function (response) {
				console.log(response);
				sweetAlert('Hata Oluştu', response.responseJSON.errors, 'error');
				// sweetAlert('Hata Oluştu', 'lütfen yeniden deneyiniz', "error");

			}
		});
	}
	function dopayment(){
		$('#shop-form').submit();
	}
	$(document).ready(function(){
		$('#updateCart').submit( function (e) {
			e.preventDefault();
			let quantity = $('#quantity').val();
			$.ajax({
                url: '{{ url('update-cart') }}',
                method: "POST",
                data: {_token: '{{ csrf_token() }}', quantity: quantity},
                success: function (response) {
					let price = response.data.orginal['price'];
					let quantity = response.data.orginal['quantity'];
					let text = price + " x " + quantity + " = " + ( price * quantity );
					let dis = "<strike><small> " + ( price + 500 ) +" x " + quantity + " = " + ( (price + 500)  * quantity) + "</small></strike>"
					if (response.data.coupon) {
						let rate =  ( ( ( 100 - response.data.coupon['coupon'] ) * price ) / 100 ).toFixed(2) ;
						let rate_text = rate + " x " + quantity + " = " + (rate * quantity ).toFixed(2);
						$('#rate_price').text(rate_text);
					}
					$('#text_discound_price').html(dis);
					$('#orginal_price').text(text);
					// gettable();
					sweetAlert('Tebrikler', 'Kit Miktarı Değiştirildi', 'success');
                },
				error: function (response) {
					 sweetAlert('Bir Hata Oluştu', response.responseJSON.errors['quantity'][0] +' lütfen yeniden deneyiniz', "error");
				}
			});
		});
		$('#shop-form').submit( function (){
			$('#de_code').val($('#temp_de_code').val());
			$('#taksit').val( $("input[type='radio'][name='instalment']:checked").val() );
		});
		$('.de-form-radio').change(function(){
			if($(this).val() == "true"){
				$('.de-form').show();
			}else{
				$('.de-form').hide();
			}
		});

		$('.fatura-form-radio').change(function(){
			if($(this).val() == "true"){
				$('#fatura-form').empty();
			}else{
				let from = '<div class="col-md-12">'+
						'<input type="hidden" name="quantity" class="quantity" value="1">'+
						'<h1 class="buy__formtitle"> {{__('pages/checkout.fatura_adesi.0') }}<b>{{__('pages/checkout.fatura_adesi.1') }}</b></h1>'+
					'</div>'+
					'<div class="form-group col-md-6 buy__input">'+
						'<label for="exampleInputEmail1">{{__('pages/checkout.il') }}</label>'+
						'<select id="fcity" class="form-control" required="" name="fcity">';
					$('#city option').each(function() {
						from += '<option value="' + $(this).val() + '">' + $(this).text() + '</option>';
					});
					from += '</select>'+
						'</div>'+
						'<div class="form-group col-md-6 buy__input" id="fcounty_div">'+
						'<label for="exampleInputEmail1">{{__('pages/checkout.ilce') }}</label>'+
					'</div>'+
					'<div class="form-group col-md-12 buy__input">'+
						'<label for="exampleInputEmail1">{{__('pages/checkout.fadres') }}</label>'+
						'<textarea class="form-control" name="faddress" placeholder="{{__('pages/checkout.fadres_place') }}"></textarea>'+
					'</div>';
				$('#fatura-form').append(from);
				$('#fcity').select2({
					minimumResultsForSearch: -1
				});
			}
		});
		$('#city').change(function(){
			city_id = $('#city option:selected').attr('value');
			$.ajax({
                url: '{{ url('getCounty') }}',
                method: "POST",
                data: {_token: '{{ csrf_token() }}', id: city_id},
                success: function (response) {
					let select = '<select id="county" class="form-control" required="" name="county">'+
								 '<option selected="selected">اختر</option>';
					let data = response.data;
					Object.keys(data).forEach(d => {
						select += '<option value="' + d + '">' + data[d] + '</option>';
					});
					select += '</select>'
					$('#county').remove();
					$('#county_div').append(select);
					$('#county').select2({
						minimumResultsForSearch: -1
					});
				},
				error: function (response) {
					$('#new__loading').removeClass("loading");
					$('#new__loading-logo').removeClass("loading-logo");
                }
			});
		});
	});
	$('input[type="radio"][name="instalment"]').click(function() {
		$('#installments-table').toggle();
	});
	$('.buy__quantitylink').click(function(){
		$('.buy__quantitydiv').toggleClass('buy__quantitydivshow');
	});
	$('.couponlink').click(function(){
		$('.buy__couponforms').toggle();
	});
	$('.installmentlink').click(function(){
		$('#installments-table').toggle();
	});
	$(document).change(function(){
		$('#fcity').change(function(){
				city_id = $('#fcity option:selected').attr('value');
				$.ajax({
					url: '{{ url('getCounty') }}',
					method: "POST",
					data: {_token: '{{ csrf_token() }}', id: city_id},
					success: function (response) {
						let select = '<select id="fcounty" class="form-control" required="" name="fcounty">'+
							'<option selected="selected">اختر</option>';
						let data = response.data;
						Object.keys(data).forEach(d => {
							select += '<option value="' + d + '">' + data[d] + '</option>';
						});
						select += '</select>'
						$('#fcounty').remove();
						$('#fcounty_div').append(select);
						$('#fcounty').select2({
							minimumResultsForSearch: -1
						});
					},
					error: function (response) {
						$('#new__loading').removeClass("loading");
						$('#new__loading-logo').removeClass("loading-logo");
					}
				});
			});
		});
</script>
@if ( count($errors) > 0 || old('county') )
	<script>
		$(document).ready(function(){
			city_id = $('#city option:selected').attr('value');
			$.ajax({
				url: '{{ url('getCounty') }}',
				method: "POST",
				data: {_token: '{{ csrf_token() }}', id: city_id},
				success: function (response) {
					let select = '<select id="county" class="form-control" required="" name="county">'+
									'<option selected="selected"></option>';
					let data = response.data;
					let selected = '';
					Object.keys(data).forEach(d => {
						if ( d == {{old('county')}} )
							selected = 'selected';
						select += '<option value="' + d + '" '+ selected +'>' + data[d] + '</option>';
						selected = '';
					});
					select += '</select>';
					@error('county')
						select += '<small id="emailHelp" class="form-text text-muted">{{$message}}</small>';
					@enderror
					$('#county').remove();
					$('#county_div').append(select);
					$('#county').select2({
						minimumResultsForSearch: -1
					});
				},
				error: function (response) {
					$('#new__loading').removeClass("loading");
					$('#new__loading-logo').removeClass("loading-logo");
				}
			});
		});
	</script>
@endif
@endsection
