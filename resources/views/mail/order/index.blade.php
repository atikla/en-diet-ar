@extends('mail.layouts.order')
@section('content')
    
  <tr  style="direction:rtl">
    <td align="center" valign="top" width="100%" style="background-color: #f7f7f7;" class="content-padding">
      <center>
        <table cellspacing="0" cellpadding="0" width="600" class="w320">
          <tr>
            <td class="header-lg">
              شكرا لطلبك
            </td>
          </tr>
          <tr>
            <td class="free-text">
              تم استلام طلبك من قبلنا ، وسيتم تسليم شحنتك إلى عنوانك في أقرب وقت ممكن.
              <br>
              تفاصيل طلبك أدناه.
            </td>
          </tr>
          {{-- <tr>
            <td class="button">
              <div><!--[if mso]>
                <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="http://" style="height:45px;v-text-anchor:middle;width:155px;" arcsize="15%" strokecolor="#ffffff" fillcolor="#ff6f6f">
                  <w:anchorlock/>
                  <center style="color:#ffffff;font-family:Helvetica, Arial, sans-serif;font-size:14px;font-weight:regular;">Track Order</center>
                </v:roundrect>
            <![endif]--><a href="{{route('tesekkurler', '1')}}"
              style="background-color:#ff6f6f;border-radius:5px;color:#ffffff;display:inline-block;font-family:'Cabin', Helvetica, Arial, sans-serif;font-size:14px;font-weight:regular;line-height:45px;text-align:center;text-decoration:none;width:155px;-webkit-text-size-adjust:none;mso-hide:all;">Track Order</a></div>
            </td>
          </tr> --}}
          <tr>
            <td class="w320">
              <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                  <td class="mini-container-left">
                    <table cellpadding="0" cellspacing="0" width="100%">
                      <tr>
                        <td class="mini-block-padding">
                          <table cellspacing="0" cellpadding="0" width="100%" style="border-collapse:separate !important;">
                            <tr>
                              <td class="mini-block">
                                <span class="header-sm">عنوان التسليم</span><br />
                                {{ $order->name }} <br />
                                {{ $order->address }} <br />
                                {{$order->county ? $order->county->county : ''}}, {{$order->city ? $order->city->city : ''}} <br />
                                {{$order->city ? config('cities.' . $order->city->country_code . '.name') : ''}}
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td class="mini-container-right">
                    <table cellpadding="0" cellspacing="0" width="100%">
                      <tr>
                        <td class="mini-block-padding">
                          <table cellspacing="0" cellpadding="0" width="100%" style="border-collapse:separate !important;">
                            <tr>
                              <td class="mini-block">
                                <span class="header-sm">تاريخ الطلب</span><br />
                                {{$order->created_at->isoformat('MMMM D, Y')}}<br />
                                <br />
                                <span class="header-sm">رقم تتبع الطلب</span> <br />
                                #{{$order->tracking}}
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </center>
    </td>
  </tr>
  <tr>
    <td align="center" valign="top" width="100%" style="background-color: #ffffff;  border-top: 1px solid #e5e5e5; border-bottom: 1px solid #e5e5e5;">
      <center>
        <table cellpadding="0" cellspacing="0" width="600" class="w320">
            <tr>
              <td class="item-table">
                <table cellspacing="0" cellpadding="0" width="100%">
                  <tr>
                    <td class="title-dark" width="300">
                      اسم المنتج
                    </td>
                    <td class="title-dark" width="163">
                      قطعة
                    </td>
                    <td class="title-dark" width="97">
                      المجموع
                    </td>
                  </tr>
                  @php
                      $total = 0
                  @endphp
                @foreach ($order->orderDetail as $detail)
                @php
                    $price = $detail->price;
                    $total = $total + ( $detail->price * $detail->quantity) ;
                @endphp
                    
                    <tr>
                        <td class="item-col item">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                            <td>
                                <img width="110" height="92" src="https://www.enbiosis.com/new/img/enbiosis-box.webp" alt="item1">
                            </td>
                            <td class="product">
                                <span style="color: #4d4d4d; font-weight:bold;">{{ __('pages/checkout.'.$detail->product->slug) }}</span> <br />
                            </td>
                            </tr>
                        </table>
                        </td>
                        <td class="item-col quantity">
                        {{$detail->quantity}}
                        </td>
                        <td class="item-col">
                        {{$detail->price}} $
                        </td>
                    </tr>
                @endforeach
                  
                  <tr>
                    <td class="item-col item mobile-row-padding"></td>
                    <td class="item-col quantity"></td>
                    <td class="item-col price"></td>
                  </tr>


                  <tr>
                    <td class="item-col item">
                    </td>
                    <td class="item-col quantity" style="text-align:right; padding-right: 10px; border-top: 1px solid #cccccc;">
                        <span class="total-space">المجموع الفرعي</span> <br />
                        @if ($order->coupon)
                            <span class="total-space">{{$order->coupon->discount}}% قسيمة تخفيض </span>  <br />
                            <span class="total-space">المجموع الفرعي</span> <br />

                        @endif
                        @if ($order->payment)
                            @if ($order->payment->taksit > 1)
                                <span class="total-space">{{$order->payment->taksit}} taksit </span>  <br />
                                <span class="total-space">المجموع الفرعي</span> <br />
                            @endif
                        @endif
                        <br>
                        <span class="total-space" style="font-weight: bold; color: #4d4d4d">المجموع الكلي</span>
                    </td>
                    <td class="item-col price" style="text-align: left; border-top: 1px solid #cccccc;">
                        <span class="total-space">{{$total}} $</span> <br />
                        @if ($order->coupon)
                            <span class="total-space">- {{ $total - round ( (double) ( ( ( 100 - $order->coupon->discount ) * $total ) / 100 ), 2) }} $</span>  <br />
                            @php
                                $total = round ( (double) ( ( ( 100 - $order->coupon->discount ) * $total ) / 100 ), 2)
                            @endphp
                            <span class="total-space">{{$total}} $</span> <br />
                        
                        @endif
                        @if ($order->payment)
                            @if ($order->payment->taksit > 1)
                                @php
                                    foreach ( config('instalments') as $key => $item ){
                                        if ( $order->payment->taksit ==  $item['taksit'] ) {
                                            $added  = round ( (double) ( ( $total * $item['rate'] ) / 100 ), 2) ;
                                            $total = round( $total +  $added , 2);
                                        }
                                    }
                                @endphp
                                <span class="total-space">+ {{ $added }} $</span>  <br />
                              
                                <span class="total-space">{{$total}} $</span> <br />
                            @endif
                        @endif
                        <br>
                        <span class="total-space" style="font-weight:bold; color: #4d4d4d">{{$order->price}} $</span>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>

        </table>
      </center>
    </td>
  </tr>
@endsection
