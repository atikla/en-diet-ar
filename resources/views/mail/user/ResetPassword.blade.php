@extends('mail.layouts.kit')
@section('content')
<tr>
  <td align="center" valign="top" width="100%" style="background-color: #f7f7f7;" class="content-padding">
    <center>
      <table cellspacing="0" cellpadding="0" width="600" class="w320">
        <tr>
          <td class="mini-block-container">
            <table cellspacing="0" cellpadding="0" width="100%"  style="border-collapse:separate !important;">
              <tr>
                <td class="mini-block">
                  <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td >
                        Hesabınız için parola sıfırlama isteği aldığımız için bu e-postayı alıyorsunuz.
                      </td>
                    </tr>
                  </table>
                  <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td class="button">
                              <div><!--[if mso]>
                                <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="http://" style="height:45px;v-text-anchor:middle;width:155px;" arcsize="15%" strokecolor="#ffffff" fillcolor="#ff6f6f">
                                  <w:anchorlock/>
                                  <center style="color:#ffffff;font-family:Helvetica, Arial, sans-serif;font-size:14px;font-weight:regular;">Shop Now</center>
                                </v:roundrect>
                              <![endif]-->
                              <a class="button-mobile" href="{{$url}}"
                              style="background-color:#ff6f6f;border-radius:5px;color:#ffffff;display:inline-block;font-family:'Cabin', Helvetica, Arial, sans-serif;font-size:14px;font-weight:regular;line-height:45px;text-align:center;text-decoration:none;width:300px;">
                                Parolayı Sıfırlamak için tıklayınız
                            </a></div>
                            </td>
                          </tr>
                  </table>
                  <table style=" margin-top:30px; margin-bottom:-30px; text-align: left" align="left" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td >
                        <small>Bu parola sıfırlama linki 60 dakika sonra geçerliliğini yitirecek.</small>
                      </td>
                    </tr>
                  </table>
                  <table style=" margin-top:30px; margin-bottom:-30px; text-align: left" align="left" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td >
                        <hr>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </center>
  </td>
</tr>
@endsection
