@extends('mail.layouts.kit')
@section('content')
<tr>
  <td align="center" valign="top" width="100%" style="background-color: #f7f7f7;" class="content-padding">
    <center>
      <table cellspacing="0" cellpadding="0" width="600" class="w320">
        <tr>
          <td class="mini-block-container">
            <table cellspacing="0" cellpadding="0" width="100%"  style="border-collapse:separate !important;">
              <tr>
                <td class="mini-block">
                  <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td >
                        {{-- <span style="font-weight:700; color: #ff6f6f; font-size: 18px;">{{$kit_code}}</span> --}}
                        E-posta adresinizi doğrulamak için lütfen aşağıdaki kodu kullanınız.
                      </td>
                    </tr>
                  </table>
                  <table cellpadding="0" cellspacing="0" width="100%" style="margin-top:20px">
                        {{-- <span style="font-weight:700; color: #ff6f6f; font-size: 18px;">{{$kit_code}}</span> --}}
                    <tr>
                      <td style="color: #65cde6;" class="code-block">
                        {{ $verify_code }}
                      </td>
                    </tr>
                  </table>
                  <table style=" margin-top:30px; margin-bottom:-30px; text-align: left" align="left" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td >
                        <small>Doğrulama kodunuzun geçerlilik süresi iki saattir. Kodunuzun geçerlilik süresi dolduysa tekrar kod isteyiniz.</small>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </center>
  </td>
</tr>
@endsection
