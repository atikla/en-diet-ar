@extends('mail.layouts.kit')
@section('content')
<tr>
  <td align="center" valign="top" width="100%" style="background-color: #f7f7f7;" class="content-padding">
    <center>
      <table cellspacing="0" cellpadding="0" width="600" class="w320">
        <tr>
          <td class="mini-block-container">
            <table cellspacing="0" cellpadding="0" width="100%"  style="border-collapse:separate !important;">
              <tr>
                <td class="mini-block">
                  <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td >
                        {{-- <span style="font-weight:700; color: #ff6f6f; font-size: 18px;">{{$kit_code}}</span> --}}
                        <span style="font-weight:700; color: #65cde6; font-size: 18px;">{{$kit_code}}</span>
                        kodlu kitinizin sonuçları çıktı.
                      </td>
                    </tr>
                  </table>
                  <table style=" margin-top:30px; margin-bottom:-30px; text-align: left" align="left" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td >
                        <a style ="color:#ff6f6f" href="https://app.enbiosis.com/login"> app.enbiosis.com </a>  dan giriş yaparak sonuçlarınızı görebilirsiniz.
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </center>
  </td>
</tr>
@endsection