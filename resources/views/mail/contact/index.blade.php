@extends('mail.layouts.kit')
@section('content')
<td align="center" valign="top" width="100%" style="background-color: #f7f7f7;" class="content-padding">
    <center>
      <table cellspacing="0" cellpadding="0" width="600" class="w320">
        <tr>
          <td class="mini-block-container">
            <table cellspacing="0" cellpadding="0" width="100%"  style="border-collapse:separate !important;">
              <tr>
                <td class="mini-block">
                  <table class="pull-left" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="float:left">
                            <b>Adı Soyadı:</b> <span>{{$contact->name}}</span>
                        </td>
                    </tr>
                    <tr>
                        <td style="float:left">
                            <b>Email:</b> <span>{{$contact->email}}</span>
                        </td>
                        
                    </tr>
                    <tr>
                        <td style="float:left">
                            <b>Başlık:</b> <span>{{$contact->subject}}</span>
                        </td>
                        
                    </tr>
                    <tr>
                        <td>
                            <h3><b>mesaj</b></h3>
                        </td>
                    </tr>
                    <tr>
                        <td style="float:left">
                            <span>{{$contact->mesaj}}</span>
                        </td>
                    </tr>
                   
                  </table>
                  <table style=" margin-top:30px; margin-bottom:-30px; text-align: left" align="left" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td >
                        {{\Carbon\Carbon::now()->isoformat('dddd - DD MMM - Y - HH:mm')}}'da gönderilmiştir.
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </center>
  </td>
</tr>
@endsection