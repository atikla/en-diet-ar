@extends('layouts.public')
@section('subpageMenu','topmenu--subpage')

@section('pageTitle',__('pages/urunlerimiz.page_title'))
@section('pageDescription',__('pages/urunlerimiz.page_description'))

@section('content')
<section class="subpage-header subpage-header--microbiome-analysis">
	<div class="container">
		<div class="row">
			<div class="subpage-header__content">
				<h1 class="subpage-header__title">{{__('pages/urunlerimiz.header_title')}}</h1>
				<div class="subpage-header__seperator"></div>
				<div class="subpage-header__breadcrumb">
					<nav aria-label="breadcrumb">
						<ol itemscope itemtype="https://schema.org/BreadcrumbList"
						class="breadcrumb no-bg-color text-light">
							<li itemprop="itemListElement" itemscope
							itemtype="https://schema.org/ListItem"
							class="breadcrumb-item">
								<a itemprop="item"
								style="color:#007bff"
								href="{{ route('/')}}">
								<span itemprop="name">{{__('pages/urunlerimiz.breadcrumb.0')}}</span>
								</a>
								<meta itemprop="position" content="1"/>
							</li>
							<li itemprop="itemListElement" itemscope
							itemtype="https://schema.org/ListItem"
							class="breadcrumb-item active te">
								<a itemprop="item" 
								style="color:#6c757d"
								href="{{ route('mikrobiyom-analizi')}}">
									<span itemprop="name">{{__('pages/urunlerimiz.breadcrumb.1')}}</span>
								</a>
								<meta itemprop="position" content="2"/>
							</li>
						</ol>
					</nav>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="discover-yourself">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<h2 class="discover-yourself__title">
					{{__('pages/urunlerimiz.discover_title.0')}}<br/>
					{{__('pages/urunlerimiz.discover_title.1')}}
					<b>{{__('pages/urunlerimiz.discover_title.2')}}</b>
				</h2>
				<p class="discover-yourself__subtitle">{{__('pages/urunlerimiz.discover_subtitle')}}</p>
			</div>
			<div class="col-md-6">
				<p class="discover-yourself__desc">
					{{-- {{__('pages/urunlerimiz.discover_desc.2')}} --}}
					{{__('pages/urunlerimiz.discover_desc.0')}}
				</p>
			</div>
			<div class="col-md-12" style="margin-top:20px;">
				
				@if (app()->getLocale() == 'tr')
					<a class="link-btn link-btn--orange" href="{{url('/checkout')}}" style="margin-right:10px;"
					onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'button', Label: '{{slug('satin_al')}}' })">
					{{__('pages/urunlerimiz.satin_al')}}</a>
					<a class="link-btn link-btn--border" href="{{url('/')}}/Microbiome_Demo_TR.pdf">{{__('pages/urunlerimiz.demo_rapor')}}</a>
				@else
				<a class="link-btn link-btn--orange" href="{{url('/')}}/Foods_Demo_EN.pdf">{{__('pages/urunlerimiz.satin_al')}}</a>
				<a class="link-btn link-btn--border" href="{{url('/')}}/Microbiome_Demo_EN.pdf">{{__('pages/urunlerimiz.demo_rapor')}}</a>
				@endif
			</div>
		</div>
	</div>
</section>
<section class="triple-box">
	<div class="container">
		<h2 class="triple-box__title">{{__('pages/urunlerimiz.box_title')}}</h2>
		<p class="triple-box__subtitle">{{__('pages/urunlerimiz.box_subtitle')}}</p>
		<div class="row">
			<div class="col-md-4 triple-box__box">
				<img src="{{url('/')}}/new/img/triple-icon1.webp" />
				<span>{{__('pages/urunlerimiz.box_item_1')}}</span>
			</div>
			<div class="col-md-4 triple-box__box">
				<img src="{{url('/')}}/new/img/triple-icon2.webp" />
				<span>{{__('pages/urunlerimiz.box_item_2')}}</span>
			</div>
			<div class="col-md-4 triple-box__box">
				<img src="{{url('/')}}/new/img/triple-icon3.webp" />
				<span>{{__('pages/urunlerimiz.box_item_3')}}</span>
			</div>
		</div>
	</div>
</section>
@if (app()->getLocale() == 'tr')
<section class="bg-none">
	<div class="container">
		<div class="row">
			<div class="col-12">
				@include('include.video.surec', ['id' => 'surec'])
			</div>
		</div>
	</div>
</section>
@endif
<section class="analyze-report">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="analyze-report__title">
					{{__('pages/urunlerimiz.report_title.0')}}
					<b>{{__('pages/urunlerimiz.report_title.1')}}</b>
				</h2>
			</div>
			<div class="col-md-12 col-lg-6 col-xl-6">
				<div class="analyze-report__item">
					<h3>{{__('pages/urunlerimiz.report_item_title_1')}}</h3>
					<span>{{__('pages/urunlerimiz.report_item_desc_1')}}</span>
				</div>

				<div class="analyze-report__item">
					<h3>{{__('pages/urunlerimiz.report_item_title_2')}}</h3>
					<span>{{__('pages/urunlerimiz.report_item_desc_2')}}</span>
				</div>

				<div class="analyze-report__item">
					<h3>{{__('pages/urunlerimiz.report_item_title_3')}}</h3>
					<span>{{__('pages/urunlerimiz.report_item_desc_3')}}</span>
				</div>


				<div class="analyze-report__item">
					<h3>{{__('pages/urunlerimiz.report_item_title_4')}}</h3>
					<span>{{__('pages/urunlerimiz.report_item_desc_4')}}</span>
				</div>


				<div class="analyze-report__item">
					<h3>{{__('pages/urunlerimiz.report_item_title_5')}}</h3>
					<span>{{__('pages/urunlerimiz.report_item_desc_5')}}</span>
				</div>

			</div>
			<div class="col-md-12 col-lg-6 col-xl-6 iphone-wrapper">
				<div class="device device-ipad-pro device-gold">
					<div class="device-frame">
						@if (app()->getLocale() == 'tr')
							<iframe class="device-content" src="https://app.enbiosis.com/demo/home/tr" style="object-fit: initial;width:100%;height:100%;border:0;"></iframe>
						@else
							<iframe class="device-content" src="https://app.enbiosis.com/demo/home/en" style="object-fit: initial;width:100%;height:100%;border:0;"></iframe>
						@endif
						{{-- <iframe class="device-content" src="https://app.enbiosis.com/demo/home" style="object-fit: initial;width:100%;height:100%;border:0;"></iframe> --}}
					</div>
					<div class="device-stripe"></div>
					<div class="device-header"></div>
					<div class="device-sensors"></div>
					<div class="device-btns"></div>
					<div class="device-power"></div>
				</div>
			</div>
		</div>
		<div class="row analyze-report__besin-skorlari">
			<div class="col-md-12 col-lg-6 col-xl-6 d-flex align-items-center">
				<div class="analyze-report__item">
					<h3>{{__('pages/urunlerimiz.foods_title')}}</h3>
					<span>{{__('pages/urunlerimiz.foods_desc')}}</span>

					<div class="analyze-report__food-rating">
						<img src="{{url('/')}}/new/img/iyi-besin.webp" width="100"/>
						<div class="content">
							<span>{{__('pages/urunlerimiz.foods_item_title_1')}}</span>
							<p>
								{{__('pages/urunlerimiz.foods_item_desc_1')}}
							</p>
						</div>
					</div>
					<div class="analyze-report__food-rating">
						<img src="{{url('/')}}/new/img/orta-besin.webp" width="100"/>
						<div class="content">
							<span>{{__('pages/urunlerimiz.foods_item_title_2')}}</span>
							<p>
								{{__('pages/urunlerimiz.foods_item_desc_2')}}
							</p>
						</div>
					</div>
					<div class="analyze-report__food-rating">
						<img src="{{url('/')}}/new/img/kotu-besin.webp" width="100"/>
						<div class="content">
							<span>{{__('pages/urunlerimiz.foods_item_title_3')}}</span>
							<p>
								{{__('pages/urunlerimiz.foods_item_desc_3')}}
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12 col-lg-6 col-xl-6 iphone-wrapper">
				<div class="device device-ipad-pro device-gold">
					<div class="device-frame">
						@if (app()->getLocale() == 'tr')
							<iframe class="device-content" src="https://app.enbiosis.com/demo/foods/tr" style="object-fit: initial;width:100%;height:100%;border:0;"></iframe>
						@else
							<iframe class="device-content" src="https://app.enbiosis.com/demo/foods/en" style="object-fit: initial;width:100%;height:100%;border:0;"></iframe>
						@endif
						{{-- <iframe class="device-content" src="https://app.enbiosis.com/demo/foods/tr" style="object-fit: initial;width:100%;height:100%;border:0;"></iframe> --}}
					</div>
					<div class="device-stripe"></div>
					<div class="device-header"></div>
					<div class="device-sensors"></div>
					<div class="device-btns"></div>
					<div class="device-power"></div>
				</div>
			</div>
		</div>
		<div class="row d-flex justify-content-center">
			<div class="col-md-8 analyze-report__quote">
				<i>
					{{__('pages/urunlerimiz.report_quote')}}
				</i>
				<br/><br/>
				@if (app()->getLocale() == 'tr')
				<a class="link-btn link-btn--orange" href="{{url('/checkout')}}"
				onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'button', Label: '{{slug('satin_al')}}' })">
					{{__('pages/urunlerimiz.simdi_kesfet')}}
				</a>
				@endif
			</div>
		</div>
	</div>
</section>
<section class="hprocess hprocess--home">
	<div class="container">
		<div class="row hprocess__howto">
			<h1 class="hprocess__howtotitle">{{__('pages/welcome.hprocess_title')}}</h1>
			<div class="col-md-6 col-lg-3 col-xl-3 hprocess__step">
				<div class="hprocess__number">
					<img src="{{ url('/') }}/new/img/icons/surec1.webp" width="50"/>
				</div>
				<h2 class="hprocess__steptitle">{{__('pages/welcome.hprocess_step_title_1')}}</h2>
				<p class="hprocess__stepdesc">
					{{__('pages/welcome.hprocess_step_desc_1')}}
				</p>
			</div>
			<div class="col-md-6 col-lg-3 col-xl-3 hprocess__step">
				<div class="hprocess__number">
					<img src="{{ url('/') }}/new/img/icons/surec2.webp" width="50"/>
				</div>
				<h2 class="hprocess__steptitle">{{__('pages/welcome.hprocess_step_title_2')}}</h2>
				<p class="hprocess__stepdesc">
					{{__('pages/welcome.hprocess_step_desc_2')}}
				</p>
			</div>
			<div class="col-md-6 col-lg-3 col-xl-3 hprocess__step">
				<div class="hprocess__number">
					<img src="{{ url('/') }}/new/img/icons/surec3.webp" width="50"/>
				</div>
				<h2 class="hprocess__steptitle">{{__('pages/welcome.hprocess_step_title_3')}}</h2>
				<p class="hprocess__stepdesc">
					{{__('pages/welcome.hprocess_step_desc_3')}}
				</p>
			</div>
			<div class="col-md-6 col-lg-3 col-xl-3 hprocess__step">
				<div class="hprocess__number">
					<img src="{{ url('/') }}/new/img/icons/surec4.webp" width="50"/>
				</div>
				<h2 class="hprocess__steptitle">{{__('pages/welcome.hprocess_step_title_4')}}</h2>
				<p class="hprocess__stepdesc">
					{{__('pages/welcome.hprocess_step_desc_4')}}
				</p>
			</div>
		</div>
	</div>
</section>
@if (app()->getLocale() == 'tr')
<section class="discover">
	<div class="container">
		<div class="row">
			<div class="col-md-5">
				<h1 class="discover__title">{{__('pages/urunlerimiz.discover_bottom_title')}}</h1>
				<p class="discover__desc">
					{{__('pages/urunlerimiz.discover_bottom_desc.0')}}
					<br/><br/>
					<b>{{__('pages/urunlerimiz.discover_bottom_desc.1')}}</b>
				</p>
				<a class="link-btn link-btn--orange" href="{{url('/checkout')}}"
					onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'button', Label: '{{slug('satin_al')}}' })">
					{{__('pages/urunlerimiz.discover_bottom_button')}}
				</a>
			</div>
		</div>
	</div>
</section>
@endif
<section class="sss">
	<div class="container">
		<div class="row">
			<h2 class="sss__title">{{__('pages/urunlerimiz.sss_title')}}</h2>
		</div>
		<div id="accordion">
			<div class="card">
				<div class="card-header" id="heading1">
					<button data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
						{{__('pages/urunlerimiz.card_1.title')}}
					</button>
					<div class="icon plus"></div>
				</div>

				<div id="collapse1" class="collapse" aria-labelledby="heading1" data-parent="#accordion">
					<div class="card-body">
						{!!__('pages/urunlerimiz.card_1.desc')!!}
					</div>
				</div>
			</div>

			<div class="card">
				<div class="card-header" id="heading2">
					<button data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse2">
						{{__('pages/urunlerimiz.card_2.title')}}
					</button>
					<div class="icon plus"></div>
				</div>

				<div id="collapse2" class="collapse" aria-labelledby="heading2" data-parent="#accordion">
					<div class="card-body">
						{!!__('pages/urunlerimiz.card_2.desc')!!}
					</div>
				</div>
			</div>

			<div class="card">
				<div class="card-header" id="heading3">
					<button data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapse3">
						{{__('pages/urunlerimiz.card_3.title')}}
					</button>
					<div class="icon plus"></div>
				</div>

				<div id="collapse3" class="collapse" aria-labelledby="heading3" data-parent="#accordion">
					<div class="card-body">
						{!!__('pages/urunlerimiz.card_3.desc')!!}
					</div>
				</div>
			</div>

			<div class="card">
				<div class="card-header" id="heading4">
					<button data-toggle="collapse" data-target="#collapse4" aria-expanded="true" aria-controls="collapse4">
						{{__('pages/urunlerimiz.card_4.title')}}
						
					</button>
					<div class="icon plus"></div>
				</div>

				<div id="collapse4" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
					<div class="card-body">
						{!!__('pages/urunlerimiz.card_4.desc')!!}
						
					</div>
				</div>
			</div>

			<div class="card">
				<div class="card-header" id="heading5">
					<button data-toggle="collapse" data-target="#collapse5" aria-expanded="true" aria-controls="collapse5">
						{{__('pages/urunlerimiz.card_5.title')}}
						
					</button>
					<div class="icon plus"></div>
				</div>

				<div id="collapse5" class="collapse" aria-labelledby="heading5" data-parent="#accordion">
					<div class="card-body">
						{!!__('pages/urunlerimiz.card_5.desc')!!}
						
					</div>
				</div>
			</div>
		</div>
		<div class="row d-flex justify-content-center">
			<a class="link-btn link-btn--orange" href="{{url('/sss')}}">{!!__('pages/urunlerimiz.tumu_gor')!!}</a>
		</div>
	</div>
</section>
@endsection
@section('scripts')
<script>
	$(document).ready(function(){
		$(".collapse.show").each(function(){
			$(this).prev(".card-header").find(".icon").addClass("minus").removeClass("plus");
		});

		$(".collapse").on('show.bs.collapse', function(){
			$(this).prev(".card-header").find(".icon").removeClass("plus").addClass("minus");
		}).on('hide.bs.collapse', function(){
			$(this).prev(".card-header").find(".icon").removeClass("minus").addClass("plus");
		});
	});
</script>
@endsection