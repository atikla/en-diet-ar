@extends('layouts.public')
@section('subpageMenu','topmenu--subpage')
@section('pageTitle', __('pages/hakkimizda.page_title'))
@section('pageDescription', __('pages/hakkimizda.page_description'))

@section('content')
<section class="subpage-header subpage-header--about">
	<div class="container">
		<div class="row">
			<div class="subpage-header__content">
				<h1 class="subpage-header__title">{{ __('pages/hakkimizda.header_title')}}</h1>
				<div class="subpage-header__seperator"></div>
				<div class="subpage-header__breadcrumb">
					<nav aria-label="breadcrumb">
						<ol itemscope itemtype="https://schema.org/BreadcrumbList"
						class="breadcrumb no-bg-color text-light">
							<li itemprop="itemListElement" itemscope
							itemtype="https://schema.org/ListItem"
							class="breadcrumb-item">
								<a itemprop="item"
								style="color:#007bff"
								href="{{ route('/')}}">
								<span itemprop="name">{{__('pages/hakkimizda.breadcrumb.0')}}</span>
								</a>
								<meta itemprop="position" content="1"/>
							</li>
							<li itemprop="itemListElement" itemscope
							itemtype="https://schema.org/ListItem"
							class="breadcrumb-item active te">
								<a itemprop="item" 
								style="color:#6c757d"
								href="{{ route('hakkimizda')}}">
									<span itemprop="name">{{__('pages/hakkimizda.breadcrumb.1')}}</span>
								</a>
								<meta itemprop="position" content="2"/>
							</li>
						</ol>
					</nav>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="team">
	<section class="mictobiome-balance" >
		<section class="microbiome-analysis microbiome-analysis--2" style="background-image:url({{ url('/') }}/new/img/about-2.webp);">
			<div class="container">
				<div class="row">
					<div class="col-md-12 mictobiome-balance-mobile">
						<img src="/new/img/about-2-mobile.webp" style="width:100%;height:auto;margin-bottom:70px;"/>
					</div>
					<div class="col-md-12 col-lg-8 col-xl-6">
						<h2 class="microbiome-analysis__title" style="font-size: 34px;"><b>{{__('pages/hakkimizda.__title')}}</b></h2>
						<p class="microbiome-analysis__desc">
							{{__('pages/hakkimizda.team_title.0')}}
							<br/><br/>
							{{__('pages/hakkimizda.team_title.1')}}

						</p>
					</div>
				</div>
			</div>
		</section>
	</section>
	<section class="microbiome" style="background-color:transparent;">
		<div class="container">
			<div class="row d-flex justify-content-center">
				<div class="col-md-6 microbiome__col">
					<img src="{{url('/')}}/new/img/imagine_tomorrow.webp" width="130" />
					<div>
						<span><i>{{__('pages/hakkimizda.odul')}}</i></span>
						<p>{{__('pages/hakkimizda.odul_desc')}}<br/>- {{__('pages/hakkimizda.odul_ceo')}}</p>
					</div>
				</div>
				<div class="col-md-6 microbiome__col">
					<img src="{{url('/')}}/new/img/bayerlogo.webp" width="130" />
					<div>
						<span><i>{{__('pages/hakkimizda.bay_odul_title')}}</i></span>
						<p>{{__('pages/hakkimizda.bay_odul_desc')}}</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<div class="container">
		<div class="row" style="margin-top:70px;text-align:left;">
			<div class="col-md-10"  style="font-family: 'PT Serif', sans-serif;font-size: 17px;font-weight: 400;color: #333333;">
				<span>
					<i>{{__('pages/hakkimizda.que_1')}}</i>
				</span><br/><br/>
				<span>
					<b><i>{{__('pages/hakkimizda.que_1_owner')}}</i></b>
				</span>
			</div>
		</div>
		<div class="row" style="margin-top:70px;display:flex;justify-content:flex-end;text-align:right;">
			<div class="col-md-10"  style="font-family: 'PT Serif', sans-serif;font-size: 17px;font-weight: 400;color: #333333;">
				<span>
					<i>{{__('pages/hakkimizda.que_2')}}</i>
				</span><br/><br/>
				<span>
					<b><i>{{__('pages/hakkimizda.que_2_owner')}}</i></b>
				</span>
			</div>
		</div>
	</div>

	<section class="mictobiome-balance">
		<section class="microbiome-analysis microbiome-analysis--2"  style="background-image:url({{ url('/') }}/new/img/about-1.webp);">
			<div class="container">
				<div class="row">
					<div class="col-md-12 mictobiome-balance-mobile">
						<img src="/new/img/about-1-mobile.webp" style="width:100%;height:auto;margin-bottom:70px;"/>
					</div>
					<div class="col-md-12 col-lg-8 col-xl-6">
						<h2 class="microbiome-analysis__title"><b>{{__('pages/hakkimizda.balance_title.0')}}<br/>{{__('pages/hakkimizda.balance_title.1')}}</b></h2>
						<p class="microbiome-analysis__desc">
							{{__('pages/hakkimizda.balance_desc.0')}}
							<br/><br/>
							{{__('pages/hakkimizda.balance_desc.1')}}
						</p>
					</div>
				</div>
			</div>
		</section>
	</section>
	<div class="container">
		<div class="row team__row">
			<div class="col-md-6 col-lg-4 col-xl-4 team__col">
				<a href="{{route('ceo-omer-ozkan')}}">
					<div class="team__member">
						<img style="width: 210px; height: auto;"class="team__member__avatar" src="{{ url('/') }}/new/img/omer-ozkan.webp" />
						<h2 class="team__member__name">{{__('pages/hakkimizda.member_1.name')}}</h2>
						<span class="team__member__job team__member__job--2">{{__('pages/hakkimizda.member_1.job')}}</span>
						<span class="team__member__job">{{__('pages/hakkimizda.member_1.desc')}}</span>
					</div>
				</a>
			</div>
			<div class="col-md-6 col-lg-4 col-xl-4 team__col">
				<a href="{{route('dr-ozkan-ufuk-nalbantoglu')}}">
					<div class="team__member">
						<img class="team__member__avatar" src="{{ url('/') }}/new/img/ozkan-ufuk-nalbantoglu.webp" />
						<h2 class="team__member__name">{!!__('pages/hakkimizda.member_4.name')!!}</h2>
						<span class="team__member__job team__member__job--2">{!!__('pages/hakkimizda.member_4.job')!!}</span>
						<span class="team__member__job">{{__('pages/hakkimizda.member_4.desc')}}</span>
					</div>
				</a>
			</div>
			<div class="col-md-6 col-lg-4 col-xl-4 team__col">
				<a href="{{route('doc-dr-aycan-gundogdu')}}">
					<div class="team__member">
						<img class="team__member__avatar" src="{{ url('/') }}/new/img/aycan-gundogdu.webp" />
						<h2 class="team__member__name">{!!__('pages/hakkimizda.member_3.name')!!}</h2>
						<span class="team__member__job team__member__job--2">{!!__('pages/hakkimizda.member_3.job')!!}</span>
						<span class="team__member__job">{{__('pages/hakkimizda.member_3.desc')}}</span>
					</div>
				</a>
			</div>
		</div>
	</div>
</section>
@include('include.basin')
@endsection
