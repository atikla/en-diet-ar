@extends('layouts.public')
@section('subpageMenu','topmenu--subpage')
@section('pageTitle','Mikrobiyota Testi ile Kendini Keşfet | Enbiosis')
@section('pageDescription',' BEDENİNDE HAYAT BULAN SAVAŞÇILARI KEŞFETMENİN ZAMANI GELDİ!')
@section('metaTag')
	<meta name="robots" content="noindex">
@endsection
{{-- @php
	$total = 0;
	$sale = '';
	$salePrice = '';
	$kitAmount = 0;
	$productName = '';
	$couponCode = '';
	$refCode = '';
	$price = $order->price;
	foreach($order->orderDetail as $detail) {

		$total += $detail->price * $detail->quantity;
		$kitAmount += $detail->quantity;
		$productName = $detail->name . ' X ' . $detail->quantity;
	}
	if($order->coupon) {
		$couponCode = $order->coupon->code;
		$sale = $order->coupon->discount . '%';
		$salePrice = $order->price . ' $';
	}
	if($order->dietitian_code)
		$refCode = $order->dietitian_code;
	$temp = explode(' ', $order->name);
	$userName = $temp[0];
	unset($temp[0]);
	$userSurName = implode(' ', $temp);
@endphp
@if ( session('payment-done') )
	@section('gtag-s')
		<!-- Event snippet for Purchase conversion page -->
		<script>
			pushObjectToDataLayer({
				'Category': 'Purchase',
                'Action': 'Ecommerce',
                'event': 'gaEvent',
				'Label': {{$price}},
				'Value': {{$price}},
				'productName': '{{$productName}}',
				'refCode': '{{$refCode}}',
				'couponCode': '{{$couponCode}}',
				'Price' : '{{ $total }}',
				'salePrice' : '{{ $salePrice }}',
				'discount' : '{{ $sale }}',
				'UserName': '{{ $userName }}',
				'UserSurName': '{{ $userSurName }}',
				'email': '{{ $order->email }}',
				'OrderID': '{{ $order->tracking  }}',
				'installment': {{ $order->payment->taksit }},
				'amount': {{ $kitAmount }}
			});
			// gtag('event', 'conversion', {
			// 	'send_to': 'AW-658369982/YnaZCNbr5c0BEL7b97kC',
			// 	'Price': {{ $total }},
			// 	'Sale price' : {{ $salePrice }},
			// 	'discount' : '{{ $sale }}',
			// 	'currency': 'TRY',
			// 	'transaction_id': '{{ $order->tracking  }}'
			// });
		</script>
	@endsection 
@endif
--}}

@section('content')
@php
	$total = 0;
@endphp

<div class="container" style="direction: rtl">
	<div class="row">
		<div class="col-md-12 tesekkurler">
			<img src="{{url('/')}}/new/img/tick.webp" width="200" style="margin-bottom:40px;"/>
			<h1 class="tesekkurler__title">شكرا لطلبك</h1>
			<p>شكرا لاختيارك لنا ، وسوف نتصل بك بمجرد استلام طلبك.</p>
			<table class="table borderless">
				<thead>
					<tr>
						<th class="text-right"><b>اسم المنتج</b></th>
						<th class="text-right"><b>قطعة</b></th>
						<th class="text-left"><b>المجموع</b></th>
					</tr>
				</thead>
				<tbody>
					@foreach ( $order->orderDetail as $details )
						<tr>
							<td class="text-right">{{ __('pages/checkout.'.$details->product->slug) }}</td>
							<td class="text-right">{{ $details->quantity }}</td>
							<td class="text-left"> {{ $details->quantity * $details->price }} $</td>
							<?php $total += $details->quantity * $details->price ?>
						</tr>
					@endforeach
					
				</tbody>
				<tfooter>
					<tr>
						<td colspan="3" class="text-left"><b>المجموع الفرعي :</b> {{ $total }} $</td>
					</tr>
					{{-- @if ( $order->payment )
						@if ( $order->payment->taksit > 1 )
							<tr>
								<td><b>{{ $order->payment->taksit  }} Taksit</td>
								@php
									foreach ( config('instalments') as $key => $item ){
										if ( $order->payment->taksit ==  $item['taksit'] ) {
											$added  = round ( (double) ( ( $total * $item['rate'] ) / 100 ), 2) ;
											$total = round( $total +  $added , 2);
										}
									}
								@endphp
								<td colspan="2" class="text-right"><b></b> + {{ $added }} $</td>
							</tr>
							<tr>
								<td></td>
								<td colspan="2" class="text-right"><b>المجموع الفرعي:</b> {{ $total }} $</td>
							</tr>
						@endif
					@endif --}}
					@if ($order->coupon)
						<tr>
							@if ($order->coupon->campaign)
							<td><b>{{$order->coupon->code}} Kampanyasına katılarak %{{$order->coupon->discount}} tasarruf ettiniz</td>
							@else
							
								<td  class="text-right"><b>لقد وفرت {{$order->coupon->discount}} % باستخدام قسيمة تخفيض</td>
							@endif
							<td colspan="2" class="text-left">- {{ round ( ( $order->coupon->discount * $total ) / 100, 2)}} $</td>
						</tr>
						<tr>
							<td></td>
							<td colspan="2" class="text-left"><b>المجموع الفرعي:</b> {{ round ( (double) ( ( ( 100 - $order->coupon->discount ) * $total ) / 100 ), 2)}} $</td>
						</tr>
					@endif 
					<tr>
						<td colspan="2" class="text-right"><b>العنوان :</b> {{ $order->address }} {{$order->city ? ', ' . $order->city->city : ''}} {{$order->county ? '/ ' . $order->county->county : ''}} - {{$order->city ? config('cities.' . $order->city->country_code . '.name') : ''}}</td>
						<td><b>المجموع الكلي :</b>  {{ $order->price }} $</td>
					</tr>
				</tfooter>
			</table>
		</div>
	</div>
</div>

@endsection