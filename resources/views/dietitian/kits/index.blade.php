@extends('layouts.dietitian')

@section('content')
<div class="mx-5">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-dark text-light">Team Member</div>

                <div class="card-body ">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    You are logged in as {{auth()->user()->kind}} you are in  show member section!
                </div>
            </div>
            @include('include.messages')
            @include('include.form-errors')
        </div>
    </div>
</div>
<div class="mx-5 mt-3">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="text-left">
                <h3>
                    Kits
                </h3>
            </div>
            @if ( $dietitians->count() > 0  )
                <table class="table table-responsive-lg table-striped">
                    <thead class="thead-light">
                    <tr>
                        <th>#</th>
                        <th>Kit Code</th>
                        <th>referance</th>
                        <th>Created</th>
                        <th>process</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($dietitians as $dietitian)
                            @foreach ($dietitian->kits as $kit)
                                <tr>
                                    <td>{{$kit->id}}</td>
                                    <td>{{$kit->kit_code}}</td>
                                    <td>{{$dietitian->strRole}}. {{ $dietitian->name }} - {{ $dietitian->reference_code }}</td>
                                    <td>{{$kit->created_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}</td>   
                                    <td>
                                        <a href="{{ route('dietitian.team.show.kit',$kit->kit_code)}}" class="btn btn-outline-info mr-2 mb-1" >Show kit Details</a>
                                    </td>
                                </tr>
                            @endforeach
                        @endforeach
                    </tbody>
                </table>
            @else
                <div class="text-center text-danger">
                    <h4>
                        DONT HAVE ANY kit
                    </h4> 
                </div>
            @endif
        </div>
    </div>
</div>
@endsection