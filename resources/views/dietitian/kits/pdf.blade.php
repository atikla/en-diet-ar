@extends('layouts.dietitian')
@section('content')
    <div class="mt-3 mx-5">
        <div class="row justify-content-center text-center">
            <div class="col-12">
                <h3>
                   Kit Code: {{ $kit->kit_code }}
                </h3>
            </div>
            <div class="col-lg-6 col-12">
                <button type="button" class="btn btn-outline-info mr-2 mb-1" data-toggle="modal" data-target="#foods">
                    Foods Report
                </button><br>
                <a href="{{$food}}" class="btn btn-outline-info mr-2 mb-1" download="{{__('food.file_name')}}_kit_{{$kit->kit_code}}.pdf">if report show not work download this</a>
            </div>
            <div class="col-lg-6 col-12">
                <button type="button" class="btn btn-outline-primary mr-2 mb-1" data-toggle="modal" data-target="#microbiome">
                    Microbiome Report
                </button><br>
                <a href="{{$microbiome}}" class="btn btn-outline-info mr-2 mb-1" download="{{__('mikrobyom.file_name')}}_kit_{{$kit->kit_code}}.pdf">if report show not work download this</a>
            </div>
        </div>
    </div>
    
    <div class="modal fade bd-example-modal-lg" id="foods" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document" style="max-width: 85%">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Foods Report</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" style="height:85vh">
                        <div class="col-lg-12" >
                        <iframe id="food-iframe"src="" width="100%" style="height:95%"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bd-example-modal-lg" id="microbiome" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document" style="max-width: 85%">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Microbiome Report</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row" style="height:85vh">
                        <div class="col-lg-12" >
                            <iframe id="micro-iframe" src="" width="100%" style="height:95%"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
    $(document).ready(function(){

        convertBase64ToBlob = (base64String) => {
            const parts = base64String.split(';base64,');
            const mimeType = 'application/pdf'
            const decodedData = window.atob(parts[1]);
            const uInt8Array = new Uint8Array(decodedData.length);

            for(let i = 0; i < decodedData.length; i++){
                uInt8Array[i] = decodedData.charCodeAt(i);
            }

            return new Blob([uInt8Array], {type: mimeType});
        }
        let food = URL.createObjectURL(convertBase64ToBlob('{{$food}}'));
        let microbiome = URL.createObjectURL(convertBase64ToBlob('{{$microbiome}}'));
        $('#food-iframe').attr('src',food);
        $('#micro-iframe').attr('src',microbiome);
    });
    </script>
@endsection