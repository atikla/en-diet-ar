@extends('layouts.dietitian')

@section('content')

<div class="row justify-content-center px-4">
    <div class="col-12">
        <div class="card">
            <div class="card-header">Dashboard</div>

            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                You are logged in!
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card my-3 ">
            <div class="card-header bg-info text-light">Team Informations</div>
            <div class="card-body ">
                @if (auth()->user()->sellingTeam)
                    <p> team leadar: {{auth()->user()->isLeader ? 'Yes' : 'No'}} </p>
                    <p> team name: {{auth()->user()->sellingTeam->name}} </p>
                    <p> team Members conut: {{auth()->user()->sellingTeam->dietitians()->count()}} </p>
                    <a href="{{route('admin.selling-teams.show', auth()->user()->sellingTeam->id)}}" class="btn btn-outline-info mr-2 mb-1" >Show</a>
                @else
                <div class="text-center text-danger">
                    <h4>
                        DONT BELONGS TO ANY TEAM
                    </h4> 
                </div>
                @endif
                <p></p>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card my-3">
            <div class="card-header bg-info text-light">Your Kits</div>
            <div class="card-body">
                @if (auth()->user()->kits->count() > 0)
                    <table class="table table-responsive-lg table-striped">
                        <thead class="thead-light">
                        <tr>
                            <th>#</th>
                            <th>Kit Code</th>
                            <th>process</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach (auth()->user()->kits as $kit)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$kit->kit_code}}</td>
                                <td>
                                    <a href="{{route('dietitian.my-kit', $kit->kit_code)}}" class="btn btn-outline-info mr-2 mb-1" >Show kit Details</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                @else
                    <div class="text-center text-danger">
                        <h4>
                            DONT HAVE ANY KIT
                        </h4> 
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card my-3">
            <div class="card-header bg-info text-light">Your Orders</div>
            <div class="card-body">
                @if (auth()->user()->orders->count() > 0)
                    <table class="table table-responsive-lg table-striped">
                        <thead class="thead-light">
                        <tr>
                        <th>#</th>
                        <th>Traking Number</th>
                        <th>name</th>
                        <th>phone</th>
                        <th>dietitian_code</th>
                        <th>Created</th>
                        <th>process</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach (auth()->user()->orders as $order)
                            <tr>
                                <td>{{$order->id}}</td>
                                <td>{{$order->tracking}}</td>
                                <td>{{$order->name}}</td>
                                <td>{{$order->phone}}</td>
                                <td>{{$order->dietitian_code ?? 'not Entered'}}</td>
                                <td>{{$order->created_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}</td>   
                                <td>
                                    <a href="{{route('dietitian.my-order', $order->tracking)}}" class="btn btn-outline-info mr-2 mb-1" >Show Order Details</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                @else
                    <div class="text-center text-danger">
                        <h4>
                            DONT HAVE ANY ORDER
                        </h4> 
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
