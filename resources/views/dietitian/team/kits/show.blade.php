@extends('layouts.dietitian')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-dark text-light">Kits</div>

                    <div class="card-body ">
                        You are logged in as Admin You are in Kit Details Section!
                    </div>
                </div>
                @include('include.messages')
                @include('include.form-errors')
            </div>
        </div>
    </div>
    <div class="mt-3 mx-5">
        <h3>Kit Code: {{ $kit->kit_code }}</h3>
        @if ( $kit->show )
            <p>User <span class="text-success"> CAN </span> Show This Kit Result</p>
        @else
            <p>User <span class="text-danger"> Can NOT</span> Show This Kit Result</p>
        @endif
        <div class="row">
            <div class="col-lg-6 col-12 mt-3 mb-3">
                <div class="card">
                    <div class="card-header bg-info text-light">User Informations</div>
                    <div class="card-body ">
                        @if ($kit->user)
                            <div class="text-right">
                                <a href="{{route('admin.users.show', $kit->user->id)}}" class="btn btn-outline-info mr-2 mb-1" >Show Profile</a>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-12 text-left"><h4><b> Name   : </b> {{$kit->user->name}}</h4></div>
                                <div class="col-lg-6 col-md-12 text-left"><h4><b> Email  : </b> {{$kit->user->email}}</h4></div>
                                <div class="col-lg-6 col-md-12 text-left"><h4><b> address   : </b> {{$kit->user->address}}</h4></div>
                                <div class="col-lg-6 col-md-12 text-left"><h4><b> phone  : </b> {{$kit->user->phone}}</h4></div>
                                <div class="col-lg-6 col-md-12 text-left"><h4><b> UserGender  : </b> {{$kit->user->UserGender}}</h4></div>
                                <div class="col-lg-6 col-md-12 text-left"><h4><b> vegetarian  : </b> {{$kit->user->vegetarian == 0 ? 'NO' : 'YES'}}</h4></div>
                                <div class="col-12 text-center">
                                </div>
                            </div>
                        @else 
                        <div class="text-center text-danger">
                            <h3>
                                Dont Have a User Yet!
                            </h3>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-12 mt-3 mb-3">
                <div class="card">
                    <div class="card-header bg-secondary text-light">dietitian Informations</div>
                    <div class="card-body ">
                        @if ($kit->dietitian)
                            <div class="text-right">
                                <a href="{{route('admin.dietitians.show', $kit->dietitian->id)}}" class="btn btn-outline-info mr-2 mb-1" >Show Profile</a>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-12 text-left"><h4><b> Name   : </b> {{$kit->dietitian->name}}</h4></div>
                                <div class="col-lg-6 col-md-12 text-left"><h4><b> Email  : </b> {{$kit->dietitian->email}}</h4></div>
                                <div class="col-lg-6 col-md-12 text-left"><h4><b> address   : </b> {{$kit->dietitian->address}}</h4></div>
                                <div class="col-lg-6 col-md-12 text-left"><h4><b> phone  : </b> {{$kit->dietitian->phone}}</h4></div>
                                <div class="col-12 text-center">
                                </div>
                            </div>
                        @else
                        <div class="text-center text-danger">
                            <h3>
                                Dont Have a Dietitian
                            </h3>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            @if ($kit->food && $kit->microbiome)
                <div class="col-6 text-center">
                    <a href="{{route('dietitian.team.generate.kit', [$kit->kit_code, 'lang' => 'tr'])}}" class="btn btn-outline-secondary btn-block mb-2">
                        Go To <span class="flag-icon flag-icon-tr"> </span> Reports
                    </a>
                </div>
                <div class="col-6 text-center">
                    <a href="{{route('dietitian.team.generate.kit', [$kit->kit_code, 'lang' => 'en'])}}" class="btn btn-outline-secondary btn-block mb-2">
                        Go To <span class="flag-icon flag-icon-gb"> </span> Reports
                    </a>
                </div>
            @else 
                <div class="col-12 text-center">
                    <h3 class="text-danger">
                        THE KIT RESULTS NOT UPLOADED YET!
                    </h3>
                </div>
            @endif
        </div>
    </div>
@endsection
@section('script')
@endsection
