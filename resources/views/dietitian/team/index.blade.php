@extends('layouts.dietitian')

@section('content')
<div class="mx-5">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-dark text-light">Selling Teams</div>

                <div class="card-body ">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    You are logged in as {{auth()->user()->kind}} you are in team show section!
                </div>
            </div>
            @include('include.messages')
            @include('include.form-errors')
        </div>
    </div>
</div>
<div class="mt-2 mx-5">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="row">
                <div class="col-md-4">
                    <div class="card my-3">
                        <div class="card-header bg-dark text-light">Team leader</div>
                        <div class="card-body ">
                            <ul>
                                <li>name: {{$team->leader->name}}</li>
                                <li>email: {{$team->leader->email}}</li>
                                <li>phone: {{$team->leader->phone}}</li>
                                <li>address: {{$team->leader->address}}</li>
                                <li>reference code: {{$team->leader->reference_code}}</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card my-3">
                        <div class="card-header bg-info text-light">Selling Team info</div>
                        <div class="card-body ">
                            <ul>
                                <li>team name: {{$team->name}}</li>
                                <li>team note: {{$team->note ? $team->note : 'Not Entered'}}</li>
                                <li>team member conut: {{$team->dietitians()->count()}}</li>
                                <li>team citeis conut: {{$team->cities()->count()}}</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card my-3">
                        <div class="card-header bg-success text-light">Cities</div>
                        <div class="card-body text-center" style="max-height: 171px; overflow: auto;">
                            <table class="table table-striped">
                                <thead class="thead-light">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ($team->cities as $city)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td with=50>{{$city->city}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        
        <div class="col-12">
            <h4>Members</h4>
            <table class="table table-striped">
                <thead class="thead-light">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Reference Code</th>
                    <th scope="col">Type</th>
                    <th scope="col">Phone</th>
                    <th scope="col">process</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($team->dietitians as $member)
                        <tr {{ $team->leader->id ==  $member->id ? 'class="bg-success"' : ''}}>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$member->name}}</td>
                            <td>{{$member->reference_code}}</td>
                            <td>{{$member->kind}}</td>
                            <td>{{$member->phone}}</td>
                            <td>
                                <a href="{{route('dietitian.team.member', [$member->reference_code])}}" class="btn btn-outline-info mr-2 mb-1" >Show Profile</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
