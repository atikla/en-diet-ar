@extends('layouts.dietitian')

@section('content')
<div class="mx-5">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-dark text-light">Team Member</div>

                <div class="card-body ">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    You are logged in as {{auth()->user()->kind}} you are in  show member section!
                </div>
            </div>
            @include('include.messages')
            @include('include.form-errors')
        </div>
    </div>
</div>
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 text-center">
                <img class="w-75" style="border-radius: 50%;"src="{{$member->photo ? url('/') . '/img/private/' . $member->photo->path : url('/') . '/img/private/default.png'}}" alt="">
            </div>
            <div class="col-lg-9 col-md-6 col-sm-6 col-xs-12 pt-lg-5">
                <div class="card my-3 ">
                    <div class="card-header bg-info text-light">member Informations</div>
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left"><h4><b> Name   : </b> {{$member->name}}</h4></div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left"><h4><b> Type   : </b> {{$member->kind}}</h4></div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left"><h4><b> Role   : </b> {{$member->StrTeam}}</h4></div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left"><h4><b> Email  : </b> {{$member->email}}</h4></div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left"><h4><b> status  : </b> {!! $member->email_verified_at == NULL ? '<span class="text-danger">Email Not Verified</span>' : '<span class="text-info">Email Verified</span>' !!}</h4></div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left"><h4><b> reference code  : </b> {{$member->reference_code}}</h4></div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left"><h4><b> City  : </b> {{$member->city ? $member->city->city : '---'}}</h4></div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left"><h4><b> Address  : </b> {{$member->address}}</h4></div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left"><h4><b>created_at : </b> {{$member->created_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}</h4></div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left"><h4><b>updated_at : </b> {{$member->updated_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}</h4></div>
                            <div class="col-lg-12 col-md-6 col-sm-12 col-xs-12 text-center">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card my-3">
                    <div class="card-header bg-info text-light">Orders</div>
                    <div class="card-body">
                        @if ($member->orders->count() > 0)
                            <table class="table table-responsive-lg table-striped">
                                <thead class="thead-light">
                                <tr>
                                <th>#</th>
                                <th>Traking Number</th>
                                <th>name</th>
                                <th>phone</th>
                                <th>address</th>
                                <th>member_code</th>
                                <th>Created</th>
                                <th>Updated</th>
                                <th>process</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ($member->orders as $order)
                                    <tr>
                                        <td>{{$order->id}}</td>
                                        <td>{{$order->tracking}}</td>
                                        <td>{{$order->name}}</td>
                                        <td>{{$order->phone}}</td>
                                        <td>{{$order->address}} - {{$order->city ? $order->city->city : ''}} - {{ $order->county ? $order->county->county : '' }}</td>
                                        <td>{{$order->member_code ?? 'not Entered'}}</td>
                                        <td>{{$order->updated_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}</td>
                                        <td>{{$order->created_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}</td>   
                                        <td>
                                            <a href="{{route('dietitian.team.show.order', $order->tracking)}}" class="btn btn-outline-info mr-2 mb-1" >Show Order Details</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="text-center text-danger">
                                <h4>
                                    DONT HAVE ANY ORDER
                                </h4> 
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection