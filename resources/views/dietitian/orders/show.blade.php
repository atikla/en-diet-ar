@extends('layouts.dietitian')

@section('content')
<div class="mx-5">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-dark text-light">Team Member</div>

                <div class="card-body ">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    You are logged in as {{auth()->user()->kind}} you are in  show Order section!
                </div>
            </div>
            @include('include.messages')
            @include('include.form-errors')
        </div>
    </div>
</div>
<div class="mt-3 mx-5">
    <div class="row justify-content-center">
        <div class="col-lg-3 col-md-6 col-12 mt-3 mx-0">
            <div class="card">
                <div class="card-header bg-info text-light">Costumer info</div>

                <div class="card-body ">
                    <p>Costumer IP :{{$order->payment->clientIp}}</p>
                    <p>Placed Order At :  {{$order->updated_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}</p>
                    <p>costumer name :  {{$order->name}}</p>
                    <p>costumer Email :  {{$order->email}}</p>
                    <p>costumer Phone :  {{$order->phone}}</p>
                    @if ( $order->dietitian_code )
                        <p>costumer Reference :  {{ $order->dietitian_code}}</p>
                        @if ( $order->dietitian )
                            <p> Reference name: {{ $order->dietitian->name }} ( {{ $order->dietitian->email }} ) </p>
                            <a href="{{route('dietitian.team.member', $order->dietitian->reference_code)}}" class="btn btn-outline-info mr-2 mb-1" >Show Reference Profile</a>
                        @else
                           <span class="text-danger"> Reference code not match any one in our records </span>

                        @endif
                    @else
                        <p>costumer Reference : <span class="text-danger" > not Entered </span> </p>
                    @endif                    
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-12 mt-3">
            <div class="card">
                <div class="card-header bg-primary text-light">Addresses</div>

                <div class="card-body ">
                    <u><h6>shipping address</h6></u>
                    <p>City :  {{$order->city ? $order->city->city : '***'}}</p>
                    <p>County :  {{$order->county ? $order->county->county : '***'}}</p>
                    <p>Address :  {{$order->address}}</p>
                   
                    <u><h6>Billing Address</h6></u>
                    @if ($order->fcity_id)
                    <p>City :  {{$order->biCity ? $order->biCity->city : '***'}}</p>
                    <p>County :  {{$order->biCounty ? $order->biCounty->county : '***'}}</p>
                    <p>Address :  {{$order->faddress}}</p>
                    @else 
                    <p class="text-danger">NO</p>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-12 mt-3">
            <div class="card">
                <div class="card-header bg-warning  text-dark">Payment Info</div>

                <div class="card-body ">
                    <p>Payment Amount :  {{$order->payment->amount}}</p>
                    <p>Installmens :  {{ $order->payment->taksit ? $order->payment->taksit . ' Taksit' : 'Tek Çekim' }}</p>
                    <p>maskedCreditCard:  {{$order->payment->maskedCreditCard}}</p>
                    <p>name on the card:  {{$order->payment->EXTRA_CARDHOLDERNAME}}</p>
                    <p>Bank:  {{$order->payment->EXTRA_CARDISSUER}}</p>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-12 mt-3">
            <div class="card">
                <div class="card-header bg-secondary text-light">Coupon Usage</div>

                <div class="card-body ">
                    @if ($order->coupon)
                        <p>Coupon Code:  {{$order->coupon->code}}</p>
                        <p>Coupon discount:  {{$order->coupon->discount}} %</p>
                        <p>Coupon Usage date and time:  {{$order->coupon->updated_at}}</p>
                        <p>Coupon note :  {{$order->coupon->note}}</p>
                        <p>Coupon reference :  {!! $order->coupon->reference ? $order->coupon->reference . ' - ' .$order->coupon->dietitian->reference_code . '<a  href="' . route('admin.dietitians.show', $order->coupon->dietitian->id). '" class="btn btn-outline-success ml-2">show profile </a>'  : '<sapn class="text-danger">no reference</span>' !!}</p>
                    @endif
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection