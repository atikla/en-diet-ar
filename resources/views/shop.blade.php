@extends('layouts.public')
@section('styles')
<style>
.border-top { border-top: 1px solid #e5e5e5; }
.border-bottom { border-bottom: 1px solid #e5e5e5; }
.border-top-gray { border-top-color: #adb5bd; }
.box-shadow { box-shadow: 0 .25rem .75rem rgba(0, 0, 0, .05); }
.lh-condensed { line-height: 1.25; }
.text-gray {color: #aaa;}
</style>
@endsection
@section('content')
<section class="header subpage " ></section>
<section class="hw">
    <div class="container">
        <div class="hw-title">
        <h3>{{__('app.shop')}} </h3>

            <h1>
                {{__('app.EnBiosis_Products')}} 
            </h1>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <!-- row-->
        <div class="row">
            <div class="col-lg-12">
            <!-- List group-->
            <ul class="list-group shadow">
                <!-- list group item-->
                @foreach ($products as $product)
                    <li class="list-group-item">
                        <!-- Custom content-->
                        <div class="media align-items-lg-center flex-column flex-lg-row p-3">
                            <div class="media-body order-2 order-lg-1 mt-3">
                            <h5 class="mt-0 font-weight-bold mb-2">{{$product->name}}</h5>
                            <p class="font-italic text-muted mb-0 small">{!! \Illuminate\Support\Str::words($product->description, 10, $end=' <a href="'. route('product', $product->slug ) . '"> ' . __('app.header_6')  . '</a>')!!}</p>
                            <div class="d-flex align-items-center justify-content-between mt-1">
                                <h6>{{__('app.price')}} :</h6>
                                <h6 class="font-weight-bold ml-1">
                                    @if ($product->discount_price)
                                        <strike class="text-danger">
                                            {{$product->price}} TL
                                        </strike>
                                        <span class="ml-2 text-success">{{(double) ( ( ( 100 - $product->discount_price ) * $product->price ) / 100 )}} TL</span>
                                    @else
                                        {{$product->price}} TL
                                    @endif
                                </h6>
                                <ul class="list-inline small">
                                <li class="list-inline-item m-0" style="font-size:1rem;"><a style="color:#ea502c" href="{{route('add.cart', $product->id)}}"> {{ __('app.sepete_ekle') }} <i style="color:#ea502c" class="mx-2 fas fa-cart-plus"></i> </a></li>
                                </ul>
                            </div>
                            </div><img src="{{$product->photo ? url('/') . '/img/private/' . $product->photo->path : url('/') . '/img/private/default.png'}}" alt="Generic placeholder image" width="200" class="ml-lg-5 order-1 order-lg-2">
                        </div>
                        <!-- End -->
                    </li>
                @endforeach
                <!-- End -->
              </ul>
              <!-- End -->
            </div> 
            <div class="col-lg-8 mx-auto  my-5">
                {{$products->links()}}
            </div>    
        </div>
    </div>
</section>
<div class="mt-5 pt-5">
{{-- {{print_r($products)}} --}}
</div>
@endsection