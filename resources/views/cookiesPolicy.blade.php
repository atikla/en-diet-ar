@extends('layouts.public')
@section('subpageMenu','topmenu--subpage')
@section('pageTitle', __('pages/checkout.page_title'))
@section('pageDescription', __('pages/checkout.page_description'))
@section('metaTag')
<meta name="robots" content="noindex">
@endsection
@section('styles')
    <style>
        .d {list-style-type: lower-alpha;}
    </style>
@endsection
@section('content')

<section class="subpage-header subpage-header--contact">
	<div class="container">
		<div class="row">
			<div class="subpage-header__content">
				<h1 class="subpage-header__title">ÇEREZ İLKELERİ</h1>
				<div class="subpage-header__seperator"></div>
			</div>
		</div>
	</div>
</section>
<section class="bilim-content">
    <section class="bilim-content">

        <div class="container">
            <div class="text">
                {{-- <h4>
                    GİZLİLİK İLKELERİ
                </h4>
                <p>
                    İşbu Gizlilik İlkeleri, üyelik sözleşmesinin tamamlayıcısı ve ayrılmaz bir parçasıdır. www.enbiosis.com internet sitesinde yer alan internet tarayıcıları ve aktif internet bağlantısı üzerinden çalıştırılabilen Mikrobiyom Analizi ve yapay zekâ temelli kişiselleştirilmiş beslenme rehberi hizmetinin (“ENBİOSİS”) sağlayıcısı ENBİOSİS Biyoteknoloji  A.Ş. (ENBİOSİS). (“Sağlayıcı”) olarak, ENBİOSİS’i kullanan kullanıcıların (“Kullanıcı”) gizliliğini korumak temel hedeflerimizden biridir. Bu amaçla, ENBİOSİS Gizlilik İlkeleri (“Gizlilik İlkeleri”), Kullanıcının kişisel verilerinin 6698 Sayılı Kişisel Verilerin Korunması Kanunu (“KVKK”) ile yürürlükteki diğer ilgili mevzuat ile uyumlu bir şekilde işlenmesi ve Kullanıcının veri işleme konusunda bilgilendirmesi için hazırlanmıştır. Ayrıca, ENBİOSİS Çerez İlkeleri de (“Çerez İlkeleri”) işbu Gizlilik İlkeleri’nin tamamlayıcısı ve ayrılmaz bir parçasıdır. ENBİOSİS, işbu Gizlilik İlkeleri’ni KVKK ve yürürlükteki diğer mevzuatta yapılabilecek değişiklikler çerçevesinde her zaman güncelleme hakkını saklı tutar. Mevzuatta değişikliklerin olması halinde işbu Gizlilik İlkeleri’ni yeni mevzuata uygun olarak güncelleyeceğimizi, yapılan güncellemeleri de ENBİOSİS üzerinden her zaman kolaylıkla takip edebileceğinizi size bildirmek isteriz. Bu kapsamda, ENBİOSİS ’i sürekli kullanımınız Gizlilik İlkeleri’ndeki değişikliklere onay verdiğinizi göstermektedir. Bu Gizlilik İlkeleri sadece ENBİOSİS için geçerlidir ve üçüncü taraflara ait internet siteleri kapsam dışındadır. Sizi ilgilendirebileceğini düşündüğümüz diğer internet sitelerine ait bağlantılar (link) verebiliriz. Ancak, internetin doğası gereği, bu internet sitelerinin gizlilik standartlarını garanti edememekteyiz. ENBİOSİS dışındaki platformların içeriğinden dolayı sorumluluk kabul edememekteyiz. Diğer web sitelerine ait linklere bastığınızda, söz konusu web sitesinin gizlilik beyanlarını okumanızı öneririz.
                </p>
                <ol>
                    <li>
                        <strong>Kişisel Verilerin İşleme Amaçları</strong>
                        <p class="pl-3">
                            Kişisel verilerinizi, temel olarak bilimsel yöntemler kullanarak mikrobiyom profilinizi çıkarmak ve bu sayede kendi profiliniz için en uygun kişiselleştirilmiş beslenme rehberini diyetisyenler aracılığıyla tespit etmek için işleriz. Bir başka deyişle ENBİOSİS uygulamasını kullanarak kişiye özgü beslenme rehberinizi çıkarabilmemiz için aşağıda detaylarını listelediğimiz kişisel verilerinizi işlememiz gerekmektedir. Kişisel verilerin işlenmesinden anlaşılması gereken bu verilerin toplanması, saklanması, kullanılması, istatistiki analizlerinin yapılması ve aktarılması gibi size ait verilerin otomatik yahut otomatik olmayan yollarla işlenmesidir. Yine kişisel verilerinizin ENBİOSİS uygulamasını kullanmaya devam etmeniz hâlinde beslenme programınızın periyodik olarak takip edilmesi ve mikrobiyotanın sağlıklı hâle getirilmesi için de işlenmesi gerekmektedir. Öte yandan; ENBİOSİS kişiselleştirilmiş beslenme rehberine ek olarak, gelecekte kişiye özel tedavi yöntemlerinin geliştirilmesi üzerine de bilimsel hazırlık çalışmaları yapmaktadır. Bu kapsamda; kişisel verileriniz bu tür bilimsel çalışmalarda da anonim hale getirilerek kullanılabilir. Bu verilerin kapsamını sağlamış olduğunuz sağlık ve yaşam tarzı beyanları, göndermiş olduğunuz numuneden elde edilen bağırsak mikroorganizmalarının (bakteri, virus, mantar) ve mikrobiyomun analizine yönelik metagenomik, transkriptomik, proteomik veya metabolomik gibi farkliomikeknolojileri,  biyoinformatik analizler ve bu numunelerden izole edilerek elde edilen her türlü DNA, RNA, protein ve metabolitlere ait moleküler biyoloji ve biyoinformatik analiz sonuçları oluşturmaktadır. Bu biyolojik materyeller içerisinde mikroorganizma kökenli olanların yanında, dışkı içerisindeki besin artığı kaynaklı olabilecek ve konakçı olarak sizden bulaşmış olabilecek genomik ve matebolomik komponentler de yer almaktadır. Bununla birlikte; ENBİOSİS halihazırda kişiye özel tedavi hizmeti vermemektedir. Miktobiyom analizi ve Kişiselleştirilmiş Beslenme Rehberinin oluşturabilmesi için tarafımıza iletilen dışkı (gaita) örneklerinin laboratuvar ortamında incelenmesi gerekmektedir. Bu inceleme, sırasıyla gaita örneklerinden total DNA’nın izole edilmesi, bu DNA içerisinden sadece bir grup mikroorganizmada (bakteriler ve arkealar) bulunan 16S rRNA geninin polimeraz zincir reaksiyonu işlemiyle amplifiye edilmesi, bu genlerin yeni nesil DNA dizileme yöntemleriyle  dizilenmesi ve dijital veri haline getirilmesi, bu dijital verinin biyoinformatik analizle sizin örneğinizdeki bakteri cinsleri ve bağıl miktarlarını ortaya koyacak şekilde işlenmesi, biyoinformatik analiz sonucunun sizin beyanlarınız ile birlikte ENBİOSİS’e ait analiz algoritması ile işlenerek kişisel beslenme önerileri çıktılarının alınması aşamalarından oluşmaktadır. Sizden alınan dışkı örnekleri ENBİOSİS tarafından size özel bir ID numarası verilerek anomin hale getirildikten sonra işleme alınır. Laboratuvar ortamına örnekleriniz açık kimlik bilgileriniz verilmeden (size özel bir ID numarası verilerek) aktarılır. Yine laboratuvar sonuçları, anonim olarak kişiselleştirilmiş beslenme rehberinin oluşturulabilmesi için diyetisyenlerle ID numaranız ile paylaşılır. ENBİOSİS, halihazırda Erciyes Üniversitesi Genom ve Kök Hücre Merkezi ile işbirliği yapmakta olup, ileride yurt içinde yahut yurt dışında başkaca laboratuvarla işbirliği yapabilir. Yukarıdaki ek olarak; ENBİOSİS tarafından elde edilen her türlü kişisel veriniz (özel nitelikli kişisel veriler de dahil fakat bunlarla sınırlı olmamak kaydıyla) aşağıdaki amaçlar ile işlenebilecektir:
                        </p>
                        <ul class="pl-5">
                            <li>Kimliğinizi teyit etme,</li>
                            <li>Sağlık hizmetleri ile finansmanının planlanması ve yönetimi,</li>
                            <li>İlaç temini,</li>
                            <li>Risk yönetimi ve kalite geliştirme aktivitelerinin yerine getirilmesi,</li>
                            <li>Sağlık hizmetlerini geliştirme amacıyla analiz yapma,</li>
                            <li>Mali İşler, Pazarlama bölümleri tarafından sağlık hizmetlerinizin finansmanı, tetkik ve tedavi giderlerinizin karşılanması, müstehaklık sorgusu kapsamında özel sigorta şirketler ile talep edilen bilgilerin paylaşılması,</li>
                            <li>Araştırma yapılması,</li>
                            <li>Yasal ve düzenleyici gereksinimlerin yerine getirilmesi,</li>
                            <li>Sağlık hizmetlerinin finansmanı kapsamında özel sigorta şirketler ile talep edilen bilgileri paylaşma,</li>
                            <li>Kalite, Bilgi Sistemleri bölümleri tarafından risk yönetimi ve kalite geliştirme aktivitelerinin yerine getirilmesi,</li>
                            <li>Mali İşler, Pazarlama bölümleri tarafından hizmetlerimiz karşılığında faturalandırma yapılması ve anlaşmalı olan kurumlarla ilişkinizin teyit edilmesi,</li>
                            <li>Pazarlama, Medya ve İletişim, Çağrı Merkezi bölümleri tarafından kampanyalara katılım ve kampanya bilgisi verilmesi, Web ve mobil kanallarda özel içeriklerin, somut ve soyut faydaların tasarlanması ve iletilebilmesi.</li>
                        </ul> <br>
                        <p>
                            İlgili mevzuat uyarınca elde edilen ve işlenen Kişisel Verileriniz, ENBİOSİS ‘e ait fiziki arşivler ve/veya bilişim sistemlerine nakledilerek, hem dijital ortamda hem de fiziki ortamda muhafaza altında tutulabilecektir.
                        </p>
                    </li>
                    <li>
                        <strong>Kişisel Verilen Elde Edilmesi</strong>
                        <p class="pl-3">
                            Özel nitelikli kişisel sağlık verileriniz kurye ile tarafınıza gönderdiğimiz kit ile bağırsak mikrobiyomunuzu içeren dışkı örneğinizin kendi tarafınızdan alınarak yine kurye ile bize iletmeniz yoluyla elde edilir. Tarafınıza ilettiğimiz kitte dışkı örneğinizin nasıl alınacağı, paketleneceği ve tarafımıza gönderileceği adım adım anlatılmaktadır. Öte yandan yine hassas nitelikli sağlık verileriniz; kit ile birlikte tarafınıza gönderdiğimiz anketin doldurulması yoluyla da yazılı olarak elde edilir. Bunlara ek olarak; mobil uygulamamız, internet sitemiz, telefon numaralarımız gibi sesli, yazılı, görsel yollarla da kişisel verileriniz tarafımızca toplanmaktadır. 
                        </p>
                    </li>
                    <li>
                        <strong>Topladığımız Kişisel Veriler</strong>
                        <p class="pl-3">
                            Sağlık verileriniz başta olmak üzere özel nitelikli kişisel verileriniz ve genel nitelikli kişisel verileriniz, ENBİOSİS tarafından aşağıda yer alanlar dâhil ve bunlarla sınırlı olmaksızın bu maddede belirtilen amaçlar ile bağlantılı, sınırlı ve ölçülü şekilde işlenebilmektedir:
                        </p>
                        <ul class="pl-5">
                            <li>Mikrobiyom (gaita) örnekleri,</li>
                            <li>Tarafınıza ilettiğimiz ekteki ankette doldurduğunuz tüm bilgiler;</li>
                            <li>Yaşam tarzına ilişkin bilgiler (yaş, meslek, kilo, boy vs),</li>
                            <li>Sağlık geçmişi (geçmiş hastalık şikayetleriniz, diyabet, karaciğer, bağırsak vs.)</li>
                            <li>Alerji bilgisi (gıda ve sair başkaca alerjiler)</li>
                            <li>İlaç kullanım bilgisi ve geçmişi (antibiyotikler, grip aşısı, vitaminler vs.)</li>
                            <li>Beslenme alışkanlıklarınız (beslenme tipi, içecekler, et, sebze, meyve tüketimi vs.)</li>
                            <li>Kimlik bilgileriniz: Adınız, soyadınız, T.C. Kimlik numaranız, pasaport numaranız veya geçici T.C. Kimlik numaranız, doğum yeri ve tarihiniz, medeni haliniz, cinsiyetiniz, sigorta veya hasta protokol numaranız ve sizi tanımlayabileceğimiz diğer kimlik verileriniz.</li>
                            <li>İletişim Bilgileriniz: Adresiniz, telefon numaranız, elektronik posta adresiniz ve sair iletişim verileriniz, müşteri temsilcileri ya da hasta hizmetleri tarafından çağrı merkezi standartları gereği tutulan sesli görüşme kayıtlarınız ile elektronik posta, mektup veya sair vasıtalar aracılığı ile tarafımızla iletişime geçtiğinizde elde edilen kişisel verileriniz.</li>
                            <li>Muhasebesel Bilgileriniz: Banka hesap numaranız, IBAN numaranız, kredi kartı bilginiz, faturalama bilgileriniz gibi finansal verileriniz.</li>
                            <li>Sağlık hizmetlerinin finansmanı ve planlaması amacıyla özel sağlık sigortasına ilişkin verileriniz ve Sosyal Güvenlik Kurumu verileriniz.</li>
                            <li>Sağlık Bilgileriniz: Laboratuvar sonuçlarınız, test sonuçlarınız, muayene verileriniz, check-up bilgileriniz, danışan-diyet bilgileriniz dahil ancak bunlarla sınırlı olmaksızın önerilen diyet sırasında veya bunların bir sonucu olarak elde edilen her türlü sağlık ve kişisel verileriniz.</li>
                            <li>www.enbiosis.com.tr sitesine veya ENBİOSİS mobil uygulaması üzerinden gönderdiğiniz veya girdiğiniz sağlık verileriniz ve sair kişisel verileriniz.</li>
                        </ul>
                    </li>
                    <li>
                        <strong>Kişisel Verilerin Aktarılması</strong>
                        <p class="pl-3">
                            Kişisel verileriniz, Kanun ve sair mevzuat kapsamında ve yukarıda yer verilen amaçlarla ENBİOSİS ile, Özel sigorta şirketleri, Sağlık bakanlığı ve bağlı alt birimleri, Sosyal Güvenlik Kurumu, Türkiye Eczacılar Birliği ve sair üçüncü kişiler, yetki vermiş olduğunuz temsilcileriniz, avukatlar, vergi ve finans danışmanları ve denetçiler de dâhil olmak üzere danışmanlık aldığımız üçüncü kişiler, düzenleyici ve denetleyici kurumlar, resmi merciler dâhil sağlık hizmetlerini yukarıda belirtilen amaçlarla geliştirmek veya yürütmek üzere işbirliği yaptığımız iş ortaklarımız ve diğer üçüncü kişiler ile paylaşılabilecektir. Kişisel verileriniz reklam veren şirketlerle paylaşılmaz, sırf reklam maksatlı kullanılmaz. ENBİOSİS personeli dışında üçüncü kişilere bilimsel araştırma yapması için yahut kendi ticari faaliyetlerini yürütmesi için aktarılmaz. <br>
                            Sağlık verileriniz başta olmak üzere özel nitelikli kişisel verileriniz ve genel nitelikli kişisel verileriniz, ENBİOSİS tarafından aşağıda yer alanlar dâhil ve bunlarla sınırlı olmaksızın bu maddede belirtilen amaçlar ile bağlantılı, sınırlı ve ölçülü şekilde işlenebilmektedir: <br>
                            Kişisel verileriniz:
                        </p>
                        <ul class="pl-5 d">
                            <li>Yukarıda KVKK Madde 5.2 ve Madde 6.3 kapsamında belirlenen amaçların varlığı halinde, yurtiçinde ve başta AB ülkeleri, Amerika, İngiltere, OECD ülkeleri, Hindistan, Çin ve Rusya olmak üzere yurtdışında bulunan depolama, arşivleme, bilişim teknolojileri desteği (sunucu, hosting, program, bulut bilişim), güvenlik, çağrı merkezi gibi alanlarda destek aldığımız üçüncü kişilerle, işbirliği yapılan ve/veya hizmet alınan Grup Şirketleri, iş ortakları, tedarikçi firmalar, bankalar, finans kuruluşları, hukuk, vergi vb. benzeri alanlarda destek alınan danışmanlık firmaları ve belirlenen amaçlarla aktarımın gerekli olduğu diğer ilişkili taraflara aktarılabilmektedir;</li>
                            <li>Yukarıda KVKK Madde 5.1 ve Madde 6.2 kapsamında yurtiçinde ve başta AB ülkeleri, Amerika, İngiltere, OECD ülkeleri, Hindistan, Çin ve Rusya olmak üzere yurtdışında bulunan belirlenen amaçlar bakımından açık rızanızın alınması şartı ile, pazarlama şirketleri, Grup Şirketleri, pazarlama desteği veren üçüncü kişi hizmet firmalarına (e-posta gönderimi, kampanya oluşturulması amacı ile reklam firmaları, CRM desteği veren firmalara açık rızanıza istinaden aktarılabilmektedir.</li>
                        </ul>
                        <p class="pl-3">
                            Ayrıca açık rızanıza istinaden ENBİOSİS’in bir kısmının veya varlıklarının (marka, alan adı ve sair ticari işletme unsurları dahil ancak bunlarla sınırlı olmaksızın) satılması durumunda Kişisel Verileriniz devralan gerçek ya da tüzel kişilere, bunların paydaşları, iş ortakları, aracıları, danışmanları da dahil olmak üzere yurtiçi ve yurtdışındaki üçüncü kişilere devrin gerektirdiği ölçüde aktarılabilir, işlem sürecinde bu üçüncü kişiler tarafından gerekli değerlendirmenin yapılması ile sınırlı ölçüde işlenebilir ve devir halinde devralan taraf varlıklarla birlikte bu varlıklara bağlı değerler olan Kişisel Verilerinizi kendisi veri sorumlusu olacak şekilde işlemeye devam edebilir.
                        </p>
                    </li>
                    <li>
                        <strong>Kişisel Veri Elde Etmenin Yöntemi ve Hukuki Sebebi</strong>
                        <p class="pl-3">
                            Kişisel verileriniz, her türlü sözlü, yazılı, görsel ya da elektronik ortamda, yukarıda yer verilen amaçlar ve ENBİOSİS ’in faaliyet konusuna dahil her türlü işin yasal çerçevede yürütülebilmesi ve bu kapsamda ENBİOSİS ’in akdi ve kanuni yükümlülüklerini tam ve gereği gibi ifa edebilmesi için toplanmakta ve işlenmektedir. İşbu kişiler verilerinizin toplanmasının hukuki sebebi;
                        </p>
                        <ul class="pl-5">
                            <li>6698 sayılı Kişisel Verilerin Korunması Kanunu,</li>
                            <li>3359 sayılı Sağlık Hizmetleri Temel Kanunu,</li>
                            <li>663 sayılı Sağlık Bakanlığı ve Bağlı Kuruluşlarının Teşkilat ve Görevleri Hakkında Kanun Hükmünde Kararname,</li>
                            <li>Özel Hastaneler Yönetmeliği,</li>
                            <li>Kişisel Sağlık Verileri Hakkında Yönetmelik,</li>
                            <li>Sağlık Bakanlığı düzenlemeleri ve sair mevzuat hükümleridir.</li>
                            <li>Avrupa Birliği Genel Veri Koruma Tüzüğü (GDPR)</li>
                        </ul>
                        <p class="pl-3">
                            Ayrıca, Kanun’un 6. maddesi 3. fıkrasında da belirtildiği üzere sağlık ve cinsel hayata ilişkin kişisel veriler ise ancak kamu sağlığının korunması, koruyucu hekimlik, tıbbı teşhis, tedavi ve bakım hizmetlerinin yürütülmesi, sağlık hizmetleri ile finansmanının planlanması ve yönetimi amacıyla, sır saklama yükümlülüğü altında bulunan kişiler veya yetkili kurum ve kuruluşlar tarafından ilgilinin açık rızası aranmaksızın işlenebilir.
                        </p>
                    </li>
                    <li>
                        <strong>Kişisel Verilerin Korunmasına Yönelik Haklar ve Yükümlülükler</strong>
                        <p class="pl-3">
                            Kanun ve ilgili mevzuatlar uyarınca;
                        </p>
                        <ul class="pl-5">
                            <li>Kişisel veri işlenip işlenmediğini öğrenme,</li>
                            <li>Kişisel veriler işlenmişse buna ilişkin bilgi talep etme,</li>
                            <li>Kişisel sağlık verilerine erişim ve bu verileri isteme,</li>
                            <li>Kişisel verilerin işlenme amacını ve bunların amacına uygun kullanılıp kullanılmadığını öğrenme,</li>
                            <li>Yurt içinde veya yurt dışında kişisel verilerin aktarıldığı üçüncü kişileri bilme,</li>
                            <li>Kişisel verilerin eksik veya yanlış işlenmiş olması hâlinde bunların düzeltilmesini isteme,</li>
                            <li>Kişisel verilerin silinmesini veya yok edilmesini isteme,</li>
                            <li>Kişisel verilerin eksik veya yanlış işlenmiş olması hâlinde bunların düzeltilmesine ve/veya kişisel verilerin silinmesini veya yok edilmesine ilişkin işlemlerin kişisel verilerin aktarıldığı üçüncü kişilere bildirilmesini isteme,</li>
                            <li>İşlenen verilerin münhasıran otomatik sistemler vasıtasıyla analiz edilmesi suretiyle kişinin kendisi aleyhine bir sonucun ortaya çıkmasına itiraz etme hakkını haizsiniz.</li>
                        </ul>
                        <p class="pl-3">
                            Mezkûr haklarınızdan birini ya da birkaçını kullanmanız halinde ilgili bilgi tarafınıza, açık ve anlaşılabilir bir şekilde yazılı olarak ya da elektronik ortamda, tarafınızca sağlanan iletişim bilgileri yoluyla, bildirilir. <br>
                            Tarafınız ile kurulmuş olan sözleşmesel ilişki ve sözleşmenin ifası kapsamında kişisel verilerinizi işleyeceğimizden gerek duyulması halinde yurt içi ve yurt dışı ile paylaşılacağı, bu doğrultuda talep ettiğiniz takdirde kişisel veri işleme, aktarma, muhafaza, kullanım amacı, düzeltme, itiraz etme, silme, yok etme, anonimleştirme, zarar doğması halinde giderim talep etme hakkınız bulunduğunu bildirir, bu hususlarda kanun gereğince tarafımıza başvuru yapabilme hakkına haizsiniz.
                        </p>
                    </li>
                    <li>
                        <strong>Veri Güvenliği</strong>
                        <p class="pl-3">
                            ENBİOSİS, kişisel verilerinizi bilgi güvenliği standartları ve prosedürleri gereğince alınması gereken tüm teknik ve idari güvenlik kontrollerine tam uygunlukla korumaktadır. Söz konusu güvenlik tedbirleri, teknolojik imkânlar da göz önünde bulundurularak muhtemel riske uygun bir düzeyde sağlanmaktadır. Şirketimiz ile paylaşmış olduğunuz kişisel verilerinizi, veri sorumlusu ENBİOSİS Biyoteknoloji A.Ş. olarak kanun ve mevzuata olarak kullanarak muhafaza edeceğiz.
                        </p>
                    </li>
                </ol>
                <h4>
                    KİŞİSEL VERİLERİN TOPLANMA YÖNTEMLERİ
                </h4>
                <p class="pl-3">
                    Kişisel veriler, her türlü sözlü, yazılı ya da elektronik ortamda toplanabilmektedir.
                </p>
                <h4>
                    KULLANICININ HAKLARI
                </h4>
                <p class="pl-3">
                    KVKK’nın 11. maddesi uyarınca Kullanıcı;<br>
                    Kişisel veri işlenip işlenmediğini öğrenme;<br>
                    Kişisel verileri işlenmişse buna ilişkin bilgi talep etme;<br>
                    (c)Kişisel verilerin işlenme amacını ve bunların amacına uygun kullanılıp kullanılmadığınıöğrenme;<br>
                    Yurt içinde veya yurt dışında kişisel verilerin aktarıldığı üçüncü kişileri bilme;<br>
                    Kişisel verilerin eksik veya yanlış işlenmiş olması hâlinde bunların düzeltilmesini isteme ve bu kapsamda yapılan işlemin kişisel verilerin aktarıldığı üçüncü kişilere bildirilmesini isteme;<br>
                    KVKK ve ilgili diğer kanun hükümlerine uygun olarak işlenmiş olmasına rağmen, işlenmesini gerektiren sebeplerin ortadan kalkması hâlinde kişisel verilerin silinmesini veya yok edilmesini isteme ve bu kapsamda yapılan işlemin kişisel verilerin aktarıldığı üçüncü kişilere bildirilmesini isteme;<br>
                    İşlenen verilerin münhasıran otomatik sistemler vasıtasıyla analiz edilmesi suretiyle kişinin kendisi aleyhine bir sonucun ortaya çıkmasına itiraz etme;<br>
                    Kişisel verilerin kanuna aykırı olarak işlenmesi sebebiyle zarara uğraması hâlinde zararın giderilmesini talep etme haklarına sahiptir.<br>
                    Kullanıcı olarak, haklarınıza ilişkin taleplerinizi, aşağıda düzenlenen yöntemlerle tarafımıza iletmeniz durumunda, talebiniz, talebin niteliğine göre talebi en geç otuz gün içinde ücretsiz olarak sonuçlandıracaktır. Ancak, Kişisel Verileri Koruma Kurulunca bir ücret öngörülmesi halinde, tarafımızca belirlenen tarifedeki ücret alınacaktır.<br>
                    Yukarıda belirtilen haklarınızı kullanmak için kimliğinizi tespit edici gerekli bilgiler ile KVKK’nın 11. maddesinde belirtilen haklardan kullanmayı talep ettiğiniz hakkınıza/haklarınıza yönelik açıklamalarınızı içeren yazılı talebinizi;<br>
                    ENBİOSİS Biyoteknoloji Anonim Şirketi Maslak Mah. Taşyoncası Sk. Maslak 1453 1 U T4A T4B No: 1 U İç Kapı No:B91 Sarıyer/İSTANBUL adresine kimliğinizi tespit edici belgeler ile bizzat elden iletebilir, noter kanalıyla veya KVKK ’da belirtilen diğer yöntemler ile gönderebilir veya yazılı talebinizi info@enbiosis.com adresine güvenli elektronik imzalı olarak gönderebilirsiniz.
                </p> --}}
                <h4>
                    ÇEREZ İLKELERİ
                </h4>
                <p class="pl-3">
                    İşbu ENBİOSİS Çerez İlkeleri (“Çerez İlkeleri”) Gizlilik İlkeleri’nin tamamlayıcısı ve ayrılmaz bir parçasıdır.<br>
                    Çoğu internet sitesinde olduğu gibi, ENBİOSİS içerisinde, kullanıcıya kişisel içerik ve reklamlar göstermek, site içinde analitik faaliyetler gerçekleştirmek ve ziyaretçi kullanım alışkanlıklarını takip etmek amacıyla çerezler kullanılabilmektedir.<br>
                    ENBİOSİS, işbu Çerez İlkeleri’ni ENBİOSİS ’te hangi çerezlerin kullanıldığını ve Kullanıcının bu konudaki seçimini nasıl yönetebileceğini açıklamak amacıyla hazırlamıştır.
                </p>
                <h4>
                    ÇEREZ (COOKIE) TANIMI
                </h4>
                <p class="pl-3">
                    Çerezler, kullanıcının sabit diskinde geçici olarak kaydedilen ve kullanıcının ENBİOSİS ’i bir sonraki ziyaretinde, ENBİOSİS ’in kullanıcının bilgisayarını tanımasını sağlayan küçük dosyalardır. Çerezler, kullanıcının ziyaret ettiği internet sitesiyle ilişkili sunucular tarafından oluşturulurlar. Böylelikle kullanıcı aynı internet sitesini ziyaret ettiğinde sunucu bunu anlayabilir. Çerezler sadece ENBİOSİS ’i ilgilendiren bilgileri toplamak için kullanır.
                </p>
                <h4>
                    ENBİOSİS’DA KULLANILAN ÇEREZLER
                </h4>
                <p class="pl-3">
                    Çerezleri, analitik / performans, işlevsel amaçlar ve sitemizin güvenli alanlarına erişme becerisi için kullanırız.<br>
                    İzleme Teknolojileri<br>
                    Çerezler ENBİOSİS ve pazarlama aracımız (Google Analytics) tarafından kullanılır. Bu teknolojiler eğilimleri analiz etmek, siteyi yönetmek, kullanıcıların sitelerdeki hareketlerini izlemek ve kullanıcı tabanımız hakkında demografik bilgileri toplamak için kullanılmaktadır.<br>
                    Analytics / Günlük Dosyaları<br>
                    Günlük Dosyaları – Çoğu web sitesinde olduğu gibi, belirli bilgileri otomatik olarak toplarız ve günlük dosyalarına kaydederiz. Bu bilgi, internet protokolü (IP) adreslerini, tarayıcı türünü, internet servis sağlayıcısını (ISS), yönlendiren / çıkan sayfaları, işletim sistemini, tarih / saat damgasını ve / veya tıklama akışı verilerini içerebilir.<br>
                    Bu otomatik olarak toplanan günlük bilgilerini, sizin hakkınızda topladığımız diğer bilgilerle birleştirebiliriz. Bunu size sunduğumuz hizmetleri iyileştirmek, pazarlamayı geliştirmek ve bağlantı geçmişiniz hakkında şeffaflık sağlamak için yapıyoruz.
                </p>
                <h4>
                    ÇEREZLERİN KULLANIM AMAÇLARI
                </h4>
                <p class="pl-3">
                    ENBİOSİS ’te, çerezler aşağıdaki amaçlar kapsamında kullanılmaktadır:<br>
                    ENBİOSİS ’in çalışması için gerekli temel fonksiyonları gerçekleştirmek. Örneğin, Kullanıcının ENBİOSİS 'te farklı sayfaları ziyaret ederken tekrar şifre girmesine gerek kalmaması,<br>
                    ENBİOSİS ’i analiz etmek ve performansını arttırmak. Örneğin, ENBİOSİS ’in üzerinde çalıştığı farklı sunucuların entegrasyonu, ENBİOSİS ’i ziyaret edenlerin sayısının tespit edilmesi ve buna göre performans ayarlarının yapılması ya da ziyaretçilerin aradıklarını bulmalarının kolaylaştırılması,<br>
                    ENBİOSİS ’in işlevselliğini arttırmak ve kullanım kolaylığı sağlamak. Örneğin, ENBİOSİS üzerinden üçüncü taraf sosyal medya mecralarına paylaşımda bulunmak, ENBİOSİS ’i ziyaret eden ziyaretçinin daha sonraki ziyaretinde kullanıcı adı bilgisinin ya da arama sorgularının hatırlanması,<br>
                    Kişiselleştirme, hedefleme ve reklamcılık faaliyeti gerçekleştirmek. Örneğin, ziyaretçilerin görüntüledikleri sayfa ve ürünler üzerinden ziyaretçilerin ilgi alanlarıyla bağlantılı reklam gösterilmesi.
                </p>
                <h4>
                    ÇEREZ TERCİHLERİNİN YÖNETİMİ
                </h4>
                <p class="pl-3">
                    Birçok internet tarayıcısı, varsayılan olarak çerezleri otomatik olarak kabul etmeye ayarlıdır. Bu ayarları, çerezleri engelleyecek veya cihazınıza çerez gönderildiğinde uyarı verecek şekilde değiştirebilirsiniz. Çerezleri yönetmenin birkaç yolu bulunmaktadır. Tarayıcı ayarlarınızı nasıl düzenleyeceğiniz hakkında ayrıntılı bilgi almak için lütfen tarayıcınızın talimat veya yardım ekranına başvurun. Çerezler, üyeler/kullanıcılar tarafından devre dışı bırakabilir ya da engellenebilir. Çerezleri reddederseniz, sitemizin bazı özelliklerini veya alanlarını kullanma yeteneğiniz sınırlı olabilir.
                </p>
            </div>
        </div>
</section>
@endsection
