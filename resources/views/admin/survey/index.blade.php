@extends('layouts.admin')

@section('styles')
    <style>
        ._survey-display-inline{
            display: inline,
        }
    </style>
@endsection

@section('content')
<div class="mt-3 mx-5">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-dark text-light">surveys</div>
                <div class="card-body ">
                    You are logged in as Admin You are in surveys Section!
                </div>
            </div>
            @include('include.messages')
            @include('include.form-errors')
        </div>
    </div>
</div>
<div class="mt-3 mx-5">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="row">
                <div class="col-lg-6 col-12">
                    {{$surveys->links()}}
                </div>
                <div class="col-lg-6 col-12 text-right">
                    <a  href="{{route('admin.surveys.create')}}" class="header-table btn btn-outline-success mb-2">Create Survey</a>
                    {{-- <a  href="{{route('admin.surveys.trash')}}" class="header-table btn btn-outline-danger mb-2">User trash</a> --}}
                </div>
            </div>
            @if ($surveys)
                @foreach ($surveys as $survey)
                    <div class="card my-4">
                        <div class="card-header bg-light text-dark">
                            <div class="row">
                                <div class="col-6">
                                    # {{$loop->iteration}}

                                </div>
                                <div class="col-6 text-right">
                                    <a href="{{ route('admin.survey.export', $survey->id) }}" class="btn btn-outline-success mx-3 my-3">
                                        Export survey template
                                    </a>
                                    <a href="{{ route('admin.sections.index', $survey->id) }}" class="btn btn-outline-success mx-3 my-3">
                                        View All Section
                                    </a>
                                    {!! Form::open(['method'=>'DELETE', 'action' => ['AdminSurveyController@destroy', $survey->id], 'style'=> 'display:inline']) !!}
                                        {!! Form::submit('Delete', ['class'=>'btn btn-outline-danger mr-2 mb-1','onclick'=>"return confirm('Silmek isteğinizden Emin misiniz?')"]) !!}
                                    {!! Form::close() !!}   
                                </div>
                            </div>
                        </div>
                        <div class="card-body ">
                            <div class="row">
                                <div class="col-lg-6 col-12">
                                    <p class="mx-3 my-3">
                                        publisher : {{$survey->admin->name}}, 
                                    </p>
                                    <p class="mx-3 my-3">
                                        created : {{$survey->created_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}, 
                                    </p>
                                    <p class="mx-3 my-3">
                                        updated : {{$survey->updated_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}, 
                                    </p>
                                    {{-- <p class="mx-3 my-3">
                                        number Of Section : {{$survey->numberOfSection}}, 
                                    </p> --}}
                                    <p class="mx-3 my-3">
                                        @if ($survey->publish === 1)
                                            status : <span class="text-success mx-3"> published </span>
                                            // Process : <button class="publish-survey btn btn-danger mx-2" data-id="{{ $survey->id }}" data="0" > unpublished </button>
                                        @else
                                            status : <span class="text-danger mx-3">  not published </span>
                                            // Process : <button class="publish-survey btn btn-success mx-2" data-id="{{ $survey->id }}" data="1" > publish </button>
                                        @endif
                                    </p>
                                    <div class="text-left mx-3 my-3"><h4 class="mb-4">Survey Section</h4></div>
                                    <p class="mx-3 my-3"> This Survey Have {{$survey->numberOfSection}} sections </p>
                                    <a href="{{ route('admin.sections.index', $survey->id) }}" class="btn btn-success mx-3 my-3">
                                        View All Section
                                    </a>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <div class="text-center"><h4 class="mb-4">Survey Lang</h4></div>
                                    @if ($survey->surveyLang)
                                        <table class="table table-responsive-lg table-striped">
                                            <thead class="thead-light">
                                            <tr>
                                                <th scope="col" style="width:10%">#</th>
                                                <th scope="col" style="width:10%">lang</th>
                                                <th scope="col" style="width:30%">title</th>
                                                <th scope="col" style="width:40%">description</th>
                                                <th scope="col" style="width:10%">process</th>
                                                
                                            </tr>
                                            </thead>
                                            <tbody>
                                                
                                                @foreach ($survey->surveyLang as $surveyLang)
                                                    <tr>
                                                        <td>{{$loop->iteration}}</td>
                                                        <td>{{$surveyLang->language}}</td>
                                                        <td>{{$surveyLang->title}}</td>
                                                        <td>{!! str_limit($surveyLang->description, 20) !!}</td>
                                                        <td>
                                                            <button type="button" class="btn btn-outline-success mr-2 mb-1" data-toggle="modal" data-target="#id__{{$surveyLang->id}}_update">
                                                                Update
                                                            </button>
                                                            {!! Form::open(['method'=>'DELETE', 'action' => ['AdminSurveyController@destroyLang', $survey->id, $surveyLang->id], 'style'=> 'display:inline']) !!}
                                                                {!! Form::submit('Delete', ['class'=>'btn btn-outline-danger mr-2 mb-1','onclick'=>"return confirm('Silmek isteğinizden Emin misiniz?')"]) !!}
                                                            {!! Form::close() !!} 
                                                            <div class="modal fade" id="id__{{$surveyLang->id}}_update" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalCenterTitle">Create Lang For Survey</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            {!! Form::model($surveyLang, ['method'=>'PATCH', 'action' => ['AdminSurveyController@updateLang', $survey->id , $surveyLang->id], 'id' => 'id__' . $surveyLang->id . '_update_form']) !!}
                                                                                <div class="form-group">
                                        
                                                                                    {!! Form::label('title', 'Title : ') !!}
                                                                                    {!! Form::text('id_' . $surveyLang->id . 'title', $surveyLang->title, ['class'=>'form-control', 'min'=> '1', 'max' => '100', 'id' => '__' . $surveyLang->id . '_title']) !!}
                                                                    
                                                                                </div>
                                                                    
                                                                                <div class="form-group">
                                                                    
                                                                                    {!! Form::label('Description', 'description : ') !!}
                                                                                    {!! Form::textarea('id_' . $surveyLang->id . 'description', $surveyLang->description, ['class'=>'form-control', 'rows' => 5, 'id' => '__' . $surveyLang->id . '_description']) !!}
                                                                                    
                                                                                </div>
                                                                            {!! Form::close() !!} 
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                        <button class="btn btn-primary" onclick="
                                                                            event.preventDefault();
                                                                            document.getElementById('id__{{$surveyLang->id}}_update_form').submit();
                                                                        ">
                                                                        Save Changes</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                <tr class="text-center">
                                                    <td colspan= "5" >
                                                        <button type="button" class="btn btn-success mr-2 mb-1" data-toggle="modal" data-target="#id_create">
                                                            create a new lang for this survey
                                                        </button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal -->
                    <div class="modal fade" id="id_create" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalCenterTitle">Create Lang For Survey</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                </div>
                                <div class="modal-body">
                                    {{-- {!! Form::open(['method'=>'DELETE', 'action' => ['AdminCouponController@destroy', $coupon->id], 'style'=> 'display:inline', 'id' => 'id_form_' . $coupon->code]) !!} --}}
                                    {!! Form::open(['method'=>'POST', 'action' => ['AdminSurveyController@storeLang', $survey->id], 'style'=> 'display:inline', 'id' => 'id_form_create']) !!}
                                        <div class="form-group">

                                            {!! Form::label('title', 'Title : ') !!}
                                            {!! Form::text('title', null, ['class'=>'form-control', 'min'=> '1', 'max' => '100']) !!}
                            
                                        </div>
                            
                                        <div class="form-group">
                            
                                            {!! Form::label('Description', 'description : ') !!}
                                            {!! Form::textarea('description', null, ['class'=>'form-control', 'rows' => 5, 'id'=> 'description']) !!}
                                            
                                        </div>
                                        <div class="form-group">
                            
                                            {{ Form::label('Lang : ') }}
                                            {{ Form::select('lang', $countres, null, array('class'=>'form-control', 'placeholder'=>'Please select ...')) }}
                                            
                                        </div>
                                    {!! Form::close() !!} 
                                </div>
                                <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button class="btn btn-primary" onclick="
                                    event.preventDefault();
                                    document.getElementById('id_form_create').submit();
                                ">
                                Save Changes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</div>

@endsection

@section('script')

    <script src="https://cdn.ckeditor.com/4.13.0/basic/ckeditor.js"></script>
    <script type="text/javascript">
        $( document ).ready(function() {
            //console.log( "ready!" );
            $(".publish-survey").click(function (e) {
            //    e.preventDefault();
 
                let ele = $(this);
                $.ajax({
                    url: '{{ url("admin/surveys/") }}/' +ele.attr("data-id") ,
                    method: "patch",
                    data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id"), publish: ele.attr("data")},
                    success: function (response) {
                        window.location.reload();
                    },
                    error: function (error){
                        return console.log(error);
                    }
                });
            }); 
            $('textarea').each(function(e){
                CKEDITOR.replace( this.id, {language: '{{ app()->getlocale() }}'});
            });
        });
    </script>
@endsection