@extends('layouts.admin')

@section('content')
<div class="mt-3 mx-5">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-dark text-light">surveys</div>
                <div class="card-body ">
                    You are logged in as Admin You are in surveys Section!
                    <p class="py-1">survey Create Steps</p>
                    <ol>
                        <li>Create survey for default Language ( to )
                            <ol>
                                <li>create sections</li>
                                <li>create a question for every section</li>
                            </ol>
                        </li>
                        <li>after create a survey with default lang you can repeat all previous steps but for another lang such as ( en, ru, uk.... )</li>
                    </ol>
                </div>
            </div>
            @include('include.form-errors')
        </div>
        <dic class="col-12 mt-3">
            <div class="card">
                <div class="card-header bg-light text-dark">survey info for lang = tr</div>
                <div class="card-body ">
                    {!! Form::open(['method'=>'POST', 'action' => 'AdminSurveyController@store', 'files'=>true]) !!}
                    <div class="row">
                        <div class="form-group col-lg-6 col-md-12 col-sm-12 col-xs-12">
                
                            <label for="title">title <sup class="text-danger">*</sup> :</label>
                            {!! Form::text('title', null, ['class'=>'form-control']) !!}
                    
                        </div>

                        <div class="form-group col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    
                            <label for="description">Description <sup class="text-danger">*</sup> :</label>
                            {!! Form::textarea('description', null, ['class'=>'form-control', 'rows' => 5]) !!}
                            
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::submit('Create Product', ['class'=>'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </dic>
    </div>
</div>
@endsection

@section('script')
    <script src="https://cdn.ckeditor.com/4.13.0/basic/ckeditor.js"></script>
    <script>
        $( document ).ready(function() {
            CKEDITOR.replace( 'description', {language: '{{ app()->getlocale() }}'});
        });
    </script>
@endsection
