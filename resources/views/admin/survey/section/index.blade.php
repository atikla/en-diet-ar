@extends('layouts.admin')

@section('styles')
    <style>
        .card-body{
            border-bottom: 1px solid rgba(0,0,0,.125)
        }
    </style>
@endsection

@section('content')
<div class="mt-3 mx-5">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-dark text-light">Survey Sections</div>
                <div class="card-body ">
                    You are logged in as Admin You are in survey Sections Section!
                </div>
            </div>
            @include('include.messages')
            @include('include.form-errors')
        </div>
    </div>
</div>
<div class="mt-3 mx-5 mb-5 pb-5">
    <div class="row justify-content-center">
        <div class="col-12">
            @if ($sections)
                @foreach ($sections as $section)
                    @if( $loop->first )
                        <div class="text-center mb-3">
                            <button type="button" class="btn btn-success mr-2 mb-1" data-toggle="modal" data-target="#id_create">
                                create a new section for this survey
                            </button>
                        </div>
                    @endif 
                    <div class="accordion" id="accordionExample">

                        <div class="card" aria-controls="__su_col_section_{{$loop->index}}">

                            <div class="card-header" id="__su_section{{$loop->index}}">
                                <div class="row">
                                    <div class="col-lg-6 col-12">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link" type="button"  data-toggle="collapse" data-target="#__su_col_section_{{$loop->index}}">
                                                Section {{$loop->iteration}} - section order {{$section->section_order}} -
                                                section duration {{$section->duration}} min - 
                                                <span class="text-dark"> question Count {{$section->question()->count()}} </span>
                                            </button>
                                        </h2>
                                    </div>
                                    <!-- Modal For update Section Order -->
                                    <div class="modal fade" id="id_update_section_order_form{{$section->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalCenterTitle">Update Section Order</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                </div>
                                                <div class="modal-body">
                                                    {!! Form::model($section, ['method'=>'PATCH', 'action' => ['AdminSurveySectionsController@update', $section->survey->id,  $section->id ], 'style'=> 'display:inline', 'id' => 'update_section_order' . $section->id]) !!}
                                                        <div class="form-group">
                
                                                            {!! Form::label('update_' . $section->id . 'section_order', 'Section Order : ') !!}
                                                            {!! Form::number('update_' . $section->id . 'section_order', $section->section_order, ['class'=>'form-control', 'max' => '100', 'min' => '1']) !!}
                                            
                                                        </div>
                                                        <div class="form-group">
                
                                                            {!! Form::label('update_' . $section->id . 'duration', 'Section Duration : ') !!}
                                                            {!! Form::number('update_' . $section->id . 'duration', $section->duration, ['class'=>'form-control', 'max' => '100', 'min' => '1']) !!}
                                            
                                                        </div>
                                                    {!! Form::close() !!} 
                                                </div>
                                                <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button class="btn btn-primary" onclick="
                                                    event.preventDefault();
                                                    document.getElementById('update_section_order{{$section->id}}').submit();
                                                ">
                                                Save Changes</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-12 text-lg-right">
                                       
                                        <a href="#" class="btn btn-outline-primary ml-2 mb-lg-0 mb-2" data-toggle="modal" data-target="#id_update_section_order_form{{$section->id}}">
                                            Update Section Order
                                        </a>
                                        <a href="#" class="btn btn-outline-success ml-2 mb-lg-0 mb-2" data-toggle="modal" data-target="#id_create_lang_{{$section->id}}">
                                            Add Lang
                                        </a>
                                        <a href="{{route('admin.questions.index', [$section->survey->id,  $section->id])}}" class="btn btn-outline-info ml-2 mb-lg-0 mb-2">
                                            Show Section Question
                                        </a>
                                        {!! Form::open(['method'=>'DELEtE', 'action' => ['AdminSurveySectionsController@destroy', $section->survey->id,  $section->id ], 'style'=> 'display:inline']) !!}
                                            {!! Form::submit('Delete Section', ['class'=>'btn btn-outline-danger ml-2 mb-lg-0 mb-2','onclick'=>"return confirm('Silmek isteğinizden Emin misiniz?')"]) !!}
                                        {!! Form::close() !!}
                                        {{-- <a href="#" class="btn btn-outline-danger">
                                            Delete
                                        </a> --}}

                                    </div>
                                    <!-- Modal For Add New Lang For Sections-->
                                    <div class="modal fade" id="id_create_lang_{{$section->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalCenterTitle">Create Lang For Survey</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                </div>
                                                <div class="modal-body">
                                                    {{-- {!! Form::open(['method'=>'DELETE', 'action' => ['AdminCouponController@destroy', $coupon->id], 'style'=> 'display:inline', 'id' => 'id_form_' . $coupon->code]) !!} --}}
                                                    {!! Form::open(['method'=>'POST', 'action' => ['AdminSurveySectionsController@storeLang', $section->survey->id, $section->id], 'style'=> 'display:inline', 'id' => 'id_form_create_lang_' . $section->id]) !!}
                                                        <div class="form-group">
                
                                                            {!! Form::label('section_id_' . $section->id . '_title', 'Title : ') !!}
                                                            {!! Form::text('section_id_' . $section->id . '_title', null, ['class'=>'form-control']) !!}
                                            
                                                        </div>
                                            
                                                        <div class="form-group">
                                                            
                                                            {!! Form::label('section_id_' . $section->id . '_description', 'description : ') !!}
                                                            {!! Form::textarea('section_id_' . $section->id . '_description', null, ['class'=>'form-control', 'rows' => 5, 'id'=> 'section_id_' . $section->id . '_description']) !!}
                                                            
                                                        </div>
                                                        <div class="form-group">
                                            
                                                            {{ Form::label('section_lang : ') }}
                                                            {{ Form::select('section_id_' . $section->id .  '_lang', $countres, null, array('class'=>'form-control', 'placeholder'=>'Please select ...')) }}
                                                            
                                                        </div>
                                                    {!! Form::close() !!} 
                                                </div>
                                                <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button class="btn btn-primary" onclick="
                                                    event.preventDefault();
                                                    document.getElementById('id_form_create_lang_{{$section->id}}').submit();
                                                ">
                                                Save Changes</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="__su_col_section_{{$loop->index}}" class="collapse {{ $loop->first ? 'show' : ''}}" aria-labelledby="__su_section{{$loop->index}}" data-parent="#accordionExample">
                                <div class="card-body">
                                    <table class="table table-responsive-lg table-striped">
                                        <thead class="thead-light">
                                        <tr>
                                            <th scope="col" style="width:10%">#</th>
                                            <th scope="col" style="width:10%">lang</th>
                                            <th scope="col" style="width:30%">title</th>
                                            <th scope="col" style="width:30%">description</th>
                                            <th scope="col" style="width:20%">process</th>
                                            
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($section->surveySectionLang as $sectionLang)
                                                <tr>
                                                    <td>{{$loop->iteration}}</td>
                                                    <td>{{$sectionLang->Language}}</td>
                                                    <td>{{$sectionLang->title}}</td>
                                                    <td>{!! $sectionLang->description !!}</td>
                                                    <td>
                                                        {!! Form::open(['method'=>'DELETE', 'action' => ['AdminSurveySectionsController@destroyLang', $section->survey->id, $section->id, $sectionLang->id], 'style'=> 'display:inline']) !!}
                                                            {!! Form::submit('Delete', ['class'=>'btn btn-outline-danger mr-2 mb-1','onclick'=>"return confirm('Silmek isteğinizden Emin misiniz?')"]) !!}
                                                        {!! Form::close() !!} 
                                                        <a href="#" class="btn btn-outline-primary " data-toggle="modal" data-target="#id_update_lang_{{$sectionLang->id}}">
                                                            Update Section Lang
                                                        </a>
                                                        <!-- Modal For Update section lang-->
                                                        <div class="modal fade" id="id_update_lang_{{$sectionLang->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                    <h5 class="modal-title" id="exampleModalCenterTitle">Create New Section for Survey </h5>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        {{-- {!! Form::open(['method'=>'DELETE', 'action' => ['AdminCouponController@destroy', $coupon->id], 'style'=> 'display:inline', 'id' => 'id_form_' . $coupon->code]) !!} --}}
                                                                        {!! Form::model($sectionLang, ['method'=>'PATCH', 'action' => ['AdminSurveySectionsController@updateLang', $section->survey->id, $section->id, $sectionLang->id], 'style'=> 'display:inline', 'id' => 'id_update_lang_form_' . $sectionLang->id]) !!}
                                                                            <div class="form-group">

                                                                                {!! Form::label('update_lang_' . $sectionLang->id . '_title', 'Title : ') !!}    
                                                                                {!! Form::text('update_lang_' . $sectionLang->id . '_title', $sectionLang->title, ['class'=>'form-control', 'min'=> '1', 'max' => '100']) !!}
                                                                
                                                                            </div>
                                                                
                                                                            <div class="form-group">
                                                                
                                                                                {!! Form::label('update_lang_' . $sectionLang->id . '_description', 'Description : ') !!}
                                                                                {!! Form::textarea('update_lang_' . $sectionLang->id . '_description', $sectionLang->description, ['class'=>'form-control', 'rows' => 5, 'id'=> 'update_lang_' . $sectionLang->description . 'description']) !!}
                                                                                
                                                                            </div>
                                                                        {!! Form::close() !!} 
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                    <button class="btn btn-primary" onclick="
                                                                        event.preventDefault();
                                                                        document.getElementById('id_update_lang_form_{{$sectionLang->id}}').submit();
                                                                    ">
                                                                    Save Changes</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>               
                @endforeach
                <!-- Modal For Add New Section For Survey-->
                <div class="modal fade" id="id_create" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle">Create New Section for Survey </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                            <div class="modal-body">
                                {{-- {!! Form::open(['method'=>'DELETE', 'action' => ['AdminCouponController@destroy', $coupon->id], 'style'=> 'display:inline', 'id' => 'id_form_' . $coupon->code]) !!} --}}
                                {!! Form::open(['method'=>'POST', 'action' => ['AdminSurveySectionsController@store', $sections[0]->survey->id], 'style'=> 'display:inline', 'id' => 'id_form_create']) !!}
                                    <div class="form-group">

                                        {!! Form::label('title', 'Title : ') !!}    
                                        {!! Form::text('title', null, ['class'=>'form-control', 'min'=> '1', 'max' => '100']) !!}
                        
                                    </div>
                        
                                    <div class="form-group">
                        
                                        {!! Form::label('Description', 'description : ') !!}
                                        {!! Form::textarea('description', null, ['class'=>'form-control', 'rows' => 5, 'id'=> 'description']) !!}
                                        
                                    </div>
                                    <div class="form-group">
                        
                                        {!! Form::label('section_order', 'section_order : ') !!}
                                        {{ Form::number('section_order', $sections->last()->section_order + 1, ['class'=>'form-control']) }}
                                        
                                    </div>
                                {!! Form::close() !!} 
                            </div>
                            <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button class="btn btn-primary" onclick="
                                event.preventDefault();
                                document.getElementById('id_form_create').submit();
                            ">
                            Save Changes</button>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
@endsection

@section('script')
    <script src="https://cdn.ckeditor.com/4.13.0/basic/ckeditor.js"></script>
    <script type="text/javascript">
        $( document ).ready(function() {
            $('textarea').each(function(e){
                CKEDITOR.replace( this.id, {language: '{{ app()->getlocale() }}'});
            });
        });
</script>
@endsection