@extends('layouts.admin')

@section('content')
<div class="mt-3 mx-5">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-dark text-light">Create question Lang</div>
                <div class="card-body ">
                    You are logged in as Admin You are in Create question Lang Section!
                </div>
            </div>
            @include('include.form-errors')
        </div>
        <div class="col-12 mt-3">
            <div class="card">
                <div class="card-header bg-light text-dark">Question info for New lang</div>
                <div class="card-body ">
                    

                    @if ($question->question_type == 'radio' || $question->question_type == 'checkbox')
                        {!! Form::open(['method'=>'POST', 'action' => ['AdminQuestionController@storeLang', $surveyId, $sectionId, $questionId, 'type' => '1'], 'files'=>true]) !!}
                        <div class="row">
                        <input type="hidden" name="q_type" value="{{ $question->question_type }}">
                        {{-- {{$question->question_type}} --}}
                        <div class="col-6">
                            <h4>Default Language values</h4><hr>
                            <div class="form-group">
                                                                    
                                {!! Form::label('temp', 'Question Title For Default Language ( TR ) : ', ['class' => 'text-danger']) !!}
                                {!! Form::text('temp', $question->defaultLanguage->title, ['class'=>'form-control', 'placeholder' => 'Enter Your Question Title', 'disabled']) !!}
                                
                            </div>
                            <div class="form-group">
                                            
                                {{ Form::label('lang : ') }}
                                {{ Form::select('lang', $countres, $question->defaultLanguage->lang, ['class'=>'form-control', 'disabled']) }}
                                
                            </div>
                            <h4>Default Language Options</h4><hr>
                            {{-- {{$question->defaultLanguage}} <br> --}}
                            @foreach ($question->defaultLanguage->option_name as $item)
                                <div class="form-group">
                                                                    
                                    {!! Form::label('temp', $loop->iteration. ' option For Default Language ( TR ) : ', ['class' => 'text-danger']) !!}
                                    {!! Form::text('temp', $item['title'], ['class'=>'form-control', 'placeholder' => 'Enter Your Question Title', 'disabled']) !!}
                                    
                                </div>
                                @if ( $item['is_if'] )
                                    <div class="form-group">
                                                                        
                                        {!! Form::label('temp', 'the conditional question for this option For Default Language ( TR ) : ', ['class' => 'text-danger']) !!}
                                        {!! Form::text('temp', $item['is_if'], ['class'=>'form-control', 'disabled']) !!}
                                        
                                    </div>
                                @endif
                                <hr>
                            @endforeach
                        </div>
                        <div class="col-6">
                            <h4>New Language values</h4><hr>
                            <div class="form-group">
                                                                    
                                {!! Form::label('title', 'Question Title For New Lang : ') !!}
                                {!! Form::text('title', null, ['class'=>'form-control', 'placeholder' => 'Enter Your Question Title', 'required']) !!}
                                
                            </div>
                            <div class="form-group">
                                            
                                {{ Form::label('lang : ') }}
                                {{ Form::select('lang', $countres, 'en', [ 'class'=>'form-control', 'placeholder'=>'Please select ...', 'required' ]) }}
                                
                            </div>
                            <h4>New Language Options</h4><hr>
                            @foreach ($question->defaultLanguage->option_name as $item)
                                <div class="form-group">
                                                                        
                                    {!! Form::label('temp', $loop->iteration. ' option For New Language') !!}
                                    {!! Form::text('option_name[choices][' . $loop->iteration . ']', null, ['class'=>'form-control', 'placeholder' => 'Enter ' . $loop->iteration. ' option For New Language', 'required']) !!}
                                    
                                </div>
                                @if ( $item['is_if'] )
                                    <div class="form-group">
                                                                        
                                        {!! Form::label('temp', 'the conditional question for option this For New Language : ') !!}
                                        {!! Form::text('option_name[if][' . $loop->iteration .']', null, ['class'=>'form-control', 'placeholder'=> 'Enter the conditional question for this option', 'required']) !!}
                                        
                                    </div>
                                @endif
                                <hr>
                            @endforeach
                        </div>
                    @else
                        {!! Form::open(['method'=>'POST', 'action' => ['AdminQuestionController@storeLang', $surveyId, $sectionId, $questionId, 'type' => '0'], 'files'=>true]) !!}
                            <div class="row">
                            <input type="hidden" name="q_type" value="{{ $question->question_type }}">
                            <div class="col-12">
                                <div class="form-group">
                                                                    
                                    {!! Form::label('temp', 'Question Title For Default Language ( TR ) : ', ['class' => 'text-danger']) !!}
                                    {!! Form::text('temp', $question->defaultLanguage->title, ['class'=>'form-control', 'placeholder' => 'Enter Your Question Title', 'disabled']) !!}
                                    
                                </div>
                            </div>
                            <div class="col-lg-6 col-12">
                                <div class="form-group">
                                                                    
                                    {!! Form::label('title', 'Question Title For New Lang : ') !!}
                                    {!! Form::text('title', null, ['class'=>'form-control', 'placeholder' => 'Enter Your Question Title', 'required']) !!}
                                    
                                </div>
                            </div>
                            <div class="col-lg-6 col-12">
                                <div class="form-group">
                                            
                                    {{ Form::label('lang : ') }}
                                    {{ Form::select('lang', $countres, 'en', array('class'=>'form-control', 'placeholder'=>'Please select ...')) }}
                                    
                                </div>
                            </div>
                        @endif
                        <div class="col-12">
                            <div class="form-group">
                                {!! Form::submit('Create Lang For Question', ['class'=>'btn btn-primary']) !!}
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script>
    
    </script>
@endsection
