@extends('layouts.admin')

@section('styles')
    <style>
        .card-body{
            border-bottom: 1px solid rgba(0,0,0,.125)
        }
    </style>
@endsection

@section('content')
<div class="mt-3 mx-5">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-dark text-light">questions Section</div>
                <div class="card-body ">
                    You are logged in as Admin You are in questions Section!
                </div>
            </div>
            @include('include.messages')
            @include('include.form-errors')
        </div>
    </div>
</div>
<div class="mt-3 mx-5 mb-5 pb-5">
    <div class="row justify-content-center">
        <div class="col-12">
            @if($questions)
                <div class="text-center mb-3">
                    <a href="{{ route('admin.questions.create', [$surveyId, $sectionId]) }}" class="btn btn-success mr-2 mb-1" >
                        create a new question for this Section
                    </a>
                </div>
                <div id="accordion">
                    @foreach ($questions as $question)
                        <div class="card">
                            <div class="card-header" id="question_{{$loop->index}}">
                                <div class="row">
                                    <div class="col-lg-6 col-12">
                                        <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse{{$loop->index}}" aria-expanded="{{ $loop->first ? 'ture' : 'false'}}" aria-controls="collapse{{$loop->index}}">
                                            question {{$loop->iteration}} - question Order : {{$question->question_order}} - <span class="text-dark"> question Type : {{$question->question_type}} </span>
                                        </button>
                                    </h5></div>
                                    <div class="col-lg-6 col-12 text-lg-right">
                                        <a href="#" class="btn btn-outline-primary ml-2 mb-lg-0 mb-2" data-toggle="modal" data-target="#id_update_question_order_form{{$question->id}}">
                                            Update Question
                                        </a>
                                        {{-- <a href="{{ route('admin.questions.lang.create', [$surveyId, $sectionId, $question->id]) }}" class="btn btn-outline-success ml-2 mb-lg-0 mb-2">
                                            Add Lang
                                        </a> --}}
                                        <a href="{{route('admin.sections.index', $question->surveySection->survey->id)}}" class="btn btn-outline-info ml-2 mb-lg-0 mb-2">
                                            Show Sections
                                        </a>
                                        {!! Form::open(['method'=>'DELEtE', 'action' => ['AdminQuestionController@destroy', $question->surveySection->survey->id,  $question->surveySection->id, $question->id ], 'style'=> 'display:inline']) !!}
                                            {!! Form::submit('Delete Question', ['class'=>'btn btn-outline-danger ml-2 mb-lg-0 mb-2','onclick'=>"return confirm('Silmek isteğinizden Emin misiniz?')"]) !!}
                                        {!! Form::close() !!}
                                    </div>
                                    <!-- Modal For update question Order -->
                                    <div class="modal fade" id="id_update_question_order_form{{$question->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalCenterTitle">Update question</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                </div>
                                                <div class="modal-body">
                                                    {!! Form::model($question, ['method'=>'PATCH', 'action' => ['AdminQuestionController@update', $surveyId, $sectionId, $question->id ], 'style'=> 'display:inline', 'id' => 'update_question_order' . $question->id]) !!}
                                                        <div class="form-group">
                
                                                            {!! Form::label('update_' . $question->id . '_question_order', 'question Order : ') !!}
                                                            {!! Form::number('update_' . $question->id . '_question_order', $question->question_order, ['class'=>'form-control', 'max' => '100', 'min' => '1']) !!}
                                            
                                                        </div>
                                                        @if ( $question->question_type == 'checkbox' || $question->question_type == 'radio' )
                                                        <div class="form-group">
                
                                                            {{-- {!! Form::label('update_' . $question->id . '_question_type', ' Question Type: ') !!}
                                                            {!! Form::select('update_' . $question->id . '_question_type', ['checkbox' => 'checkbox','radio' => 'radio'],
                                                                $question->question_type, ['class'=>'form-control', 'max' => '100', 'min' => '1']) !!} --}}
                                                                <input type="hidden" name ="update_{{$question->id}}_question_type" value="{{$question->question_type}}">
                                            
                                                        </div>
                                                        @else
                                                            <div class="form-group">
                    
                                                                {!! Form::label('update_' . $question->id . '_question_type', ' Question Type: ') !!}
                                                                {!! Form::select('update_' . $question->id . '_question_type', [
                                                                    'text' => 'text',
                                                                    'number' => 'number',
                                                                    'date' => 'date' 
                                                                ],
                                                                    $question->question_type, ['class'=>'form-control', 'max' => '100', 'min' => '1']) !!}
                                                
                                                            </div>
                                                        @endif
                                                    {!! Form::close() !!} 
                                                </div>
                                                <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button class="btn btn-primary" onclick="
                                                    event.preventDefault();
                                                    document.getElementById('update_question_order{{$question->id}}').submit();
                                                ">
                                                Save Changes</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="collapse{{$loop->index}}" class="collapse {{ $loop->first ? 'show' : ''}}" aria-labelledby="question_{{$loop->index}}" data-parent="#accordion">
                                <div class="card-body">
                                    <table class="table table-responsive-lg table-striped">
                                        <thead class="thead-light">
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Question Type</th>
                                            <th scope="col">lang</th>
                                            <th scope="col">title</th>
                                            <th scope="col">options</th>
                                            <th scope="col">process</th>
                                            
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($question->questionLang as $lang)
                                                <tr>
                                                    <td>{{$loop->iteration}}</td>
                                                    <td>{{$question->question_type}}</td>
                                                    <td>{{$lang->language}}</td>
                                                    <td>{{$lang->title}}</td>
                                                    <td>
                                                        @if ( is_array($lang->option_name) )
                                                            <ol>
                                                                @foreach ( $lang->option_name as $key_lv_1 => $value_lv_1 )
                                                                    <li>{{ $key_lv_1 }}</li>
                                                                    @if ( is_array($value_lv_1) )
                                                                        <ul>
                                                                            @foreach ( $value_lv_1 as $key_lv_2 => $value_lv_2 )
                                                                                <li> {{ $key_lv_2 }} : {{ $value_lv_2 }} </li>
                                                                            @endforeach
                                                                        </ul>
                                                                    @else 
                                                                    {{ $value_lv_1 }}
                                                                    @endif
                                                                @endforeach
                                                            </ol>
                                                        @else
                                                            <p class="text-center">****</p>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        {!! Form::open(['method'=>'DELEtE', 'action' => ['AdminQuestionController@destroyLang', $surveyId, $sectionId, $question->id, $lang->id ], 'style'=> 'display:inline']) !!}
                                                            {!! Form::submit('Delete Question Lang', ['class'=>'btn btn-outline-danger ml-2 mb-lg-0 mb-2','onclick'=>"return confirm('Silmek isteğinizden Emin misiniz?')"]) !!}
                                                        {!! Form::close() !!}
                                                    </td>
                                                </tr>
                                            @endforeach
                                            <tr>
                                                <td colspan="6 text-center">
                                                    <div class="text-center mb-3">
                                                        <a href="{{ route('admin.questions.lang.create', [$surveyId, $sectionId, $question->id]) }}" class="btn btn-success mr-2 mb-1" >
                                                           Add New Lang 
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif
        </div>
    </div>
</div>

@endsection

@section('script')
    <script src="https://cdn.ckeditor.com/4.13.0/basic/ckeditor.js"></script>
    <script type="text/javascript">
        $( document ).ready(function() {
            $('textarea').each(function(e){
                CKEDITOR.replace( this.id, {language: '{{ app()->getlocale() }}'});
            });
        });
</script>
@endsection