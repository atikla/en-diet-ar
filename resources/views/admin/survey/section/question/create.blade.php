@extends('layouts.admin')

@section('content')
<div class="mt-3 mx-5">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-dark text-light">Create question</div>
                <div class="card-body ">
                    You are logged in as Admin You are in Create question Section!
                </div>
            </div>
            @include('include.form-errors')
        </div>
        <dic class="col-12 mt-3">
            <div class="card">
                <div class="card-header bg-light text-dark">Question info for default lang = tr</div>
                <div class="card-body ">
                    {!! Form::open(['method'=>'POST', 'action' => ['AdminQuestionController@store', $surveyId, $sectionId], 'files'=>true]) !!}
                    <div class="row">
                        <div class="col-lg-6 col-12">
                            <div class="form-group">
                                                                
                                {!! Form::label('title', 'Question Title : ') !!}
                                {!! Form::text('title', null, ['class'=>'form-control', 'placeholder' => 'Enter Your Question Title', 'required']) !!}
                                
                            </div>
                        </div>
                        <div class="col-lg-6 col-12">
                            <div class="form-group">
                                                                
                                {!! Form::label('question_order', 'Question Order : ') !!}
                                {!! Form::number('question_order', 1, ['class'=>'form-control', 'required']) !!}
                                
                            </div>
                        </div>
                        <div class="col-lg-6 col-12">
                            <div class="form-group">                             
                                {{ Form::label('question_type', 'Question Type : ') }}
                                {{ Form::select('question_type', $selectInput, null, ['class'=>'form-control question_type', 'placeholder'=>'Please select ...', 'required']) }}
                                
                            </div>
                        </div>
                        <div class="col-lg-6 col-12" id="dynamic-input">
                            <div class="form-group" id="form-inputs"></div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                {!! Form::submit('Create Question', ['class'=>'btn btn-primary']) !!}
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </dic>
    </div>
</div>
@endsection

@section('script')
    <script>
        $( document ).ready(function() {
        //     CKEDITOR.replace( 'description', {language: '{{ app()->getlocale() }}'});
        
            $('#question_type').change(function(){

                
                var selected    = $(this).children("option:selected").val();
                //alert('you have selected : ' + selected);
                $('#dynamic-input').children("#form-inputs").each(function(e){
                        $(this).empty();
                });
                if(selected == 'checkbox' || selected == 'radio'){
                    var component   =       '<div class="text-center"><span class="btn btn-success text-light add-field-button my-2">'
                                        +       'Add A Input field'
                                        +   '</div></span>'
                                        +   '<div id="conti-for-inputs">'
                                            +   '<div><div class="form-group mb-2">'
                                                +   '<label for="option_name[choices][]">Option Name : </label>'
                                                +   '<div class="form-check ml-4">'
                                                    +   '<input data="1" type="checkbox" name="option_name[if][1]" class="form-check-input" onclick="check($ ( this ) )">'
                                                    +   '<label>Check it if this option have a conditional answer</label>'
                                                +   '</div>'
                                                +   '<input type="text" name="option_name[choices][1]" required class="form-control">'
                                            +   '</div><hr></div>'
                                        +   '</div>';
                    $('#dynamic-input').children("#form-inputs").append(component);
                    let x = 1; //initlal text box count
                    $('.add-field-button').click(function(e){ //on add input button click
                        e.preventDefault();
                        if(x < 500){ //max input box allowed
                            ++x; //text box increment
                            var input =  '<div><div class="form-group mb-2">'
                                            +   '<label for="option_name[choices][]">Option Name : </label> <a href="#" class="remove-field text-danger">Remove</a>'
                                                +   '<div class="form-check ml-4">'
                                                    +   '<input data="'+ x +'" type="checkbox" class="form-check-input" onclick="check($ ( this ) )">'
                                                    +   '<label>Check it if this option have a conditional answer</label>'
                                                +   '</div>'
                                            +   '<input type="text" name="option_name[choices]['+ x +']" required class="form-control">'
                                        +   '</div><hr></div>';
                            
                            $('#conti-for-inputs').append(input); //add input box
                        }else{
                            alert('max input fiel per time is 5');
                        }
                        $('.form-group').on("click",".remove-field", function(e){ //user click on remove text
                            e.preventDefault(); $(this).parent('div').parent('div').remove();
                        });
                    });
                    
                }else {

                }
            });
            $('#dynamic-input').change(function(){
                // alert('Done : ');
            });
        });

    function check(e) {
        if ( e.prop('checked') == true ) {
            let newInput = '<div class="form-group conditional-'+ e.attr('data') +' mb-2">'
                                +   '<label for="option_name[if]['+ e.attr('data') +']">Conditional Question : </label>'
                                +   '<input type="text" name="option_name[if]['+  e.attr('data') +']" required class="form-control"  style="display:inline;width:30">'
                            + '</div>';
            e.parent().parent().append(newInput);
        }
        else if ( e.prop('checked') == false ) {
            e.parent().parent().children('.conditional-'+ e.attr('data')).remove();
        }
    }

    </script>
@endsection
