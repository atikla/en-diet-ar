@extends('layouts.admin')

@section('content')
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-dark text-light">Edit User</div>

                    <div class="card-body ">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        You are logged in as Admin You are in User Profile Section!   
                    </div>
                </div>
                @include('include.form-errors')
            </div>
        </div>
    </div>
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 text-center">
                <img class="w-75" style="border-radius: 50%;"src="{{$user->photo ? url('/') . '/img/private/' . $user->photo->path : url('/') . '/img/private/default.png'}}" alt="">
            </div>
            <div class="col-lg-9 col-md-6 col-sm-6 col-xs-12 pt-lg-5">
                <div class="card my-3 ">
                    <div class="card-header bg-info text-light">User Informations</div>
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left"><h4><b> Name   : </b> {{$user->name}}</h4></div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left"><h4><b> Email  : </b> {{$user->email}}</h4></div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">
                                <h4>
                                    <b> email_verified_at  : </b> 
                                    {!! $user->email_verified_at == NULL ? '<span class="text-danger">Email Not Verified</span>' : '<span class="text-success">Email Verified at </span>' . $user->email_verified_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss') !!}

                                </h4>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left"><h4><b> address   : </b> {{$user->address}}</h4></div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left"><h4><b> phone  : </b> {{$user->phone}}</h4></div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left"><h4><b> UserGender  : </b> {{$user->UserGender}}</h4></div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left"><h4><b> vegetarian  : </b> {{$user->vegetarian == 0 ? 'NO' : 'YES'}}</h4></div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left"><h4><b>created_at : </b> {{$user->created_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}</h4></div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left"><h4><b>updated_at : </b> {{$user->updated_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}</h4></div>
                            <div class="col-lg-12 col-md-6 col-sm-12 col-xs-12 text-center">
                                <a   href="{{route('admin.users.edit', $user->id)}}"	class="btn btn-outline-success update mr-2 mb-1">Update</a>    
                                {!! Form::open(['method'=>'DELETE', 'action' => ['AdminUsersController@destroy', $user->id], 'style'=> 'display:inline']) !!}
                                    {!! Form::submit('Delete', ['class'=>'btn btn-outline-danger mr-2 mb-1','onclick'=>"return confirm('Silmek isteğinizden Emin misiniz?')"]) !!}
                                {!! Form::close() !!} 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card my-3 ">
                    <div class="card-header bg-info text-light">User Kits</div>
                    <div class="card-body ">
                        <div class="text-right mb-3">
                            <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#add_kit_to_user">
                                Add Kit To This use
                            </button>
                        </div>
                        @if ($user->kits->count() > 0 )
                            <table class="table table-bordered table-responsive-lg">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>kit code</th>
                                    <th>Registered at</th>
                                    <th>kit status</th>
                                    <th>Order</th>
                                    <th>process</th>
                                    <th>Dislink this kit</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ($user->kits as $kit)
                                        <tr>
                                            <td>{{$kit->id}}</td>
                                            @if ($kit->kit_code)
                                                <td>{{$kit->kit_code}}</td>
                                            @else
                                                <td>
                                                    <a href="{{route('admin.kits.show', $kit->id)}}" class="btn btn-primary text-light" >ُEnter Kit Code</a>
                                                </td>
                                            @endif
                                            
                                            <td>{{$kit->registered_at ? $kit->registered_at->isoFormat('DD MMMM Y') : '*-*-*'}}</td>
                                            <td>{{$kit->kitStatuse}}</td>
                                            @if ($kit->OrderDetail)
                                                <td> <a href="{{ route('admin.orders.show', $kit->OrderDetail->order->id) }}"> Show Order</a></td>
                                            @else
                                                <td>Dont Have a Order ( sold offline ) </td>
                                            @endif
                                            <td>
                                                <div class="btn-group" role="group" aria-label="Basic example">
                                                    <button data="{{$kit->kit_code}}" data1="{{$kit->id}}" type="button" class="btn btn-secondary delever" {{$kit->kit_status == 0 && $kit->kit_code ? '' : 'disabled'}}>Delever to user</button>
                                                    <button data="{{$kit->kit_code}}" data1="{{$kit->id}}" type="button" class="btn btn-info text-light received" {{$kit->kit_status == 1 && $kit->kit_code ? '' : 'disabled'}}>received from user</button>
                                                    <button data="{{$kit->kit_code}}" data1="{{$kit->id}}" type="button" class="btn btn-warning text-dark sent" {{$kit->kit_status == 2 && $kit->kit_code ? '' : 'disabled'}}>sent to lab</button>
                                                    <button data="{{$kit->kit_code}}" data1="{{$kit->id}}" type="button" class="btn btn-success text-light upload" {{$kit->kit_status == 3 && $kit->kit_code ? '' : 'disabled'}}>upload results</button>
                                                    <a href="{{route('admin.kits.show', $kit->id)}}" class="btn btn-secondary text-light" >Show Kit Details</a>
                                                </div>
                                            </td>
                                            <td>
                                                {!! Form::open(['method'=>'POST', 'action' => ['AdminKitsController@dislink', $kit->id], 'style'=> 'display:inline']) !!}
                                                    {!! Form::submit('Dislink this Kit to this user', ['class'=>'btn btn-outline-danger mr-2 mb-1','onclick'=>"return confirm('Emin misiniz?')"]) !!}
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else 
                            <span class="text-danger" >This user dont have any kit</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- Modal -->
<div class="modal fade" id="add_kit_to_user" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Add Kit To User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {!! Form::open(['method'=>'POST', 'action' => ['AdminUsersController@link', $user->id], 'style'=> 'display:inline', 'id' => 'add_kit_to_user_form']) !!}
                    
                    <div class="form-group">
                                                
                        {{ Form::label('Select User : ') }}
                        {{ Form::select('kit', $kits, null, array('class'=>'form-control', 'placeholder'=>'Please select ...')) }}
                        
                    </div>

                {!! Form::close() !!} 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" onclick="
                    event.preventDefault();
                    document.getElementById('add_kit_to_user_form').submit();
                ">Save Changes
                </button>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    $(document).ready(function(){

        $('.delever').click(function(){
            $.ajax({
                url: '{{ route('admin.kits.delever') }}',
                method: "post",
                data: {_token: '{{ csrf_token() }}', kit_code: $( this ).attr('data'), kit_id:$( this ).attr('data1')},
                success: function (response) {
                    window.location.reload();
                },
                error: function (response) {
                    window.location.reload();
                }
            });
        });
        $('.received').click(function(){
            $.ajax({
                url: '{{  route('admin.kits.received') }}',
                method: "post",
                data: {_token: '{{ csrf_token() }}', kit_code: $( this ).attr('data'), kit_id:$( this ).attr('data1')},
                success: function (response) {
                    window.location.reload();

                },
                error: function (response) {
                    window.location.reload();
                }
            });
        });

        $('.sent').click(function(){
            $.ajax({
                url: '{{  route('admin.kits.sent') }}',
                method: "post",
                data: {_token: '{{ csrf_token() }}', kit_code: $( this ).attr('data'), kit_id:$( this ).attr('data1')},
                success: function (response) {

                    window.location.reload();
                },
                error: function (response) {

                    window.location.reload();
                }
            });
        });
        $('.upload').click(function(){
            window.location.href = '{{route('admin.kits.index')}}'
        });
    });
</script>
@endsection