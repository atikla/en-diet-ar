@extends('layouts.admin')
@section('content')
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-dark text-light">Users</div>

                    <div class="card-body ">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        You are logged in as Admin You are in Users Section!
                    </div>
                </div>
                @include('include.messages')
                @include('include.form-errors')
            </div>
        </div>
    </div>
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="text-right">
                    <a  href="{{route('admin.users.create')}}" class="header-table btn btn-outline-success mb-2">Create User</a>
                    <a  href="{{route('admin.users.trash')}}" class="header-table btn btn-outline-danger mb-2">User trash</a>
                    {!! Form::open(['method'=>'POST', 'action' => ['AdminUsersController@search'], 'style'=> 'display:inline']) !!}

                        {!! Form::label('search', 'Query:') !!}
                        {!! Form::text('search', null, ['class'=>'form-control', 'style'=> 'display:inline; width:initial', 'required']) !!}

                        {!! Form::submit('search', ['class'=>'btn btn-outline-success mr-2 mb-1']) !!}

                    {!! Form::close() !!} 
                </div>
                <table class="table table-responsive-lg table-striped">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Photo</th>
                        <th scope="col">Name</th>
                        <th scope="col">Gender</th>
                        <th scope="col">date Of Birth</th>
                        <th scope="col">Email</th>
                        <th scope="col">Status</th>
                        <th scope="col">Adress</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Created</th>
                        {{-- <th scope="col">Updated</th> --}}
                        <th scope="col">process</th>
                    </tr>
                    </thead>
                    <tbody>
                        @if ($users)
                            @foreach ($users as $user)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td><img class='img-rounded' height="50" src="{{$user->photo ? url('/') . '/img/private/' . $user->photo->path : url('/') . '/img/private/default.png'}}" alt=""></td>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->userGender}}</td>
                                    <td>{!! $user->date_of_birth ? $user->age . ' year ' : '<span class="text-danger"> Not Entered </span>'!!}</td>
                                    <td>{{$user->email}}</td>
                                    {!! $user->email_verified_at == NULL ? '<td class="text-danger">Email Not Verified</td>' : '<td class="text-success">Email Verified</td>' !!}
                                    <td>{{$user->address}}</td>
                                    <td>{{$user->phone}}</td>
                                    <td>{{$user->created_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}</td>
                                    {{-- <td>{{$user->updated_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}</td> --}}
                                    <td>
                                        <a href="{{route('admin.users.show', $user->id)}}" class="btn btn-outline-info mr-2 mb-1" >Show Profile</a>
                                        <a   href="{{route('admin.users.edit', $user->id)}}"	class="btn btn-outline-success update mr-2 mb-1">Update</a>
                                        {!! Form::open(['method'=>'DELETE', 'action' => ['AdminUsersController@destroy', $user->id], 'style'=> 'display:inline']) !!}
                                            {!! Form::submit('Delete', ['class'=>'btn btn-outline-danger mr-2 mb-1','onclick'=>"return confirm('Silmek isteğinizden Emin misiniz?')"]) !!}
                                        {!! Form::close() !!} 
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
            <div class="text-center my-4">
                {{$users->links()}}
            </div>
        </div>
    </div>
@endsection
