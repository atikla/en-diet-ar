@extends('layouts.admin')

@section('content')
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-dark text-light">Edit User</div>

                    <div class="card-body ">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        You are logged in as Admin You are in Edit User Section!   
                    </div>
                </div>
                @include('include.form-errors')
            </div>
        </div>
    </div>
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 text-center">
                <img class="w-75" style="border-radius: 10%;"src="{{$user->photo ? url('/') . '/img/private/' . $user->photo->path : url('/') . '/img/private/default.png'}}" alt="">
            </div>
            <div class="col-lg-9 col-md-6 col-sm-6 col-xs-12">
                {!! Form::model($user, ['method'=>'PATCH', 'action' => ['AdminUsersController@update', $user->id], 'files'=>true]) !!}

            <div class="form-group">

                {!! Form::label('name', 'Name:') !!}
                {!! Form::text('name', null, ['class'=>'form-control']) !!}

            </div>
            <div class="form-row">
                <div class="form-group col-lg-6">
            
                    {!! Form::label('email', 'Email:') !!}
                    {!! Form::email('email', null, ['class'=>'form-control']) !!}
                </div>
                @if (!$user->email_verified_at)
                    <div class="form-group col-lg-6">
                
                        {!! Form::label('email_verified_at', 'Email Verified:') !!} 
                        <div class="ml-2">
                            <input type='checkbox' id='checkbox' class="de-form-radio ml-5" name='email_verified_at' value="1" style="margin-right:5px;" /> YES
                        </div>
                    </div>
                @endif
            </div>
            <div class="form-group">
            
                {!! Form::label('address', 'Address:') !!}
                {!! Form::text('address', null, ['class'=>'form-control', 'row' => '3']) !!}
        
            </div>

            <div class="form-row">

                <div class="form-group col-lg-6">
            
                    {!! Form::label('phone', 'Phone:') !!}
                    {!! Form::text('phone', null, ['class'=>'form-control']) !!}
            
                </div>
                <div class="form-group col-lg-6">
            
                    {!! Form::label('photo_id', 'photo:') !!} <br>
                    {!! Form::file('photo_id', []) !!}
                    
                </div>

            </div>
            <div class="form-row">

                <div class="form-group col-lg-6">

                    {!! Form::label('date_of_birth', 'Date Of Birth:') !!}
                    {!! Form::date('date_of_birth', $user->date_of_birth, ['class'=>'form-control']) !!}

                </div>

                <div class="form-group col-lg-6">

                    {!! Form::label('gender', 'Gender:') !!}
                    {!! Form::select('gender', [ 1 => __('app.erkek'), 2 => __('app.kadin')], null, ['class'=>'form-control', 'placeholder'=>'Please select ...']) !!}

                </div>

            </div>

            <div class="form-row">

                <div class="form-group col-lg-6">

                    {!! Form::label('password', 'Password:') !!}
                    {!! Form::password('password', ['class'=>'form-control', 'autocomplete' =>'on']) !!}
                    
                </div>

                <div class="form-group col-lg-6">

                        {!! Form::label('password_confirmation', 'Password Confirmation:') !!}
                        {!! Form::password('password_confirmation', ['class'=>'form-control', 'autocomplete' =>'on']) !!}
                        
                </div>

            </div>

            <div class="form-group">
                {!! Form::submit('Update User', ['class'=>'btn btn-primary']) !!}
            </div>
            {!! Form::close() !!}

            {!! Form::open(['method'=>'DELETE', 'action' => ['AdminUsersController@destroy', $user->id]]) !!}
                <div class="form-group">
                    {!! Form::submit('Delete User', ['class'=>'btn btn-danger']) !!}
                </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection