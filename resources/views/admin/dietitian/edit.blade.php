@extends('layouts.admin')

@section('content')
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-dark text-light">Edit dietitian</div>

                    <div class="card-body ">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        You are logged in as Admin You are in Edit dietitian Section!   
                    </div>
                </div>
                @include('include.form-errors')
            </div>
        </div>
    </div>
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 text-center">
                <img class="w-75" style="border-radius: 10%;"src="{{$dietitian->photo ? url('/') . '/img/private/' . $dietitian->photo->path : url('/') . '/img/private/default.png'}}" alt="">
            </div>
            <div class="col-lg-9 col-md-6 col-sm-6 col-xs-12">
                {!! Form::model($dietitian, ['method'=>'PATCH', 'action' => ['AdminDietitiansController@update', $dietitian->id], 'files'=>true]) !!}

                    <div class="form-row">

                        <div class="form-group col-lg-6">
                    
                            {!! Form::label('name', 'Name:') !!}
                            {!! Form::text('name', null, ['class'=>'form-control']) !!}
                    
                        </div>
                    
                        <div class="form-group col-lg-6">
                    
                            {!! Form::label('email', 'Email:') !!}
                            {!! Form::email('email', null, ['class'=>'form-control']) !!}
                            
                        </div>

                    </div>

                    <div class="form-row">

                        <div class="form-group  col-lg-6">

                            {!! Form::label('city_id', 'City:') !!}
                            {{ Form::select('city_id', $cities, null, ['class'=>'form-control ', 'required']) }}

                        </div>

                        <div class="form-group  col-lg-6">
                    
                            {!! Form::label('address', 'Address:') !!}
                            {!! Form::text('address', null, ['class'=>'form-control', 'row' => '3']) !!}
                    
                        </div>

                    </div>

                    <div class="form-row">

                        <div class="form-group col-lg-6">
                
                            {!! Form::label('type', 'Type:') !!}
                            {{ Form::select('type', $types, null, ['class'=>'form-control ', 'required']) }}
                    
                        </div>

                        <div class="form-group  col-lg-6">
                    
                            {!! Form::label('reference_code', 'Reference Code') !!}
                            {!! Form::text('reference_code', null, ['class'=>'form-control']) !!}
                    
                        </div>

                    </div>

                    <div class="form-row">


                        <div class="form-group col-lg-6">
                    
                            {!! Form::label('photo_id', 'Photo:') !!}<br>
                            {!! Form::file('photo_id', []) !!}
                            
                        </div>

                        <div class="form-group  col-lg-6">
                        
                            {!! Form::label('phone', 'Phone:') !!}
                            {!! Form::text('phone', null, ['class'=>'form-control']) !!}
                    
                        </div>

                    </div>

                    <div class="form-row">

                        <div class="form-group col-lg-6">
                    
                            {!! Form::label('password', 'Password:') !!}
                            {!! Form::password('password', ['class'=>'form-control', 'autocomplete' =>'on']) !!}
                            
                        </div>
                        <div class="form-group col-lg-6">
                    
                                {!! Form::label('password_confirmation', 'Password Confirmation:') !!}
                                {!! Form::password('password_confirmation', ['class'=>'form-control', 'autocomplete' =>'on']) !!}
                                
                        </div>

                    </div> 
        
                    <div class="form-group">
                        {!! Form::submit('Update dietitian', ['class'=>'btn btn-primary']) !!}
                    </div>
                {!! Form::close() !!}

                {!! Form::open(['method'=>'DELETE', 'action' => ['AdminDietitiansController@destroy', $dietitian->id]]) !!}
                    <div class="form-group">
                        {!! Form::submit('Delete dietitian', ['class'=>'btn btn-danger']) !!}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection