@extends('layouts.admin')

@section('content')
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-dark text-light">Create Dietitian</div>

                    <div class="card-body ">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        You are logged in as Admin You are in Create Dietitian Section!
                    </div>
                </div>
                @include('include.form-errors')
            </div>
        </div>
    </div>
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                {!! Form::open(['method'=>'POST', 'action' => 'AdminDietitiansController@store', 'files'=>true]) !!}

                    <div class="form-row">

                        <div class="form-group col-lg-6">
                    
                            {!! Form::label('name', 'Name:') !!}
                            {!! Form::text('name', null, ['class'=>'form-control']) !!}
                    
                        </div>
                    
                        <div class="form-group col-lg-6">
                    
                            {!! Form::label('email', 'Email:') !!}
                            {!! Form::email('email', null, ['class'=>'form-control']) !!}
                            
                        </div>

                    </div>
                    <div class="form-row">
                        <div class="form-group  col-lg-6">

                            {!! Form::label('city_id', 'City:') !!}
                            {{ Form::select('city_id', $cities, 40, ['class'=>'form-control ', 'required']) }}

                        </div>

                        <div class="form-group  col-lg-6">
                    
                            {!! Form::label('address', 'Address:') !!}
                            {!! Form::text('address', null, ['class'=>'form-control', 'row' => '3']) !!}
                    
                        </div>

                    </div>

                    <div class="form-row">

                        <div class="form-group col-lg-6">
                
                            {!! Form::label('type', 'Type:') !!}
                            {{ Form::select('type', $types, 0, ['class'=>'form-control ', 'required']) }}
                    
                        </div>

                        <div class="form-group  col-lg-6">
                    
                            {!! Form::label('reference_code', 'Reference Code') !!}
                            {!! Form::text('reference_code', null, ['class'=>'form-control']) !!}
                    
                        </div>

                    </div>

                    <div class="form-row">


                        <div class="form-group col-lg-6">
                    
                            {!! Form::label('photo_id', 'Photo:') !!}<br>
                            {!! Form::file('photo_id', []) !!}
                            
                        </div>

                        <div class="form-group  col-lg-6">
                        
                            {!! Form::label('phone', 'Phone:') !!}
                            {!! Form::text('phone', null, ['class'=>'form-control']) !!}
                    
                        </div>

                    </div>

                    <div class="form-row">

                        <div class="form-group col-lg-6">
                    
                            {!! Form::label('password', 'Password:') !!}
                            {!! Form::password('password', ['class'=>'form-control', 'autocomplete' =>'on']) !!}
                            
                        </div>
                        <div class="form-group col-lg-6">
                    
                                {!! Form::label('password_confirmation', 'Password Confirmation:') !!}
                                {!! Form::password('password_confirmation', ['class'=>'form-control', 'autocomplete' =>'on']) !!}
                                
                        </div>

                    </div> 
        
                    <div class="form-group">
                        {!! Form::submit('Create User', ['class'=>'btn btn-primary']) !!}
                    </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection