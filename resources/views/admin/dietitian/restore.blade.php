@extends('layouts.admin')

@section('content')
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-dark text-light">dietitians</div>

                    <div class="card-body ">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        You are logged in as Admin You are in dietitians Trash Section!
                    </div>
                </div>
                @include('include.messages')
            </div>
        </div>
    </div>
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="text-right">
                    <a  href="{{route('admin.dietitians.create')}}" class="header-table btn btn-outline-success mb-2">Create dietitian</a>
                    <a  href="{{route('admin.dietitians.index')}}" class="header-table btn btn-outline-danger mb-2">dietitian</a>
                </div>
                <table class="table table-responsive-lg table-striped">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Photo</th>
                            <th scope="col">Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Status</th>
                            <th scope="col">Type</th>
                            <th scope="col">City</th>
                            <th scope="col">Adress</th>
                            <th scope="col">Phone</th>
                            <th scope="col">Created</th>
                            <th scope="col">Updated</th>
                            <th scope="col">process</th>
                        </tr>
                        </thead>
                        <tbody>
                            @if ($dietitians)
                                @foreach ($dietitians as $dietitian)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td><img class='img-rounded' height="50" src="{{$dietitian->photo ? url('/') . '/img/private/' . $dietitian->photo->path : url('/') . '/img/private/default.png'}}" alt=""></td>
                                        <td>{{$dietitian->name}}</td>
                                        <td>{{$dietitian->email}}</td>
                                        {!! $dietitian->email_verified_at == NULL ? '<td class="text-danger">Email Not Verified</td>' : '<td class="text-info">Email Verified</td>' !!}
                                        <td>{{$dietitian->kind}}</td>
                                        <td>{{$dietitian->city ? $dietitian->city->city : '***'}}</td>
                                        <td>{{$dietitian->address}}</td>
                                        <td>{{$dietitian->phone}}</td>
                                        <td>{{$dietitian->created_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}</td>
                                        <td>{{$dietitian->updated_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}</td>
                        <td>
                            <a   href="{{route('admin.dietitians.restore', $dietitian->id)}}"	class="btn btn-outline-success update mr-2 mb-1">Restore</a>   
                            {!! Form::open(['method'=>'DELETE', 'action' => ['AdminDietitiansController@destroy', $dietitian->id, 'soft' => '1'], 'style'=> 'display:inline']) !!}
                                {!! Form::submit('Delete Permanently', ['class'=>'btn btn-outline-danger mr-2 mb-1','onclick'=>"return confirm('Silmek isteğinizden Emin misiniz?')"]) !!}
                            {!! Form::close() !!} 
                        </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
    <div class="col-lg-12 text-center my-4">
        {{$dietitians->links()}}
    </div>
</div>
</div>
@endsection