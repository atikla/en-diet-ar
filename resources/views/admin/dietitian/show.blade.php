@extends('layouts.admin')

@section('content')
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-dark text-light">Edit dietitian</div>

                    <div class="card-body ">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        You are logged in as Admin You are in dietitian Profile Section!   
                    </div>
                </div>
                @include('include.form-errors')
            </div>
        </div>
    </div>
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 text-center">
                <img class="w-75" style="border-radius: 50%;"src="{{$dietitian->photo ? url('/') . '/img/private/' . $dietitian->photo->path : url('/') . '/img/private/default.png'}}" alt="">
            </div>
            <div class="col-lg-9 col-md-6 col-sm-6 col-xs-12 pt-lg-5">
                <div class="card my-3 ">
                    <div class="card-header bg-info text-light">dietitian Informations</div>
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left"><h4><b> Name   : </b> {{$dietitian->name}}</h4></div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left"><h4><b> Type   : </b> {{$dietitian->kind}}</h4></div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left"><h4><b> Email  : </b> {{$dietitian->email}}</h4></div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left"><h4><b> status  : </b> {!! $dietitian->email_verified_at == NULL ? '<span class="text-danger">Email Not Verified</span>' : '<span class="text-info">Email Verified</span>' !!}</h4></div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left"><h4><b> reference code  : </b> {{$dietitian->reference_code}}</h4></div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left"><h4><b> City  : </b> {{$dietitian->city ? $dietitian->city->city : '---'}}</h4></div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left"><h4><b> Address  : </b> {{$dietitian->address}}</h4></div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left"><h4><b>created_at : </b> {{$dietitian->created_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}</h4></div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left"><h4><b>updated_at : </b> {{$dietitian->updated_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}</h4></div>
                            <div class="col-lg-12 col-md-6 col-sm-12 col-xs-12 text-center">
                                <a   href="{{route('admin.dietitians.edit', $dietitian->id)}}"	class="btn btn-outline-success update mr-2 mb-1">Update</a>    
                                {!! Form::open(['method'=>'DELETE', 'action' => ['AdminDietitiansController@destroy', $dietitian->id], 'style'=> 'display:inline']) !!}
                                    {!! Form::submit('Delete', ['class'=>'btn btn-outline-danger mr-2 mb-1','onclick'=>"return confirm('Silmek isteğinizden Emin misiniz?')"]) !!}
                                {!! Form::close() !!} 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card my-3 ">
                    <div class="card-header bg-info text-light">Team Informations</div>
                    <div class="card-body ">
                        @if ($dietitian->sellingTeam)
                            <p> team leadar: {{$dietitian->isLeader ? 'Yes' : 'No'}} </p>
                            <p> team name: {{$dietitian->sellingTeam->name}} </p>
                            <p> team Members conut: {{$dietitian->sellingTeam->dietitians()->count()}} </p>
                            <a href="{{route('admin.selling-teams.show', $dietitian->sellingTeam->id)}}" class="btn btn-outline-info mr-2 mb-1" >Show</a>
                        @else
                        <div class="text-center text-danger">
                            <h4>
                                DONT BELONGS TO ANY TEAM
                            </h4> 
                        </div>
                        @endif
                        <p></p>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="card my-3 ">
                    <div class="card-header bg-info text-light">Kits Informations</div>
                    <div class="card-body ">
                        <div class="text-center mb-2">
                            <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#add_kit_to_dietitian">
                                Add  kit to This dietitian
                            </button>
                        </div>
                        @if (!$dietitian->kits->isEmpty())
                        <table class="table table-responsive-lg table-striped">
                            <thead class="thead-light">
                            <tr>
                            <th>#</th>
                            <th>Kit Code</th>
                            <th>process</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach ($dietitian->kits as $kit)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$kit->kit_code}}</td>
                                    <td>
                                        <a href="{{route('admin.kits.show', $kit->id)}}" class="btn btn-outline-info mr-2 mb-1" >Show Kit Details</a>
                                        {!! Form::open(['method'=>'POST', 'action' => ['AdminKitsController@dietitianDislink', $kit->id], 'style'=> 'display:inline']) !!}
                                            {!! Form::submit('Dislink', ['class'=>'btn btn-outline-danger mr-2 mb-1','onclick'=>"return confirm('Silmek isteğinizden Emin misiniz?')"]) !!}
                                        {!! Form::close() !!} 
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @else
                        <div class="text-center text-danger">
                            <h4>
                                DONT BELONGS TO ANY Kit
                            </h4> 
                        </div>
                        @endif
                        <p></p>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="card my-3">
                    <div class="card-header bg-info text-light">Orders</div>
                    <div class="card-body">
                        @if ($dietitian->orders->count() > 0)
                            <table class="table table-responsive-lg table-striped">
                                <thead class="thead-light">
                                <tr>
                                <th>#</th>
                                <th>Traking Number</th>
                                <th>name</th>
                                <th>address</th>
                                <th>Created</th>
                                <th>process</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ($dietitian->orders as $order)
                                    <tr>
                                        <td>{{$order->id}}</td>
                                        <td>{{$order->tracking}}</td>
                                        <td>{{$order->name}}</td>
                                        <td>{{$order->address}} - {{$order->city ? $order->city->city : ''}} - {{ $order->county ? $order->county->county : '' }}</td>
                                        <td>{{$order->updated_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}</td>
                                        <td>
                                            <a href="{{route('admin.orders.show', $order->id)}}" class="btn btn-outline-info mr-2 mb-1" >Show Order Details</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="text-center text-danger">
                                <h4>
                                    DONT HAVE ANY ORDER
                                </h4> 
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- Modal -->
<div class="modal fade" id="add_kit_to_dietitian" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Add dietitian To This Kit</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {!! Form::open(['method'=>'POST', 'action' => ['AdminDietitiansController@kitLink', $dietitian->id], 'style'=> 'display:inline', 'id' => 'add_dietitian_to_kit_form']) !!}
                    
                    <div class="form-group">
                                                
                        {{ Form::label('Select kit : ') }}
                        {{ Form::select('kit', $kits, null, array('class'=>'form-control', 'placeholder'=>'Please select ...')) }}
                        
                    </div>

                {!! Form::close() !!} 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" onclick="
                    event.preventDefault();
                    document.getElementById('add_dietitian_to_kit_form').submit();
                ">Save Changes
                </button>
            </div>
        </div>
    </div>
</div>
@endsection