@extends('layouts.admin')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-dark text-light">Errors</div>

                    <div class="card-body ">
                        You are logged in as Admin You are in Error Section!
                    </div>
                </div>
                @include('include.messages')
                @include('include.form-errors')
            </div>
        </div>
    </div>
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="text-right">
                    {!! Form::open(['method'=>'POST', 'action' => ['AdminErrorsController@search'], 'style'=> 'display:inline']) !!}

                        {!! Form::label('search', 'Query:') !!}
                        {!! Form::text('search', null, ['class'=>'form-control', 'style'=> 'display:inline; width:initial', 'required']) !!}

                        {!! Form::submit('search', ['class'=>'btn btn-outline-success mr-2 mb-1']) !!}

                    {!! Form::close() !!}
                    <a href="{{ route('admin.errors.index') }}"  class="header-table btn btn-outline-success mb-2"> Go To Errors Index</a>
                </div>
                <table class="table table-responsive-lg table-striped">
                    <thead class="thead-light">
                    <tr>
                        <th>#</th>
                        <th>model</th>
                        <th>Show</th>
                        <th>time</th>
                    </tr>
                    </thead>
                    <tbody>
                        @if ($paymentErrors)
                            @foreach ($paymentErrors as $paymentError)
                            <tr>
                                <td>{{$paymentError->id}}</td>
                                <td>{{$paymentError->model}}</td>
                                <td>
                                <button type="button" class="btn btn-outline-success update mr-2 mb-1" data-toggle="modal" data-target="#show_{{$paymentError->id}}">
                                        Show 
                                    </button>
                                </td>
                                <td>{{$paymentError->created_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}</td>
                                <div class="modal fade  bd-example-modal-lg" id="show_{{$paymentError->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterTitle">{{$paymentError->model}} - {{$paymentError->id}}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                                <ol>
                                                    @foreach ($paymentError->json_string as $key => $item)
                                                        <li>{{$key}} :  {{$item}}</li>
                                                    @endforeach
                                                </ol>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
