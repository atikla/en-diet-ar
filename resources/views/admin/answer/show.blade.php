@extends('layouts.admin')
@section('content')
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-dark text-light">Kits</div>

                    <div class="card-body ">
                        You are logged in as Admin You are in Survey Kit Answer Section!
                    </div>
                </div>
                @include('include.messages')
                @include('include.form-errors')
            </div>
        </div>
    </div>
    <div class="col-lg-8 col-12  mt-5 px-5 my-5">
        <div id="accordion">
            @foreach ($survey->surveySection as $section)
                <div class="card">
                    <div class="card-header" id="heading{{$section->id}}">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapse{{$section->id}}" aria-expanded="true" aria-controls="collapse{{$section->id}}">
                                Section - {{$loop->iteration}} -- section title : {{ $section->DefaultLanguage->title }}
                            </button>
                        </h5>
                    </div>

                    <div id="collapse{{$section->id}}" class="collapse {{$loop->first ? 'show': ''}}" aria-labelledby="heading{{$section->id}}" data-parent="#accordion">
                        <div class="card-body text-left">
                            
                            @foreach ($section->question as $question)
                                @if ($kit->answer->contains('question_id', $question->id))
                                    @php
                                        $default = $question->defaultLanguage;
                                    @endphp
                                    @if ($default->option_name)
                                        <div class="row  mb-4">
                                            <div class="col-6-lg col-12 mb-lg-2">{{$loop->iteration}}) question : {{$default->title}}</div>
                                            <div class="col-6-lg col-12 mb-lg-2 ml-3">Answer : 
                                            @php 
                                                $options = $default->option_name;
                                                $answer = $kit->answer->where('question_id', '=', $question->id)->first()->answer;
                                                
                                                foreach ($options as $key => $value) {
                                                    
                                                    $checked = '';
                                                    $if = '';
                                                    
                                                    foreach ($answer as  $nestedValue) {
                                                        if ( array_key_exists( '"'. $key . '"', $nestedValue) ) {
                                                            $checked = 'checked';
                                                            if ( $nestedValue['"'. $key . '"'] )
                                                                $if = '( kosullu cevab - ' . $nestedValue['"'. $key . '"'] .' )';
                                                        }
                                                    }
                                                    if($question->question_type == 'checkbox'){
                                                        echo '
                                                            <div class="form-check ml-3">
                                                                <input class="form-check-input" type="checkbox"' . $checked . ' disabled >
                                                                <label class="form-check-label" for="defaultCheck1">
                                                                    ' . $value['title'] . ' - ' . $if . '
                                                                </label>
                                                            </div>
                                                        ';
                                                    }else{
                                                        echo '
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" name="__question' . $question->id . '"' . $checked . ' disabled>
                                                                <label class="form-check-label" for="exampleRadios1">
                                                                    ' . $value['title'] . ' - ' . $if . '
                                                                </label>
                                                            </div>
                                                        ';
                                                    }
                                                }
                                            @endphp
                                            </div>
                                        </div>
                                    @else
                                    <div class="row  mb-4">
                                        <div class="col-6-lg col-12 mb-lg-2">{{$loop->iteration}}) question : {{$default->title}}</div>
                                        <div class="col-6-lg col-12 mb-lg-2 ml-3">Answer : {!! $kit->answer->where('question_id', '=', $question->id)->first()->answer !!}</div>
                                    </div>
                                    
                                    
                                    @endif
                                @else

                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection

@section('script')

@endsection