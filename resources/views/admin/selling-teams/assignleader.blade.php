@extends('layouts.admin')
@section('content')
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-dark text-light">Selling Teams</div>

                    <div class="card-body ">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        You are logged in as Admin You are in assign leader for selling team Section!
                    </div>
                </div>
                @include('include.messages')
                @include('include.form-errors')
            </div>
        </div>
    </div>
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="text-right">
                    <a  href="{{route('admin.selling-teams.index')}}" class="header-table btn btn-outline-success mb-2">Selling Teams</a>
                </div>
                {!! Form::open(['method'=>'POST', 'action' => ['AdminSellingTeamsController@storeLeader', $team->id]]) !!}
                
                    <div class="form-group">
                        
                        {!! Form::label('members[]', 'Assign New Leader:') !!}
                        {!! Form::select('leader_id', $dietitians, null, ['class'=>'form-control', 'placeholder'=>'Please select ...']) !!}
                        
                    </div>

                    <div class="form-group">
                        {!! Form::submit('Assign Leader', ['class'=>'btn btn-primary']) !!}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('script')
 
@endsection