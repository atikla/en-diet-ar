@extends('layouts.admin')
@section('content')
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-dark text-light">Selling Teams</div>

                    <div class="card-body ">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        You are logged in as Admin You are in Add New Members for selling team Section!
                    </div>
                </div>
                @include('include.messages')
                @include('include.form-errors')
            </div>
        </div>
    </div>
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="text-right">
                    <a  href="{{route('admin.dietitians.create')}}" class="header-table btn btn-outline-success mb-2">Create {{ $team->kind }}</a>
                    <a  href="{{route('admin.selling-teams.show', $team->id)}}" class="header-table btn btn-outline-success mb-2">Show Selling Team</a>
                </div>
                {!! Form::open(['method'=>'POST', 'action' => ['AdminSellingTeamsController@storeMember', $team->id]]) !!}
                
                    <div class="form-group">
                        
                        {!! Form::label('members[]', 'Add New Members:') !!}
                        {!! Form::select('members[]', $dietitians, null, ['class'=>'form-control', 'multiple']) !!}
                        
                    </div>

                    <div class="form-group">
                        {!! Form::submit('Add Member', ['class'=>'btn btn-primary']) !!}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('script')
 
@endsection