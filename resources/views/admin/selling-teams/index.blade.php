@extends('layouts.admin')
@section('content')
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-dark text-light">Selling Teams</div>

                    <div class="card-body ">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        You are logged in as Admin You are in Selling Teams Section!
                    </div>
                </div>
                @include('include.messages')
                @include('include.form-errors')
            </div>
        </div>
    </div>
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="text-right">
                    <a  href="{{route('admin.selling-teams.create')}}" class="header-table btn btn-outline-success mb-2">Create Team</a>
                </div>
                <table class="table table-responsive-lg table-striped">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Leader</th>
                        <th scope="col">Note</th>
                        <th scope="col">Members conut</th>
                        <th scope="col">cities</th>
                        <th scope="col">Created</th>
                        <th scope="col">process</th>
                    </tr>
                    </thead>
                    <tbody>
                        @if ($teams)
                            @foreach ($teams as $team)
                                <tr>
                                    <td>{{$team->id}}</td>
                                    <td>{{$team->name}}</td>
                                    <td>{!! $team->Leader ? $team->Leader->name . ' - ' . $team->Leader->reference_code : '<a href="' . route('admin.selling-teams.assign-leader.create', $team->id) . '" class="header-table btn btn-outline-danger"> Assign a Leader </a>'!!}</td>
                                    <td>{!! $team->note ? $team->note : '<span class="text-danger"> Not Entered </span>' !!}</td>
                                    <td>{{$team->dietitians()->count()}}</td>
                                    <td>{!! $team->cities ? $team->cities->count() : '<span class="text-danger"> Not Entered </span>' !!}</td>
                                    <td>{{$team->created_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}</td>
                                    <td>
                                        <a href="{{route('admin.selling-teams.show', $team->id)}}" class="btn btn-outline-info mr-2 mb-1" >Show</a>
                                        <a href="#" data-toggle="modal" data-target="#update_{{ $team->id }}" class="btn btn-outline-success mr-2 mb-1" >Update</a>
                                        {!! Form::open(['method'=>'DELETE', 'action' => ['AdminSellingTeamsController@destroy', $team->id], 'style'=> 'display:inline']) !!}
                                            {!! Form::submit('Delete', ['class'=>'btn btn-outline-danger mr-2 mb-1','onclick'=>"return confirm('Silmek isteğinizden Emin misiniz?')"]) !!}
                                        {!! Form::close() !!} 
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
            <div class="text-center my-4">
                {{$teams->links()}}
            </div>
        </div>
    </div>
    @if ($teams)
        @foreach ($teams as $team)
            <!-- Modal -->
            <div class="modal fade" id="update_{{ $team->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">Create Admin</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        </div>
                        <div class="modal-body">
                            {{-- {!! Form::open(['method'=>'DELETE', 'action' => ['AdminCouponController@destroy', $coupon->id], 'style'=> 'display:inline', 'id' => 'id_form_' . $coupon->code]) !!} --}}
                            {!! Form::open(['method'=>'PATCH', 'action' => ['AdminSellingTeamsController@update', $team->id], 'style'=> 'display:inline', 'id' => $team->id . '_form_update', 'files'=>true]) !!}
                                <div class="form-group">

                                    {!! Form::label($team->id . '_name', 'Name') !!}
                                    {!! Form::text($team->id . '_name', $team->name, ['class'=>'form-control']) !!}
                    
                                </div>
                                <div class="form-group">

                                    {!! Form::label($team->id . '_note', 'note') !!}
                                    {!! Form::text($team->id . '_note', $team->note, ['class'=>'form-control']) !!}
                    
                                </div>
                            {!! Form::close() !!} 
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button class="btn btn-primary" onclick="event.preventDefault();document.getElementById('{{$team->id}}_form_update').submit();">
                                Save Changes
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    @endif
@endsection
