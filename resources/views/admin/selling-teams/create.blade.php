@extends('layouts.admin')
@section('content')
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-dark text-light">Selling Teams</div>

                    <div class="card-body ">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        You are logged in as Admin You are in Create Selling Teams Section!
                    </div>
                </div>
                @include('include.messages')
                @include('include.form-errors')
            </div>
        </div>
    </div>
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="text-right">
                    <a  href="{{route('admin.selling-teams.create')}}" class="header-table btn btn-outline-success mb-2">Create Team</a>
                </div>
                {!! Form::open(['method'=>'POST', 'action' => 'AdminSellingTeamsController@store', 'files'=>true]) !!}

                    <div class="form-row">

                        <div class="form-group col-lg-6">
                    
                            {!! Form::label('name', 'Name:') !!}
                            {!! Form::text('name', null, ['class'=>'form-control']) !!}
                    
                        </div>

                        <div class="form-group col-lg-6">

                            {!! Form::label('note', 'Note') !!}
                            {!! Form::text('note', null, ['class'=>'form-control']) !!}
    
                        </div>

                    </div>
                
                    <div class="form-group" style="height: 500px">
                    
                        {!! Form::label('cities[]', 'Cities:') !!}
                        {!! Form::select('cities[]', $cities, null, ['class'=>'form-control', 'placeholder'=>'Please select ...', 'multiple', 'style' => 'height:90%']) !!}
                        
                    </div>

                    <div class="form-group">
                        {!! Form::submit('Create Team', ['class'=>'btn btn-primary']) !!}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('script')
 
@endsection