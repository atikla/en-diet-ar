@extends('layouts.admin')
@section('content')
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-dark text-light">Selling Teams</div>

                    <div class="card-body ">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        You are logged in as Admin You are in selling team show Section!
                    </div>
                </div>
                @include('include.messages')
                @include('include.form-errors')
            </div>
        </div>
    </div>
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="text-right">
                    <a  href="{{route('admin.selling-teams.index')}}" class="header-table btn btn-outline-success mb-2">Selling Teams</a>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="card my-3">
                            <div class="card-header bg-dark text-light">Selling Team leader</div>
                            <div class="card-body ">
                                <ul>
                                    <li>name: {{$team->leader->name}}</li>
                                    <li>email: {{$team->leader->email}}</li>
                                    <li>phone: {{$team->leader->phone}}</li>
                                    <li>address: {{$team->leader->address}}</li>
                                    <li>reference code: {{$team->leader->reference_code}}</li>
                                </ul>
                                <button type="button" class="ml-3 btn btn-outline-danger" data-toggle="modal" data-target="#change_leader">
                                    Change Leader
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card my-3">
                            <div class="card-header bg-info text-light">Selling Team info</div>
                            <div class="card-body ">
                                <ul>
                                    <li>team name: {{$team->name}}</li>
                                    <li>team note: {{$team->note ? $team->note : 'Not Entered'}}</li>
                                    <li>team member conut: {{$team->dietitians()->count()}}</li>
                                    <li>team citeis conut: {{$team->cities()->count()}}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card my-3">
                            <div class="card-header bg-success text-light">test</div>
                            <div class="card-body ">
                               <ul>
                                   <li></li>
                                   <li></li>
                                   <li></li>
                                   <li></li>
                                   <li></li>
                               </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="text-center">
                            <button type="button" class="ml-3 btn btn-outline-success" data-toggle="modal" data-target="#add_city">
                                Add city to this team
                            </button>
                        </div>
                        <h4>Cities</h4>
                        <table class="table table-striped">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">process</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach ($team->cities as $city)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{$city->city}}</td>
                                        <td>
                                            {!! Form::open(['method'=>'DELETE', 'action' => ['AdminSellingTeamsController@destroyCity', $team->id, $city->id], 'style'=> 'display:inline']) !!}
                                                {!! Form::submit('Delete This City', ['class'=>'btn btn-outline-danger mr-2 mb-1','onclick'=>"return confirm('bu takimden Silmek isteğinizden Emin misiniz?')"]) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-6">
                        <div class="text-center">
                            <a href="{{route('admin.selling-teams.add-member.create', $team->id)}}" class="header-table btn btn-outline-success mb-2">Add memebers to this team</a>
                        </div>
                        <h4>Members</h4>
                        <table class="table table-striped">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Reference Code</th>
                                <th scope="col">Type</th>
                                <th scope="col">Phone</th>
                                <th scope="col">process</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach ($team->dietitians as $member)
                                    <tr {{ $team->leader->id ==  $member->id ? 'class="bg-success"' : ''}}>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{$member->name}}</td>
                                        <td>{{$member->reference_code}}</td>
                                        <td>{{$member->kind}}</td>
                                        <td>{{$member->phone}}</td>
                                        <td>
                                            <a href="{{route('admin.dietitians.show', $member->id)}}" class="btn btn-outline-info mr-2 mb-1" >Show Profile</a>
                                            {!! Form::open(['method'=>'DELETE', 'action' => ['AdminSellingTeamsController@destroyMember', $member->id], 'style'=> 'display:inline']) !!}
                                                {!! Form::submit('Delete From Team', ['class'=>'btn btn-outline-danger mr-2 mb-1','onclick'=>"return confirm('bu takimden Silmek isteğinizden Emin misiniz?')"]) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- Modal -for change leader -->
<div class="modal fade" id="change_leader" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle"> Change Leader</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {!! Form::open(['method'=>'POST', 'action' => ['AdminSellingTeamsController@changeLeader', $team->id], 'style'=> 'display:inline', 'id' => 'change_leader_form']) !!}
                    
                    <div class="form-group">
                                                
                        {{ Form::label('Select New Leader : ') }}
                        {{ Form::select('leader_id', $members, null, array('class'=>'form-control', 'placeholder'=>'Please select ...')) }}
                        
                    </div>
                    <input type="hidden" name="old_leader_id" value="{{$team->leader->id}}">

                {!! Form::close() !!} 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" onclick="
                    event.preventDefault();
                    document.getElementById('change_leader_form').submit();
                ">Save Changes
                </button>
            </div>
        </div>
    </div>
</div>
<!-- Modal -for Add New City -->
<div class="modal fade" id="add_city" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle"> Add City</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {!! Form::open(['method'=>'POST', 'action' => ['AdminSellingTeamsController@storeCity', $team->id], 'style'=> 'display:inline', 'id' => 'add_city_form']) !!}
                    
                    <div class="form-group">
                                                
                        {{ Form::label('city', 'Select New Leader : ') }}
                        {{ Form::select('city[]', $cities, null, array('class'=>'form-control', 'multiple', 'placeholder'=>'Please select ...')) }}
                        
                    </div>

                {!! Form::close() !!} 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" onclick="
                    event.preventDefault();
                    document.getElementById('add_city_form').submit();
                ">Save Changes
                </button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
 
@endsection