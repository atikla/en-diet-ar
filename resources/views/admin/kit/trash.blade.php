@extends('layouts.admin')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-dark text-light">Kits</div>

                    <div class="card-body ">
                        You are logged in as Admin You are in Kits Trash Section!
                    </div>
                </div>
                @include('include.messages')
                @include('include.form-errors')
            </div>
        </div>
    </div>
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="text-right">
                    <a href="{{route('admin.kits.index')}}" class="btn btn-outline-success mb-2 mr-5">All Kit</a>
                </div>
                <table class="table table-responsive-lg table-striped">
                    <thead class="thead-light">
                    <tr>
                        <th>#</th>
                        <th>kit code</th>
                        <th>sell status</th>
                        <th>kit status</th>
                        <th>deleted</th>
                        <th>process</th>
                    </tr>
                    </thead>
                    <tbody>
                        @if ($kits)
                            @foreach ($kits as $kit)
                            <tr>
                                <td>{{$kit->id}}</td>
                                <td>{{$kit->kit_code}}</td>
                                <td>{{$kit->status}}</td>
                                <td>{{$kit->kitStatuse}}</td>
                                <td>{{$kit->deleted_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}</td>   
                                <td>
                                    <a href="{{route('admin.kits.restore', $kit->id)}}" class="btn btn-outline-success mb-2 mr-5">Restore</a>
                                    {!! Form::open(['method'=>'DELETE', 'action' => ['AdminKitsController@destroy', $kit->id], 'style'=> 'display:inline']) !!}
                                        <input type="hidden" name="soft" value="1">
                                        {!! Form::submit('Delete', ['class'=>'btn btn-outline-danger mr-2 mb-1','onclick'=>"return confirm('Silmek isteğinizden Emin misiniz?')"]) !!}
                                    {!! Form::close() !!} 
                                </td>
                            </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
                <div class="text-center my-4">
                    {{$kits->links()}}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')

@endsection