@extends('layouts.admin')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-dark text-light">Kits</div>

                    <div class="card-body ">
                        You are logged in as Admin You are in Kit Details Section!
                    </div>
                </div>
                @include('include.messages')
                @include('include.form-errors')
            </div>
        </div>
    </div>
    <div class="mt-3 mx-5">
        @if ($kit->kit_code)
            <h2>Kit Code {{ $kit->kit_code }}</h3>
            {!! Form::open(['method'=>'DELETE', 'action' => ['AdminKitsController@destroy', $kit->id], 'style'=> 'display:inline']) !!}
                {!! Form::submit('Delete', ['class'=>'btn btn-outline-danger mr-2 mb-1','onclick'=>"return confirm('Silmek isteğinizden Emin misiniz?')"]) !!}
            {!! Form::close() !!} 
        @else
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#kit_id{{$kit->id}}">
                Enter Kit Code
            </button>
            
            <!-- Modal -->
            <div class="modal fade" id="kit_id{{$kit->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                        {!! Form::open(['method'=>'PATCH', 'action' => ['AdminKitsController@update', $kit->id], 'style'=> 'display:inline', 'id' => 'id_form_create_' . $kit->id]) !!}
                            
                            <div class="form-group">

                                {!! Form::label('id_' . $kit->id . 'kit_code', 'Kit Code : ') !!}
                                {!! Form::text('id_' . $kit->id . 'kit_code', null, ['class'=>'form-control']) !!}
                
                            </div>
                            <input type="hidden" name="id_{{$kit->id}}_kit_id" value="{{$kit->id}}">      
                        {!! Form::close() !!} 
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary" onclick="
                        event.preventDefault();
                        document.getElementById('id_form_create_{{$kit->id}}').submit();
                    ">
                    Save Changes</button>
                    </div>
                </div>
                </div>
            </div>
        @endif
        <p>Show Status</p>
        @if ( $kit->show )
            <p>User <span class="text-success"> CAN </span> Show This Kit Result</p>
            {!! Form::open(['method'=>'POST', 'action' => ['AdminKitsController@changeShow', $kit->kit_code, 'show' => 0], 'style'=> 'display:inline']) !!}

                {!! Form::submit('make it DEACTIVATE', ['class'=>'btn btn-outline-danger mb-2', 'onclick'=>'return confirm("After submitted, the results of the kit will NOT be displayed to the user. should it continue")']) !!}

            {!! Form::close() !!}
        @else
            <p>User <span class="text-danger"> Can NOT</span> Show This Kit Result</p>
            {!! Form::open(['method'=>'POST', 'action' => ['AdminKitsController@changeShow', $kit->kit_code, 'show' => 1], 'style'=> 'display:inline']) !!}

                {!! Form::submit('make it ACTIVE', ['class'=>'btn btn-outline-success mb-2', 'onclick'=>'return confirm("THIS KIT RESULT WILL BE SHOWN TO USER")']) !!}

            {!! Form::close() !!}
        @endif
        <div class="row">
            @if ($kit->food && $kit->microbiome)
                <div class="col-6 text-center">
                    <a href="{{route('admin.kits.pdf.generate', [$kit->kit_code, 'lang' => 'tr'])}}" class="btn btn-outline-secondary btn-block mb-2">
                        Go To <span class="flag-icon flag-icon-tr"> </span> Reports
                    </a>
                </div>
                <div class="col-6 text-center">
                    <a href="{{route('admin.kits.pdf.generate', [$kit->kit_code, 'lang' => 'en'])}}" class="btn btn-outline-secondary btn-block mb-2">
                        Go To <span class="flag-icon flag-icon-gb"> </span> Reports
                    </a>
                </div>
            @else 
                <div class="col-12 text-center">
                    <h3 class="text-danger">
                        THE KIT RESULTS NOT UPLOADED YET!
                    </h3>
                </div>
            @endif
            @include('include.kits.suggestion', [ 'kit' => $kit ])
            @include('include.kits.kitStatus', [ 'kit' => $kit ])
            @if ($kit->user)
                <div class="col-lg-6 col-12 mt-3 mb-3">
                    <div class="card">
                        <div class="card-header bg-info text-light">User Informations</div>
                        <div class="card-body ">
                            <div class="text-right">
                                <a href="{{route('admin.users.show', $kit->user->id)}}" class="btn btn-outline-info mr-2 mb-1" >Show Profile</a>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-12 text-left"><h4><b> Name   : </b> {{$kit->user->name}}</h4></div>
                                <div class="col-lg-6 col-md-12 text-left"><h4><b> Email  : </b> {{$kit->user->email}}</h4></div>
                                <div class="col-lg-6 col-md-12 text-left"><h4><b> address   : </b> {{$kit->user->address}}</h4></div>
                                <div class="col-lg-6 col-md-12 text-left"><h4><b> phone  : </b> {{$kit->user->phone}}</h4></div>
                                <div class="col-lg-6 col-md-12 text-left"><h4><b> UserGender  : </b> {{$kit->user->UserGender}}</h4></div>
                                <div class="col-lg-6 col-md-12 text-left"><h4><b> vegetarian  : </b> {{$kit->user->vegetarian == 0 ? 'NO' : 'YES'}}</h4></div>
                                <div class="col-12 text-center">
                                    {!! Form::open(['method'=>'POST', 'action' => ['AdminKitsController@dislink', $kit->id], 'style'=> 'display:inline']) !!}
                                        {!! Form::submit('Dislink this Kit to this user', ['class'=>'btn btn-outline-danger mr-2 mb-1','onclick'=>"return confirm('Emin misiniz?')"]) !!}
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @else 
            <div class="col-lg-6 col-12 mt-3">
                <div class="card">
                    <div class="card-header bg-info text-light">Kit's user</div>
                    <div class="card-body text-center">
                        @if ($kit->kit_code)
                            <div class="text-right mb-3">
                                <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#add_user_to_kit">
                                    Add  user to This kit
                                </button>
                            </div>
                        @endif
                        <p class="text-danger" > this kit not registered yet! </p>
                    </div>
                </div>
            </div>
            @endif
            @if ($kit->dietitian)
                <div class="col-lg-6 col-12 mt-3 mb-3">
                    <div class="card">
                        <div class="card-header bg-secondary text-light">dietitian Informations</div>
                        <div class="card-body ">
                            <div class="text-right">
                                <a href="{{route('admin.dietitians.show', $kit->dietitian->id)}}" class="btn btn-outline-info mr-2 mb-1" >Show Profile</a>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-12 text-left"><h4><b> Name   : </b> {{$kit->dietitian->name}}</h4></div>
                                <div class="col-lg-6 col-md-12 text-left"><h4><b> Email  : </b> {{$kit->dietitian->email}}</h4></div>
                                <div class="col-lg-6 col-md-12 text-left"><h4><b> address   : </b> {{$kit->dietitian->address}}</h4></div>
                                <div class="col-lg-6 col-md-12 text-left"><h4><b> phone  : </b> {{$kit->dietitian->phone}}</h4></div>
                                <div class="col-12 text-center">
                                    {!! Form::open(['method'=>'POST', 'action' => ['AdminKitsController@dietitianDislink', $kit->id], 'style'=> 'display:inline']) !!}
                                        {!! Form::submit('Dislink this Kit from this dietitian', ['class'=>'btn btn-outline-danger mr-2 mb-1','onclick'=>"return confirm('Emin misiniz?')"]) !!}
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @else 
            <div class="col-lg-6 col-12 mt-3">
                <div class="card">
                    <div class="card-header bg-secondary text-light">Kit's dietitian</div>
                    <div class="card-body text-center">
                        @if ($kit->kit_code)
                            <div class="text-right mb-3">
                                <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#add_dietitian_to_kit">
                                    Add  dietitian to This kit
                                </button>
                            </div>
                        @endif
                        <p class="text-danger" > this kit not have a dietitian as a reference yet! </p>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
<!-- Modal -->
<div class="modal fade" id="add_user_to_kit" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Add User To This Kit</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {!! Form::open(['method'=>'POST', 'action' => ['AdminKitsController@link', $kit->id], 'style'=> 'display:inline', 'id' => 'add_user_to_kit_form']) !!}
                    
                    <div class="form-group">
                                                
                        {{ Form::label('Select User : ') }}
                        {{ Form::select('user', $users, null, array('class'=>'form-control', 'placeholder'=>'Please select ...')) }}
                        
                    </div>

                {!! Form::close() !!} 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" onclick="
                    event.preventDefault();
                    document.getElementById('add_user_to_kit_form').submit();
                ">Save Changes
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="add_dietitian_to_kit" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Add dietitian To This Kit</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {!! Form::open(['method'=>'POST', 'action' => ['AdminKitsController@dietitianLink', $kit->id], 'style'=> 'display:inline', 'id' => 'add_dietitian_to_kit_form']) !!}
                    
                    <div class="form-group">
                                                
                        {{ Form::label('Select User : ') }}
                        {{ Form::select('dietitian', $dietitians, null, array('class'=>'form-control', 'placeholder'=>'Please select ...')) }}
                        
                    </div>

                {!! Form::close() !!} 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" onclick="
                    event.preventDefault();
                    document.getElementById('add_dietitian_to_kit_form').submit();
                ">Save Changes
                </button>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script src="https://cdn.ckeditor.com/4.13.0/basic/ckeditor.js"></script>
    <script type="text/javascript">

        $(document).ready(function() {
            $('textarea').each(function(e){
                CKEDITOR.replace( this.id, {language: '{{ app()->getlocale() }}'});
            });
        });

    </script>
@endsection
