@extends('layouts.admin')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-dark text-light">Kits</div>

                    <div class="card-body ">
                        You are logged in as Admin You are in Kits Section!
                    </div>
                </div>
                @include('include.messages')
                @include('include.form-errors')
            </div>
        </div>
    </div>
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="text-right">
                    <a  href="{{route('admin.kits.export')}}" class="header-table btn btn-outline-success mb-2">Export All</a>
                    <a  href="{{route('admin.kits.index')}}" class="header-table btn btn-outline-success mb-2">All Kits</a>
                    {!! Form::open(['method'=>'POST', 'action' => ['AdminKitsController@search', 'view' => 'pdf'], 'style'=> 'display:inline']) !!}

                        {!! Form::label('search', 'Query:') !!}
                        {!! Form::text('search', null, ['class'=>'form-control', 'style'=> 'display:inline; width:initial', 'required']) !!}

                        {!! Form::submit('search', ['class'=>'btn btn-outline-success mr-2 mb-1']) !!}

                    {!! Form::close() !!}
                </div>
                <table class="table table-responsive-lg table-striped">
                    <thead class="thead-light">
                    <tr>
                        <th>#</th>
                        <th>kit code</th>
                        <th>sell status</th>
                        <th>kit status</th>
                        <th>Order</th>
                        <th>user</th>
                        <th>Created</th>
                        <th>Updated</th>
                        <th>process</th>
                    </tr>
                    </thead>
                    <tbody>
                        @if ($kits)
                            @foreach ($kits as $kit)
                            <tr>
                                <td>{{$kit->id}}</td>
                                <td>{{$kit->kit_code}}</td>
                                <td>{{$kit->status}}</td>
                                <td>{{$kit->kitStatuse}}</td>
                                @if ($kit->OrderDetail)
                                <td> <a href="{{ route('admin.orders.show', $kit->OrderDetail->order->id) }}"> Show Order</a></td>
                                @else
                                    <td>Dont Have a Order ( sold offline ) </td>
                                @endif
                                <td>{!! $kit->user ? '<a href="'. route('admin.users.show', $kit->user->id) . '">user Profile</a>' : 'no' !!}</td>
                                <td>{{$kit->created_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}</td>
                                <td>{{$kit->updated_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}</td>   
                                <td>
                                    @if ( $kit->food && $kit->microbiome )
                                        <a href="{{route('admin.kits.pdf.generate', [$kit->kit_code, 'lang' => 'tr'])}}" class=" pdf btn btn-outline-secondary mb-2">
                                            Go To <span class="flag-icon flag-icon-tr"> </span> Reports
                                        </a>
                                        <a href="{{route('admin.kits.pdf.generate', [$kit->kit_code, 'lang' => 'en'])}}" class=" pdf btn btn-outline-secondary mb-2">
                                            Go To <span class="flag-icon flag-icon-gb"> </span> Reports
                                        </a>
                                        {!! Form::open(['method'=>'POST', 'action' => ['AdminKitsController@sendMail', $kit->kit_code], 'style'=> 'display:inline']) !!}

                                            {!! Form::submit('Send Mails', ['class'=>'btn btn-outline-secondary mb-2', 'onclick'=>'return confirm("Gondermek isteğinizden Emin misiniz?")']) !!}

                                        {!! Form::close() !!}
                                        @if ( $kit->show )
                                            {!! Form::open(['method'=>'POST', 'action' => ['AdminKitsController@changeShow', $kit->kit_code, 'show' => 0], 'style'=> 'display:inline']) !!}

                                                {!! Form::submit('make it DEACTIVATE', ['class'=>'btn btn-outline-danger mb-2', 'onclick'=>'return confirm("After submitted, the results of the kit will NOT be displayed to the user. should it continue")']) !!}

                                            {!! Form::close() !!}
                                        @else
                                            {!! Form::open(['method'=>'POST', 'action' => ['AdminKitsController@changeShow', $kit->kit_code, 'show' => 1], 'style'=> 'display:inline']) !!}

                                                {!! Form::submit('make it ACTIVE', ['class'=>'btn btn-outline-success mb-2', 'onclick'=>'return confirm("THIS KIT RESULT WILL BE SHOWN TO USER")']) !!}

                                            {!! Form::close() !!}
                                        @endif
                                    @endif
                                    <a href="{{route('admin.kits.show', $kit->id)}}" class="btn btn-outline-info mr-2 mb-1" >Show Kit Details</a>
                                </td>
                            </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('script')

    <script type="text/javascript">
        $(document).ready(function(){
            $('.pdf').click(function (){
                $body.addClass("loading");
            });
        });
    </script>
@endsection