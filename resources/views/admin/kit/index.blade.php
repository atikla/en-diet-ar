@extends('layouts.admin')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-dark text-light">Kits</div>

                    <div class="card-body ">
                        You are logged in as Admin You are in Kits Section!
                    </div>
                </div>
                @include('include.messages')
                @include('include.form-errors')
            </div>
        </div>
    </div>
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-lg-3 col-12">
                {{-- <div class="card">
                    <div class="card-header bg-success text-light">Generate Kits Codes</div>
                    <div class="card-body ">
                        {!! Form::open(['method'=>'POST', 'action' => 'AdminKitsController@store']) !!}
        
                            <div class="form-group">
                        
                                {!! Form::label('kit_code', 'how many code(min:1 | max:200 per time):') !!}
                                {!! Form::number('kit_code', null, ['class'=>'form-control']) !!}
                        
                            </div>
        
                            <div class="form-group">
                                {!! Form::submit('Generate Kit codes', ['class'=>'btn btn-primary']) !!}
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div> --}}
                <div class="card my-4">
                    <div class="card-header bg-success text-light">Register old Kit codes (min:1 | max:10 per time)</div>
                    <div class="card-body ">
                        <span class="btn btn-info text-light add-field-button mb-2">
                            Add A Input field
                        </span>
                        {!! Form::open(['method'=>'POST', 'action' => 'AdminKitsController@storeNewKit']) !!}
                            <div class="input-fields-wrap">
                                <div class="form-group">
                            
                                    {!! Form::label('kit_code_(1)', 'kit code ( 1 ) : ') !!}
                                    {!! Form::text('kit_code[]', null, ['class'=>'form-control']) !!}
                            
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::submit('Register Old Kit codes', ['class'=>'btn btn-primary']) !!}
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="col-lg-9 col-12">
                <div class="text-right">
                    <button type="button" class="header-table btn btn-outline-success mb-2" data-toggle="modal" data-target="#gida_skorlari">
                        Upload Food scores 
                    </button>
                    <button type="button" class="header-table btn btn-outline-success mb-2" data-toggle="modal" data-target="#skorlar">
                        Upload Mikrobiyom Analiz
                    </button>
                    <a  href="{{route('admin.kits.export')}}" class="header-table btn btn-outline-success mb-2">Export All</a>
                    <a  href="{{route('admin.kits.pdf')}}" class="header-table btn btn-outline-secondary mb-2">Kit Reports</a>
                    <a  href="{{route('admin.kits.trash')}}" class="header-table btn btn-outline-danger mb-2">Kit Trash</a>
                    {!! Form::open(['method'=>'POST', 'action' => ['AdminKitsController@search'], 'style'=> 'display:inline']) !!}

                        {!! Form::label('search', 'Query:') !!}
                        {!! Form::text('search', null, ['class'=>'form-control', 'style'=> 'display:inline; width:initial', 'required']) !!}

                        {!! Form::submit('search', ['class'=>'btn btn-outline-success mr-2 mb-1']) !!}

                    {!! Form::close() !!}
                </div>
                <table class="table table-responsive-lg table-striped">
                    <thead class="thead-light">
                    <tr>
                        <th>#</th>
                        <th>kit code</th>
                        <th>sell status</th>
                        <th>kit status</th>
                        <th>kit show status</th>
                        <th>Order</th>
                        <th>user</th>
                        <th>Created</th>
                        <th>Updated</th>
                        <th>process</th>
                    </tr>
                    </thead>
                    <tbody>
                        @if ($kits)
                            @foreach ($kits as $kit)
                            <tr>
                                <td>{{$kit->id}}</td>
                                <td>{{$kit->kit_code}}</td>
                                <td>{{$kit->status}}</td>
                                <td>{{$kit->kitStatuse}}</td>
                                <td>{!! $kit->show ? '<span class="text-success">User Can View Results</span>' : '<span class="text-danger">User Can Not View Results</span>' !!}</td>
                                @if ($kit->OrderDetail)
                                <td> <a href="{{ route('admin.orders.show', $kit->OrderDetail->order->id) }}"> Show Order</a></td>
                                @else
                                    <td>Dont Have a Order ( sold offline ) </td>
                                @endif
                                <td>{!! $kit->user ? '<a href="'. route('admin.users.show', $kit->user->id) . '">user Profile</a>' : 'no' !!}</td>
                                <td>{{$kit->created_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}</td>
                                <td>{{$kit->updated_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}</td>   
                                <td>
                                    @if ( $kit->food && $kit->microbiome)
                                        <a href="{{route('admin.kits.pdf.generate', $kit->kit_code)}}" class="btn btn-outline-secondary mb-2"> Go To Reports</a>
                                    @endif
                                    <a href="{{route('admin.kits.show', $kit->id)}}" class="btn btn-outline-info mr-2 mb-1" >Show Kit Details</a>
                                    {!! Form::open(['method'=>'DELETE', 'action' => ['AdminKitsController@destroy', $kit->id], 'style'=> 'display:inline']) !!}
                                        {!! Form::submit('Delete', ['class'=>'btn btn-outline-danger mr-2 mb-1','onclick'=>"return confirm('Silmek isteğinizden Emin misiniz?')"]) !!}
                                    {!! Form::close() !!}
                                    @if ( $kit->show )
                                        {!! Form::open(['method'=>'POST', 'action' => ['AdminKitsController@changeShow', $kit->kit_code, 'show' => 0], 'style'=> 'display:inline']) !!}

                                            {!! Form::submit('make it DEACTIVATE', ['class'=>'btn btn-outline-danger mb-2', 'onclick'=>'return confirm("After submitted, the results of the kit will NOT be displayed to the user. should it continue")']) !!}

                                        {!! Form::close() !!}
                                    @else
                                        {!! Form::open(['method'=>'POST', 'action' => ['AdminKitsController@changeShow', $kit->kit_code, 'show' => 1], 'style'=> 'display:inline']) !!}

                                            {!! Form::submit('make it ACTIVE', ['class'=>'btn btn-outline-success mb-2', 'onclick'=>'return confirm("THIS KIT RESULT WILL BE SHOWN TO USER")']) !!}

                                        {!! Form::close() !!}
                                    @endif
                                    <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#kit_id{{$kit->id}}">
                                        Update Kit Code
                                    </button>
                                </td>
                            </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
                <div class="text-center my-4">
                    {{$kits->links()}}
                </div>
            </div>
        </div>
    </div>
    @if ($kits)
        @foreach ($kits as $kit)
            <!-- Modal -->
            <div class="modal fade" id="kit_id{{$kit->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                        {!! Form::open(['method'=>'PATCH', 'action' => ['AdminKitsController@update', $kit->id], 'style'=> 'display:inline', 'id' => 'id_form_create_' . $kit->id]) !!}
                            <div class="form-group">

                                {!! Form::label('id_' . $kit->id . 'kit_code', 'Kit Code : ') !!}
                                {!! Form::text('id_' . $kit->id . 'kit_code', $kit->kit_code, ['class'=>'form-control']) !!}
                
                            </div>
                            <input type="hidden" name="id_{{$kit->id}}_kit_id" value="{{$kit->id}}">      
                        {!! Form::close() !!} 
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary" onclick="
                        event.preventDefault();
                        document.getElementById('id_form_create_{{$kit->id}}').submit();
                    ">
                    Save Changes</button>
                    </div>
                </div>
                </div>
            </div>
        @endforeach
    @endif
    {{-- For Food  --}}
    <div class="modal fade" id="gida_skorlari" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitle">Upload Food scores </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                {!! Form::open(['method'=>'POST', 'action' => 'AdminKitsController@uploadFoodJsonFile', 'files'=>true, 'id' => 'upload_gida_skorlari']) !!}
                    <div class="form-group">
                        {!! Form::label('json-file', 'Json File:') !!}
                        {!! Form::file('json-file', [ 'class' => 'ml-5' ]) !!}
                    </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button class="btn btn-primary" onclick="event.preventDefault();document.getElementById('upload_gida_skorlari').submit();">Save changes</button>
            </div>
        </div>
        </div>
    </div>
    {{-- Upload Mikrobiyom Analiz --}}
    <div class="modal fade" id="skorlar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitle">Upload Mikrobiyom Analiz</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                {!! Form::open(['method'=>'POST', 'action' => 'AdminKitsController@uploadMicrobiomeJsonFile', 'files'=>true, 'id' => 'skorlari']) !!}
                    <div class="form-group">
                        {!! Form::label('json-file', 'Json File:') !!}
                        {!! Form::file('json-file', [ 'class' => 'ml-5' ]) !!}
                    </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button class="btn btn-primary" onclick="event.preventDefault();document.getElementById('skorlari').submit();">Save changes</button>
            </div>
        </div>
        </div>
    </div>
@endsection
@section('script')

    <script type="text/javascript">
        $(document).ready(function(){

            let maxFields      = 5; //maximum input boxes allowed
            let wrapper   		= $(".input-fields-wrap"); //Fields wrapper
            let addButton      = $(".add-field-button"); //Add button ID
            
            let x = 1; //initlal text box count
            $(addButton).click(function(e){ //on add input button click
                e.preventDefault();
                let el = '';
                if(x < maxFields){ //max input box allowed
                    ++x; //text box increment
                    el += '<div class="form-group">';
                        el += '<label for="kit_code_('+ x +')">kit code ( '+ x +' ) : </label> <a href="#" class="remove-field text-danger">Remove</a>';
                        el += ' <input name="kit_code[]" id ="kit_code_('+ x +')" type="text" class="form-control">';
                    el += '</div>';
                    
                    $(wrapper).append(el); //add input box
                }else{
                    alert('max input fiel per time is 5');
                }
            });
            
            $(wrapper).on("click",".remove-field", function(e){ //user click on remove text
                e.preventDefault(); $(this).parent('div').remove(); x--;
            });
            
        });
    </script>
@endsection