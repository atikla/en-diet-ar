@extends('layouts.admin')
@section('content')
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-dark text-light">coupons</div>
                    <div class="card-body ">
                        You are logged in as Admin You are in coupons search result Section!
                    </div>
                </div>
                @include('include.form-errors')
            </div>
        </div>
    </div>
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="text-right">
                    <a  href="{{route('admin.coupons.index')}}" class="header-table btn btn-outline-info mb-2">Go to coupons index page</a>

                    {!! Form::open(['method'=>'POST', 'action' => ['AdminCouponController@search'], 'style'=> 'display:inline']) !!}

                        {!! Form::label('search', 'Query:') !!}
                        {!! Form::text('search', null, ['class'=>'form-control', 'style'=> 'display:inline; width:initial', 'required']) !!}

                        {!! Form::submit('search', ['class'=>'btn btn-outline-success mr-2 mb-1']) !!}

                    {!! Form::close() !!} 
                </div>
                <div class="row">
                    <div class="col-lg-12 col-12">
                        <table class="table table-responsive-lg table-striped w-100">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Code</th>
                                <th scope="col">Discount</th>
                                <th scope="col">Reference</th>
                                <th scope="col">Note</th>
                                <th scope="col">Status</th>
                                <th scope="col">Created</th>
                                <th scope="col">Updated</th>
                                <th scope="col">process</th>
                            </tr>
                            </thead>
                            <tbody>
                                @if ($coupons)
                                @foreach ($coupons as $coupon)
                                    <tr>
                                        <td>{{$coupon->id}}</td>
                                        <td>{{$coupon->code}}</td>
                                        <td>{{$coupon->discount}} %</td>
                                        <td>{!! $coupon->reference ? $coupon->reference . '<a  href="' . route('admin.dietitians.show', $coupon->dietitian->id). '" class="btn btn-outline-success ml-2">show profile </a>'  : '<sapn class="text-danger">no reference</span>' !!}</td>
                                        <td>{{$coupon->note}}</td>
                                        <td>{!! $coupon->status !!}</td>
                                        <td>{{$coupon->created_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}</td>
                                        <td>{{$coupon->updated_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}</td>
                                        <td>
                                            @if ($coupon->used != 1 )
                                                <button type="button" class="btn btn-outline-success update mr-2 mb-1" data-toggle="modal" data-target="#id_{{$coupon->code}}">
                                                    update
                                                </button>
                                                
                                                <!-- Modal -->
                                                <div class="modal fade" id="id_{{$coupon->code}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalCenterTitle">Update Coupon {{$coupon->code}}</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                {{-- {!! Form::open(['method'=>'DELETE', 'action' => ['AdminCouponController@destroy', $coupon->id], 'style'=> 'display:inline', 'id' => 'id_form_' . $coupon->code]) !!} --}}
                                                                {!! Form::model($coupon, ['method'=>'PATCH', 'action' => ['AdminCouponController@update', $coupon->id],'style'=> 'display:inline', 'id' => 'id_form_' . $coupon->code]) !!}
                                                                    <div class="form-group">

                                                                        {!! Form::label('discount ', 'Discount :') !!}
                                                                        {!! Form::number('discount', null, ['class'=>'form-control', 'min'=> '1', 'max' => '100', 'id' => 'id_dis_' . $coupon->code]) !!}
                                                        
                                                                    </div>
                                                        
                                                                    <div class="form-group">
                                                        
                                                                        {!! Form::label('note', 'Note:') !!}
                                                                        {!! Form::text('note', null, ['class'=>'form-control', 'id' => 'id_note_' . $coupon->code]) !!}
                                                                        
                                                                    </div>
                                                                {!! Form::close() !!} 
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                <button class="btn btn-primary" onclick="
                                                                    event.preventDefault();
                                                                    if(getElementById('id_dis_{{$coupon->code}}').value  > 100 || getElementById('id_dis_{{$coupon->code}}').value  < 1 ){
                                                                        alert('100 ve 1 arasinda bir deger almalidir');return 0;
                                                                    }

                                                                    document.getElementById('id_form_{{$coupon->code}}').submit();
                                                                ">Save Changes
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {!! Form::open(['method'=>'DELETE', 'action' => ['AdminCouponController@destroy', $coupon->id], 'style'=> 'display:inline']) !!}
                                                    {!! Form::submit('Delete', ['class'=>'btn btn-outline-danger mr-2 mb-1','onclick'=>"return confirm('Silmek isteğinizden Emin misiniz?')"]) !!}
                                                {!! Form::close() !!} 
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')

<script type="text/javascript">
    $(document).ready(function(){
        
    });
    </script>
@endsection
