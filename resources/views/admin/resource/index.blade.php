@extends('layouts.admin')
@section('content')
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-dark text-light">Resources</div>

                    <div class="card-body ">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        You are logged in as Admin You are in Resources Section!
                    </div>
                </div>
                @include('include.messages')
                @include('include.form-errors')
            </div>
        </div>
    </div>
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="text-right">
                    {{-- <a  href="{{route('admin.resources.create')}}" class="header-table btn btn-outline-success mb-2">Create resource</a> --}}
                    <button type="button" class="btn btn-outline-success mr-2 mb-1" data-toggle="modal" data-target="#id_create">
                        Create Resource
                    </button>
                </div>
                <table class="table table-responsive-lg table-striped">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Photo</th>
                        <th scope="col">video URL</th>
                        <th scope="col">description</th>
                        <th scope="col">Created</th>
                        <th scope="col">Updated</th>
                        <th scope="col">process</th>
                    </tr>
                    </thead>
                    <tbody>
                        @if ($resources)
                            @foreach ($resources as $resource)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td><img class='img-rounded' height="50" src="{{$resource->photo ? $resource->photo->path : url('/') . '/img/private/default.png'}}" alt=""></td>
                                    <td><a href="{{$resource->video}}" target="_blanck">{{$resource->video}}</a></td>
                                    <td>{{$resource->description}}</td>
                                    <td>{{$resource->created_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}</td>
                                    <td>{{$resource->updated_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}</td>
                                    <td>
                                        {{-- <a href="{{route('admin.resources.show', $resource->id)}}" class="btn btn-outline-info mr-2 mb-1" >Show Profile</a> --}}
                                        <a   href="{{route('admin.resources.edit', $resource->id)}}"	class="btn btn-outline-success update mr-2 mb-1">Update</a>
                                        {!! Form::open(['method'=>'DELETE', 'action' => ['AdminResourcesController@destroy', $resource->id], 'style'=> 'display:inline']) !!}
                                            {!! Form::submit('Delete', ['class'=>'btn btn-outline-danger mr-2 mb-1','onclick'=>"return confirm('Silmek isteğinizden Emin misiniz?')"]) !!}
                                        {!! Form::close() !!} 
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
            <div class="text-center my-4">
                {{$resources->links()}}
            </div>
        </div>
    </div>
   <!-- Resource Modal -->
   <div class="modal fade" id="id_create" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitle">Create Resource</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                {!! Form::open(['method'=>'POST', 'action' => 'AdminResourcesController@store', 'style'=> 'display:inline', 'id' => 'id_form_create', 'files'=>true]) !!}
                    <div class="form-group">

                        {!! Form::label('video_url', 'video URL ( Youtube Video URL )') !!}
                        {!! Form::text('video_url', null, ['class'=>'form-control']) !!}
        
                    </div>
                    <div class="form-group">

                        {!! Form::label('description', 'Description ( Optional )') !!}
                        {!! Form::text('description', null, ['class'=>'form-control']) !!}
        
                    </div>
        
                    <div class="form-group">
        
                        {!! Form::label('photo', 'photo ( Optional ) max file size : 2MB') !!}
                        {!! Form::file('photo', null, ['class'=>'form-control']) !!}
                        
                    </div>
                    <input type="hidden" name="imageUrl" id="imageUrl">
                    <input type="hidden" name="embed_video" id="embed_video">
                    
                {!! Form::close() !!} 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" onclick="
                    event.preventDefault();
                    var i = validateYouTubeUrl();
                    if( !i ){
                        alert('video_url  Alani Gerekli ve Dogru Bicimde olmasi lazim');return 0;
                    }
                    document.getElementById('id_form_create').submit();
                ">
                Create</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')

<script type="text/javascript">
    $(document).ready(function(){
        
    });
    function validateYouTubeUrl()
    {
        var url = $('#video_url').val();
        if (url != undefined || url != '') {
            var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
            var match = url.match(regExp);
            if (match && match[2].length == 11) {
                // Do anything for being valid
                // if need to change the url to embed url then use below line
                $('#video_url').val("https://www.youtube.com/watch?v="+ match[2]);
                $('#embed_video').val("https://www.youtube.com/embed/"+ match[2]);
                $('#imageUrl').val("https://img.youtube.com/vi/" +  match[2] + "/maxresdefault.jpg");
                return true;
            }
            else {
                return false;
            }
        }
        return false;
    }
</script>
@endsection