@extends('layouts.admin')
@section('styles')
<style>
    .enbiosis-color {
       color: #65cde6;
    }
    .card-title {
       color:#d46854;
    }
</style>
@endsection
@section('content')
<div class="row p-4">
    <div class="col-lg-3 col-6 mb-3">
        <div class="card bg-light">
            <div class="card-body">
                <span class="float-right">
                    <i class="enbiosis-color fas fa-users fa-3x"></i>
                </span>
                <h4 class="card-title">Total Users</h4>
                <h4 class="card-text">{{ $users->count() }}</h4>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-6 mb-3">
        <div class="card bg-light">
            <div class="card-body">
                <span class="float-right">
                    <i class="enbiosis-color fas fa-box fa-3x"></i>
                </span>
                <h4 class="card-title">Total Orders</h4>
                <h4 class="card-text">{{ $orders->count() }}</h4>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-6 mb-3">
        <div class="card bg-light">
            <div class="card-body">
                <span class="float-right">
                    <i class="enbiosis-color fas fa-box-open fa-3x"></i>
                </span>
                <h4 class="card-title">All Kits</h4>
                <h4 class="card-text">{{ $kits->count() }}</h4>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-6 mb-3">
        <div class="card bg-light">
            <div class="card-body">
                <span class="float-right">
                    <i class="enbiosis-color fas fa-tags fa-3x"></i>
                </span>
                <h4 class="card-title">All Coupons</h4>
                <h4 class="card-text">{{ $coupons->count() }}</h4>
            </div>
        </div>
    </div>
    <div class="col-lg-6"id="activity1" style="height: 350px" >

    </div>

    <div class="col-lg-6"id="activity2" style="height: 350px" >

    </div>

</div>
<h2 class="enbiosis-color pl-4">Kits</h2>
<div class="row p-4">
    <div class="col-lg-4 col-md-12 col-12"id="kit-re-nre" style="height: 350px" >

    </div>
    <div class="col-lg-4 col-md-12 col-12"id="kit-on-off" style="height: 350px" >

    </div>
    <div class="col-lg-4 col-md-12 col-12"id="kit-status" style="height: 350px" >

    </div>
</div>
<h2 class="enbiosis-color pl-4">Coupons</h2>
<div class="row p-4">
    <div class="col-lg-6 col-md-12 col-12"id="coupon" style="height: 350px" >

    </div>

    {{-- <div class="col-lg-3 col-6 mb-3">
        <div class="card bg-light">
            <div class="card-body">
                <span class="float-right">
                    <i class="enbiosis-color fas fa-apple-alt fa-3x"></i>
                </span>
                <h4 class="card-title">Total Kits</h4>
                <h4 class="card-text">{{ $kits->count() }}</h4>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-6 mb-3">
        <div class="card bg-light">
            <div class="card-body">
                <span class="float-right">
                    <i class="enbiosis-color fas fa-level-up-alt fa-3x"></i>
                </span>
                <h4 class="card-title"> Registered Kits</h4>
                <h4 class="card-text">{{ $kits->where('registered', '=', '1')->count() }}</h4>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-6 mb-3">
        <div class="card bg-light">
            <div class="card-body">
                <span class="float-right">
                    <i class="enbiosis-color fas fa-level-down-alt fa-3x"></i>

                </span>
                <h4 class="card-title">Not Registered Kits</h4>
                <h4 class="card-text">{{ $kits->where('registered', '=', '0')->count() }}</h4>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-6 mb-3">
        <div class="card bg-light">
            <div class="card-body">
                <span class="float-right">
                    <i class="enbiosis-color fas fa-globe fa-3x"></i>
                </span>
                <h4 class="card-title">Sold Online</h4>
                <h4 class="card-text">{{ $kits->where('sell_status', '=', 1)->count() }}</h4>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-6 mb-3">
        <div class="card bg-light">
            <div class="card-body">
                <span class="float-right">
                    <i class="enbiosis-color fas fa-power-off fa-3x"></i>
                </span>
                <h4 class="card-title">Sold Offline</h4>
                <h4 class="card-text">{{ $kits->where('sell_status', '=', 2)->count() }}</h4>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-6 mb-3">
        <div class="card bg-light">
            <div class="card-body">
                <span class="float-right">
                    <i class="enbiosis-color fas fa-shopping-basket fa-3x"></i>
                </span>
                <h4 class="card-title">Order Reseved</h4>
                <h4 class="card-text">{{ $kits->where('kit_status', '=', 0)->count() }}</h4>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-6 mb-3">
        <div class="card bg-light">
            <div class="card-body">
                <span class="float-right">
                    <i class="enbiosis-color fas fa-truck fa-3x"></i>
                </span>
                <h4 class="card-title">Kits Send To User</h4>
                <h4 class="card-text">{{ $kits->where('kit_status', '=', 1)->count() }}</h4>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-6 mb-3">
        <div class="card bg-light">
            <div class="card-body">
                <span class="float-right">
                    <i class="enbiosis-color fas fa-truck fa-3x" style="transform: rotateY(180deg)"></i>
                </span>
                <h4 class="card-title">Received From User</h4>
                <h4 class="card-text">{{ $kits->where('kit_status', '=', 2)->count() }}</h4>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-6 mb-3">
        <div class="card bg-light">
            <div class="card-body">
                <span class="float-right">
                    <i class="enbiosis-color fas fa-flask fa-3x"></i>
                </span>
                <h4 class="card-title">Send To Lab</h4>
                <h4 class="card-text">{{ $kits->where('kit_status', '=', 3)->count() }}</h4>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-6 mb-3">
        <div class="card bg-light">
            <div class="card-body">
                <span class="float-right">
                    <i class="enbiosis-color fas fa-poll fa-3x"></i>
                </span>
                <h4 class="card-title">Result Uploaded </h4>
                <h4 class="card-text">{{ $kits->where('kit_status', '=', 4)->count() }}</h4>
            </div>
        </div>
    </div> --}}
</div>

@endsection
@section('script')
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var RegistrationsData = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Registered Kits',       {{ $kits->where('registered', '=', '1')->count() }}],
          ['Not Registered Kits',   {{ $kits->where('registered', '=', '0')->count() }}],
        ]);

        var RegistrationsOptions = {
            title: 'Kits Registrations status',
            titleTextStyle: {fontSize: 16},
            is3D: true,
            legend: {position: 'right', textStyle: {fontSize: 16}},
            colors: ['#edd1a1', '#2699d6', '#59c4d9', '#4f4fa1', '#f59178', '#ffe8bf', '#e3614d', '#4f4fa1', '#59c4d9', '#fe7a6e', '#e67475', '#E8AEB0', '#DDCCFF', '#FFE7BF'] 
        };


        var OnOffData = google.visualization.arrayToDataTable([
          ['title', 'items'],
          ['Sold Online',       {{ $kits->where('sell_status', '=', '1')->count() }}],
          ['Sold Offline',   {{ $kits->where('sell_status', '=', '2')->count() }}],
        ]);

        var OnOffOptions = {
            title: 'sales distribution',
            titleTextStyle: {fontSize: 16},
            is3D: true,
            legend: {position: 'right', textStyle: {fontSize: 16}},
            colors: ['#edd1a1', '#2699d6', '#59c4d9', '#4f4fa1', '#f59178', '#ffe8bf', '#e3614d', '#4f4fa1', '#59c4d9', '#fe7a6e', '#e67475', '#E8AEB0', '#DDCCFF', '#FFE7BF'] 
        };

        var statusData = google.visualization.arrayToDataTable([
          ['title', 'items'],
          ['Order Reseved',       {{ $kits->where('kit_status', '=', '0')->count() }}],
          ['Kits Send To User',       {{ $kits->where('kit_status', '=', '1')->count() }}],
          ['Received From User',       {{ $kits->where('kit_status', '=', '2')->count() }}],
          ['Send To Lab',       {{ $kits->where('kit_status', '=', '3')->count() }}],
          ['Result Uploaded',   {{ $kits->where('kit_status', '=', '4')->count() }}],
        ]);

        var statusOptions = {
            title: 'Status distribution',
            titleTextStyle: {fontSize: 16, margin: '2px'},
            is3D: true,
            legend: {position: 'right', textStyle: {fontSize: 15}, maxLines: 5},
            colors: ['#edd1a1', '#2699d6', '#59c4d9', '#4f4fa1', '#f59178', '#ffe8bf', '#e3614d', '#4f4fa1', '#59c4d9', '#fe7a6e', '#e67475', '#E8AEB0', '#DDCCFF', '#FFE7BF'] 
        };

        var couponData = google.visualization.arrayToDataTable([
          ['title', 'items'],
          ['Available Coupons', {{ $coupons->where('used', '=', '0')->count() }}],
          ['used Coupons',      {{ $coupons->where('used', '=', '1')->count() }}],
        ]);

        var couponOptions = {
            title: 'Coupon Usage',
            titleTextStyle: {fontSize: 16, margin: '2px'},
            is3D: true,
            legend: {position: 'right', textStyle: {fontSize: 15}, maxLines: 5},
            colors: ['#edd1a1', '#2699d6', '#59c4d9', '#4f4fa1', '#f59178', '#ffe8bf', '#e3614d', '#4f4fa1', '#59c4d9', '#fe7a6e', '#e67475', '#E8AEB0', '#DDCCFF', '#FFE7BF'] 
        };

        var RegistrationsChart = new google.visualization.PieChart(document.getElementById('kit-re-nre'));

        var OnOffChart = new google.visualization.PieChart(document.getElementById('kit-on-off'));

        var statusChart = new google.visualization.PieChart(document.getElementById('kit-status'));

        var couponChart = new google.visualization.PieChart(document.getElementById('coupon'));

        // Registrations
        RegistrationsChart.draw(RegistrationsData, RegistrationsOptions);
        // sales distribution
        OnOffChart.draw(OnOffData, OnOffOptions);
        // status distribution
        statusChart.draw(statusData, statusOptions);
        // coupons
        couponChart.draw(couponData, couponOptions);
      }
    </script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', {'packages':['line']});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {

            var activityData = google.visualization.arrayToDataTable([
            ]);
            var activityData = new google.visualization.DataTable();
            activityData.addColumn('string', 'Day');
            activityData.addColumn('number', 'Super Admin');
            activityData.addColumn('number', 'Operation Admin');
            activityData.addColumn('number', 'Laboratory Admin');

            activityData.addRows(
                @json($logs['line1'])
            );

            var activity1Data = google.visualization.arrayToDataTable([
            ]);
            var activity1Data = new google.visualization.DataTable();
            activity1Data.addColumn('string', 'Day');
            activity1Data.addColumn('number', 'User Api');
            activity1Data.addColumn('number', 'Dietitian Api');

            activity1Data.addRows(
                @json($logs['line2'])
            );

            var activityOptions = {
                chart: {
                title: 'Admins Activity',
                subtitle: 'this data between ( Last 7 day )',
                },
                colors: ['#4f4fa1', '#59c4d9', '#f59178', '#ffe8bf', '#e3614d', '#4f4fa1', '#59c4d9', '#fe7a6e', '#e67475', '#E8AEB0', '#DDCCFF', '#FFE7BF'] 
            };

            
            var activity1Options = {
                chart: {
                title: 'Users Activity',
                subtitle: 'this data between  ( Last 7 day )',
                },
                colors: ['#e3614d', '#DDCCFF', '#FFE7BF'] 
            };

            var activityChart1 = new google.charts.Line(document.getElementById('activity2'));
            var activityChart = new google.charts.Line(document.getElementById('activity1'));
            // activity
            activityChart.draw(activityData, google.charts.Line.convertOptions(activityOptions));
            activityChart1.draw(activity1Data, google.charts.Line.convertOptions(activity1Options));
        }
    </script>
@endsection