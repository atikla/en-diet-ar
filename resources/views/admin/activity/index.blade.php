@extends('layouts.admin')
@section('styles')
    <style>
        .ul-logs {
            overflow: auto;
            max-height: 600px;
        }
        .list-group-item:hover {
            background-color: #f8f9fa;
        }
        .list-group-item a {
            text-decoration: none;
            color: #343a40;
        }
        .list-group-item a:hover {
            cursor: pointer;

        }
        .list-group-item .selecetd {
            color: #f8f9fa
        }
    </style>
@endsection
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-dark text-light">Activity</div>

                    <div class="card-body ">
                        You are logged in as Admin You are in Activity Section!
                    </div>
                </div>
                @include('include.messages')
                @include('include.form-errors')
            </div>
        </div>
    </div>
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="text-right">
                    {{-- {!! Form::open(['method'=>'POST', 'action' => ['AdminSurveyKitAnswerController@search'], 'style'=> 'display:inline']) !!}

                        {!! Form::label('search', 'Kit Code:') !!}
                        {!! Form::text('search', null, ['class'=>'form-control', 'style'=> 'display:inline; width:initial', 'required']) !!}

                        {!! Form::submit('search', ['class'=>'btn btn-outline-success mr-2 mb-1']) !!}

                    {!! Form::close() !!} --}}
                </div>
            </div>
            <div class="col-lg-2 col-md-12 mt-3">
                <ul class="list-group ul-logs">
                    @foreach ($logs as $key => $log)
                        <li class="list-group-item {{$loop->first ? 'bg-secondary' : ''}}">
                            <a class="{{$loop->first ? 'selecetd' : ''}} w-100" href="{{ route('admin.activity.show', \Carbon\Carbon::parse($key)->isoFormat('Y-M-D')) }}">
                                {{ \Carbon\Carbon::parse($key)->isoFormat(' DD MMMM ,dddd Y') }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="col-lg-10 col-md-12 mt-3">
                @if ( $logs->count() > 0 )
                    <div class="text-left">
                        <h3>
                            This Log For {{ \Carbon\Carbon::parse($logs->first()->first()->created_at)->isoFormat(' DD MMMM ,dddd Y') }}
                        </h3>
                        <small>  in This day we have {{ $logs->first()->count() }} item</small>
                    </div>
                    <table class="table table-responsive-lg table-striped">
                        <thead class="thead-light">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">log_name</th>
                            <th scope="col">description</th>
                            <th scope="col">subject</th>
                            <th scope="col">causer</th>
                            <th scope="col">properties</th>
                            <th scope="col">created</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($logs->first() as $log)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$log->log_name}}</td>
                                    <td>{{$log->description}}</td>
                                    @if ( $log->subject_type )
                                        <td>{{str_replace('App\\', '', $log->subject_type)}}</td>
                                    @else 
                                    <td>****</td>
                                    @endif
                                    @if ( $log->causer_type )
                                        <td>{{$log->causer ? $log->causer->name : ''}} - {{$log->causer ? $log->causer->strRole ? $log->causer->strRole : '' : ''}}</td>
                                    @else 
                                    <td>****</td>
                                    @endif
                                    <td> 
                                        <button type="button" class="btn btn-outline-success update mr-2 mb-1" data-toggle="modal" data-target="#show_{{$log->id}}">
                                            Show
                                        </button>
                                    </td>
                                     <!-- Modal -->
                                    <div class="modal fade" id="show_{{$log->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalCenterTitle">Show log - {{ $log->id }}</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                </div>
                                                <div class="modal-body">
                                                    <p> ip : {{ $log->getExtraProperty('ip') }} </p>
                                                    <div class="row justify-content-center">
                                                        @if ($log->getExtraProperty('old'))
                                                            <div class="col-12">
                                                                @if ( is_array( $log->getExtraProperty('old') ) )
                                                                <p>Old</p>
                                                                <ul class="list-group">
                                                                
                                                                    @foreach ($log->getExtraProperty('old') as $key => $item)
                                                                        @if (is_array($item))
                                                                            <li class="list-group-item  bg-danger text-light">{{ $key }}: @json($item)</li>
                                                                        @else 
                                                                            <li class="list-group-item  bg-danger text-light">{{ $key }}: {{ $item }}</li>
                                                                        @endif
                                                                    @endforeach
                                                                </ul>
                                                                @else
                                                                    <p> old : {{ $log->getExtraProperty('old') }} </p>
                                                                @endif
                                                            </div>
                                                        @endif
                                                        @if ($log->getExtraProperty('attributes'))
                                                            <div class="col-12">
                                                                @if ( is_array( $log->getExtraProperty('attributes') ) )
                                                                <p>New</p>
                                                                <ul class="list-group">
                                                                
                                                                    @foreach ($log->getExtraProperty('attributes') as $key => $item)
                                                                        @if (is_array($item))
                                                                            <li class="list-group-item  bg-success text-light">{{ $key }}: @json($item)</li>
                                                                        @else
                                                                            <li class="list-group-item  bg-success text-light">{{ $key }}: {{ $item }}</li>
                                                                        @endif
                                                                    @endforeach
                                                                </ul>
                                                                @else
                                                                    <p> New : {{ $log->getExtraProperty('attributes') }} </p>
                                                                @endif
                                                            </div>
                                                        @endif
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <td>{{$log->created_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $( document ).ready(function() {
        });
    </script>
@endsection
