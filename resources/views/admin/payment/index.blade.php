@extends('layouts.admin')
@section('content')
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-dark text-light">Payment</div>

                    <div class="card-body ">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        You are logged in as Admin You are in Payment Section!
                    </div>
                </div>
                @include('include.messages')
                @include('include.form-errors')
            </div>
        </div>
    </div>
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="text-left">
                    <h2>TOPLAM: {{$sum}} TL</h2>
                </div>
                <table class="table table-responsive-lg table-striped">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">amount</th>
                        <th scope="col">Installment</th>
                        <th scope="col">Cart No</th>
                        <th scope="col">Name</th>
                        <th scope="col">Response</th>
                        <th scope="col">Show Order</th>
                        <th scope="col">Created</th>
                    </tr>
                    </thead>
                    <tbody>
                        @if ($payments)
                            @foreach ($payments as $payment)
                                <tr>
                                    <td>{{ $payment->id }}</td>
                                    <td>{{ $payment->amount }} TL</td>
                                    <td>{{ $payment->taksit == 0 || $payment->taksit == 1 ? 'Tek Çekim': $payment->taksit . ' Taksit '}} </td>
                                    <td>{{ $payment->maskedCreditCard }}</td>
                                    <td>{{ $payment->EXTRA_CARDHOLDERNAME }}</td>
                                    <td>{{ $payment->Response }} </td>
                                    <td> <a href="{{ route('admin.orders.show', $payment->order ? $payment->order->id : -1111111) }}">Show Order</a> </td>
                                    <td>{{$payment->created_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}</td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
            <div class="text-center my-4">
                {{$payments->links()}}
            </div>
        </div>
    </div>
@endsection
