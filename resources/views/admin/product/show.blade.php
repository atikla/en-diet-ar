@extends('layouts.admin')
@section('content')
<div class="mt-3 mx-5">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-dark text-light">Products</div>

                <div class="card-body ">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    You are logged in as Admin You are in Product Show Section!
                </div>
            </div>
            @include('include.messages')
        </div>
    </div>
</div>
<div class="mt-3 mx-5">
    <div class="row justify-content-center">
        <div class="col-lg-4 col-12 mb-lg-0 mb-5">
            <div class="card pb-3">
                <div class="card-header bg-secondary text-light mb-3">Product Photo</div>
                <div class="card-body text-center py-5 my-5">
                    <img style="max-width:300px" class='img-rounded'src="{{$product->photo ? url('/') . '/img/private/' . $product->photo->path : url('/') . '/img/private/default.png'}}" alt="">
                </div>
            </div>
        </div>
        <div class="col-lg-8 col-12 mb-5">
            <div class="card pb-3">
                <div class="card-header bg-secondary text-light mb-3">Product Info</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 mb-5">
                            <div class="text-right">
                                <a   href="{{route('admin.products.edit', $product->id)}}"	class="btn btn-outline-success update mr-2 mb-1">Update</a>
                                {!! Form::open(['method'=>'DELETE', 'action' => ['AdminProductsController@destroy', $product->id], 'style'=> 'display:inline']) !!}
                                    {!! Form::submit('Delete', ['class'=>'btn btn-outline-danger mr-2 mb-1','onclick'=>"return confirm('Silmek isteğinizden Emin misiniz?')"]) !!}
                                {!! Form::close() !!} 
                            </div>
                        </div>
                        <div class="col-lg-6 col-12 mb-3">
                            <h4>Product Name</h4>
                            <hr class="w-25" style="margin-right: 100%" >
                            <h5 class="ml-3">{{$product->name}}</h5>
                        </div>
                        <div class="col-lg-6 col-12 mb-3">
                            <h4>Product Slug (URL)</h4>
                            <hr class="w-25" style="margin-right: 100%" >
                            <h5 class="ml-3">{{$product->slug}}</h5>
                        </div>
                        <div class="col-lg-3 col-6 mb-3">
                            <h4>Product Price</h4>
                            <hr class="w-25" style="margin-right: 100%" >
                            <h5 class="ml-3">{{$product->price}} TL</h5>
                        </div>
                        <div class="col-lg-3 col-6 mb-3">
                            <h4>Product Discount</h4>
                            <hr class="w-25" style="margin-right: 100%" >
                            <h5 class="ml-3">{!! $product->discount !!} TL</h5>
                        </div>
                        <div class="col-lg-3 col-12 mb-3">
                            <h4>Product Stok</h4>
                            <hr class="w-25" style="margin-right: 100%" >
                            <h5 class="ml-3">{{$product->stok}} Piece</h5>
                        </div>
                        <div class="col-lg-3 col-12 mb-3">
                            <h4>number of kit</h4>
                            <hr class="w-25" style="margin-right: 100%" >
                            <h5 class="ml-3">{{$product->number_of_kit}} </h5>
                        </div>
                        <div class="col-12 mb-3">
                            <h4>Product Description</h4>
                            <hr class="w-25" style="margin-right: 100%" >
                            <h5 class="ml-3">{!! $product->description !!}</h5>
                        </div>
                    </div>
                    
                    {{-- <img style="max-width:300px" class='img-rounded'src="{{$product->photo ? url('/') . '/img/private/' . $product->photo->path : url('/') . '/img/private/default.png'}}" alt=""> --}}
                </div>
            </div>
        </div>
        {{-- <div class="col-12">
            
            
        </div> --}}
    </div>
</div>
@endsection