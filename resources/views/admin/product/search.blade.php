@extends('layouts.admin')
@section('content')
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-dark text-light">Products</div>

                    <div class="card-body ">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        You are logged in as Admin You are in Products Search Resul Section!
                    </div>
                </div>
                @include('include.messages')
            </div>
        </div>
    </div>
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="text-right">
                    <a  href="{{route('admin.products.index')}}" class="header-table btn btn-outline-success mb-2">Go to product index page</a>
                    {!! Form::open(['method'=>'POST', 'action' => ['AdminProductsController@search'], 'style'=> 'display:inline']) !!}

                    {!! Form::label('search', 'Query:') !!}
                    {!! Form::text('search', null, ['class'=>'form-control', 'style'=> 'display:inline; width:initial', 'required']) !!}

                    {!! Form::submit('search', ['class'=>'btn btn-outline-success mr-2 mb-1']) !!}

                {!! Form::close() !!} 
                </div>
                <table class="table table-responsive-lg table-striped">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Photo</th>
                        <th scope="col">Owner</th>
                        <th scope="col">Name</th>
                        <th scope="col">Price</th>
                        <th scope="col">Discount Price</th>
                        <th scope="col">Slug</th>
                        <th scope="col">Stok</th>
                        <th scope="col">number of kit</th>
                        <th scope="col">Description</th>
                        <th scope="col">Created</th>
                        <th scope="col">Updated</th>
                        <th scope="col">process</th>
                    </tr>
                    </thead>
                    <tbody>
                        @if ($products)
                        @foreach ($products as $product)
                            <tr>
                                <td>{{$product->id}}</td>
                                <td><img class='img-rounded' width="80" src="{{$product->photo ? url('/') . '/img/private/' . $product->photo->path : url('/') . '/img/private/default.png'}}" alt=""></td>
                                <td>{{$product->admin->name}}</td>
                                <td>{{$product->name}}</td>
                                <td>{{$product->price}}</td>
                                {!! $product->discount!!}
                                <td><a href="{{ route('product', $product->slug) }}"> Show In Shop </a></td>
                                <td>{{$product->stok}}</td>
                                <td>{{$product->number_of_kit}}</td>
                                <td>
                                    {!! 
                                        \Illuminate\Support\Str::words($product->description, 5, $end=' <a href="'. route('admin.products.show', [ $product->id, $product->slug ] ) . '"> read more </a>')
                                    !!}
                                </td>
                                <td>{{$product->created_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}</td>
                                <td>{{$product->updated_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}</td>
                                <td>
                                    <a href="{{route('admin.products.show', [ $product->id, $product->slug ] )}}" class="btn btn-outline-info mr-2 mb-1" >Show</a>
                                    <a   href="{{route('admin.products.edit', $product->id)}}"	class="btn btn-outline-success update mr-2 mb-1">Update</a>
                                    {!! Form::open(['method'=>'DELETE', 'action' => ['AdminProductsController@destroy', $product->id], 'style'=> 'display:inline']) !!}
                                        {!! Form::submit('Delete', ['class'=>'btn btn-outline-danger mr-2 mb-1','onclick'=>"return confirm('Silmek isteğinizden Emin misiniz?')"]) !!}
                                    {!! Form::close() !!} 
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection