@extends('layouts.admin')

@section('content')
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-dark text-light">Edit Product</div>

                    <div class="card-body ">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        You are logged in as Admin You are in Edit Product Section!
                    </div>
                </div>
                @include('include.form-errors')
            </div>
        </div>
    </div>
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-lg-4 col-12 mb-lg-0 mb-5">
                <div class="card pb-3">
                    <div class="card-header bg-secondary text-light mb-3">Product Photo</div>
                    <div class="card-body text-center py-5 my-5">
                        <img style="max-width:300px" class='img-rounded'src="{{$product->photo ? url('/') . '/img/private/' . $product->photo->path : url('/') . '/img/private/default.png'}}" alt="">
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-12">
                {!! Form::model($product, ['method'=>'PATCH', 'action' => ['AdminProductsController@update', $product->id], 'files'=>true]) !!}
                {{-- {!! Form::model($user, ['method'=>'PATCH', 'action' => ['AdminUsersController@update', $user->id], 'files'=>true]) !!} --}}
                <div class="row">
                    <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
            
                        {{-- {!! Form::label('name', 'Name') !!} --}}
                        <label for="name">Name <sup class="text-danger">*</sup> :</label>
                        {!! Form::text('name', null, ['class'=>'form-control']) !!}
                
                    </div>
                
                    <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                
                        {{-- {!! Form::label('slug', 'Slug<small><sup>12</sup></small>:') !!} --}}
                        <label for="slug">Slug <sup class="text-danger">*</sup> :</label>
                        {!! Form::text('slug', null, ['class'=>'form-control']) !!}
                        
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
            
                        {{-- {!! Form::label('price', 'Price<small><sup>12</sup></small>:') !!} --}}
                        <label for="price">Price <sup class="text-danger">*</sup> :</label>
                        {!! Form::number('price', null, ['class'=>'form-control']) !!}
                
                    </div>
                
                    <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                
                        {{-- {!! Form::label('discount_price', 'Discount Price<small><sup>12</sup></small>:') !!} --}}
                        <label for="discount_price">Discount %:</label>
                        {!! Form::number('discount_price', null, ['min' => '1', 'max' => '80', 'class'=>'form-control']) !!}
                        
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
            
                        {{-- {!! Form::label('price', 'Price<small><sup>12</sup></small>:') !!} --}}
                        <label for="photo_id">Photo <sup class="text-danger">*</sup> :</label><br>
                        {!! Form::file('photo_id', null, ['class'=>'form-control']) !!}
                
                    </div>
                
                    <div class="form-group col-lg-3 col-md-3 col-sm-12 col-xs-12">
                
                        {{-- {!! Form::label('discount_price', 'Discount Price<small><sup>12</sup></small>:') !!} --}}
                        <label for="stok">stok <sup class="text-danger">*</sup> :</label>
                        {!! Form::number('stok', null, ['class'=>'form-control']) !!}
                        
                    </div>
                    <div class="form-group col-lg-3 col-md-3 col-sm-12 col-xs-12">
                
                        {{-- {!! Form::label('discount_price', 'Discount Price<small><sup>12</sup></small>:') !!} --}}
                        <label for="number_of_kit"> number of kit in this product <sup class="text-danger">*</sup> :</label>
                        {!! Form::number('number_of_kit', null, ['class'=>'form-control']) !!}
                        
                    </div>
                </div>
                <div class="form-group">
                
                    {{-- {!! Form::label('discount_price', 'Discount Price<small><sup>12</sup></small>:') !!} --}}
                    <label for="description">Description <sup class="text-danger">*</sup> :</label>
                    {!! Form::textarea('description', null, ['class'=>'form-control']) !!}
                    
                </div>

                <div class="form-group">
                    {!! Form::submit('Create Product', ['class'=>'btn btn-primary']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    @section('script')
        <script src="https://cdn.ckeditor.com/4.13.0/basic/ckeditor.js"></script>
        <script>
             $(document).ready(function(){
                CKEDITOR.replace( 'description', {language: 'tr'});
             });
        </script>
    @endsection
@endsection