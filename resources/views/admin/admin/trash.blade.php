@extends('layouts.admin')
@section('content')
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-dark text-light">Admins</div>

                    <div class="card-body ">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        You are logged in as Super Admin You are in Admins Trash Section!
                    </div>
                </div>
                @include('include.messages')
                @include('include.form-errors')
            </div>
        </div>
    </div>
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="text-right">
                    <a  href="{{route('admin.admins.index')}}" class="header-table btn btn-outline-success mb-2">Admins</a>
                    {{-- {!! Form::open(['method'=>'POST', 'action' => ['AdminadminsController@search'], 'style'=> 'display:inline']) !!}

                        {!! Form::label('search', 'Query:') !!}
                        {!! Form::text('search', null, ['class'=>'form-control', 'style'=> 'display:inline; width:initial', 'required']) !!}

                        {!! Form::submit('search', ['class'=>'btn btn-outline-success mr-2 mb-1']) !!}

                    {!! Form::close() !!}  --}}
                </div>
                <table class="table table-responsive-lg table-striped">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Deleted</th>
                        <th scope="col">process</th>
                    </tr>
                    </thead>
                    <tbody>
                        @if ($admins)
                            @foreach ($admins as $admin)
                                
                                <tr {{ $admin->id == auth()->user()->id ? 'class=table-success' : '' }}>
                                    <td>{{$admin->id}}</td>
                                    <td>{{$admin->name}}</td>
                                    <td>{{$admin->email}}</td>
                                    <td>{{$admin->deleted_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}</td>
                                    <td>
                                        <a  href="{{route('admin.admins.restore', $admin->id)}}" class="btn btn-outline-success update mr-2 mb-1">Restore</a>
                                        {!! Form::open(['method'=>'DELETE', 'action' => ['AdminAdminsController@destroy', $admin->id, 'soft' => '1'], 'style'=> 'display:inline']) !!}
                                            {!! Form::submit('Delete Permanently', ['class'=>'btn btn-outline-danger mr-2 mb-1','onclick'=>"return confirm('Silmek isteğinizden Emin misiniz?')"]) !!}
                                        {!! Form::close() !!} 
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
            <div class="text-center my-4">
                {{$admins->links()}}
            </div>

        </div>
    </div>
@endsection
