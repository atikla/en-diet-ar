@extends('layouts.admin')
@section('content')
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-dark text-light">Admins</div>

                    <div class="card-body ">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        You are logged in as Super Admin You are in Admins Section!
                    </div>
                </div>
                @include('include.messages')
                @include('include.form-errors')
            </div>
        </div>
    </div>
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="text-right">
                    <button type="button" class="btn btn-outline-success update mr-2 mb-1" data-toggle="modal" data-target="#id_create">
                        Create Admin
                    </button>
                    <a  href="{{route('admin.admins.trash')}}" class="header-table btn btn-outline-danger mb-2">admin trash</a>
                    {{-- {!! Form::open(['method'=>'POST', 'action' => ['AdminadminsController@search'], 'style'=> 'display:inline']) !!}

                        {!! Form::label('search', 'Query:') !!}
                        {!! Form::text('search', null, ['class'=>'form-control', 'style'=> 'display:inline; width:initial', 'required']) !!}

                        {!! Form::submit('search', ['class'=>'btn btn-outline-success mr-2 mb-1']) !!}

                    {!! Form::close() !!}  --}}
                </div>
                <table class="table table-responsive-lg table-striped">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Photo</th>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Status</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Role</th>
                        <th scope="col">Created</th>
                        <th scope="col">Updated</th>
                        <th scope="col">process</th>
                    </tr>
                    </thead>
                    <tbody>
                        @if ($admins)
                            @foreach ($admins as $admin)
                                @if ( $admin->id == auth()->user()->id || $admin->email == 'admin@enbiosis.com')
                                    {{-- <tr>
                                        <td colspan ="10" class="text-center">
                                            <a href="{{route('admin.admins.show', $admin->id)}}" class="btn btn-outline-info mr-2 mb-1" >Show You Profile</a>
                                        </td>
                                    </tr> --}}
                                @else 
                                <tr {{ $admin->id == auth()->user()->id ? 'class=table-success' : '' }}>
                                    <td>{{$admin->id}}</td>
                                    <td><img class='img-rounded' height="50" src="{{$admin->photo ? url('/') . '/img/private/' . $admin->photo->path : url('/') . '/img/private/default.png'}}" alt=""></td>
                                    <td>{{$admin->name}}</td>
                                    <td>{{$admin->email}}</td>
                                    <td>{!! $admin->active ? '<span class="text-success"> is Active</span>' : '<span class="text-danger"> is Not Active</span>' !!}</td>
                                    <td>{{$admin->phone}}</td>
                                    <td>{{$admin->strRole}}</td>
                                    <td>{{$admin->created_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}</td>
                                    <td>{{$admin->updated_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}</td>
                                    <td>
                                        <a href="{{route('admin.admins.show', $admin->id)}}" class="btn btn-outline-info mr-2 mb-1" >Show Profile</a>
                                        <a   href="{{route('admin.admins.edit', $admin->id)}}"	class="btn btn-outline-success update mr-2 mb-1">Update</a>
                                        {!! Form::open(['method'=>'DELETE', 'action' => ['AdminAdminsController@destroy', $admin->id], 'style'=> 'display:inline']) !!}
                                            {!! Form::submit('Delete', ['class'=>'btn btn-outline-danger mr-2 mb-1','onclick'=>"return confirm('Silmek isteğinizden Emin misiniz?')"]) !!}
                                        {!! Form::close() !!} 
                                    </td>
                                </tr>
                                @endif
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
            <div class="text-center my-4">
                {{$admins->links()}}
            </div>

        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="id_create" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Create Admin</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    {{-- {!! Form::open(['method'=>'DELETE', 'action' => ['AdminCouponController@destroy', $coupon->id], 'style'=> 'display:inline', 'id' => 'id_form_' . $coupon->code]) !!} --}}
                    {!! Form::open(['method'=>'POST', 'action' => 'AdminAdminsController@store', 'style'=> 'display:inline', 'id' => 'id_form_create', 'files'=>true]) !!}
                        <div class="form-group">

                            {!! Form::label('name', 'Name') !!}
                            {!! Form::text('name', null, ['class'=>'form-control']) !!}
            
                        </div>
                        <div class="form-group">

                            {!! Form::label('email', 'Email') !!}
                            {!! Form::email('email', null, ['class'=>'form-control']) !!}
            
                        </div>
                        <div class="form-group">

                            {!! Form::label('phone', 'phone') !!}
                            {!! Form::number('phone', null, ['class'=>'form-control']) !!}
            
                        </div>
                        <div class="form-group">
            
                            
                            {!! Form::label('active', 'Status') !!}
                            {!! Form::select('active', [0 => 'Not active', 1 => 'Active'], 0, ['class'=>'form-control']) !!}
                            
                        </div>
                        <div class="form-group">
            
                            {!! Form::label('role', 'role') !!}
                            {!! Form::select('role', [0 => 'Super Admin', 1 => 'Operation Admin', 2 => 'Laboratory Admin', 3 => 'Draft'], 3, ['class'=>'form-control']) !!}
                            
                        </div>
                        <div class="form-group">
            
                            {!! Form::label('photo', 'photo') !!}
                            {!! Form::file('photo') !!}
                            
                        </div>
                        <div class="form-group">
            
                            {!! Form::label('password', 'Password:') !!}
                            {!! Form::password('password', ['class'=>'form-control']) !!}
                            
                        </div>
                        <div class="form-group">
            
                            {!! Form::label('password_confirmation', 'Password Confirmation:') !!}
                            {!! Form::password('password_confirmation', ['class'=>'form-control', 'autocomplete' =>'on']) !!}
                            
                    </div> 
                    {!! Form::close() !!} 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary" onclick="event.preventDefault();document.getElementById('id_form_create').submit();">
                        Save Changes
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection
