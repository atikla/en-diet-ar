@extends('layouts.admin')
@section('content')
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-dark text-light">campaigns</div>
                    <div class="card-body ">
                        You are logged in as Admin You are in show campaign Section!
                    </div>
                </div>
                @include('include.form-errors')
            </div>
        </div>
    </div>
    <div class="mt-3 mx-5">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-info text-light">campaign info</div>
                    <div class="card-body ">
                        <button type="button" class="btn btn-outline-success update mr-2 mb-1" data-toggle="modal" data-target="#id_{{$campaign->code}}">
                            update
                        </button>
                        
                        <!-- Modal -->
                        <div class="modal fade" id="id_{{$campaign->code}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalCenterTitle">Update campaign {{$campaign->code}}</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        {!! Form::model($campaign, ['method'=>'PATCH', 'action' => ['AdminCampaignController@update', $campaign->code],'style'=> 'display:inline', 'id' => 'id_form_' . $campaign->id]) !!}
                                        <div class="form-group">
                            
                                            {!! Form::label('reference_code', 'Reference:') !!}
                                            {!! Form::select('reference_code', $dietitians, null,['class'=>'form-control', 'disabled']) !!}
                                            
                                        </div>
    
                                        <div class="form-group">
                            
                                            {!! Form::label('code', 'Campaign Code:') !!} <small class="text-danger">max: 10 characters</small>
                                            {!! Form::text('code', null, ['class'=>'form-control', 'disabled']) !!}
                                            
                                        </div>
    
                                        <div class="form-group">
    
                                            {!! Form::label('discount', 'Discount ( 1 - 100 % ) :') !!}
                                            {!! Form::number('discount', null, ['class'=>'form-control', 'disabled']) !!}
                            
                                        </div>
    
                                        <div class="form-group">
    
                                            {!! Form::label('expires_at', 'Expires At:') !!} <small class="text-danger">default: after 4 day</small>
                                            {!! Form::date('expires_at', $campaign->expires_at, ['class'=>'form-control']) !!}
            
                                        </div>
    
                                        <div class="form-group">
                            
                                            {!! Form::label('note', 'note:') !!}
                                            {!! Form::text('note', null, ['class'=>'form-control']) !!}
                                            
                                        </div>
                                        {!! Form::close() !!} 
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button class="btn btn-primary" onclick="
                                            event.preventDefault();
                                            document.getElementById('id_form_{{$campaign->id}}').submit();
                                        ">Save Changes
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                       <ul>
                           <li>campaign code : {{ $campaign->code }}</li>
                           <li>campaign discount : {{ $campaign->discount }} %</li>
                           <li>campaign note : {{ $campaign->note }}</li>
                           <li>campaign Reference: {!! $campaign->reference ? $campaign->reference . '<a  href="' . route('admin.dietitians.show', $campaign->dietitian->id). '" class="btn btn-outline-success ml-2">show profile </a>'  : '<sapn class="text-danger">no reference</span>' !!}</li>
                           <li>campaign usege ( order count ) : {{ $campaign->order()->count() }} order</li>
                           <li>campaign Expires At  : {{$campaign->expires_at->isoFormat(' DD MMM ,ddd Y')}}</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-12 mt-4">
                @if (!$campaign->order->isEmpty())
                    <table class="table table-responsive-lg table-striped">
                        <thead class="thead-light">
                        <tr>
                        <th>#</th>
                        <th>Traking Number</th>
                        <th>name</th>
                        <th>phone</th>
                        <th>address</th>
                        <th>dietitian_code</th>
                        <th>Created</th>
                        <th>Updated</th>
                        <th>process</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($campaign->order as $order)
                                <tr>
                                    <td>{{$order->id}}</td>
                                    <td>{{$order->tracking}}</td>
                                    <td>{{$order->name}}</td>
                                    <td>{{$order->phone}}</td>
                                    <td>{{$order->address}} - {{$order->city ? $order->city->city : ''}} - {{ $order->county ? $order->county->county : '' }}</td>
                                    <td>{{$order->dietitian_code ?? 'not Entered'}}</td>
                                    <td>{{$order->updated_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}</td>
                                    <td>{{$order->created_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}</td>   
                                    <td>
                                        <a href="{{route('admin.orders.show', $order->id)}}" class="btn btn-outline-info mr-2 mb-1" >Show Order Details</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @else
                    <div class="text-center">
                        <h3 class="text-danger">THIS CAMPAIGN DONT HAVE ANY ORDER</h3>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection