@extends('layouts.admin')
@section('content')
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-dark text-light">campaigns</div>
                    <div class="card-body ">
                        You are logged in as Admin You are in campaigns Section!
                    </div>
                </div>
                @include('include.form-errors')
            </div>
        </div>
    </div>
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="text-right">
                    <a  href="{{route('admin.campaigns.index')}}" class="btn btn-outline-info">Available campaigns</a>
                    <a  href="{{route('admin.campaigns.not')}}" class="btn btn-outline-danger">Not Available campaigns</a>
                    <button type="button" class="btn btn-outline-success update " data-toggle="modal" data-target="#id_create">
                        Create campaigns
                    </button>
                </div>
                    <!-- Modal -->
                <div class="modal fade" id="id_create" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle">Create campaigns</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                            <div class="modal-body">
                                {{-- {!! Form::open(['method'=>'DELETE', 'action' => ['AdmincampaignController@destroy', $campaign->id], 'style'=> 'display:inline', 'id' => 'id_form_' . $campaign->code]) !!} --}}
                                {!! Form::open(['method'=>'POST', 'action' => 'AdminCampaignController@store', 'style'=> 'display:inline', 'id' => 'id_form_create']) !!}
                                    <div class="form-group">
                            
                                        {!! Form::label('reference_code', 'Reference:') !!}
                                        {!! Form::select('reference_code', $dietitians, null,['class'=>'form-control', 'placeholder'=>'Please select ...']) !!}
                                        
                                    </div>

                                    <div class="form-group">
                        
                                        {!! Form::label('code', 'Campaign Code:') !!} <small class="text-danger">max: 10 characters</small>
                                        {!! Form::text('code', null, ['class'=>'form-control']) !!}
                                        
                                    </div>

                                    <div class="form-group">

                                        {!! Form::label('discount', 'Discount ( 1 - 100 % ) :') !!}
                                        {!! Form::number('discount', null, ['class'=>'form-control', 'min'=> '1', 'max' => '100']) !!}
                        
                                    </div>

                                    <div class="form-group">

                                        {!! Form::label('expires_at', 'Expires At:') !!} <small class="text-danger">default: after 4 day</small>
                                        {!! Form::date('expires_at', date('Y-m-d', strtotime("+4 days")), ['class'=>'form-control']) !!}
        
                                    </div>

                                    <div class="form-group">
                        
                                        {!! Form::label('note', 'note:') !!}
                                        {!! Form::text('note', 'new Campaign', ['class'=>'form-control']) !!}
                                        
                                    </div>
                                    
                                {!! Form::close() !!} 
                            </div>
                            <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button class="btn btn-primary" onclick="
                                event.preventDefault();
                                if(getElementById('discount').value  > 100 || getElementById('discount').value  < 1 ){
                                    alert('discount  100 ve 1 arasinda bir deger almalidir');return 0;
                                }
                                document.getElementById('id_form_create').submit();
                            ">
                            Save Changes</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-12">
                        <table class="table table-responsive-lg table-striped w-100">
                            <h4> {{ $title }}</h4>
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Code</th>
                                <th scope="col">Discount</th>
                                <th scope="col">Note</th>
                                <th scope="col">Reference</th>
                                @if ($title == 'Available campaigns')
                                    <th scope="col">campaign page</th>
                                @endif
                                <th scope="col">Created</th>
                                <th scope="col">Expires At</th>
                                <th scope="col">Order Count</th>
                                <th scope="col">process</th>
                            </tr>
                            </thead>
                            <tbody>
                                @if ($campaigns)
                                    @foreach ($campaigns as $campaign)
                                        <tr>
                                            <td>{{$campaign->id}}</td>
                                            <td>{{$campaign->code}}</td>
                                            <td>{{$campaign->discount}} %</td>
                                            <td>{{$campaign->note}}</td>
                                            <td>{!! $campaign->reference ? $campaign->reference . '<a  href="' . route('admin.dietitians.show', $campaign->dietitian->id). '" class="btn btn-outline-success ml-2">show profile </a>'  : '<sapn class="text-danger">no reference</span>' !!}</td>
                                            @if ($title == 'Available campaigns')
                                                <td class="text-success"><a target="_blank" href="{{route('checkout.Campaign', strtolower($campaign->code))}}">Show</a></td>
                                            @endif
                                            <td>{{$campaign->created_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}</td>
                                            <td>{{$campaign->expires_at->isoFormat(' DD MMM ,ddd Y')}}</td>
                                            <td>{{ $campaign->order()->count() }}</td>
                                            <td>
                                                <a  href="{{route('admin.campaigns.show', $campaign->code)}}" class="btn btn-outline-success mb-1">Show</a>
                                                {!! Form::open(['method'=>'DELETE', 'action' => ['AdminCampaignController@destroy', $campaign->id], 'style'=> 'display:inline']) !!}
                                                    {!! Form::submit('Delete', ['class'=>'btn btn-outline-danger mr-2 mb-1','onclick'=>"return confirm('Silmek isteğinizden Emin misiniz?')"]) !!}
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                        <div>
                            {{ $campaigns->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')

<script type="text/javascript">
    $(document).ready(function(){
        
    });
    </script>
@endsection
