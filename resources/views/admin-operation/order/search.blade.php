@extends('layouts.admin-operation')
@section('content')
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-dark text-light">Orders</div>

                    <div class="card-body ">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        You are logged in as Operation Admin You are in Orders Section!
                    </div>
                </div>
                @include('include.messages')
                @include('include.form-errors')
            </div>
        </div>
    </div>
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="text-right">
                    {{-- <a  href="{{route('admin.users.create')}}" class="header-table btn btn-outline-success mb-2">Create User</a>
                    <a  href="{{route('admin.users.trash')}}" class="header-table btn btn-outline-danger mb-2">User trash</a> --}}
                    <a  href="{{route('operation.orders.index')}}" class="header-table btn btn-outline-success mb-2">Go to order index page</a>
                    {!! Form::open(['method'=>'POST', 'action' => ['OperationAdminController@search'], 'style'=> 'display:inline']) !!}

                        {!! Form::label('search', 'Query:') !!}
                        {!! Form::text('search', null, ['class'=>'form-control', 'style'=> 'display:inline; width:initial', 'required']) !!}

                        {!! Form::submit('search', ['class'=>'btn btn-outline-success mr-2 mb-1']) !!}

                    {!! Form::close() !!} 
                </div>
                <table class="table table-responsive-lg table-striped">
                    <thead class="thead-light">
                    <tr>
                       <th>#</th>
                       <th>Traking Number</th>
                       <th>name</th>
                       <th>phone</th>
                       <th>address</th>
                       <th>dietitian_code</th>
                       <th>Created</th>
                       <th>Updated</th>
                       <th>process</th>
                    </tr>
                    </thead>
                    <tbody>
                        @if ($orders)
                            @foreach ($orders as $order)
                            <tr>
                                <td>{{$order->id}}</td>
                                <td>{{$order->tracking}}</td>
                                <td>{{$order->name}}</td>
                                <td>{{$order->phone}}</td>
                                <td>{{$order->address}} - {{$order->city ? $order->city->city : ''}} - {{ $order->county ? $order->county->county : '' }}</td>
                                <td>{{$order->dietitian_code ?? 'not Entered'}}</td>
                                <td>{{$order->updated_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}</td>
                                <td>{{$order->created_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}</td>   
                                <td>
                                    <a href="{{route('operation.orders.show', $order->id)}}" class="btn btn-outline-info mr-2 mb-1" >Show Order Details</a>
                                </td>
                           </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    {{-- {{print_r($orders)}} --}}
@endsection