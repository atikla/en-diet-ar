@extends('layouts.admin-operation')
@section('content')
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-dark text-light">Order traking number {{$order->tracking}}</div>

                    <div class="card-body ">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        You are logged in as Operation Admin You are in Orders Section!
                    </div>
                </div>
                @include('include.messages')
            </div>
        </div>
    </div>
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-lg-3 col-md-6 col-12 mt-3 mx-0">
                <div class="card">
                    <div class="card-header bg-info text-light">Costumer info</div>

                    <div class="card-body ">
                        <p>Costumer IP {{$order->payment->clientIp}}</p>
                        <p>Placed Order At :  {{$order->updated_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss')}}</p>
                        <p>costumer name :  {{$order->name}}</p>
                        <p>costumer Email :  {{$order->email}}</p>
                        <p>costumer Phone :  {{$order->phone}}</p>
                        <p>costumer Reference :  {{$order->dietitian_code ?? 'not Entered'}} (dietitian code)</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-12 mt-3">
                <div class="card">
                    <div class="card-header bg-primary text-light">Addresses</div>

                    <div class="card-body ">
                        <u><h6>shipping address</h6></u>
                        <p>City :  {{$order->city ? $order->city->city : '***'}}</p>
                        <p>County :  {{$order->county ? $order->county->county : '***'}}</p>
                        <p>Address :  {{$order->address}}</p>
                       
                        <u><h6>Billing Address</h6></u>
                        @if ($order->fcity_id)
                        <p>City :  {{$order->biCity ? $order->biCity->city : '***'}}</p>
                        <p>County :  {{$order->biCounty ? $order->biCounty->county : '***'}}</p>
                        <p>Address :  {{$order->faddress}}</p>
                        @else 
                        <p class="text-danger">NO</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-12 mt-3">
                <div class="card">
                    <div class="card-header bg-warning  text-dark">Payment Info</div>

                    <div class="card-body ">
                        <p>Payment Amount :  {{$order->payment->amount}}</p>
                        <p>maskedCreditCard:  {{$order->payment->maskedCreditCard}}</p>
                        <p>name on the card:  {{$order->payment->EXTRA_CARDHOLDERNAME}}</p>
                        <p>Bank:  {{$order->payment->EXTRA_CARDISSUER}}</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-12 mt-3">
                <div class="card">
                    <div class="card-header bg-secondary text-light">Coupon Usage</div>

                    <div class="card-body ">
                        @if ($order->coupon)
                            <p>Coupon Code:  {{$order->coupon->code}}</p>
                            <p>Coupon discount:  {{$order->coupon->discount}} %</p>
                            <p>Coupon Usage date and time:  {{$order->coupon->updated_at}}</p>
                            <p>Coupon note :  {{$order->coupon->note}}</p>
                        @endif
                        
                    </div>
                </div>
            </div>
            <div class="col-12 mt-3">
                <div class="card">
                    <div class="card-header bg-success text-light">Order  <span class="text-dark"> ( {!! $order->orderStatus !!} ) </span> </div>

                    <div class="card-body ">
                        
                        <table class="table table-bordered table-responsive-lg">
                            <thead>
                              <tr>
                                <th scope="col">#</th>
                                <th scope="col">Product id</th>
                                <th scope="col">name</th>
                                <th scope="col">price</th>
                                <th scope="col">quantity</th>
                                <th scope="col">kits by product</th>
                              </tr>
                            </thead>
                            <tbody>
                                @php $total = 0 ; @endphp
                                @foreach ($order->orderDetail as $detail)
                                    <tr>
                                        <td>{{ $detail->id }}</td>
                                        <td>{{ $detail->product_id }}</td>
                                        <td>{{ $detail->name }}</td>
                                        <td>{{ $detail->price }}</td>
                                        <td>{{ $detail->quantity }}</td>
                                        <td>{{ $detail->Product->number_of_kit }}</td>
                                        @php $total = $total + ( $detail->Product->number_of_kit * $detail->quantity )  @endphp
                                    </tr>
                                @endforeach
                                <tr>
                                    <td colspan="6" class="text-center"> <b> the Total Kit in this Order is : {{ $total }} </b></td>
                                </tr>
                                <tr class="bg-dark text-light">
                                    <td scope="col">#</td>
                                    <td colspan="1"><b>kits Codes</b></td>
                                    <td colspan="2">kit Status</td>
                                    <td colspan="2">Change kit Status</td>
                                    <td colspan="1">For Test</td>
                                </tr>
                                @if (count($errors) > 0)
                                    <tr class="bg-danger text-light">
                                        <td colspan="6">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                <li>{{$error}}</li>
                                                @endforeach
                                            </ul>
                                        </td>
                                    </tr>
                                @endif
                                @php $total = 0 ; @endphp
                                @foreach ($order->orderDetail as $details)
                                    @foreach ($details->kit as $kit)
                                    <tr>
                                        <td colspan="1"> {{ ++$total }} </td>
                                        @if ($kit->kit_code)
                                            <td colspan="1"> <b>{{ $kit->kit_code }}</b></td>
                                        @else
                                            <td colspan="1">
                                                <!-- Button trigger modal -->
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#kit_id{{$kit->id}}">
                                                    Enter Kit Code
                                                </button>
                                                
                                                <!-- Modal -->
                                                <div class="modal fade" id="kit_id{{$kit->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            {!! Form::open(['method'=>'PATCH', 'action' => ['OperationAdminController@updateKit', $kit->id], 'style'=> 'display:inline', 'id' => 'id_form_create_' . $kit->id]) !!}
                                                                
                                                                <div class="form-group">
                        
                                                                    {!! Form::label('id_' . $kit->id . 'kit_code', 'Kit Code : ') !!}
                                                                    {!! Form::text('id_' . $kit->id . 'kit_code', null, ['class'=>'form-control']) !!}
                                                    
                                                                </div>
                                                                <input type="hidden" name="id_{{$kit->id}}_kit_id" value="{{$kit->id}}">      
                                                            {!! Form::close() !!} 
                                                        </div>
                                                        <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <button class="btn btn-primary" onclick="
                                                            event.preventDefault();
                                                            document.getElementById('id_form_create_{{$kit->id}}').submit();
                                                        ">
                                                        Save Changes</button>
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                            </td>
                                        @endif
                                        
                                        <td colspan="2" class="bg-light">{{ $kit->kitStatuse }}</td>  
                                        <td colspan="2" class="bg-light">
                                            <div class="btn-group" role="group" aria-label="Basic example">
                                                <button data="{{$kit->kit_code}}" data1="{{$kit->id}}" type="button" class="btn btn-secondary delever" {{$kit->kit_status == 0 && $kit->kit_code ? '' : 'disabled'}}>Delever to user</button>
                                                <button data="{{$kit->kit_code}}" data1="{{$kit->id}}" type="button" class="btn btn-info text-light received" {{$kit->kit_status == 1 && $kit->kit_code ? '' : 'disabled'}}>received from user</button>
                                                <button data="{{$kit->kit_code}}" data1="{{$kit->id}}" type="button" class="btn btn-warning text-dark sent" {{$kit->kit_status == 2 && $kit->kit_code ? '' : 'disabled'}}>sent to lab</button>
                                                <button data="{{$kit->kit_code}}" data1="{{$kit->id}}" type="button" class="btn btn-success text-light upload" {{$kit->kit_status == 3 && $kit->kit_code ? '' : 'disabled'}}>upload results</button>
                                            </div>
                                        </td>
                                        <td colspan="1" class="bg-light">{{$kit->orderDetail->name}}</td>  
                                    </tr>
                                    @endforeach
                                @endforeach
                            </tbody>
                          </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
<script>
    $(document).ready(function(){

        $('.delever').click(function(){
            $.ajax({
                url: '{{ route('operation.kit.delever') }}',
                method: "post",
                data: {_token: '{{ csrf_token() }}', kit_code: $( this ).attr('data'), kit_id:$( this ).attr('data1')},
                success: function (response) {
                    window.location.reload();
                },
                error: function (response) {
                    window.location.reload();
                }
            });
        });
        $('.received').click(function(){
            $.ajax({
                url: '{{  route('operation.kit.received') }}',
                method: "post",
                data: {_token: '{{ csrf_token() }}', kit_code: $( this ).attr('data'), kit_id:$( this ).attr('data1')},
                success: function (response) {
                    window.location.reload();

                },
                error: function (response) {
                    window.location.reload();
                }
            });
        });

        $('.sent').click(function(){
            $.ajax({
                url: '{{  route('operation.kit.sent') }}',
                method: "post",
                data: {_token: '{{ csrf_token() }}', kit_code: $( this ).attr('data'), kit_id:$( this ).attr('data1')},
                success: function (response) {

                    window.location.reload();
                },
                error: function (response) {

                    window.location.reload();
                }
            });
        });
    });
</script>
@endsection