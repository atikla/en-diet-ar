@extends('layouts.admin-operation')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-dark text-light">Kits</div>

                    <div class="card-body ">
                        You are logged in as Operation Admin You are in Kits Section!
                    </div>
                </div>
                @include('include.messages')
                @include('include.form-errors')
            </div>
        </div>
    </div>
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="text-right">
                    {!! Form::open(['method'=>'POST', 'action' => ['OperationAdminController@Kitsearch'], 'style'=> 'display:inline']) !!}

                        {!! Form::label('search', 'Query:') !!}
                        {!! Form::text('search', $search, ['class'=>'form-control', 'style'=> 'display:inline; width:initial', 'required']) !!}

                        {!! Form::submit('search', ['class'=>'btn btn-outline-success  mb-1']) !!}

                    {!! Form::close() !!}
                    <a href="{{ route('operation.kit') }}" class="btn btn-outline-success ml-2 mb-1"> Change Status</a>
                </div>
                <table class="table table-responsive-lg table-striped">
                    <thead class="thead-light">
                    <tr>
                        <th>#</th>
                        <th>kit code</th>
                        <th>sell status</th>
                        <th>kit status</th>
                        <th>kit show status</th>
                        <th>Order</th>
                        <th>user</th>
                        <th>process</th>
                    </tr>
                    </thead>
                    <tbody>
                        @if ($kits)
                            @foreach ($kits as $kit)
                            <tr>
                                <td>{{$kit->id}}</td>
                                <td>{{$kit->kit_code}}</td>
                                <td>{{$kit->status}}</td>
                                <td>{{$kit->kitStatuse}}</td>
                                <td>{!! $kit->show ? '<span class="text-success">User Can View Results</span>' : '<span class="text-danger">User Can Not View Results</span>' !!}</td>
                                @if ($kit->OrderDetail)
                                <td> <a href="{{ route('operation.orders.show', $kit->OrderDetail->order->id) }}"> Show Order</a></td>
                                @else
                                    <td>Dont Have a Order ( sold offline ) </td>
                                @endif
                                <td>{!! $kit->user ? $kit->user->name : 'Done Have User' !!}</td>
                                <td>
                                    <button type="button" class="btn btn-outline-success update mr-2 mb-1" data-toggle="modal" data-target="#kit_id{{$kit->id}}">
                                        Change Status
                                    </button>
                                </td>
                            </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @if ($kits)
        @foreach ($kits as $kit)
            <!-- Modal -->
            <div class="modal fade" id="kit_id{{$kit->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button data="{{$kit->kit_code}}" data1="{{$kit->id}}" type="button" class="btn btn-secondary delever" {{$kit->kit_status == 0 && $kit->kit_code ? '' : 'disabled'}}>Delever to user</button>
                            <button data="{{$kit->kit_code}}" data1="{{$kit->id}}" type="button" class="btn btn-info text-light received" {{$kit->kit_status == 1 && $kit->kit_code ? '' : 'disabled'}}>received from user</button>
                            <button data="{{$kit->kit_code}}" data1="{{$kit->id}}" type="button" class="btn btn-warning text-dark sent" {{$kit->kit_status == 2 && $kit->kit_code ? '' : 'disabled'}}>sent to lab</button>
                            <button data="{{$kit->kit_code}}" data1="{{$kit->id}}" type="button" class="btn btn-danger text-light" {{$kit->kit_status >= 3 && $kit->kit_code ? '' : 'disabled'}}>you're done</button>
                        </div>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary" onclick="
                        event.preventDefault();
                        document.getElementById('id_form_create_{{$kit->id}}').submit();
                    ">
                    Save Changes</button>
                    </div>
                </div>
                </div>
            </div>
        @endforeach
    @endif
@endsection
@section('script')
<script>
    $(document).ready(function(){

        $('.delever').click(function(){
            $.ajax({
                url: '{{ route('operation.kit.delever') }}',
                method: "post",
                data: {_token: '{{ csrf_token() }}', kit_code: $( this ).attr('data'), kit_id:$( this ).attr('data1')},
                success: function (response) {
                    window.location.reload();
                },
                error: function (response) {
                    window.location.reload();
                }
            });
        });
        $('.received').click(function(){
            $.ajax({
                url: '{{  route('operation.kit.received') }}',
                method: "post",
                data: {_token: '{{ csrf_token() }}', kit_code: $( this ).attr('data'), kit_id:$( this ).attr('data1')},
                success: function (response) {
                    window.location.reload();

                },
                error: function (response) {
                    window.location.reload();
                }
            });
        });

        $('.sent').click(function(){
            $.ajax({
                url: '{{  route('operation.kit.sent') }}',
                method: "post",
                data: {_token: '{{ csrf_token() }}', kit_code: $( this ).attr('data'), kit_id:$( this ).attr('data1')},
                success: function (response) {

                    window.location.reload();
                },
                error: function (response) {

                    window.location.reload();
                }
            });
        });
    });
</script>
@endsection