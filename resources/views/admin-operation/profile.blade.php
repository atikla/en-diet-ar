@extends('layouts.admin-operation')
@section('content')
    <div class="mt-3 mx-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-dark text-light">Admins</div>

                    <div class="card-body ">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        You are logged in as Super Admin You are in Update Your Profile Section!
                    </div>
                </div>
                @include('include.messages')
                @include('include.form-errors')
            </div>
        </div>
    </div>
    <div class="m-5">
        <div class="row justify-content-center">
            <div class="col-lg-3 col-12">
                <img class='img-rounded' src="{{$admin->photo ? url('/') . '/img/private/' . $admin->photo->path : url('/') . '/img/private/default.png'}}" alt="">
            </div>
            <div class="col-lg-9 col-12">
                {!! Form::model($admin,['method'=>'PATCH', 'action' => 'AdminController@updateProfile', 'files'=>true]) !!}
                        <div class="form-group">

                            {!! Form::label('name', 'Name') !!}
                            {!! Form::text('name', null, ['class'=>'form-control']) !!}
            
                        </div>
                        <div class="form-group">

                            {!! Form::label('email', 'Email') !!}
                            {!! Form::email('email', null, ['class'=>'form-control']) !!}
            
                        </div>
                        <div class="form-group">

                            {!! Form::label('phone', 'phone') !!}
                            {!! Form::number('phone', null, ['class'=>'form-control']) !!}
            
                        </div>
                        <div class="form-group">
            
                            {!! Form::label('photo', 'photo') !!}
                            {!! Form::file('photo') !!}
                            
                        </div>
                        <div class="form-group">
            
                            {!! Form::label('password', 'Password:') !!}
                            {!! Form::password('password', ['class'=>'form-control']) !!}
                            
                        </div>
                        <div class="form-group">
            
                            {!! Form::label('password_confirmation', 'Password Confirmation:') !!}
                            {!! Form::password('password_confirmation', ['class'=>'form-control', 'autocomplete' =>'on']) !!}
                            
                        </div>
                        <div class="form-group">
            
                            {!! Form::submit('Update Admin', ['class'=>'btn btn-primary']) !!}
                            
                        </div> 
                    {!! Form::close() !!} 
            </div>
        </div>
    </div>


@endsection
