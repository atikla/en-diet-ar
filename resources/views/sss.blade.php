@extends('layouts.public')
@section('subpageMenu','topmenu--subpage')

@section('pageTitle', __('pages/sss.page_title'))
@section('pageDescription', __('pages/sss.page_description'))

@section('content')
<section class="subpage-header subpage-header--sss">
	<div class="container">
		<div class="row">
			<div class="subpage-header__content">

				<h1 class="subpage-header__title">{{__('pages/sss.header_title')}}</h1>
				<div class="subpage-header__seperator"></div>
				<div class="subpage-header__breadcrumb">
					<nav aria-label="breadcrumb">
						<ol itemscope itemtype="https://schema.org/BreadcrumbList"
						class="breadcrumb no-bg-color text-light">
							<li itemprop="itemListElement" itemscope
							itemtype="https://schema.org/ListItem"
							class="breadcrumb-item">
								<a itemprop="item"
								style="color:#007bff"
								href="{{ route('/')}}">
								<span itemprop="name">{{__('pages/sss.breadcrumb.0')}}</span>
								</a>
								<meta itemprop="position" content="1"/>
							</li>
							<li itemprop="itemListElement" itemscope
							itemtype="https://schema.org/ListItem"
							class="breadcrumb-item active te">
								<a itemprop="item" 
								style="color:#6c757d"
								href="{{ route('sss')}}">
									<span itemprop="name">{{__('pages/sss.breadcrumb.1')}}</span>
								</a>
								<meta itemprop="position" content="2"/>
							</li>
						</ol>
					</nav>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="sss">
	<div class="container">

		<div id="accordion">
			<div class="card">
				<div class="card-header" id="heading1">
					<button data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
						{{__('pages/sss.card_1.title')}}
					</button>
					<div class="icon plus"></div>
				</div>

				<div id="collapse1" class="collapse" aria-labelledby="heading1" data-parent="#accordion">
					<div class="card-body">
						{!!__('pages/sss.card_1.desc')!!}
					</div>
				</div>
			</div>

			<div class="card">
				<div class="card-header" id="heading2">
					<button data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse2">
						{{__('pages/sss.card_2.title')}}
					</button>
					<div class="icon plus"></div>
				</div>

				<div id="collapse2" class="collapse" aria-labelledby="heading2" data-parent="#accordion">
					<div class="card-body">
						{!!__('pages/sss.card_2.desc')!!}
					</div>
				</div>
			</div>

			<div class="card">
				<div class="card-header" id="heading3">
					<button data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapse3">
						{{__('pages/sss.card_3.title')}}
					</button>
					<div class="icon plus"></div>
				</div>

				<div id="collapse3" class="collapse" aria-labelledby="heading3" data-parent="#accordion">
					<div class="card-body">
						{!!__('pages/sss.card_3.desc')!!}
					</div>
				</div>
			</div>

			<div class="card">
				<div class="card-header" id="heading4">
					<button data-toggle="collapse" data-target="#collapse4" aria-expanded="true" aria-controls="collapse4">
						{{__('pages/sss.card_4.title')}}
					</button>
					<div class="icon plus"></div>
				</div>

				<div id="collapse4" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
					<div class="card-body">
						{!!__('pages/sss.card_4.desc')!!}
					</div>
				</div>
			</div>

			<div class="card">
				<div class="card-header" id="heading5">
					<button data-toggle="collapse" data-target="#collapse5" aria-expanded="true" aria-controls="collapse5">
						{{__('pages/sss.card_5.title')}}
					</button>
					<div class="icon plus"></div>
				</div>

				<div id="collapse5" class="collapse" aria-labelledby="heading5" data-parent="#accordion">
					<div class="card-body">
						{!!__('pages/sss.card_5.desc')!!}
					</div>
				</div>
			</div>

			<div class="card">
				<div class="card-header" id="heading6">
					<button data-toggle="collapse" data-target="#collapse6" aria-expanded="true" aria-controls="collapse6">
						{{__('pages/sss.card_6.title')}}
					</button>
					<div class="icon plus"></div>
				</div>

				<div id="collapse6" class="collapse" aria-labelledby="heading6" data-parent="#accordion">
					<div class="card-body">
						{!!__('pages/sss.card_6.desc')!!}
					</div>
				</div>
			</div>

			<div class="card">
				<div class="card-header" id="heading7">
					<button data-toggle="collapse" data-target="#collapse7" aria-expanded="true" aria-controls="collapse7">
						{{__('pages/sss.card_7.title')}}
					</button>
					<div class="icon plus"></div>
				</div>

				<div id="collapse7" class="collapse" aria-labelledby="heading7" data-parent="#accordion">
					<div class="card-body">
						{!!__('pages/sss.card_7.desc')!!}
					</div>
				</div>
			</div>

			<div class="card">
				<div class="card-header" id="heading8">
					<button data-toggle="collapse" data-target="#collapse8" aria-expanded="true" aria-controls="collapse8">
						{{__('pages/sss.card_8.title')}}
					</button>
					<div class="icon plus"></div>
				</div>

				<div id="collapse8" class="collapse" aria-labelledby="heading8" data-parent="#accordion">
					<div class="card-body">
						{!!__('pages/sss.card_8.desc')!!}
					</div>
				</div>
			</div>

			<div class="card">
				<div class="card-header" id="heading9">
					<button data-toggle="collapse" data-target="#collapse9" aria-expanded="true" aria-controls="collapse9">
						{{__('pages/sss.card_9.title')}}
					</button>
					<div class="icon plus"></div>
				</div>

				<div id="collapse9" class="collapse" aria-labelledby="heading9" data-parent="#accordion">
					<div class="card-body">
						{!!__('pages/sss.card_9.desc')!!}
					</div>
				</div>
			</div>

			<div class="card">
				<div class="card-header" id="heading10">
					<button data-toggle="collapse" data-target="#collapse10" aria-expanded="true" aria-controls="collapse10">
						{{__('pages/sss.card_10.title')}}
					</button>
					<div class="icon plus"></div>
				</div>

				<div id="collapse10" class="collapse" aria-labelledby="heading10" data-parent="#accordion">
					<div class="card-body">
						{!!__('pages/sss.card_10.desc')!!}
					</div>
				</div>
			</div>

			<div class="card">
				<div class="card-header" id="heading11">
					<button data-toggle="collapse" data-target="#collapse11" aria-expanded="true" aria-controls="collapse11">
						{{__('pages/sss.card_11.title')}}
					</button>
					<div class="icon plus"></div>
				</div>

				<div id="collapse11" class="collapse" aria-labelledby="heading11" data-parent="#accordion">
					<div class="card-body">
						{!!__('pages/sss.card_11.desc')!!}
					</div>
				</div>
			</div>

			<div class="card">
				<div class="card-header" id="heading12">
					<button data-toggle="collapse" data-target="#collapse12" aria-expanded="true" aria-controls="collapse12">
						{{__('pages/sss.card_12.title')}}
					</button>
					<div class="icon plus"></div>
				</div>

				<div id="collapse12" class="collapse" aria-labelledby="heading12" data-parent="#accordion">
					<div class="card-body">
						 {!!__('pages/sss.card_12.desc')!!}
					</div>
				</div>
			</div>

			<div class="card">
				<div class="card-header" id="heading13">
					<button data-toggle="collapse" data-target="#collapse13" aria-expanded="true" aria-controls="collapse13">
						{{__('pages/sss.card_13.title')}}
					</button>
					<div class="icon plus"></div>
				</div>

				<div id="collapse13" class="collapse" aria-labelledby="heading13" data-parent="#accordion">
					<div class="card-body">
						{!!__('pages/sss.card_13.desc')!!}
					</div>
				</div>
			</div>
			<div class="card">
				<div class="card-header" id="heading14">
					<button data-toggle="collapse" data-target="#collapse14" aria-expanded="true" aria-controls="collapse14">
						{{__('pages/sss.card_14.title')}}
					</button>
					<div class="icon plus"></div>
				</div>

				<div id="collapse14" class="collapse" aria-labelledby="heading14" data-parent="#accordion">
					<div class="card-body">
						{!!__('pages/sss.card_14.desc')!!}
					</div>
				</div>
			</div>
			<div class="card">
				<div class="card-header" id="heading15">
					<button data-toggle="collapse" data-target="#collapse15" aria-expanded="true" aria-controls="collapse15">
						{{__('pages/sss.card_15.title')}}
					</button>
					<div class="icon plus"></div>
				</div>

				<div id="collapse15" class="collapse" aria-labelledby="heading15" data-parent="#accordion">
					<div class="card-body">
						{!!__('pages/sss.card_15.desc')!!}
					</div>
				</div>
			</div>
		</div>
		<p class="sss__subnote">
			{!!__('pages/sss.bottom.0')!!}<a href="mailto:destek@enbiosis.com"> destek@enbiosis.com </a> {!!__('pages/sss.bottom.1')!!}

		</p>
	</div>
</section>
@endsection
@section('scripts')
<script>
	$(document).ready(function(){
		$(".collapse.show").each(function(){
			$(this).prev(".card-header").find(".icon").addClass("minus").removeClass("plus");
		});

		$(".collapse").on('show.bs.collapse', function(){
			$(this).prev(".card-header").find(".icon").removeClass("plus").addClass("minus");
		}).on('hide.bs.collapse', function(){
			$(this).prev(".card-header").find(".icon").removeClass("minus").addClass("plus");
		});
	});
</script>
@endsection
