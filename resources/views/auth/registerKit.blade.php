@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center mt-lg-5">
        <div class="col-md-8 mt-lg-5">
            @include('include.form-errors')
            <div class="card mt-lg-5">
                <div class="card-header">{{ __('app.register_kit') }}</div>

                <div class="card-body">
                    
                    {!! Form::open(['method'=>'POST', 'action' => 'Auth\UserRegisterKitController@RegisterKit']) !!}

                    <div class="form-group row px-5 mb-5">

                        {!! Form::label('kit_code',  __('app.kit_code'). ': ', ['class' => 'col-sm-3 col-form-label']) !!}

                        <div class="col-sm-9">

                          {!! Form::text('kit_code', null, ['class'=>'form-control', 'placeholder' => __('app.kit_code')]) !!}

                        </div>

                    </div>

                    <div class="form-group text-center px-5 mx-5">
                        {!! Form::submit(__('app.submit'), ['class'=>'btn btn-primary btn-block']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection