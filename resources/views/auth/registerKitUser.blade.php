@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @include('include.form-errors')
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-4 text-left">{{ __('Register') }}</div>
                        <div class="col-8 text-right">
                            <a class="" data-toggle="modal" data-target="#login" style="cursor: -webkit-pointer; cursor: pointer;">
                                {{__('app.üye_mi')}}
                            </a>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    {!! Form::open(['method'=>'POST', 'action' => 'Auth\UserRegisterKitController@registerKitWithUser']) !!}

                        <div class="form-group row">
                           
                            {!! Form::label('name',   __('app.Name'), ['class' => 'col-md-4 col-form-label text-md-right']) !!}

                            <div class="col-md-6">
                                
                                {!! Form::text('name', null, ['class'=>'form-control', 'placeholder' => __('app.Name'), 'autocomplete' => 'name', 'autofocus']) !!}
                           
                            </div>
                        </div>

                        <div class="form-group row">
            
                            {!! Form::label('address', __('app.address'), ['class' => 'col-md-4 col-form-label text-md-right']) !!}

                            <div class="col-md-6">

                            {!! Form::text('address', null, ['class'=>'form-control', 'placeholder' => __('app.address')]) !!}

                            </div>
                        </div>
                        <div class="form-group row">

                            {!! Form::label('phone', 'Phone', ['class' => 'col-md-4 col-form-label text-md-right']) !!}
                            
                            <div class="col-md-6">

                            {!! Form::text('phone', null, ['class'=>'form-control', 'placeholder' => '5XXXXXXXX']) !!}

                            </div>
                        </div>
                        <div class="form-group row">
                           
                            {{ Form::label('gender', __('app.cinsiyet'), ['class' => 'col-md-4 col-form-label text-md-right']) }}
                           
                            <div class="col-md-6">

                                {{ Form::select('gender', [ '1' => __('app.erkek'), '2' => __('app.kadin')], null,['class' => 'form-control', 'placeholder' => __('app.lütfen_seçin') . ' ...']) }}

                            </div>
                        </div>

                        <div class="form-group row">

                            {!! Form::label('email',   __('app.Email'), ['class' => 'col-md-4 col-form-label text-md-right']) !!}
                            
                            <div class="col-md-6">

                                {!! Form::email('email', null, ['class'=>'form-control', 'placeholder' => __('app.Email'), 'autocomplete' => 'email']) !!}

                            </div>
                        </div>

                        <div class="form-group row">
                    
                            {!! Form::label('password',   __('Password'), ['class' => 'col-md-4 col-form-label text-md-right']) !!}

                            <div class="col-md-6">

                                {!! Form::password('password', ['class'=>'form-control', 'autocomplete' => 'new-password', 'placeholder' => __('Password')]) !!}

                            </div>
                        </div>

                        <div class="form-group row">
                
                            {!! Form::label('password_confirmation',   __('Confirm Password'), ['class' => 'col-md-4 col-form-label text-md-right']) !!}

                            <div class="col-md-6">

                                {!! Form::password('password_confirmation', ['class'=>'form-control', 'autocomplete' => 'new-password', 'placeholder' => __('Confirm Password')]) !!}

                            </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('password_confirmation',   __('Sözleşmeler'), ['class' => 'col-md-4 col-form-label text-md-right']) !!}

                            <div class="col-md-6 ml-4">

                                <input type="checkbox" class="form-check-input" name="Üyelik-Sozleşmesi">
                                <label class="form-check-label"> <a href="{{route('uyelik-sozlesmesi')}}" target="_blank">Üyelik Sözleşmesini</a>  kabul ediyorum. </label>
                                <br>
                                <input type="checkbox" class="form-check-input" name="Gizlilik-Politikası">
                                <label class="form-check-label"> <a href="{{route('gizlilik-politikasi')}}" target="_blank">Gizlilik politikasını</a>  kabul ediyorum. </label>
                                <br>
                                <input type="checkbox" class="form-check-input" name="Ön_Bilgilendirme_Formu">
                                <label class="form-check-label"><a href="{{route('on-bilgilendirme-formu')}}" target="_blank">Ön bilgilendirme formunu </a>kabul ediyorum.</label>
                                <br>
                                <input type="checkbox" class="form-check-input" name="Aydınlatma_Formu">
                                <label class="form-check-label"><a href="{{route('aydinlatma-formu')}}" target="_blank">Aydınlatma formunu </a>kabul ediyorum. </label>
                                <small> </small>

                            </div>
                        </div>
                        <div class="form-group row">
							<div class="col-md-6 mb-3">
									
							</div>
						</div>
                        <input type="hidden" name="kit_code" value="{{session()->get('kit_code')}}">
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitle">{{__('login')}}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                {!! Form::open(['method'=>'POST', 'action' => 'Auth\UserRegisterKitController@LoginUserAfterKitCodeMatch', 'style'=> 'display:inline', 'id' => 'id_form_create']) !!}
                    
                    <div class="form-group row">

                        {!! Form::label('email_',   __('app.Email'), ['class' => 'col-md-4 col-form-label text-md-right']) !!}
                        
                        <div class="col-md-6">

                            {!! Form::email('email_', null, ['class'=>'form-control', 'placeholder' => __('app.Email'), 'autocomplete' => 'email']) !!}

                        </div>
                    </div>

                    <div class="form-group row">
                    
                        {!! Form::label('password_',   __('Password'), ['class' => 'col-md-4 col-form-label text-md-right']) !!}

                        <div class="col-md-6">

                            {!! Form::password('password_', ['class'=>'form-control', 'autocomplete' => 'new-password']) !!}

                        </div>
                    </div>
                 {!! Form::close() !!} 
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button class="btn btn-primary" onclick="event.preventDefault();document.getElementById('id_form_create').submit();">
            Save Changes</button>
            </div>
        </div>
    </div>
</div>
@endsection
