<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="ltr">
	<head>
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-WH9H7HG');</script>
		<!-- Hotjar Tracking Code for https://www.enbiosis.com/ -->
		<script>
			(function(h,o,t,j,a,r){
				h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
				h._hjSettings={hjid:1735768,hjsv:6};
				a=o.getElementsByTagName('head')[0];
				r=o.createElement('script');r.async=1;
				r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
				a.appendChild(r);
			})(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
		</script>
		<!-- Global site tag (gtag.js) - Google Ads: 658369982 -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=AW-658369982"></script>
		<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'AW-658369982');
		</script>
		<script>
			function pushObjectToDataLayer(object) {
				try {
					window.dataLayer.push(object);
				}
				catch{
					return false;
				}
			}
		</script>
		<!-- End Google Tag Manager -->
		<meta charset="utf-8">
		<title>@yield('pageTitle')</title>
		<meta name="description" content="@yield('pageDescription')">
		<link rel="icon" type="image/png" href="{{ url('/') }}/new/img/favicon.webp">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
		@yield('metaTag')
		<link rel="canonical" href="{{url()->current()}}" />
		<meta http-equiv="ScreenOrientation" content="autoRotate:disabled">
		{{-- <meta name="google-site-verification" content="scU6mcXqCl8ElU-JIrTT39HLk-RrJyASIGX9Jwjl9BA"/> --}}
		<link href="{{ url('/') }}/new/css/owl.carousel.min.css" rel="stylesheet">
		<link href="{{ url('/') }}/new/css/owl.theme.default.min.css" rel="stylesheet">
		<link href="{{ url('/') }}/new/css/devices.min.css" rel="stylesheet">
		<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link rel="stylesheet/less" type="text/css" href="{{ url('/') }}/new/css/style.less"/>
		@yield('styles')
		<script src="{{ url('/') }}/new/js/less.min.js" ></script>
		@yield('gtag')
		@yield('gtag-s')

	</head>
	<body>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WH9H7HG"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		<div class="mobile-menu">
			<ul itemscope itemtype="http://schema.org/BreadcrumbList">
				<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
					<a itemprop="item" href="{{url('/checkout')}}">
						<span itemprop="name">{{__('pages/layout.satin_al')}}</span>
					</a>
					<meta itemprop="position" content="1" />
				</li>
				<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
					<a itemprop="item" href="https://enbiosis.com/blog">
						<span itemprop="name">مدونة</span>
					</a>
					<meta itemprop="position" content="2" />
				</li>
				<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
					<a itemprop="item" href="https://app.enbiosis.com/register">
						<span itemprop="name">{{__('pages/layout.kit_kayit')}}</span>
					</a>
					<meta itemprop="position" content="3" />
				</li>
				
				<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
					<a itemprop="item" href="https://app.enbiosis.com/login">
						<span itemprop="name">{{__('pages/layout.giris_yap')}}</span>
					</a>
					<meta itemprop="position" content="4" />
				</li>
			</ul>
		</div>
		<section class="topmenu @yield('subpageMenu')">
			<div class="container">
				<div class="row topmenu__row">
					<div class="topmenu__logo">
						<a href="{{ url('/') }}">
							<img src="{{ url('/') }}/new/img/en-diet.png"/>
						</a>
					</div>
					<div class="topmenu__menu">
						<ul itemscope itemtype="http://schema.org/BreadcrumbList">
							<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
								<a itemprop="item" href="{{url('/checkout')}}">
									<span itemprop="name">{{__('pages/layout.satin_al')}}</span>
								</a>
								<meta itemprop="position" content="1" />
							</li>
							{{-- <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
								<a itemprop="item" href="https://enbiosis.com/blog">
									<span itemprop="name">مدونة</span>
								</a>
								<meta itemprop="position" content="2" />
							</li> --}}
							{{-- <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
								<a itemprop="item" href="https://app.enbiosis.com/register">
									<span itemprop="name">{{__('pages/layout.kit_kayit')}}</span>
								</a>
								<meta itemprop="position" content="3" />
							</li> --}}
							{{-- <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
								<a itemprop="item" href="https://app.enbiosis.com/login">
									<span itemprop="name">{{__('pages/layout.giris_yap')}}</span>
								</a>
								<meta itemprop="position" content="4" />
							</li> --}}
						</ul>
					</div>
					<div class="topmenu__menu">
						{{-- <ul>
							<li>
								<a href="https://app.enbiosis.com/login" style="width:100px;background-color:#e8957e;display:flex;align-items:center;justify-content: center;color:white;height:30px;border-radius:5px;">
								{{__('pages/layout.giris_yap')}}</a>
							</li>
						</ul> --}}
					</div>
					<a href="javascript:void(0);" onclick="openMenu()" class="mobile-menu-button"><img src="{{ url('/') }}/new/img/open-menu.webp" height="32"/></a>
				</div>
			</div>
		</section>
		<div class="main-wrapper">

			@yield('content')

		</div>
		<div id="turn">
			يرجى استخدام هاتفك في الوضع الرأسي من أجل استخدام موقع الويب بشكل صحيح.
		</div>
		{{-- @if (\Route::current()->getName() != 'iletisim' && \Route::current()->getName() != 'yatirimci')			
			<section class="contact-form" style="margin-bottom: -40px">
				<h2 class="contact-form__title">{{__('pages/layout.bulten')}}</h2>
				<p class="contact-form__sub_title">{{__('pages/layout.bulten_desc')}}</p>
				<div class="container justify-content-center">
					{!! Form::open(['method'=>'POST', 'action' => 'GeneralController@newsletter', 'class' => 'row']) !!}
			
					<div class="form-group col-md-8 contact-form__input">
						
						{!! Form::text('email', null, ['class'=>'form-control', 'id' => 'form_email', 'aria-describedby' => 'emailHelp', 'placeholder' => __('pages/iletisim.e_posta_adresiniz')]) !!}
						@error('email')
							<small id="emailHelp" class="form-text text-danger">{{$message}}</small>
						@enderror
					</div>
					
					<div class="form-group col-md-4 text-center">
			
						{!! Form::submit(__('pages/layout.kayit'), [
							'class'=>'link-btn link-btn--orange form-button form-send', 
							'style' => 'border: 1px solid #fff;padding: 10px 80px !important;',
							'onclick' => "pushObjectToDataLayer({ event: 'gaEvent', Action: 'submit', Category: 'form', Label: 'signup' })"
						]) !!}
					
					</div>
					
					{!! Form::close() !!} 

				</div>
			</section>
		@endif --}}
		<section class="footer @yield('footerContact')">
			<div class="footer__bg" @yield('imza')>
				{{-- @if (trim($__env->yieldContent('footerContact')))
					<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d192365.58117773966!2d29.007692!3d41.118976!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xe6c11a45657c054a!2sAISOLT%20Yapay%20Zeka%20Teknolojileri!5e0!3m2!1str!2str!4v1584554528677!5m2!1str!2str" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
				@endif --}}
			</div>
			<div class="container">
				<div class="row">
					<div class="col-md-12 footer__box">
						<div class="footer__logo">
							<a href="{{ url('/') }}">
								<img src="{{ url('/') }}/new/img/en-diet.png" />
							</a>
						</div>
						<div class="footer__menu">
							<ul itemscope itemtype="http://schema.org/BreadcrumbList">
								<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
									<a itemprop="item" href="{{url('/checkout')}}">
										<span itemprop="name">{{__('pages/layout.satin_al')}}</span>
									</a>
									<meta itemprop="position" content="1" />
								</li>
								<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
									<a itemprop="item" href="https://enbiosis.com/blog">
										<span itemprop="name">مدونة</span>
									</a>
									<meta itemprop="position" content="2" />
								</li>
								<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
									<a itemprop="item" href="https://app.enbiosis.com/register">
										<span itemprop="name">{{__('pages/layout.kit_kayit')}}</span>
									</a>
									<meta itemprop="position" content="3" />
								</li>
								
								<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
									<a itemprop="item" href="https://app.enbiosis.com/login">
										<span itemprop="name">{{__('pages/layout.giris_yap')}}</span>
									</a>
									<meta itemprop="position" content="4" />
								</li>
							</ul>
						</div>
						<div class="footer__socials">
							<ul>
								<li>
									<a href="https://www.facebook.com/enbiosis/" target="_blank">
										<img src="{{ url('/') }}/new/img/facebook-icon.webp" />
									</a>
								</li>
								<li>
									<a href="https://www.instagram.com/enbiosis/" target="_blank">
										<img src="{{ url('/') }}/new/img/instagram-icon.webp" />
									</a>
								</li>
								<li>
									<a href="https://www.youtube.com/channel/UCLmsr81DJ7euqFc_Rgdn4zA" target="_blank">
										<img src="{{ url('/') }}/new/img/youtube-icon.webp" />
									</a>
								</li>
								<li>
									<a href="https://www.linkedin.com/company/enbiosis" target="_blank">
										<img src="{{ url('/') }}/new/img/linkedin-icon.webp" />
									</a>
								</li>
								<li>
									<a href="https://twitter.com/enbiosis" target="_blank">
										<img src="{{ url('/') }}/new/img/twitter-icon.webp" />
									</a>
								</li>
							</ul>
						</div>
						{{-- <div class="footer__contact">
							<h4 class="footer__contact__title">{{__('pages/layout.bize_ulasin')}}</h4>
							<div class="row" style="margin-bottom:20px;">
								<div class="col-md-6 text-center">
									<span class="w-100 "><b>{{__('pages/layout.turkiye_ofisi')}}:</b><br/>{!!__('pages/layout.turkiye_ofisi_desc')!!}</span>
								</div>
								<div class="col-md-6">
									<span><b>{{__('pages/layout.uae_dubai_ofisi')}}:</b><br/>{{__('pages/layout.uae_dubai_ofisi_desc')}}</span>
								</div>
							</div>
							<span>{{__('pages/layout.telefon')}}:
								<a 	href="tel:+90 850 840 77 93"
									style="color:#696969"
									onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'phone'})">
								+90 (850) 840 77 93
								</a> 
							</span>
							<span>{{__('pages/layout.e_posta')}}:
								<a 	href="mailto:destek@enbiosis.com" 
									style="color:#696969"
									onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'mail'})">
									destek@enbiosis.com
								</a>
								-
								<a 	href="mailto:info@enbiosis.com"
									style="color:#696969"
									onclick="pushObjectToDataLayer({ event: 'gaEvent', Action: 'click', Category: 'mail'})">
									info@enbiosis.com
								</a>
							</span>

						</div> --}}
					</div>
					<div class="col-md-12 footer__copyright" style="direction: rtl">
						<span>ُen-diet © {{date('Y')}}.</span>
					</div>
				</div>
			</div>
		</section>
		{{-- @include('include.cookie') --}}
		<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
		<script src="{{ url('/') }}/new/js/owl.carousel.min.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
		<script src="{{ url('/') }}/new/js/script.js"></script>
		@yield('scripts')
		@include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])
	</body>
</html>
