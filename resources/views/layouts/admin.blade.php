<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <!-- flag Icon -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.1.0/css/flag-icon.min.css" rel="stylesheet">
    <!-- font-awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" integrity="sha256-+N4/V/SbAFiW1MPBCXnfnP9QSN3+Keu+NlB+0ev/YKQ=" crossorigin="anonymous" />

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
    @yield('styles')
</head>
<body class="loading">
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">

            <button class='navbar-toggler' type="button" onclick="myFunction()" id="sidebarCollapse" >
                <span class="navbar-toggler-icon"></span>
            </button>

            <a href="{{ route('admin.dashboard') }}"><img class='navbar-brand p-0' src="{{url('/')}}/new/img/logo.webp" alt='Dashboard Home'/></a>
            
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto mr-5">
                    <li class="nav-item dropdown">
                        @if (\Session::has('locale'))

                            <a class="nav-link dropdown-toggle" href="#" id="dropdown09" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-globe"></i> {{__('app.dil')}}</a>
                        
                        @else
                        
                            <a class="nav-link dropdown-toggle" href="#" id="dropdown09" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-globe"></i> {{__('app.dil')}}</a>
                        
                        @endif
                        
                        <div class="dropdown-menu" aria-labelledby="dropdown09">
                            <a class="dropdown-item" href="{{ route('lang', 'tr') }}"  > <span class="flag-icon flag-icon-tr"> </span>  Türkçe      </a>
                            <a class="dropdown-item" href="{{ route('lang', 'en') }}"  > <span class="flag-icon flag-icon-gb"> </span>  English     </a>
                            <a class="dropdown-item" href="{{ route('lang', 'ru') }}"  > <span class="flag-icon flag-icon-ru"> </span>  русский     </a>
                            <a class="dropdown-item" href="{{ route('lang', 'uk') }}"  > <span class="flag-icon flag-icon-ua"> </span>  Українська  </a>
                        </div>
                    </li>
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('admin.logout') }}"
                                    onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <a href="{{ route('admin.profile') }}" class="dropdown-item">
                                     Update Your Info
                                </a>

                                <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </nav>

        {{--  <main class="py-4">
            @yield('content')
        </main>  --}}
        <div class='container-fluid'>
            <div class='row flex-nowrap mr-0'>
                <nav class='navbar-dark sidebar' id='sidebar'>
                    <div class='sidebar-sticky'>
                        <div class='navbar-brand  w-100 text-left border-bottom border-dark'>
                                <img class="img-responsive img-fluid mr-3 mb-3" style ="border-radius: 50%;max-width: 30%" src="{{ Auth::user()->photo ? url('/') . '/img/private/' . Auth::user()->photo->path : url('/') . '/img/private/default.png'}}" alt="">
                            Welcome
                        </div>
                        <div>
                            <ul class='navbar-nav mb-5'>
                                <li class='nav-item py-2'>
                                    <a href="{{ route('admin.dashboard') }}" class='nav-link ml-3'>
                                        <i class="fas fa-home mr-2"></i> <span>Dashboard</span>
                                    </a>
                                </li>
                                    
                                <li class='nav-item py-2'>
                                    <a href="{{ route('admin.admins.index') }}" class='nav-link ml-3'>
                                        <i class="fas fa-users-cog mr-2"></i> <span>Admins</span>
                                    </a>
                                </li>
                                <li class='nav-item py-2'>
                                    <a href="{{ route('admin.users.index') }}" class='nav-link ml-3'>
                                        <i class="fas fa-users-cog mr-2"></i> <span>Users</span>
                                    </a>
                                </li>
                                <li class='nav-item py-2'>
                                    <a href="{{ route('admin.products.index') }}" class='nav-link ml-3'>
                                        <i class="far fa-eye mr-2"></i> <span>Products</span>
                                    </a>
                                </li>
                                
                                <li class='nav-item py-2'>
                                    <a href="{{ route('admin.dietitians.index') }}" class='nav-link ml-3'>
                                        <i class="far fa-images mr-2"></i> <span>Dietitian</span>
                                    </a>
                                </li>

                                <li class='nav-item py-2'>
                                    <a href="{{ route('admin.selling-teams.index') }}" class='nav-link ml-3'>
                                        <i class="fas fa-sitemap mr-2"></i> <span>Selling Teams</span>
                                    </a>
                                </li>

                                <li class='nav-item py-2'>
                                    <a href="{{ route('admin.coupons.index') }}" class='nav-link ml-3'>
                                        <i class="fas fa-shield-alt mr-2"></i> <span>Coupons</span>
                                    </a>
                                </li>

                                <li class='nav-item py-2'>
                                    <a href="{{ route('admin.campaigns.index') }}" class='nav-link ml-3'>
                                        <i class="fas fa-compass mr-2"></i> <span>Kampanyalar</span>
                                    </a>
                                </li>

                                <li class='nav-item py-2'>
                                    <a href="{{route('admin.payment.index')}}" class='nav-link ml-3'>
                                        <i class="fas fa-money-check-alt mr-2"></i> <span>Payment</span>
                                    </a>
                                </li>
                                <li class='nav-item py-2'>
                                    <a href="{{ route('admin.orders.index') }}" class='nav-link ml-3'>
                                        <i class="fab fa-speakap mr-2"></i> <span>Orders</span>
                                    </a>
                                </li>
                                
                                <li class='nav-item py-2'>
                                    <a href="{{ route('admin.kits.index') }}" class='nav-link ml-3'>
                                        <i class="far fa-handshake mr-2"></i> <span>kits</span>
                                    </a>
                                </li>
                                
                                <li class='nav-item py-2'>
                                    <a href="{{ route('admin.surveys.index') }}" class='nav-link ml-3'>
                                        <i class="fas fa-microchip  mr-2"></i> <span>surveys</span>
                                    </a>
                                </li>
                                <li class='nav-item py-2'>
                                    <a href="{{route('admin.answer.index')}}" class='nav-link ml-3'>
                                        <i class="fas fa-users  mr-2"></i> <span>surveys Answer</span>
                                    </a>
                                </li>
                                <li class='nav-item py-2'>
                                    <a href="{{route('admin.contact.index')}}" class='nav-link ml-3'>
                                        <i class="far fa-id-badge mr-2"></i><span>contact us</span>
                                    </a>
                                </li>
                                <li class='nav-item py-2'>
                                    <a href="{{route('admin.resources.index')}}" class='nav-link ml-3'>
                                        <i class="fab fa-sourcetree  mr-2"></i> <span>Resources</span>
                                    </a>
                                </li>
                                <li class='nav-item py-2'>
                                    <a href="{{route('admin.activity.index')}}" class='nav-link ml-3'>
                                        <i class="fas fa-clipboard-list  mr-2"></i> <span>activity Logs</span>
                                    </a>
                                </li>
                                <li class='nav-item py-2'>
                                    <a href="{{route('admin.errors.index')}}" class='nav-link ml-3'>
                                        <i class="fas fa-bomb  mr-2"></i> <span>Error Logs</span>
                                    </a>
                                </li>
                                <li class='nav-item py-2'>
                                    <a href="{{route('admin.error.logs')}}" class='nav-link ml-3'>
                                        <i class="fas fa-bug  mr-2"></i> <span>General Errors Logs</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>

                <main class="py-4">
                    @yield('content')
                </main>
                {{--  <div class='main py-5'>
                   
                </div>  --}}
            </div>
        </div>
    </div>
    <div class="modal-load"><!-- Place at bottom of page --></div>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script>
        document.onreadystatechange = function () {
		var state = document.readyState
			if (state == 'complete') {
			setTimeout(function(){
				$body.removeClass("loading");
			},1000);
		}
	}
        function myFunction(){document.getElementById('sidebar').classList.toggle('active');}
        $(document).ready(function(){
            if( $('#message').length ){
                setTimeout(function(){
                        $("#message").fadeOut(2000);
                }, 3000);
            }  
            $body = $("body");
            $(document).on({
                ajaxStart: function() { $body.addClass("loading");    },
                ajaxStop: function() { $body.removeClass("loading"); }    
            });
        });
    </script>
    @yield('script')
    @include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])
</body>
</html>
