<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <!-- flag Icon -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.1.0/css/flag-icon.min.css" rel="stylesheet">
    <!-- font-awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" integrity="sha256-+N4/V/SbAFiW1MPBCXnfnP9QSN3+Keu+NlB+0ev/YKQ=" crossorigin="anonymous" />

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <a href="{{route('/')}}"><img class='navbar-brand p-0' src="{{url('/')}}/new/img/logo.webp" alt='Dashboard Home'/></a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto mr-5">
                    {{-- <li class="nav-item dropdown">
                        @if (\Session::has('locale'))

                            <a class="nav-link dropdown-toggle" href="#" id="dropdown09" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-globe"></i> {{__('app.dil')}}</a>

                        @else

                            <a class="nav-link dropdown-toggle" href="#" id="dropdown09" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-globe"></i> {{__('app.dil')}}</a>

                        @endif

                        <div class="dropdown-menu" aria-labelledby="dropdown09">
                            <a class="dropdown-item" href="{{ route('lang', 'tr') }}"  > <span class="flag-icon flag-icon-tr"> </span>  Türkçe      </a>
                            <a class="dropdown-item" href="{{ route('lang', 'en') }}"  > <span class="flag-icon flag-icon-gb"> </span>  English     </a>
                            <a class="dropdown-item" href="{{ route('lang', 'ru') }}"  > <span class="flag-icon flag-icon-ru"> </span>  русский     </a>
                            <a class="dropdown-item" href="{{ route('lang', 'uk') }}"  > <span class="flag-icon flag-icon-ua"> </span>  Українська  </a>
                        </div>
                    </li> --}}
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('app.login') }}</a>
                        </li>
                        @if (Route::has('register-kit.form'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register-kit.form') }}">{{ __('app.register_kit') }}</a>
                            </li>
                        @endif
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                        @if (Route::has('register-kit.form'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register-kit.form') }}">{{ __('app.register_kit') }}</a>
                        </li>
                        @endif
                    @endguest
                </ul>
            </div>
        </nav>

        {{--  <main class="py-4">
            @yield('content')
        </main>  --}}
        @guest

        @else

        @endguest
        <main class="py-4">
            @yield('content')
        </main>
    </div>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function(){
            if( $('#_enbiosis_errors_enbiosis_').length ){
                setTimeout(function(){
                        $("#_enbiosis_errors_enbiosis_").fadeOut(3000);
                }, 5000);
            }
        });
    </script>
     @yield('script')
     @include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])
</body>
</html>
