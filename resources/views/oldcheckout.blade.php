@extends('layouts.public')
@section('styles')
<style>
.border-top { border-top: 1px solid #e5e5e5; }
.border-bottom { border-bottom: 1px solid #e5e5e5; }
.border-top-gray { border-top-color: #adb5bd; }

.box-shadow { box-shadow: 0 .25rem .75rem rgba(0, 0, 0, .05); }

.lh-condensed { line-height: 1.25; }
/* body {
  background: #00b09b;
  background: -webkit-linear-gradient(to right, #00b09b, #96c93d);
  background: linear-gradient(to right, #00b09b, #96c93d);
  min-height: 100vh;
} */

.text-gray {
  color: #aaa;
}
</style>
@endsection
@section('content')
<section class="header" style="height: 30vh;"></section>
<div class="container" style="max-width: 960px;">
    <div class="py-5 text-center">
        <img class="d-block mx-auto mb-4" src="{{url('/')}}/img/header-logo.webp" alt="" width="120">
        <h2> {{__('app.Check_Out')}}</h2>
      </div>
{{-- {{print_r(session('cart'))}} --}}
        <div class="row">
            <div class="col-md-5 order-md-2 mb-4">
                <h4 class="d-flex justify-content-between align-items-center mb-3">
                    <span class="text-muted">{{__('app.Shopping_Cart')}}</span>
                    <span class="badge badge-secondary badge-pill">{{ session('cart') ? count(session('cart')) : '0'}}</span>
                </h4>
                @if(session('cart'))
                    <ul class="list-group mb-3">
                        @php $total = 0 ; @endphp
                @foreach(session('cart') as $id => $details)
                        <li class="list-group-item d-flex justify-content-between lh-condensed">
                            <div>
                                <h6 class="my-0">{{ $details['name'] }}</h6>
                                <small class="text-muted">{{__('app.Quantity')}}:  {{$details['quantity']}}</small>
                            </div>
                            <span class="text-muted"> {{__('app.price')}} {{ $details['price'] }} TL</span>
                            <span class="text-muted">{{__('app.Subtotal')}} {{ $details['price'] * $details['quantity']}} TL</span>
                        </li>
                        @php $total = $total + ( $details['price'] * $details['quantity']) ;@endphp
                @endforeach
                {{-- Total --}}
                        <li class="list-group-item d-flex justify-content-between">
                            <span>{{__('app.Total')}} (TL)</span>
                            <strong>{{$total}}TL</strong>
                        </li>
                  @endif
                  @if (session()->get('coupon') && session('cart'))
                    <li class="list-group-item d-flex justify-content-between">
                    <span >{{__('app.indirim')}} {{ session()->get('coupon') }}%</span>
                    <strike><strong class="text-danger">{{$total}}TL</strong></strike>  <strong class="text-success">{{(double) ( ( ( 100 - session()->get('coupon') ) * $total ) / 100 )}} TL</strong>
                    </li>
                    <li class="list-group-item d-flex justify-content-between">
                      {!! Form::open(['method'=>'POST', 'action' => 'ShopController@removeCouponFromPaymentForm']) !!}
                        {!! Form::submit(__('app.sil_kopun'), ['class'=>'btn btn-primary']) !!}
                      {!! Form::close() !!}
                    </li>
                  @elseif(session('cart'))
                  <li class="list-group-item d-flex justify-content-between">
                    {!! Form::open(['method'=>'POST', 'action' => 'ShopController@checkForCoupon', 'class' => 'needs-validation p-2', 'novalidate']) !!}
                      <div class="input-group">
                        <input type="text" class="form-control" placeholder="{{ __('app.kupon') }} code" name="code" id="code">
                        <div class="input-group">
                            <button type="submit" class="btn btn-secondary">{{__('app.submit')}}</button>
                        </div>
                      </div>
                    </form>
                  </li>
                  @else 
                  
                  @endif
              </ul>     
            </div>
        @if(session('cart'))
        @include('include.form-errors')
        <div class="col-md-7 order-md-1">
            <h4 class="mb-3">{{__('app.Billing_address')}}</h4>

            {!! Form::open(['method'=>'POST', 'action' => 'ShopController@CheckOut', 'class' => 'needs-validation', 'novalidate']) !!}
            <input type="hidden" name="price" value="{{$total}}">
            <div class="row">
                <div class="col-md-12 mb-3">
                  <label for="name">{{__('app.Name')}}</label>
                  
                  {!! Form::text('name', null, ['class'=>'form-control']) !!}

                </div>
            </div>
            <div class="mb-3">
              <label for="de_code">{{__('app.de_code')}}</label>
              
              {!! Form::text('de_code', null, ['class'=>'form-control']) !!}

            </div>
            <div class="mb-3">
                <label for="email">{{__('app.iletisim_mail')}} <span class="text-muted"></span></label> 
                {!! Form::email('email', null, ['class'=>'form-control', 'placeholder' => 'you@example.com']) !!}
                
                <div class="invalid-feedback">
                  Please enter a valid email address for shipping updates.
                </div>
              </div>
              <div class="mb-3">
                <label for="address">{{__('app.adres')}}</label>

                {!! Form::text('address', null, ['class'=>'form-control', 'placeholder' => '"1234 Main St']) !!}

                <div class="invalid-feedback">
                  Please enter your shipping address.
                </div>
              </div>

              <div class="mb-3">
                <label for="phone">{{__('app.iletisim_tel')}}</label>
                
                {!! Form::text('phone', null, ['class'=>'form-control', 'placeholder' => '53xxxxxxxx']) !!}
              </div>
          

            <hr class="mb-4">
            {{-- <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="same-address">
              <label class="custom-control-label" for="same-address">Shipping address is the same as my billing address</label>
            </div>
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="save-info">
              <label class="custom-control-label" for="save-info">Save this information for next time</label>
            </div> --}}

            <h4 class="mb-3">{{__('app.Payment')}}</h4>
            <div class="row">
              <div class="col-md-6 mb-3">
                <label for="cc-name">{{__('app.Name_on_card')}}</label>
                <input type="text" class="form-control" id="cc-name" name= "cc-name" placeholder="" required>
                <small class="text-muted">{{__('app.Name_on_card_desc')}}</small>
                <div class="invalid-feedback">
                  Name on card is required
                </div>
              </div>
              <div class="col-md-6 mb-3">
                <label for="card_number">{{__('app.Credit_card_number')}}</label>
                {{-- <input type="text" class="form-control" id="card_number" placeholder="" required> --}}
                {!! Form::text('card_number', '5355761528128256', ['class'=>'form-control', 'placeholder' => '']) !!}
                <div class="invalid-feedback">
                  Credit card number is required
                </div>
              </div>
            </div>
            <div class="row">
              {{-- <div class="col-md-3 mb-3">
                <label for="expiration_year">{{__('app.Expiration_year')}}</label>
                
              </div> --}}
              <div class="col-md-12 mb-3 pt-4">
                <label for="expiratio_month">{{__('app.Expiration_Month')}}</label><br>
                <select style="" name="expiration_month" id="expiration_month" required>
                    @for($i = 1; $i <= 12; ++$i)
                        <option value="{{ $i  < 10 ? '0' . $i  : $i }}" name="expiration_month" id="expiration_month" required>{{ $i  < 10 ? '0' . $i  : $i }}</option>
                    @endfor
                </select>
                <select style="" name="expiration_year" id="expiration_year" required>
                  {{-- <option value="{{16}}" name="expiration_year" id="expiration_year" required>2016</option> --}}
                  @for($i = 2020; $i < 2040; ++$i)
                      <option value="{{substr($i, -2)}}" name="expiration_year" id="expiration_year" required>{{$i}}</option>
                  @endfor
              </select>
                
              </div>
              <br>
              <div class="col-md-4 mb-3">
                <label for="cvc">CVC</label>
                {!! Form::text('cvc', null, ['style' => 'display:inline','class'=>'form-control', 'placeholder' => '000']) !!}
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 mb-3">
                  <div class="form-check">
                      <input type="checkbox" class="form-check-input" name="Mesafeli_Satış_Sözleşmesi">
                      <label class="form-check-label"> Mesafeli Satış Sözleşmesi <a type="button"data-toggle="modal" data-target="#satis">kabul ediyorum.</a>  </label>
                      <br>
                      <input type="checkbox" class="form-check-input" name="Ön_Bilgilendirme_Formu">
                      <label class="form-check-label">Ön bilgilendirme formunu  <a type="button"data-toggle="modal" data-target="#exampleModalLong">kabul ediyorum.</a></label>
                      <br>
                      <input type="checkbox" class="form-check-input" name="Aydınlatma_Formu">
                      <label class="form-check-label">   Aydınlatma formunu <a type="button"data-toggle="modal" data-target="#bil"> kabul ediyorum.</a> </label>
                      <small> </small>
                  </div>
                </div>
                <div class="col">
                  
                  
                </div>
              </div>    
                <button class="btn btn-primary btn-lg btn-block" type="submit">{{__('app.il_gonder')}}</button>
            {!! Form::close() !!}
        </div> 
        @else
        <div class="col-md-7 order-md-1">
            <a href="{{ route('shop') }}" class="btn btn-warning"><i class="fa fa-angle-left"></i> {{__('app.Continue_Shopping')}}</a>
        </div>
        @endif
        {{-- End Of Row --}}
      </div>
      {{-- End Of Continer --}}
    </div>
@endsection
@section('script')
@endsection