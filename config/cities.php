<?php

return [
    'qa' => [
        'name' => 'Qatar',
        'native' => 'قطر',
    ],
    'bh' => [
        'name' => 'Bahrain',
        'native' => 'البحرين',
    ],
];
