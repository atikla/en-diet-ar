<?php

return [

    /*
    |--------------------------------------------------------------------------
    | limit Values For Bagirsak 
    |--------------------------------------------------------------------------
    |
    */
    // İyi < 0.2
    // 0.2 < Ortalama < 0.61
    // 0.61 < Needs improvement
    'Vücut Kütle Endeksi-Mikrobiyom İlişkiniz' => [
        0   => 'iyi',
        20  => 'orta_1',
        61  => 'orta_2',
        100 => 'kotu'
    ],
    // Needs improvement< 0.34
    // 0.15 < Ortalama < 0.6
    // 0.6 < iyi
    // orta_2 =  ( 0.34 + 0.15 ) / 2
    'Karbonhidrat Metabolizmanız' => [
        0   => 'kotu',
        24  => 'orta_2',
        60  => 'orta_1',
        100 => 'iyi'
    ],
    // Needs improvement< 0.15
    // 0.15 < Ortalama < 0.4
    // 0.4 < iyi
    'Protein Metabolizmanız' => [
        0   => 'kotu',
        15  => 'orta_2',
        40  => 'orta_1',
        100 => 'iyi'
    ],
    // Needs improvement< 0.12
    // 0.12 < Ortalama < 0.43
    // 0.43 < iyi
    'Yağ Metabolizmanız' => [
        0   => 'kotu',
        12  => 'orta_2',
        43  => 'orta_1',
        100 => 'iyi'
    ],
    // Needs improvement< 0.25
    // 0.25 < Ortalama < 0.7
    // 0.7 < iyi
    'Probiyotik Mikroorganizma Ölçümünüz' => [
        0   => 'kotu',
        25  => 'orta_2',
        70  => 'orta_1',
        100 => 'iyi'
    ],
    // Needs improvement< 0.3
    // 0.3 < Ortalama < 0.78
    // 0.78 < iyi
    'Uyku Kaliteniz' => [
        0   => 'kotu',
        30  => 'orta_2',
        78  => 'orta_1',
        100 => 'iyi'
    ],
    // Needs improvement< 0.4
    // 0.4 < Ortalama < 0.67
    // 0.67 < iyi
    'Bağırsak Hareketlilik Ölçümünüz' => [
        0   => 'kotu',
        40  => 'orta_2',
        67  => 'orta_1',
        100 => 'iyi'
    ],
    // İyi < 0.25
    // 0.2 < Ortalama < 0.45
    // 0.45 < Needs improvement
    // orta_1 =  ( 0.25 + 0.20 ) / 2
    'Gluten Hassasiyeti Ölçümünüz' => [
        0   => 'iyi',
        23  => 'orta_1',
        45  => 'orta_2',
        100 => 'kotu'
    ],
    // İyi < 0.2
    // 0.2 < Ortalama < 0.75
    // 0.75 < Needs improvement
    'Laktoz Hassasiyeti Ölçümünüz' => [
        0   => 'iyi',
        20  => 'orta_1',
        75  => 'orta_2',
        100 => 'kotu'
    ],
    // eeds improvement< 0.5
    // 0.5 < Ortalama < 0.83
    // 0.83 < iyi
    'Vitamin Senteziniz' => [
        0   => 'kotu',
        50  => 'orta_2',
        83  => 'orta_1',
        100 => 'iyi'
    ],
    // İyi < 0.4
    // 0.4 < Ortalama < 0.6
    // 0.6 < Needs improvement
    'Yapay Tatlandırıcı Hasarınız' => [
        0   => 'iyi',
        40  => 'orta_1',
        60  => 'orta_2',
        100 => 'kotu'
    ],
    // İyi < 0.11
    // 0.11 < Ortalama < 0.7
    // 0.7 < Needs improvement
    'Şeker Tüketim Skorunuz' => [
        0   => 'iyi',
        11  => 'orta_1',
        70  => 'orta_2',
        100 => 'kotu'
    ],
    // İyi < 0.4
    // 0.4 < Ortalama < 0.65
    // 0.65 < Needs improvement
    'Antibiyotik Hasar Ölçümünüz' => [
        0   => 'iyi',
        40  => 'orta_1',
        65  => 'orta_2',
        100 => 'kotu'
    ],
    // İyi < 0.4
    // 0.2 < Ortalama < 0.6
    // 0.6 < Needs improvement
    // orta_1 =  ( 0.40 + 0.20 ) / 2
    'Otoimmünite Endeksiniz' => [
        0   => 'iyi',
        30  => 'orta_1',
        60  => 'orta_2',
        100 => 'kotu'
    ],

    'colors' => [
        'iyi'       => 'rgba(89,196,217,1)',
        'orta_1'    => 'rgba(237,209,161,1)',
        'orta_2'    => 'rgba(238,175,130,1)',
        'kotu'      => 'rgba(227,99,79,1)'
    ]
];
