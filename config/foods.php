<?php

return [

    /*
    |--------------------------------------------------------------------------
    | food scores category
    |--------------------------------------------------------------------------
    |
    */
    'Tümü'                  => 0,
    'Süt ve Süt Ürünleri'   => 1,
    'Et ve Et Ürünleri'     => 2,
    'Ekmek ve Tahıl Grubu'  => 3,
    'Kurubaklagiller'       => 4,
    'Sebzeler'              => 5,
    'Meyveler'              => 6,
    'Yağlar'                => 7,
    'İçecekler'             => 8,
    'Takviyeler'            => 9,
    'Diğer'                 => 10,
    'data' => [
        0 => [
            'id' => 'Tümü',
            'foodType' => '',
            'foodItems' => [
                0 => [
                    'rank' => 'iyi',
                    'items' => [
                    ]
                ],
                1 => [
                    'rank' => 'orta',
                    'items' => [
                    ]
                ],
                2 => [
                    'rank' => 'kotu',
                    'items' => [
                    ]
                ],
            ]
        ],
        1 => [
            'id' => 'Süt ve Süt Ürünleri',
            'foodType' => '',
            'foodItems' => [
                0 => [
                    'rank' => 'iyi',
                    'items' => [
                    ]
                ],
                1 => [
                    'rank' => 'orta',
                    'items' => [
                    ]
                ],
                2 => [
                    'rank' => 'kotu',
                    'items' => [
                    ]
                ],
            ]
        ],
        2 => [
            'id' => 'Et ve Et Ürünleri',
            'foodType' => '',
            'foodItems' => [
                0 => [
                    'rank' => 'iyi',
                    'items' => [
                    ]
                ],
                1 => [
                    'rank' => 'orta',
                    'items' => [
                    ]
                ],
                2 => [
                    'rank' => 'kotu',
                    'items' => [
                    ]
                ],
            ]
        ],
        3 => [
            'id' => 'Ekmek ve Tahıl Grubu',
            'foodType' => '',
            'foodItems' => [
                0 => [
                    'rank' => 'iyi',
                    'items' => [
                    ]
                ],
                1 => [
                    'rank' => 'orta',
                    'items' => [
                    ]
                ],
                2 => [
                    'rank' => 'kotu',
                    'items' => [
                    ]
                ],
            ]
        ],
        4 => [
            'id' => 'Kurubaklagiller',
            'foodType' => '',
            'foodItems' => [
                0 => [
                    'rank' => 'iyi',
                    'items' => [
                    ]
                ],
                1 => [
                    'rank' => 'orta',
                    'items' => [
                    ]
                ],
                2 => [
                    'rank' => 'kotu',
                    'items' => [
                    ]
                ],
            ]
        ],
        5 => [
            'id' => 'Sebzeler',
            'foodType' => '',
            'foodItems' => [
                0 => [
                    'rank' => 'iyi',
                    'items' => [
                    ]
                ],
                1 => [
                    'rank' => 'orta',
                    'items' => [
                    ]
                ],
                2 => [
                    'rank' => 'kotu',
                    'items' => [
                    ]
                ],
            ]
        ],
        6 => [
            'id' => 'Meyveler',
            'foodType' => '',
            'foodItems' => [
                0 => [
                    'rank' => 'iyi',
                    'items' => [
                    ]
                ],
                1 => [
                    'rank' => 'orta',
                    'items' => [
                    ]
                ],
                2 => [
                    'rank' => 'kotu',
                    'items' => [
                    ]
                ],
            ]
        ],
        7 => [
            'id' => 'Yağlar',
            'foodType' => '',
            'foodItems' => [
                0 => [
                    'rank' => 'iyi',
                    'items' => [
                    ]
                ],
                1 => [
                    'rank' => 'orta',
                    'items' => [
                    ]
                ],
                2 => [
                    'rank' => 'kotu',
                    'items' => [
                    ]
                ],
            ]
        ],
        8 => [
            'id' => 'İçecekler',
            'foodType' => '',
            'foodItems' => [
                0 => [
                    'rank' => 'iyi',
                    'items' => [
                    ]
                ],
                1 => [
                    'rank' => 'orta',
                    'items' => [
                    ]
                ],
                2 => [
                    'rank' => 'kotu',
                    'items' => [
                    ]
                ],
            ]
        ],
        9 => [
            'id' => 'Takviyeler',
            'foodType' => '',
            'foodItems' => [
                0 => [
                    'rank' => 'iyi',
                    'items' => [
                    ]
                ],
                1 => [
                    'rank' => 'orta',
                    'items' => [
                    ]
                ],
                2 => [
                    'rank' => 'kotu',
                    'items' => [
                    ]
                ],
            ]
        ],
        10 => [
            'id' => 'Diğer',
            'foodType' => '',
            'foodItems' => [
                0 => [
                    'rank' => 'iyi',
                    'items' => [
                    ]
                ],
                1 => [
                    'rank' => 'orta',
                    'items' => [
                    ]
                ],
                2 => [
                    'rank' => 'kotu',
                    'items' => [
                    ]
                ],
            ]
        ]
    ]
];
