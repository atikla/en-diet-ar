<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Bagirsak Scores labels
    |--------------------------------------------------------------------------
    |
    */
    
        'Karbonhidrat Metabolizmanız'=> [
            0 => 'Sizin Örneğiniz',
            1 => 'Toplum'
        ],
        
        'Protein Metabolizmanız'=> [
            0 => 'Sizin Örneğiniz',
            1 => 'Toplum'
        ],
        
        'Yağ Metabolizmanız'=> [
            0 => 'Sizin Örneğiniz',
            1 => 'Toplum',
        ],
        
        'Vücut Kütle Endeksi-Mikrobiyom İlişkiniz'=> [
            0 => 'Sizin Örneğiniz',
            1 => 'Toplum',
            2 => 'Zayıf',
            3 => 'Aşırı Kilolu'
        ],
        
        'Probiyotik Mikroorganizma Ölçümünüz'=> [
            0 => 'Sizin Örneğiniz',
            1 => 'Toplum',
            2 => 'Düşük',
            3 => 'Yüksek',
        ],
        
        'Uyku Kaliteniz'=> [
            0 => 'Sizin Örneğiniz',
            1 => 'Toplum',
            2 => 'Uykusuzluk Çekenler', 
            3 => 'Uyku Kalitesi Yüksek Olanlar',
        ],
        
        'Bağırsak Hareketlilik Ölçümünüz'=> [
            0 => 'Sizin Örneğiniz',
            1 => 'Toplum',
            2 => 'Bağırsağı Tembel',
            3 => 'Bağırsağı Hareketli',
        ],
        
        'Gluten Hassasiyeti Ölçümünüz'=> [
            0 => 'Sizin Örneğiniz',
            1 => 'Toplum',
            2 => 'Hassasiyeti Olmayan',
            3 => 'Glutene Hassasiyetli',
        ],
        
        'Laktoz Hassasiyeti Ölçümünüz'=> [
            0 => 'Sizin Örneğiniz',
            1 => 'Toplum',
            2 => 'Hassasiyeti Olmayan',
            3 => 'Laktoza Hassasiyetli',
        ],
        
        'Vitamin Senteziniz'=> [
            0 => 'Sizin Örneğiniz',
            1 => 'Toplum',
            2 => 'Düşük',
            3 => 'Yüksek',
        ],
        
        'Yapay Tatlandırıcı Hasarınız'=> [
            0 => 'Sizin Örneğiniz',
            1 => 'Toplum',
            2 => 'Yapay Tatlandırıcı Tüketmeyen',
            3 => 'Yapay Tatlandırıcı Tüketen',
        ],
        
        'Şeker Tüketim Skorunuz'=> [
            0 => 'Sizin Örneğiniz',
            1 => 'Toplum',
            2 => 'Şeker Tüketmeyen',
            3 => 'Şeker Tüketen',
        ],
        
        'Antibiyotik Hasar Ölçümünüz'=> [
            0 => 'Sizin Örneğiniz',
            1 => 'Toplum',
            2 => 'Antibiyotik Kullanmayan',
            3 => 'Antibiyotik Kullanan',
        ],
        
        'Otoimmünite Endeksiniz'=> [
            0 => 'Sizin Örneğiniz',
            1 => 'Toplum',
            2 => 'Düşük',
            3 => 'Yüksek'
        ],
];
