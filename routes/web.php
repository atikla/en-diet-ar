<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','GeneralController@welcome')->name('/');
// Route::post('/setcookie','GeneralController@setcookie')->name('setcookie');
// Route::get('/cookies-policy','GeneralController@cookiesPolicy')->name('cookies-policy');
//Routes For pages

// Route::get('/kisisellestirilmis-beslenme', 'GeneralController@kisisellestirilmis_beslenme')->name('kisisellestirilmis-beslenme');
// Route::get('/bagisiklik', 'GeneralController@bagisiklik')->name('bagisiklik');
// Route::get('/mikrobiyom-testleri', 'GeneralController@mikrobiyom_testleri')->name('mikrobiyom_testleri');
// Route::get('/kilo-kontrolu', 'GeneralController@kilo_kontrolu')->name('kilo-kontrolu');
// Route::get('/bagirsak-hastaliklari', 'GeneralController@bagirsak_hastaliklari')->name('bagirsak-hastaliklari');
// Route::get('/ibs', 'GeneralController@ibs')->name('ibs');
// Route::get('/gida-hassasiyeti', 'GeneralController@colyak')->name('colyak-hastalıgı');
// Route::get('/mikrobiyom-diyeti', 'GeneralController@mikrobiyomDiyeti')->name('mikrobiyom-diyeti');
// Route::get('/actifit', 'GeneralController@actifit')->name('actifit');
// Route::get('/investor ', 'GeneralController@yatirimci')->name('yatirimci');

Route::get('/mesafeli-satis-sozlesmesi', 'GeneralController@mesafeliSatisSozlesmesi')->name('mesafeli-satis-sozlesmesi');

Route::get('/on-bilgilendirme-formu', 'GeneralController@onBilgilendirmeFormu')->name('on-bilgilendirme-formu');

Route::get('/aydinlatma-formu', 'GeneralController@aydinlatmaFormu')->name('aydinlatma-formu');

// Route::get('/tesekkurler/{traking}', 'GeneralController@tesekkurler')->name('tesekkurler');

// Route::get('/uyelik-sozlesmesi', 'GeneralController@uyelikSozlesmesi')->name('uyelik-sozlesmesi');

// Route::get('/gizlilik-politikasi', 'GeneralController@gizlilikPolitikasi')->name('gizlilik-politikasi');

// Route::get('hakkimizda', 'GeneralController@hakkimizda')->name('hakkimizda');

// Route::get('/mikrobiyom', 'GeneralController@mikrobiyom')->name('mikrobiyom');

// Route::get('/urunlerimiz', 'GeneralController@mikrobiyom_analizi')->name('mikrobiyom-analizi');

// Route::get('bilim', 'GeneralController@bilim')->name('bilim');

// Route::get('enbiosis-test', 'GeneralController@enbiosisTest')->name('enbiosis-test');

// Route::get('sss', 'GeneralController@sss')->name('sss');

// Route::get('iletisim', 'GeneralController@iletisim')->name('iletisim');

// Route::post('iletisim', 'GeneralController@sendMail')->name('send_iletisim');

// newsletter 
// Route::post('newsletter', 'GeneralController@newsletter')->name('newsletter');

//Prof. Dr. Hakan Alagözlü cv

// Route::get('prof-dr-hakan-alagozlu', 'GeneralController@profDrHakanAlagozlu')->name('prof-dr-hakan-alagozlu');

// //Prof. Dr. Hakan Alagözlü cv

// Route::get('prof-dr-tarkan-karakan', 'GeneralController@profDrTarkanKarakan')->name('prof-dr-tarkan-karakan');

// //Prof. Dr. Halil Coşkun

// Route::get('prof-dr-halil-coskun', 'GeneralController@profDrHalilCoskun')->name('prof-dr-halil-coskun');

// //Dr. Özkan Ufuk Nalbantoğlu

// Route::get('dr-ozkan-ufuk-nalbantoglu', 'GeneralController@drOzkanUfukNalbantoglu')->name('dr-ozkan-ufuk-nalbantoglu');

// //Doç. Dr. Aycan Gündoğdu

// Route::get('doc-dr-aycan-gundogdu', 'GeneralController@docDrAycanGundogdu')->name('doc-dr-aycan-gundogdu');

// //Doç. Dr. Aycan Gündoğdu

// Route::get('ceo-omer-ozkan', 'GeneralController@ceoOmerOzkan')->name('ceo-omer-ozkan');

/*
|--------------------------------------------------------------------------
| Lang Route
|--------------------------------------------------------------------------
*/
Route::get('locale/{locale}', 'GeneralController@locale')->name('lang');

// For Ajax Request For updata and remove items from cart
Route::post('update-cart', 'ShopController@updateCartProduct');

Route::post('installment', 'ShopController@installment');
// Show CheckOut Form
Route::get('checkout', 'CheckoutController@index')->name('checkout');
// Get County based in City
Route::post('getCounty', 'ShopController@getCounty')->name('getCounty');
// get instalments table
Route::post('instalmentsTable', 'GeneralController@instalmentsTable')->name('instalmentsTable');
// Check for Coupon  Form
Route::post('checkForCoupon', 'ShopController@checkForCoupon')->name('checkForCoupon');
// Remove Coupon From Payment form
Route::post('removeCouponFromPaymentForm', 'ShopController@removeCouponFromPaymentForm')->name('removeCouponFromPaymentForm');

// Show CheckOut For prodduct
Route::get('checkout/{slug}', 'ShopController@showCheckOut')->name('checkout.product');
// Submit CheckOut For prodduct
Route::post('checkout/{slug}', 'ShopController@CheckOut')->name('checkout.product.submit');


// Route For get POST Requset from bank api for results
Route::post('checkout-result/{slug}', 'ShopController@CheckOutResult')->name('checkout.result.p');

Route::get('/tesekkurler/{slug}/{traking}', 'GeneralController@tesekkurler')->name('tesekkurler');

//campaign pages
// Show campaign CheckOut Page
// Route::get('kampanya', 'CheckoutController@campaign')->name('Campaign');
// // // Show campaign CheckOut Page
// Route::get('kampanya/{campaignCode}', 'CheckoutController@campaignIndex')->name('checkout.Campaign');
// // // Show campaign CheckOut form
// Route::get('checkout/{campaignCode}/{slug}', 'ShopCampaignController@index')->name('checkout.Campaign.index');

// // updaate kit quantity in campaign 
// Route::post('campaign-update-cart/{campaignCode}', 'ShopCampaignController@updateQuantity')->name('checkout.Campaign.updatecart');

// // get instalments table
// Route::post('campaign-instalments-table/{campaignCode}', 'ShopCampaignController@campaignInstalmentsTable')->name('checkout.campaign.instalmentsTable');

// // Submit CheckOut campaign Form
// Route::post('checkout/{campaignCode}/{slug}', 'ShopCampaignController@CheckOut')->name('checkout.campaign.submit');

// // Submit CheckOut result campaign Form
// Route::post('checkout-result/{campaignCode}/{slug}', 'ShopCampaignController@CheckOutResult')->name('checkout.campaign.result');
/*
|--------------------------------------------------------------------------
|   Auth Routes For User
|--------------------------------------------------------------------------
*/
Auth::routes(['verify' => true, 'register' => false]);

Route::get('/home', 'HomeController@index')->name('home');

// Register kit Routes
Route::get('register-kit', 'Auth\UserRegisterKitController@showRegisterKitForm')->name('register-kit.form');
Route::post('register-kit', 'Auth\UserRegisterKitController@RegisterKit')->name('register-kit');

Route::get('register-kit-user', 'Auth\UserRegisterKitController@showRegisterKitWithUserForm')->name('register-kit-user.form');
Route::post('register-kit-user', 'Auth\UserRegisterKitController@registerKitWithUser')->name('register-kit-user');
Route::post('login-kit-user', 'Auth\UserRegisterKitController@LoginUserAfterKitCodeMatch')->name('login-kit-user');


/*
|--------------------------------------------------------------------------
| Login and Loguot Routes For Admin
|--------------------------------------------------------------------------
*/
Route::prefix('admin')->group(function() {

    Route::get('/login','Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::post('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');
    // Route::get('/', 'AdminController@index')->name('admin.index');
}) ;

/*
|--------------------------------------------------------------------------
| Login and Loguot Routes For dietitian
|--------------------------------------------------------------------------
*/
Route::prefix('dietitian')->group(function() {

    Route::get('/login','Auth\DietitianLoginController@showLoginForm')->name('dietitian.login');
    Route::post('/login', 'Auth\DietitianLoginController@login')->name('dietitian.login.submit');
    Route::post('logout/', 'Auth\DietitianLoginController@logout')->name('dietitian.logout');
    // Route::get('/', 'DietitianController@index')->name('dietitian.index');
}) ;



/*
|--------------------------------------------------------------------------
| ADMIN Route
|--------------------------------------------------------------------------
*/
Route::get('/admin','AdminController@index')->middleware(['auth:admin', 'admin'])->name('admin.index');
Route::patch('/admin/profile','AdminController@updateProfile')->name('admin.updateProfile');
// operation admin
Route::group(['prefix' => 'operation',  'middleware' => ['auth:admin', 'admin', 'op-admin'], 'as' => 'operation.'], function()
{
    Route::get('/','OperationAdminController@indexRoute')->name('index');
    Route::get('/profile','AdminController@OperationAdminProfile')->name('profile');

    // Admin Orders MAnagement Section Routes
    Route::post('/orders/query', 'OperationAdminController@search')->name('orders.search');
    Route::get('/orders/export', 'OperationAdminController@export')->name('orders.export');
    Route::patch('/kit/{kit}', 'OperationAdminController@updateKit')->name('kit.update');
    Route::post('/kit/delever', 'OperationAdminController@kitDeleverToUser')->name('kit.delever');
    Route::post('/kit/received', 'OperationAdminController@kitReceived')->name('kit.received');
    Route::post('/kit/sent', 'OperationAdminController@kitSentToLab')->name('kit.sent');

    Route::get('/kits', 'OperationAdminController@Kit')->name('kit');
    Route::post('/kits/query', 'OperationAdminController@Kitsearch')->name('kit.search');

    /*
    *   if you want to Add new methods to a resource controller in Laravel
    *   Just add a route to that method separately, before you register the resource:
    */
    Route::resource('orders', 'OperationAdminController');

});

// laboratory admin
Route::group(['prefix' => 'laboratory',  'middleware' => ['auth:admin', 'admin', 'lab-admin'], 'as' => 'lab.'], function()
{

    Route::get('/','LaboratoryAdminController@indexRoute')->name('index');

    Route::get('/profile','AdminController@LabAdminprofile')->name('profile');

    // answers
    Route::get('answer', 'LaboratoryAdminController@indexAnswer')->name('answer.index');
    Route::post('kit/answer/export', 'LaboratoryAdminController@export')->name('answer.export');
    Route::post('kit/answer/query', 'LaboratoryAdminController@search')->name('answer.search');
    Route::get('kit/answer/{kit_code}', 'LaboratoryAdminController@show')->name('answer.show');


    //kit
    Route::post('kit/upload/food', 'LaboratoryAdminController@uploadFoodJsonFile')->name('food.upload');
    Route::post('kit/upload/microbiome', 'LaboratoryAdminController@uploadMicrobiomeJsonFile')->name('microbiome.upload');
    Route::get('generate/{id}', 'LaboratoryAdminController@Generate')->name('pdf.generate');
});

// Super Admin
Route::group(['prefix' => 'admin',  'middleware' => ['auth:admin', 'admin', 'superAdmin'], 'as' => 'admin.'], function()
{

    Route::get('/dashboard','AdminDashboardController@index')->name('dashboard');

    Route::get('/profile','AdminController@SuperAdminprofile')->name('profile');
    //All the routes that belongs to the group goes here

    // Admin Admin Management Section Routes

    Route::get('/admins/trash', 'AdminAdminsController@trash')->name('admins.trash');
    Route::get('/admins/{id}/restore', 'AdminAdminsController@restore')->name('admins.restore');

    /*
    *   if you want to Add new methods to a resource controller in Laravel
    *   Just add a route to that method separately, before you register the resource:
    */
    Route::resource('admins', 'AdminAdminsController');

    // Admin User Management Section Routes

    Route::get('/users/trash', 'AdminUsersController@trash')->name('users.trash');
    Route::get('/users/{id}/restore', 'AdminUsersController@restore')->name('users.restore');
    Route::post('/users/query', 'AdminUsersController@search')->name('users.search');
    Route::post('/users/link/{user}', 'AdminUsersController@link')->name('kitlink');


    /*
    *   if you want to Add new methods to a resource controller in Laravel
    *   Just add a route to that method separately, before you register the resource:
    */
    Route::resource('users', 'AdminUsersController');

    //Admin Prodcuts MAnagement Section Routes

    Route::get('/products/trash', 'AdminProductsController@trash')->name('products.trash');
    Route::get('/products/{id}/restore', 'AdminProductsController@restore')->name('products.restore');
    Route::get('/products/show/{id}/{slug}', 'AdminProductsController@show')->name('products.show');
    Route::post('/products/query', 'AdminProductsController@search')->name('products.search');

    /*
    *   if you want to Add new methods to a resource controller in Laravel
    *   Just add a route to that method separately, before you register the resource:
    */
    Route::resource('products', 'AdminProductsController', ['except' => ['show'] ] );

    //Admin Coupon MAnagement Section Routes

    Route::get('/coupons/notAvailableCoupons', 'AdminCouponController@notAvailableCoupons')->name('coupons.notAvailableCoupons');
    // Route::get('/coupons/awardedCoupons', 'AdminCouponController@awardedCoupons')->name('coupons.awardedCoupons');
    // Route::post('/coupons/updateGiveOrTake/{id}', 'AdminCouponController@updateGiveOrTake')->name('coupons.updateGiveOrTake');
    Route::post('/coupons/query', 'AdminCouponController@search')->name('coupons.search');


    /*
    *   if you want to Add new methods to a resource controller in Laravel
    *   Just add a route to that method separately, before you register the resource:
    */
    Route::resource('coupons', 'AdminCouponController', ['except' => ['show'] ] );


     //Admin campaigns MAnagement Section Routes
    Route::get('/campaigns/notAvailablecampaigns', 'AdminCampaignController@notAvailableCampaigns')->name('campaigns.not');
 
     /*
     *   if you want to Add new methods to a resource controller in Laravel
     *   Just add a route to that method separately, before you register the resource:
     */
     Route::resource('campaigns', 'AdminCampaignController');

    //Admin Dietitians MAnagement Section Routes

    Route::get('/dietitians/trash', 'AdminDietitiansController@trash')->name('dietitians.trash');
    Route::get('/dietitians/{id}/restore', 'AdminDietitiansController@restore')->name('dietitians.restore');
    Route::post('/dietitians/link/{dietitian}', 'AdminDietitiansController@kitLink')->name('dietitians.kitlink');


    /*
    *   if you want to Add new methods to a resource controller in Laravel
    *   Just add a route to that method separately, before you register the resource:
    */
    Route::resource('dietitians', 'AdminDietitiansController');

    //Admin Orders MAnagement Section Routes
    Route::post('/orders/query', 'AdminOrdersController@search')->name('orders.search');
    Route::get('/orders/export', 'AdminOrdersController@export')->name('orders.export');
    //Admin Orders MAnagement Section Routes
    Route::post('/orders/{id}/assign-ref-code', 'AdminOrdersController@assignRefCode')->name('orders.assign-ref-code');


    /*
    *   if you want to Add new methods to a resource controller in Laravel
    *   Just add a route to that method separately, before you register the resource:
    */
    Route::resource('orders', 'AdminOrdersController');


    //Admin kits MAnagement Section Routes

    Route::group(['prefix' => 'kits', 'as' => 'kits.'], function () {
        Route::get('/trash', 'AdminKitsController@trash')->name('trash');
        Route::get('/{id}/restore', 'AdminKitsController@restore')->name('restore');
        Route::post('/store/new', 'AdminKitsController@storeNewKit')->name('store.new');
        Route::get('/export', 'AdminKitsController@export')->name('export');
        Route::post('/delever', 'AdminKitsController@kitDeleverToUser')->name('delever');
        Route::post('/received', 'AdminKitsController@kitReceived')->name('received');
        Route::post('/sent', 'AdminKitsController@kitSentToLab')->name('sent');
        Route::post('/dislink/{kit}', 'AdminKitsController@dislink')->name('dislink');
        Route::post('/link/{kit}', 'AdminKitsController@link')->name('link');
        // dietitian
        Route::post('/dislink/dietitian/{kit}', 'AdminKitsController@dietitianDislink')->name('dietitian.dislink');
        Route::post('/link/dietitian/{kit}', 'AdminKitsController@dietitianLink')->name('dietitian.link');

        Route::post('/upload/food', 'AdminKitsController@uploadFoodJsonFile')->name('food.upload');
        Route::post('/upload/microbiome', 'AdminKitsController@uploadMicrobiomeJsonFile')->name('microbiome.upload');
        // kit suggestion
        Route::resource('/{kit}/suggestion', 'AdminSuggestionController', [ 'only' => ['destroy', 'store', 'update' ] ] );
        Route::post('/query', 'AdminKitsController@search')->name('search');
        Route::post('/sendMail/{kit_code}', 'AdminKitsController@sendMail')->name('sendMail');
        Route::post('/changeShow/{kit_code}/', 'AdminKitsController@changeShow')->name('changeShow');

        Route::get('generate/{id}', 'AdminKitPdfGeneratorController@Generate')->name('pdf.generate');
        Route::get('generate/', 'AdminKitPdfGeneratorController@index')->name('pdf');
    });

    /*
    *   if you want to Add new methods to a resource controller in Laravel
    *   Just add a route to that method separately, before you register the resource:
    */
    Route::resource('kits', 'AdminKitsController');


    //Admin Survey MAnagement Section Routes

    // Store a new Lang for Survey
    Route::post('/surveys/create/lang/{id}', 'AdminSurveyController@storeLang')->name('surveys.lang.store');

    // Update Lang for Survey
    Route::patch('/surveys/update/lang/{surveyid}-{langid}', 'AdminSurveyController@updateLang')->name('surveys.lang.update');

    // Delete Lang for Survey
    Route::delete('/surveys/delete/{surveyid}-{langid}', 'AdminSurveyController@destroyLang')->name('surveys.lang.destroy');

    /*
    *   if you want to Add new methods to a resource controller in Laravel
    *   Just add a route to that method separately, before you register the resource:
    */
    Route::resource('surveys', 'AdminSurveyController', ['except' => ['show', 'edit'] ]);

    //Admin Survey Sections Management Section Routes

    // Store a new Lang for Survey Section
    Route::post('{id}/sections/create/lang/{sectionId}', 'AdminSurveySectionsController@storeLang')->name('sections.lang.store');

    // Update Lang for Survey Section
    Route::patch('{id}/sections/update/lang/{sectionId}-{langid}', 'AdminSurveySectionsController@updateLang')->name('sections.lang.update');

    // Delete Lang for Survey Section
    Route::delete('{id}/sections/delete/lang/{sectionid}-{langid}', 'AdminSurveySectionsController@destroyLang')->name('sections.lang.destroy');

    /*
    *   if you want to Add new methods to a resource controller in Laravel
    *   Just add a route to that method separately, before you register the resource:
    */
    Route::resource('{id}/sections', 'AdminSurveySectionsController', ['except' => ['show', 'edit'] ]);

    //Admin Survey Sections  questions Management Section Routes

    // Store a new Lang for Survey Section question

    //create view
    Route::get('{survey}/{sections}/questions/create/lang/{questionId}', 'AdminQuestionController@createLang')->name('questions.lang.create');

    // store record
    Route::post('{survey}/{sections}/questions/store/lang/{questionId}', 'AdminQuestionController@storeLang')->name('questions.lang.store');

    // Delete Lang for Survey Section
    Route::delete('{survey}/{sections}/questions/delete/lang/{questionId}-{langid}', 'AdminQuestionController@destroyLang')->name('questions.lang.destroy');
    /*
    *   if you want to Add new methods to a resource controller in Laravel
    *   Just add a route to that method separately, before you register the resource:
    */
    Route::resource('{survey}/{sections}/questions', 'AdminQuestionController');


    //Admin Survey Kit Answer Management Section Routes


    Route::post('kit/answer/export', 'AdminSurveyKitAnswerController@export')->name('answer.export');
    Route::get('survey/{id}/export', 'AdminSurveyKitAnswerController@getSurveyTemplate')->name('survey.export');
    Route::post('kit/answer/query', 'AdminSurveyKitAnswerController@search')->name('answer.search');

    /*
    *   if you want to Add new methods to a resource controller in Laravel
    *   Just add a route to that method separately, before you register the resource:
    */
    Route::resource('kit/answer', 'AdminSurveyKitAnswerController', ['only' => ['index', 'show'] ]);

    /*
    *   if you want to Add new methods to a resource controller in Laravel
    *   Just add a route to that method separately, before you register the resource:
    */
    Route::resource('resources', 'AdminResourcesController', ['except' => ['show'] ]);

    /*
    *   if you want to Add new methods to a resource controller in Laravel
    *   Just add a route to that method separately, before you register the resource:
    */
    Route::resource('payment', 'AdminPaymentController', ['only' => 'index' ]);

    // Error Logs
    Route::get('error/logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')->name('error.logs');

    Route::post('errors/query', 'AdminErrorsController@search')->name('errors.search');
    /*
    *   if you want to Add new methods to a resource controller in Laravel
    *   Just add a route to that method separately, before you register the resource:
    */
    Route::resource('errors', 'AdminErrorsController', ['only' => 'index' ]);

    Route::get("/activity", 'AdminActivityLogController@index')->name('activity.index');
    Route::get("/activity/{date}", 'AdminActivityLogController@show')->name('activity.show');

    Route::get("/contact", 'AdminContactController@index')->name('contact.index');

    /*
    *   if you want to Add new methods to a resource controller in Laravel
    *   Just add a route to that method separately, before you register the resource:
    */
    // leader
    Route::get('selling-teams/assign-leader/{id}', 'AdminSellingTeamsController@createLeader')->name('selling-teams.assign-leader.create');
    Route::post('selling-teams/assign-leader/{id}', 'AdminSellingTeamsController@storeLeader')->name('selling-teams.assign-leader.store');
    Route::post('selling-teams/change-leader/{id}', 'AdminSellingTeamsController@changeLeader')->name('selling-teams.change-leader');
    
    // member
    Route::get('selling-teams/add-member/{id}', 'AdminSellingTeamsController@createMember')->name('selling-teams.add-member.create');
    Route::post('selling-teams/assign-member/{id}', 'AdminSellingTeamsController@storeMember')->name('selling-teams.add-member.store');
    Route::delete('selling-teams/destroy-member/{id}', 'AdminSellingTeamsController@destroyMember')->name('selling-teams.destroy-membere');

    //city 
    Route::delete('selling-teams/destroy-city/{selling_team_id}/{city_id}', 'AdminSellingTeamsController@destroyCity')->name('selling-teams.destroy-city');
    Route::post('selling-teams/city/{selling_team_id}', 'AdminSellingTeamsController@storeCity')->name('selling-teams.storecity');

    Route::resource('selling-teams', 'AdminSellingTeamsController');


    Route::get("/test", 'GeneralController@test');
});

/*
|--------------------------------------------------------------------------
| DIETITIAN Route
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'dietitian',  'middleware' => 'dietitian', 'as' => 'dietitian.'], function()
{
    //All the routes that belongs to the group goes here
    Route::get('/','DietitianController@index')->name('index');
    Route::get('/my-order/{tracking}','DietitianController@myOrder')->name('my-order');
    Route::group(['middleware' => 'team-leader'], function () {
        Route::get('/team','DietitianController@team')->name('team');
        Route::get('/team/{refcode}','DietitianController@member')->name('team.member');
        Route::get('/all-orders','DietitianController@AllOrders')->name('team.allorders');
        Route::get('/order/{tracking}','DietitianController@showOrder')->name('team.show.order');
        Route::get('/all-kits','DietitianController@allKits')->name('team.allkits');
        Route::get('/kit/{kitCode}','DietitianController@showKit')->name('team.show.kit');
        Route::get('/kit/generate/{kitCode}','DietitianController@generateKit')->name('team.generate.kit');
    });
    Route::get('/my-kit/{kitCode}','DietitianController@myKit')->name('my-kit');
    Route::get('/my-kit/generate/{kitCode}','DietitianController@generateMyKit')->name('generate.my-kit');
});

/*
|--------------------------------------------------------------------------
| USER Route
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'user',  'middleware' => ['auth', 'verified'], 'as' => 'user.'], function()
{
    //All the routes that belongs to the group goes here
    Route::get('/','UserController@index')->name('index');
    // Create View

    Route::get('/{kitCode}-{kitId}', 'UserController@surveyView')->name('survey.create');
    // Store
    Route::post('/{kitCode}-{kitId}', 'UserController@storeSurvey')->name('survey.store');

    Route::get('/kit/{id}', 'UserController@showAnswer')->name('answer.show');

});



Route::redirect('/colyak-hastaligi', 'gida-hassasiyeti', 301);
Route::redirect('/shop/cift-paketi', '/checkout', 301);
Route::redirect('/bilim', '/', 301);
Route::redirect('/cart', '/', 301);
Route::redirect('/enbiosis-test', '/', 301);
Route::redirect('/index.php', '/', 301);
Route::redirect('/index.php/bilim', '/', 301);
Route::redirect('/index.php/login', '/', 301);
Route::redirect('/index.php/mikrobiyom', '/mikrobiyom', 301);
Route::redirect('/index.php/password/reset', '/', 301);
Route::redirect('/index.php/register-kit', '/', 301);
Route::redirect('/index.php/urunlerimiz', '/urunlerimiz', 301);
Route::redirect('/login', '/', 301);
Route::redirect('/mikrobiyom-analizi', '/urunlerimiz', 301);
Route::redirect('/password/reset', '/', 301);
Route::redirect('/shop/cift-paket', '/', 301);
Route::redirect('/shop/ucludiyet-paket', '/', 301);
Route::redirect('/blog/content-blocks/separators/', '/blog', 301);
Route::redirect('/blog/content-blocks/styled-lists/', '/blog', 301);
Route::redirect('/blog/content-blocks/tabs-pills/', '/blog', 301);
Route::redirect('/blog/content-blocks/instagram-feed/', '/blog', 301);
Route::redirect('/blog/content-blocks/social-links/', '/blog', 301);
Route::redirect('/blog/ornek-sayfa/', '/blog', 301);
Route::redirect('/blog/content-blocks/numbered-headings/', '/blog', 301);
Route::redirect('/blog/galleries/', '/blog', 301);
Route::redirect('/blog/content-blocks/', '/blog', 301);
Route::redirect('/blog/content-blocks/accordions/', '/blog', 301);
Route::redirect('/blog/content-blocks/drop-caps/', '/blog', 301);
Route::redirect('/blog/sample-page/', '/blog', 301);
Route::redirect('/blog/content-blocks/facebook-fanpage/', '/blog', 301);
Route::redirect('/blog/content-blocks/share-buttons/', '/blog', 301);
Route::redirect('/blog/content-blocks/pinterest-board/', '/blog', 301);
Route::redirect('/blog/content-blocks/subscription-forms/', '/blog', 301);
Route::redirect('/blog/contact-form/', '/blog', 301);
Route::redirect('/blog/content-blocks/author/', '/blog', 301);
Route::redirect('/blog/coming-soon/', '/blog', 301);
Route::redirect('/blog/content-blocks/badges/', '/blog', 301);
Route::redirect('/blog/content-blocks/styled-blocks/', '/blog', 301);
Route::redirect('/blog/content-blocks/twitter-feed/', '/blog', 301);
Route::redirect('/blog/content-blocks/alerts/', '/blog', 301);
Route::redirect('/blog/content-blocks/progress-bars/', '/blog', 301);
Route::redirect('/blog/inline-posts/', '/blog', 301);
Route::redirect('/blog/category-blocks/', '/blog', 301);
Route::redirect('/blog/promo-blocks/', '/blog', 301);
Route::redirect('/index.php', '/', 301);
Route::redirect('/index.php/register-kit', '/', 301);
Route::redirect('/register-kit', '/', 301);
