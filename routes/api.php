<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', 'API\ApiGeneralController@getApi');
Route::get('/test', 'API\ApiGeneralController@testApi');
Route::get('/testKit', 'API\ApiGeneralController@testKit');
Route::get('/testKit/{kitCode}', 'API\ApiGeneralController@testKit_aa');

Route::group(['as' => 'api.user.'], function()
{

    Route::post('/login', 'API\user\AuthController@login')->name('login');
    Route::post('/registerKit', 'API\user\UserRegisterKitController@RegisterKit')->name('registerKit');
    Route::post('/registerUser', 'API\user\UserRegisterKitController@RegisterUser')->name('registerUser');
    Route::post('/validatregisterUser', 'API\user\UserRegisterKitController@validatregisterUser')->name('validatregisterUser');
    Route::post('/loginkituser', 'API\user\UserRegisterKitController@loginKitUser')->name('loginKitUser');
    // check for email if exist
    Route::post('/password/reset', 'API\user\PasswordResetController@passwordreset')->name('password.reset');
    Route::post('/password/checkAndReset', 'API\user\PasswordResetController@checkandReset')->name('password.checkandReset');
});

Route::group(['prefix' => 'user',  'middleware' => ['auth:api'], 'as' => 'api.user.'], function()
{
    //Get Auth user
    Route::get('/', 'API\user\UserController@index')->name('index');
    // send Email Verification
    Route::post('/email', 'API\user\UserController@sendEmailVerification')->name('email.Verification');
    // check for Verification code
    Route::post('/email/verify', 'API\user\UserController@checkForVerificationCode')->name('email.checkForVerificationCode');
    // Get Survey for kit
    Route::get('/survey/{kitCode}', 'API\user\UserController@getSurvey')->name('survey.kit');
    Route::post('/survey/answers', 'API\user\UserController@storeSurvey')->name('survey.kit.answer');
    
    Route::group(['middleware' => 'kit-show'], function () {
        // Get foods for kit
        Route::get('/kit/food', 'API\user\UserController@getKitFoods')->name('kit.foods');
        // Get Close Profle for kit
        Route::get('/kit/close-profle', 'API\user\UserController@getKitCloseProfle')->name('kit.closeProfle');
        // Get Age Range for kit
        Route::get('/kit/Age-Range', 'API\user\UserController@getKitAgeRange')->name('kit.AgeRange');
        // Get Bacteria for kit
        Route::get('/kit/bacteria', 'API\user\UserController@getKitBacteria')->name('kit.bacteria');
        // Get AllScores for kit
        Route::get('/kit/all-scores/{pdf?}', 'API\user\UserController@getKitAllScores')->name('kit.AllScores');
        // Get Taksonomik for kit
        Route::get('/kit/taksonomik/{type?}', 'API\user\UserController@getKitTaksonomik')->name('kit.taksonomik');
        // Get suggestion for kit
        Route::get('/kit/suggestion', 'API\user\UserController@getsuggestion')->name('kit.suggestion');
        // send kit report to email
        Route::post('/kit/report', 'API\user\UserController@sendReport')->name('api.sendReport');
    });
    // Get resources for user panel
    Route::get('/resources', 'API\user\UserController@getresources')->name('api.getresources');
});


Route::group([ 'as' => 'api.dietitian.', 'prefix' => 'dietitian' ], function()
{
    Route::post('/login', 'API\dietitian\AuthController@login')->name('login');
    // password reset
    Route::post('/password/reset', 'API\dietitian\PasswordResetController@passwordreset')->name('password.reset');
    Route::post('/password/checkAndReset', 'API\dietitian\PasswordResetController@checkandReset')->name('password.checkandReset');
    
    Route::group(['middleware' => ['api', 'dietitian-oauth'] ], function () {

        // Send Verification code
        Route::post('/email', 'API\dietitian\EmailVerificationController@sendEmailVerification')->name('email.Verification');
        // check for Verification code
        Route::post('/email/verify', 'API\dietitian\EmailVerificationController@checkForVerificationCode')->name('email.checkForVerificationCode');

        //Get Auth dietitian
        Route::get('/', 'API\dietitian\DietitianController@index')->name('index');

        //Get dietitian info like (clients, kits, ...)
        Route::get('/info', 'API\dietitian\DietitianController@info')->name('info');

        //search
        Route::get('/search', 'API\dietitian\DietitianController@search')->name('search');
        // getSurvey
        Route::get('/survey/{kitCode}', 'API\dietitian\DietitianController@getSurvey')->name('getSurvey');

        Route::group(['prefix' => 'kit', 'as' => 'kit.'], function () {
            // Get foods for kit
            Route::get('/food', 'API\dietitian\DietitianKitController@getKitFoods')->name('foods');
            // Get Close Profle for kit
            Route::get('/close-profle', 'API\dietitian\DietitianKitController@getKitCloseProfle')->name('closeProfle');
            // Get Age Range for kit
            Route::get('/age-range', 'API\dietitian\DietitianKitController@getKitAgeRange')->name('ageRange');
            // Get Bacteria for kit
            Route::get('/bacteria', 'API\dietitian\DietitianKitController@getKitBacteria')->name('bacteria');
            // Get AllScores for kit
            Route::get('/all-scores/{pdf?}', 'API\dietitian\DietitianKitController@getKitAllScores')->name('AllScores');
            // Get Taksonomik for kit
            Route::get('/taksonomik/{type?}', 'API\dietitian\DietitianKitController@getKitTaksonomik')->name('taksonomik');
        });
    });
});