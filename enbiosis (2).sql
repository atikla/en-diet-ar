-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 30, 2020 at 03:05 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `enbiosis`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

CREATE TABLE `activity_log` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `log_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_id` bigint(20) UNSIGNED DEFAULT NULL,
  `subject_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `causer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `causer_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `properties` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`properties`)),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `activity_log`
--

INSERT INTO `activity_log` (`id`, `log_name`, `description`, `subject_id`, `subject_type`, `causer_id`, `causer_type`, `properties`, `created_at`, `updated_at`) VALUES
(1, 'apiUser', 'user with email: ot.themechanic95@gmail.com login unsuccessful', NULL, NULL, NULL, NULL, '{\"ip\":\"::1\"}', '2020-04-27 12:04:08', '2020-04-27 12:04:08'),
(2, 'apiUser', 'user with email: ot.themechanic95@gmail.com login unsuccessful', NULL, NULL, NULL, NULL, '{\"ip\":\"::1\"}', '2020-04-27 12:04:21', '2020-04-27 12:04:21'),
(3, 'apiUser', 'user with email: ot.themechanic95@gmail.com login unsuccessful', NULL, NULL, NULL, NULL, '{\"ip\":\"::1\"}', '2020-04-27 12:04:27', '2020-04-27 12:04:27'),
(4, 'apiUser', 'user with email: ot.themechanic95@gmail.com login unsuccessful', NULL, NULL, NULL, NULL, '{\"ip\":\"::1\"}', '2020-04-27 12:04:31', '2020-04-27 12:04:31'),
(5, 'apiUser', 'user with email: ot.themechanic95@gmail.com login successful', NULL, NULL, 118, 'App\\User', '{\"ip\":\"::1\"}', '2020-04-27 12:04:38', '2020-04-27 12:04:38'),
(6, 'apiUser', 'Get result - age range - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-04-27 12:04:39', '2020-04-27 12:04:39'),
(7, 'apiUser', 'Get result - all scores - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-04-27 12:04:39', '2020-04-27 12:04:39'),
(8, 'apiUser', 'Get result - close profle - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-04-27 12:04:39', '2020-04-27 12:04:39'),
(9, 'apiUser', 'Get result - food - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-04-27 12:04:39', '2020-04-27 12:04:39'),
(10, 'apiUser', 'Get result - taksonomik - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-04-27 12:04:57', '2020-04-27 12:04:57'),
(11, 'apiUser', 'Send reports for kit code: AA0056', NULL, 'App\\User', 118, 'App\\User', '{\"ip\":\"127.0.0.1\",\"attributes\":null,\"old\":null}', '2020-04-27 12:05:39', '2020-04-27 12:05:39'),
(12, 'adminlogin', 'Admin with email: latik@enbiosis.com login successful', NULL, NULL, NULL, NULL, '{\"ip\":\"::1\"}', '2020-04-28 10:35:59', '2020-04-28 10:35:59'),
(13, 'adminlogin', 'Admin with email: latik@enbiosis.com login successful', NULL, NULL, NULL, NULL, '{\"ip\":\"::1\"}', '2020-04-28 10:36:08', '2020-04-28 10:36:08'),
(14, 'apiUser', 'user with email: ot.themechanic95@gmail.com login successful', NULL, NULL, 118, 'App\\User', '{\"ip\":\"::1\"}', '2020-05-06 19:22:13', '2020-05-06 19:22:13'),
(15, 'apiUser', 'Get result - age range - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-06 19:22:14', '2020-05-06 19:22:14'),
(16, 'apiUser', 'Get result - all scores - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-06 19:22:14', '2020-05-06 19:22:14'),
(17, 'apiUser', 'Get result - close profle - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-06 19:22:14', '2020-05-06 19:22:14'),
(18, 'apiUser', 'Get result - food - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-06 19:22:15', '2020-05-06 19:22:15'),
(19, 'apiUser', 'Get result - taksonomik - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-06 19:22:27', '2020-05-06 19:22:27'),
(20, 'apiUser', 'user with email: ot.themechanic95@gmail.com login successful', NULL, NULL, 118, 'App\\User', '{\"ip\":\"::1\"}', '2020-05-07 19:25:07', '2020-05-07 19:25:07'),
(21, 'apiUser', 'Get result - age range - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-07 19:25:08', '2020-05-07 19:25:08'),
(22, 'apiUser', 'Get result - close profle - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-07 19:25:08', '2020-05-07 19:25:08'),
(23, 'apiUser', 'Get result - all scores - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-07 19:25:08', '2020-05-07 19:25:08'),
(24, 'apiUser', 'Get result - food - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-07 19:25:09', '2020-05-07 19:25:09'),
(25, 'apiUser', 'Get result - taksonomik - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-07 19:25:15', '2020-05-07 19:25:15'),
(26, 'apiUser', 'user with email: ot.themechanic95@gmail.com login successful', NULL, NULL, 118, 'App\\User', '{\"ip\":\"::1\"}', '2020-05-13 13:27:24', '2020-05-13 13:27:24'),
(27, 'apiUser', 'Get result - age range - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 13:27:24', '2020-05-13 13:27:24'),
(28, 'apiUser', 'Get result - close profle - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 13:27:25', '2020-05-13 13:27:25'),
(29, 'apiUser', 'Get result - food - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 13:27:25', '2020-05-13 13:27:25'),
(30, 'apiUser', 'Get result - all scores - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 13:27:52', '2020-05-13 13:27:52'),
(31, 'apiUser', 'Get result - age range - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 13:27:52', '2020-05-13 13:27:52'),
(32, 'apiUser', 'Get result - close profle - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 13:27:52', '2020-05-13 13:27:52'),
(33, 'apiUser', 'Get result - food - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 13:27:52', '2020-05-13 13:27:52'),
(34, 'apiUser', 'Get result - taksonomik - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 13:27:57', '2020-05-13 13:27:57'),
(35, 'apiUser', 'Get result - taksonomik - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 13:30:45', '2020-05-13 13:30:45'),
(36, 'apiUser', 'Get result - age range - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 13:30:45', '2020-05-13 13:30:45'),
(37, 'apiUser', 'Get result - all scores - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 13:30:45', '2020-05-13 13:30:45'),
(38, 'apiUser', 'Get result - close profle - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 13:30:45', '2020-05-13 13:30:45'),
(39, 'apiUser', 'Get result - food - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 13:30:45', '2020-05-13 13:30:45'),
(40, 'apiUser', 'Get result - close profle - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 13:32:04', '2020-05-13 13:32:04'),
(41, 'apiUser', 'Get result - age range - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 13:32:04', '2020-05-13 13:32:04'),
(42, 'apiUser', 'Get result - taksonomik - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 13:32:04', '2020-05-13 13:32:04'),
(43, 'apiUser', 'Get result - all scores - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 13:32:04', '2020-05-13 13:32:04'),
(44, 'apiUser', 'Get result - food - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 13:32:04', '2020-05-13 13:32:04'),
(45, 'apiUser', 'Get result - close profle - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 13:32:10', '2020-05-13 13:32:10'),
(46, 'apiUser', 'Get result - taksonomik - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 13:32:10', '2020-05-13 13:32:10'),
(47, 'apiUser', 'Get result - all scores - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 13:32:10', '2020-05-13 13:32:10'),
(48, 'apiUser', 'Get result - age range - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 13:32:10', '2020-05-13 13:32:10'),
(49, 'apiUser', 'Get result - food - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 13:32:10', '2020-05-13 13:32:10'),
(50, 'adminlogin', 'Admin with email: latik@enbiosis.com login successful', NULL, NULL, NULL, NULL, '{\"ip\":\"::1\"}', '2020-05-13 14:07:07', '2020-05-13 14:07:07'),
(51, 'superAdmin', 'go to admin Dashboard page', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-13 14:07:08', '2020-05-13 14:07:08'),
(52, 'superAdmin', 'go to kits index', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-13 14:07:57', '2020-05-13 14:07:57'),
(53, 'superAdmin', 'kit searched for AA0056', NULL, 'App\\Kit', 1, 'App\\Admin', '{\"ip\":\"::1\",\"attributes\":\"AA0056\",\"old\":null}', '2020-05-13 14:08:47', '2020-05-13 14:08:47'),
(54, 'superAdmin', 'upload food for kitcode: AA0056. ', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-13 14:09:26', '2020-05-13 14:09:26'),
(55, 'superAdmin', 'kit result uploaded', 582, 'App\\Kit', 1, 'App\\Admin', '{\"ip\":\"::1\",\"attributes\":{\"id\":582,\"kit_code\":\"AA0056\",\"microbiome\":{\"Mikrobiyom \\u00c7e\\u015fitlili\\u011finiz\":\"6\",\"\\u00d6nemli Organizmalar\\u0131n\\u0131z\":{\"ORGANIZMA\":[\"KULLANICI\",\"TOPLUM\"],\"Alistipes\":[\"0\",\"76\"],\"Anaerostipes\":[\"0\",\"74\"],\"Butyrivibrio\":[\"0\",\"90\"],\"Phascolarctobacterium\":[\"0\",\"70\"],\"Escherichia\":[\"0\",\"92\"],\"Bacteroides\":[\"3\",\"54\"],\"Blautia\":[\"7\",\"64\"],\"Lachnospiraceae\":[\"8\",\"56\"],\"Ruminococcus\":[\"9\",\"64\"],\"Parabacteroides\":[\"11\",\"65\"],\"Coprococcus\":[\"15\",\"62\"],\"Dorea\":[\"16\",\"70\"],\"Bifidobacterium\":[\"21\",\"77\"],\"Clostridium\":[\"22\",\"72\"],\"Roseburia\":[\"25\",\"70\"],\"Akkermansia\":[\"29\",\"80\"],\"Haemophilus\":[\"32\",\"83\"],\"Eubacterium\":[\"47\",\"79\"],\"Collinsella\":[\"51\",\"73\"],\"Anaerotruncus\":[\"55\",\"78\"],\"Ruminococcaceae\":[\"61\",\"53\"],\"Faecalibacterium\":[\"75\",\"61\"],\"Streptococcus\":[\"78\",\"81\"],\"Desulfovibrio\":[\"90\",\"74\"],\"Catenibacterium\":[\"91\",\"91\"],\"Prevotella\":[\"96\",\"81\"]},\"Taksonomik Analiz\":{\"Taksonomik Analiz (Cins Seviyesi)\":{\"TAX\":[\"KULLANICI\",\"TOPLUM\",\"SAGLIKLI\"],\"Prevotella\":[\"4.444819602546787\",\"0.1755526608462995\",\"0.18805477790579875\"],\"Succinivibrio\":[\"13.055903916650587\",\"0.06243439885384906\",\"0.0868518692885645\"],\"Ruminococcaceae\":[\"9.897742620104188\",\"8.59439235986533\",\"9.066843166957936\"],\"Faecalibacterium\":[\"9.35148562608528\",\"6.514846244369883\",\"6.706931790107228\"],\"Clostridiales\":[\"5.65792012348061\",\"4.955044777367546\",\"5.690149576974722\"],\"Di\\u011fer\":[\"22.847530387806287\",\"79.52217689785078\",\"78.07311404085995\"]},\"Taksonomik Analiz (Aile seviyesi)\":{\"TAX\":[\"KULLANICI\",\"TOPLUM\",\"SAGLIKLI\"],\"Prevotellaceae\":[\"34.74459772332626\",\"4.464190420269295\",\"5.339408930792739\"],\"Ruminococcaceae\":[\"20.984468454562993\",\"18.593607440392915\",\"19.173199559772755\"],\"Succinivibrionaceae\":[\"13.063139108624346\",\"0.06278135179239813\",\"0.08729209740417539\"],\"Clostridiales\":[\"5.65792012348061\",\"4.955044777367546\",\"5.690149576974722\"],\"Paraprevotellaceae\":[\"4.651022573798958\",\"0.7241708123085702\",\"0.8064155859179338\"],\"Di\\u011fer\":[\"20.898852016206845\",\"71.20020519786927\",\"68.90353424913768\"]},\"Taksonomik Analiz (\\u015eube Seviyesi)\":{\"TAX\":[\"KULLANICI\",\"TOPLUM\",\"SAGLIKLI\"],\"Bacteroidetes\":[\"40.990980127339384\",\"37.866835633649075\",\"37.89050589756257\"],\"Firmicutes\":[\"40.33740111904302\",\"46.73062860310761\",\"47.58803695446815\"],\"Di\\u011fer\":[\"18.671618753617594\",\"15.402535763243321\",\"14.521457147969272\"]}},\"Yak\\u0131n Profiller\":{\"PROFIL\":[\"YUZDE\"],\"fazla kilolu\":[\"20\"],\"cilt rahats\\u0131zl\\u0131klar\\u0131na sahip\":[\"19\"],\"laktoz hassasiyeti problemi var\":[\"14\"],\"akci\\u011fer rahats\\u0131zl\\u0131\\u011f\\u0131na sahip\":[\"10\"],\"migren rahats\\u0131zl\\u0131\\u011f\\u0131 ya\\u015f\\u0131yor\":[\"10\"],\"vejetaryen, ancak deniz \\u00fcr\\u00fcnleri t\\u00fcketiyor\":[\"10\"],\"gluten hassasiyeti veya alerjisine sahip\":[\"8\"],\"zaman zaman ishal problemi ya\\u015f\\u0131yor\":[\"8\"],\"dikkat bozuklu\\u011fu rahats\\u0131zl\\u0131\\u011f\\u0131 ya\\u015f\\u0131yor\":[\"7\"],\"g\\u0131da alerjisi bulunuyor\":[\"6\"],\"k\\u0131rm\\u0131z\\u0131 et yemiyor\":[\"6\"],\"hayat\\u0131n\\u0131n bir d\\u00f6neminde kanser te\\u015fhisi alm\\u0131\\u015f\":[\"6\"],\"vejetaryen\":[\"5\"],\"huzursuz ba\\u011f\\u0131rsak sendromu ya\\u015f\\u0131yor\":[\"5\"],\"karaci\\u011fer rahats\\u0131zl\\u0131\\u011f\\u0131na sahip\":[\"5\"],\"tiroid rahats\\u0131zl\\u0131\\u011f\\u0131na sahip\":[\"4\"],\"kalp rahats\\u0131zl\\u0131\\u011f\\u0131 ya\\u015f\\u0131yor\":[\"3\"]},\"Ba\\u011f\\u0131rsak Skorlar\\u0131\":{\"V\\u00fccut K\\u00fctle Endeksi-Mikrobiyom \\u0130li\\u015fkiniz\":[\"19\",\"15\",\"72\",\"35\"],\"Karbonhidrat Metabolizman\\u0131z\":[\"46\",\"\",\"\",\"48\"],\"Protein Metabolizman\\u0131z\":[\"1\",\"\",\"\",\"29\"],\"Ya\\u011f Metabolizman\\u0131z\":[\"2\",\"\",\"\",\"29\"],\"Probiyotik Mikroorganizma \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"39\",\"17\",\"78\",\"43\"],\"Uyku Kaliteniz\":[\"77\",\"23\",\"84\",\"74\"],\"Ba\\u011f\\u0131rsak Hareketlilik \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"19\",\"29\",\"69\",\"35\"],\"Gluten Hassasiyeti \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"19\",\"16\",\"64\",\"24\"],\"Laktoz Hassasiyeti \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"12\",\"15\",\"82\",\"27\"],\"Vitamin Senteziniz\":[\"83\",\"56\",\"73\",\"67\"],\"Mikrobiyom Ya\\u015f\\u0131n\\u0131z\":[\"27\",\"\",\"\",\"\"],\"Yapay Tatland\\u0131r\\u0131c\\u0131 Hasar\\u0131n\\u0131z\":[\"58\",\"27\",\"58\",\"32\"],\"\\u015eeker T\\u00fcketim Skorunuz\":[\"49\",\"7\",\"81\",\"59\"],\"Antibiyotik Hasar \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"14\",\"27\",\"74\",\"34\"],\"Otoimm\\u00fcnite Endeksiniz\":[\"9\",\"23\",\"62\",\"29\"]}},\"food\":{\"Ada\\u00e7ay\\u0131\":7,\"Ahududu\":9,\"Alabal\\u0131k\":4,\"Amaranth\":7,\"Ananas\":8,\"Antep F\\u0131st\\u0131\\u011f\\u0131\":4,\"Armut\":9,\"Armut Suyu\":9,\"Arpa\":8,\"Asma Yapra\\u011f\\u0131\":8,\"Aspir Ya\\u011f\\u0131\":7,\"Avokado\":7,\"Ay\\u00e7ekirde\\u011fi\":6,\"Ay\\u00e7i\\u00e7ek Ya\\u011f\\u0131\":6,\"\\u0130nek S\\u00fct\\u00fc (Az Ya\\u011fl\\u0131)\":5,\"A\\u00e7ai \\u00dcz\\u00fcm\\u00fc\":9,\"Badem \":3,\"Bakla\":8,\"Balkaba\\u011f\\u0131\":7,\"Bal\\u0131k Ya\\u011f\\u0131 Destekleri\":8,\"Barbunya\":7,\"Beyaz Dut\":10,\"Beyaz Ekmek\":8,\"Beyaz Peynir (Tam Ya\\u011fl\\u0131)\":4,\"Beyaz Un\":9,\"Bezelye\":6,\"Bira\":8,\"Brokoli\":7,\"Br\\u00fcksel Lahanas\\u0131\":8,\"Bulgur\":7,\"Bu\\u011fday\":7,\"Bu\\u011fday Ru\\u015feymi\":6,\"Bu\\u011fday Unu\":7,\"B\\u00f6r\\u00fclce\":8,\"B\\u00f6\\u011f\\u00fcrtlen\":8,\"Ceviz\":3,\"Cheddar\":5,\"Chia Tohumu\":7,\"Dana Eti\":4,\"Domates\":7,\"Dut Kurusu\":10,\"Ebeg\\u00fcmeci\":8,\"Elma\":9,\"Elma Suyu\":9,\"Enginar\":8,\"Erik\":8,\"Eski Ka\\u015far\":5,\"Esmer Pirin\\u00e7\":7,\"Ezine Peyniri\":5,\"Frambuaz Suyu\":9,\"F\\u0131nd\\u0131k\":4,\"F\\u0131nd\\u0131k Ya\\u011f\\u0131\":7,\"Grayver\":5,\"Greyfurt \":7,\"Greyfurt Suyu\":7,\"Hamsi\":5,\"Havu\\u00e7\":8,\"Havu\\u00e7 Suyu\":9,\"Hindi Eti\":6,\"Hindiba\":7,\"Hindistan Cevizi\":7,\"Taze Hurma\":8,\"Kabak \\u00c7ekirde\\u011fi\":7,\"Kahve\":8,\"Kaju\":7,\"Kakao\":7,\"Kale\":8,\"Kanola Ya\\u011f\\u0131\":6,\"Kapari\":8,\"Karabu\\u011fday \":6,\"Karadut\":10,\"Karalahana\":7,\"Karides\":7,\"Karnabahar\":8,\"Kavun\":8,\"Kaymak\":8,\"Taze Kay\\u0131s\\u0131\":9,\"Kay\\u0131s\\u0131 Suyu\":9,\"Ka\\u015far Peyniri\":6,\"Kefir \":7,\"Kepekli Ekmek\":7,\"Kepekli Makarna\":7,\"Kereviz\":8,\"Kestane\":8,\"Keten Tohumu\":8,\"Ke\\u00e7i Eti\":7,\"Ke\\u00e7i Peyniri\":5,\"Ke\\u00e7i S\\u00fct\\u00fc\":4,\"Kinoa\":6,\"Kivi \":9,\"Koyun Eti \":8,\"Koyun Peyniri\":6,\"Koyun S\\u00fct\\u00fc\":4,\"Krem Peynir\":6,\"Kril Ya\\u011f\\u0131 Destekleri\":7,\"Kuru Fasulye\":6,\"Kuru Hurma\":8,\"Kuru Kay\\u0131s\\u0131\":7,\"Kuru \\u00dcz\\u00fcm\":10,\"Kuyruk Ya\\u011f\\u0131\":6,\"Kuzu Eti \":9,\"Ku\\u015fkonmaz\":8,\"K\\u0131l\\u0131\\u00e7 Bal\\u0131\\u011f\\u0131\":5,\"K\\u0131rm\\u0131z\\u0131-Mor \\u00dcz\\u00fcm\":9,\"K\\u0131rm\\u0131z\\u0131 Biber\":8,\"Frenk \\u00dcz\\u00fcm\\u00fc\":10,\"K\\u0131rm\\u0131z\\u0131 Pancar\":8,\"K\\u0131rm\\u0131z\\u0131 \\u015earap\":6,\"K\\u0131z\\u0131lc\\u0131k\":8,\"Beyaz Lahana\":8,\"Lava\\u015f\":8,\"Levrek\":5,\"Limon\":7,\"Lor Peyniri\":4,\"L\\u00fcfer\":5,\"Maden Suyu\":8,\"Makarna\":7,\"Manda S\\u00fct\\u00fc\":5,\"Mandalina \":7,\"Mandalina Suyu\":8,\"Mango\":8,\"Mantar\":8,\"Margarin\":7,\"Maydanoz\":8,\"Mercimek\":6,\"Midye \":7,\"Mor Lahana\":8,\"Morina Bal\\u0131\\u011f\\u0131\":5,\"Muz\":7,\"M\\u0131s\\u0131r\":8,\"M\\u0131s\\u0131r Gevre\\u011fi\":8,\"M\\u0131s\\u0131r Ke\\u011fe\\u011fi\":8,\"M\\u0131s\\u0131r Ni\\u015fastas\\u0131\":9,\"M\\u0131s\\u0131r Unu\":8,\"M\\u0131s\\u0131r Ya\\u011f\\u0131\":7,\"M\\u0131s\\u0131r\\u00f6z\\u00fc Ya\\u011f\\u0131\":7,\"Taze Nane\":7,\"Nar Suyu\":9,\"Nar\":9,\"Nektarin\":8,\"Nohut\":7,\"Noodle\":8,\"Oolong \\u00c7ay\\u0131\":8,\"Otlu Peynir\":6,\"Palamut\":5,\"Papaya\":8,\"\\u0130nek S\\u00fct\\u00fc (Tam Ya\\u011fl\\u0131)\":4,\"Patates\":8,\"Patl\\u0131can\":8,\"Pembe \\u015earap\":7,\"Pirin\\u00e7\":8,\"Pirin\\u00e7 Unu\":7,\"Portakal\":7,\"Portakal Suyu\":7,\"Protein Barlar\":7,\"Ricotta\":5,\"Roka\":8,\"Rokfor\":5,\"Salep\":8,\"Sardalya\":5,\"Sar\\u0131msak\":8,\"Semizotu \":8,\"Siyah Havu\\u00e7\":8,\"Siyah \\u00c7ay\":7,\"Somon\":2,\"Soya Fasulyesi\":5,\"Soya Filizi\":7,\"Soya Unu\":6,\"Soya Ya\\u011f\\u0131\":6,\"So\\u011fan\":8,\"Tere\":8,\"Susam\":7,\"S\\u00fctl\\u00fc kremalar\":8,\"S\\u0131\\u011f\\u0131r Eti\":7,\"Tam Bu\\u011fday Unu\":7,\"Tam Tah\\u0131ll\\u0131 Ekmek\":10,\"Tam Tah\\u0131ll\\u0131 Gevrekler\":8,\"Tam Tah\\u0131l Unu\":8,\"Tatl\\u0131 Patates\":8,\"Tavuk Eti\":6,\"Tereya\\u011f\\u0131\":6,\"Tofu\":6,\"Ton Bal\\u0131\\u011f\\u0131\":5,\"Tulum Peyniri\":5,\"Tuna Bal\\u0131\\u011f\\u0131\":5,\"Turp\":9,\"Uskumru\":4,\"Vi\\u015fne \":9,\"Vi\\u015fne Suyu\":9,\"Whey Protein Destekleri\":6,\"Yaban Mersini\":8,\"Yo\\u011furt (Az Ya\\u011fl\\u0131)\":8,\"Yenge\\u00e7\":7,\"Yer Elmas\\u0131\":8,\"Yer F\\u0131st\\u0131\\u011f\\u0131\":6,\"Taze Fasulye\":8,\"Ye\\u015fil \\u00c7ay\":7,\"Yo\\u011furt (Tam Ya\\u011fl\\u0131)\":6,\"Yulaf\":9,\"Yulaf Kepe\\u011fi\":8,\"Yulafl\\u0131 Ekmek\":9,\"Yumurta\":5,\"Zeytin\":9,\"Zeytin Ezmesi\":8,\"Zeytinya\\u011f\\u0131\":6,\"\\u00c7am F\\u0131st\\u0131\\u011f\\u0131\":7,\"\\u00c7avdar\":6,\"\\u00c7avdar Ekme\\u011fi\":7,\"\\u00c7avdar Unu\":7,\"\\u00c7ilek\":8,\"\\u00c7ipura\":5,\"\\u00c7\\u00f6kelek\":7,\"\\u00dcz\\u00fcm Suyu \":9,\"\\u015ealgam\":8,\"\\u015eeftali\":9},\"user_id\":118,\"order_detail_id\":null,\"survey_id\":16,\"registered\":1,\"registered_at\":\"2020-04-09 12:25:39\",\"delevered_to_user_at\":\"2020-03-30 18:09:13\",\"received_from_user_at\":null,\"send_to_lab_at\":null,\"uploaded_at\":\"2020-05-13 17:09:26\",\"survey_filled_at\":\"2020-05-06 22:25:20\",\"kit_status\":4,\"sell_status\":2,\"created_at\":\"2020-03-22 15:54:00\",\"updated_at\":\"2020-05-13 17:09:26\",\"deleted_at\":null},\"old\":null}', '2020-05-13 14:09:26', '2020-05-13 14:09:26'),
(56, 'superAdmin', 'go to kits index', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-13 14:09:30', '2020-05-13 14:09:30'),
(57, 'superAdmin', 'upload Microbiome for kitcode: AA0056. ', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-13 14:09:44', '2020-05-13 14:09:44'),
(58, 'superAdmin', 'kit result uploaded', 582, 'App\\Kit', 1, 'App\\Admin', '{\"ip\":\"::1\",\"attributes\":{\"id\":582,\"kit_code\":\"AA0056\",\"microbiome\":{\"Mikrobiyom \\u00c7e\\u015fitlili\\u011finiz\":\"6\",\"\\u00d6nemli Organizmalar\\u0131n\\u0131z\":{\"ORGANIZMA\":[\"KULLANICI\",\"TOPLUM\"],\"Alistipes\":[\"0\",\"76\"],\"Butyrivibrio\":[\"0\",\"90\"],\"Catenibacterium\":[\"0\",\"91\"],\"Escherichia\":[\"0\",\"92\"],\"Eubacterium\":[\"29\",\"79\"],\"Phascolarctobacterium\":[\"31\",\"70\"],\"Desulfovibrio\":[\"34\",\"74\"],\"Streptococcus\":[\"38\",\"81\"],\"Dorea\":[\"38\",\"70\"],\"Akkermansia\":[\"41\",\"80\"],\"Haemophilus\":[\"45\",\"83\"],\"Roseburia\":[\"45\",\"70\"],\"Anaerostipes\":[\"47\",\"74\"],\"Clostridium\":[\"49\",\"72\"],\"Prevotella\":[\"53\",\"81\"],\"Blautia\":[\"56\",\"64\"],\"Ruminococcus\":[\"56\",\"64\"],\"Anaerotruncus\":[\"61\",\"78\"],\"Bifidobacterium\":[\"64\",\"77\"],\"Faecalibacterium\":[\"64\",\"61\"],\"Bacteroides\":[\"65\",\"54\"],\"Parabacteroides\":[\"65\",\"65\"],\"Coprococcus\":[\"67\",\"62\"],\"Lachnospiraceae\":[\"72\",\"56\"],\"Ruminococcaceae\":[\"78\",\"53\"],\"Collinsella\":[\"97\",\"73\"]},\"Taksonomik Analiz\":{\"Taksonomik Analiz (Cins Seviyesi)\":{\"TAX\":[\"KULLANICI\",\"TOPLUM\",\"SAGLIKLI\"],\"Bacteroides\":[\"32.28796328337891\",\"26.36581355734437\",\"25.35405933303747\"],\"Ruminococcaceae\":[\"15.946302654454078\",\"8.59439235986533\",\"9.066843166957936\"],\"Lachnospiraceae\":[\"10.905934532054253\",\"7.11711465936931\",\"7.260400822181206\"],\"Faecalibacterium\":[\"6.979108453952549\",\"6.514846244369883\",\"6.706931790107228\"],\"Clostridiales\":[\"4.256191954546849\",\"4.955044777367546\",\"5.690149576974722\"],\"Lachnospira\":[\"4.046325254227563\",\"1.0500766460544306\",\"0.9840398332972775\"],\"YS2\":[\"2.627792769856573\",\"0.13041948038096782\",\"0.1593879042265837\"],\"Di\\u011fer\":[\"22.950381097529217\",\"45.27229227524816\",\"44.778187573217586\"]},\"Taksonomik Analiz (Aile seviyesi)\":{\"TAX\":[\"KULLANICI\",\"TOPLUM\",\"SAGLIKLI\"],\"Bacteroidaceae\":[\"32.28796328337891\",\"26.36711369744144\",\"25.354459755148163\"],\"Ruminococcaceae\":[\"27.592418635950178\",\"18.593607440392915\",\"19.173199559772755\"],\"Lachnospiraceae\":[\"20.23935506529856\",\"15.562550243855489\",\"15.435685293116789\"],\"Di\\u011fer\":[\"19.88026301537235\",\"39.476728618310155\",\"40.03665539196229\"]},\"Taksonomik Analiz (\\u015eube Seviyesi)\":{\"TAX\":[\"KULLANICI\",\"TOPLUM\",\"SAGLIKLI\"],\"Firmicutes\":[\"53.026717877689435\",\"46.73062860310761\",\"47.58803695446815\"],\"Bacteroidetes\":[\"38.68800609296301\",\"37.866835633649075\",\"37.89050589756257\"],\"Di\\u011fer\":[\"8.285276029347557\",\"15.402535763243321\",\"14.521457147969272\"]}},\"Yak\\u0131n Profiller\":{\"PROFIL\":[\"YUZDE\"],\"fazla kilolu\":[\"27\"],\"cilt rahats\\u0131zl\\u0131klar\\u0131na sahip\":[\"24\"],\"gluten hassasiyeti veya alerjisine sahip\":[\"19\"],\"laktoz hassasiyeti problemi var\":[\"17\"],\"migren rahats\\u0131zl\\u0131\\u011f\\u0131 ya\\u015f\\u0131yor\":[\"16\"],\"huzursuz ba\\u011f\\u0131rsak sendromu ya\\u015f\\u0131yor\":[\"14\"],\"g\\u0131da alerjisi bulunuyor\":[\"12\"],\"k\\u0131rm\\u0131z\\u0131 et yemiyor\":[\"9\"],\"akci\\u011fer rahats\\u0131zl\\u0131\\u011f\\u0131na sahip\":[\"9\"],\"otoimm\\u00fcn bozuklu\\u011fa sahip\":[\"9\"],\"dikkat bozuklu\\u011fu rahats\\u0131zl\\u0131\\u011f\\u0131 ya\\u015f\\u0131yor\":[\"8\"],\"tiroid rahats\\u0131zl\\u0131\\u011f\\u0131na sahip\":[\"7\"],\"zaman zaman ishal problemi ya\\u015f\\u0131yor\":[\"7\"],\"kab\\u0131zl\\u0131k problemi ya\\u015f\\u0131yor\":[\"6\"],\"hayat\\u0131n\\u0131n bir d\\u00f6neminde kanser te\\u015fhisi alm\\u0131\\u015f\":[\"5\"],\"vejetaryen, ancak deniz \\u00fcr\\u00fcnleri t\\u00fcketiyor\":[\"5\"],\"kalp rahats\\u0131zl\\u0131\\u011f\\u0131 ya\\u015f\\u0131yor\":[\"4\"],\"mantar problemleri ya\\u015f\\u0131yor\":[\"3\"],\"iltihapl\\u0131 ba\\u011f\\u0131rsak hastal\\u0131\\u011f\\u0131 te\\u015fhisi alm\\u0131\\u015f\":[\"3\"],\"b\\u00f6brek rahats\\u0131zl\\u0131\\u011f\\u0131 ya\\u015f\\u0131yor\":[\"3\"]},\"Ba\\u011f\\u0131rsak Skorlar\\u0131\":{\"V\\u00fccut K\\u00fctle Endeksi-Mikrobiyom \\u0130li\\u015fkiniz\":[\"67\",\"15\",\"72\",\"35\"],\"Karbonhidrat Metabolizman\\u0131z\":[\"55\",\"\",\"\",\"48\"],\"Protein Metabolizman\\u0131z\":[\"34\",\"\",\"\",\"29\"],\"Ya\\u011f Metabolizman\\u0131z\":[\"34\",\"\",\"\",\"29\"],\"Probiyotik Mikroorganizma \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"28\",\"17\",\"78\",\"43\"],\"Uyku Kaliteniz\":[\"22\",\"23\",\"84\",\"74\"],\"Ba\\u011f\\u0131rsak Hareketlilik \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"38\",\"29\",\"69\",\"35\"],\"Gluten Hassasiyeti \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"51\",\"16\",\"64\",\"24\"],\"Laktoz Hassasiyeti \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"62\",\"15\",\"82\",\"27\"],\"Vitamin Senteziniz\":[\"75\",\"56\",\"73\",\"67\"],\"Mikrobiyom Ya\\u015f\\u0131n\\u0131z\":[\"55\",\"\",\"\",\"\"],\"Yapay Tatland\\u0131r\\u0131c\\u0131 Hasar\\u0131n\\u0131z\":[\"40\",\"27\",\"58\",\"32\"],\"\\u015eeker T\\u00fcketim Skorunuz\":[\"42\",\"7\",\"81\",\"59\"],\"Antibiyotik Hasar \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"48\",\"27\",\"74\",\"34\"],\"Otoimm\\u00fcnite Endeksiniz\":[\"50\",\"23\",\"62\",\"29\"]}},\"food\":{\"Ada\\u00e7ay\\u0131\":7,\"Ahududu\":9,\"Alabal\\u0131k\":4,\"Amaranth\":7,\"Ananas\":8,\"Antep F\\u0131st\\u0131\\u011f\\u0131\":4,\"Armut\":9,\"Armut Suyu\":9,\"Arpa\":8,\"Asma Yapra\\u011f\\u0131\":8,\"Aspir Ya\\u011f\\u0131\":7,\"Avokado\":7,\"Ay\\u00e7ekirde\\u011fi\":6,\"Ay\\u00e7i\\u00e7ek Ya\\u011f\\u0131\":6,\"\\u0130nek S\\u00fct\\u00fc (Az Ya\\u011fl\\u0131)\":5,\"A\\u00e7ai \\u00dcz\\u00fcm\\u00fc\":9,\"Badem \":3,\"Bakla\":8,\"Balkaba\\u011f\\u0131\":7,\"Bal\\u0131k Ya\\u011f\\u0131 Destekleri\":8,\"Barbunya\":7,\"Beyaz Dut\":10,\"Beyaz Ekmek\":8,\"Beyaz Peynir (Tam Ya\\u011fl\\u0131)\":4,\"Beyaz Un\":9,\"Bezelye\":6,\"Bira\":8,\"Brokoli\":7,\"Br\\u00fcksel Lahanas\\u0131\":8,\"Bulgur\":7,\"Bu\\u011fday\":7,\"Bu\\u011fday Ru\\u015feymi\":6,\"Bu\\u011fday Unu\":7,\"B\\u00f6r\\u00fclce\":8,\"B\\u00f6\\u011f\\u00fcrtlen\":8,\"Ceviz\":3,\"Cheddar\":5,\"Chia Tohumu\":7,\"Dana Eti\":4,\"Domates\":7,\"Dut Kurusu\":10,\"Ebeg\\u00fcmeci\":8,\"Elma\":9,\"Elma Suyu\":9,\"Enginar\":8,\"Erik\":8,\"Eski Ka\\u015far\":5,\"Esmer Pirin\\u00e7\":7,\"Ezine Peyniri\":5,\"Frambuaz Suyu\":9,\"F\\u0131nd\\u0131k\":4,\"F\\u0131nd\\u0131k Ya\\u011f\\u0131\":7,\"Grayver\":5,\"Greyfurt \":7,\"Greyfurt Suyu\":7,\"Hamsi\":5,\"Havu\\u00e7\":8,\"Havu\\u00e7 Suyu\":9,\"Hindi Eti\":6,\"Hindiba\":7,\"Hindistan Cevizi\":7,\"Taze Hurma\":8,\"Kabak \\u00c7ekirde\\u011fi\":7,\"Kahve\":8,\"Kaju\":7,\"Kakao\":7,\"Kale\":8,\"Kanola Ya\\u011f\\u0131\":6,\"Kapari\":8,\"Karabu\\u011fday \":6,\"Karadut\":10,\"Karalahana\":7,\"Karides\":7,\"Karnabahar\":8,\"Kavun\":8,\"Kaymak\":8,\"Taze Kay\\u0131s\\u0131\":9,\"Kay\\u0131s\\u0131 Suyu\":9,\"Ka\\u015far Peyniri\":6,\"Kefir \":7,\"Kepekli Ekmek\":7,\"Kepekli Makarna\":7,\"Kereviz\":8,\"Kestane\":8,\"Keten Tohumu\":8,\"Ke\\u00e7i Eti\":7,\"Ke\\u00e7i Peyniri\":5,\"Ke\\u00e7i S\\u00fct\\u00fc\":4,\"Kinoa\":6,\"Kivi \":9,\"Koyun Eti \":8,\"Koyun Peyniri\":6,\"Koyun S\\u00fct\\u00fc\":4,\"Krem Peynir\":6,\"Kril Ya\\u011f\\u0131 Destekleri\":7,\"Kuru Fasulye\":6,\"Kuru Hurma\":8,\"Kuru Kay\\u0131s\\u0131\":7,\"Kuru \\u00dcz\\u00fcm\":10,\"Kuyruk Ya\\u011f\\u0131\":6,\"Kuzu Eti \":9,\"Ku\\u015fkonmaz\":8,\"K\\u0131l\\u0131\\u00e7 Bal\\u0131\\u011f\\u0131\":5,\"K\\u0131rm\\u0131z\\u0131-Mor \\u00dcz\\u00fcm\":9,\"K\\u0131rm\\u0131z\\u0131 Biber\":8,\"Frenk \\u00dcz\\u00fcm\\u00fc\":10,\"K\\u0131rm\\u0131z\\u0131 Pancar\":8,\"K\\u0131rm\\u0131z\\u0131 \\u015earap\":6,\"K\\u0131z\\u0131lc\\u0131k\":8,\"Beyaz Lahana\":8,\"Lava\\u015f\":8,\"Levrek\":5,\"Limon\":7,\"Lor Peyniri\":4,\"L\\u00fcfer\":5,\"Maden Suyu\":8,\"Makarna\":7,\"Manda S\\u00fct\\u00fc\":5,\"Mandalina \":7,\"Mandalina Suyu\":8,\"Mango\":8,\"Mantar\":8,\"Margarin\":7,\"Maydanoz\":8,\"Mercimek\":6,\"Midye \":7,\"Mor Lahana\":8,\"Morina Bal\\u0131\\u011f\\u0131\":5,\"Muz\":7,\"M\\u0131s\\u0131r\":8,\"M\\u0131s\\u0131r Gevre\\u011fi\":8,\"M\\u0131s\\u0131r Ke\\u011fe\\u011fi\":8,\"M\\u0131s\\u0131r Ni\\u015fastas\\u0131\":9,\"M\\u0131s\\u0131r Unu\":8,\"M\\u0131s\\u0131r Ya\\u011f\\u0131\":7,\"M\\u0131s\\u0131r\\u00f6z\\u00fc Ya\\u011f\\u0131\":7,\"Taze Nane\":7,\"Nar Suyu\":9,\"Nar\":9,\"Nektarin\":8,\"Nohut\":7,\"Noodle\":8,\"Oolong \\u00c7ay\\u0131\":8,\"Otlu Peynir\":6,\"Palamut\":5,\"Papaya\":8,\"\\u0130nek S\\u00fct\\u00fc (Tam Ya\\u011fl\\u0131)\":4,\"Patates\":8,\"Patl\\u0131can\":8,\"Pembe \\u015earap\":7,\"Pirin\\u00e7\":8,\"Pirin\\u00e7 Unu\":7,\"Portakal\":7,\"Portakal Suyu\":7,\"Protein Barlar\":7,\"Ricotta\":5,\"Roka\":8,\"Rokfor\":5,\"Salep\":8,\"Sardalya\":5,\"Sar\\u0131msak\":8,\"Semizotu \":8,\"Siyah Havu\\u00e7\":8,\"Siyah \\u00c7ay\":7,\"Somon\":2,\"Soya Fasulyesi\":5,\"Soya Filizi\":7,\"Soya Unu\":6,\"Soya Ya\\u011f\\u0131\":6,\"So\\u011fan\":8,\"Tere\":8,\"Susam\":7,\"S\\u00fctl\\u00fc kremalar\":8,\"S\\u0131\\u011f\\u0131r Eti\":7,\"Tam Bu\\u011fday Unu\":7,\"Tam Tah\\u0131ll\\u0131 Ekmek\":10,\"Tam Tah\\u0131ll\\u0131 Gevrekler\":8,\"Tam Tah\\u0131l Unu\":8,\"Tatl\\u0131 Patates\":8,\"Tavuk Eti\":6,\"Tereya\\u011f\\u0131\":6,\"Tofu\":6,\"Ton Bal\\u0131\\u011f\\u0131\":5,\"Tulum Peyniri\":5,\"Tuna Bal\\u0131\\u011f\\u0131\":5,\"Turp\":9,\"Uskumru\":4,\"Vi\\u015fne \":9,\"Vi\\u015fne Suyu\":9,\"Whey Protein Destekleri\":6,\"Yaban Mersini\":8,\"Yo\\u011furt (Az Ya\\u011fl\\u0131)\":8,\"Yenge\\u00e7\":7,\"Yer Elmas\\u0131\":8,\"Yer F\\u0131st\\u0131\\u011f\\u0131\":6,\"Taze Fasulye\":8,\"Ye\\u015fil \\u00c7ay\":7,\"Yo\\u011furt (Tam Ya\\u011fl\\u0131)\":6,\"Yulaf\":9,\"Yulaf Kepe\\u011fi\":8,\"Yulafl\\u0131 Ekmek\":9,\"Yumurta\":5,\"Zeytin\":9,\"Zeytin Ezmesi\":8,\"Zeytinya\\u011f\\u0131\":6,\"\\u00c7am F\\u0131st\\u0131\\u011f\\u0131\":7,\"\\u00c7avdar\":6,\"\\u00c7avdar Ekme\\u011fi\":7,\"\\u00c7avdar Unu\":7,\"\\u00c7ilek\":8,\"\\u00c7ipura\":5,\"\\u00c7\\u00f6kelek\":7,\"\\u00dcz\\u00fcm Suyu \":9,\"\\u015ealgam\":8,\"\\u015eeftali\":9},\"user_id\":118,\"order_detail_id\":null,\"survey_id\":16,\"registered\":1,\"registered_at\":\"2020-04-09 12:25:39\",\"delevered_to_user_at\":\"2020-03-30 18:09:13\",\"received_from_user_at\":null,\"send_to_lab_at\":null,\"uploaded_at\":\"2020-05-13 17:09:44\",\"survey_filled_at\":\"2020-05-06 22:25:20\",\"kit_status\":4,\"sell_status\":2,\"created_at\":\"2020-03-22 15:54:00\",\"updated_at\":\"2020-05-13 17:09:44\",\"deleted_at\":null},\"old\":null}', '2020-05-13 14:09:44', '2020-05-13 14:09:44'),
(59, 'superAdmin', 'go to kits index', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-13 14:09:47', '2020-05-13 14:09:47'),
(60, 'apiUser', 'Get result - taksonomik - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 14:10:42', '2020-05-13 14:10:42'),
(61, 'apiUser', 'Get result - all scores - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 14:10:42', '2020-05-13 14:10:42'),
(62, 'apiUser', 'Get result - close profle - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 14:10:42', '2020-05-13 14:10:42'),
(63, 'apiUser', 'Get result - age range - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 14:10:42', '2020-05-13 14:10:42'),
(64, 'apiUser', 'Get result - food - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 14:10:42', '2020-05-13 14:10:42'),
(65, 'apiUser', 'Get result - taksonomik - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 14:18:18', '2020-05-13 14:18:18'),
(66, 'apiUser', 'Get result - close profle - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 14:18:18', '2020-05-13 14:18:18'),
(67, 'apiUser', 'Get result - age range - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 14:18:18', '2020-05-13 14:18:18'),
(68, 'apiUser', 'Get result - all scores - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 14:18:18', '2020-05-13 14:18:18'),
(69, 'apiUser', 'Get result - food - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 14:18:18', '2020-05-13 14:18:18');
INSERT INTO `activity_log` (`id`, `log_name`, `description`, `subject_id`, `subject_type`, `causer_id`, `causer_type`, `properties`, `created_at`, `updated_at`) VALUES
(70, 'apiUser', 'Get result - all scores - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 14:19:50', '2020-05-13 14:19:50'),
(71, 'apiUser', 'Get result - age range - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 14:19:50', '2020-05-13 14:19:50'),
(72, 'apiUser', 'Get result - close profle - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 14:19:50', '2020-05-13 14:19:50'),
(73, 'apiUser', 'Get result - taksonomik - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 14:19:50', '2020-05-13 14:19:50'),
(74, 'apiUser', 'Get result - food - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 14:19:50', '2020-05-13 14:19:50'),
(75, 'apiUser', 'Get result - age range - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 15:22:06', '2020-05-13 15:22:06'),
(76, 'apiUser', 'Get result - taksonomik - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 15:22:06', '2020-05-13 15:22:06'),
(77, 'apiUser', 'Get result - close profle - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 15:22:06', '2020-05-13 15:22:06'),
(78, 'apiUser', 'Get result - all scores - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 15:22:07', '2020-05-13 15:22:07'),
(79, 'apiUser', 'Get result - food - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 15:22:07', '2020-05-13 15:22:07'),
(80, 'apiUser', 'Send reports for kit code: AA0056', NULL, 'App\\User', 118, 'App\\User', '{\"ip\":\"127.0.0.1\",\"attributes\":null,\"old\":null}', '2020-05-13 15:32:25', '2020-05-13 15:32:25'),
(81, 'apiUser', 'Send reports for kit code: AA0056', NULL, 'App\\User', 118, 'App\\User', '{\"ip\":\"127.0.0.1\",\"attributes\":null,\"old\":null}', '2020-05-13 16:02:58', '2020-05-13 16:02:58'),
(82, 'apiUser', 'Get result - taksonomik - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 16:47:54', '2020-05-13 16:47:54'),
(83, 'apiUser', 'Get result - all scores - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 16:47:54', '2020-05-13 16:47:54'),
(84, 'apiUser', 'Get result - age range - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 16:47:54', '2020-05-13 16:47:54'),
(85, 'apiUser', 'Get result - close profle - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 16:47:54', '2020-05-13 16:47:54'),
(86, 'apiUser', 'Get result - food - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 16:47:54', '2020-05-13 16:47:54'),
(87, 'apiUser', 'Get result - taksonomik - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 17:25:45', '2020-05-13 17:25:45'),
(88, 'apiUser', 'Get result - age range - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 17:25:45', '2020-05-13 17:25:45'),
(89, 'apiUser', 'Get result - close profle - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 17:25:45', '2020-05-13 17:25:45'),
(90, 'apiUser', 'Get result - all scores - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 17:25:45', '2020-05-13 17:25:45'),
(91, 'apiUser', 'Get result - food - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 17:25:45', '2020-05-13 17:25:45'),
(92, 'apiUser', 'Get result - age range - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 17:26:12', '2020-05-13 17:26:12'),
(93, 'apiUser', 'Get result - close profle - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 17:26:12', '2020-05-13 17:26:12'),
(94, 'apiUser', 'Get result - taksonomik - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 17:26:12', '2020-05-13 17:26:12'),
(95, 'apiUser', 'Get result - all scores - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 17:26:12', '2020-05-13 17:26:12'),
(96, 'apiUser', 'Get result - food - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 17:26:12', '2020-05-13 17:26:12'),
(97, 'apiUser', 'Get result - all scores - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 17:26:35', '2020-05-13 17:26:35'),
(98, 'apiUser', 'Get result - age range - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 17:26:35', '2020-05-13 17:26:35'),
(99, 'apiUser', 'Get result - close profle - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 17:26:35', '2020-05-13 17:26:35'),
(100, 'apiUser', 'Get result - taksonomik - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 17:26:35', '2020-05-13 17:26:35'),
(101, 'apiUser', 'Get result - food - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 17:26:35', '2020-05-13 17:26:35'),
(102, 'superAdmin', 'go to kits show', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-13 18:04:37', '2020-05-13 18:04:37'),
(103, 'apiUser', 'Trying to register kit with this code: TEST', NULL, NULL, NULL, NULL, '{\"ip\":\"::1\"}', '2020-05-13 18:05:17', '2020-05-13 18:05:17'),
(104, 'apiUser', 'Trying to register kit with this code: TEST', NULL, NULL, NULL, NULL, '{\"ip\":\"::1\"}', '2020-05-13 18:07:44', '2020-05-13 18:07:44'),
(105, 'apiUser', 'Trying to register kit with this code: TEST', NULL, NULL, NULL, NULL, '{\"ip\":\"::1\"}', '2020-05-13 18:11:54', '2020-05-13 18:11:54'),
(106, 'apiUser', 'Trying to register kit with this code: TEST', NULL, NULL, NULL, NULL, '{\"ip\":\"::1\"}', '2020-05-13 18:13:17', '2020-05-13 18:13:17'),
(107, 'apiUser', 'Trying to register kit with this code: TEST', NULL, NULL, NULL, NULL, '{\"ip\":\"::1\"}', '2020-05-13 18:13:59', '2020-05-13 18:13:59'),
(108, 'apiUser', 'Trying to register kit with this code: 123321', NULL, NULL, NULL, NULL, '{\"ip\":\"::1\"}', '2020-05-13 18:19:49', '2020-05-13 18:19:49'),
(109, 'apiUser', 'Trying to register kit with this code: TEST', NULL, NULL, NULL, NULL, '{\"ip\":\"::1\"}', '2020-05-13 18:20:09', '2020-05-13 18:20:09'),
(110, 'apiUser', 'user with email: ot.themechanic95@gmail.com login successful', NULL, NULL, 118, 'App\\User', '{\"ip\":\"::1\"}', '2020-05-13 18:21:43', '2020-05-13 18:21:43'),
(111, 'apiUser', 'Get result - age range - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 18:21:44', '2020-05-13 18:21:44'),
(112, 'apiUser', 'Get result - all scores - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 18:21:44', '2020-05-13 18:21:44'),
(113, 'apiUser', 'Get result - close profle - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 18:21:44', '2020-05-13 18:21:44'),
(114, 'apiUser', 'Get result - food - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 18:21:44', '2020-05-13 18:21:44'),
(115, 'apiUser', 'Get result - taksonomik - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 18:22:19', '2020-05-13 18:22:19'),
(116, 'apiUser', 'Trying to register kit with this code: TEST', NULL, NULL, NULL, NULL, '{\"ip\":\"::1\"}', '2020-05-13 18:24:42', '2020-05-13 18:24:42'),
(117, 'apiUser', 'Trying to register kit with this code: TEST', NULL, NULL, NULL, NULL, '{\"ip\":\"::1\"}', '2020-05-13 18:24:42', '2020-05-13 18:24:42'),
(118, 'apiUser', 'New user register successfully to this kit: TEST', 124, 'App\\User', NULL, NULL, '{\"ip\":\"::1\",\"attributes\":{\"id\":124,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"otaha@aisolt.com\",\"email_verified_at\":null,\"gender\":1,\"date_of_birth\":\"1999-10-20\",\"address\":null,\"phone\":\"1231423534534\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-05-13 21:25:27\",\"updated_at\":\"2020-05-13 21:25:27\",\"kits\":[{\"id\":607,\"kit_code\":\"TEST\",\"microbiome\":null,\"food\":null,\"user_id\":124,\"order_detail_id\":null,\"survey_id\":null,\"registered\":1,\"registered_at\":\"2020-05-13 21:25:27\",\"delevered_to_user_at\":null,\"received_from_user_at\":null,\"send_to_lab_at\":null,\"uploaded_at\":null,\"survey_filled_at\":null,\"kit_status\":0,\"sell_status\":2,\"created_at\":\"2020-04-09 12:19:19\",\"updated_at\":\"2020-05-13 21:25:27\",\"deleted_at\":null}]},\"old\":null}', '2020-05-13 18:25:27', '2020-05-13 18:25:27'),
(119, 'apiUser', 'Verification code sent to user email: otaha@aisolt.com', 124, 'App\\User', 124, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":124,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"otaha@aisolt.com\",\"email_verified_at\":null,\"gender\":1,\"date_of_birth\":\"1999-10-20\",\"address\":null,\"phone\":\"1231423534534\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-05-13 21:25:27\",\"updated_at\":\"2020-05-13 21:25:28\"},\"old\":null}', '2020-05-13 18:25:30', '2020-05-13 18:25:30'),
(120, 'apiUser', 'The email address has been verified, email: otaha@aisolt.com', 124, 'App\\User', 124, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":124,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"otaha@aisolt.com\",\"email_verified_at\":\"2020-05-13 21:26:28\",\"gender\":1,\"date_of_birth\":\"1999-10-20\",\"address\":null,\"phone\":\"1231423534534\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-05-13 21:25:27\",\"updated_at\":\"2020-05-13 21:26:28\",\"kits\":[{\"id\":607,\"kit_code\":\"TEST\",\"microbiome\":null,\"food\":null,\"user_id\":124,\"order_detail_id\":null,\"survey_id\":null,\"registered\":1,\"registered_at\":\"2020-05-13 21:25:27\",\"delevered_to_user_at\":null,\"received_from_user_at\":null,\"send_to_lab_at\":null,\"uploaded_at\":null,\"survey_filled_at\":null,\"kit_status\":0,\"sell_status\":2,\"created_at\":\"2020-04-09 12:19:19\",\"updated_at\":\"2020-05-13 21:25:27\",\"deleted_at\":null}]},\"old\":null}', '2020-05-13 18:26:28', '2020-05-13 18:26:28'),
(121, 'apiUser', 'user with email: ot.themechanic95@gmail.com login successful', NULL, NULL, 118, 'App\\User', '{\"ip\":\"::1\"}', '2020-05-13 19:06:29', '2020-05-13 19:06:29'),
(122, 'apiUser', 'Get result - taksonomik - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 19:06:30', '2020-05-13 19:06:30'),
(123, 'apiUser', 'Get result - age range - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 19:06:30', '2020-05-13 19:06:30'),
(124, 'apiUser', 'Get result - close profle - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 19:06:30', '2020-05-13 19:06:30'),
(125, 'apiUser', 'Get result - all scores - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 19:06:30', '2020-05-13 19:06:30'),
(126, 'apiUser', 'Get result - age range - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 19:06:30', '2020-05-13 19:06:30'),
(127, 'apiUser', 'Get result - food - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 19:06:30', '2020-05-13 19:06:30'),
(128, 'apiUser', 'Get result - close profle - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 19:06:30', '2020-05-13 19:06:30'),
(129, 'apiUser', 'Get result - all scores - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 19:06:30', '2020-05-13 19:06:30'),
(130, 'apiUser', 'Get result - food - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-13 19:06:30', '2020-05-13 19:06:30'),
(131, 'apiUser', 'Send reports for kit code: AA0056', NULL, 'App\\User', 118, 'App\\User', '{\"ip\":\"127.0.0.1\",\"attributes\":null,\"old\":null}', '2020-05-13 19:09:36', '2020-05-13 19:09:36'),
(132, 'apiUser', 'Send reports for kit code: AA0056', NULL, 'App\\User', 118, 'App\\User', '{\"ip\":\"127.0.0.1\",\"attributes\":null,\"old\":null}', '2020-05-13 19:20:06', '2020-05-13 19:20:06'),
(133, 'superAdmin', 'go to kits index', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-14 12:25:52', '2020-05-14 12:25:52'),
(134, 'apiUser', 'user with email: ot.themechanic95@gmail.com login unsuccessful', NULL, NULL, NULL, NULL, '{\"ip\":\"::1\"}', '2020-05-14 13:36:58', '2020-05-14 13:36:58'),
(135, 'apiUser', 'user with email: ot.themechanic95@gmail.com login unsuccessful', NULL, NULL, NULL, NULL, '{\"ip\":\"::1\"}', '2020-05-14 13:36:58', '2020-05-14 13:36:58'),
(136, 'apiUser', 'user with email: ot.themechanic95@gmail.com login successful', NULL, NULL, 118, 'App\\User', '{\"ip\":\"::1\"}', '2020-05-14 13:37:09', '2020-05-14 13:37:09'),
(137, 'apiUser', 'Get result - age range - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 13:37:09', '2020-05-14 13:37:09'),
(138, 'apiUser', 'Get result - close profle - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 13:37:10', '2020-05-14 13:37:10'),
(139, 'apiUser', 'Get result - all scores - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 13:37:10', '2020-05-14 13:37:10'),
(140, 'apiUser', 'Get result - food - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 13:37:11', '2020-05-14 13:37:11'),
(141, 'apiUser', 'Get result - taksonomik - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 13:37:21', '2020-05-14 13:37:21'),
(142, 'apiUser', 'Get result - all scores - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 13:37:33', '2020-05-14 13:37:33'),
(143, 'apiUser', 'Get result - taksonomik - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 13:37:33', '2020-05-14 13:37:33'),
(144, 'apiUser', 'Get result - close profle - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 13:37:33', '2020-05-14 13:37:33'),
(145, 'apiUser', 'Get result - age range - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 13:37:33', '2020-05-14 13:37:33'),
(146, 'apiUser', 'Get result - food - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 13:37:33', '2020-05-14 13:37:33'),
(147, 'apiUser', 'Get result - age range - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 13:38:06', '2020-05-14 13:38:06'),
(148, 'apiUser', 'Get result - close profle - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:24:25', '2020-05-14 14:24:25'),
(149, 'apiUser', 'Get result - taksonomik - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:24:25', '2020-05-14 14:24:25'),
(150, 'apiUser', 'Get result - all scores - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:24:25', '2020-05-14 14:24:25'),
(151, 'apiUser', 'Get result - age range - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:24:25', '2020-05-14 14:24:25'),
(152, 'apiUser', 'Get result - food - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:24:25', '2020-05-14 14:24:25'),
(153, 'apiUser', 'Get result - all scores - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:24:32', '2020-05-14 14:24:32'),
(154, 'apiUser', 'Get result - age range - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:24:32', '2020-05-14 14:24:32'),
(155, 'apiUser', 'Get result - taksonomik - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:24:32', '2020-05-14 14:24:32'),
(156, 'apiUser', 'Get result - close profle - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:24:32', '2020-05-14 14:24:32'),
(157, 'apiUser', 'Get result - food - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:24:32', '2020-05-14 14:24:32'),
(158, 'apiUser', 'Get result - all scores - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:51:00', '2020-05-14 14:51:00'),
(159, 'apiUser', 'Get result - taksonomik - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:51:00', '2020-05-14 14:51:00'),
(160, 'apiUser', 'Get result - age range - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:51:00', '2020-05-14 14:51:00'),
(161, 'apiUser', 'Get result - close profle - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:51:01', '2020-05-14 14:51:01'),
(162, 'apiUser', 'Get result - food - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:51:01', '2020-05-14 14:51:01'),
(163, 'apiUser', 'Get result - age range - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:51:31', '2020-05-14 14:51:31'),
(164, 'apiUser', 'Get result - all scores - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:51:31', '2020-05-14 14:51:31'),
(165, 'apiUser', 'Get result - taksonomik - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:51:31', '2020-05-14 14:51:31'),
(166, 'apiUser', 'Get result - close profle - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:51:31', '2020-05-14 14:51:31'),
(167, 'apiUser', 'Get result - food - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:51:31', '2020-05-14 14:51:31'),
(168, 'apiUser', 'Get result - taksonomik - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:51:32', '2020-05-14 14:51:32'),
(169, 'apiUser', 'Get result - close profle - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:51:32', '2020-05-14 14:51:32'),
(170, 'apiUser', 'Get result - all scores - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:51:32', '2020-05-14 14:51:32'),
(171, 'apiUser', 'Get result - age range - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:51:32', '2020-05-14 14:51:32'),
(172, 'apiUser', 'Get result - food - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:51:32', '2020-05-14 14:51:32'),
(173, 'apiUser', 'Get result - age range - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:52:28', '2020-05-14 14:52:28');
INSERT INTO `activity_log` (`id`, `log_name`, `description`, `subject_id`, `subject_type`, `causer_id`, `causer_type`, `properties`, `created_at`, `updated_at`) VALUES
(174, 'apiUser', 'Get result - close profle - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:52:28', '2020-05-14 14:52:28'),
(175, 'apiUser', 'Get result - taksonomik - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:52:28', '2020-05-14 14:52:28'),
(176, 'apiUser', 'Get result - all scores - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:52:28', '2020-05-14 14:52:28'),
(177, 'apiUser', 'Get result - food - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:52:28', '2020-05-14 14:52:28'),
(178, 'apiUser', 'Get result - all scores - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:53:14', '2020-05-14 14:53:14'),
(179, 'apiUser', 'Get result - age range - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:53:14', '2020-05-14 14:53:14'),
(180, 'apiUser', 'Get result - taksonomik - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:53:14', '2020-05-14 14:53:14'),
(181, 'apiUser', 'Get result - close profle - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:53:14', '2020-05-14 14:53:14'),
(182, 'apiUser', 'Get result - food - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:53:14', '2020-05-14 14:53:14'),
(183, 'apiUser', 'Get result - taksonomik - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:55:26', '2020-05-14 14:55:26'),
(184, 'apiUser', 'Get result - age range - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:55:26', '2020-05-14 14:55:26'),
(185, 'apiUser', 'Get result - all scores - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:55:26', '2020-05-14 14:55:26'),
(186, 'apiUser', 'Get result - close profle - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:55:26', '2020-05-14 14:55:26'),
(187, 'apiUser', 'Get result - food - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:55:26', '2020-05-14 14:55:26'),
(188, 'apiUser', 'Get result - taksonomik - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:55:34', '2020-05-14 14:55:34'),
(189, 'apiUser', 'Get result - age range - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:55:34', '2020-05-14 14:55:34'),
(190, 'apiUser', 'Get result - all scores - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:55:34', '2020-05-14 14:55:34'),
(191, 'apiUser', 'Get result - close profle - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:55:34', '2020-05-14 14:55:34'),
(192, 'apiUser', 'Get result - food - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:55:34', '2020-05-14 14:55:34'),
(193, 'apiUser', 'Get result - age range - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:55:45', '2020-05-14 14:55:45'),
(194, 'apiUser', 'Get result - close profle - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:55:45', '2020-05-14 14:55:45'),
(195, 'apiUser', 'Get result - taksonomik - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:55:45', '2020-05-14 14:55:45'),
(196, 'apiUser', 'Get result - all scores - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:55:45', '2020-05-14 14:55:45'),
(197, 'apiUser', 'Get result - food - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 14:55:45', '2020-05-14 14:55:45'),
(198, 'apiUser', 'Get result - taksonomik - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 15:12:15', '2020-05-14 15:12:15'),
(199, 'apiUser', 'Get result - all scores - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 15:12:15', '2020-05-14 15:12:15'),
(200, 'apiUser', 'Get result - age range - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 15:12:15', '2020-05-14 15:12:15'),
(201, 'apiUser', 'Get result - close profle - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 15:12:15', '2020-05-14 15:12:15'),
(202, 'apiUser', 'Get result - food - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 15:12:15', '2020-05-14 15:12:15'),
(203, 'apiUser', 'Get result - taksonomik - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 15:12:56', '2020-05-14 15:12:56'),
(204, 'apiUser', 'Get result - close profle - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 15:12:56', '2020-05-14 15:12:56'),
(205, 'apiUser', 'Get result - all scores - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 15:12:56', '2020-05-14 15:12:56'),
(206, 'apiUser', 'Get result - age range - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 15:12:56', '2020-05-14 15:12:56'),
(207, 'apiUser', 'Get result - food - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 15:12:56', '2020-05-14 15:12:56'),
(208, 'apiUser', 'Get result - taksonomik - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 15:13:02', '2020-05-14 15:13:02'),
(209, 'apiUser', 'Get result - all scores - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 15:13:02', '2020-05-14 15:13:02'),
(210, 'apiUser', 'Get result - age range - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 15:13:02', '2020-05-14 15:13:02'),
(211, 'apiUser', 'Get result - close profle - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 15:13:02', '2020-05-14 15:13:02'),
(212, 'apiUser', 'Get result - food - kit code: AA0056', 118, 'App\\User', 118, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":118,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"ot.themechanic95@gmail.com\",\"email_verified_at\":\"2020-04-09 12:33:21\",\"gender\":0,\"date_of_birth\":null,\"address\":\"ISTANBUL\",\"phone\":\"45345345345345\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-04-09 12:25:39\",\"updated_at\":\"2020-04-09 12:33:21\"},\"old\":null}', '2020-05-14 15:13:02', '2020-05-14 15:13:02'),
(213, 'superAdmin', 'go to kits index', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-14 16:06:02', '2020-05-14 16:06:02'),
(214, 'superAdmin', 'go to kits index', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-14 16:06:17', '2020-05-14 16:06:17'),
(215, 'superAdmin', 'go to kits show', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-14 16:06:47', '2020-05-14 16:06:47'),
(216, 'superAdmin', 'go to resources index', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-14 16:10:48', '2020-05-14 16:10:48'),
(217, 'superAdmin', 'go to admin Dashboard page', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-14 16:11:04', '2020-05-14 16:11:04'),
(218, 'superAdmin', 'go to answers index', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-14 16:11:07', '2020-05-14 16:11:07'),
(219, 'superAdmin', 'go to products index', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-14 16:11:12', '2020-05-14 16:11:12'),
(220, 'superAdmin', 'go to kits index', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-14 16:11:13', '2020-05-14 16:11:13'),
(221, 'superAdmin', 'go to kits index', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-14 16:14:38', '2020-05-14 16:14:38'),
(222, 'superAdmin', 'go to kits index', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-14 16:15:05', '2020-05-14 16:15:05'),
(223, 'adminlogin', 'Admin with email: latik@enbiosis.com login successful', NULL, NULL, NULL, NULL, '{\"ip\":\"::1\"}', '2020-05-20 12:43:29', '2020-05-20 12:43:29'),
(224, 'superAdmin', 'go to admin Dashboard page', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-20 12:43:30', '2020-05-20 12:43:30'),
(225, 'superAdmin', 'go to kits index', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-20 12:43:42', '2020-05-20 12:43:42'),
(226, 'superAdmin', 'generate a pdf for kit codeERU4 pdf lang is - tr', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-20 12:44:46', '2020-05-20 12:44:46'),
(227, 'superAdmin', 'go to admin Dashboard page', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-20 12:44:49', '2020-05-20 12:44:49'),
(228, 'superAdmin', 'go to admin Dashboard page', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-20 12:45:24', '2020-05-20 12:45:24'),
(229, 'superAdmin', 'go to admin Dashboard page', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-20 15:11:28', '2020-05-20 15:11:28'),
(230, 'superAdmin', 'go to kits index', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-20 15:11:31', '2020-05-20 15:11:31'),
(231, 'superAdmin', 'generate a pdf for kit codeERU4 pdf lang is - tr', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-20 15:11:58', '2020-05-20 15:11:58'),
(232, 'superAdmin', 'generate a pdf for kit codeERU4 pdf lang is - en', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-20 15:12:50', '2020-05-20 15:12:50'),
(233, 'superAdmin', 'go to kits index', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-20 16:09:29', '2020-05-20 16:09:29'),
(234, 'superAdmin', 'generate a pdf for kit codeERU4 pdf lang is - en', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-20 16:20:24', '2020-05-20 16:20:24'),
(235, 'superAdmin', 'go to kits index', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-20 16:27:04', '2020-05-20 16:27:04'),
(236, 'adminlogin', 'Admin with email: latik@enbiosis.com login successful', NULL, NULL, NULL, NULL, '{\"ip\":\"::1\"}', '2020-05-21 12:20:22', '2020-05-21 12:20:22'),
(237, 'superAdmin', 'go to admin Dashboard page', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 12:20:23', '2020-05-21 12:20:23'),
(238, 'superAdmin', 'go to kits index', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 12:20:25', '2020-05-21 12:20:25'),
(239, 'superAdmin', 'go to kits index', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 12:20:53', '2020-05-21 12:20:53'),
(240, 'superAdmin', 'generate a pdf for kit codeERU4 pdf lang is - en', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 12:21:01', '2020-05-21 12:21:01'),
(241, 'superAdmin', 'generate a pdf for kit codeERU4 pdf lang is - tr', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 12:21:47', '2020-05-21 12:21:47'),
(242, 'superAdmin', 'go to admin Dashboard page', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 13:20:06', '2020-05-21 13:20:06'),
(243, 'superAdmin', 'generate a pdf for kit codeERU4 pdf lang is - tr', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 13:20:45', '2020-05-21 13:20:45'),
(244, 'superAdmin', 'generate a pdf for kit codeERU4 pdf lang is - tr', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 13:21:00', '2020-05-21 13:21:00'),
(245, 'superAdmin', 'go to admin Dashboard page', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 13:34:51', '2020-05-21 13:34:51'),
(246, 'superAdmin', 'generate a pdf for kit codeAA0056 pdf lang is - tr', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 13:35:31', '2020-05-21 13:35:31'),
(247, 'superAdmin', 'generate a pdf for kit codeAA0056 pdf lang is - tr', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 13:38:39', '2020-05-21 13:38:39'),
(248, 'superAdmin', 'generate a pdf for kit codeAA0056 pdf lang is - tr', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 13:40:06', '2020-05-21 13:40:06'),
(249, 'superAdmin', 'generate a pdf for kit codeAA0056 pdf lang is - en', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 13:46:43', '2020-05-21 13:46:43'),
(250, 'superAdmin', 'go to admin Dashboard page', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 15:42:04', '2020-05-21 15:42:04'),
(251, 'superAdmin', 'go to admin Dashboard page', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 16:14:39', '2020-05-21 16:14:39'),
(252, 'superAdmin', 'go to users index', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 16:14:41', '2020-05-21 16:14:41'),
(253, 'superAdmin', 'go to users edit', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 16:14:48', '2020-05-21 16:14:48'),
(254, 'superAdmin', 'go to users edit', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 16:14:55', '2020-05-21 16:14:55'),
(255, 'superAdmin', 'user updated', 124, 'App\\User', 1, 'App\\Admin', '{\"ip\":\"::1\",\"attributes\":{\"id\":124,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"otaha@aisolt.com\",\"email_verified_at\":\"2020-05-13 21:26:28\",\"gender\":\"1\",\"date_of_birth\":\"1999-10-20\",\"address\":\"12333211\",\"phone\":\"1231423534534\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-05-13 21:25:27\",\"updated_at\":\"2020-05-21 19:14:59\"},\"old\":{\"id\":124,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"otaha@aisolt.com\",\"email_verified_at\":\"2020-05-13 21:26:28\",\"gender\":1,\"date_of_birth\":\"1999-10-20\",\"address\":null,\"phone\":\"1231423534534\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-05-13 21:25:27\",\"updated_at\":\"2020-05-13 21:26:28\"}}', '2020-05-21 16:14:59', '2020-05-21 16:14:59'),
(256, 'superAdmin', 'go to users index', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 16:15:00', '2020-05-21 16:15:00'),
(257, 'superAdmin', 'go to users edit', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 16:15:03', '2020-05-21 16:15:03'),
(258, 'superAdmin', 'user updated', 124, 'App\\User', 1, 'App\\Admin', '{\"ip\":\"::1\",\"attributes\":{\"id\":124,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"otaha@aisolt.com\",\"email_verified_at\":\"2020-05-13 21:26:28\",\"gender\":\"1\",\"date_of_birth\":\"1999-10-20\",\"address\":\"12333211\",\"phone\":\"1231423534534\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-05-13 21:25:27\",\"updated_at\":\"2020-05-21 19:15:07\"},\"old\":{\"id\":124,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"otaha@aisolt.com\",\"email_verified_at\":\"2020-05-13 21:26:28\",\"gender\":1,\"date_of_birth\":\"1999-10-20\",\"address\":\"12333211\",\"phone\":\"1231423534534\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-05-13 21:25:27\",\"updated_at\":\"2020-05-21 19:14:59\"}}', '2020-05-21 16:15:07', '2020-05-21 16:15:07'),
(259, 'superAdmin', 'go to users index', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 16:15:07', '2020-05-21 16:15:07'),
(260, 'apiUser', 'user with email: otaha@aisolt.com login unsuccessful', NULL, NULL, NULL, NULL, '{\"ip\":\"::1\"}', '2020-05-21 16:15:22', '2020-05-21 16:15:22'),
(261, 'superAdmin', 'go to user show', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 16:15:36', '2020-05-21 16:15:36'),
(262, 'superAdmin', 'go to users edit', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 16:15:38', '2020-05-21 16:15:38'),
(263, 'superAdmin', 'user updated', 124, 'App\\User', 1, 'App\\Admin', '{\"ip\":\"::1\",\"attributes\":{\"id\":124,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"otaha@aisolt.com\",\"email_verified_at\":\"2020-05-13 21:26:28\",\"gender\":\"1\",\"date_of_birth\":\"1999-10-20\",\"address\":\"12333211\",\"phone\":\"1231423534534\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-05-13 21:25:27\",\"updated_at\":\"2020-05-21 19:15:42\"},\"old\":{\"id\":124,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"otaha@aisolt.com\",\"email_verified_at\":\"2020-05-13 21:26:28\",\"gender\":1,\"date_of_birth\":\"1999-10-20\",\"address\":\"12333211\",\"phone\":\"1231423534534\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-05-13 21:25:27\",\"updated_at\":\"2020-05-21 19:15:07\"}}', '2020-05-21 16:15:42', '2020-05-21 16:15:42'),
(264, 'superAdmin', 'go to users index', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 16:15:42', '2020-05-21 16:15:42'),
(265, 'apiUser', 'user with email: otaha@aisolt.com login successful', NULL, NULL, 124, 'App\\User', '{\"ip\":\"::1\"}', '2020-05-21 16:16:12', '2020-05-21 16:16:12'),
(266, 'superAdmin', 'go to user show', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 16:16:44', '2020-05-21 16:16:44'),
(267, 'superAdmin', 'go to kits index', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 16:16:53', '2020-05-21 16:16:53'),
(268, 'superAdmin', 'go to user show', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 16:17:06', '2020-05-21 16:17:06'),
(269, 'superAdmin', 'kit with  code = 12 Has Been Dislink from user - user email: laythateek@gmail.com -', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 16:17:26', '2020-05-21 16:17:26'),
(270, 'superAdmin', 'go to user show', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 16:17:26', '2020-05-21 16:17:26'),
(271, 'superAdmin', 'go to users index', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 16:17:32', '2020-05-21 16:17:32'),
(272, 'superAdmin', 'go to user show', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 16:17:35', '2020-05-21 16:17:35'),
(273, 'superAdmin', 'kit with  code = 12 Has Been link to user - user email: otaha@aisolt.com -', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 16:17:42', '2020-05-21 16:17:42'),
(274, 'superAdmin', 'go to user show', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 16:17:43', '2020-05-21 16:17:43'),
(275, 'apiUser', 'Get result - all scores - kit code: 12', 124, 'App\\User', 124, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":124,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"otaha@aisolt.com\",\"email_verified_at\":\"2020-05-13 21:26:28\",\"gender\":1,\"date_of_birth\":\"1999-10-20\",\"address\":\"12333211\",\"phone\":\"1231423534534\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-05-13 21:25:27\",\"updated_at\":\"2020-05-21 19:15:42\"},\"old\":null}', '2020-05-21 16:18:52', '2020-05-21 16:18:52'),
(276, 'apiUser', 'Get result - all scores - kit code: 12', 124, 'App\\User', 124, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":124,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"otaha@aisolt.com\",\"email_verified_at\":\"2020-05-13 21:26:28\",\"gender\":1,\"date_of_birth\":\"1999-10-20\",\"address\":\"12333211\",\"phone\":\"1231423534534\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-05-13 21:25:27\",\"updated_at\":\"2020-05-21 19:15:42\"},\"old\":null}', '2020-05-21 16:19:03', '2020-05-21 16:19:03'),
(277, 'apiUser', 'Get result - all scores - kit code: 12', 124, 'App\\User', 124, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":124,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"otaha@aisolt.com\",\"email_verified_at\":\"2020-05-13 21:26:28\",\"gender\":1,\"date_of_birth\":\"1999-10-20\",\"address\":\"12333211\",\"phone\":\"1231423534534\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-05-13 21:25:27\",\"updated_at\":\"2020-05-21 19:15:42\"},\"old\":null}', '2020-05-21 16:19:13', '2020-05-21 16:19:13'),
(278, 'apiUser', 'Get result - all scores - kit code: 12', 124, 'App\\User', 124, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":124,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"otaha@aisolt.com\",\"email_verified_at\":\"2020-05-13 21:26:28\",\"gender\":1,\"date_of_birth\":\"1999-10-20\",\"address\":\"12333211\",\"phone\":\"1231423534534\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-05-13 21:25:27\",\"updated_at\":\"2020-05-21 19:15:42\"},\"old\":null}', '2020-05-21 16:19:34', '2020-05-21 16:19:34'),
(279, 'apiUser', 'Get result - all scores - kit code: 12', 124, 'App\\User', 124, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":124,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"otaha@aisolt.com\",\"email_verified_at\":\"2020-05-13 21:26:28\",\"gender\":1,\"date_of_birth\":\"1999-10-20\",\"address\":\"12333211\",\"phone\":\"1231423534534\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-05-13 21:25:27\",\"updated_at\":\"2020-05-21 19:15:42\"},\"old\":null}', '2020-05-21 16:19:40', '2020-05-21 16:19:40'),
(280, 'apiUser', 'Get result - all scores - kit code: 12', 124, 'App\\User', 124, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":124,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"otaha@aisolt.com\",\"email_verified_at\":\"2020-05-13 21:26:28\",\"gender\":1,\"date_of_birth\":\"1999-10-20\",\"address\":\"12333211\",\"phone\":\"1231423534534\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-05-13 21:25:27\",\"updated_at\":\"2020-05-21 19:15:42\"},\"old\":null}', '2020-05-21 16:19:46', '2020-05-21 16:19:46'),
(281, 'apiUser', 'Get result - all scores - kit code: 12', 124, 'App\\User', 124, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":124,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"otaha@aisolt.com\",\"email_verified_at\":\"2020-05-13 21:26:28\",\"gender\":1,\"date_of_birth\":\"1999-10-20\",\"address\":\"12333211\",\"phone\":\"1231423534534\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-05-13 21:25:27\",\"updated_at\":\"2020-05-21 19:15:42\"},\"old\":null}', '2020-05-21 16:19:53', '2020-05-21 16:19:53'),
(282, 'apiUser', 'Get result - all scores - kit code: 12', 124, 'App\\User', 124, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":124,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"otaha@aisolt.com\",\"email_verified_at\":\"2020-05-13 21:26:28\",\"gender\":1,\"date_of_birth\":\"1999-10-20\",\"address\":\"12333211\",\"phone\":\"1231423534534\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-05-13 21:25:27\",\"updated_at\":\"2020-05-21 19:15:42\"},\"old\":null}', '2020-05-21 16:20:17', '2020-05-21 16:20:17'),
(283, 'apiUser', 'user with email: laythateek@gmail.com login unsuccessful', NULL, NULL, NULL, NULL, '{\"ip\":\"::1\"}', '2020-05-21 16:20:53', '2020-05-21 16:20:53'),
(284, 'apiUser', 'user with email: otaha@aisolt.com login successful', NULL, NULL, 124, 'App\\User', '{\"ip\":\"::1\"}', '2020-05-21 16:21:25', '2020-05-21 16:21:25'),
(285, 'apiUser', 'Get result - all scores - kit code: 12', 124, 'App\\User', 124, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":124,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"otaha@aisolt.com\",\"email_verified_at\":\"2020-05-13 21:26:28\",\"gender\":1,\"date_of_birth\":\"1999-10-20\",\"address\":\"12333211\",\"phone\":\"1231423534534\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-05-13 21:25:27\",\"updated_at\":\"2020-05-21 19:15:42\"},\"old\":null}', '2020-05-21 16:21:53', '2020-05-21 16:21:53'),
(286, 'apiUser', 'Get result - all scores - kit code: 12', 124, 'App\\User', 124, 'App\\User', '{\"ip\":\"::1\",\"attributes\":{\"id\":124,\"dietitian_id\":null,\"name\":\"Omar Taha\",\"email\":\"otaha@aisolt.com\",\"email_verified_at\":\"2020-05-13 21:26:28\",\"gender\":1,\"date_of_birth\":\"1999-10-20\",\"address\":\"12333211\",\"phone\":\"1231423534534\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-05-13 21:25:27\",\"updated_at\":\"2020-05-21 19:15:42\"},\"old\":null}', '2020-05-21 16:22:16', '2020-05-21 16:22:16'),
(287, 'superAdmin', 'go to admin Dashboard page', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 16:39:46', '2020-05-21 16:39:46'),
(288, 'superAdmin', 'go to admin Dashboard page', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 18:20:04', '2020-05-21 18:20:04'),
(289, 'superAdmin', 'go to kits index', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 18:20:06', '2020-05-21 18:20:06'),
(290, 'superAdmin', 'go to kits index', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 18:20:49', '2020-05-21 18:20:49'),
(291, 'superAdmin', 'generate a pdf for kit code12 pdf lang is - tr', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 18:20:49', '2020-05-21 18:20:49'),
(292, 'superAdmin', 'generate a pdf for kit codeAA0056 pdf lang is - en', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 18:20:53', '2020-05-21 18:20:53'),
(293, 'superAdmin', 'go to kits index', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 18:20:58', '2020-05-21 18:20:58'),
(294, 'superAdmin', 'generate a pdf for kit codeAA0056 pdf lang is - tr', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 18:21:22', '2020-05-21 18:21:22'),
(295, 'superAdmin', 'generate a pdf for kit codeAA0056 pdf lang is - en', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-21 18:21:46', '2020-05-21 18:21:46'),
(296, 'adminlogin', 'Admin with email: latik@enbiosis.com login successful', NULL, NULL, NULL, NULL, '{\"ip\":\"::1\"}', '2020-05-22 13:57:26', '2020-05-22 13:57:26'),
(297, 'superAdmin', 'go to admin Dashboard page', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-22 13:57:26', '2020-05-22 13:57:26'),
(298, 'superAdmin', 'go to available coupon page', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-22 13:57:28', '2020-05-22 13:57:28'),
(299, 'adminlogin', 'Admin with email: latik@enbiosis.com login successful', NULL, NULL, NULL, NULL, '{\"ip\":\"::1\"}', '2020-05-23 13:23:20', '2020-05-23 13:23:20'),
(300, 'superAdmin', 'go to admin Dashboard page', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-23 13:23:21', '2020-05-23 13:23:21'),
(301, 'superAdmin', 'go to admin Dashboard page', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-23 13:33:19', '2020-05-23 13:33:19'),
(302, 'superAdmin', 'go to admins index', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-23 13:33:21', '2020-05-23 13:33:21'),
(303, 'superAdmin', 'New Admin Created', 2, 'App\\Admin', 1, 'App\\Admin', '{\"ip\":\"::1\",\"attributes\":{\"name\":\"Laith Atik\",\"email\":\"lab@enbiosis.com\",\"phone\":\"12332112321\",\"active\":true,\"role\":\"1\",\"updated_at\":\"2020-05-23 16:33:51\",\"created_at\":\"2020-05-23 16:33:51\",\"id\":2},\"old\":null}', '2020-05-23 13:33:51', '2020-05-23 13:33:51'),
(304, 'superAdmin', 'go to admins index', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-23 13:33:51', '2020-05-23 13:33:51'),
(305, 'adminlogin', 'Admin with email: lab@enbiosis.com login successful', NULL, NULL, NULL, NULL, '{\"ip\":\"::1\"}', '2020-05-23 13:34:11', '2020-05-23 13:34:11'),
(306, 'opAdmin', 'go to order index', NULL, NULL, 2, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-23 13:34:14', '2020-05-23 13:34:14'),
(307, 'opAdmin', 'go to orders show', 24, 'App\\Order', 2, 'App\\Admin', '{\"ip\":\"::1\",\"attributes\":{\"id\":24,\"payment_id\":30,\"coupon_id\":22,\"tracking\":\"20097Mt8E04011117\",\"status\":0,\"city_id\":67,\"county_id\":755,\"address\":\"Ba\\u011flar Mh Hakan Kurtulan Sk No 8\\/3\",\"fcity_id\":1,\"fcounty_id\":3,\"faddress\":\"laith atik\",\"name\":\"Laith Atik\",\"email\":\"laythateek@gmail.com\",\"dietitian_code\":null,\"phone\":\"+905348447726\",\"price\":0.39,\"checkbox\":1,\"created_at\":\"2020-05-23 12:46:01\",\"updated_at\":\"2020-04-06 12:46:01\"},\"old\":null}', '2020-05-23 13:34:18', '2020-05-23 13:34:18'),
(308, 'adminlogin', 'Admin with email: latik@enbiosis.com login successful', NULL, NULL, NULL, NULL, '{\"ip\":\"::1\"}', '2020-05-23 13:34:44', '2020-05-23 13:34:44'),
(309, 'superAdmin', 'go to admin Dashboard page', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-23 13:34:44', '2020-05-23 13:34:44'),
(310, 'superAdmin', 'go to orders index', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-23 13:34:46', '2020-05-23 13:34:46'),
(311, 'superAdmin', 'go to orders show', 24, 'App\\Order', 1, 'App\\Admin', '{\"ip\":\"::1\",\"attributes\":{\"id\":24,\"payment_id\":30,\"coupon_id\":22,\"tracking\":\"20097Mt8E04011117\",\"status\":0,\"city_id\":67,\"county_id\":755,\"address\":\"Ba\\u011flar Mh Hakan Kurtulan Sk No 8\\/3\",\"fcity_id\":1,\"fcounty_id\":3,\"faddress\":\"laith atik\",\"name\":\"Laith Atik\",\"email\":\"laythateek@gmail.com\",\"dietitian_code\":null,\"phone\":\"+905348447726\",\"price\":0.39,\"checkbox\":1,\"created_at\":\"2020-05-23 12:46:01\",\"updated_at\":\"2020-04-06 12:46:01\"},\"old\":null}', '2020-05-23 13:34:56', '2020-05-23 13:34:56'),
(312, 'superAdmin', 'go to orders show', 24, 'App\\Order', 1, 'App\\Admin', '{\"ip\":\"::1\",\"attributes\":{\"id\":24,\"payment_id\":30,\"coupon_id\":22,\"tracking\":\"20097Mt8E04011117\",\"status\":0,\"city_id\":67,\"county_id\":755,\"address\":\"Ba\\u011flar Mh Hakan Kurtulan Sk No 8\\/3\",\"fcity_id\":1,\"fcounty_id\":3,\"faddress\":\"laith atik\",\"name\":\"Laith Atik\",\"email\":\"laythateek@gmail.com\",\"dietitian_code\":null,\"phone\":\"+905348447726\",\"price\":0.39,\"checkbox\":1,\"created_at\":\"2020-05-23 12:46:01\",\"updated_at\":\"2020-04-06 12:46:01\"},\"old\":null}', '2020-05-23 13:35:54', '2020-05-23 13:35:54'),
(313, 'superAdmin', 'go to orders show', 24, 'App\\Order', 1, 'App\\Admin', '{\"ip\":\"::1\",\"attributes\":{\"id\":24,\"payment_id\":30,\"coupon_id\":22,\"tracking\":\"20097Mt8E04011117\",\"status\":0,\"city_id\":67,\"county_id\":755,\"address\":\"Ba\\u011flar Mh Hakan Kurtulan Sk No 8\\/3\",\"fcity_id\":1,\"fcounty_id\":3,\"faddress\":\"laith atik\",\"name\":\"Laith Atik\",\"email\":\"laythateek@gmail.com\",\"dietitian_code\":null,\"phone\":\"+905348447726\",\"price\":0.39,\"checkbox\":1,\"created_at\":\"2020-05-23 12:46:01\",\"updated_at\":\"2020-04-06 12:46:01\"},\"old\":null}', '2020-05-23 13:36:33', '2020-05-23 13:36:33'),
(314, 'superAdmin', 'go to orders show', 24, 'App\\Order', 1, 'App\\Admin', '{\"ip\":\"::1\",\"attributes\":{\"id\":24,\"payment_id\":30,\"coupon_id\":22,\"tracking\":\"20097Mt8E04011117\",\"status\":0,\"city_id\":67,\"county_id\":755,\"address\":\"Ba\\u011flar Mh Hakan Kurtulan Sk No 8\\/3\",\"fcity_id\":1,\"fcounty_id\":3,\"faddress\":\"laith atik\",\"name\":\"Laith Atik\",\"email\":\"laythateek@gmail.com\",\"dietitian_code\":null,\"phone\":\"+905348447726\",\"price\":0.39,\"checkbox\":1,\"created_at\":\"2020-05-23 12:46:01\",\"updated_at\":\"2020-04-06 12:46:01\"},\"old\":null}', '2020-05-23 13:36:56', '2020-05-23 13:36:56'),
(315, 'superAdmin', 'go to orders show', 24, 'App\\Order', 1, 'App\\Admin', '{\"ip\":\"::1\",\"attributes\":{\"id\":24,\"payment_id\":30,\"coupon_id\":22,\"tracking\":\"20097Mt8E04011117\",\"status\":0,\"city_id\":67,\"county_id\":755,\"address\":\"Ba\\u011flar Mh Hakan Kurtulan Sk No 8\\/3\",\"fcity_id\":1,\"fcounty_id\":3,\"faddress\":\"laith atik\",\"name\":\"Laith Atik\",\"email\":\"laythateek@gmail.com\",\"dietitian_code\":null,\"phone\":\"+905348447726\",\"price\":0.39,\"checkbox\":1,\"created_at\":\"2020-05-23 12:46:01\",\"updated_at\":\"2020-04-06 12:46:01\"},\"old\":null}', '2020-05-23 13:37:45', '2020-05-23 13:37:45'),
(316, 'superAdmin', 'go to orders show', 24, 'App\\Order', 1, 'App\\Admin', '{\"ip\":\"::1\",\"attributes\":{\"id\":24,\"payment_id\":30,\"coupon_id\":22,\"tracking\":\"20097Mt8E04011117\",\"status\":0,\"city_id\":67,\"county_id\":755,\"address\":\"Ba\\u011flar Mh Hakan Kurtulan Sk No 8\\/3\",\"fcity_id\":1,\"fcounty_id\":3,\"faddress\":\"laith atik\",\"name\":\"Laith Atik\",\"email\":\"laythateek@gmail.com\",\"dietitian_code\":null,\"phone\":\"+905348447726\",\"price\":0.39,\"checkbox\":1,\"created_at\":\"2020-05-23 12:46:01\",\"updated_at\":\"2020-04-06 12:46:01\"},\"old\":null}', '2020-05-23 13:37:52', '2020-05-23 13:37:52'),
(317, 'adminlogin', 'Admin with email: latik@enbiosis.com login successful', NULL, NULL, NULL, NULL, '{\"ip\":\"::1\"}', '2020-05-27 14:46:35', '2020-05-27 14:46:35'),
(318, 'superAdmin', 'go to admin Dashboard page', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-27 14:46:35', '2020-05-27 14:46:35'),
(319, 'superAdmin', 'go to admin Dashboard page', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-27 14:51:54', '2020-05-27 14:51:54'),
(320, 'apiUser', 'user with email: laythateek@gmail.com login unsuccessful', NULL, NULL, NULL, NULL, '{\"ip\":\"::1\"}', '2020-05-27 15:08:21', '2020-05-27 15:08:21'),
(321, 'apiUser', 'user with email: laythateek@gmail.com login unsuccessful', NULL, NULL, NULL, NULL, '{\"ip\":\"::1\"}', '2020-05-27 15:08:26', '2020-05-27 15:08:26'),
(322, 'superAdmin', 'go to admin Dashboard page', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-27 15:08:31', '2020-05-27 15:08:31'),
(323, 'superAdmin', 'go to users index', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-27 15:08:34', '2020-05-27 15:08:34'),
(324, 'superAdmin', 'user searched for laythateek@gmail.com', NULL, 'App\\User', 1, 'App\\Admin', '{\"ip\":\"::1\",\"attributes\":\"laythateek@gmail.com\",\"old\":null}', '2020-05-27 15:08:40', '2020-05-27 15:08:40'),
(325, 'superAdmin', 'go to user show', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-27 15:08:45', '2020-05-27 15:08:45'),
(326, 'superAdmin', 'go to users edit', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-27 15:08:47', '2020-05-27 15:08:47'),
(327, 'superAdmin', 'user updated', 7, 'App\\User', 1, 'App\\Admin', '{\"ip\":\"::1\",\"attributes\":{\"id\":7,\"dietitian_id\":null,\"name\":\"Laith Atik\",\"email\":\"laythateek@gmail.com\",\"email_verified_at\":\"2020-04-07 18:21:49\",\"gender\":\"1\",\"date_of_birth\":\"2020-05-07\",\"address\":\"Ba\\u011flar Mh Hakan Kurtulan Sk No 8\\/3\",\"phone\":\"05348447726\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-02-17 08:18:03\",\"updated_at\":\"2020-05-27 18:08:58\"},\"old\":{\"id\":7,\"dietitian_id\":null,\"name\":\"Laith Atik\",\"email\":\"laythateek@gmail.com\",\"email_verified_at\":\"2020-04-07 18:21:49\",\"gender\":1,\"date_of_birth\":null,\"address\":\"Ba\\u011flar Mh Hakan Kurtulan Sk No 8\\/3\",\"phone\":\"05348447726\",\"vegetarian\":0,\"deleted_at\":null,\"created_at\":\"2020-02-17 08:18:03\",\"updated_at\":\"2020-04-07 22:59:50\"}}', '2020-05-27 15:08:58', '2020-05-27 15:08:58'),
(328, 'superAdmin', 'go to users index', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-27 15:08:58', '2020-05-27 15:08:58'),
(329, 'apiUser', 'user with email: laythateek@gmail.com login successful', NULL, NULL, 7, 'App\\User', '{\"ip\":\"::1\"}', '2020-05-27 15:09:06', '2020-05-27 15:09:06'),
(330, 'apiUser', 'Send reports for kit code: AAAAA', NULL, 'App\\User', 7, 'App\\User', '{\"ip\":\"::1\",\"attributes\":null,\"old\":null}', '2020-05-27 15:17:32', '2020-05-27 15:17:32'),
(331, 'superAdmin', 'go to kits index', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-27 15:35:16', '2020-05-27 15:35:16'),
(332, 'superAdmin', 'generate a pdf for kit code12 pdf lang is - tr', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-27 15:35:38', '2020-05-27 15:35:38'),
(333, 'adminlogin', 'Admin with email: latik@enbiosis.com login successful', NULL, NULL, NULL, NULL, '{\"ip\":\"::1\"}', '2020-05-28 09:20:44', '2020-05-28 09:20:44'),
(334, 'superAdmin', 'go to admin Dashboard page', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-28 09:20:44', '2020-05-28 09:20:44'),
(335, 'superAdmin', 'go to kits index', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-28 09:20:47', '2020-05-28 09:20:47');
INSERT INTO `activity_log` (`id`, `log_name`, `description`, `subject_id`, `subject_type`, `causer_id`, `causer_type`, `properties`, `created_at`, `updated_at`) VALUES
(336, 'superAdmin', 'generate a pdf for kit code12 pdf lang is - tr', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-28 09:21:21', '2020-05-28 09:21:21'),
(337, 'superAdmin', 'generate a pdf for kit code12 pdf lang is - tr', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-28 09:24:26', '2020-05-28 09:24:26'),
(338, 'superAdmin', 'go to kits index', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-28 09:28:47', '2020-05-28 09:28:47'),
(339, 'superAdmin', 'go to dietitians index', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-28 11:12:12', '2020-05-28 11:12:12'),
(340, 'superAdmin', 'dietitian trashed', 2, 'App\\Dietitian', 1, 'App\\Admin', '{\"ip\":\"::1\",\"attributes\":{\"id\":2,\"name\":\"wdawd\",\"email\":\"laithatik@gmail.comwww\",\"email_verified_at\":null,\"reference_code\":\"EMqj34sGiJ\",\"address\":\"Ba\\u011flar Mh Hakan Kurtulan Sk No 8\\/3\",\"phone\":\"05348447726\",\"deleted_at\":\"2020-05-28 14:12:16\",\"created_at\":\"2020-02-16 18:28:40\",\"updated_at\":\"2020-05-28 14:12:16\"},\"old\":null}', '2020-05-28 11:12:16', '2020-05-28 11:12:16'),
(341, 'superAdmin', 'go to dietitians index', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-28 11:12:16', '2020-05-28 11:12:16'),
(342, 'superAdmin', 'dietitian trashed', 1, 'App\\Dietitian', 1, 'App\\Admin', '{\"ip\":\"::1\",\"attributes\":{\"id\":1,\"name\":\"Laith Atik\",\"email\":\"laithatik@gmail.com\",\"email_verified_at\":null,\"reference_code\":\"EMqj34sGiq\",\"address\":\"Ba\\u011flar Mh Hakan Kurtulan Sk No 8\\/3\",\"phone\":\"05348447726\",\"deleted_at\":\"2020-05-28 14:12:20\",\"created_at\":\"2020-02-05 09:09:47\",\"updated_at\":\"2020-05-28 14:12:20\"},\"old\":null}', '2020-05-28 11:12:20', '2020-05-28 11:12:20'),
(343, 'superAdmin', 'go to dietitians index', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-28 11:12:20', '2020-05-28 11:12:20'),
(344, 'superAdmin', 'go to dietitians trash', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-28 11:12:27', '2020-05-28 11:12:27'),
(345, 'superAdmin', 'dietitian deleted', 2, 'App\\Dietitian', 1, 'App\\Admin', '{\"ip\":\"::1\",\"attributes\":{\"id\":2,\"name\":\"wdawd\",\"email\":\"laithatik@gmail.comwww\",\"email_verified_at\":null,\"reference_code\":\"EMqj34sGiJ\",\"address\":\"Ba\\u011flar Mh Hakan Kurtulan Sk No 8\\/3\",\"phone\":\"05348447726\",\"deleted_at\":\"2020-05-28 14:12:16\",\"created_at\":\"2020-02-16 18:28:40\",\"updated_at\":\"2020-05-28 14:12:16\",\"photo\":null},\"old\":null}', '2020-05-28 11:12:30', '2020-05-28 11:12:30'),
(346, 'superAdmin', 'go to dietitians trash', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-28 11:12:30', '2020-05-28 11:12:30'),
(347, 'superAdmin', 'dietitian deleted', 1, 'App\\Dietitian', 1, 'App\\Admin', '{\"ip\":\"::1\",\"attributes\":{\"id\":1,\"name\":\"Laith Atik\",\"email\":\"laithatik@gmail.com\",\"email_verified_at\":null,\"reference_code\":\"EMqj34sGiq\",\"address\":\"Ba\\u011flar Mh Hakan Kurtulan Sk No 8\\/3\",\"phone\":\"05348447726\",\"deleted_at\":\"2020-05-28 14:12:20\",\"created_at\":\"2020-02-05 09:09:47\",\"updated_at\":\"2020-05-28 14:12:20\",\"photo\":null},\"old\":null}', '2020-05-28 11:12:34', '2020-05-28 11:12:34'),
(348, 'superAdmin', 'go to dietitians trash', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-28 11:12:35', '2020-05-28 11:12:35'),
(349, 'superAdmin', 'go to dietitians index', NULL, NULL, 1, 'App\\Admin', '{\"ip\":\"::1\"}', '2020-05-28 11:12:38', '2020-05-28 11:12:38');

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(11) DEFAULT 3,
  `active` tinyint(1) DEFAULT 0,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `role`, `active`, `password`, `phone`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin Laith Atik', 'latik@enbiosis.com', 0, 1, '$2y$10$1oIiWAP5qux4LlV3FINTi.I69hharSB2eJyulxP8L/1QXkCSmbdXC', NULL, 'G0Y9bzJZ8uXJQSRLxligsiPFyUw368xphUm6qrKi94xQHucSbp0OH7kW6svU', '2020-02-05 06:09:47', '2020-05-20 12:43:08', NULL),
(2, 'Laith Atik', 'lab@enbiosis.com', 1, 1, '$2y$10$ZjZ5wxzHZI9uuMlxeoU32.o6HHdf/dn1hYugZMtcBA8.DaDCjw4BC', '12332112321', NULL, '2020-05-23 13:33:51', '2020-05-23 13:33:51', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE `answers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kit_id` bigint(20) UNSIGNED NOT NULL,
  `question_id` bigint(20) NOT NULL,
  `answer` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`id`, `kit_id`, `question_id`, `answer`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1840, 554, 95, '\"2020-03-03\"', '2020-03-11 13:07:02', '2020-03-06 12:14:43', '2020-03-11 13:07:02'),
(1841, 554, 68, '\"Mesle\\u011finiz\"', '2020-03-11 13:07:02', '2020-03-06 12:14:43', '2020-03-11 13:07:02'),
(1842, 554, 69, '\"Kilonuz\"', '2020-03-11 13:07:02', '2020-03-06 12:14:43', '2020-03-11 13:07:02'),
(1843, 554, 70, '\"Kilonuz\"', '2020-03-11 13:07:02', '2020-03-06 12:14:43', '2020-03-11 13:07:02'),
(1844, 554, 71, '[{\"\\\"1\\\"\":null}]', '2020-03-11 13:07:02', '2020-03-06 12:14:43', '2020-03-11 13:07:02'),
(1845, 554, 72, '\"6 - Ya\\u015fad\\u0131\\u011f\\u0131n\\u0131z \\u015fehir\"', '2020-03-11 13:07:02', '2020-03-06 12:14:43', '2020-03-11 13:07:02'),
(1846, 554, 73, '\"\\u00dclke\"', '2020-03-11 13:07:02', '2020-03-06 12:14:43', '2020-03-11 13:07:02'),
(1847, 554, 74, '[{\"\\\"0\\\"\":\"Hangi y\\u0131llar aras\\u0131nda nerede y\"}]', '2020-03-11 13:07:02', '2020-03-06 12:14:43', '2020-03-11 13:07:02'),
(1848, 554, 75, '[{\"\\\"0\\\"\":null}]', '2020-03-11 13:07:02', '2020-03-06 12:14:43', '2020-03-11 13:07:02'),
(1849, 554, 76, '[{\"\\\"0\\\"\":null}]', '2020-03-11 13:07:02', '2020-03-06 12:14:43', '2020-03-11 13:07:02'),
(1850, 554, 77, '\"Hangi y\\u0131llar aras\\u0131nda nerede y\"', '2020-03-11 13:07:02', '2020-03-06 12:14:43', '2020-03-11 13:07:02'),
(1851, 554, 78, '[{\"\\\"2\\\"\":null}]', '2020-03-11 13:07:02', '2020-03-06 12:14:43', '2020-03-11 13:07:02'),
(1852, 554, 79, '[{\"\\\"2\\\"\":null}]', '2020-03-11 13:07:02', '2020-03-06 12:14:43', '2020-03-11 13:07:02'),
(1853, 554, 80, '[{\"\\\"0\\\"\":\"Hangi y\\u0131llar aras\\u0131nda nerede y\"}]', '2020-03-11 13:07:02', '2020-03-06 12:14:43', '2020-03-11 13:07:02'),
(1854, 554, 81, '[{\"\\\"0\\\"\":null}]', '2020-03-11 13:07:02', '2020-03-06 12:14:43', '2020-03-11 13:07:02'),
(1855, 554, 82, '[{\"\\\"0\\\"\":null}]', '2020-03-11 13:07:02', '2020-03-06 12:14:43', '2020-03-11 13:07:02'),
(1856, 554, 83, '[{\"\\\"1\\\"\":null}]', '2020-03-11 13:07:02', '2020-03-06 12:14:43', '2020-03-11 13:07:02'),
(1857, 554, 84, '[{\"\\\"1\\\"\":null}]', '2020-03-11 13:07:02', '2020-03-06 12:14:44', '2020-03-11 13:07:02'),
(1858, 554, 85, '[{\"\\\"0\\\"\":null}]', '2020-03-11 13:07:02', '2020-03-06 12:14:44', '2020-03-11 13:07:02'),
(1859, 554, 86, '[{\"\\\"4\\\"\":\"nedir ?\"}]', '2020-03-11 13:07:02', '2020-03-06 12:14:44', '2020-03-11 13:07:02'),
(1860, 554, 87, '[{\"\\\"0\\\"\":null}]', '2020-03-11 13:07:02', '2020-03-06 12:14:44', '2020-03-11 13:07:02'),
(1861, 554, 88, '[{\"\\\"1\\\"\":null}]', '2020-03-11 13:07:02', '2020-03-06 12:14:44', '2020-03-11 13:07:02'),
(1862, 554, 89, '[{\"\\\"1\\\"\":null}]', '2020-03-11 13:07:02', '2020-03-06 12:14:44', '2020-03-11 13:07:02'),
(1863, 554, 90, '[{\"\\\"3\\\"\":null}]', '2020-03-11 13:07:02', '2020-03-06 12:14:44', '2020-03-11 13:07:02'),
(1864, 554, 91, '[{\"\\\"2\\\"\":null}]', '2020-03-11 13:07:02', '2020-03-06 12:14:44', '2020-03-11 13:07:02'),
(1865, 554, 92, '[{\"\\\"1\\\"\":null}]', '2020-03-11 13:07:02', '2020-03-06 12:14:44', '2020-03-11 13:07:02'),
(1866, 554, 93, '[{\"\\\"1\\\"\":\"wadwadawd\"}]', '2020-03-11 13:07:02', '2020-03-06 12:14:44', '2020-03-11 13:07:02'),
(1867, 554, 96, '[{\"\\\"3\\\"\":\"awdawd\"}]', '2020-03-11 13:07:02', '2020-03-06 12:14:44', '2020-03-11 13:07:02'),
(1868, 554, 97, '[{\"\\\"3\\\"\":\"awdawd\"}]', '2020-03-11 13:07:02', '2020-03-06 12:14:44', '2020-03-11 13:07:02'),
(1869, 554, 99, '[{\"\\\"1\\\"\":null}]', '2020-03-11 13:07:02', '2020-03-06 12:14:44', '2020-03-11 13:07:02'),
(1870, 554, 100, '[{\"\\\"1\\\"\":null}]', '2020-03-11 13:07:02', '2020-03-06 12:14:44', '2020-03-11 13:07:02'),
(1871, 554, 101, '[{\"\\\"1\\\"\":null}]', '2020-03-11 13:07:02', '2020-03-06 12:14:44', '2020-03-11 13:07:02'),
(1872, 554, 102, '[{\"\\\"0\\\"\":null}]', '2020-03-11 13:07:02', '2020-03-06 12:14:44', '2020-03-11 13:07:02'),
(1873, 554, 103, '[{\"\\\"1\\\"\":null}]', '2020-03-11 13:07:02', '2020-03-06 12:14:44', '2020-03-11 13:07:02'),
(1874, 554, 104, '[{\"\\\"1\\\"\":\"awdawd\"}]', '2020-03-11 13:07:02', '2020-03-06 12:14:44', '2020-03-11 13:07:02'),
(1875, 554, 105, '[{\"\\\"0\\\"\":null}]', '2020-03-11 13:07:02', '2020-03-06 12:14:44', '2020-03-11 13:07:02'),
(1876, 554, 106, '[{\"\\\"3\\\"\":\"awdawd\"}]', '2020-03-11 13:07:02', '2020-03-06 12:14:44', '2020-03-11 13:07:02'),
(1877, 554, 107, '[{\"\\\"2\\\"\":null}]', '2020-03-11 13:07:02', '2020-03-06 12:14:44', '2020-03-11 13:07:02'),
(1878, 554, 108, '[{\"\\\"1\\\"\":\"awdawd\"}]', '2020-03-11 13:07:02', '2020-03-06 12:14:44', '2020-03-11 13:07:02'),
(1879, 554, 109, '[{\"\\\"0\\\"\":null}]', '2020-03-11 13:07:02', '2020-03-06 12:14:44', '2020-03-11 13:07:02'),
(1880, 554, 110, '[{\"\\\"2\\\"\":null}]', '2020-03-11 13:07:02', '2020-03-06 12:14:44', '2020-03-11 13:07:02'),
(1881, 554, 111, '[{\"\\\"1\\\"\":null}]', '2020-03-11 13:07:02', '2020-03-06 12:14:44', '2020-03-11 13:07:02'),
(1882, 554, 112, '\"<p>adawd awdawd<\\/p>\"', '2020-03-11 13:07:02', '2020-03-06 12:14:44', '2020-03-11 13:07:02'),
(1883, 554, 113, '[{\"\\\"0\\\"\":null},{\"\\\"1\\\"\":null},{\"\\\"2\\\"\":null},{\"\\\"3\\\"\":null},{\"\\\"4\\\"\":\"awdawd\"}]', '2020-03-11 13:07:02', '2020-03-06 12:14:44', '2020-03-11 13:07:02'),
(1884, 554, 114, '[{\"\\\"0\\\"\":null},{\"\\\"1\\\"\":null},{\"\\\"2\\\"\":null},{\"\\\"3\\\"\":null},{\"\\\"4\\\"\":\"wadawdaw\"}]', '2020-03-11 13:07:02', '2020-03-06 12:14:44', '2020-03-11 13:07:02'),
(1885, 554, 115, '[{\"\\\"1\\\"\":\"ngi besini t\\u00fcketinc\"}]', '2020-03-11 13:07:02', '2020-03-06 12:14:44', '2020-03-11 13:07:02'),
(1886, 554, 116, '[{\"\\\"2\\\"\":null}]', '2020-03-11 13:07:02', '2020-03-06 12:14:44', '2020-03-11 13:07:02'),
(1887, 554, 117, '[{\"\\\"0\\\"\":null}]', '2020-03-11 13:07:02', '2020-03-06 12:14:44', '2020-03-11 13:07:02'),
(1888, 554, 118, '[{\"\\\"1\\\"\":null}]', '2020-03-11 13:07:02', '2020-03-06 12:14:44', '2020-03-11 13:07:02'),
(1889, 554, 119, '[{\"\\\"3\\\"\":null}]', '2020-03-11 13:07:02', '2020-03-06 12:14:44', '2020-03-11 13:07:02'),
(1890, 554, 120, '[{\"\\\"2\\\"\":null}]', '2020-03-11 13:07:02', '2020-03-06 12:14:44', '2020-03-11 13:07:02'),
(1891, 554, 121, '[{\"\\\"2\\\"\":null}]', '2020-03-11 13:07:02', '2020-03-06 12:14:44', '2020-03-11 13:07:02'),
(1892, 554, 122, '[{\"\\\"1\\\"\":null}]', '2020-03-11 13:07:02', '2020-03-06 12:14:44', '2020-03-11 13:07:02'),
(1893, 554, 123, '[{\"\\\"4\\\"\":\"eesfsef\"}]', '2020-03-11 13:07:02', '2020-03-06 12:14:44', '2020-03-11 13:07:02'),
(1894, 554, 124, '[{\"\\\"1\\\"\":\"akviye\\/kimyasal t\\u00fcr\\u00fc\"}]', '2020-03-11 13:07:02', '2020-03-06 12:14:44', '2020-03-11 13:07:02'),
(1895, 554, 125, '[{\"\\\"1\\\"\":null}]', '2020-03-11 13:07:02', '2020-03-06 12:14:44', '2020-03-11 13:07:02'),
(1896, 554, 126, '[{\"\\\"0\\\"\":null}]', '2020-03-11 13:07:03', '2020-03-06 12:14:44', '2020-03-11 13:07:03'),
(1897, 554, 127, '[{\"\\\"1\\\"\":null}]', '2020-03-11 13:07:03', '2020-03-06 12:14:44', '2020-03-11 13:07:03'),
(1898, 554, 128, '[{\"\\\"3\\\"\":null}]', '2020-03-11 13:07:03', '2020-03-06 12:14:44', '2020-03-11 13:07:03'),
(1899, 554, 129, '[{\"\\\"0\\\"\":null}]', '2020-03-11 13:07:03', '2020-03-06 12:14:44', '2020-03-11 13:07:03'),
(1900, 554, 130, '[{\"\\\"0\\\"\":null}]', '2020-03-11 13:07:03', '2020-03-06 12:14:44', '2020-03-11 13:07:03'),
(1901, 554, 131, '[{\"\\\"1\\\"\":\"ada ortalama ka\\u00e7 ml ?\"}]', '2020-03-11 13:07:03', '2020-03-06 12:14:44', '2020-03-11 13:07:03'),
(1902, 554, 132, '[{\"\\\"2\\\"\":\"awdwd\"}]', '2020-03-11 13:07:03', '2020-03-06 12:14:44', '2020-03-11 13:07:03'),
(1903, 554, 133, '[{\"\\\"1\\\"\":\"awwd\"}]', '2020-03-11 13:07:03', '2020-03-06 12:14:44', '2020-03-11 13:07:03'),
(1904, 554, 134, '[{\"\\\"2\\\"\":null}]', '2020-03-11 13:07:03', '2020-03-06 12:14:44', '2020-03-11 13:07:03'),
(1905, 554, 135, '[{\"\\\"1\\\"\":null}]', '2020-03-11 13:07:03', '2020-03-06 12:14:44', '2020-03-11 13:07:03'),
(1906, 554, 136, '[{\"\\\"3\\\"\":null}]', '2020-03-11 13:07:03', '2020-03-06 12:14:44', '2020-03-11 13:07:03'),
(1907, 554, 137, '[{\"\\\"2\\\"\":null}]', '2020-03-11 13:07:03', '2020-03-06 12:14:44', '2020-03-11 13:07:03'),
(1908, 554, 138, '[{\"\\\"1\\\"\":null}]', '2020-03-11 13:07:03', '2020-03-06 12:14:44', '2020-03-11 13:07:03'),
(1909, 554, 139, '[{\"\\\"1\\\"\":null}]', '2020-03-11 13:07:03', '2020-03-06 12:14:44', '2020-03-11 13:07:03'),
(1910, 554, 140, '[{\"\\\"1\\\"\":null}]', '2020-03-11 13:07:03', '2020-03-06 12:14:44', '2020-03-11 13:07:03'),
(1911, 554, 141, '[{\"\\\"2\\\"\":null}]', '2020-03-11 13:07:03', '2020-03-06 12:14:44', '2020-03-11 13:07:03'),
(1912, 554, 142, '[{\"\\\"2\\\"\":null}]', '2020-03-11 13:07:03', '2020-03-06 12:14:44', '2020-03-11 13:07:03'),
(1913, 554, 143, '[{\"\\\"1\\\"\":null}]', '2020-03-11 13:07:03', '2020-03-06 12:14:44', '2020-03-11 13:07:03'),
(1914, 554, 144, '[{\"\\\"1\\\"\":\"wadawdawd\"}]', '2020-03-11 13:07:03', '2020-03-06 12:14:44', '2020-03-11 13:07:03'),
(1915, 554, 145, '[{\"\\\"1\\\"\":\"hangi y\\u00f6nt\"}]', '2020-03-11 13:07:03', '2020-03-06 12:14:44', '2020-03-11 13:07:03'),
(1916, 554, 146, '[{\"\\\"0\\\"\":\"hangi y\\u00f6nt hangi y\\u00f6nt hangi y\\u00f6nt hangi y\\u00f6nt hangi y\\u00f6nt\"}]', '2020-03-11 13:07:03', '2020-03-06 12:14:44', '2020-03-11 13:07:03'),
(1917, 577, 67, '\"05-02-1995\"', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1918, 577, 68, '\"Yazilim muh\"', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1919, 577, 69, '\"90\"', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1920, 577, 70, '\"169\"', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1921, 577, 71, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1922, 577, 72, '\"istanbul\"', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1923, 577, 73, '\"turkiye\"', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1924, 577, 74, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1925, 577, 75, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1926, 577, 76, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1927, 577, 77, '\"5000\"', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1928, 577, 78, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1929, 577, 79, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1930, 577, 80, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1931, 577, 81, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1932, 577, 82, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1933, 577, 83, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1934, 577, 84, '[{\"\\\"3\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1935, 577, 85, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1936, 577, 86, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1937, 577, 87, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1938, 577, 88, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1939, 577, 89, '[{\"\\\"3\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1940, 577, 90, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1941, 577, 91, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1942, 577, 92, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1943, 577, 93, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1944, 577, 96, '[{\"\\\"4\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1945, 577, 97, '[{\"\\\"4\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1946, 577, 98, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1947, 577, 99, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1948, 577, 100, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1949, 577, 101, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1950, 577, 102, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1951, 577, 103, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1952, 577, 104, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1953, 577, 105, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1954, 577, 106, '[{\"\\\"4\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1955, 577, 107, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1956, 577, 108, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1957, 577, 109, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1958, 577, 110, '[{\"\\\"4\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1959, 577, 111, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1960, 577, 112, '\"yok\"', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1961, 577, 113, '[{\"\\\"5\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1962, 577, 114, '[{\"\\\"5\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1963, 577, 115, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1964, 577, 116, '[{\"\\\"3\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1965, 577, 117, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1966, 577, 118, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1967, 577, 119, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1968, 577, 120, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1969, 577, 121, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1970, 577, 122, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1971, 577, 123, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1972, 577, 124, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1973, 577, 125, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1974, 577, 126, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1975, 577, 127, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1976, 577, 128, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1977, 577, 129, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1978, 577, 130, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1979, 577, 131, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1980, 577, 132, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1981, 577, 133, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1982, 577, 134, '[{\"\\\"2\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1983, 577, 135, '[{\"\\\"2\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1984, 577, 136, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1985, 577, 137, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1986, 577, 138, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1987, 577, 139, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1988, 577, 140, '[{\"\\\"2\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1989, 577, 141, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1990, 577, 142, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1991, 577, 143, '[{\"\\\"3\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1992, 577, 144, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1993, 577, 145, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1994, 577, 146, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 06:15:16', '2020-03-10 06:15:16'),
(1995, 575, 67, '\"awd\"', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(1996, 575, 68, '\"awd\"', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(1997, 575, 69, '\"awd\"', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(1998, 575, 70, '\"wad\"', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(1999, 575, 72, '\"awd\"', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2000, 575, 73, '\"awd\"', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2001, 575, 74, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2002, 575, 75, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2003, 575, 76, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2004, 575, 77, '\"adadw\"', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2005, 575, 78, '[{\"\\\"2\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2006, 575, 79, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2007, 575, 80, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2008, 575, 81, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2009, 575, 82, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2010, 575, 83, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2011, 575, 84, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2012, 575, 85, '[{\"\\\"3\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2013, 575, 86, '[{\"\\\"4\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2014, 575, 87, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2015, 575, 88, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2016, 575, 89, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2017, 575, 90, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2018, 575, 92, '[{\"\\\"2\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2019, 575, 93, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2020, 575, 96, '[{\"\\\"3\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2021, 575, 97, '[{\"\\\"2\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2022, 575, 98, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2023, 575, 99, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2024, 575, 100, '[{\"\\\"2\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2025, 575, 101, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2026, 575, 102, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2027, 575, 103, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2028, 575, 104, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2029, 575, 105, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2030, 575, 106, '[{\"\\\"3\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2031, 575, 107, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2032, 575, 108, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2033, 575, 109, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2034, 575, 110, '[{\"\\\"5\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2035, 575, 111, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2036, 575, 112, '\"awdawd\"', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2037, 575, 113, '[{\"\\\"0\\\"\":null},{\"\\\"1\\\"\":null},{\"\\\"3\\\"\":null},{\"\\\"4\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2038, 575, 114, '[{\"\\\"2\\\"\":null},{\"\\\"3\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2039, 575, 116, '[{\"\\\"3\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2040, 575, 118, '[{\"\\\"2\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2041, 575, 119, '[{\"\\\"3\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2042, 575, 120, '[{\"\\\"3\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2043, 575, 122, '[{\"\\\"2\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2044, 575, 123, '[{\"\\\"3\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2045, 575, 124, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2046, 575, 125, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2047, 575, 126, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2048, 575, 127, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2049, 575, 128, '[{\"\\\"4\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2050, 575, 129, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2051, 575, 130, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2052, 575, 132, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2053, 575, 133, '[{\"\\\"3\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2054, 575, 134, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2055, 575, 135, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2056, 575, 136, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2057, 575, 137, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2058, 575, 138, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2059, 575, 139, '[{\"\\\"4\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2060, 575, 140, '[{\"\\\"3\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2061, 575, 141, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2062, 575, 142, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2063, 575, 143, '[{\"\\\"3\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2064, 575, 144, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2065, 575, 145, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2066, 575, 146, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-10 12:36:49', '2020-03-10 12:36:49'),
(2067, 578, 67, '\"wad\"', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2068, 578, 68, '\"awd\"', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2069, 578, 69, '\"awd\"', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2070, 578, 70, '\"awd\"', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2071, 578, 71, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2072, 578, 72, '\"awd\"', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2073, 578, 73, '\"dwad\"', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2074, 578, 74, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2075, 578, 75, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2076, 578, 76, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2077, 578, 77, '\"awd\"', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2078, 578, 78, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2079, 578, 79, '[{\"\\\"2\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2080, 578, 80, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2081, 578, 81, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2082, 578, 83, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2083, 578, 84, '[{\"\\\"2\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2084, 578, 85, '[{\"\\\"2\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2085, 578, 86, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2086, 578, 87, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2087, 578, 88, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2088, 578, 89, '[{\"\\\"3\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2089, 578, 90, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2090, 578, 91, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2091, 578, 92, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2092, 578, 93, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2093, 578, 96, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2094, 578, 97, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2095, 578, 98, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2096, 578, 99, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2097, 578, 100, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2098, 578, 101, '[{\"\\\"2\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2099, 578, 102, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2100, 578, 105, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2101, 578, 106, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2102, 578, 107, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2103, 578, 108, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2104, 578, 109, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2105, 578, 110, '[{\"\\\"3\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2106, 578, 111, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2107, 578, 112, '\"awdawd\"', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2108, 578, 113, '[{\"\\\"1\\\"\":null},{\"\\\"2\\\"\":null},{\"\\\"3\\\"\":null},{\"\\\"4\\\"\":null},{\"\\\"5\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2109, 578, 114, '[{\"\\\"1\\\"\":null},{\"\\\"2\\\"\":null},{\"\\\"3\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2110, 578, 115, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2111, 578, 116, '[{\"\\\"3\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2112, 578, 117, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2113, 578, 118, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2114, 578, 119, '[{\"\\\"3\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2115, 578, 120, '[{\"\\\"2\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2116, 578, 121, '[{\"\\\"3\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2117, 578, 122, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2118, 578, 123, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2119, 578, 124, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2120, 578, 125, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2121, 578, 126, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2122, 578, 127, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2123, 578, 128, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2124, 578, 129, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2125, 578, 130, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2126, 578, 131, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2127, 578, 132, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2128, 578, 133, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2129, 578, 134, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2130, 578, 135, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2131, 578, 136, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2132, 578, 137, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2133, 578, 138, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2134, 578, 139, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2135, 578, 140, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2136, 578, 141, '[{\"\\\"4\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2137, 578, 142, '[{\"\\\"4\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2138, 578, 143, '[{\"\\\"3\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2139, 578, 144, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2140, 578, 145, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2141, 578, 146, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-11 11:54:12', '2020-03-11 11:54:12'),
(2142, 554, 95, '\"2020-03-03\"', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2143, 554, 68, '\"Mesle\\u011finiz\"', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2144, 554, 69, '\"Kilonuz\"', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2145, 554, 70, '\"Kilonuz\"', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2146, 554, 71, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2147, 554, 72, '\"6 - Ya\\u015fad\\u0131\\u011f\\u0131n\\u0131z \\u015fehir\"', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2148, 554, 73, '\"\\u00dclke\"', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2149, 554, 74, '[{\"\\\"0\\\"\":\"Hangi y\\u0131llar aras\\u0131nda nerede y\"}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2150, 554, 75, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2151, 554, 76, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2152, 554, 77, '\"Hangi y\\u0131llar aras\\u0131nda nerede y\"', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2153, 554, 78, '[{\"\\\"2\\\"\":null}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2154, 554, 79, '[{\"\\\"2\\\"\":null}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2155, 554, 80, '[{\"\\\"0\\\"\":\"Hangi y\\u0131llar aras\\u0131nda nerede y\"}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2156, 554, 81, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2157, 554, 82, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2158, 554, 83, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2159, 554, 84, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2160, 554, 85, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2161, 554, 86, '[{\"\\\"4\\\"\":\"nedir ?\"}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2162, 554, 87, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2163, 554, 88, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2164, 554, 89, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2165, 554, 90, '[{\"\\\"3\\\"\":null}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2166, 554, 91, '[{\"\\\"2\\\"\":null}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2167, 554, 92, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2168, 554, 93, '[{\"\\\"1\\\"\":\"wadwadawd\"}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2169, 554, 96, '[{\"\\\"3\\\"\":\"awdawd\"}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2170, 554, 97, '[{\"\\\"3\\\"\":\"awdawd\"}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2171, 554, 99, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2172, 554, 100, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2173, 554, 101, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2174, 554, 102, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2175, 554, 103, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2176, 554, 104, '[{\"\\\"1\\\"\":\"awdawd\"}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2177, 554, 105, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2178, 554, 106, '[{\"\\\"3\\\"\":\"awdawd\"}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2179, 554, 107, '[{\"\\\"2\\\"\":null}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2180, 554, 108, '[{\"\\\"1\\\"\":\"awdawd\"}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2181, 554, 109, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2182, 554, 110, '[{\"\\\"2\\\"\":null}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2183, 554, 111, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2184, 554, 112, '\"<p>adawd awdawd<\\/p>\"', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2185, 554, 113, '[{\"\\\"0\\\"\":null},{\"\\\"1\\\"\":null},{\"\\\"2\\\"\":null},{\"\\\"3\\\"\":null},{\"\\\"4\\\"\":\"awdawd\"}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2186, 554, 114, '[{\"\\\"0\\\"\":null},{\"\\\"1\\\"\":null},{\"\\\"2\\\"\":null},{\"\\\"3\\\"\":null},{\"\\\"4\\\"\":\"wadawdaw\"}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2187, 554, 115, '[{\"\\\"1\\\"\":\"ngi besini t\\u00fcketinc\"}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2188, 554, 116, '[{\"\\\"2\\\"\":null}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2189, 554, 117, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2190, 554, 118, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2191, 554, 119, '[{\"\\\"3\\\"\":null}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2192, 554, 120, '[{\"\\\"2\\\"\":null}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2193, 554, 121, '[{\"\\\"2\\\"\":null}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2194, 554, 122, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2195, 554, 123, '[{\"\\\"4\\\"\":\"eesfsef\"}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2196, 554, 124, '[{\"\\\"1\\\"\":\"akviye\\/kimyasal t\\u00fcr\\u00fc\"}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2197, 554, 125, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 13:07:02', '2020-03-11 13:07:02'),
(2198, 554, 126, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-11 13:07:03', '2020-03-11 13:07:03'),
(2199, 554, 127, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 13:07:03', '2020-03-11 13:07:03'),
(2200, 554, 128, '[{\"\\\"3\\\"\":null}]', NULL, '2020-03-11 13:07:03', '2020-03-11 13:07:03'),
(2201, 554, 129, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-11 13:07:03', '2020-03-11 13:07:03'),
(2202, 554, 130, '[{\"\\\"0\\\"\":null}]', NULL, '2020-03-11 13:07:03', '2020-03-11 13:07:03'),
(2203, 554, 131, '[{\"\\\"1\\\"\":\"ada ortalama ka\\u00e7 ml ?\"}]', NULL, '2020-03-11 13:07:03', '2020-03-11 13:07:03'),
(2204, 554, 132, '[{\"\\\"2\\\"\":\"awdwd\"}]', NULL, '2020-03-11 13:07:03', '2020-03-11 13:07:03'),
(2205, 554, 133, '[{\"\\\"1\\\"\":\"awwd\"}]', NULL, '2020-03-11 13:07:03', '2020-03-11 13:07:03'),
(2206, 554, 134, '[{\"\\\"2\\\"\":null}]', NULL, '2020-03-11 13:07:03', '2020-03-11 13:07:03'),
(2207, 554, 135, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 13:07:03', '2020-03-11 13:07:03'),
(2208, 554, 136, '[{\"\\\"3\\\"\":null}]', NULL, '2020-03-11 13:07:03', '2020-03-11 13:07:03'),
(2209, 554, 137, '[{\"\\\"2\\\"\":null}]', NULL, '2020-03-11 13:07:03', '2020-03-11 13:07:03'),
(2210, 554, 138, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 13:07:03', '2020-03-11 13:07:03'),
(2211, 554, 139, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 13:07:03', '2020-03-11 13:07:03'),
(2212, 554, 140, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 13:07:03', '2020-03-11 13:07:03'),
(2213, 554, 141, '[{\"\\\"2\\\"\":null}]', NULL, '2020-03-11 13:07:03', '2020-03-11 13:07:03'),
(2214, 554, 142, '[{\"\\\"2\\\"\":null}]', NULL, '2020-03-11 13:07:03', '2020-03-11 13:07:03'),
(2215, 554, 143, '[{\"\\\"1\\\"\":null}]', NULL, '2020-03-11 13:07:03', '2020-03-11 13:07:03'),
(2216, 554, 144, '[{\"\\\"1\\\"\":\"wadawdawd\"}]', NULL, '2020-03-11 13:07:03', '2020-03-11 13:07:03'),
(2217, 554, 145, '[{\"\\\"1\\\"\":\"hangi y\\u00f6nt\"}]', NULL, '2020-03-11 13:07:03', '2020-03-11 13:07:03'),
(2218, 554, 146, '[{\"\\\"0\\\"\":\"hangi y\\u00f6nt hangi y\\u00f6nt hangi y\\u00f6nt hangi y\\u00f6nt hangi y\\u00f6nt\"}]', NULL, '2020-03-11 13:07:03', '2020-03-11 13:07:03'),
(2223, 582, 71, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:47:25', '2020-04-09 09:47:25'),
(2224, 582, 72, '\"asda\"', NULL, '2020-04-09 09:47:29', '2020-04-09 09:47:29'),
(2225, 582, 73, '\"asda\"', NULL, '2020-04-09 09:47:31', '2020-04-09 09:47:31'),
(2226, 582, 74, '[{\"\\\"1\\\"\":null}]', NULL, '2020-04-09 09:47:34', '2020-04-09 09:47:34'),
(2227, 582, 75, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:47:35', '2020-04-09 09:47:35'),
(2228, 582, 76, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:47:36', '2020-04-09 09:47:36'),
(2229, 582, 77, '\"asda\"', NULL, '2020-04-09 09:47:39', '2020-04-09 09:47:39'),
(2230, 582, 78, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:47:41', '2020-04-09 09:47:41'),
(2231, 582, 79, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:47:42', '2020-04-09 09:47:42'),
(2232, 582, 80, '[{\"\\\"0\\\"\":\"rwer\"}]', NULL, '2020-04-09 09:47:47', '2020-04-09 09:47:47'),
(2233, 582, 81, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:47:49', '2020-04-09 09:47:49'),
(2234, 582, 82, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:47:50', '2020-04-09 09:47:50'),
(2235, 582, 83, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:47:52', '2020-04-09 09:47:52'),
(2236, 582, 84, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:47:53', '2020-04-09 09:47:53'),
(2237, 582, 85, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:47:55', '2020-04-09 09:47:55'),
(2238, 582, 86, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:47:56', '2020-04-09 09:47:56'),
(2239, 582, 87, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:47:57', '2020-04-09 09:47:57'),
(2240, 582, 88, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:47:59', '2020-04-09 09:47:59'),
(2241, 582, 89, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:48:01', '2020-04-09 09:48:01'),
(2242, 582, 90, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:48:02', '2020-04-09 09:48:02'),
(2243, 582, 125, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:48:05', '2020-04-09 09:48:05'),
(2244, 582, 126, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:48:07', '2020-04-09 09:48:07'),
(2245, 582, 127, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:48:08', '2020-04-09 09:48:08'),
(2246, 582, 128, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:48:10', '2020-04-09 09:48:10'),
(2247, 582, 129, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:48:11', '2020-04-09 09:48:11'),
(2248, 582, 130, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:48:18', '2020-04-09 09:48:18'),
(2249, 582, 131, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:48:20', '2020-04-09 09:48:20'),
(2250, 582, 132, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:48:21', '2020-04-09 09:48:21'),
(2251, 582, 133, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:48:23', '2020-04-09 09:48:23'),
(2252, 582, 134, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:48:24', '2020-04-09 09:48:24'),
(2253, 582, 135, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:48:26', '2020-04-09 09:48:26'),
(2254, 582, 136, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:48:27', '2020-04-09 09:48:27'),
(2255, 582, 137, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:48:29', '2020-04-09 09:48:29'),
(2256, 582, 138, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:48:30', '2020-04-09 09:48:30'),
(2257, 582, 139, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:48:32', '2020-04-09 09:48:32'),
(2258, 582, 140, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:48:33', '2020-04-09 09:48:33'),
(2259, 582, 141, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:48:35', '2020-04-09 09:48:35'),
(2260, 582, 142, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:48:36', '2020-04-09 09:48:36'),
(2261, 582, 143, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:48:38', '2020-04-09 09:48:38'),
(2262, 582, 144, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:48:40', '2020-04-09 09:48:40'),
(2263, 582, 145, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:48:41', '2020-04-09 09:48:41'),
(2264, 582, 146, '[{\"\\\"1\\\"\":null}]', NULL, '2020-04-09 09:48:44', '2020-04-09 09:48:44'),
(2265, 582, 91, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:48:49', '2020-04-09 09:48:49'),
(2266, 582, 92, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:48:50', '2020-04-09 09:48:50'),
(2267, 582, 93, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:48:52', '2020-04-09 09:48:52'),
(2268, 582, 96, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:48:53', '2020-04-09 09:48:53'),
(2269, 582, 97, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:48:55', '2020-04-09 09:48:55'),
(2270, 582, 98, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:48:56', '2020-04-09 09:48:56'),
(2271, 582, 99, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:48:57', '2020-04-09 09:48:57'),
(2272, 582, 100, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:48:58', '2020-04-09 09:48:58'),
(2273, 582, 101, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:49:00', '2020-04-09 09:49:00'),
(2274, 582, 102, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:49:02', '2020-04-09 09:49:02'),
(2275, 582, 103, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:49:06', '2020-04-09 09:49:06'),
(2276, 582, 104, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:51:57', '2020-04-09 09:51:57'),
(2277, 582, 105, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:51:59', '2020-04-09 09:51:59'),
(2278, 582, 106, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:52:00', '2020-04-09 09:52:00'),
(2279, 582, 107, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:52:02', '2020-04-09 09:52:02'),
(2280, 582, 108, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:52:03', '2020-04-09 09:52:03'),
(2281, 582, 109, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:52:05', '2020-04-09 09:52:05'),
(2282, 582, 110, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:52:07', '2020-04-09 09:52:07'),
(2283, 582, 111, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 09:52:08', '2020-04-09 09:52:08'),
(2284, 582, 112, '\"asda\"', NULL, '2020-04-09 09:52:11', '2020-04-09 09:52:11'),
(2285, 582, 113, '[{\"\\\"0\\\"\":null},{\"\\\"5\\\"\":null}]', NULL, '2020-04-09 10:02:26', '2020-04-09 10:02:26'),
(2286, 582, 114, '[{\"\\\"1\\\"\":null},{\"\\\"2\\\"\":null}]', NULL, '2020-04-09 10:02:28', '2020-04-09 10:02:28'),
(2287, 582, 115, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:02:30', '2020-04-09 10:02:30'),
(2288, 582, 116, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:02:33', '2020-04-09 10:02:33'),
(2289, 582, 117, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:02:35', '2020-04-09 10:02:35'),
(2290, 582, 118, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:02:36', '2020-04-09 10:02:36'),
(2291, 582, 119, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:02:37', '2020-04-09 10:02:37'),
(2292, 582, 120, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:02:39', '2020-04-09 10:02:39'),
(2293, 582, 121, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:02:41', '2020-04-09 10:02:41'),
(2294, 582, 122, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:02:43', '2020-04-09 10:02:43'),
(2295, 582, 123, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:02:45', '2020-04-09 10:02:45'),
(2296, 582, 124, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:02:47', '2020-04-09 10:02:47'),
(2297, 607, 67, '\"1999-09-30\"', '2020-04-09 14:41:33', '2020-04-09 10:29:46', '2020-04-09 14:41:33'),
(2298, 607, 68, '\"asda\"', '2020-04-09 14:41:34', '2020-04-09 10:29:49', '2020-04-09 14:41:34'),
(2299, 607, 69, '\"asda\"', '2020-04-09 10:30:52', '2020-04-09 10:29:51', '2020-04-09 10:30:52'),
(2300, 607, 70, '\"asda\"', '2020-04-09 10:34:50', '2020-04-09 10:29:55', '2020-04-09 10:34:50'),
(2303, 607, 71, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:34:52', '2020-04-09 10:34:52'),
(2304, 607, 72, '\"sdasda\"', NULL, '2020-04-09 10:34:56', '2020-04-09 10:34:56'),
(2305, 607, 73, '\"asda\"', NULL, '2020-04-09 10:34:58', '2020-04-09 10:34:58'),
(2306, 607, 74, '[{\"\\\"1\\\"\":null}]', NULL, '2020-04-09 10:35:01', '2020-04-09 10:35:01'),
(2307, 607, 75, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:35:06', '2020-04-09 10:35:06'),
(2308, 607, 76, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:35:07', '2020-04-09 10:35:07'),
(2309, 607, 77, '\"ewrw\"', NULL, '2020-04-09 10:35:10', '2020-04-09 10:35:10'),
(2310, 607, 78, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:35:11', '2020-04-09 10:35:11'),
(2311, 607, 79, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:35:13', '2020-04-09 10:35:13'),
(2312, 607, 80, '[{\"\\\"0\\\"\":\"asda\"}]', NULL, '2020-04-09 10:35:17', '2020-04-09 10:35:17'),
(2313, 607, 81, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:35:19', '2020-04-09 10:35:19'),
(2314, 607, 82, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:35:21', '2020-04-09 10:35:21'),
(2315, 607, 83, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:35:23', '2020-04-09 10:35:23'),
(2316, 607, 84, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:35:24', '2020-04-09 10:35:24'),
(2317, 607, 85, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:35:26', '2020-04-09 10:35:26'),
(2318, 607, 86, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:35:27', '2020-04-09 10:35:27'),
(2319, 607, 87, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:35:29', '2020-04-09 10:35:29'),
(2320, 607, 88, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:35:30', '2020-04-09 10:35:30'),
(2321, 607, 89, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:35:31', '2020-04-09 10:35:31'),
(2322, 607, 90, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:35:33', '2020-04-09 10:35:33'),
(2323, 607, 91, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:35:35', '2020-04-09 10:35:35'),
(2324, 607, 92, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:35:37', '2020-04-09 10:35:37'),
(2325, 607, 93, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:35:38', '2020-04-09 10:35:38'),
(2326, 607, 96, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:35:40', '2020-04-09 10:35:40'),
(2327, 607, 97, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:35:41', '2020-04-09 10:35:41'),
(2328, 607, 98, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:35:42', '2020-04-09 10:35:42'),
(2329, 607, 99, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:35:44', '2020-04-09 10:35:44'),
(2330, 607, 100, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:35:45', '2020-04-09 10:35:45'),
(2331, 607, 101, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:35:46', '2020-04-09 10:35:46'),
(2332, 607, 102, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:35:48', '2020-04-09 10:35:48'),
(2333, 607, 103, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:35:49', '2020-04-09 10:35:49'),
(2334, 607, 104, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:35:50', '2020-04-09 10:35:50'),
(2335, 607, 105, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:35:51', '2020-04-09 10:35:51'),
(2336, 607, 106, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:35:53', '2020-04-09 10:35:53'),
(2337, 607, 107, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:35:55', '2020-04-09 10:35:55'),
(2338, 607, 108, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:36:01', '2020-04-09 10:36:01'),
(2339, 607, 109, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:36:02', '2020-04-09 10:36:02'),
(2340, 607, 110, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:36:06', '2020-04-09 10:36:06'),
(2341, 607, 111, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:36:07', '2020-04-09 10:36:07'),
(2342, 607, 112, '\"qwe\"', NULL, '2020-04-09 10:36:10', '2020-04-09 10:36:10'),
(2343, 607, 125, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:36:25', '2020-04-09 10:36:25'),
(2344, 607, 126, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:36:26', '2020-04-09 10:36:26'),
(2345, 607, 127, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:36:28', '2020-04-09 10:36:28'),
(2346, 607, 128, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:36:30', '2020-04-09 10:36:30'),
(2347, 607, 129, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:36:33', '2020-04-09 10:36:33'),
(2348, 607, 130, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:36:34', '2020-04-09 10:36:34'),
(2349, 607, 131, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:36:35', '2020-04-09 10:36:35'),
(2350, 607, 132, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:36:37', '2020-04-09 10:36:37'),
(2351, 607, 133, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:36:39', '2020-04-09 10:36:39'),
(2352, 607, 134, '[{\"\\\"4\\\"\":null}]', NULL, '2020-04-09 10:36:40', '2020-04-09 10:36:40'),
(2353, 607, 135, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:36:42', '2020-04-09 10:36:42'),
(2354, 607, 136, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:36:43', '2020-04-09 10:36:43');
INSERT INTO `answers` (`id`, `kit_id`, `question_id`, `answer`, `deleted_at`, `created_at`, `updated_at`) VALUES
(2355, 607, 137, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:36:44', '2020-04-09 10:36:44'),
(2356, 607, 138, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:36:46', '2020-04-09 10:36:46'),
(2357, 607, 139, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:36:48', '2020-04-09 10:36:48'),
(2358, 607, 140, '[{\"\\\"3\\\"\":null}]', NULL, '2020-04-09 10:36:50', '2020-04-09 10:36:50'),
(2359, 607, 141, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:36:52', '2020-04-09 10:36:52'),
(2360, 607, 142, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:36:54', '2020-04-09 10:36:54'),
(2361, 607, 143, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:36:56', '2020-04-09 10:36:56'),
(2362, 607, 144, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:36:57', '2020-04-09 10:36:57'),
(2363, 607, 145, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:36:59', '2020-04-09 10:36:59'),
(2364, 607, 146, '[{\"\\\"1\\\"\":null}]', NULL, '2020-04-09 10:37:01', '2020-04-09 10:37:01'),
(2365, 607, 116, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:37:04', '2020-04-09 10:37:04'),
(2366, 607, 117, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:37:05', '2020-04-09 10:37:05'),
(2367, 607, 118, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:37:06', '2020-04-09 10:37:06'),
(2368, 607, 119, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:37:08', '2020-04-09 10:37:08'),
(2369, 607, 120, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:37:09', '2020-04-09 10:37:09'),
(2370, 607, 121, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:37:11', '2020-04-09 10:37:11'),
(2371, 607, 122, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:37:12', '2020-04-09 10:37:12'),
(2373, 607, 124, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:37:15', '2020-04-09 10:37:15'),
(2374, 607, 113, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:37:18', '2020-04-09 10:37:18'),
(2375, 607, 114, '[{\"\\\"2\\\"\":null},{\"\\\"1\\\"\":null},{\"\\\"3\\\"\":null}]', NULL, '2020-04-09 10:37:22', '2020-04-09 10:37:22'),
(2376, 607, 115, '[{\"\\\"0\\\"\":null}]', NULL, '2020-04-09 10:37:24', '2020-04-09 10:37:24'),
(2377, 607, 67, '\"1999-09-30\"', '2020-04-09 14:44:43', '2020-04-09 14:41:33', '2020-04-09 14:44:43'),
(2378, 607, 68, '\"asda\"', '2020-04-09 14:41:42', '2020-04-09 14:41:34', '2020-04-09 14:41:42'),
(2379, 607, 68, '\"asda\"', '2020-04-09 14:44:44', '2020-04-09 14:41:42', '2020-04-09 14:44:44'),
(2380, 607, 69, '\"12341231\"', '2020-04-09 14:44:45', '2020-04-09 14:42:18', '2020-04-09 14:44:45'),
(2381, 607, 67, '\"1999-09-30\"', '2020-04-09 14:45:36', '2020-04-09 14:44:43', '2020-04-09 14:45:36'),
(2382, 607, 68, '\"asda\"', '2020-04-09 14:45:41', '2020-04-09 14:44:44', '2020-04-09 14:45:41'),
(2385, 607, 67, '\"1999-09-30\"', '2020-04-09 14:45:36', '2020-04-09 14:45:36', '2020-04-09 14:45:36'),
(2386, 607, 67, '\"1999-09-30\"', '2020-04-09 14:55:42', '2020-04-09 14:45:36', '2020-04-09 14:55:42'),
(2387, 607, 68, '\"asda\"', '2020-04-09 14:45:41', '2020-04-09 14:45:41', '2020-04-09 14:45:41'),
(2388, 607, 68, '\"asda\"', '2020-04-09 14:55:49', '2020-04-09 14:45:41', '2020-04-09 14:55:49'),
(2389, 607, 67, '\"1999-09-30\"', '2020-04-09 14:55:42', '2020-04-09 14:55:42', '2020-04-09 14:55:42'),
(2390, 607, 67, '\"1999-09-30\"', '2020-04-09 14:58:01', '2020-04-09 14:55:42', '2020-04-09 14:58:01'),
(2391, 607, 68, '\"asda\"', '2020-04-09 14:58:03', '2020-04-09 14:55:49', '2020-04-09 14:58:03'),
(2392, 607, 69, '\"12\"', '2020-04-09 14:55:51', '2020-04-09 14:55:51', '2020-04-09 14:55:51'),
(2393, 607, 69, '\"12\"', '2020-04-09 14:58:05', '2020-04-09 14:55:51', '2020-04-09 14:58:05'),
(2394, 607, 67, '\"1999-09-30\"', '2020-04-09 14:58:39', '2020-04-09 14:58:01', '2020-04-09 14:58:39'),
(2395, 607, 68, '\"asda\"', '2020-04-09 14:58:40', '2020-04-09 14:58:03', '2020-04-09 14:58:40'),
(2396, 607, 69, '\"12\"', '2020-04-09 14:58:42', '2020-04-09 14:58:05', '2020-04-09 14:58:42'),
(2397, 607, 67, '\"1999-09-30\"', '2020-04-09 15:00:02', '2020-04-09 14:58:39', '2020-04-09 15:00:02'),
(2398, 607, 68, '\"asda\"', '2020-04-09 15:00:04', '2020-04-09 14:58:40', '2020-04-09 15:00:04'),
(2399, 607, 69, '\"12\"', '2020-04-09 15:00:05', '2020-04-09 14:58:42', '2020-04-09 15:00:05'),
(2400, 607, 67, '\"1999-09-30\"', '2020-04-09 15:01:22', '2020-04-09 15:00:02', '2020-04-09 15:01:22'),
(2401, 607, 68, '\"asda\"', '2020-04-09 15:14:50', '2020-04-09 15:00:04', '2020-04-09 15:14:50'),
(2402, 607, 69, '\"12\"', '2020-04-09 15:00:11', '2020-04-09 15:00:05', '2020-04-09 15:00:11'),
(2403, 607, 69, '\"12\"', '2020-04-09 15:14:52', '2020-04-09 15:00:11', '2020-04-09 15:14:52'),
(2405, 607, 67, '\"1999-09-30\"', '2020-04-09 15:01:22', '2020-04-09 15:01:22', '2020-04-09 15:01:22'),
(2406, 607, 67, '\"1999-09-30\"', '2020-04-09 15:14:48', '2020-04-09 15:01:22', '2020-04-09 15:14:48'),
(2407, 607, 67, '\"1999-09-30\"', '2020-05-13 18:30:14', '2020-04-09 15:14:48', '2020-05-13 18:30:14'),
(2408, 607, 68, '\"asda\"', '2020-05-13 18:30:15', '2020-04-09 15:14:50', '2020-05-13 18:30:15'),
(2410, 607, 70, '\"324\"', NULL, '2020-04-09 15:14:54', '2020-04-09 15:14:54'),
(2412, 582, 67, '\"1999-09-30\"', '2020-04-13 13:51:52', '2020-04-13 11:11:03', '2020-04-13 13:51:52'),
(2413, 582, 67, '\"1999-09-30\"', '2020-04-13 15:40:00', '2020-04-13 13:51:52', '2020-04-13 15:40:00'),
(2414, 582, 68, '\"asdas\"', '2020-04-13 15:40:38', '2020-04-13 13:51:55', '2020-04-13 15:40:38'),
(2415, 582, 67, '\"1999-09-30\"', '2020-04-13 15:40:37', '2020-04-13 15:40:00', '2020-04-13 15:40:37'),
(2416, 582, 67, '\"1999-09-30\"', '2020-04-13 15:44:17', '2020-04-13 15:40:37', '2020-04-13 15:44:17'),
(2417, 582, 68, '\"asdas\"', '2020-04-13 15:44:19', '2020-04-13 15:40:38', '2020-04-13 15:44:19'),
(2420, 582, 68, '\"asdas\"', NULL, '2020-04-13 15:44:19', '2020-04-13 15:44:19'),
(2423, 582, 67, '\"12-11-2121\"', NULL, '2020-05-06 19:24:01', '2020-05-06 19:24:01'),
(2424, 582, 69, '\"1231\"', NULL, '2020-05-06 19:25:06', '2020-05-06 19:25:06'),
(2425, 582, 70, '\"1231\"', NULL, '2020-05-06 19:25:20', '2020-05-06 19:25:20'),
(2426, 607, 67, '\"1999-09-30\"', NULL, '2020-05-13 18:30:14', '2020-05-13 18:30:14'),
(2427, 607, 68, '\"asda\"', NULL, '2020-05-13 18:30:15', '2020-05-13 18:30:15'),
(2428, 607, 69, '\"1312\"', NULL, '2020-05-13 18:30:21', '2020-05-13 18:30:21'),
(2429, 607, 123, '[{\"\\\"0\\\"\":null}]', NULL, '2020-05-13 18:30:29', '2020-05-13 18:30:29');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `city` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `city`, `created_at`, `updated_at`) VALUES
(1, 'Adana', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(2, 'Adıyaman', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(3, 'Afyonkarahisar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(4, 'Ağrı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(5, 'Aksaray', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(6, 'Amasya', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(7, 'Ankara', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(8, 'Antalya', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(9, 'Ardahan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(10, 'Artvin', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(11, 'Aydın', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(12, 'Balıkesir', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(13, 'Bartın', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(14, 'Batman', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(15, 'Bayburt', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(16, 'Bilecik', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(17, 'Bingöl', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(18, 'Bitlis', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(19, 'Bolu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(20, 'Burdur', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(21, 'Bursa', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(22, 'Çanakkale', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(23, 'Çankırı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(24, 'Çorum', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(25, 'Denizli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(26, 'Diyarbakır', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(27, 'Düzce', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(28, 'Edirne', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(29, 'Elazığ', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(30, 'Erzincan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(31, 'Erzurum', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(32, 'Eskişehir', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(33, 'Gaziantep', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(34, 'Giresun', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(35, 'Gümüşhane', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(36, 'Hakkari', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(37, 'Hatay', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(38, 'Iğdır', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(39, 'Isparta', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(40, 'İstanbul', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(41, 'İzmir', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(42, 'Kahramanmaraş', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(43, 'Karabük', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(44, 'Karaman', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(45, 'Kars', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(46, 'Kastamonu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(47, 'Kayseri', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(48, 'Kırıkkale', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(49, 'Kırklareli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(50, 'Kırşehir', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(51, 'Kilis', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(52, 'Kocaeli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(53, 'Konya', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(54, 'Kütahya', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(55, 'Malatya', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(56, 'Manisa', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(57, 'Mardin', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(58, 'Mersin', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(59, 'Muğla', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(60, 'Muş', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(61, 'Nevşehir', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(62, 'Niğde', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(63, 'Ordu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(64, 'Osmaniye', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(65, 'Rize', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(66, 'Sakarya', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(67, 'Samsun', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(68, 'Siirt', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(69, 'Sinop', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(70, 'Sivas', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(71, 'Şanlıurfa', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(72, 'Şırnak', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(73, 'Tekirdağ', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(74, 'Tokat', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(75, 'Trabzon', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(76, 'Tunceli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(77, 'Uşak', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(78, 'Van', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(79, 'Yalova', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(80, 'Yozgat', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(81, 'Zonguldak', '2020-04-01 11:23:12', '2020-04-01 11:23:12');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mesaj` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `subject`, `mesaj`, `created_at`, `updated_at`) VALUES
(1, 'Laith Atik', 'laythateek@gmail.com', '123321', '123321123wad d awdawd wad', '2020-05-27 14:51:41', '2020-05-27 14:51:41');

-- --------------------------------------------------------

--
-- Table structure for table `counties`
--

CREATE TABLE `counties` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `city_id` bigint(20) NOT NULL,
  `county` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `counties`
--

INSERT INTO `counties` (`id`, `city_id`, `county`, `created_at`, `updated_at`) VALUES
(1, 1, 'Aladağ', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(2, 1, 'Ceyhan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(3, 1, 'Çukurova', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(4, 1, 'Feke', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(5, 1, 'İmamoğlu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(6, 1, 'Karaisalı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(7, 1, 'Karataş', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(8, 1, 'Kozan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(9, 1, 'Pozantı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(10, 1, 'Saimbeyli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(11, 1, 'Sarıçam', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(12, 1, 'Seyhan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(13, 1, 'Tufanbeyli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(14, 1, 'Yumurtalık', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(15, 1, 'Yüreğir', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(16, 2, 'Besni', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(17, 2, 'Çelikhan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(18, 2, 'Gerger', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(19, 2, 'Gölbaşı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(20, 2, 'Kahta', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(21, 2, 'Merkez', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(22, 2, 'Samsat', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(23, 2, 'Sincik', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(24, 2, 'Tut', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(25, 3, 'Başmakçı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(26, 3, 'Bayat', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(27, 3, 'Bolvadin', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(28, 3, 'Çay', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(29, 3, 'Çobanlar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(30, 3, 'Dazkırı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(31, 3, 'Dinar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(32, 3, 'Emirdağ', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(33, 3, 'Evciler', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(34, 3, 'Hocalar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(35, 3, 'İhsaniye', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(36, 3, 'İscehisar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(37, 3, 'Kızılören', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(38, 3, 'Sandıklı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(39, 3, 'Sinanpaşa', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(40, 3, 'Sultandağı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(41, 3, 'Şuhut', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(42, 4, 'Diyadin', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(43, 4, 'Doğubayazıt', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(44, 4, 'Eleşkirt', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(45, 4, 'Hamur', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(46, 4, 'Patnos', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(47, 4, 'Taşlıçay', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(48, 4, 'Tutak', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(49, 5, 'Ağaçören', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(50, 5, 'Eskil', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(51, 5, 'Gülağaç', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(52, 5, 'Güzelyurt', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(53, 5, 'Ortaköy', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(54, 5, 'Sarıyahşi', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(55, 6, 'Göynücek', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(56, 6, 'Gümüşhacıköy', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(57, 6, 'Hamamözü', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(58, 6, 'Merzifon', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(59, 6, 'Suluova', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(60, 6, 'Taşova', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(61, 7, 'Akyurt', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(62, 7, 'Altındağ', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(63, 7, 'Ayaş', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(64, 7, 'Bala', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(65, 7, 'Beypazarı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(66, 7, 'Çamlıdere', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(67, 7, 'Çankaya', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(68, 7, 'Çubuk', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(69, 7, 'Elmadağ', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(70, 7, 'Etimesgut', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(71, 7, 'Evren', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(72, 7, 'Güdül', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(73, 7, 'Haymana', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(74, 7, 'Kalecik', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(75, 7, 'Kazan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(76, 7, 'Keçiören', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(77, 7, 'Kızılcahamam', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(78, 7, 'Mamak', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(79, 7, 'Nallıhan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(80, 7, 'Polatlı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(81, 7, 'Pursaklar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(82, 7, 'Sincan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(83, 7, 'Şereflikoçhisar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(84, 7, 'Yenimahalle', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(85, 8, 'Akseki', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(86, 8, 'Aksu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(87, 8, 'Alanya', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(88, 8, 'Demre', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(89, 8, 'Döşemealtı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(90, 8, 'Elmalı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(91, 8, 'Finike', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(92, 8, 'Gazipaşa', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(93, 8, 'Gündoğmuş', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(94, 8, 'İbradı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(95, 8, 'Kaş', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(96, 8, 'Kemer', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(97, 8, 'Kepez', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(98, 8, 'Konyaaltı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(99, 8, 'Korkuteli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(100, 8, 'Kumluca', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(101, 8, 'Manavgat', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(102, 8, 'Muratpaşa', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(103, 8, 'Serik', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(104, 9, 'Çıldır', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(105, 9, 'Damal', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(106, 9, 'Göle', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(107, 9, 'Hanak', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(108, 9, 'Posof', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(109, 10, 'Ardanuç', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(110, 10, 'Arhavi', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(111, 10, 'Borçka', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(112, 10, 'Hopa', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(113, 10, 'Murgul', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(114, 10, 'Şavşat', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(115, 10, 'Yusufeli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(116, 11, 'Bozdoğan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(117, 11, 'Buharkent', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(118, 11, 'Çine', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(119, 11, 'Didim', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(120, 11, 'Efeler', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(121, 11, 'Germencik', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(122, 11, 'İncirliova', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(123, 11, 'Karacasu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(124, 11, 'Karpuzlu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(125, 11, 'Koçarlı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(126, 11, 'Köşk', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(127, 11, 'Kuşadası', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(128, 11, 'Kuyucak', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(129, 11, 'Nazilli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(130, 11, 'Söke', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(131, 11, 'Sultanhisar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(132, 11, 'Yenipazar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(133, 12, 'Altıeylül', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(134, 12, 'Ayvalık', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(135, 12, 'Balya', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(136, 12, 'Bandırma', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(137, 12, 'Bigadiç', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(138, 12, 'Burhaniye', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(139, 12, 'Dursunbey', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(140, 12, 'Edremit', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(141, 12, 'Erdek', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(142, 12, 'Gömeç', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(143, 12, 'Gönen', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(144, 12, 'Havran', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(145, 12, 'İvrindi', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(146, 12, 'Karesi', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(147, 12, 'Kepsut', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(148, 12, 'Manyas', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(149, 12, 'Marmara', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(150, 12, 'Savaştepe', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(151, 12, 'Sındırgı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(152, 12, 'Susurluk', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(153, 13, 'Amasra', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(154, 13, 'Kurucaşile', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(155, 13, 'Ulus', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(156, 14, 'Beşiri', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(157, 14, 'Gercüş', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(158, 14, 'Hasankeyf', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(159, 14, 'Kozluk', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(160, 14, 'Sason', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(161, 15, 'Aydıntepe', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(162, 15, 'Demirözü', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(163, 16, 'Bozüyük', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(164, 16, 'Gölpazarı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(165, 16, 'İnhisar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(166, 16, 'Osmaneli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(167, 16, 'Pazaryeri', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(168, 16, 'Söğüt', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(169, 17, 'Adaklı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(170, 17, 'Genç', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(171, 17, 'Karlıova', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(172, 17, 'Kiğı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(173, 17, 'Solhan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(174, 17, 'Yayladere', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(175, 17, 'Yedisu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(176, 18, 'Adilcevaz', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(177, 18, 'Ahlat', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(178, 18, 'Güroymak', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(179, 18, 'Hizan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(180, 18, 'Mutki', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(181, 18, 'Tatvan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(182, 19, 'Dörtdivan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(183, 19, 'Gerede', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(184, 19, 'Göynük', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(185, 19, 'Kıbrıscık', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(186, 19, 'Mengen', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(187, 19, 'Mudurnu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(188, 19, 'Seben', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(189, 19, 'Yeniçağa', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(190, 20, 'Ağlasun', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(191, 20, 'Altınyayla', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(192, 20, 'Bucak', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(193, 20, 'Çavdır', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(194, 20, 'Çeltikçi', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(195, 20, 'Gölhisar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(196, 20, 'Karamanlı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(197, 20, 'Tefenni', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(198, 20, 'Yeşilova', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(199, 21, 'Büyükorhan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(200, 21, 'Gemlik', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(201, 21, 'Gürsu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(202, 21, 'Harmancık', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(203, 21, 'İnegöl', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(204, 21, 'İznik', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(205, 21, 'Karacabey', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(206, 21, 'Keles', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(207, 21, 'Kestel', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(208, 21, 'Mudanya', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(209, 21, 'Mustafakemalpaşa', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(210, 21, 'Nilüfer', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(211, 21, 'Orhaneli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(212, 21, 'Orhangazi', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(213, 21, 'Osmangazi', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(214, 21, 'Yenişehir', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(215, 21, 'Yıldırım', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(216, 22, 'Ayvacık', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(217, 22, 'Bayramiç', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(218, 22, 'Biga', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(219, 22, 'Bozcaada', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(220, 22, 'Çan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(221, 22, 'Eceabat', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(222, 22, 'Ezine', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(223, 22, 'Gelibolu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(224, 22, 'Gökçeada', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(225, 22, 'Lapseki', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(226, 22, 'Yenice', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(227, 23, 'Atkaracalar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(228, 23, 'Bayramören', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(229, 23, 'Çerkeş', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(230, 23, 'Eldivan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(231, 23, 'Ilgaz', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(232, 23, 'Kızılırmak', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(233, 23, 'Korgun', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(234, 23, 'Kurşunlu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(235, 23, 'Orta', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(236, 23, 'Şabanözü', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(237, 23, 'Yapraklı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(238, 24, 'Alaca', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(239, 24, 'Boğazkale', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(240, 24, 'Dodurga', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(241, 24, 'İskilip', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(242, 24, 'Kargı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(243, 24, 'Laçin', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(244, 24, 'Mecitözü', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(245, 24, 'Oğuzlar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(246, 24, 'Osmancık', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(247, 24, 'Sungurlu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(248, 24, 'Uğurludağ', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(249, 25, 'Acıpayam', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(250, 25, 'Babadağ', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(251, 25, 'Baklan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(252, 25, 'Bekilli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(253, 25, 'Beyağaç', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(254, 25, 'Bozkurt', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(255, 25, 'Buldan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(256, 25, 'Çal', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(257, 25, 'Çameli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(258, 25, 'Çardak', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(259, 25, 'Çivril', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(260, 25, 'Güney', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(261, 25, 'Honaz', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(262, 25, 'Kale', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(263, 25, 'Merkezefendi', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(264, 25, 'Pamukkale', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(265, 25, 'Sarayköy', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(266, 25, 'Serinhisar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(267, 25, 'Tavas', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(268, 26, 'Bağlar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(269, 26, 'Bismil', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(270, 26, 'Çermik', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(271, 26, 'Çınar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(272, 26, 'Çüngüş', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(273, 26, 'Dicle', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(274, 26, 'Eğil', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(275, 26, 'Ergani', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(276, 26, 'Hani', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(277, 26, 'Hazro', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(278, 26, 'Kayapınar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(279, 26, 'Kocaköy', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(280, 26, 'Kulp', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(281, 26, 'Lice', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(282, 26, 'Silvan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(283, 26, 'Sur', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(284, 27, 'Akçakoca', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(285, 27, 'Cumayeri', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(286, 27, 'Çilimli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(287, 27, 'Gölyaka', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(288, 27, 'Gümüşova', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(289, 27, 'Kaynaşlı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(290, 27, 'Yığılca', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(291, 28, 'Enez', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(292, 28, 'Havsa', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(293, 28, 'İpsala', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(294, 28, 'Keşan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(295, 28, 'Lalapaşa', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(296, 28, 'Meriç', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(297, 28, 'Süloğlu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(298, 28, 'Uzunköprü', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(299, 29, 'Ağın', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(300, 29, 'Alacakaya', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(301, 29, 'Arıcak', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(302, 29, 'Baskil', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(303, 29, 'Karakoçan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(304, 29, 'Keban', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(305, 29, 'Kovancılar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(306, 29, 'Maden', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(307, 29, 'Palu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(308, 29, 'Sivrice', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(309, 30, 'Çayırlı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(310, 30, 'İliç', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(311, 30, 'Kemah', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(312, 30, 'Kemaliye', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(313, 30, 'Otlukbeli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(314, 30, 'Refahiye', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(315, 30, 'Tercan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(316, 30, 'Üzümlü', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(317, 31, 'Aşkale', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(318, 31, 'Aziziye', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(319, 31, 'Çat', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(320, 31, 'Hınıs', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(321, 31, 'Horasan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(322, 31, 'İspir', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(323, 31, 'Karaçoban', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(324, 31, 'Karayazı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(325, 31, 'Köprüköy', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(326, 31, 'Narman', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(327, 31, 'Oltu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(328, 31, 'Olur', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(329, 31, 'Palandöken', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(330, 31, 'Pasinler', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(331, 31, 'Pazaryolu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(332, 31, 'Şenkaya', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(333, 31, 'Tekman', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(334, 31, 'Tortum', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(335, 31, 'Uzundere', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(336, 31, 'Yakutiye', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(337, 32, 'Alpu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(338, 32, 'Beylikova', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(339, 32, 'Çifteler', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(340, 32, 'Günyüzü', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(341, 32, 'Han', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(342, 32, 'İnönü', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(343, 32, 'Mahmudiye', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(344, 32, 'Mihalgazi', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(345, 32, 'Mihalıççık', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(346, 32, 'Odunpazarı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(347, 32, 'Sarıcakaya', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(348, 32, 'Seyitgazi', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(349, 32, 'Sivrihisar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(350, 32, 'Tepebaşı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(351, 33, 'Araban', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(352, 33, 'İslahiye', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(353, 33, 'Karkamış', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(354, 33, 'Nizip', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(355, 33, 'Nurdağı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(356, 33, 'Oğuzeli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(357, 33, 'Şahinbey', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(358, 33, 'Şehitkamil', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(359, 33, 'Yavuzeli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(360, 34, 'Alucra', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(361, 34, 'Bulancak', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(362, 34, 'Çamoluk', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(363, 34, 'Çanakçı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(364, 34, 'Dereli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(365, 34, 'Doğankent', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(366, 34, 'Espiye', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(367, 34, 'Eynesil', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(368, 34, 'Görele', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(369, 34, 'Güce', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(370, 34, 'Keşap', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(371, 34, 'Piraziz', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(372, 34, 'Şebinkarahisar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(373, 34, 'Tirebolu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(374, 34, 'Yağlıdere', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(375, 35, 'Kelkit', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(376, 35, 'Köse', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(377, 35, 'Kürtün', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(378, 35, 'Şiran', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(379, 35, 'Torul', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(380, 36, 'Çukurca', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(381, 36, 'Şemdinli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(382, 36, 'Yüksekova', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(383, 37, 'Altınözü', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(384, 37, 'Antakya', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(385, 37, 'Arsuz', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(386, 37, 'Belen', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(387, 37, 'Defne', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(388, 37, 'Dörtyol', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(389, 37, 'Erzin', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(390, 37, 'Hassa', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(391, 37, 'İskenderun', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(392, 37, 'Kırıkhan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(393, 37, 'Kumlu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(394, 37, 'Payas', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(395, 37, 'Reyhanlı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(396, 37, 'Samandağ', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(397, 37, 'Yayladağı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(398, 38, 'Aralık', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(399, 38, 'Karakoyunlu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(400, 38, 'Tuzluca', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(401, 39, 'Atabey', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(402, 39, 'Eğirdir', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(403, 39, 'Gelendost', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(404, 39, 'Keçiborlu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(405, 39, 'Senirkent', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(406, 39, 'Sütçüler', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(407, 39, 'Şarkikaraağaç', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(408, 39, 'Uluborlu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(409, 39, 'Yalvaç', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(410, 39, 'Yenişarbademli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(411, 40, 'Adalar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(412, 40, 'Arnavutköy', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(413, 40, 'Ataşehir', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(414, 40, 'Avcılar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(415, 40, 'Bağcılar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(416, 40, 'Bahçelievler', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(417, 40, 'Bakırköy', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(418, 40, 'Başakşehir', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(419, 40, 'Bayrampaşa', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(420, 40, 'Beşiktaş', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(421, 40, 'Beykoz', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(422, 40, 'Beylikdüzü', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(423, 40, 'Beyoğlu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(424, 40, 'Büyükçekmece', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(425, 40, 'Çatalca', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(426, 40, 'Çekmeköy', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(427, 40, 'Esenler', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(428, 40, 'Esenyurt', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(429, 40, 'Eyüp', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(430, 40, 'Fatih', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(431, 40, 'Gaziosmanpaşa', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(432, 40, 'Güngören', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(433, 40, 'Kadıköy', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(434, 40, 'Kağıthane', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(435, 40, 'Kartal', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(436, 40, 'Küçükçekmece', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(437, 40, 'Maltepe', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(438, 40, 'Pendik', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(439, 40, 'Sancaktepe', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(440, 40, 'Sarıyer', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(441, 40, 'Silivri', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(442, 40, 'Sultanbeyli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(443, 40, 'Sultangazi', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(444, 40, 'Şile', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(445, 40, 'Şişli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(446, 40, 'Tuzla', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(447, 40, 'Ümraniye', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(448, 40, 'Üsküdar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(449, 40, 'Zeytinburnu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(450, 41, 'Aliağa', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(451, 41, 'Balçova', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(452, 41, 'Bayındır', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(453, 41, 'Bayraklı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(454, 41, 'Bergama', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(455, 41, 'Beydağ', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(456, 41, 'Bornova', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(457, 41, 'Buca', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(458, 41, 'Çeşme', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(459, 41, 'Çiğli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(460, 41, 'Dikili', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(461, 41, 'Foça', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(462, 41, 'Gaziemir', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(463, 41, 'Güzelbahçe', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(464, 41, 'Karabağlar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(465, 41, 'Karaburun', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(466, 41, 'Karşıyaka', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(467, 41, 'Kemalpaşa', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(468, 41, 'Kınık', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(469, 41, 'Kiraz', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(470, 41, 'Konak', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(471, 41, 'Menderes', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(472, 41, 'Menemen', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(473, 41, 'Narlıdere', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(474, 41, 'Ödemiş', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(475, 41, 'Seferihisar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(476, 41, 'Selçuk', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(477, 41, 'Tire', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(478, 41, 'Torbalı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(479, 41, 'Urla', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(480, 42, 'Afşin', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(481, 42, 'Andırın', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(482, 42, 'Çağlayancerit', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(483, 42, 'Dulkadiroğlu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(484, 42, 'Ekinözü', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(485, 42, 'Elbistan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(486, 42, 'Göksun', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(487, 42, 'Nurhak', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(488, 42, 'Onikişubat', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(489, 42, 'Pazarcık', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(490, 42, 'Türkoğlu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(491, 43, 'Eflani', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(492, 43, 'Eskipazar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(493, 43, 'Ovacık', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(494, 43, 'Safranbolu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(495, 44, 'Ayrancı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(496, 44, 'Başyayla', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(497, 44, 'Ermenek', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(498, 44, 'Kazımkarabekir', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(499, 44, 'Sarıveliler', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(500, 45, 'Akyaka', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(501, 45, 'Arpaçay', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(502, 45, 'Digor', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(503, 45, 'Kağızman', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(504, 45, 'Sarıkamış', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(505, 45, 'Selim', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(506, 45, 'Susuz', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(507, 46, 'Abana', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(508, 46, 'Ağlı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(509, 46, 'Araç', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(510, 46, 'Azdavay', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(511, 46, 'Cide', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(512, 46, 'Çatalzeytin', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(513, 46, 'Daday', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(514, 46, 'Devrekani', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(515, 46, 'Doğanyurt', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(516, 46, 'Hanönü', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(517, 46, 'İhsangazi', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(518, 46, 'İnebolu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(519, 46, 'Küre', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(520, 46, 'Pınarbaşı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(521, 46, 'Seydiler', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(522, 46, 'Şenpazar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(523, 46, 'Taşköprü', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(524, 46, 'Tosya', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(525, 47, 'Akkışla', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(526, 47, 'Bünyan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(527, 47, 'Develi', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(528, 47, 'Felahiye', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(529, 47, 'Hacılar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(530, 47, 'İncesu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(531, 47, 'Kocasinan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(532, 47, 'Melikgazi', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(533, 47, 'Özvatan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(534, 47, 'Sarıoğlan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(535, 47, 'Sarız', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(536, 47, 'Talas', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(537, 47, 'Tomarza', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(538, 47, 'Yahyalı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(539, 47, 'Yeşilhisar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(540, 48, 'Bahşili', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(541, 48, 'Balışeyh', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(542, 48, 'Çelebi', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(543, 48, 'Delice', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(544, 48, 'Karakeçili', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(545, 48, 'Keskin', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(546, 48, 'Sulakyurt', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(547, 48, 'Yahşihan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(548, 49, 'Babaeski', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(549, 49, 'Demirköy', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(550, 49, 'Kofçaz', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(551, 49, 'Lüleburgaz', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(552, 49, 'Pehlivanköy', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(553, 49, 'Pınarhisar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(554, 49, 'Vize', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(555, 50, 'Akçakent', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(556, 50, 'Akpınar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(557, 50, 'Boztepe', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(558, 50, 'Çiçekdağı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(559, 50, 'Kaman', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(560, 50, 'Mucur', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(561, 51, 'Elbeyli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(562, 51, 'Musabeyli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(563, 51, 'Polateli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(564, 52, 'Başiskele', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(565, 52, 'Çayırova', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(566, 52, 'Darıca', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(567, 52, 'Derince', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(568, 52, 'Dilovası', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(569, 52, 'Gebze', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(570, 52, 'Gölcük', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(571, 52, 'İzmit', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(572, 52, 'Kandıra', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(573, 52, 'Karamürsel', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(574, 52, 'Kartepe', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(575, 52, 'Körfez', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(576, 53, 'Ahırlı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(577, 53, 'Akören', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(578, 53, 'Akşehir', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(579, 53, 'Altınekin', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(580, 53, 'Beyşehir', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(581, 53, 'Bozkır', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(582, 53, 'Cihanbeyli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(583, 53, 'Çeltik', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(584, 53, 'Çumra', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(585, 53, 'Derbent', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(586, 53, 'Derebucak', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(587, 53, 'Doğanhisar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(588, 53, 'Emirgazi', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(589, 53, 'Ereğli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(590, 53, 'Güneysınır', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(591, 53, 'Hadim', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(592, 53, 'Halkapınar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(593, 53, 'Hüyük', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(594, 53, 'Ilgın', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(595, 53, 'Kadınhanı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(596, 53, 'Karapınar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(597, 53, 'Karatay', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(598, 53, 'Kulu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(599, 53, 'Meram', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(600, 53, 'Sarayönü', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(601, 53, 'Selçuklu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(602, 53, 'Seydişehir', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(603, 53, 'Taşkent', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(604, 53, 'Tuzlukçu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(605, 53, 'Yalıhüyük', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(606, 53, 'Yunak', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(607, 54, 'Altıntaş', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(608, 54, 'Aslanapa', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(609, 54, 'Çavdarhisar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(610, 54, 'Domaniç', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(611, 54, 'Dumlupınar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(612, 54, 'Emet', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(613, 54, 'Gediz', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(614, 54, 'Hisarcık', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(615, 54, 'Pazarlar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(616, 54, 'Simav', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(617, 54, 'Şaphane', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(618, 54, 'Tavşanlı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(619, 55, 'Akçadağ', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(620, 55, 'Arapgir', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(621, 55, 'Arguvan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(622, 55, 'Battalgazi', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(623, 55, 'Darende', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(624, 55, 'Doğanşehir', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(625, 55, 'Doğanyol', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(626, 55, 'Hekimhan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(627, 55, 'Kuluncak', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(628, 55, 'Pütürge', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(629, 55, 'Yazıhan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(630, 55, 'Yeşilyurt', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(631, 56, 'Ahmetli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(632, 56, 'Akhisar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(633, 56, 'Alaşehir', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(634, 56, 'Demirci', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(635, 56, 'Gölmarmara', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(636, 56, 'Gördes', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(637, 56, 'Kırkağaç', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(638, 56, 'Köprübaşı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(639, 56, 'Kula', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(640, 56, 'Salihli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(641, 56, 'Sarıgöl', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(642, 56, 'Saruhanlı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(643, 56, 'Selendi', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(644, 56, 'Soma', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(645, 56, 'Şehzadeler', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(646, 56, 'Turgutlu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(647, 56, 'Yunusemre', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(648, 57, 'Artuklu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(649, 57, 'Dargeçit', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(650, 57, 'Derik', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(651, 57, 'Kızıltepe', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(652, 57, 'Mazıdağı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(653, 57, 'Midyat', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(654, 57, 'Nusaybin', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(655, 57, 'Ömerli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(656, 57, 'Savur', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(657, 57, 'Yeşilli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(658, 58, 'Akdeniz', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(659, 58, 'Anamur', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(660, 58, 'Aydıncık', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(661, 58, 'Bozyazı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(662, 58, 'Çamlıyayla', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(663, 58, 'Erdemli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(664, 58, 'Gülnar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(665, 58, 'Mezitli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(666, 58, 'Mut', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(667, 58, 'Silifke', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(668, 58, 'Tarsus', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(669, 58, 'Toroslar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(670, 59, 'Bodrum', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(671, 59, 'Dalaman', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(672, 59, 'Datça', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(673, 59, 'Fethiye', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(674, 59, 'Kavaklıdere', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(675, 59, 'Köyceğiz', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(676, 59, 'Marmaris', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(677, 59, 'Menteşe', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(678, 59, 'Milas', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(679, 59, 'Ortaca', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(680, 59, 'Seydikemer', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(681, 59, 'Ula', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(682, 59, 'Yatağan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(683, 60, 'Bulanık', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(684, 60, 'Hasköy', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(685, 60, 'Korkut', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(686, 60, 'Malazgirt', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(687, 60, 'Varto', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(688, 61, 'Acıgöl', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(689, 61, 'Avanos', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(690, 61, 'Derinkuyu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(691, 61, 'Gülşehir', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(692, 61, 'Hacıbektaş', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(693, 61, 'Kozaklı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(694, 61, 'Ürgüp', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(695, 62, 'Altunhisar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(696, 62, 'Bor', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(697, 62, 'Çamardı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(698, 62, 'Çiftlik', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(699, 62, 'Ulukışla', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(700, 63, 'Akkuş', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(701, 63, 'Altınordu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(702, 63, 'Aybastı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(703, 63, 'Çamaş', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(704, 63, 'Çatalpınar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(705, 63, 'Çaybaşı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(706, 63, 'Fatsa', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(707, 63, 'Gölköy', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(708, 63, 'Gülyalı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(709, 63, 'Gürgentepe', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(710, 63, 'İkizce', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(711, 63, 'Kabadüz', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(712, 63, 'Kabataş', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(713, 63, 'Korgan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(714, 63, 'Kumru', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(715, 63, 'Mesudiye', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(716, 63, 'Perşembe', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(717, 63, 'Ulubey', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(718, 63, 'Ünye', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(719, 64, 'Bahçe', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(720, 64, 'Düziçi', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(721, 64, 'Hasanbeyli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(722, 64, 'Kadirli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(723, 64, 'Sumbas', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(724, 64, 'Toprakkale', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(725, 65, 'Ardeşen', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(726, 65, 'Çamlıhemşin', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(727, 65, 'Çayeli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(728, 65, 'Derepazarı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(729, 65, 'Fındıklı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(730, 65, 'Güneysu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(731, 65, 'Hemşin', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(732, 65, 'İkizdere', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(733, 65, 'İyidere', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(734, 65, 'Kalkandere', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(735, 65, 'Pazar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(736, 66, 'Adapazarı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(737, 66, 'Akyazı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(738, 66, 'Arifiye', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(739, 66, 'Erenler', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(740, 66, 'Ferizli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(741, 66, 'Geyve', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(742, 66, 'Hendek', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(743, 66, 'Karapürçek', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(744, 66, 'Karasu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(745, 66, 'Kaynarca', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(746, 66, 'Kocaali', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(747, 66, 'Pamukova', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(748, 66, 'Sapanca', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(749, 66, 'Serdivan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(750, 66, 'Söğütlü', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(751, 66, 'Taraklı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(752, 67, '42143', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(753, 67, 'Alaçam', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(754, 67, 'Asarcık', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(755, 67, 'Atakum', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(756, 67, 'Bafra', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(757, 67, 'Canik', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(758, 67, 'Çarşamba', '2020-04-01 11:23:12', '2020-04-01 11:23:12');
INSERT INTO `counties` (`id`, `city_id`, `county`, `created_at`, `updated_at`) VALUES
(759, 67, 'Havza', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(760, 67, 'İlkadım', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(761, 67, 'Kavak', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(762, 67, 'Ladik', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(763, 67, 'Salıpazarı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(764, 67, 'Tekkeköy', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(765, 67, 'Terme', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(766, 67, 'Vezirköprü', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(767, 67, 'Yakakent', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(768, 68, 'Baykan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(769, 68, 'Eruh', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(770, 68, 'Kurtalan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(771, 68, 'Pervari', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(772, 68, 'Şirvan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(773, 68, 'Tillo', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(774, 69, 'Ayancık', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(775, 69, 'Boyabat', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(776, 69, 'Dikmen', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(777, 69, 'Durağan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(778, 69, 'Erfelek', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(779, 69, 'Gerze', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(780, 69, 'Saraydüzü', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(781, 69, 'Türkeli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(782, 70, 'Akıncılar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(783, 70, 'Divriği', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(784, 70, 'Doğanşar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(785, 70, 'Gemerek', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(786, 70, 'Gölova', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(787, 70, 'Gürün', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(788, 70, 'Hafik', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(789, 70, 'İmranlı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(790, 70, 'Kangal', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(791, 70, 'Koyulhisar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(792, 70, 'Suşehri', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(793, 70, 'Şarkışla', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(794, 70, 'Ulaş', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(795, 70, 'Yıldızeli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(796, 70, 'Zara', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(797, 71, 'Akçakale', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(798, 71, 'Birecik', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(799, 71, 'Bozova', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(800, 71, 'Ceylanpınar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(801, 71, 'Eyyübiye', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(802, 71, 'Halfeti', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(803, 71, 'Haliliye', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(804, 71, 'Harran', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(805, 71, 'Hilvan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(806, 71, 'Karaköprü', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(807, 71, 'Siverek', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(808, 71, 'Suruç', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(809, 71, 'Viranşehir', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(810, 72, 'Beytüşşebap', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(811, 72, 'Cizre', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(812, 72, 'Güçlükonak', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(813, 72, 'İdil', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(814, 72, 'Silopi', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(815, 72, 'Uludere', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(816, 73, 'Çerkezköy', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(817, 73, 'Çorlu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(818, 73, 'Ergene', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(819, 73, 'Hayrabolu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(820, 73, 'Kapaklı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(821, 73, 'Malkara', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(822, 73, 'Marmaraereğlisi', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(823, 73, 'Muratlı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(824, 73, 'Saray', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(825, 73, 'Süleymanpaşa', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(826, 73, 'Şarköy', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(827, 74, 'Almus', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(828, 74, 'Artova', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(829, 74, 'Başçiftlik', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(830, 74, 'Erbaa', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(831, 74, 'Niksar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(832, 74, 'Reşadiye', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(833, 74, 'Sulusaray', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(834, 74, 'Turhal', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(835, 74, 'Zile', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(836, 75, 'Akçaabat', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(837, 75, 'Araklı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(838, 75, 'Arsin', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(839, 75, 'Beşikdüzü', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(840, 75, 'Çarşıbaşı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(841, 75, 'Çaykara', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(842, 75, 'Dernekpazarı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(843, 75, 'Düzköy', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(844, 75, 'Hayrat', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(845, 75, 'Maçka', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(846, 75, 'Of', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(847, 75, 'Ortahisar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(848, 75, 'Sürmene', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(849, 75, 'Şalpazarı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(850, 75, 'Tonya', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(851, 75, 'Vakfıkebir', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(852, 75, 'Yomra', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(853, 76, 'Çemişgezek', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(854, 76, 'Hozat', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(855, 76, 'Mazgirt', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(856, 76, 'Nazımiye', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(857, 76, 'Pertek', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(858, 76, 'Pülümür', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(859, 77, 'Banaz', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(860, 77, 'Eşme', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(861, 77, 'Karahallı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(862, 77, 'Sivaslı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(863, 78, 'Bahçesaray', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(864, 78, 'Başkale', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(865, 78, 'Çaldıran', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(866, 78, 'Çatak', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(867, 78, 'Erciş', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(868, 78, 'Gevaş', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(869, 78, 'Gürpınar', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(870, 78, 'İpekyolu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(871, 78, 'Muradiye', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(872, 78, 'Özalp', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(873, 78, 'Tuşba', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(874, 79, 'Altınova', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(875, 79, 'Armutlu', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(876, 79, 'Çınarcık', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(877, 79, 'Çiftlikköy', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(878, 79, 'Termal', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(879, 80, 'Akdağmadeni', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(880, 80, 'Boğazlıyan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(881, 80, 'Çandır', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(882, 80, 'Çayıralan', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(883, 80, 'Çekerek', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(884, 80, 'Kadışehri', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(885, 80, 'Saraykent', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(886, 80, 'Sarıkaya', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(887, 80, 'Sorgun', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(888, 80, 'Şefaatli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(889, 80, 'Yenifakılı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(890, 80, 'Yerköy', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(891, 81, 'Alaplı', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(892, 81, 'Çaycuma', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(893, 81, 'Devrek', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(894, 81, 'Gökçebey', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(895, 81, 'Kilimli', '2020-04-01 11:23:12', '2020-04-01 11:23:12'),
(896, 81, 'Kozlu', '2020-04-01 11:23:12', '2020-04-01 11:23:12');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `code`) VALUES
(1, 'Turkish', 'tr'),
(2, 'English', 'en'),
(3, 'Ukrainian', 'uk'),
(4, 'Russian', 'ru'),
(5, 'Abkhazian', 'ab'),
(6, 'Afar', 'aa'),
(7, 'Afrikaans', 'af'),
(8, 'Akan', 'ak'),
(9, 'Albanian', 'sq'),
(10, 'Amharic', 'am'),
(11, 'Arabic', 'ar'),
(12, 'Aragonese', 'an'),
(13, 'Armenian', 'hy'),
(14, 'Assamese', 'as'),
(15, 'Avaric', 'av'),
(16, 'Avestan', 'ae'),
(17, 'Aymara', 'ay'),
(18, 'Azerbaijani', 'az'),
(19, 'Bambara', 'bm'),
(20, 'Bashkir', 'ba'),
(21, 'Basque', 'eu'),
(22, 'Belarusian', 'be'),
(23, 'Bengali', 'bn'),
(24, 'Bihari languages', 'bh'),
(25, 'Bislama', 'bi'),
(26, 'Bosnian', 'bs'),
(27, 'Breton', 'br'),
(28, 'Bulgarian', 'bg'),
(29, 'Burmese', 'my'),
(30, 'Catalan, Valcodeian', 'ca'),
(31, 'Central Khmer', 'km'),
(32, 'Chamorro', 'ch'),
(33, 'Chechen', 'ce'),
(34, 'Chichewa, Chewa, Nyanja', 'ny'),
(35, 'Chinese', 'zh'),
(36, 'Church Slavonic, Old Bulgarian, Old Church Slavonic', 'cu'),
(37, 'Chuvash', 'cv'),
(38, 'Cornish', 'kw'),
(39, 'Corsican', 'co'),
(40, 'Cree', 'cr'),
(41, 'Croatian', 'hr'),
(42, 'Czech', 'cs'),
(43, 'Danish', 'da'),
(44, 'Divehi, Dhivehi, Maldivian', 'dv'),
(45, 'Dutch, Flemish', 'nl'),
(46, 'Dzongkha', 'dz'),
(47, 'Esperanto', 'eo'),
(48, 'Estonian', 'et'),
(49, 'Ewe', 'ee'),
(50, 'Faroese', 'fo'),
(51, 'Fijian', 'fj'),
(52, 'Finnish', 'fi'),
(53, 'Frcodeh', 'fr'),
(54, 'Fulah', 'ff'),
(55, 'Gaelic, Scottish Gaelic', 'gd'),
(56, 'Galician', 'gl'),
(57, 'Ganda', 'lg'),
(58, 'Georgian', 'ka'),
(59, 'German', 'de'),
(60, 'Gikuyu, Kikuyu', 'ki'),
(61, 'Greek (Modern)', 'el'),
(62, 'Greenlandic, Kalaallisut', 'kl'),
(63, 'Guarani', 'gn'),
(64, 'Gujarati', 'gu'),
(65, 'Haitian, Haitian Creole', 'ht'),
(66, 'Hausa', 'ha'),
(67, 'Hebrew', 'he'),
(68, 'Herero', 'hz'),
(69, 'Hindi', 'hi'),
(70, 'Hiri Motu', 'ho'),
(71, 'Hungarian', 'hu'),
(72, 'Icelandic', 'is'),
(73, 'Ido', 'io'),
(74, 'Igbo', 'ig'),
(75, 'Indonesian', 'id'),
(76, 'Interlingua (International Auxiliary Language Association)', 'ia'),
(77, 'Interlingue', 'ie'),
(78, 'Inuktitut', 'iu'),
(79, 'Inupiaq', 'ik'),
(80, 'Irish', 'ga'),
(81, 'Italian', 'it'),
(82, 'Japanese', 'ja'),
(83, 'Javanese', 'jv'),
(84, 'Kannada', 'kn'),
(85, 'Kanuri', 'kr'),
(86, 'Kashmiri', 'ks'),
(87, 'Kazakh', 'kk'),
(88, 'Kinyarwanda', 'rw'),
(89, 'Komi', 'kv'),
(90, 'Kongo', 'kg'),
(91, 'Korean', 'ko'),
(92, 'Kwanyama, Kuanyama', 'kj'),
(93, 'Kurdish', 'ku'),
(94, 'Kyrgyz', 'ky'),
(95, 'Lao', 'lo'),
(96, 'Latin', 'la'),
(97, 'Latvian', 'lv'),
(98, 'Letzeburgesch, Luxembourgish', 'lb'),
(99, 'Limburgish, Limburgan, Limburger', 'li'),
(100, 'Lingala', 'ln'),
(101, 'Lithuanian', 'lt'),
(102, 'Luba-Katanga', 'lu'),
(103, 'Macedonian', 'mk'),
(104, 'Malagasy', 'mg'),
(105, 'Malay', 'ms'),
(106, 'Malayalam', 'ml'),
(107, 'Maltese', 'mt'),
(108, 'Manx', 'gv'),
(109, 'Maori', 'mi'),
(110, 'Marathi', 'mr'),
(111, 'Marshallese', 'mh'),
(112, 'Moldovan, Moldavian, Romanian', 'ro'),
(113, 'Mongolian', 'mn'),
(114, 'Nauru', 'na'),
(115, 'Navajo, Navaho', 'nv'),
(116, 'Northern Ndebele', 'nd'),
(117, 'Ndonga', 'ng'),
(118, 'Nepali', 'ne'),
(119, 'Northern Sami', 'se'),
(120, 'Norwegian', 'no'),
(121, 'Norwegian Bokmål', 'nb'),
(122, 'Norwegian Nynorsk', 'nn'),
(123, 'Nuosu, Sichuan Yi', 'ii'),
(124, 'Occitan (post 1500)', 'oc'),
(125, 'Ojibwa', 'oj'),
(126, 'Oriya', 'or'),
(127, 'Oromo', 'om'),
(128, 'Ossetian, Ossetic', 'os'),
(129, 'Pali', 'pi'),
(130, 'Panjabi, Punjabi', 'pa'),
(131, 'Pashto, Pushto', 'ps'),
(132, 'Persian', 'fa'),
(133, 'Polish', 'pl'),
(134, 'Portuguese', 'pt'),
(135, 'Quechua', 'qu'),
(136, 'Romansh', 'rm'),
(137, 'Rundi', 'rn'),
(138, 'Samoan', 'sm'),
(139, 'Sango', 'sg'),
(140, 'Sanskrit', 'sa'),
(141, 'Sardinian', 'sc'),
(142, 'Serbian', 'sr'),
(143, 'Shona', 'sn'),
(144, 'Sindhi', 'sd'),
(145, 'Sinhala, Sinhalese', 'si'),
(146, 'Slovak', 'sk'),
(147, 'Slovenian', 'sl'),
(148, 'Somali', 'so'),
(149, 'Sotho, Southern', 'st'),
(150, 'South Ndebele', 'nr'),
(151, 'Spanish, Castilian', 'es'),
(152, 'Sundanese', 'su'),
(153, 'Swahili', 'sw'),
(154, 'Swati', 'ss'),
(155, 'Swedish', 'sv'),
(156, 'Tagalog', 'tl'),
(157, 'Tahitian', 'ty'),
(158, 'Tajik', 'tg'),
(159, 'Tamil', 'ta'),
(160, 'Tatar', 'tt'),
(161, 'Telugu', 'te'),
(162, 'Thai', 'th'),
(163, 'Tibetan', 'bo'),
(164, 'Tigrinya', 'ti'),
(165, 'Tonga (Tonga Islands)', 'to'),
(166, 'Tsonga', 'ts'),
(167, 'Tswana', 'tn'),
(168, 'Turkmen', 'tk'),
(169, 'Twi', 'tw'),
(170, 'Uighur, Uyghur', 'ug'),
(171, 'Urdu', 'ur'),
(172, 'Uzbek', 'uz'),
(173, 'Venda', 've'),
(174, 'Vietnamese', 'vi'),
(175, 'Volap_k', 'vo'),
(176, 'Walloon', 'wa'),
(177, 'Welsh', 'cy'),
(178, 'Western Frisian', 'fy'),
(179, 'Wolof', 'wo'),
(180, 'Xhosa', 'xh'),
(181, 'Yiddish', 'yi'),
(182, 'Yoruba', 'yo'),
(183, 'Zhuang, Chuang', 'za'),
(184, 'Zulu', 'zu');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '20',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Note',
  `used` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`id`, `code`, `discount`, `note`, `used`, `created_at`, `updated_at`) VALUES
(1, 'Dh6UYzKgAa', '20', 'Note', 0, '2020-01-15 03:26:15', '2020-01-15 03:26:15'),
(2, 'lRpj53cD0h', '20', 'Note', 1, '2020-01-15 03:26:15', '2020-01-21 09:56:30'),
(3, 'plDM2CkW75', '20', 'Note', 0, '2020-01-15 03:26:15', '2020-01-15 03:26:15'),
(4, 'XI5jNGTmMu', '20', 'Note', 0, '2020-01-15 03:26:15', '2020-01-15 03:26:15'),
(5, 'GfT74qofIK', '20', 'Note', 0, '2020-01-15 03:26:15', '2020-01-15 03:26:15'),
(6, 'im71vDiAIP', '1', 'Note', 0, '2020-01-15 03:26:15', '2020-01-17 03:46:30'),
(7, 'NhaBJohcgq', '20', 'Note', 0, '2020-01-15 03:26:15', '2020-01-15 03:26:15'),
(8, 'SNf4BINGlq', '20', 'Note', 0, '2020-01-15 03:26:15', '2020-01-15 03:26:15'),
(9, 'XbEw7m3u3v', '20', 'Note', 0, '2020-01-15 03:26:15', '2020-01-15 03:26:15'),
(10, 'iefl4HsSt9', '20', 'Note', 0, '2020-01-15 03:26:15', '2020-01-15 03:26:15'),
(11, 'mk6aUsII7w', '20', 'Note', 0, '2020-01-15 03:26:15', '2020-01-15 03:26:15'),
(12, 'boBUJP1su7', '20', 'Note', 0, '2020-01-15 03:26:15', '2020-01-15 03:26:15'),
(13, 'bDvFtCnwkM', '20', 'Note', 0, '2020-01-15 03:26:15', '2020-01-15 03:26:15'),
(14, 'eSSOGWTLHV', '20', 'Note', 0, '2020-01-15 03:26:15', '2020-01-15 03:26:15'),
(15, 'aQl57nyqrZ', '100', 'laith', 0, '2020-01-15 03:26:15', '2020-03-31 09:02:59'),
(16, 'CD95eQdE3o', '90', 'for omar samir', 1, '2020-01-15 03:26:15', '2020-02-19 05:15:06'),
(20, 'BVEzWRQnlB', '80', 'Note', 1, '2020-01-15 03:26:15', '2020-01-16 00:06:22'),
(21, 'SoO11YOZsD', '90', '11', 1, '2020-02-19 08:10:27', '2020-04-02 12:04:45'),
(22, 'BYM2w1J4zy', '99.99', '111', 1, '2020-04-06 07:42:41', '2020-04-06 09:46:01'),
(23, 'dbhIxL3yHl', '99.99', 'note', 0, '2020-04-06 10:50:25', '2020-04-06 10:50:25');

-- --------------------------------------------------------

--
-- Table structure for table `dietitians`
--

CREATE TABLE `dietitians` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reference_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `d_type` int(11) NOT NULL DEFAULT 0,
  `city_id` bigint(20) DEFAULT NULL,
  `d_permission` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `errors`
--

CREATE TABLE `errors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `json_string` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `errors`
--

INSERT INTO `errors` (`id`, `json_string`, `model`, `created_at`, `updated_at`) VALUES
(1, '{\"TRANID\":null,\"PAResSyntaxOK\":\"true\",\"Filce\":\"\\u015ei\\u015fli\",\"firmaadi\":\"EnBiosis\",\"islemtipi\":\"Auth\",\"total1\":\"7.50\",\"lang\":\"tr\",\"merchantID\":\"400940535\",\"maskedCreditCard\":\"5355 76** **** 8256\",\"amount\":\"1299\",\"sID\":\"2\",\"ACQBIN\":\"510138\",\"itemnumber1\":\"a1\",\"Ecom_Payment_Card_ExpDate_Year\":\"23\",\"storekey\":\"ENDIET19\",\"MaskedPan\":\"535576***8256\",\"Fadres\":\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\u015ei\\u015fli\\/Istanbul\",\"clientIp\":\"178.243.69.174\",\"protocol\":\"3DS1.0.2\",\"iReqDetail\":null,\"okUrl\":\"http:\\/\\/localhost\\/enbiosis\\/public\\/checkout-result\",\"md\":\"535576:23477C18A2D411705C435735D09EAE5CD7F810096339982870E43C6E0031F80B:3342:##400940535\",\"clientId\":\"400940535\",\"vendorCode\":null,\"taksit\":null,\"productcode1\":\"a2\",\"paresTxStatus\":\"N\",\"Fil\":\"Istanbul\",\"signature\":\"dfFTWTs0VcAo7SLdQhCUTfkJaSW4\\/+3GfdlhN6k4dpkTvc2M9Sls6W29YrZEQuj76z6y+7NGuxRLTmiFytaf4eRPrs8QzzRbXeq7HcynJ+Ycqw2+q+TDlUx+Yfq43WGxX0XDT29xdkZis\\/w97I1DqnC48FbhCqSvWORVSJTVEChoSjAEOFraWxFKQnVfd6GXJ5SOv9v1s5xJWdHLsPfQSALCYV3zdfGitgQuhlWbsuYBm5C2aHHv5d92WLa4UlojJdZIcT3uiaYI5xnjmHoGoVrVS4C7Eet2EFnmNta1CdsGecvOkZnelCD7kwfLlB8OPZ+DCsArIlYm\\/AOUHC7i2A==\",\"Ecom_Payment_Card_ExpDate_Month\":\"08\",\"storetype\":\"3d_pay\",\"iReqCode\":null,\"veresEnrolledStatus\":\"Y\",\"tulkekod\":\"tr\",\"mdErrorMsg\":\"Not authenticated\",\"PAResVerified\":\"false\",\"cavv\":null,\"id1\":\"a5\",\"digest\":\"digest\",\"callbackCall\":\"true\",\"failUrl\":\"http:\\/\\/localhost\\/enbiosis\\/public\\/checkout-result\",\"cavvAlgorithm\":null,\"Fpostakodu\":\"postakod34367\",\"price1\":\"6.25\",\"faturaFirma\":\"faturaFirma\",\"xid\":\"i9q+5NjaeywclpvMQOR+bEfBwLY=\",\"encoding\":\"ISO-8859-9\",\"oid\":null,\"mdStatus\":\"0\",\"dsId\":\"2\",\"eci\":null,\"version\":\"4.0\",\"Fadres2\":\"XXX\",\"Fismi\":\"is\",\"qty1\":\"3\",\"desc1\":\"a4 desc\",\"_charset_\":\"UTF-8\",\"HASH\":\"Q6vYjQf+sIBLMTXbiaavfCQt5C8=\",\"rnd\":\"\\/pVs+Neam5cQ+ssMnU2L\",\"HASHPARAMS\":\"clientid:oid:mdStatus:cavv:eci:md:rnd:\",\"HASHPARAMSVAL\":\"4009405350535576:23477C18A2D411705C435735D09EAE5CD7F810096339982870E43C6E0031F80B:3342:##400940535\\/pVs+Neam5cQ+ssMnU2L\"}', 'Declinedpayment', '2020-02-20 11:29:49', '2020-05-13 13:18:47'),
(2, '{\"TRANID\":null,\"PAResSyntaxOK\":\"true\",\"Filce\":\"\\u015ei\\u015fli\",\"firmaadi\":\"EnBiosis\",\"islemtipi\":\"Auth\",\"total1\":\"7.50\",\"lang\":\"tr\",\"merchantID\":\"400940535\",\"maskedCreditCard\":\"5355 76** **** 8256\",\"amount\":\"1299\",\"sID\":\"2\",\"ACQBIN\":\"510138\",\"itemnumber1\":\"a1\",\"Ecom_Payment_Card_ExpDate_Year\":\"23\",\"storekey\":\"ENDIET19\",\"MaskedPan\":\"535576***8256\",\"Fadres\":\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\u015ei\\u015fli\\/Istanbul\",\"clientIp\":\"178.243.69.174\",\"protocol\":\"3DS1.0.2\",\"iReqDetail\":null,\"okUrl\":\"http:\\/\\/localhost\\/enbiosis\\/public\\/checkout-result\",\"md\":\"535576:A4A88404E5565BC0DEC42E69C68486E8D3AF88634427477165B68D10C0A63ED0:4311:##400940535\",\"clientId\":\"400940535\",\"vendorCode\":null,\"taksit\":null,\"productcode1\":\"a2\",\"paresTxStatus\":\"N\",\"Fil\":\"Istanbul\",\"signature\":\"RcDVLgPgkZo6TDmDyR16hmepGQsDI+tyEiOTrWJLz8QGkQK4VaYRBn15dYrtJ6lOMTOk7r7XxRrTUq0VfQdXZxXTDlV4NSWRH\\/pJ2HTaUr1QMqETlBj9b3NqSt1GkFaZ2LkRFcrjIyanI2\\/SZbmY1rlSejwABQHiTRMNOhXJHgEeTleTM5gJw842tVQImwysM6uGVPgU\\/uAk47NYsmtxudfMpDfiTRE5\\/cQNhTkiYS+Pqofd0fpixZmKj5bkiHLWy5QGii8U2kfGEN3rIhvdGVt7Io3X5IuP8ANI9lcHXaPx1pMjThYmQb22yqetupeZGBa8Jy59Lpl5eZFwTJ0RYA==\",\"Ecom_Payment_Card_ExpDate_Month\":\"08\",\"storetype\":\"3d_pay\",\"iReqCode\":null,\"veresEnrolledStatus\":\"Y\",\"tulkekod\":\"tr\",\"mdErrorMsg\":\"Not authenticated\",\"PAResVerified\":\"false\",\"cavv\":null,\"id1\":\"a5\",\"digest\":\"digest\",\"callbackCall\":\"true\",\"failUrl\":\"http:\\/\\/localhost\\/enbiosis\\/public\\/checkout-result\",\"cavvAlgorithm\":null,\"Fpostakodu\":\"postakod34367\",\"price1\":\"6.25\",\"faturaFirma\":\"faturaFirma\",\"xid\":\"OXHYdCSy\\/FLETzYiSjQBbjnW+5o=\",\"encoding\":\"ISO-8859-9\",\"oid\":null,\"mdStatus\":\"0\",\"dsId\":\"2\",\"eci\":null,\"version\":\"4.0\",\"Fadres2\":\"XXX\",\"Fismi\":\"is\",\"qty1\":\"3\",\"desc1\":\"a4 desc\",\"_charset_\":\"UTF-8\",\"HASH\":\"3a5baiOPs4F+WXfQOMOgcPmJrA0=\",\"rnd\":\"B9BjSwm\\/8j1HtIYrRmlt\",\"HASHPARAMS\":\"clientid:oid:mdStatus:cavv:eci:md:rnd:\",\"HASHPARAMSVAL\":\"4009405350535576:A4A88404E5565BC0DEC42E69C68486E8D3AF88634427477165B68D10C0A63ED0:4311:##400940535B9BjSwm\\/8j1HtIYrRmlt\"}', 'Declinedpayment', '2020-02-20 11:34:53', '2020-05-13 13:18:47'),
(3, '{\"TRANID\":null,\"PAResSyntaxOK\":\"true\",\"Filce\":\"\\u015ei\\u015fli\",\"firmaadi\":\"EnBiosis\",\"islemtipi\":\"Auth\",\"total1\":\"7.50\",\"lang\":\"tr\",\"merchantID\":\"400940535\",\"maskedCreditCard\":\"5355 76** **** 8256\",\"amount\":\"1299\",\"sID\":\"2\",\"ACQBIN\":\"510138\",\"itemnumber1\":\"a1\",\"Ecom_Payment_Card_ExpDate_Year\":\"23\",\"storekey\":\"ENDIET19\",\"MaskedPan\":\"535576***8256\",\"Fadres\":\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\u015ei\\u015fli\\/Istanbul\",\"clientIp\":\"178.243.69.174\",\"protocol\":\"3DS1.0.2\",\"iReqDetail\":null,\"okUrl\":\"http:\\/\\/localhost\\/enbiosis\\/public\\/checkout-result\",\"md\":\"535576:7C93ED070A90ED0D06542A084092F1386FBA86F7868C640D113BF5E9F461A060:3782:##400940535\",\"clientId\":\"400940535\",\"vendorCode\":null,\"taksit\":null,\"productcode1\":\"a2\",\"paresTxStatus\":\"N\",\"Fil\":\"Istanbul\",\"signature\":\"Q9h\\/Ty1BOvAMyWRdG22+SmBSvEvLJi0RY+wba1L1udYTtuBMgGrkmHnlZRfbE7Pg6skPjjLByi8BDiGXXvqSmjX8Q\\/Ir7jfYMEbRSim+UjyfBaLXVYHODUDRPxq7UCcCgYx\\/jXs5mrxUnK2yYWIx3MFmBkmxQFB3ELpNDmgMWGGkTuZ8UB473AMJgn1clu2k5TzcEGUUA063X7embI7BQDalRpFrp0t\\/k3mX+N5UENPYS56J7ky\\/OF9y+C6pkCQor4SR50p+PUU8qVogxwH1jMAEFqYVlrcBnAg2seOCdlGwGcJRVNJB+vurlsfq+fGeKnd9FHDjrF3Z819KuE\\/OFw==\",\"Ecom_Payment_Card_ExpDate_Month\":\"08\",\"storetype\":\"3d_pay\",\"iReqCode\":null,\"veresEnrolledStatus\":\"Y\",\"tulkekod\":\"tr\",\"mdErrorMsg\":\"Not authenticated\",\"PAResVerified\":\"false\",\"cavv\":null,\"id1\":\"a5\",\"digest\":\"digest\",\"callbackCall\":\"true\",\"failUrl\":\"http:\\/\\/localhost\\/enbiosis\\/public\\/checkout-result\",\"cavvAlgorithm\":null,\"Fpostakodu\":\"postakod34367\",\"price1\":\"6.25\",\"faturaFirma\":\"faturaFirma\",\"xid\":\"cJ+EHwNFz4GrjuIkKXwzZMsieMc=\",\"encoding\":\"ISO-8859-9\",\"oid\":null,\"mdStatus\":\"0\",\"dsId\":\"2\",\"eci\":null,\"version\":\"4.0\",\"Fadres2\":\"XXX\",\"Fismi\":\"is\",\"qty1\":\"3\",\"desc1\":\"a4 desc\",\"_charset_\":\"UTF-8\",\"HASH\":\"6TaqdrBxEEHaS\\/HE73qaJmXI5\\/g=\",\"rnd\":\"GCTy41v4nh12VAiu1biM\",\"HASHPARAMS\":\"clientid:oid:mdStatus:cavv:eci:md:rnd:\",\"HASHPARAMSVAL\":\"4009405350535576:7C93ED070A90ED0D06542A084092F1386FBA86F7868C640D113BF5E9F461A060:3782:##400940535GCTy41v4nh12VAiu1biM\"}', 'Declinedpayment', '2020-02-20 12:35:50', '2020-05-13 13:18:47'),
(10, '{\"TRANID\":null,\"PAResSyntaxOK\":\"true\",\"Filce\":\"\\u015ei\\u015fli\",\"firmaadi\":\"EnBiosis\",\"islemtipi\":\"Auth\",\"total1\":\"7.50\",\"lang\":\"en\",\"merchantID\":\"400940535\",\"maskedCreditCard\":\"5355 76** **** 8256\",\"amount\":\"1299\",\"sID\":\"2\",\"ACQBIN\":\"510138\",\"itemnumber1\":\"a1\",\"Ecom_Payment_Card_ExpDate_Year\":\"23\",\"storekey\":\"ENDIET19\",\"MaskedPan\":\"535576***8256\",\"Fadres\":\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\u015ei\\u015fli\\/Istanbul\",\"clientIp\":\"78.186.63.129\",\"iReqDetail\":null,\"okUrl\":\"http:\\/\\/localhost\\/enbiosis\\/public\\/checkout-result\",\"md\":\"535576:9DD59FCB139BB0A2C95B6BE26D4E04F97AAB10D68D8199563B59DDAF4BAB2D26:4214:##400940535\",\"clientId\":\"400940535\",\"vendorCode\":null,\"taksit\":\"2\",\"productcode1\":\"a2\",\"Fil\":\"Istanbul\",\"Ecom_Payment_Card_ExpDate_Month\":\"08\",\"storetype\":\"3d_pay\",\"iReqCode\":null,\"tulkekod\":\"tr\",\"mdErrorMsg\":\"Not authenticated\",\"PAResVerified\":\"false\",\"cavv\":null,\"id1\":\"a5\",\"digest\":\"digest\",\"callbackCall\":\"true\",\"failUrl\":\"http:\\/\\/localhost\\/enbiosis\\/public\\/checkout-result\",\"cavvAlgorithm\":null,\"Fpostakodu\":\"postakod34367\",\"price1\":\"6.25\",\"faturaFirma\":\"faturaFirma\",\"xid\":\"i3lxw2GMja+OrCo7iigXqw63rfs=\",\"encoding\":\"ISO-8859-9\",\"oid\":null,\"mdStatus\":\"0\",\"dsId\":\"2\",\"eci\":null,\"version\":\"2.0\",\"Fadres2\":\"XXX\",\"Fismi\":\"is\",\"qty1\":\"3\",\"desc1\":\"a4 desc\",\"txstatus\":\"N\",\"_charset_\":\"UTF-8\",\"HASH\":\"mf8pDVjsqU97hLxIwFasritMkfI=\",\"rnd\":\"5DZfGRJ1Pc4VJ1dMdXl+\",\"HASHPARAMS\":\"clientid:oid:mdStatus:cavv:eci:md:rnd:\",\"HASHPARAMSVAL\":\"4009405350535576:9DD59FCB139BB0A2C95B6BE26D4E04F97AAB10D68D8199563B59DDAF4BAB2D26:4214:##4009405355DZfGRJ1Pc4VJ1dMdXl+\"}', 'Declinedpayment', '2020-02-28 06:59:27', '2020-05-13 13:18:47'),
(11, '{\"TRANID\":null,\"PAResSyntaxOK\":\"true\",\"Filce\":\"\\u015ei\\u015fli\",\"firmaadi\":\"EnBiosis\",\"islemtipi\":\"Auth\",\"total1\":\"7.50\",\"lang\":\"en\",\"merchantID\":\"400940535\",\"maskedCreditCard\":\"5355 76** **** 8256\",\"amount\":\"1299\",\"sID\":\"2\",\"ACQBIN\":\"510138\",\"itemnumber1\":\"a1\",\"Ecom_Payment_Card_ExpDate_Year\":\"23\",\"storekey\":\"ENDIET19\",\"MaskedPan\":\"535576***8256\",\"Fadres\":\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\u015ei\\u015fli\\/Istanbul\",\"clientIp\":\"78.186.63.129\",\"iReqDetail\":null,\"okUrl\":\"http:\\/\\/localhost\\/enbiosis\\/public\\/checkout-result\",\"md\":\"535576:9B250F9B36E3FC534FDDABEA5D53EAD45423185ECA22F1C247970E064045E569:3922:##400940535\",\"clientId\":\"400940535\",\"vendorCode\":null,\"taksit\":\"3\",\"productcode1\":\"a2\",\"Fil\":\"Istanbul\",\"Ecom_Payment_Card_ExpDate_Month\":\"08\",\"storetype\":\"3d_pay\",\"iReqCode\":null,\"tulkekod\":\"tr\",\"mdErrorMsg\":\"Not authenticated\",\"PAResVerified\":\"false\",\"cavv\":null,\"id1\":\"a5\",\"digest\":\"digest\",\"callbackCall\":\"true\",\"failUrl\":\"http:\\/\\/localhost\\/enbiosis\\/public\\/checkout-result\",\"cavvAlgorithm\":null,\"Fpostakodu\":\"postakod34367\",\"price1\":\"6.25\",\"faturaFirma\":\"faturaFirma\",\"xid\":\"kqgK1lPrqnPnOj2pLdQ3nO7ahhg=\",\"encoding\":\"ISO-8859-9\",\"oid\":null,\"mdStatus\":\"0\",\"dsId\":\"2\",\"eci\":null,\"version\":\"2.0\",\"Fadres2\":\"XXX\",\"Fismi\":\"is\",\"qty1\":\"3\",\"desc1\":\"a4 desc\",\"txstatus\":\"N\",\"_charset_\":\"UTF-8\",\"HASH\":\"O5e2YFpik4EXcZb\\/z8QKcWBWfz8=\",\"rnd\":\"CnqfKgyiDBNbHyz9kOnX\",\"HASHPARAMS\":\"clientid:oid:mdStatus:cavv:eci:md:rnd:\",\"HASHPARAMSVAL\":\"4009405350535576:9B250F9B36E3FC534FDDABEA5D53EAD45423185ECA22F1C247970E064045E569:3922:##400940535CnqfKgyiDBNbHyz9kOnX\"}', 'Declinedpayment', '2020-02-28 07:01:09', '2020-05-13 13:18:47'),
(12, '{\"ReturnOid\":\"ORDER-20059NDKG04025225\",\"TRANID\":null,\"PAResSyntaxOK\":\"true\",\"Filce\":\"\\u015ei\\u015fli\",\"firmaadi\":\"EnBiosis\",\"islemtipi\":\"Auth\",\"total1\":\"7.50\",\"lang\":\"en\",\"merchantID\":\"400940535\",\"maskedCreditCard\":\"5101 38** **** 5104\",\"amount\":\"1299\",\"sID\":\"2\",\"ACQBIN\":\"510138\",\"itemnumber1\":\"a1\",\"Ecom_Payment_Card_ExpDate_Year\":\"22\",\"EXTRA_CARDBRAND\":\"MASTERCARD\",\"storekey\":\"ENDIET19\",\"MaskedPan\":\"510138***5104\",\"acqStan\":\"000085\",\"Fadres\":\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\u015ei\\u015fli\\/Istanbul\",\"clientIp\":\"78.186.63.129\",\"iReqDetail\":null,\"okUrl\":\"http:\\/\\/localhost\\/enbiosis\\/public\\/checkout-result\",\"md\":\"510138:04F5F71A5138CF2D2E8D4169D5FF928FC72C23CC2EEEC8B996D5CD4BF3359FA4:4438:##400940535\",\"ProcReturnCode\":\"12\",\"clientId\":\"400940535\",\"payResults_dsId\":\"2\",\"vendorCode\":null,\"taksit\":\"3\",\"TransId\":\"20059NDKG04025227\",\"EXTRA_TRXDATE\":\"20200228 13:03:10\",\"productcode1\":\"a2\",\"Fil\":\"Istanbul\",\"Ecom_Payment_Card_ExpDate_Month\":\"12\",\"storetype\":\"3d_pay\",\"iReqCode\":null,\"Response\":\"Declined\",\"tulkekod\":\"tr\",\"SettleId\":\"141\",\"mdErrorMsg\":\"Valid authentication attempt\",\"ErrMsg\":\"Transaction is not valid.\",\"EXTRA_SGKTUTAR\":\"1299.0\",\"PAResVerified\":\"false\",\"cavv\":\"hufWN9WLoNcoYwAAHyuSCRMAAAA=\",\"id1\":\"a5\",\"digest\":\"digest\",\"HostRefNum\":\"005913814697\",\"callbackCall\":\"true\",\"AuthCode\":null,\"failUrl\":\"http:\\/\\/localhost\\/enbiosis\\/public\\/checkout-result\",\"cavvAlgorithm\":\"3\",\"Fpostakodu\":\"postakod34367\",\"price1\":\"6.25\",\"faturaFirma\":\"faturaFirma\",\"xid\":\"uW04KVVfPTuFpExY038KWm\\/+emI=\",\"encoding\":\"ISO-8859-9\",\"oid\":null,\"mdStatus\":\"4\",\"dsId\":\"2\",\"EXTRA_TEBACIKLAMA\":\"B122354-Bonus olmayan i\\u015flemde taksit kul\",\"eci\":\"01\",\"version\":\"2.0\",\"Fadres2\":\"XXX\",\"Fismi\":\"is\",\"qty1\":\"3\",\"desc1\":\"a4 desc\",\"EXTRA_CARDISSUER\":\"T\\u00dcRK EKONOMI BANKASI A.S.\",\"clientid\":\"400940535\",\"txstatus\":\"A\",\"_charset_\":\"UTF-8\",\"HASH\":\"SRgN8tElxu1FmX6Sjt2uUWKxgTk=\",\"rnd\":\"WsHlPDurxkwPtfUkJaPB\",\"HASHPARAMS\":\"clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:\",\"HASHPARAMSVAL\":\"40094053512Declined4hufWN9WLoNcoYwAAHyuSCRMAAAA=01510138:04F5F71A5138CF2D2E8D4169D5FF928FC72C23CC2EEEC8B996D5CD4BF3359FA4:4438:##400940535WsHlPDurxkwPtfUkJaPB\"}', 'Declinedpayment', '2020-02-28 07:03:15', '2020-05-13 13:18:47'),
(13, '{\"ReturnOid\":\"ORDER-20059NKIC04026009\",\"TRANID\":null,\"PAResSyntaxOK\":\"true\",\"Filce\":\"\\u015ei\\u015fli\",\"firmaadi\":\"EnBiosis\",\"islemtipi\":\"Auth\",\"total1\":\"7.50\",\"lang\":\"en\",\"merchantID\":\"400940535\",\"maskedCreditCard\":\"5101 38** **** 5104\",\"amount\":\"0.10\",\"sID\":\"2\",\"ACQBIN\":\"510138\",\"itemnumber1\":\"a1\",\"Ecom_Payment_Card_ExpDate_Year\":\"22\",\"EXTRA_CARDBRAND\":\"MASTERCARD\",\"storekey\":\"ENDIET19\",\"MaskedPan\":\"510138***5104\",\"acqStan\":\"000086\",\"Fadres\":\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\u015ei\\u015fli\\/Istanbul\",\"EXTRA_CAVVRESULTCODE\":\"I\",\"clientIp\":\"78.186.63.129\",\"iReqDetail\":null,\"okUrl\":\"http:\\/\\/localhost\\/enbiosis\\/public\\/checkout-result\",\"md\":\"510138:129514D91DEF309B0B723FECFEFADB9B2B18F3F52642F00F56EDB931ADBC81E2:4364:##400940535\",\"ProcReturnCode\":\"14\",\"clientId\":\"400940535\",\"payResults_dsId\":\"2\",\"vendorCode\":null,\"taksit\":null,\"TransId\":\"20059NKID04026011\",\"EXTRA_TRXDATE\":\"20200228 13:10:08\",\"productcode1\":\"a2\",\"Fil\":\"Istanbul\",\"Ecom_Payment_Card_ExpDate_Month\":\"12\",\"storetype\":\"3d_pay\",\"iReqCode\":null,\"Response\":\"Declined\",\"tulkekod\":\"tr\",\"SettleId\":\"141\",\"mdErrorMsg\":\"Valid authentication attempt\",\"ErrMsg\":\"Invalid account number.\",\"EXTRA_SGKTUTAR\":\"0.1\",\"PAResVerified\":\"false\",\"cavv\":\"hufWN9WLoNcoYwAAH87VAnQAAAA=\",\"id1\":\"a5\",\"digest\":\"digest\",\"HostRefNum\":\"005913815042\",\"callbackCall\":\"true\",\"AuthCode\":null,\"failUrl\":\"http:\\/\\/localhost\\/enbiosis\\/public\\/checkout-result\",\"cavvAlgorithm\":\"3\",\"Fpostakodu\":\"postakod34367\",\"price1\":\"6.25\",\"faturaFirma\":\"faturaFirma\",\"xid\":\"ukSQ3+uAgk7fcZ9KYB78LKtjhQg=\",\"encoding\":\"ISO-8859-9\",\"oid\":null,\"mdStatus\":\"4\",\"dsId\":\"2\",\"EXTRA_TEBACIKLAMA\":\"B141296-\\u00d6DEME YAPILMAK ISTENEN KART BULU\",\"eci\":\"01\",\"version\":\"2.0\",\"Fadres2\":\"XXX\",\"Fismi\":\"is\",\"qty1\":\"3\",\"desc1\":\"a4 desc\",\"EXTRA_CARDISSUER\":\"T\\u00dcRK EKONOMI BANKASI A.S.\",\"clientid\":\"400940535\",\"txstatus\":\"A\",\"_charset_\":\"UTF-8\",\"HASH\":\"Yp5wQaDN\\/vrqN\\/raMrH7cUNVkWA=\",\"rnd\":\"+Tn39zV4uNkrtvJCo93p\",\"HASHPARAMS\":\"clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:\",\"HASHPARAMSVAL\":\"40094053514Declined4hufWN9WLoNcoYwAAH87VAnQAAAA=01510138:129514D91DEF309B0B723FECFEFADB9B2B18F3F52642F00F56EDB931ADBC81E2:4364:##400940535+Tn39zV4uNkrtvJCo93p\"}', 'Declinedpayment', '2020-02-28 07:10:12', '2020-05-13 13:18:47'),
(14, '{\"ReturnOid\":\"ORDER-20059NNWD04019328\",\"TRANID\":null,\"PAResSyntaxOK\":\"true\",\"Filce\":\"\\u015ei\\u015fli\",\"firmaadi\":\"EnBiosis\",\"islemtipi\":\"Auth\",\"total1\":\"7.50\",\"lang\":\"en\",\"merchantID\":\"400940535\",\"maskedCreditCard\":\"5101 38** **** 5104\",\"amount\":\"0.10\",\"sID\":\"2\",\"ACQBIN\":\"510138\",\"itemnumber1\":\"a1\",\"Ecom_Payment_Card_ExpDate_Year\":\"22\",\"EXTRA_CARDBRAND\":\"MASTERCARD\",\"storekey\":\"ENDIET19\",\"MaskedPan\":\"510138***5104\",\"acqStan\":\"000087\",\"Fadres\":\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\u015ei\\u015fli\\/Istanbul\",\"EXTRA_CAVVRESULTCODE\":\"I\",\"clientIp\":\"78.186.63.129\",\"iReqDetail\":null,\"okUrl\":\"http:\\/\\/localhost\\/enbiosis\\/public\\/checkout-result\",\"md\":\"510138:AE3162E788852D24ABFC9615F88850BEAF9373A283E4A6691B086A31541B9E9B:3993:##400940535\",\"ProcReturnCode\":\"14\",\"clientId\":\"400940535\",\"payResults_dsId\":\"2\",\"vendorCode\":null,\"taksit\":null,\"TransId\":\"20059NNWE04019330\",\"EXTRA_TRXDATE\":\"20200228 13:13:22\",\"productcode1\":\"a2\",\"Fil\":\"Istanbul\",\"Ecom_Payment_Card_ExpDate_Month\":\"12\",\"storetype\":\"3d_pay\",\"iReqCode\":null,\"Response\":\"Declined\",\"tulkekod\":\"tr\",\"SettleId\":\"141\",\"mdErrorMsg\":\"Valid authentication attempt\",\"ErrMsg\":\"Invalid account number.\",\"EXTRA_SGKTUTAR\":\"0.1\",\"PAResVerified\":\"false\",\"cavv\":\"hufWN9WLoNcoYwAAH9IxBkMAAAA=\",\"id1\":\"a5\",\"digest\":\"digest\",\"HostRefNum\":\"005913815186\",\"callbackCall\":\"true\",\"AuthCode\":null,\"failUrl\":\"http:\\/\\/localhost\\/enbiosis\\/public\\/checkout-result\",\"cavvAlgorithm\":\"3\",\"Fpostakodu\":\"postakod34367\",\"price1\":\"6.25\",\"faturaFirma\":\"faturaFirma\",\"xid\":\"+s4LLt73WbhNB936QXnRef7MuY4=\",\"encoding\":\"ISO-8859-9\",\"oid\":null,\"mdStatus\":\"4\",\"dsId\":\"2\",\"EXTRA_TEBACIKLAMA\":\"B141296-\\u00d6DEME YAPILMAK ISTENEN KART BULU\",\"eci\":\"01\",\"version\":\"2.0\",\"Fadres2\":\"XXX\",\"Fismi\":\"is\",\"qty1\":\"3\",\"desc1\":\"a4 desc\",\"EXTRA_CARDISSUER\":\"T\\u00dcRK EKONOMI BANKASI A.S.\",\"clientid\":\"400940535\",\"txstatus\":\"A\",\"_charset_\":\"UTF-8\",\"HASH\":\"oE0lSNkiMOnicU4WB9fV+Z45CPQ=\",\"rnd\":\"1TD7VTTnyuULSK7pozbR\",\"HASHPARAMS\":\"clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:\",\"HASHPARAMSVAL\":\"40094053514Declined4hufWN9WLoNcoYwAAH9IxBkMAAAA=01510138:AE3162E788852D24ABFC9615F88850BEAF9373A283E4A6691B086A31541B9E9B:3993:##4009405351TD7VTTnyuULSK7pozbR\"}', 'Declinedpayment', '2020-02-28 07:13:24', '2020-05-13 13:18:47'),
(15, '\"{\\\"ReturnOid\\\":\\\"ORDER-20059NNWD04019328\\\",\\\"TRANID\\\":null,\\\"PAResSyntaxOK\\\":\\\"true\\\",\\\"Filce\\\":\\\"\\\\u015ei\\\\u015fli\\\",\\\"firmaadi\\\":\\\"EnBiosis\\\",\\\"islemtipi\\\":\\\"Auth\\\",\\\"total1\\\":\\\"7.50\\\",\\\"lang\\\":\\\"en\\\",\\\"merchantID\\\":\\\"400940535\\\",\\\"maskedCreditCard\\\":\\\"5101 38** **** 5104\\\",\\\"amount\\\":\\\"0.10\\\",\\\"sID\\\":\\\"2\\\",\\\"ACQBIN\\\":\\\"510138\\\",\\\"itemnumber1\\\":\\\"a1\\\",\\\"Ecom_Payment_Card_ExpDate_Year\\\":\\\"22\\\",\\\"EXTRA_CARDBRAND\\\":\\\"MASTERCARD\\\",\\\"storekey\\\":\\\"ENDIET19\\\",\\\"MaskedPan\\\":\\\"510138***5104\\\",\\\"acqStan\\\":\\\"000087\\\",\\\"Fadres\\\":\\\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\\\u015ei\\\\u015fli\\\\\\/Istanbul\\\",\\\"EXTRA_CAVVRESULTCODE\\\":\\\"I\\\",\\\"clientIp\\\":\\\"78.186.63.129\\\",\\\"iReqDetail\\\":null,\\\"okUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"md\\\":\\\"510138:AE3162E788852D24ABFC9615F88850BEAF9373A283E4A6691B086A31541B9E9B:3993:##400940535\\\",\\\"ProcReturnCode\\\":\\\"14\\\",\\\"clientId\\\":\\\"400940535\\\",\\\"payResults_dsId\\\":\\\"2\\\",\\\"vendorCode\\\":null,\\\"taksit\\\":null,\\\"TransId\\\":\\\"20059NNWE04019330\\\",\\\"EXTRA_TRXDATE\\\":\\\"20200228 13:13:22\\\",\\\"productcode1\\\":\\\"a2\\\",\\\"Fil\\\":\\\"Istanbul\\\",\\\"Ecom_Payment_Card_ExpDate_Month\\\":\\\"12\\\",\\\"storetype\\\":\\\"3d_pay\\\",\\\"iReqCode\\\":null,\\\"Response\\\":\\\"Declined\\\",\\\"tulkekod\\\":\\\"tr\\\",\\\"SettleId\\\":\\\"141\\\",\\\"mdErrorMsg\\\":\\\"Valid authentication attempt\\\",\\\"ErrMsg\\\":\\\"Invalid account number.\\\",\\\"EXTRA_SGKTUTAR\\\":\\\"0.1\\\",\\\"PAResVerified\\\":\\\"false\\\",\\\"cavv\\\":\\\"hufWN9WLoNcoYwAAH9IxBkMAAAA=\\\",\\\"id1\\\":\\\"a5\\\",\\\"digest\\\":\\\"digest\\\",\\\"HostRefNum\\\":\\\"005913815186\\\",\\\"callbackCall\\\":\\\"true\\\",\\\"AuthCode\\\":null,\\\"failUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"cavvAlgorithm\\\":\\\"3\\\",\\\"Fpostakodu\\\":\\\"postakod34367\\\",\\\"price1\\\":\\\"6.25\\\",\\\"faturaFirma\\\":\\\"faturaFirma\\\",\\\"xid\\\":\\\"+s4LLt73WbhNB936QXnRef7MuY4=\\\",\\\"encoding\\\":\\\"ISO-8859-9\\\",\\\"oid\\\":null,\\\"mdStatus\\\":\\\"4\\\",\\\"dsId\\\":\\\"2\\\",\\\"EXTRA_TEBACIKLAMA\\\":\\\"B141296-\\\\u00d6DEME YAPILMAK ISTENEN KART BULU\\\",\\\"eci\\\":\\\"01\\\",\\\"version\\\":\\\"2.0\\\",\\\"Fadres2\\\":\\\"XXX\\\",\\\"Fismi\\\":\\\"is\\\",\\\"qty1\\\":\\\"3\\\",\\\"desc1\\\":\\\"a4 desc\\\",\\\"EXTRA_CARDISSUER\\\":\\\"T\\\\u00dcRK EKONOMI BANKASI A.S.\\\",\\\"clientid\\\":\\\"400940535\\\",\\\"txstatus\\\":\\\"A\\\",\\\"_charset_\\\":\\\"UTF-8\\\",\\\"HASH\\\":\\\"oE0lSNkiMOnicU4WB9fV+Z45CPQ=\\\",\\\"rnd\\\":\\\"1TD7VTTnyuULSK7pozbR\\\",\\\"HASHPARAMS\\\":\\\"clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:\\\",\\\"HASHPARAMSVAL\\\":\\\"40094053514Declined4hufWN9WLoNcoYwAAH9IxBkMAAAA=01510138:AE3162E788852D24ABFC9615F88850BEAF9373A283E4A6691B086A31541B9E9B:3993:##4009405351TD7VTTnyuULSK7pozbR\\\"}\"', 'Declinedpayment', '2020-02-28 07:14:13', '2020-05-13 13:18:47'),
(16, '\"{\\\"ReturnOid\\\":\\\"ORDER-20059NNWD04019328\\\",\\\"TRANID\\\":null,\\\"PAResSyntaxOK\\\":\\\"true\\\",\\\"Filce\\\":\\\"\\\\u015ei\\\\u015fli\\\",\\\"firmaadi\\\":\\\"EnBiosis\\\",\\\"islemtipi\\\":\\\"Auth\\\",\\\"total1\\\":\\\"7.50\\\",\\\"lang\\\":\\\"en\\\",\\\"merchantID\\\":\\\"400940535\\\",\\\"maskedCreditCard\\\":\\\"5101 38** **** 5104\\\",\\\"amount\\\":\\\"0.10\\\",\\\"sID\\\":\\\"2\\\",\\\"ACQBIN\\\":\\\"510138\\\",\\\"itemnumber1\\\":\\\"a1\\\",\\\"Ecom_Payment_Card_ExpDate_Year\\\":\\\"22\\\",\\\"EXTRA_CARDBRAND\\\":\\\"MASTERCARD\\\",\\\"storekey\\\":\\\"ENDIET19\\\",\\\"MaskedPan\\\":\\\"510138***5104\\\",\\\"acqStan\\\":\\\"000087\\\",\\\"Fadres\\\":\\\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\\\u015ei\\\\u015fli\\\\\\/Istanbul\\\",\\\"EXTRA_CAVVRESULTCODE\\\":\\\"I\\\",\\\"clientIp\\\":\\\"78.186.63.129\\\",\\\"iReqDetail\\\":null,\\\"okUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"md\\\":\\\"510138:AE3162E788852D24ABFC9615F88850BEAF9373A283E4A6691B086A31541B9E9B:3993:##400940535\\\",\\\"ProcReturnCode\\\":\\\"14\\\",\\\"clientId\\\":\\\"400940535\\\",\\\"payResults_dsId\\\":\\\"2\\\",\\\"vendorCode\\\":null,\\\"taksit\\\":null,\\\"TransId\\\":\\\"20059NNWE04019330\\\",\\\"EXTRA_TRXDATE\\\":\\\"20200228 13:13:22\\\",\\\"productcode1\\\":\\\"a2\\\",\\\"Fil\\\":\\\"Istanbul\\\",\\\"Ecom_Payment_Card_ExpDate_Month\\\":\\\"12\\\",\\\"storetype\\\":\\\"3d_pay\\\",\\\"iReqCode\\\":null,\\\"Response\\\":\\\"Declined\\\",\\\"tulkekod\\\":\\\"tr\\\",\\\"SettleId\\\":\\\"141\\\",\\\"mdErrorMsg\\\":\\\"Valid authentication attempt\\\",\\\"ErrMsg\\\":\\\"Invalid account number.\\\",\\\"EXTRA_SGKTUTAR\\\":\\\"0.1\\\",\\\"PAResVerified\\\":\\\"false\\\",\\\"cavv\\\":\\\"hufWN9WLoNcoYwAAH9IxBkMAAAA=\\\",\\\"id1\\\":\\\"a5\\\",\\\"digest\\\":\\\"digest\\\",\\\"HostRefNum\\\":\\\"005913815186\\\",\\\"callbackCall\\\":\\\"true\\\",\\\"AuthCode\\\":null,\\\"failUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"cavvAlgorithm\\\":\\\"3\\\",\\\"Fpostakodu\\\":\\\"postakod34367\\\",\\\"price1\\\":\\\"6.25\\\",\\\"faturaFirma\\\":\\\"faturaFirma\\\",\\\"xid\\\":\\\"+s4LLt73WbhNB936QXnRef7MuY4=\\\",\\\"encoding\\\":\\\"ISO-8859-9\\\",\\\"oid\\\":null,\\\"mdStatus\\\":\\\"4\\\",\\\"dsId\\\":\\\"2\\\",\\\"EXTRA_TEBACIKLAMA\\\":\\\"B141296-\\\\u00d6DEME YAPILMAK ISTENEN KART BULU\\\",\\\"eci\\\":\\\"01\\\",\\\"version\\\":\\\"2.0\\\",\\\"Fadres2\\\":\\\"XXX\\\",\\\"Fismi\\\":\\\"is\\\",\\\"qty1\\\":\\\"3\\\",\\\"desc1\\\":\\\"a4 desc\\\",\\\"EXTRA_CARDISSUER\\\":\\\"T\\\\u00dcRK EKONOMI BANKASI A.S.\\\",\\\"clientid\\\":\\\"400940535\\\",\\\"txstatus\\\":\\\"A\\\",\\\"_charset_\\\":\\\"UTF-8\\\",\\\"HASH\\\":\\\"oE0lSNkiMOnicU4WB9fV+Z45CPQ=\\\",\\\"rnd\\\":\\\"1TD7VTTnyuULSK7pozbR\\\",\\\"HASHPARAMS\\\":\\\"clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:\\\",\\\"HASHPARAMSVAL\\\":\\\"40094053514Declined4hufWN9WLoNcoYwAAH9IxBkMAAAA=01510138:AE3162E788852D24ABFC9615F88850BEAF9373A283E4A6691B086A31541B9E9B:3993:##4009405351TD7VTTnyuULSK7pozbR\\\"}\"', 'Declinedpayment', '2020-02-28 07:14:21', '2020-05-13 13:18:47'),
(17, '\"{\\\"ReturnOid\\\":\\\"ORDER-20059NNWD04019328\\\",\\\"TRANID\\\":null,\\\"PAResSyntaxOK\\\":\\\"true\\\",\\\"Filce\\\":\\\"\\\\u015ei\\\\u015fli\\\",\\\"firmaadi\\\":\\\"EnBiosis\\\",\\\"islemtipi\\\":\\\"Auth\\\",\\\"total1\\\":\\\"7.50\\\",\\\"lang\\\":\\\"en\\\",\\\"merchantID\\\":\\\"400940535\\\",\\\"maskedCreditCard\\\":\\\"5101 38** **** 5104\\\",\\\"amount\\\":\\\"0.10\\\",\\\"sID\\\":\\\"2\\\",\\\"ACQBIN\\\":\\\"510138\\\",\\\"itemnumber1\\\":\\\"a1\\\",\\\"Ecom_Payment_Card_ExpDate_Year\\\":\\\"22\\\",\\\"EXTRA_CARDBRAND\\\":\\\"MASTERCARD\\\",\\\"storekey\\\":\\\"ENDIET19\\\",\\\"MaskedPan\\\":\\\"510138***5104\\\",\\\"acqStan\\\":\\\"000087\\\",\\\"Fadres\\\":\\\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\\\u015ei\\\\u015fli\\\\\\/Istanbul\\\",\\\"EXTRA_CAVVRESULTCODE\\\":\\\"I\\\",\\\"clientIp\\\":\\\"78.186.63.129\\\",\\\"iReqDetail\\\":null,\\\"okUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"md\\\":\\\"510138:AE3162E788852D24ABFC9615F88850BEAF9373A283E4A6691B086A31541B9E9B:3993:##400940535\\\",\\\"ProcReturnCode\\\":\\\"14\\\",\\\"clientId\\\":\\\"400940535\\\",\\\"payResults_dsId\\\":\\\"2\\\",\\\"vendorCode\\\":null,\\\"taksit\\\":null,\\\"TransId\\\":\\\"20059NNWE04019330\\\",\\\"EXTRA_TRXDATE\\\":\\\"20200228 13:13:22\\\",\\\"productcode1\\\":\\\"a2\\\",\\\"Fil\\\":\\\"Istanbul\\\",\\\"Ecom_Payment_Card_ExpDate_Month\\\":\\\"12\\\",\\\"storetype\\\":\\\"3d_pay\\\",\\\"iReqCode\\\":null,\\\"Response\\\":\\\"Declined\\\",\\\"tulkekod\\\":\\\"tr\\\",\\\"SettleId\\\":\\\"141\\\",\\\"mdErrorMsg\\\":\\\"Valid authentication attempt\\\",\\\"ErrMsg\\\":\\\"Invalid account number.\\\",\\\"EXTRA_SGKTUTAR\\\":\\\"0.1\\\",\\\"PAResVerified\\\":\\\"false\\\",\\\"cavv\\\":\\\"hufWN9WLoNcoYwAAH9IxBkMAAAA=\\\",\\\"id1\\\":\\\"a5\\\",\\\"digest\\\":\\\"digest\\\",\\\"HostRefNum\\\":\\\"005913815186\\\",\\\"callbackCall\\\":\\\"true\\\",\\\"AuthCode\\\":null,\\\"failUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"cavvAlgorithm\\\":\\\"3\\\",\\\"Fpostakodu\\\":\\\"postakod34367\\\",\\\"price1\\\":\\\"6.25\\\",\\\"faturaFirma\\\":\\\"faturaFirma\\\",\\\"xid\\\":\\\"+s4LLt73WbhNB936QXnRef7MuY4=\\\",\\\"encoding\\\":\\\"ISO-8859-9\\\",\\\"oid\\\":null,\\\"mdStatus\\\":\\\"4\\\",\\\"dsId\\\":\\\"2\\\",\\\"EXTRA_TEBACIKLAMA\\\":\\\"B141296-\\\\u00d6DEME YAPILMAK ISTENEN KART BULU\\\",\\\"eci\\\":\\\"01\\\",\\\"version\\\":\\\"2.0\\\",\\\"Fadres2\\\":\\\"XXX\\\",\\\"Fismi\\\":\\\"is\\\",\\\"qty1\\\":\\\"3\\\",\\\"desc1\\\":\\\"a4 desc\\\",\\\"EXTRA_CARDISSUER\\\":\\\"T\\\\u00dcRK EKONOMI BANKASI A.S.\\\",\\\"clientid\\\":\\\"400940535\\\",\\\"txstatus\\\":\\\"A\\\",\\\"_charset_\\\":\\\"UTF-8\\\",\\\"HASH\\\":\\\"oE0lSNkiMOnicU4WB9fV+Z45CPQ=\\\",\\\"rnd\\\":\\\"1TD7VTTnyuULSK7pozbR\\\",\\\"HASHPARAMS\\\":\\\"clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:\\\",\\\"HASHPARAMSVAL\\\":\\\"40094053514Declined4hufWN9WLoNcoYwAAH9IxBkMAAAA=01510138:AE3162E788852D24ABFC9615F88850BEAF9373A283E4A6691B086A31541B9E9B:3993:##4009405351TD7VTTnyuULSK7pozbR\\\"}\"', 'Declinedpayment', '2020-02-28 07:14:24', '2020-05-13 13:18:47'),
(18, '\"{\\\"ReturnOid\\\":\\\"ORDER-20059NNWD04019328\\\",\\\"TRANID\\\":null,\\\"PAResSyntaxOK\\\":\\\"true\\\",\\\"Filce\\\":\\\"\\\\u015ei\\\\u015fli\\\",\\\"firmaadi\\\":\\\"EnBiosis\\\",\\\"islemtipi\\\":\\\"Auth\\\",\\\"total1\\\":\\\"7.50\\\",\\\"lang\\\":\\\"en\\\",\\\"merchantID\\\":\\\"400940535\\\",\\\"maskedCreditCard\\\":\\\"5101 38** **** 5104\\\",\\\"amount\\\":\\\"0.10\\\",\\\"sID\\\":\\\"2\\\",\\\"ACQBIN\\\":\\\"510138\\\",\\\"itemnumber1\\\":\\\"a1\\\",\\\"Ecom_Payment_Card_ExpDate_Year\\\":\\\"22\\\",\\\"EXTRA_CARDBRAND\\\":\\\"MASTERCARD\\\",\\\"storekey\\\":\\\"ENDIET19\\\",\\\"MaskedPan\\\":\\\"510138***5104\\\",\\\"acqStan\\\":\\\"000087\\\",\\\"Fadres\\\":\\\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\\\u015ei\\\\u015fli\\\\\\/Istanbul\\\",\\\"EXTRA_CAVVRESULTCODE\\\":\\\"I\\\",\\\"clientIp\\\":\\\"78.186.63.129\\\",\\\"iReqDetail\\\":null,\\\"okUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"md\\\":\\\"510138:AE3162E788852D24ABFC9615F88850BEAF9373A283E4A6691B086A31541B9E9B:3993:##400940535\\\",\\\"ProcReturnCode\\\":\\\"14\\\",\\\"clientId\\\":\\\"400940535\\\",\\\"payResults_dsId\\\":\\\"2\\\",\\\"vendorCode\\\":null,\\\"taksit\\\":null,\\\"TransId\\\":\\\"20059NNWE04019330\\\",\\\"EXTRA_TRXDATE\\\":\\\"20200228 13:13:22\\\",\\\"productcode1\\\":\\\"a2\\\",\\\"Fil\\\":\\\"Istanbul\\\",\\\"Ecom_Payment_Card_ExpDate_Month\\\":\\\"12\\\",\\\"storetype\\\":\\\"3d_pay\\\",\\\"iReqCode\\\":null,\\\"Response\\\":\\\"Declined\\\",\\\"tulkekod\\\":\\\"tr\\\",\\\"SettleId\\\":\\\"141\\\",\\\"mdErrorMsg\\\":\\\"Valid authentication attempt\\\",\\\"ErrMsg\\\":\\\"Invalid account number.\\\",\\\"EXTRA_SGKTUTAR\\\":\\\"0.1\\\",\\\"PAResVerified\\\":\\\"false\\\",\\\"cavv\\\":\\\"hufWN9WLoNcoYwAAH9IxBkMAAAA=\\\",\\\"id1\\\":\\\"a5\\\",\\\"digest\\\":\\\"digest\\\",\\\"HostRefNum\\\":\\\"005913815186\\\",\\\"callbackCall\\\":\\\"true\\\",\\\"AuthCode\\\":null,\\\"failUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"cavvAlgorithm\\\":\\\"3\\\",\\\"Fpostakodu\\\":\\\"postakod34367\\\",\\\"price1\\\":\\\"6.25\\\",\\\"faturaFirma\\\":\\\"faturaFirma\\\",\\\"xid\\\":\\\"+s4LLt73WbhNB936QXnRef7MuY4=\\\",\\\"encoding\\\":\\\"ISO-8859-9\\\",\\\"oid\\\":null,\\\"mdStatus\\\":\\\"4\\\",\\\"dsId\\\":\\\"2\\\",\\\"EXTRA_TEBACIKLAMA\\\":\\\"B141296-\\\\u00d6DEME YAPILMAK ISTENEN KART BULU\\\",\\\"eci\\\":\\\"01\\\",\\\"version\\\":\\\"2.0\\\",\\\"Fadres2\\\":\\\"XXX\\\",\\\"Fismi\\\":\\\"is\\\",\\\"qty1\\\":\\\"3\\\",\\\"desc1\\\":\\\"a4 desc\\\",\\\"EXTRA_CARDISSUER\\\":\\\"T\\\\u00dcRK EKONOMI BANKASI A.S.\\\",\\\"clientid\\\":\\\"400940535\\\",\\\"txstatus\\\":\\\"A\\\",\\\"_charset_\\\":\\\"UTF-8\\\",\\\"HASH\\\":\\\"oE0lSNkiMOnicU4WB9fV+Z45CPQ=\\\",\\\"rnd\\\":\\\"1TD7VTTnyuULSK7pozbR\\\",\\\"HASHPARAMS\\\":\\\"clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:\\\",\\\"HASHPARAMSVAL\\\":\\\"40094053514Declined4hufWN9WLoNcoYwAAH9IxBkMAAAA=01510138:AE3162E788852D24ABFC9615F88850BEAF9373A283E4A6691B086A31541B9E9B:3993:##4009405351TD7VTTnyuULSK7pozbR\\\"}\"', 'Declinedpayment', '2020-02-28 07:14:36', '2020-05-13 13:18:47'),
(19, '\"{\\\"ReturnOid\\\":\\\"ORDER-20059NOuG04026470\\\",\\\"TRANID\\\":null,\\\"PAResSyntaxOK\\\":\\\"true\\\",\\\"Filce\\\":\\\"\\\\u015ei\\\\u015fli\\\",\\\"firmaadi\\\":\\\"EnBiosis\\\",\\\"islemtipi\\\":\\\"Auth\\\",\\\"total1\\\":\\\"7.50\\\",\\\"lang\\\":\\\"en\\\",\\\"merchantID\\\":\\\"400940535\\\",\\\"maskedCreditCard\\\":\\\"5101 38** **** 5104\\\",\\\"amount\\\":\\\"0.10\\\",\\\"sID\\\":\\\"2\\\",\\\"ACQBIN\\\":\\\"510138\\\",\\\"itemnumber1\\\":\\\"a1\\\",\\\"Ecom_Payment_Card_ExpDate_Year\\\":\\\"22\\\",\\\"EXTRA_CARDBRAND\\\":\\\"MASTERCARD\\\",\\\"storekey\\\":\\\"ENDIET19\\\",\\\"MaskedPan\\\":\\\"510138***5104\\\",\\\"acqStan\\\":\\\"000088\\\",\\\"Fadres\\\":\\\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\\\u015ei\\\\u015fli\\\\\\/Istanbul\\\",\\\"EXTRA_CAVVRESULTCODE\\\":\\\"I\\\",\\\"clientIp\\\":\\\"78.186.63.129\\\",\\\"iReqDetail\\\":null,\\\"okUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"md\\\":\\\"510138:E28272328852D3C1FE4E0015DF7D7C1FD4DF9F17A1FAB3C2273ADAC627167DCD:4303:##400940535\\\",\\\"ProcReturnCode\\\":\\\"14\\\",\\\"clientId\\\":\\\"400940535\\\",\\\"payResults_dsId\\\":\\\"2\\\",\\\"vendorCode\\\":null,\\\"taksit\\\":null,\\\"TransId\\\":\\\"20059NOuG04026472\\\",\\\"EXTRA_TRXDATE\\\":\\\"20200228 13:14:46\\\",\\\"productcode1\\\":\\\"a2\\\",\\\"Fil\\\":\\\"Istanbul\\\",\\\"Ecom_Payment_Card_ExpDate_Month\\\":\\\"12\\\",\\\"storetype\\\":\\\"3d_pay\\\",\\\"iReqCode\\\":null,\\\"Response\\\":\\\"Declined\\\",\\\"tulkekod\\\":\\\"tr\\\",\\\"SettleId\\\":\\\"141\\\",\\\"mdErrorMsg\\\":\\\"Valid authentication attempt\\\",\\\"ErrMsg\\\":\\\"Invalid account number.\\\",\\\"EXTRA_SGKTUTAR\\\":\\\"0.1\\\",\\\"PAResVerified\\\":\\\"false\\\",\\\"cavv\\\":\\\"hufWN9WLoNcoYwAAHz+xAXcAAAA=\\\",\\\"id1\\\":\\\"a5\\\",\\\"digest\\\":\\\"digest\\\",\\\"HostRefNum\\\":\\\"005913815263\\\",\\\"callbackCall\\\":\\\"true\\\",\\\"AuthCode\\\":null,\\\"failUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"cavvAlgorithm\\\":\\\"3\\\",\\\"Fpostakodu\\\":\\\"postakod34367\\\",\\\"price1\\\":\\\"6.25\\\",\\\"faturaFirma\\\":\\\"faturaFirma\\\",\\\"xid\\\":\\\"3d7ioGssFn\\\\\\/TaosUAF9Fn+\\\\\\/Uqmc=\\\",\\\"encoding\\\":\\\"ISO-8859-9\\\",\\\"oid\\\":null,\\\"mdStatus\\\":\\\"4\\\",\\\"dsId\\\":\\\"2\\\",\\\"EXTRA_TEBACIKLAMA\\\":\\\"B141296-\\\\u00d6DEME YAPILMAK ISTENEN KART BULU\\\",\\\"eci\\\":\\\"01\\\",\\\"version\\\":\\\"2.0\\\",\\\"Fadres2\\\":\\\"XXX\\\",\\\"Fismi\\\":\\\"is\\\",\\\"qty1\\\":\\\"3\\\",\\\"desc1\\\":\\\"a4 desc\\\",\\\"EXTRA_CARDISSUER\\\":\\\"T\\\\u00dcRK EKONOMI BANKASI A.S.\\\",\\\"clientid\\\":\\\"400940535\\\",\\\"txstatus\\\":\\\"A\\\",\\\"_charset_\\\":\\\"UTF-8\\\",\\\"HASH\\\":\\\"ir\\\\\\/21NcX86qyNiygZHDTNmTQQq0=\\\",\\\"rnd\\\":\\\"ptGwWOcPeCWXbGxIGCcN\\\",\\\"HASHPARAMS\\\":\\\"clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:\\\",\\\"HASHPARAMSVAL\\\":\\\"40094053514Declined4hufWN9WLoNcoYwAAHz+xAXcAAAA=01510138:E28272328852D3C1FE4E0015DF7D7C1FD4DF9F17A1FAB3C2273ADAC627167DCD:4303:##400940535ptGwWOcPeCWXbGxIGCcN\\\"}\"', 'Declinedpayment', '2020-02-28 07:14:48', '2020-05-13 13:18:47'),
(20, '\"{\\\"ReturnOid\\\":\\\"ORDER-20059NOuG04026470\\\",\\\"TRANID\\\":null,\\\"PAResSyntaxOK\\\":\\\"true\\\",\\\"Filce\\\":\\\"\\\\u015ei\\\\u015fli\\\",\\\"firmaadi\\\":\\\"EnBiosis\\\",\\\"islemtipi\\\":\\\"Auth\\\",\\\"total1\\\":\\\"7.50\\\",\\\"lang\\\":\\\"en\\\",\\\"merchantID\\\":\\\"400940535\\\",\\\"maskedCreditCard\\\":\\\"5101 38** **** 5104\\\",\\\"amount\\\":\\\"0.10\\\",\\\"sID\\\":\\\"2\\\",\\\"ACQBIN\\\":\\\"510138\\\",\\\"itemnumber1\\\":\\\"a1\\\",\\\"Ecom_Payment_Card_ExpDate_Year\\\":\\\"22\\\",\\\"EXTRA_CARDBRAND\\\":\\\"MASTERCARD\\\",\\\"storekey\\\":\\\"ENDIET19\\\",\\\"MaskedPan\\\":\\\"510138***5104\\\",\\\"acqStan\\\":\\\"000088\\\",\\\"Fadres\\\":\\\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\\\u015ei\\\\u015fli\\\\\\/Istanbul\\\",\\\"EXTRA_CAVVRESULTCODE\\\":\\\"I\\\",\\\"clientIp\\\":\\\"78.186.63.129\\\",\\\"iReqDetail\\\":null,\\\"okUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"md\\\":\\\"510138:E28272328852D3C1FE4E0015DF7D7C1FD4DF9F17A1FAB3C2273ADAC627167DCD:4303:##400940535\\\",\\\"ProcReturnCode\\\":\\\"14\\\",\\\"clientId\\\":\\\"400940535\\\",\\\"payResults_dsId\\\":\\\"2\\\",\\\"vendorCode\\\":null,\\\"taksit\\\":null,\\\"TransId\\\":\\\"20059NOuG04026472\\\",\\\"EXTRA_TRXDATE\\\":\\\"20200228 13:14:46\\\",\\\"productcode1\\\":\\\"a2\\\",\\\"Fil\\\":\\\"Istanbul\\\",\\\"Ecom_Payment_Card_ExpDate_Month\\\":\\\"12\\\",\\\"storetype\\\":\\\"3d_pay\\\",\\\"iReqCode\\\":null,\\\"Response\\\":\\\"Declined\\\",\\\"tulkekod\\\":\\\"tr\\\",\\\"SettleId\\\":\\\"141\\\",\\\"mdErrorMsg\\\":\\\"Valid authentication attempt\\\",\\\"ErrMsg\\\":\\\"Invalid account number.\\\",\\\"EXTRA_SGKTUTAR\\\":\\\"0.1\\\",\\\"PAResVerified\\\":\\\"false\\\",\\\"cavv\\\":\\\"hufWN9WLoNcoYwAAHz+xAXcAAAA=\\\",\\\"id1\\\":\\\"a5\\\",\\\"digest\\\":\\\"digest\\\",\\\"HostRefNum\\\":\\\"005913815263\\\",\\\"callbackCall\\\":\\\"true\\\",\\\"AuthCode\\\":null,\\\"failUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"cavvAlgorithm\\\":\\\"3\\\",\\\"Fpostakodu\\\":\\\"postakod34367\\\",\\\"price1\\\":\\\"6.25\\\",\\\"faturaFirma\\\":\\\"faturaFirma\\\",\\\"xid\\\":\\\"3d7ioGssFn\\\\\\/TaosUAF9Fn+\\\\\\/Uqmc=\\\",\\\"encoding\\\":\\\"ISO-8859-9\\\",\\\"oid\\\":null,\\\"mdStatus\\\":\\\"4\\\",\\\"dsId\\\":\\\"2\\\",\\\"EXTRA_TEBACIKLAMA\\\":\\\"B141296-\\\\u00d6DEME YAPILMAK ISTENEN KART BULU\\\",\\\"eci\\\":\\\"01\\\",\\\"version\\\":\\\"2.0\\\",\\\"Fadres2\\\":\\\"XXX\\\",\\\"Fismi\\\":\\\"is\\\",\\\"qty1\\\":\\\"3\\\",\\\"desc1\\\":\\\"a4 desc\\\",\\\"EXTRA_CARDISSUER\\\":\\\"T\\\\u00dcRK EKONOMI BANKASI A.S.\\\",\\\"clientid\\\":\\\"400940535\\\",\\\"txstatus\\\":\\\"A\\\",\\\"_charset_\\\":\\\"UTF-8\\\",\\\"HASH\\\":\\\"ir\\\\\\/21NcX86qyNiygZHDTNmTQQq0=\\\",\\\"rnd\\\":\\\"ptGwWOcPeCWXbGxIGCcN\\\",\\\"HASHPARAMS\\\":\\\"clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:\\\",\\\"HASHPARAMSVAL\\\":\\\"40094053514Declined4hufWN9WLoNcoYwAAHz+xAXcAAAA=01510138:E28272328852D3C1FE4E0015DF7D7C1FD4DF9F17A1FAB3C2273ADAC627167DCD:4303:##400940535ptGwWOcPeCWXbGxIGCcN\\\"}\"', 'Declinedpayment', '2020-02-28 07:15:02', '2020-05-13 13:18:47'),
(21, '\"{\\\"ReturnOid\\\":\\\"ORDER-20059NWCI04010279\\\",\\\"TRANID\\\":null,\\\"PAResSyntaxOK\\\":\\\"true\\\",\\\"Filce\\\":\\\"\\\\u015ei\\\\u015fli\\\",\\\"firmaadi\\\":\\\"EnBiosis\\\",\\\"islemtipi\\\":\\\"Auth\\\",\\\"total1\\\":\\\"7.50\\\",\\\"lang\\\":\\\"en\\\",\\\"merchantID\\\":\\\"400940535\\\",\\\"maskedCreditCard\\\":\\\"5101 38** **** 5104\\\",\\\"amount\\\":\\\"0.10\\\",\\\"sID\\\":\\\"2\\\",\\\"ACQBIN\\\":\\\"510138\\\",\\\"itemnumber1\\\":\\\"a1\\\",\\\"Ecom_Payment_Card_ExpDate_Year\\\":\\\"22\\\",\\\"storekey\\\":\\\"ENDIET19\\\",\\\"MaskedPan\\\":\\\"510138***5104\\\",\\\"Fadres\\\":\\\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\\\u015ei\\\\u015fli\\\\\\/Istanbul\\\",\\\"clientIp\\\":\\\"78.186.63.129\\\",\\\"iReqDetail\\\":null,\\\"okUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"md\\\":\\\"510138:21B1034371F7B4989C1DE08C306886DC64BCAC3DB1817AB754B7BD91B2D35EF5:4488:##400940535\\\",\\\"ProcReturnCode\\\":\\\"99\\\",\\\"clientId\\\":\\\"400940535\\\",\\\"payResults_dsId\\\":\\\"2\\\",\\\"vendorCode\\\":null,\\\"taksit\\\":null,\\\"TransId\\\":\\\"20059NWCI04010281\\\",\\\"EXTRA_TRXDATE\\\":\\\"20200228 13:22:02\\\",\\\"productcode1\\\":\\\"a2\\\",\\\"Fil\\\":\\\"Istanbul\\\",\\\"Ecom_Payment_Card_ExpDate_Month\\\":\\\"12\\\",\\\"storetype\\\":\\\"3d_pay\\\",\\\"iReqCode\\\":null,\\\"Response\\\":\\\"Declined\\\",\\\"tulkekod\\\":\\\"tr\\\",\\\"SettleId\\\":null,\\\"mdErrorMsg\\\":\\\"Valid authentication attempt\\\",\\\"ErrMsg\\\":\\\"This request can be fraud. Pan (Credit Card Number) couldn\'t pass fraud controls.\\\",\\\"PAResVerified\\\":\\\"false\\\",\\\"cavv\\\":\\\"hufWN9WLoNcoYwAAH9x6BQcAAAA=\\\",\\\"id1\\\":\\\"a5\\\",\\\"digest\\\":\\\"digest\\\",\\\"HostRefNum\\\":null,\\\"callbackCall\\\":\\\"true\\\",\\\"AuthCode\\\":null,\\\"failUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"cavvAlgorithm\\\":\\\"3\\\",\\\"Fpostakodu\\\":\\\"postakod34367\\\",\\\"price1\\\":\\\"6.25\\\",\\\"faturaFirma\\\":\\\"faturaFirma\\\",\\\"xid\\\":\\\"Raw5uZYwjRrnWvzk5LqNoUXbND8=\\\",\\\"encoding\\\":\\\"ISO-8859-9\\\",\\\"oid\\\":null,\\\"mdStatus\\\":\\\"4\\\",\\\"dsId\\\":\\\"2\\\",\\\"eci\\\":\\\"01\\\",\\\"version\\\":\\\"2.0\\\",\\\"Fadres2\\\":\\\"XXX\\\",\\\"Fismi\\\":\\\"is\\\",\\\"qty1\\\":\\\"3\\\",\\\"desc1\\\":\\\"a4 desc\\\",\\\"clientid\\\":\\\"400940535\\\",\\\"txstatus\\\":\\\"A\\\",\\\"_charset_\\\":\\\"UTF-8\\\",\\\"HASH\\\":\\\"XbmEseVOrs68HEQ1lVitRk2WJVE=\\\",\\\"rnd\\\":\\\"8uY2EiRoaGULplTFdplN\\\",\\\"HASHPARAMS\\\":\\\"clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:\\\",\\\"HASHPARAMSVAL\\\":\\\"40094053599Declined4hufWN9WLoNcoYwAAH9x6BQcAAAA=01510138:21B1034371F7B4989C1DE08C306886DC64BCAC3DB1817AB754B7BD91B2D35EF5:4488:##4009405358uY2EiRoaGULplTFdplN\\\"}\"', 'Declinedpayment', '2020-02-28 07:22:04', '2020-05-13 13:18:47'),
(22, '\"{\\\"ReturnOid\\\":\\\"ORDER-20059NXpA04027445\\\",\\\"TRANID\\\":null,\\\"PAResSyntaxOK\\\":\\\"true\\\",\\\"Filce\\\":\\\"\\\\u015ei\\\\u015fli\\\",\\\"firmaadi\\\":\\\"EnBiosis\\\",\\\"islemtipi\\\":\\\"Auth\\\",\\\"total1\\\":\\\"7.50\\\",\\\"lang\\\":\\\"en\\\",\\\"merchantID\\\":\\\"400940535\\\",\\\"maskedCreditCard\\\":\\\"5101 38** **** 5104\\\",\\\"amount\\\":\\\"0.10\\\",\\\"sID\\\":\\\"2\\\",\\\"ACQBIN\\\":\\\"510138\\\",\\\"itemnumber1\\\":\\\"a1\\\",\\\"Ecom_Payment_Card_ExpDate_Year\\\":\\\"22\\\",\\\"storekey\\\":\\\"ENDIET19\\\",\\\"MaskedPan\\\":\\\"510138***5104\\\",\\\"Fadres\\\":\\\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\\\u015ei\\\\u015fli\\\\\\/Istanbul\\\",\\\"clientIp\\\":\\\"78.186.63.129\\\",\\\"iReqDetail\\\":null,\\\"okUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"md\\\":\\\"510138:0A28DAB8DA8800525AAB8A9EBB7219A922B176620EBD0CBFD9714C88B3803B5B:3772:##400940535\\\",\\\"ProcReturnCode\\\":\\\"99\\\",\\\"clientId\\\":\\\"400940535\\\",\\\"payResults_dsId\\\":\\\"2\\\",\\\"vendorCode\\\":null,\\\"taksit\\\":null,\\\"TransId\\\":\\\"20059NXpA04027447\\\",\\\"EXTRA_TRXDATE\\\":\\\"20200228 13:23:41\\\",\\\"productcode1\\\":\\\"a2\\\",\\\"Fil\\\":\\\"Istanbul\\\",\\\"Ecom_Payment_Card_ExpDate_Month\\\":\\\"12\\\",\\\"storetype\\\":\\\"3d_pay\\\",\\\"iReqCode\\\":null,\\\"Response\\\":\\\"Declined\\\",\\\"tulkekod\\\":\\\"tr\\\",\\\"SettleId\\\":null,\\\"mdErrorMsg\\\":\\\"Valid authentication attempt\\\",\\\"ErrMsg\\\":\\\"This request can be fraud. Pan (Credit Card Number) couldn\'t pass fraud controls.\\\",\\\"PAResVerified\\\":\\\"false\\\",\\\"cavv\\\":\\\"hufWN9WLoNcoYwAAH0wpCDAAAAA=\\\",\\\"id1\\\":\\\"a5\\\",\\\"digest\\\":\\\"digest\\\",\\\"HostRefNum\\\":null,\\\"callbackCall\\\":\\\"true\\\",\\\"AuthCode\\\":null,\\\"failUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"cavvAlgorithm\\\":\\\"3\\\",\\\"Fpostakodu\\\":\\\"postakod34367\\\",\\\"price1\\\":\\\"6.25\\\",\\\"faturaFirma\\\":\\\"faturaFirma\\\",\\\"xid\\\":\\\"2l9ulnDBMP+5gR3KAEzgwExCWlA=\\\",\\\"encoding\\\":\\\"ISO-8859-9\\\",\\\"oid\\\":null,\\\"mdStatus\\\":\\\"4\\\",\\\"dsId\\\":\\\"2\\\",\\\"eci\\\":\\\"01\\\",\\\"version\\\":\\\"2.0\\\",\\\"Fadres2\\\":\\\"XXX\\\",\\\"Fismi\\\":\\\"is\\\",\\\"qty1\\\":\\\"3\\\",\\\"desc1\\\":\\\"a4 desc\\\",\\\"clientid\\\":\\\"400940535\\\",\\\"txstatus\\\":\\\"A\\\",\\\"_charset_\\\":\\\"UTF-8\\\",\\\"HASH\\\":\\\"HGn9VZ\\\\\\/w7+ZlRyosVzuknJBqHgQ=\\\",\\\"rnd\\\":\\\"vNDos3gTlV\\\\\\/pOUyvhQ+O\\\",\\\"HASHPARAMS\\\":\\\"clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:\\\",\\\"HASHPARAMSVAL\\\":\\\"40094053599Declined4hufWN9WLoNcoYwAAH0wpCDAAAAA=01510138:0A28DAB8DA8800525AAB8A9EBB7219A922B176620EBD0CBFD9714C88B3803B5B:3772:##400940535vNDos3gTlV\\\\\\/pOUyvhQ+O\\\"}\"', 'Declinedpayment', '2020-02-28 07:23:42', '2020-05-13 13:18:47'),
(23, '\"{\\\"Filce\\\":\\\"\\\\u015ei\\\\u015fli\\\",\\\"firmaadi\\\":\\\"EnBiosis\\\",\\\"islemtipi\\\":\\\"Auth\\\",\\\"total1\\\":\\\"7.50\\\",\\\"lang\\\":\\\"en\\\",\\\"maskedCreditCard\\\":\\\"4402 93** **** 4406\\\",\\\"sstResponse\\\":\\\"2:CARDHOLDER_NOT_FOUND\\\",\\\"amount\\\":\\\"0.10\\\",\\\"itemnumber1\\\":\\\"a1\\\",\\\"Ecom_Payment_Card_ExpDate_Year\\\":\\\"22\\\",\\\"storekey\\\":\\\"ENDIET19\\\",\\\"MaskedPan\\\":\\\"440293***4406\\\",\\\"Fadres\\\":\\\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\\\u015ei\\\\u015fli\\\\\\/Istanbul\\\",\\\"clientIp\\\":\\\"78.186.63.129\\\",\\\"okUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"md\\\":\\\"440293:6329EBE43BF64A27F1200C7462A5976F2D7B7685B8B4F8C5F3565510BC11C64C:4084:##400940535\\\",\\\"ProcReturnCode\\\":\\\"TROY-0007\\\",\\\"clientId\\\":\\\"400940535\\\",\\\"taksit\\\":null,\\\"mdErrMsg\\\":\\\"2:CARDHOLDER_NOT_FOUND\\\",\\\"productcode1\\\":\\\"a2\\\",\\\"Fil\\\":\\\"Istanbul\\\",\\\"Ecom_Payment_Card_ExpDate_Month\\\":\\\"12\\\",\\\"storetype\\\":\\\"3d_pay\\\",\\\"Response\\\":\\\"Declined\\\",\\\"tulkekod\\\":\\\"tr\\\",\\\"mdErrorMsg\\\":\\\"2:CARDHOLDER_NOT_FOUND\\\",\\\"ErrMsg\\\":\\\"Issuer Exception\\\",\\\"cavv\\\":null,\\\"id1\\\":\\\"a5\\\",\\\"failUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"callbackCall\\\":\\\"true\\\",\\\"Fpostakodu\\\":\\\"postakod34367\\\",\\\"price1\\\":\\\"6.25\\\",\\\"faturaFirma\\\":\\\"faturaFirma\\\",\\\"xid\\\":\\\"rY12VCfZIdJaw8stj7eeW01uYnU=\\\",\\\"encoding\\\":\\\"ISO-8859-9\\\",\\\"currency\\\":\\\"949\\\",\\\"oid\\\":null,\\\"mdStatus\\\":\\\"0\\\",\\\"eci\\\":\\\"00\\\",\\\"Fadres2\\\":\\\"XXX\\\",\\\"Fismi\\\":\\\"is\\\",\\\"qty1\\\":\\\"3\\\",\\\"desc1\\\":\\\"a4 desc\\\",\\\"HASH\\\":\\\"Wu5Pd6vyGypbcv9a59FmxU0baqA=\\\",\\\"rnd\\\":\\\"1tmanN4DYTCGOJes4414\\\",\\\"HASHPARAMS\\\":\\\"clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:\\\",\\\"HASHPARAMSVAL\\\":\\\"400940535TROY-0007Declined000440293:6329EBE43BF64A27F1200C7462A5976F2D7B7685B8B4F8C5F3565510BC11C64C:4084:##4009405351tmanN4DYTCGOJes4414\\\"}\"', 'Declinedpayment', '2020-02-28 07:25:56', '2020-05-13 13:18:47'),
(24, '\"{\\\"Filce\\\":\\\"\\\\u015ei\\\\u015fli\\\",\\\"firmaadi\\\":\\\"EnBiosis\\\",\\\"islemtipi\\\":\\\"Auth\\\",\\\"total1\\\":\\\"7.50\\\",\\\"lang\\\":\\\"en\\\",\\\"maskedCreditCard\\\":\\\"4402 93** **** 4406\\\",\\\"sstResponse\\\":\\\"2:CARDHOLDER_NOT_FOUND\\\",\\\"amount\\\":\\\"0.10\\\",\\\"itemnumber1\\\":\\\"a1\\\",\\\"Ecom_Payment_Card_ExpDate_Year\\\":\\\"22\\\",\\\"storekey\\\":\\\"ENDIET19\\\",\\\"MaskedPan\\\":\\\"440293***4406\\\",\\\"Fadres\\\":\\\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\\\u015ei\\\\u015fli\\\\\\/Istanbul\\\",\\\"clientIp\\\":\\\"78.186.63.129\\\",\\\"okUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"md\\\":\\\"440293:6329EBE43BF64A27F1200C7462A5976F2D7B7685B8B4F8C5F3565510BC11C64C:4084:##400940535\\\",\\\"ProcReturnCode\\\":\\\"TROY-0007\\\",\\\"clientId\\\":\\\"400940535\\\",\\\"taksit\\\":null,\\\"mdErrMsg\\\":\\\"2:CARDHOLDER_NOT_FOUND\\\",\\\"productcode1\\\":\\\"a2\\\",\\\"Fil\\\":\\\"Istanbul\\\",\\\"Ecom_Payment_Card_ExpDate_Month\\\":\\\"12\\\",\\\"storetype\\\":\\\"3d_pay\\\",\\\"Response\\\":\\\"Declined\\\",\\\"tulkekod\\\":\\\"tr\\\",\\\"mdErrorMsg\\\":\\\"2:CARDHOLDER_NOT_FOUND\\\",\\\"ErrMsg\\\":\\\"Issuer Exception\\\",\\\"cavv\\\":null,\\\"id1\\\":\\\"a5\\\",\\\"failUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"callbackCall\\\":\\\"true\\\",\\\"Fpostakodu\\\":\\\"postakod34367\\\",\\\"price1\\\":\\\"6.25\\\",\\\"faturaFirma\\\":\\\"faturaFirma\\\",\\\"xid\\\":\\\"rY12VCfZIdJaw8stj7eeW01uYnU=\\\",\\\"encoding\\\":\\\"ISO-8859-9\\\",\\\"currency\\\":\\\"949\\\",\\\"oid\\\":null,\\\"mdStatus\\\":\\\"0\\\",\\\"eci\\\":\\\"00\\\",\\\"Fadres2\\\":\\\"XXX\\\",\\\"Fismi\\\":\\\"is\\\",\\\"qty1\\\":\\\"3\\\",\\\"desc1\\\":\\\"a4 desc\\\",\\\"HASH\\\":\\\"Wu5Pd6vyGypbcv9a59FmxU0baqA=\\\",\\\"rnd\\\":\\\"1tmanN4DYTCGOJes4414\\\",\\\"HASHPARAMS\\\":\\\"clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:\\\",\\\"HASHPARAMSVAL\\\":\\\"400940535TROY-0007Declined000440293:6329EBE43BF64A27F1200C7462A5976F2D7B7685B8B4F8C5F3565510BC11C64C:4084:##4009405351tmanN4DYTCGOJes4414\\\"}\"', 'Declinedpayment', '2020-02-28 07:27:25', '2020-05-13 13:18:47');
INSERT INTO `errors` (`id`, `json_string`, `model`, `created_at`, `updated_at`) VALUES
(25, '\"{\\\"ReturnOid\\\":\\\"ORDER-20059NXpA04027445\\\",\\\"TRANID\\\":null,\\\"PAResSyntaxOK\\\":\\\"true\\\",\\\"Filce\\\":\\\"\\\\u015ei\\\\u015fli\\\",\\\"firmaadi\\\":\\\"EnBiosis\\\",\\\"islemtipi\\\":\\\"Auth\\\",\\\"total1\\\":\\\"7.50\\\",\\\"lang\\\":\\\"en\\\",\\\"merchantID\\\":\\\"400940535\\\",\\\"maskedCreditCard\\\":\\\"5101 38** **** 5104\\\",\\\"amount\\\":\\\"0.10\\\",\\\"sID\\\":\\\"2\\\",\\\"ACQBIN\\\":\\\"510138\\\",\\\"itemnumber1\\\":\\\"a1\\\",\\\"Ecom_Payment_Card_ExpDate_Year\\\":\\\"22\\\",\\\"storekey\\\":\\\"ENDIET19\\\",\\\"MaskedPan\\\":\\\"510138***5104\\\",\\\"Fadres\\\":\\\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\\\u015ei\\\\u015fli\\\\\\/Istanbul\\\",\\\"clientIp\\\":\\\"78.186.63.129\\\",\\\"iReqDetail\\\":null,\\\"okUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"md\\\":\\\"510138:0A28DAB8DA8800525AAB8A9EBB7219A922B176620EBD0CBFD9714C88B3803B5B:3772:##400940535\\\",\\\"ProcReturnCode\\\":\\\"99\\\",\\\"clientId\\\":\\\"400940535\\\",\\\"payResults_dsId\\\":\\\"2\\\",\\\"vendorCode\\\":null,\\\"taksit\\\":null,\\\"TransId\\\":\\\"20059NXpA04027447\\\",\\\"EXTRA_TRXDATE\\\":\\\"20200228 13:23:41\\\",\\\"productcode1\\\":\\\"a2\\\",\\\"Fil\\\":\\\"Istanbul\\\",\\\"Ecom_Payment_Card_ExpDate_Month\\\":\\\"12\\\",\\\"storetype\\\":\\\"3d_pay\\\",\\\"iReqCode\\\":null,\\\"Response\\\":\\\"Declined\\\",\\\"tulkekod\\\":\\\"tr\\\",\\\"SettleId\\\":null,\\\"mdErrorMsg\\\":\\\"Valid authentication attempt\\\",\\\"ErrMsg\\\":\\\"This request can be fraud. Pan (Credit Card Number) couldn\'t pass fraud controls.\\\",\\\"PAResVerified\\\":\\\"false\\\",\\\"cavv\\\":\\\"hufWN9WLoNcoYwAAH0wpCDAAAAA=\\\",\\\"id1\\\":\\\"a5\\\",\\\"digest\\\":\\\"digest\\\",\\\"HostRefNum\\\":null,\\\"callbackCall\\\":\\\"true\\\",\\\"AuthCode\\\":null,\\\"failUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"cavvAlgorithm\\\":\\\"3\\\",\\\"Fpostakodu\\\":\\\"postakod34367\\\",\\\"price1\\\":\\\"6.25\\\",\\\"faturaFirma\\\":\\\"faturaFirma\\\",\\\"xid\\\":\\\"2l9ulnDBMP+5gR3KAEzgwExCWlA=\\\",\\\"encoding\\\":\\\"ISO-8859-9\\\",\\\"oid\\\":null,\\\"mdStatus\\\":\\\"4\\\",\\\"dsId\\\":\\\"2\\\",\\\"eci\\\":\\\"01\\\",\\\"version\\\":\\\"2.0\\\",\\\"Fadres2\\\":\\\"XXX\\\",\\\"Fismi\\\":\\\"is\\\",\\\"qty1\\\":\\\"3\\\",\\\"desc1\\\":\\\"a4 desc\\\",\\\"clientid\\\":\\\"400940535\\\",\\\"txstatus\\\":\\\"A\\\",\\\"_charset_\\\":\\\"UTF-8\\\",\\\"HASH\\\":\\\"HGn9VZ\\\\\\/w7+ZlRyosVzuknJBqHgQ=\\\",\\\"rnd\\\":\\\"vNDos3gTlV\\\\\\/pOUyvhQ+O\\\",\\\"HASHPARAMS\\\":\\\"clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:\\\",\\\"HASHPARAMSVAL\\\":\\\"40094053599Declined4hufWN9WLoNcoYwAAH0wpCDAAAAA=01510138:0A28DAB8DA8800525AAB8A9EBB7219A922B176620EBD0CBFD9714C88B3803B5B:3772:##400940535vNDos3gTlV\\\\\\/pOUyvhQ+O\\\"}\"', 'Declinedpayment', '2020-02-28 07:27:27', '2020-05-13 13:18:47'),
(26, '\"{\\\"ReturnOid\\\":\\\"ORDER-20059QHDB04010188\\\",\\\"TRANID\\\":null,\\\"PAResSyntaxOK\\\":\\\"true\\\",\\\"Filce\\\":\\\"\\\\u015ei\\\\u015fli\\\",\\\"firmaadi\\\":\\\"EnBiosis\\\",\\\"islemtipi\\\":\\\"Auth\\\",\\\"total1\\\":\\\"7.50\\\",\\\"lang\\\":\\\"en\\\",\\\"merchantID\\\":\\\"400940535\\\",\\\"maskedCreditCard\\\":\\\"5101 38** **** 5104\\\",\\\"amount\\\":\\\"0.10\\\",\\\"sID\\\":\\\"2\\\",\\\"ACQBIN\\\":\\\"510138\\\",\\\"itemnumber1\\\":\\\"a1\\\",\\\"Ecom_Payment_Card_ExpDate_Year\\\":\\\"23\\\",\\\"EXTRA_CARDBRAND\\\":\\\"MASTERCARD\\\",\\\"storekey\\\":\\\"ENDIET19\\\",\\\"MaskedPan\\\":\\\"510138***5104\\\",\\\"acqStan\\\":\\\"000090\\\",\\\"Fadres\\\":\\\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\\\u015ei\\\\u015fli\\\\\\/Istanbul\\\",\\\"EXTRA_CAVVRESULTCODE\\\":\\\"I\\\",\\\"clientIp\\\":\\\"78.186.63.129\\\",\\\"iReqDetail\\\":null,\\\"okUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"md\\\":\\\"510138:FCE3F8756CB99C6484FEE44594638679257EAD4E6ECA86420A0D9D207779638D:4196:##400940535\\\",\\\"ProcReturnCode\\\":\\\"14\\\",\\\"clientId\\\":\\\"400940535\\\",\\\"payResults_dsId\\\":\\\"2\\\",\\\"vendorCode\\\":null,\\\"taksit\\\":null,\\\"TransId\\\":\\\"20059QHDC04010190\\\",\\\"EXTRA_TRXDATE\\\":\\\"20200228 16:07:03\\\",\\\"productcode1\\\":\\\"a2\\\",\\\"Fil\\\":\\\"Istanbul\\\",\\\"Ecom_Payment_Card_ExpDate_Month\\\":\\\"08\\\",\\\"storetype\\\":\\\"3d_pay\\\",\\\"iReqCode\\\":null,\\\"Response\\\":\\\"Declined\\\",\\\"tulkekod\\\":\\\"tr\\\",\\\"SettleId\\\":\\\"141\\\",\\\"mdErrorMsg\\\":\\\"Valid authentication attempt\\\",\\\"ErrMsg\\\":\\\"Invalid account number.\\\",\\\"EXTRA_SGKTUTAR\\\":\\\"0.1\\\",\\\"PAResVerified\\\":\\\"false\\\",\\\"cavv\\\":\\\"hufWN9WLoNcoYwAAINsLBTQAAAA=\\\",\\\"id1\\\":\\\"a5\\\",\\\"digest\\\":\\\"digest\\\",\\\"HostRefNum\\\":\\\"005916824995\\\",\\\"callbackCall\\\":\\\"true\\\",\\\"AuthCode\\\":null,\\\"failUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"cavvAlgorithm\\\":\\\"3\\\",\\\"Fpostakodu\\\":\\\"postakod34367\\\",\\\"price1\\\":\\\"6.25\\\",\\\"faturaFirma\\\":\\\"faturaFirma\\\",\\\"xid\\\":\\\"bVZ6Un3EW4bFQvMlfXR4Nc3TOEI=\\\",\\\"encoding\\\":\\\"ISO-8859-9\\\",\\\"oid\\\":null,\\\"mdStatus\\\":\\\"4\\\",\\\"dsId\\\":\\\"2\\\",\\\"EXTRA_TEBACIKLAMA\\\":\\\"B141296-\\\\u00d6DEME YAPILMAK ISTENEN KART BULU\\\",\\\"eci\\\":\\\"01\\\",\\\"version\\\":\\\"2.0\\\",\\\"Fadres2\\\":\\\"XXX\\\",\\\"Fismi\\\":\\\"is\\\",\\\"qty1\\\":\\\"3\\\",\\\"desc1\\\":\\\"a4 desc\\\",\\\"EXTRA_CARDISSUER\\\":\\\"T\\\\u00dcRK EKONOMI BANKASI A.S.\\\",\\\"clientid\\\":\\\"400940535\\\",\\\"txstatus\\\":\\\"A\\\",\\\"_charset_\\\":\\\"UTF-8\\\",\\\"HASH\\\":\\\"tIw+T4c8ZW8eC7FLq+QA2BkEsAc=\\\",\\\"rnd\\\":\\\"Qz0uFXNEvReqGumE8K8y\\\",\\\"HASHPARAMS\\\":\\\"clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:\\\",\\\"HASHPARAMSVAL\\\":\\\"40094053514Declined4hufWN9WLoNcoYwAAINsLBTQAAAA=01510138:FCE3F8756CB99C6484FEE44594638679257EAD4E6ECA86420A0D9D207779638D:4196:##400940535Qz0uFXNEvReqGumE8K8y\\\"}\"', 'Declinedpayment', '2020-02-28 10:07:05', '2020-05-13 13:18:47'),
(27, '\"{\\\"ReturnOid\\\":\\\"ORDER-20059QHDB04010188\\\",\\\"TRANID\\\":null,\\\"PAResSyntaxOK\\\":\\\"true\\\",\\\"Filce\\\":\\\"\\\\u015ei\\\\u015fli\\\",\\\"firmaadi\\\":\\\"EnBiosis\\\",\\\"islemtipi\\\":\\\"Auth\\\",\\\"total1\\\":\\\"7.50\\\",\\\"lang\\\":\\\"en\\\",\\\"merchantID\\\":\\\"400940535\\\",\\\"maskedCreditCard\\\":\\\"5101 38** **** 5104\\\",\\\"amount\\\":\\\"0.10\\\",\\\"sID\\\":\\\"2\\\",\\\"ACQBIN\\\":\\\"510138\\\",\\\"itemnumber1\\\":\\\"a1\\\",\\\"Ecom_Payment_Card_ExpDate_Year\\\":\\\"23\\\",\\\"EXTRA_CARDBRAND\\\":\\\"MASTERCARD\\\",\\\"storekey\\\":\\\"ENDIET19\\\",\\\"MaskedPan\\\":\\\"510138***5104\\\",\\\"acqStan\\\":\\\"000090\\\",\\\"Fadres\\\":\\\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\\\u015ei\\\\u015fli\\\\\\/Istanbul\\\",\\\"EXTRA_CAVVRESULTCODE\\\":\\\"I\\\",\\\"clientIp\\\":\\\"78.186.63.129\\\",\\\"iReqDetail\\\":null,\\\"okUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"md\\\":\\\"510138:FCE3F8756CB99C6484FEE44594638679257EAD4E6ECA86420A0D9D207779638D:4196:##400940535\\\",\\\"ProcReturnCode\\\":\\\"14\\\",\\\"clientId\\\":\\\"400940535\\\",\\\"payResults_dsId\\\":\\\"2\\\",\\\"vendorCode\\\":null,\\\"taksit\\\":null,\\\"TransId\\\":\\\"20059QHDC04010190\\\",\\\"EXTRA_TRXDATE\\\":\\\"20200228 16:07:03\\\",\\\"productcode1\\\":\\\"a2\\\",\\\"Fil\\\":\\\"Istanbul\\\",\\\"Ecom_Payment_Card_ExpDate_Month\\\":\\\"08\\\",\\\"storetype\\\":\\\"3d_pay\\\",\\\"iReqCode\\\":null,\\\"Response\\\":\\\"Declined\\\",\\\"tulkekod\\\":\\\"tr\\\",\\\"SettleId\\\":\\\"141\\\",\\\"mdErrorMsg\\\":\\\"Valid authentication attempt\\\",\\\"ErrMsg\\\":\\\"Invalid account number.\\\",\\\"EXTRA_SGKTUTAR\\\":\\\"0.1\\\",\\\"PAResVerified\\\":\\\"false\\\",\\\"cavv\\\":\\\"hufWN9WLoNcoYwAAINsLBTQAAAA=\\\",\\\"id1\\\":\\\"a5\\\",\\\"digest\\\":\\\"digest\\\",\\\"HostRefNum\\\":\\\"005916824995\\\",\\\"callbackCall\\\":\\\"true\\\",\\\"AuthCode\\\":null,\\\"failUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"cavvAlgorithm\\\":\\\"3\\\",\\\"Fpostakodu\\\":\\\"postakod34367\\\",\\\"price1\\\":\\\"6.25\\\",\\\"faturaFirma\\\":\\\"faturaFirma\\\",\\\"xid\\\":\\\"bVZ6Un3EW4bFQvMlfXR4Nc3TOEI=\\\",\\\"encoding\\\":\\\"ISO-8859-9\\\",\\\"oid\\\":null,\\\"mdStatus\\\":\\\"4\\\",\\\"dsId\\\":\\\"2\\\",\\\"EXTRA_TEBACIKLAMA\\\":\\\"B141296-\\\\u00d6DEME YAPILMAK ISTENEN KART BULU\\\",\\\"eci\\\":\\\"01\\\",\\\"version\\\":\\\"2.0\\\",\\\"Fadres2\\\":\\\"XXX\\\",\\\"Fismi\\\":\\\"is\\\",\\\"qty1\\\":\\\"3\\\",\\\"desc1\\\":\\\"a4 desc\\\",\\\"EXTRA_CARDISSUER\\\":\\\"T\\\\u00dcRK EKONOMI BANKASI A.S.\\\",\\\"clientid\\\":\\\"400940535\\\",\\\"txstatus\\\":\\\"A\\\",\\\"_charset_\\\":\\\"UTF-8\\\",\\\"HASH\\\":\\\"tIw+T4c8ZW8eC7FLq+QA2BkEsAc=\\\",\\\"rnd\\\":\\\"Qz0uFXNEvReqGumE8K8y\\\",\\\"HASHPARAMS\\\":\\\"clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:\\\",\\\"HASHPARAMSVAL\\\":\\\"40094053514Declined4hufWN9WLoNcoYwAAINsLBTQAAAA=01510138:FCE3F8756CB99C6484FEE44594638679257EAD4E6ECA86420A0D9D207779638D:4196:##400940535Qz0uFXNEvReqGumE8K8y\\\"}\"', 'Declinedpayment', '2020-02-28 10:07:15', '2020-05-13 13:18:47'),
(28, '\"{\\\"TRANID\\\":null,\\\"PAResSyntaxOK\\\":\\\"true\\\",\\\"Filce\\\":\\\"\\\\u015ei\\\\u015fli\\\",\\\"firmaadi\\\":\\\"EnBiosis\\\",\\\"islemtipi\\\":\\\"Auth\\\",\\\"total1\\\":\\\"7.50\\\",\\\"lang\\\":\\\"en\\\",\\\"merchantID\\\":\\\"400940535\\\",\\\"maskedCreditCard\\\":\\\"5355 76** **** 8256\\\",\\\"amount\\\":\\\"0.10\\\",\\\"sID\\\":\\\"2\\\",\\\"ACQBIN\\\":\\\"510138\\\",\\\"itemnumber1\\\":\\\"a1\\\",\\\"Ecom_Payment_Card_ExpDate_Year\\\":\\\"23\\\",\\\"storekey\\\":\\\"ENDIET19\\\",\\\"MaskedPan\\\":\\\"535576***8256\\\",\\\"Fadres\\\":\\\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\\\u015ei\\\\u015fli\\\\\\/Istanbul\\\",\\\"clientIp\\\":\\\"78.186.63.129\\\",\\\"iReqDetail\\\":null,\\\"okUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"md\\\":\\\"535576:7AEC119B52C5C6D9A7F05C7F159E6FB8A0D508B0740B135648AA20A4407D947A:4010:##400940535\\\",\\\"clientId\\\":\\\"400940535\\\",\\\"vendorCode\\\":null,\\\"taksit\\\":null,\\\"productcode1\\\":\\\"a2\\\",\\\"Fil\\\":\\\"Istanbul\\\",\\\"Ecom_Payment_Card_ExpDate_Month\\\":\\\"08\\\",\\\"storetype\\\":\\\"3d_pay\\\",\\\"iReqCode\\\":null,\\\"tulkekod\\\":\\\"tr\\\",\\\"mdErrorMsg\\\":\\\"Not authenticated\\\",\\\"PAResVerified\\\":\\\"false\\\",\\\"cavv\\\":null,\\\"id1\\\":\\\"a5\\\",\\\"digest\\\":\\\"digest\\\",\\\"callbackCall\\\":\\\"true\\\",\\\"failUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"cavvAlgorithm\\\":null,\\\"Fpostakodu\\\":\\\"postakod34367\\\",\\\"price1\\\":\\\"6.25\\\",\\\"faturaFirma\\\":\\\"faturaFirma\\\",\\\"xid\\\":\\\"ttTvxVwsaAlgq33982mK3kYrdrc=\\\",\\\"encoding\\\":\\\"ISO-8859-9\\\",\\\"oid\\\":null,\\\"mdStatus\\\":\\\"0\\\",\\\"dsId\\\":\\\"2\\\",\\\"eci\\\":null,\\\"version\\\":\\\"2.0\\\",\\\"Fadres2\\\":\\\"XXX\\\",\\\"Fismi\\\":\\\"is\\\",\\\"qty1\\\":\\\"3\\\",\\\"desc1\\\":\\\"a4 desc\\\",\\\"txstatus\\\":\\\"N\\\",\\\"_charset_\\\":\\\"UTF-8\\\",\\\"HASH\\\":\\\"O1J8c8j+GpHXZHN7Ggac79uQAxg=\\\",\\\"rnd\\\":\\\"hOnwq5zhlqOV61DHWLbt\\\",\\\"HASHPARAMS\\\":\\\"clientid:oid:mdStatus:cavv:eci:md:rnd:\\\",\\\"HASHPARAMSVAL\\\":\\\"4009405350535576:7AEC119B52C5C6D9A7F05C7F159E6FB8A0D508B0740B135648AA20A4407D947A:4010:##400940535hOnwq5zhlqOV61DHWLbt\\\"}\"', 'Declinedpayment', '2020-02-28 10:09:59', '2020-05-13 13:18:47'),
(29, '\"{\\\"TRANID\\\":null,\\\"PAResSyntaxOK\\\":\\\"true\\\",\\\"Filce\\\":\\\"\\\\u015ei\\\\u015fli\\\",\\\"firmaadi\\\":\\\"EnBiosis\\\",\\\"islemtipi\\\":\\\"Auth\\\",\\\"total1\\\":\\\"7.50\\\",\\\"lang\\\":\\\"tr\\\",\\\"merchantID\\\":\\\"400940535\\\",\\\"maskedCreditCard\\\":\\\"5355 76** **** 8256\\\",\\\"amount\\\":\\\"0.10\\\",\\\"sID\\\":\\\"2\\\",\\\"ACQBIN\\\":\\\"510138\\\",\\\"itemnumber1\\\":\\\"a1\\\",\\\"Ecom_Payment_Card_ExpDate_Year\\\":\\\"23\\\",\\\"storekey\\\":\\\"ENDIET19\\\",\\\"MaskedPan\\\":\\\"535576***8256\\\",\\\"Fadres\\\":\\\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\\\u015ei\\\\u015fli\\\\\\/Istanbul\\\",\\\"clientIp\\\":\\\"78.186.63.129\\\",\\\"iReqDetail\\\":null,\\\"okUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"md\\\":\\\"535576:546FDF603354A9305D4020DE2F7FAE1800CA1627D2A51EF52EFB589AAD8F0FF9:3681:##400940535\\\",\\\"clientId\\\":\\\"400940535\\\",\\\"vendorCode\\\":null,\\\"productcode1\\\":\\\"a2\\\",\\\"Fil\\\":\\\"Istanbul\\\",\\\"Ecom_Payment_Card_ExpDate_Month\\\":\\\"08\\\",\\\"storetype\\\":\\\"3d_pay\\\",\\\"iReqCode\\\":null,\\\"tulkekod\\\":\\\"tr\\\",\\\"mdErrorMsg\\\":\\\"Not authenticated\\\",\\\"PAResVerified\\\":\\\"false\\\",\\\"cavv\\\":null,\\\"id1\\\":\\\"a5\\\",\\\"digest\\\":\\\"digest\\\",\\\"callbackCall\\\":\\\"true\\\",\\\"failUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"cavvAlgorithm\\\":null,\\\"Fpostakodu\\\":\\\"postakod34367\\\",\\\"price1\\\":\\\"6.25\\\",\\\"faturaFirma\\\":\\\"faturaFirma\\\",\\\"xid\\\":\\\"m3XtprvBVZklgfy5rQYXlRIGC0c=\\\",\\\"encoding\\\":\\\"ISO-8859-9\\\",\\\"oid\\\":null,\\\"mdStatus\\\":\\\"0\\\",\\\"dsId\\\":\\\"2\\\",\\\"eci\\\":null,\\\"version\\\":\\\"2.0\\\",\\\"Fadres2\\\":\\\"XXX\\\",\\\"Fismi\\\":\\\"is\\\",\\\"qty1\\\":\\\"3\\\",\\\"desc1\\\":\\\"a4 desc\\\",\\\"txstatus\\\":\\\"N\\\",\\\"_charset_\\\":\\\"UTF-8\\\",\\\"HASH\\\":\\\"qfw0iWvAOevXDgFw5yo0emF0ux0=\\\",\\\"rnd\\\":\\\"HoYYqPCbwcxT3EwZxu2K\\\",\\\"HASHPARAMS\\\":\\\"clientid:oid:mdStatus:cavv:eci:md:rnd:\\\",\\\"HASHPARAMSVAL\\\":\\\"4009405350535576:546FDF603354A9305D4020DE2F7FAE1800CA1627D2A51EF52EFB589AAD8F0FF9:3681:##400940535HoYYqPCbwcxT3EwZxu2K\\\"}\"', 'Declinedpayment', '2020-02-28 10:19:09', '2020-05-13 13:18:47'),
(30, '\"{\\\"ReturnOid\\\":\\\"ORDER-20059QWEE04028027\\\",\\\"TRANID\\\":null,\\\"PAResSyntaxOK\\\":\\\"true\\\",\\\"Filce\\\":\\\"\\\\u015ei\\\\u015fli\\\",\\\"firmaadi\\\":\\\"EnBiosis\\\",\\\"islemtipi\\\":\\\"Auth\\\",\\\"total1\\\":\\\"7.50\\\",\\\"lang\\\":\\\"tr\\\",\\\"merchantID\\\":\\\"400940535\\\",\\\"maskedCreditCard\\\":\\\"5355 76** **** 8256\\\",\\\"amount\\\":\\\"0.10\\\",\\\"sID\\\":\\\"2\\\",\\\"ACQBIN\\\":\\\"510138\\\",\\\"itemnumber1\\\":\\\"a1\\\",\\\"Ecom_Payment_Card_ExpDate_Year\\\":\\\"23\\\",\\\"EXTRA_CARDBRAND\\\":\\\"MASTERCARD\\\",\\\"storekey\\\":\\\"ENDIET19\\\",\\\"MaskedPan\\\":\\\"535576***8256\\\",\\\"acqStan\\\":\\\"000092\\\",\\\"Fadres\\\":\\\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\\\u015ei\\\\u015fli\\\\\\/Istanbul\\\",\\\"clientIp\\\":\\\"78.186.63.129\\\",\\\"iReqDetail\\\":null,\\\"okUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"md\\\":\\\"535576:C40E447E0DFB23C6A161A96FEE9FFEEAB5ACC0FDB6A3FAD2343093C4BF233060:4740:##400940535\\\",\\\"ProcReturnCode\\\":\\\"13\\\",\\\"clientId\\\":\\\"400940535\\\",\\\"payResults_dsId\\\":\\\"2\\\",\\\"vendorCode\\\":null,\\\"taksit\\\":\\\"2\\\",\\\"TransId\\\":\\\"20059QWEF04028030\\\",\\\"EXTRA_TRXDATE\\\":\\\"20200228 16:22:04\\\",\\\"productcode1\\\":\\\"a2\\\",\\\"Fil\\\":\\\"Istanbul\\\",\\\"Ecom_Payment_Card_ExpDate_Month\\\":\\\"08\\\",\\\"storetype\\\":\\\"3d_pay\\\",\\\"iReqCode\\\":null,\\\"Response\\\":\\\"Declined\\\",\\\"tulkekod\\\":\\\"tr\\\",\\\"SettleId\\\":\\\"141\\\",\\\"mdErrorMsg\\\":\\\"Authenticated\\\",\\\"ErrMsg\\\":\\\"Gecersiz tutar.\\\",\\\"EXTRA_SGKTUTAR\\\":\\\"0.1\\\",\\\"PAResVerified\\\":\\\"false\\\",\\\"cavv\\\":\\\"jOfWN9WLoNcoCREFKtdTBCMAAAA=\\\",\\\"id1\\\":\\\"a5\\\",\\\"digest\\\":\\\"digest\\\",\\\"HostRefNum\\\":\\\"005916825819\\\",\\\"callbackCall\\\":\\\"true\\\",\\\"AuthCode\\\":null,\\\"failUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"cavvAlgorithm\\\":\\\"3\\\",\\\"Fpostakodu\\\":\\\"postakod34367\\\",\\\"price1\\\":\\\"6.25\\\",\\\"faturaFirma\\\":\\\"faturaFirma\\\",\\\"xid\\\":\\\"9ZtC8MSsiHFBpT4oqBfLh0WmJmE=\\\",\\\"encoding\\\":\\\"ISO-8859-9\\\",\\\"oid\\\":null,\\\"mdStatus\\\":\\\"1\\\",\\\"dsId\\\":\\\"2\\\",\\\"EXTRA_TEBACIKLAMA\\\":\\\"B132493-TR:=Hatal\\\\u0131 taksit tutar\\\\u0131;;EN:=\\\",\\\"eci\\\":\\\"02\\\",\\\"version\\\":\\\"2.0\\\",\\\"Fadres2\\\":\\\"XXX\\\",\\\"Fismi\\\":\\\"is\\\",\\\"qty1\\\":\\\"3\\\",\\\"desc1\\\":\\\"a4 desc\\\",\\\"EXTRA_CARDISSUER\\\":\\\"T. VAKIFLAR BANKASI T.A.O.\\\",\\\"clientid\\\":\\\"400940535\\\",\\\"txstatus\\\":\\\"Y\\\",\\\"_charset_\\\":\\\"UTF-8\\\",\\\"HASH\\\":\\\"9RXFboLQYepP7NAsITa3bMIVMqg=\\\",\\\"rnd\\\":\\\"OXNR0WUdeJBDE\\\\\\/wve2vU\\\",\\\"HASHPARAMS\\\":\\\"clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:\\\",\\\"HASHPARAMSVAL\\\":\\\"40094053513Declined1jOfWN9WLoNcoCREFKtdTBCMAAAA=02535576:C40E447E0DFB23C6A161A96FEE9FFEEAB5ACC0FDB6A3FAD2343093C4BF233060:4740:##400940535OXNR0WUdeJBDE\\\\\\/wve2vU\\\"}\"', 'Declinedpayment', '2020-02-28 10:22:06', '2020-05-13 13:18:47'),
(31, '\"{\\\"TRANID\\\":null,\\\"PAResSyntaxOK\\\":\\\"true\\\",\\\"Filce\\\":\\\"\\\\u015ei\\\\u015fli\\\",\\\"firmaadi\\\":\\\"EnBiosis\\\",\\\"islemtipi\\\":\\\"Auth\\\",\\\"total1\\\":\\\"7.50\\\",\\\"lang\\\":\\\"en\\\",\\\"merchantID\\\":\\\"400940535\\\",\\\"maskedCreditCard\\\":\\\"5355 76** **** 8256\\\",\\\"amount\\\":\\\"0.10\\\",\\\"sID\\\":\\\"2\\\",\\\"ACQBIN\\\":\\\"510138\\\",\\\"itemnumber1\\\":\\\"a1\\\",\\\"Ecom_Payment_Card_ExpDate_Year\\\":\\\"23\\\",\\\"storekey\\\":\\\"ENDIET19\\\",\\\"MaskedPan\\\":\\\"535576***8256\\\",\\\"Fadres\\\":\\\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\\\u015ei\\\\u015fli\\\\\\/Istanbul\\\",\\\"clientIp\\\":\\\"78.186.63.129\\\",\\\"iReqDetail\\\":null,\\\"okUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"md\\\":\\\"535576:CE4C1943A5F21C7EC5B5B90122A8D4B9DE62F66CE410F6D403AB3DF3C22776DC:4523:##400940535\\\",\\\"clientId\\\":\\\"400940535\\\",\\\"vendorCode\\\":null,\\\"productcode1\\\":\\\"a2\\\",\\\"Fil\\\":\\\"Istanbul\\\",\\\"Ecom_Payment_Card_ExpDate_Month\\\":\\\"08\\\",\\\"storetype\\\":\\\"3d_pay\\\",\\\"iReqCode\\\":null,\\\"tulkekod\\\":\\\"tr\\\",\\\"mdErrorMsg\\\":\\\"Not authenticated\\\",\\\"PAResVerified\\\":\\\"false\\\",\\\"cavv\\\":null,\\\"id1\\\":\\\"a5\\\",\\\"digest\\\":\\\"digest\\\",\\\"callbackCall\\\":\\\"true\\\",\\\"failUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"cavvAlgorithm\\\":null,\\\"Fpostakodu\\\":\\\"postakod34367\\\",\\\"price1\\\":\\\"6.25\\\",\\\"faturaFirma\\\":\\\"faturaFirma\\\",\\\"xid\\\":\\\"KLzQhRg7A4iI4UaJjmVYdhWT\\\\\\/Co=\\\",\\\"encoding\\\":\\\"ISO-8859-9\\\",\\\"oid\\\":null,\\\"mdStatus\\\":\\\"0\\\",\\\"dsId\\\":\\\"2\\\",\\\"eci\\\":null,\\\"version\\\":\\\"2.0\\\",\\\"Fadres2\\\":\\\"XXX\\\",\\\"Fismi\\\":\\\"is\\\",\\\"qty1\\\":\\\"3\\\",\\\"desc1\\\":\\\"a4 desc\\\",\\\"txstatus\\\":\\\"N\\\",\\\"_charset_\\\":\\\"UTF-8\\\",\\\"HASH\\\":\\\"JN6pLyKuBkVkj\\\\\\/ykZNbaE+XSeO4=\\\",\\\"rnd\\\":\\\"i1FXaSWJZ\\\\\\/HgZ87KKq7D\\\",\\\"HASHPARAMS\\\":\\\"clientid:oid:mdStatus:cavv:eci:md:rnd:\\\",\\\"HASHPARAMSVAL\\\":\\\"4009405350535576:CE4C1943A5F21C7EC5B5B90122A8D4B9DE62F66CE410F6D403AB3DF3C22776DC:4523:##400940535i1FXaSWJZ\\\\\\/HgZ87KKq7D\\\"}\"', 'Declinedpayment', '2020-02-28 11:01:47', '2020-05-13 13:18:47'),
(32, '\"{\\\"ReturnOid\\\":\\\"ORDER-20059Tm2G04029505\\\",\\\"TRANID\\\":null,\\\"PAResSyntaxOK\\\":\\\"true\\\",\\\"Filce\\\":\\\"\\\\u015ei\\\\u015fli\\\",\\\"firmaadi\\\":\\\"EnBiosis\\\",\\\"islemtipi\\\":\\\"Auth\\\",\\\"total1\\\":\\\"7.50\\\",\\\"lang\\\":\\\"en\\\",\\\"merchantID\\\":\\\"400940535\\\",\\\"maskedCreditCard\\\":\\\"5355 76** **** 8256\\\",\\\"amount\\\":\\\"0.10\\\",\\\"sID\\\":\\\"2\\\",\\\"ACQBIN\\\":\\\"510138\\\",\\\"itemnumber1\\\":\\\"a1\\\",\\\"Ecom_Payment_Card_ExpDate_Year\\\":\\\"23\\\",\\\"EXTRA_CARDBRAND\\\":\\\"MASTERCARD\\\",\\\"storekey\\\":\\\"ENDIET19\\\",\\\"MaskedPan\\\":\\\"535576***8256\\\",\\\"acqStan\\\":\\\"000093\\\",\\\"Fadres\\\":\\\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\\\u015ei\\\\u015fli\\\\\\/Istanbul\\\",\\\"clientIp\\\":\\\"78.186.63.129\\\",\\\"iReqDetail\\\":null,\\\"okUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"md\\\":\\\"535576:EB41A8D996CBD079530A3A4A38C46DA65F8AFCD2CA85190E9EF6A0A4831D5791:4308:##400940535\\\",\\\"ProcReturnCode\\\":\\\"13\\\",\\\"clientId\\\":\\\"400940535\\\",\\\"payResults_dsId\\\":\\\"2\\\",\\\"vendorCode\\\":null,\\\"taksit\\\":\\\"2\\\",\\\"TransId\\\":\\\"20059Tm2H04029507\\\",\\\"EXTRA_TRXDATE\\\":\\\"20200228 19:38:52\\\",\\\"productcode1\\\":\\\"a2\\\",\\\"Fil\\\":\\\"Istanbul\\\",\\\"Ecom_Payment_Card_ExpDate_Month\\\":\\\"08\\\",\\\"storetype\\\":\\\"3d_pay\\\",\\\"iReqCode\\\":null,\\\"Response\\\":\\\"Declined\\\",\\\"tulkekod\\\":\\\"tr\\\",\\\"SettleId\\\":\\\"141\\\",\\\"mdErrorMsg\\\":\\\"Authenticated\\\",\\\"ErrMsg\\\":\\\"Invalid amount.\\\",\\\"EXTRA_SGKTUTAR\\\":\\\"0.1\\\",\\\"PAResVerified\\\":\\\"false\\\",\\\"cavv\\\":\\\"jOfWN9WLoNcoCREFRGezAggAAAA=\\\",\\\"id1\\\":\\\"a5\\\",\\\"digest\\\":\\\"digest\\\",\\\"HostRefNum\\\":\\\"005919841880\\\",\\\"callbackCall\\\":\\\"true\\\",\\\"AuthCode\\\":null,\\\"failUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"cavvAlgorithm\\\":\\\"3\\\",\\\"Fpostakodu\\\":\\\"postakod34367\\\",\\\"price1\\\":\\\"6.25\\\",\\\"faturaFirma\\\":\\\"faturaFirma\\\",\\\"xid\\\":\\\"4Vdihyuv9WgfaALU3zPics4jQNE=\\\",\\\"encoding\\\":\\\"ISO-8859-9\\\",\\\"oid\\\":null,\\\"mdStatus\\\":\\\"1\\\",\\\"dsId\\\":\\\"2\\\",\\\"EXTRA_TEBACIKLAMA\\\":\\\"B132493-TR:=Hatal\\\\u0131 taksit tutar\\\\u0131;;EN:=\\\",\\\"eci\\\":\\\"02\\\",\\\"version\\\":\\\"2.0\\\",\\\"Fadres2\\\":\\\"XXX\\\",\\\"Fismi\\\":\\\"is\\\",\\\"qty1\\\":\\\"3\\\",\\\"desc1\\\":\\\"a4 desc\\\",\\\"EXTRA_CARDISSUER\\\":\\\"T. VAKIFLAR BANKASI T.A.O.\\\",\\\"clientid\\\":\\\"400940535\\\",\\\"txstatus\\\":\\\"Y\\\",\\\"_charset_\\\":\\\"UTF-8\\\",\\\"HASH\\\":\\\"TyzQ0DS\\\\\\/yvOKBmjkhFXDHsfMtwM=\\\",\\\"rnd\\\":\\\"NtsX41hL4yXF\\\\\\/4IQvHQH\\\",\\\"HASHPARAMS\\\":\\\"clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:\\\",\\\"HASHPARAMSVAL\\\":\\\"40094053513Declined1jOfWN9WLoNcoCREFRGezAggAAAA=02535576:EB41A8D996CBD079530A3A4A38C46DA65F8AFCD2CA85190E9EF6A0A4831D5791:4308:##400940535NtsX41hL4yXF\\\\\\/4IQvHQH\\\"}\"', 'Declinedpayment', '2020-02-28 13:38:53', '2020-05-13 13:18:47'),
(33, '\"{\\\"TRANID\\\":null,\\\"PAResSyntaxOK\\\":\\\"true\\\",\\\"Filce\\\":\\\"\\\\u015ei\\\\u015fli\\\",\\\"firmaadi\\\":\\\"EnBiosis\\\",\\\"islemtipi\\\":\\\"Auth\\\",\\\"total1\\\":\\\"7.50\\\",\\\"lang\\\":\\\"tr\\\",\\\"merchantID\\\":\\\"400940535\\\",\\\"maskedCreditCard\\\":\\\"5331 69** **** 7020\\\",\\\"amount\\\":\\\"1337.97\\\",\\\"sID\\\":\\\"2\\\",\\\"ACQBIN\\\":\\\"510138\\\",\\\"itemnumber1\\\":\\\"a1\\\",\\\"Ecom_Payment_Card_ExpDate_Year\\\":\\\"23\\\",\\\"storekey\\\":\\\"ENDIET19\\\",\\\"MaskedPan\\\":\\\"533169***7020\\\",\\\"Fadres\\\":\\\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\\\u015ei\\\\u015fli\\\\\\/Istanbul\\\",\\\"clientIp\\\":\\\"78.186.63.129\\\",\\\"iReqDetail\\\":null,\\\"okUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"md\\\":\\\"533169:FB4E44583876A93031FEDE43DF36EB928F1D84469945E3B5C6624082418DCB63:4128:##400940535\\\",\\\"clientId\\\":\\\"400940535\\\",\\\"vendorCode\\\":null,\\\"taksit\\\":\\\"3\\\",\\\"productcode1\\\":\\\"a2\\\",\\\"Fil\\\":\\\"Istanbul\\\",\\\"Ecom_Payment_Card_ExpDate_Month\\\":\\\"05\\\",\\\"storetype\\\":\\\"3d_pay\\\",\\\"iReqCode\\\":null,\\\"tulkekod\\\":\\\"tr\\\",\\\"mdErrorMsg\\\":\\\"Not authenticated\\\",\\\"PAResVerified\\\":\\\"false\\\",\\\"cavv\\\":null,\\\"id1\\\":\\\"a5\\\",\\\"digest\\\":\\\"digest\\\",\\\"callbackCall\\\":\\\"true\\\",\\\"failUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"cavvAlgorithm\\\":null,\\\"Fpostakodu\\\":\\\"postakod34367\\\",\\\"price1\\\":\\\"6.25\\\",\\\"faturaFirma\\\":\\\"faturaFirma\\\",\\\"xid\\\":\\\"5nfrwMZfYdYb+kbSOW5ff4C\\\\\\/zB4=\\\",\\\"encoding\\\":\\\"ISO-8859-9\\\",\\\"oid\\\":null,\\\"mdStatus\\\":\\\"0\\\",\\\"dsId\\\":\\\"2\\\",\\\"eci\\\":null,\\\"version\\\":\\\"2.0\\\",\\\"Fadres2\\\":\\\"XXX\\\",\\\"Fismi\\\":\\\"is\\\",\\\"qty1\\\":\\\"3\\\",\\\"desc1\\\":\\\"a4 desc\\\",\\\"txstatus\\\":\\\"N\\\",\\\"_charset_\\\":\\\"UTF-8\\\",\\\"HASH\\\":\\\"wurKWmrBxySrh6ykn5iPWSdqbnk=\\\",\\\"rnd\\\":\\\"ToHE\\\\\\/Uw6GNJTM7\\\\\\/\\\\\\/C2VL\\\",\\\"HASHPARAMS\\\":\\\"clientid:oid:mdStatus:cavv:eci:md:rnd:\\\",\\\"HASHPARAMSVAL\\\":\\\"4009405350533169:FB4E44583876A93031FEDE43DF36EB928F1D84469945E3B5C6624082418DCB63:4128:##400940535ToHE\\\\\\/Uw6GNJTM7\\\\\\/\\\\\\/C2VL\\\"}\"', 'Declinedpayment', '2020-03-02 04:22:25', '2020-05-13 13:18:47'),
(34, '\"{\\\"TRANID\\\":null,\\\"PAResSyntaxOK\\\":\\\"true\\\",\\\"Filce\\\":\\\"\\\\u015ei\\\\u015fli\\\",\\\"firmaadi\\\":\\\"EnBiosis\\\",\\\"islemtipi\\\":\\\"Auth\\\",\\\"total1\\\":\\\"7.50\\\",\\\"lang\\\":\\\"tr\\\",\\\"merchantID\\\":\\\"400940535\\\",\\\"maskedCreditCard\\\":\\\"5355 76** **** 8256\\\",\\\"amount\\\":\\\"1324.98\\\",\\\"sID\\\":\\\"2\\\",\\\"ACQBIN\\\":\\\"510138\\\",\\\"itemnumber1\\\":\\\"a1\\\",\\\"Ecom_Payment_Card_ExpDate_Year\\\":\\\"23\\\",\\\"storekey\\\":\\\"ENDIET19\\\",\\\"MaskedPan\\\":\\\"535576***8256\\\",\\\"Fadres\\\":\\\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\\\u015ei\\\\u015fli\\\\\\/Istanbul\\\",\\\"clientIp\\\":\\\"78.186.63.129\\\",\\\"iReqDetail\\\":null,\\\"okUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"md\\\":\\\"535576:0582993BD8137F1207BB2BB90A66C984B5F546BF88E8FE6B3ED4F8B59156A41B:4135:##400940535\\\",\\\"clientId\\\":\\\"400940535\\\",\\\"vendorCode\\\":null,\\\"taksit\\\":\\\"2\\\",\\\"productcode1\\\":\\\"a2\\\",\\\"Fil\\\":\\\"Istanbul\\\",\\\"Ecom_Payment_Card_ExpDate_Month\\\":\\\"08\\\",\\\"storetype\\\":\\\"3d_pay\\\",\\\"iReqCode\\\":null,\\\"tulkekod\\\":\\\"tr\\\",\\\"mdErrorMsg\\\":\\\"Not authenticated\\\",\\\"PAResVerified\\\":\\\"false\\\",\\\"cavv\\\":null,\\\"id1\\\":\\\"a5\\\",\\\"digest\\\":\\\"digest\\\",\\\"callbackCall\\\":\\\"true\\\",\\\"failUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"cavvAlgorithm\\\":null,\\\"Fpostakodu\\\":\\\"postakod34367\\\",\\\"price1\\\":\\\"6.25\\\",\\\"faturaFirma\\\":\\\"faturaFirma\\\",\\\"xid\\\":\\\"24RGfUOuwvhwH\\\\\\/c+KgHHY4fr1mk=\\\",\\\"encoding\\\":\\\"ISO-8859-9\\\",\\\"oid\\\":null,\\\"mdStatus\\\":\\\"0\\\",\\\"dsId\\\":\\\"2\\\",\\\"eci\\\":null,\\\"version\\\":\\\"2.0\\\",\\\"Fadres2\\\":\\\"XXX\\\",\\\"Fismi\\\":\\\"is\\\",\\\"qty1\\\":\\\"3\\\",\\\"desc1\\\":\\\"a4 desc\\\",\\\"txstatus\\\":\\\"N\\\",\\\"_charset_\\\":\\\"UTF-8\\\",\\\"HASH\\\":\\\"NLS60xBe\\\\\\/KHgfSNGsVQMIZd9uOc=\\\",\\\"rnd\\\":\\\"x\\\\\\/Qwl5enlyS6NRTiNa\\\\\\/U\\\",\\\"HASHPARAMS\\\":\\\"clientid:oid:mdStatus:cavv:eci:md:rnd:\\\",\\\"HASHPARAMSVAL\\\":\\\"4009405350535576:0582993BD8137F1207BB2BB90A66C984B5F546BF88E8FE6B3ED4F8B59156A41B:4135:##400940535x\\\\\\/Qwl5enlyS6NRTiNa\\\\\\/U\\\"}\"', 'Declinedpayment', '2020-03-02 04:29:12', '2020-05-13 13:18:47'),
(35, '\"{\\\"ReturnOid\\\":\\\"ORDER-20062KgCD04017217\\\",\\\"TRANID\\\":null,\\\"PAResSyntaxOK\\\":\\\"true\\\",\\\"Filce\\\":\\\"\\\\u015ei\\\\u015fli\\\",\\\"firmaadi\\\":\\\"EnBiosis\\\",\\\"islemtipi\\\":\\\"Auth\\\",\\\"total1\\\":\\\"7.50\\\",\\\"lang\\\":\\\"tr\\\",\\\"merchantID\\\":\\\"400940535\\\",\\\"maskedCreditCard\\\":\\\"5355 76** **** 8256\\\",\\\"amount\\\":\\\"1349\\\",\\\"sID\\\":\\\"2\\\",\\\"ACQBIN\\\":\\\"510138\\\",\\\"itemnumber1\\\":\\\"a1\\\",\\\"Ecom_Payment_Card_ExpDate_Year\\\":\\\"23\\\",\\\"EXTRA_CARDBRAND\\\":\\\"MASTERCARD\\\",\\\"storekey\\\":\\\"ENDIET19\\\",\\\"MaskedPan\\\":\\\"535576***8256\\\",\\\"acqStan\\\":\\\"000095\\\",\\\"Fadres\\\":\\\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\\\u015ei\\\\u015fli\\\\\\/Istanbul\\\",\\\"clientIp\\\":\\\"78.186.63.129\\\",\\\"iReqDetail\\\":null,\\\"okUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"md\\\":\\\"535576:D6A70606FCAF2385F72DED2AEBE8992920714FF5CBFB16F75BCA33AD6902F597:4432:##400940535\\\",\\\"ProcReturnCode\\\":\\\"12\\\",\\\"clientId\\\":\\\"400940535\\\",\\\"payResults_dsId\\\":\\\"2\\\",\\\"vendorCode\\\":null,\\\"taksit\\\":\\\"2\\\",\\\"TransId\\\":\\\"20062KgCD04017219\\\",\\\"EXTRA_TRXDATE\\\":\\\"20200302 10:32:02\\\",\\\"productcode1\\\":\\\"a2\\\",\\\"Fil\\\":\\\"Istanbul\\\",\\\"Ecom_Payment_Card_ExpDate_Month\\\":\\\"05\\\",\\\"storetype\\\":\\\"3d_pay\\\",\\\"iReqCode\\\":null,\\\"Response\\\":\\\"Declined\\\",\\\"tulkekod\\\":\\\"tr\\\",\\\"SettleId\\\":\\\"144\\\",\\\"mdErrorMsg\\\":\\\"Authenticated\\\",\\\"ErrMsg\\\":\\\"Gecersiz Transaction.\\\",\\\"EXTRA_SGKTUTAR\\\":\\\"1349.0\\\",\\\"PAResVerified\\\":\\\"false\\\",\\\"cavv\\\":\\\"jOfWN9WLoNcoCREAXcEgBBAAAAA=\\\",\\\"id1\\\":\\\"a5\\\",\\\"digest\\\":\\\"digest\\\",\\\"HostRefNum\\\":\\\"006210968788\\\",\\\"callbackCall\\\":\\\"true\\\",\\\"AuthCode\\\":null,\\\"failUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"cavvAlgorithm\\\":\\\"3\\\",\\\"Fpostakodu\\\":\\\"postakod34367\\\",\\\"price1\\\":\\\"6.25\\\",\\\"faturaFirma\\\":\\\"faturaFirma\\\",\\\"xid\\\":\\\"YmLfngR9l2CNKrZ6rXZIXzAwjLc=\\\",\\\"encoding\\\":\\\"ISO-8859-9\\\",\\\"oid\\\":null,\\\"mdStatus\\\":\\\"1\\\",\\\"dsId\\\":\\\"2\\\",\\\"EXTRA_TEBACIKLAMA\\\":\\\"B122354-Bonus olmayan i\\\\u015flemde taksit kul\\\",\\\"eci\\\":\\\"02\\\",\\\"version\\\":\\\"2.0\\\",\\\"Fadres2\\\":\\\"XXX\\\",\\\"Fismi\\\":\\\"is\\\",\\\"qty1\\\":\\\"3\\\",\\\"desc1\\\":\\\"a4 desc\\\",\\\"EXTRA_CARDISSUER\\\":\\\"T. VAKIFLAR BANKASI T.A.O.\\\",\\\"clientid\\\":\\\"400940535\\\",\\\"txstatus\\\":\\\"Y\\\",\\\"_charset_\\\":\\\"UTF-8\\\",\\\"HASH\\\":\\\"dzHNcsKGgRaFCdyRevofbTFabq4=\\\",\\\"rnd\\\":\\\"LEAF7dl0BDmegfTDy0qX\\\",\\\"HASHPARAMS\\\":\\\"clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:\\\",\\\"HASHPARAMSVAL\\\":\\\"40094053512Declined1jOfWN9WLoNcoCREAXcEgBBAAAAA=02535576:D6A70606FCAF2385F72DED2AEBE8992920714FF5CBFB16F75BCA33AD6902F597:4432:##400940535LEAF7dl0BDmegfTDy0qX\\\"}\"', 'Declinedpayment', '2020-03-02 04:32:06', '2020-05-13 13:18:47'),
(36, '\"{\\\"ReturnOid\\\":\\\"ORDER-20062KicI04028849\\\",\\\"TRANID\\\":null,\\\"PAResSyntaxOK\\\":\\\"true\\\",\\\"Filce\\\":\\\"\\\\u015ei\\\\u015fli\\\",\\\"firmaadi\\\":\\\"EnBiosis\\\",\\\"islemtipi\\\":\\\"Auth\\\",\\\"total1\\\":\\\"7.50\\\",\\\"lang\\\":\\\"tr\\\",\\\"merchantID\\\":\\\"400940535\\\",\\\"maskedCreditCard\\\":\\\"5355 76** **** 8256\\\",\\\"amount\\\":\\\"3\\\",\\\"sID\\\":\\\"2\\\",\\\"ACQBIN\\\":\\\"510138\\\",\\\"itemnumber1\\\":\\\"a1\\\",\\\"Ecom_Payment_Card_ExpDate_Year\\\":\\\"23\\\",\\\"EXTRA_CARDBRAND\\\":\\\"MASTERCARD\\\",\\\"storekey\\\":\\\"ENDIET19\\\",\\\"MaskedPan\\\":\\\"535576***8256\\\",\\\"acqStan\\\":\\\"000096\\\",\\\"Fadres\\\":\\\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\\\u015ei\\\\u015fli\\\\\\/Istanbul\\\",\\\"clientIp\\\":\\\"78.186.63.129\\\",\\\"iReqDetail\\\":null,\\\"okUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"md\\\":\\\"535576:56790C88D7228D843413383EBF5A739BF70EE054A7F1248E8ED77A9374ED3839:3864:##400940535\\\",\\\"ProcReturnCode\\\":\\\"12\\\",\\\"clientId\\\":\\\"400940535\\\",\\\"payResults_dsId\\\":\\\"2\\\",\\\"vendorCode\\\":null,\\\"taksit\\\":\\\"2\\\",\\\"TransId\\\":\\\"20062KicI04028853\\\",\\\"EXTRA_TRXDATE\\\":\\\"20200302 10:34:28\\\",\\\"productcode1\\\":\\\"a2\\\",\\\"Fil\\\":\\\"Istanbul\\\",\\\"Ecom_Payment_Card_ExpDate_Month\\\":\\\"08\\\",\\\"storetype\\\":\\\"3d_pay\\\",\\\"iReqCode\\\":null,\\\"Response\\\":\\\"Declined\\\",\\\"tulkekod\\\":\\\"tr\\\",\\\"SettleId\\\":\\\"144\\\",\\\"mdErrorMsg\\\":\\\"Authenticated\\\",\\\"ErrMsg\\\":\\\"Gecersiz Transaction.\\\",\\\"EXTRA_SGKTUTAR\\\":\\\"3.0\\\",\\\"PAResVerified\\\":\\\"false\\\",\\\"cavv\\\":\\\"jOfWN9WLoNcoCREAXcqACFgAAAA=\\\",\\\"id1\\\":\\\"a5\\\",\\\"digest\\\":\\\"digest\\\",\\\"HostRefNum\\\":\\\"006210969012\\\",\\\"callbackCall\\\":\\\"true\\\",\\\"AuthCode\\\":null,\\\"failUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"cavvAlgorithm\\\":\\\"3\\\",\\\"Fpostakodu\\\":\\\"postakod34367\\\",\\\"price1\\\":\\\"6.25\\\",\\\"faturaFirma\\\":\\\"faturaFirma\\\",\\\"xid\\\":\\\"CrqsW2YRXsqTydZNaF1kAsj6oSs=\\\",\\\"encoding\\\":\\\"ISO-8859-9\\\",\\\"oid\\\":null,\\\"mdStatus\\\":\\\"1\\\",\\\"dsId\\\":\\\"2\\\",\\\"EXTRA_TEBACIKLAMA\\\":\\\"B122354-Bonus olmayan i\\\\u015flemde taksit kul\\\",\\\"eci\\\":\\\"02\\\",\\\"version\\\":\\\"2.0\\\",\\\"Fadres2\\\":\\\"XXX\\\",\\\"Fismi\\\":\\\"is\\\",\\\"qty1\\\":\\\"3\\\",\\\"desc1\\\":\\\"a4 desc\\\",\\\"EXTRA_CARDISSUER\\\":\\\"T. VAKIFLAR BANKASI T.A.O.\\\",\\\"clientid\\\":\\\"400940535\\\",\\\"txstatus\\\":\\\"Y\\\",\\\"_charset_\\\":\\\"UTF-8\\\",\\\"HASH\\\":\\\"HXqK3LrlRpKsXa6hbYnaZ4UrB2A=\\\",\\\"rnd\\\":\\\"M92qFoWjcRGT4y\\\\\\/t9G6a\\\",\\\"HASHPARAMS\\\":\\\"clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:\\\",\\\"HASHPARAMSVAL\\\":\\\"40094053512Declined1jOfWN9WLoNcoCREAXcqACFgAAAA=02535576:56790C88D7228D843413383EBF5A739BF70EE054A7F1248E8ED77A9374ED3839:3864:##400940535M92qFoWjcRGT4y\\\\\\/t9G6a\\\"}\"', 'Declinedpayment', '2020-03-02 04:34:32', '2020-05-13 13:18:47'),
(37, '\"{\\\"TRANID\\\":null,\\\"PAResSyntaxOK\\\":\\\"true\\\",\\\"Filce\\\":\\\"\\\\u015ei\\\\u015fli\\\",\\\"firmaadi\\\":\\\"EnBiosis\\\",\\\"islemtipi\\\":\\\"Auth\\\",\\\"total1\\\":\\\"7.50\\\",\\\"lang\\\":\\\"tr\\\",\\\"merchantID\\\":\\\"400940535\\\",\\\"maskedCreditCard\\\":\\\"5331 69** **** 7020\\\",\\\"amount\\\":\\\"3\\\",\\\"sID\\\":\\\"2\\\",\\\"ACQBIN\\\":\\\"510138\\\",\\\"itemnumber1\\\":\\\"a1\\\",\\\"Ecom_Payment_Card_ExpDate_Year\\\":\\\"23\\\",\\\"storekey\\\":\\\"ENDIET19\\\",\\\"MaskedPan\\\":\\\"533169***7020\\\",\\\"Fadres\\\":\\\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\\\u015ei\\\\u015fli\\\\\\/Istanbul\\\",\\\"clientIp\\\":\\\"78.186.63.129\\\",\\\"iReqDetail\\\":null,\\\"okUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"md\\\":\\\"533169:49B33F6AC23108EF435469A388782CFAA1FC55231B37C281CA8948AC81FB2899:3974:##400940535\\\",\\\"clientId\\\":\\\"400940535\\\",\\\"vendorCode\\\":null,\\\"taksit\\\":\\\"2\\\",\\\"productcode1\\\":\\\"a2\\\",\\\"Fil\\\":\\\"Istanbul\\\",\\\"Ecom_Payment_Card_ExpDate_Month\\\":\\\"05\\\",\\\"storetype\\\":\\\"3d_pay\\\",\\\"iReqCode\\\":null,\\\"tulkekod\\\":\\\"tr\\\",\\\"mdErrorMsg\\\":\\\"Not authenticated\\\",\\\"PAResVerified\\\":\\\"false\\\",\\\"cavv\\\":null,\\\"id1\\\":\\\"a5\\\",\\\"digest\\\":\\\"digest\\\",\\\"callbackCall\\\":\\\"true\\\",\\\"failUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"cavvAlgorithm\\\":null,\\\"Fpostakodu\\\":\\\"postakod34367\\\",\\\"price1\\\":\\\"6.25\\\",\\\"faturaFirma\\\":\\\"faturaFirma\\\",\\\"xid\\\":\\\"y+Rux\\\\\\/K7GxV9j2EmCviXL9BM\\\\\\/u8=\\\",\\\"encoding\\\":\\\"ISO-8859-9\\\",\\\"oid\\\":null,\\\"mdStatus\\\":\\\"0\\\",\\\"dsId\\\":\\\"2\\\",\\\"eci\\\":null,\\\"version\\\":\\\"2.0\\\",\\\"Fadres2\\\":\\\"XXX\\\",\\\"Fismi\\\":\\\"is\\\",\\\"qty1\\\":\\\"3\\\",\\\"desc1\\\":\\\"a4 desc\\\",\\\"txstatus\\\":\\\"N\\\",\\\"_charset_\\\":\\\"UTF-8\\\",\\\"HASH\\\":\\\"ZdGmyggzX2ZoYtbL\\\\\\/JCUw5n4Wnw=\\\",\\\"rnd\\\":\\\"WzrQchCAmlsMKcakWehN\\\",\\\"HASHPARAMS\\\":\\\"clientid:oid:mdStatus:cavv:eci:md:rnd:\\\",\\\"HASHPARAMSVAL\\\":\\\"4009405350533169:49B33F6AC23108EF435469A388782CFAA1FC55231B37C281CA8948AC81FB2899:3974:##400940535WzrQchCAmlsMKcakWehN\\\"}\"', 'Declinedpayment', '2020-03-02 04:36:40', '2020-05-13 13:18:47'),
(38, '\"{\\\"TRANID\\\":null,\\\"PAResSyntaxOK\\\":\\\"true\\\",\\\"Filce\\\":\\\"\\\\u015ei\\\\u015fli\\\",\\\"firmaadi\\\":\\\"EnBiosis\\\",\\\"islemtipi\\\":\\\"Auth\\\",\\\"total1\\\":\\\"7.50\\\",\\\"lang\\\":\\\"tr\\\",\\\"merchantID\\\":\\\"400940535\\\",\\\"maskedCreditCard\\\":\\\"5406 69** **** 6011\\\",\\\"amount\\\":\\\"3\\\",\\\"sID\\\":\\\"2\\\",\\\"ACQBIN\\\":\\\"510138\\\",\\\"itemnumber1\\\":\\\"a1\\\",\\\"Ecom_Payment_Card_ExpDate_Year\\\":\\\"22\\\",\\\"storekey\\\":\\\"ENDIET19\\\",\\\"MaskedPan\\\":\\\"540669***6011\\\",\\\"Fadres\\\":\\\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\\\u015ei\\\\u015fli\\\\\\/Istanbul\\\",\\\"clientIp\\\":\\\"78.186.63.129\\\",\\\"iReqDetail\\\":null,\\\"okUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"md\\\":\\\"540669:3525D9848D2D01FB53E42B0A1894159258176FBE758C9DEAA2678FF562455EEC:3790:##400940535\\\",\\\"clientId\\\":\\\"400940535\\\",\\\"vendorCode\\\":null,\\\"taksit\\\":\\\"2\\\",\\\"productcode1\\\":\\\"a2\\\",\\\"Fil\\\":\\\"Istanbul\\\",\\\"Ecom_Payment_Card_ExpDate_Month\\\":\\\"02\\\",\\\"storetype\\\":\\\"3d_pay\\\",\\\"iReqCode\\\":null,\\\"tulkekod\\\":\\\"tr\\\",\\\"mdErrorMsg\\\":\\\"Not authenticated\\\",\\\"PAResVerified\\\":\\\"false\\\",\\\"cavv\\\":null,\\\"id1\\\":\\\"a5\\\",\\\"digest\\\":\\\"digest\\\",\\\"callbackCall\\\":\\\"true\\\",\\\"failUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"cavvAlgorithm\\\":null,\\\"Fpostakodu\\\":\\\"postakod34367\\\",\\\"price1\\\":\\\"6.25\\\",\\\"faturaFirma\\\":\\\"faturaFirma\\\",\\\"xid\\\":\\\"Rb9+2QOmsiJykrDa7Qqo7MT1e3I=\\\",\\\"encoding\\\":\\\"ISO-8859-9\\\",\\\"oid\\\":null,\\\"mdStatus\\\":\\\"0\\\",\\\"dsId\\\":\\\"2\\\",\\\"eci\\\":null,\\\"version\\\":\\\"2.0\\\",\\\"Fadres2\\\":\\\"XXX\\\",\\\"Fismi\\\":\\\"is\\\",\\\"qty1\\\":\\\"3\\\",\\\"desc1\\\":\\\"a4 desc\\\",\\\"txstatus\\\":\\\"N\\\",\\\"_charset_\\\":\\\"UTF-8\\\",\\\"HASH\\\":\\\"fS7elYG2CimaAoatYJuGSmAyUHY=\\\",\\\"rnd\\\":\\\"NZSFJUHy2yEmGQSjCWaP\\\",\\\"HASHPARAMS\\\":\\\"clientid:oid:mdStatus:cavv:eci:md:rnd:\\\",\\\"HASHPARAMSVAL\\\":\\\"4009405350540669:3525D9848D2D01FB53E42B0A1894159258176FBE758C9DEAA2678FF562455EEC:3790:##400940535NZSFJUHy2yEmGQSjCWaP\\\"}\"', 'Declinedpayment', '2020-03-02 04:38:03', '2020-05-13 13:18:47'),
(39, '\"{\\\"ReturnOid\\\":\\\"ORDER-20066KecC04019399\\\",\\\"TRANID\\\":null,\\\"PAResSyntaxOK\\\":\\\"true\\\",\\\"Filce\\\":\\\"\\\\u015ei\\\\u015fli\\\",\\\"firmaadi\\\":\\\"EnBiosis\\\",\\\"islemtipi\\\":\\\"Auth\\\",\\\"total1\\\":\\\"7.50\\\",\\\"lang\\\":\\\"tr\\\",\\\"merchantID\\\":\\\"400940535\\\",\\\"maskedCreditCard\\\":\\\"5355 76** **** 9811\\\",\\\"amount\\\":\\\"1299\\\",\\\"sID\\\":\\\"2\\\",\\\"ACQBIN\\\":\\\"510138\\\",\\\"itemnumber1\\\":\\\"a1\\\",\\\"Ecom_Payment_Card_ExpDate_Year\\\":\\\"24\\\",\\\"EXTRA_CARDBRAND\\\":\\\"MASTERCARD\\\",\\\"storekey\\\":\\\"ENDIET19\\\",\\\"MaskedPan\\\":\\\"535576***9811\\\",\\\"acqStan\\\":\\\"000099\\\",\\\"Fadres\\\":\\\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\\\u015ei\\\\u015fli\\\\\\/Istanbul\\\",\\\"clientIp\\\":\\\"78.186.63.129\\\",\\\"iReqDetail\\\":null,\\\"okUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"md\\\":\\\"535576:0BF2E3E66D231A5A4B785DF1BE98B4B6AA0B53E12F8A26829FC8C77711CACCBD:4334:##400940535\\\",\\\"ProcReturnCode\\\":\\\"57\\\",\\\"clientId\\\":\\\"400940535\\\",\\\"payResults_dsId\\\":\\\"2\\\",\\\"vendorCode\\\":null,\\\"TransId\\\":\\\"20066KecC04019401\\\",\\\"EXTRA_TRXDATE\\\":\\\"20200306 10:30:28\\\",\\\"productcode1\\\":\\\"a2\\\",\\\"Fil\\\":\\\"Istanbul\\\",\\\"Ecom_Payment_Card_ExpDate_Month\\\":\\\"02\\\",\\\"storetype\\\":\\\"3d_pay\\\",\\\"iReqCode\\\":null,\\\"Response\\\":\\\"Declined\\\",\\\"tulkekod\\\":\\\"tr\\\",\\\"SettleId\\\":\\\"148\\\",\\\"mdErrorMsg\\\":\\\"Valid authentication attempt\\\",\\\"ErrMsg\\\":\\\"Kart sahibine acik olmayan islem.\\\",\\\"EXTRA_SGKTUTAR\\\":\\\"1299.0\\\",\\\"PAResVerified\\\":\\\"false\\\",\\\"cavv\\\":\\\"hufWN9WLoNcoYwAAUZVvB2gAAAA=\\\",\\\"id1\\\":\\\"a5\\\",\\\"digest\\\":\\\"digest\\\",\\\"HostRefNum\\\":\\\"006610269556\\\",\\\"callbackCall\\\":\\\"true\\\",\\\"AuthCode\\\":null,\\\"failUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"cavvAlgorithm\\\":\\\"3\\\",\\\"Fpostakodu\\\":\\\"postakod34367\\\",\\\"price1\\\":\\\"6.25\\\",\\\"faturaFirma\\\":\\\"faturaFirma\\\",\\\"xid\\\":\\\"dSYch62lj+AT4uTDA49vUnYyVyM=\\\",\\\"encoding\\\":\\\"ISO-8859-9\\\",\\\"oid\\\":null,\\\"mdStatus\\\":\\\"4\\\",\\\"dsId\\\":\\\"2\\\",\\\"eci\\\":\\\"01\\\",\\\"version\\\":\\\"2.0\\\",\\\"Fadres2\\\":\\\"XXX\\\",\\\"Fismi\\\":\\\"is\\\",\\\"qty1\\\":\\\"3\\\",\\\"desc1\\\":\\\"a4 desc\\\",\\\"EXTRA_CARDISSUER\\\":\\\"T. VAKIFLAR BANKASI T.A.O.\\\",\\\"clientid\\\":\\\"400940535\\\",\\\"txstatus\\\":\\\"A\\\",\\\"_charset_\\\":\\\"UTF-8\\\",\\\"HASH\\\":\\\"T66OyLmaqp+HEJZvp6Rf+eaAO4c=\\\",\\\"rnd\\\":\\\"mZ0htrihiOnNVwPMxWwQ\\\",\\\"HASHPARAMS\\\":\\\"clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:\\\",\\\"HASHPARAMSVAL\\\":\\\"40094053557Declined4hufWN9WLoNcoYwAAUZVvB2gAAAA=01535576:0BF2E3E66D231A5A4B785DF1BE98B4B6AA0B53E12F8A26829FC8C77711CACCBD:4334:##400940535mZ0htrihiOnNVwPMxWwQ\\\"}\"', 'Declinedpayment', '2020-03-06 04:30:35', '2020-05-13 13:18:47'),
(40, '\"{\\\"TRANID\\\":null,\\\"PAResSyntaxOK\\\":\\\"true\\\",\\\"Filce\\\":\\\"\\\\u015ei\\\\u015fli\\\",\\\"firmaadi\\\":\\\"EnBiosis\\\",\\\"islemtipi\\\":\\\"Auth\\\",\\\"total1\\\":\\\"7.50\\\",\\\"lang\\\":\\\"tr\\\",\\\"merchantID\\\":\\\"400940535\\\",\\\"maskedCreditCard\\\":\\\"5355 76** **** 8256\\\",\\\"amount\\\":\\\"1344\\\",\\\"sID\\\":\\\"2\\\",\\\"ACQBIN\\\":\\\"510138\\\",\\\"itemnumber1\\\":\\\"a1\\\",\\\"Ecom_Payment_Card_ExpDate_Year\\\":\\\"23\\\",\\\"storekey\\\":\\\"ENDIET19\\\",\\\"MaskedPan\\\":\\\"535576***8256\\\",\\\"Fadres\\\":\\\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\\\u015ei\\\\u015fli\\\\\\/Istanbul\\\",\\\"clientIp\\\":\\\"78.186.63.129\\\",\\\"iReqDetail\\\":null,\\\"okUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"md\\\":\\\"535576:A8A4D83969053D85CC8461D81C5600D9FE8C3DF708AB90FDF5E2A40E2BACA6E0:4421:##400940535\\\",\\\"clientId\\\":\\\"400940535\\\",\\\"vendorCode\\\":null,\\\"taksit\\\":\\\"3\\\",\\\"productcode1\\\":\\\"a2\\\",\\\"Fil\\\":\\\"Istanbul\\\",\\\"Ecom_Payment_Card_ExpDate_Month\\\":\\\"01\\\",\\\"storetype\\\":\\\"3d_pay\\\",\\\"iReqCode\\\":null,\\\"tulkekod\\\":\\\"tr\\\",\\\"mdErrorMsg\\\":\\\"Not authenticated\\\",\\\"PAResVerified\\\":\\\"false\\\",\\\"cavv\\\":null,\\\"id1\\\":\\\"a5\\\",\\\"digest\\\":\\\"digest\\\",\\\"callbackCall\\\":\\\"true\\\",\\\"failUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"cavvAlgorithm\\\":null,\\\"Fpostakodu\\\":\\\"postakod34367\\\",\\\"price1\\\":\\\"6.25\\\",\\\"faturaFirma\\\":\\\"faturaFirma\\\",\\\"xid\\\":\\\"XItfpFWD\\\\\\/jhEq3YKyw9E6ab6wyc=\\\",\\\"encoding\\\":\\\"ISO-8859-9\\\",\\\"oid\\\":null,\\\"mdStatus\\\":\\\"0\\\",\\\"dsId\\\":\\\"2\\\",\\\"eci\\\":null,\\\"version\\\":\\\"2.0\\\",\\\"Fadres2\\\":\\\"XXX\\\",\\\"Fismi\\\":\\\"is\\\",\\\"qty1\\\":\\\"3\\\",\\\"desc1\\\":\\\"a4 desc\\\",\\\"txstatus\\\":\\\"N\\\",\\\"_charset_\\\":\\\"UTF-8\\\",\\\"HASH\\\":\\\"nIykDY6oB52N+WNo6ijcZ8xolxE=\\\",\\\"rnd\\\":\\\"F7\\\\\\/RGYcDVT4M95CaMhKj\\\",\\\"HASHPARAMS\\\":\\\"clientid:oid:mdStatus:cavv:eci:md:rnd:\\\",\\\"HASHPARAMSVAL\\\":\\\"4009405350535576:A8A4D83969053D85CC8461D81C5600D9FE8C3DF708AB90FDF5E2A40E2BACA6E0:4421:##400940535F7\\\\\\/RGYcDVT4M95CaMhKj\\\"}\"', 'Declinedpayment', '2020-03-06 06:40:48', '2020-05-13 13:18:47'),
(41, '\"{\\\"TRANID\\\":null,\\\"PAResSyntaxOK\\\":\\\"true\\\",\\\"Filce\\\":\\\"\\\\u015ei\\\\u015fli\\\",\\\"firmaadi\\\":\\\"EnBiosis\\\",\\\"islemtipi\\\":\\\"Auth\\\",\\\"total1\\\":\\\"7.50\\\",\\\"lang\\\":\\\"tr\\\",\\\"merchantID\\\":\\\"400940535\\\",\\\"maskedCreditCard\\\":\\\"5355 76** **** 8256\\\",\\\"amount\\\":\\\"672.17\\\",\\\"sID\\\":\\\"2\\\",\\\"ACQBIN\\\":\\\"510138\\\",\\\"itemnumber1\\\":\\\"a1\\\",\\\"Ecom_Payment_Card_ExpDate_Year\\\":\\\"23\\\",\\\"storekey\\\":\\\"ENDIET19\\\",\\\"MaskedPan\\\":\\\"535576***8256\\\",\\\"Fadres\\\":\\\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\\\u015ei\\\\u015fli\\\\\\/Istanbul\\\",\\\"clientIp\\\":\\\"78.186.63.129\\\",\\\"iReqDetail\\\":null,\\\"okUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"md\\\":\\\"535576:8348D32115F73A1B6136F81A2AE3DD544C0A3C0D6E73CF77D2B1E6F631D80C52:3731:##400940535\\\",\\\"clientId\\\":\\\"400940535\\\",\\\"vendorCode\\\":null,\\\"taksit\\\":\\\"3\\\",\\\"productcode1\\\":\\\"a2\\\",\\\"Fil\\\":\\\"Istanbul\\\",\\\"Ecom_Payment_Card_ExpDate_Month\\\":\\\"08\\\",\\\"storetype\\\":\\\"3d_pay\\\",\\\"iReqCode\\\":null,\\\"tulkekod\\\":\\\"tr\\\",\\\"mdErrorMsg\\\":\\\"Not authenticated\\\",\\\"PAResVerified\\\":\\\"false\\\",\\\"cavv\\\":null,\\\"id1\\\":\\\"a5\\\",\\\"digest\\\":\\\"digest\\\",\\\"callbackCall\\\":\\\"true\\\",\\\"failUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"cavvAlgorithm\\\":null,\\\"Fpostakodu\\\":\\\"postakod34367\\\",\\\"price1\\\":\\\"6.25\\\",\\\"faturaFirma\\\":\\\"faturaFirma\\\",\\\"xid\\\":\\\"XWTkpDwtqYv3\\\\\\/JD3WyzJk4BrefI=\\\",\\\"encoding\\\":\\\"ISO-8859-9\\\",\\\"oid\\\":null,\\\"mdStatus\\\":\\\"0\\\",\\\"dsId\\\":\\\"2\\\",\\\"eci\\\":null,\\\"version\\\":\\\"2.0\\\",\\\"Fadres2\\\":\\\"XXX\\\",\\\"Fismi\\\":\\\"is\\\",\\\"qty1\\\":\\\"3\\\",\\\"desc1\\\":\\\"a4 desc\\\",\\\"txstatus\\\":\\\"N\\\",\\\"_charset_\\\":\\\"UTF-8\\\",\\\"HASH\\\":\\\"i3A7rsy26Hp2GuXV30XKOtW\\\\\\/jL4=\\\",\\\"rnd\\\":\\\"TTqWUKP7LcqzpEiAsrtV\\\",\\\"HASHPARAMS\\\":\\\"clientid:oid:mdStatus:cavv:eci:md:rnd:\\\",\\\"HASHPARAMSVAL\\\":\\\"4009405350535576:8348D32115F73A1B6136F81A2AE3DD544C0A3C0D6E73CF77D2B1E6F631D80C52:3731:##400940535TTqWUKP7LcqzpEiAsrtV\\\"}\"', 'Declinedpayment', '2020-03-06 06:42:31', '2020-05-13 13:18:47'),
(42, '\"{\\\"TRANID\\\":null,\\\"PAResSyntaxOK\\\":\\\"true\\\",\\\"Filce\\\":\\\"\\\\u015ei\\\\u015fli\\\",\\\"firmaadi\\\":\\\"EnBiosis\\\",\\\"islemtipi\\\":\\\"Auth\\\",\\\"total1\\\":\\\"7.50\\\",\\\"lang\\\":\\\"tr\\\",\\\"merchantID\\\":\\\"400940535\\\",\\\"maskedCreditCard\\\":\\\"5355 76** **** 8256\\\",\\\"amount\\\":\\\"672.17\\\",\\\"sID\\\":\\\"2\\\",\\\"ACQBIN\\\":\\\"510138\\\",\\\"itemnumber1\\\":\\\"a1\\\",\\\"Ecom_Payment_Card_ExpDate_Year\\\":\\\"23\\\",\\\"storekey\\\":\\\"ENDIET19\\\",\\\"MaskedPan\\\":\\\"535576***8256\\\",\\\"Fadres\\\":\\\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\\\u015ei\\\\u015fli\\\\\\/Istanbul\\\",\\\"clientIp\\\":\\\"78.186.63.129\\\",\\\"iReqDetail\\\":null,\\\"okUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"md\\\":\\\"535576:9F202AA481D270D57D6D0F6FFD9C1450F30E64700814DC2171992166F15850C5:3687:##400940535\\\",\\\"clientId\\\":\\\"400940535\\\",\\\"vendorCode\\\":null,\\\"taksit\\\":\\\"3\\\",\\\"productcode1\\\":\\\"a2\\\",\\\"Fil\\\":\\\"Istanbul\\\",\\\"Ecom_Payment_Card_ExpDate_Month\\\":\\\"08\\\",\\\"storetype\\\":\\\"3d_pay\\\",\\\"iReqCode\\\":null,\\\"tulkekod\\\":\\\"tr\\\",\\\"mdErrorMsg\\\":\\\"Not authenticated\\\",\\\"PAResVerified\\\":\\\"false\\\",\\\"cavv\\\":null,\\\"id1\\\":\\\"a5\\\",\\\"digest\\\":\\\"digest\\\",\\\"callbackCall\\\":\\\"true\\\",\\\"failUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"cavvAlgorithm\\\":null,\\\"Fpostakodu\\\":\\\"postakod34367\\\",\\\"price1\\\":\\\"6.25\\\",\\\"faturaFirma\\\":\\\"faturaFirma\\\",\\\"xid\\\":\\\"GKLtML2irJdPpbIRhZTHsKWuA3c=\\\",\\\"encoding\\\":\\\"ISO-8859-9\\\",\\\"oid\\\":null,\\\"mdStatus\\\":\\\"0\\\",\\\"dsId\\\":\\\"2\\\",\\\"eci\\\":null,\\\"version\\\":\\\"2.0\\\",\\\"Fadres2\\\":\\\"XXX\\\",\\\"Fismi\\\":\\\"is\\\",\\\"qty1\\\":\\\"3\\\",\\\"desc1\\\":\\\"a4 desc\\\",\\\"txstatus\\\":\\\"N\\\",\\\"_charset_\\\":\\\"UTF-8\\\",\\\"HASH\\\":\\\"gE1YedORlPKpPB8YlFjZyBFz0Uk=\\\",\\\"rnd\\\":\\\"hDZP0vrIHUBTN93pIaSC\\\",\\\"HASHPARAMS\\\":\\\"clientid:oid:mdStatus:cavv:eci:md:rnd:\\\",\\\"HASHPARAMSVAL\\\":\\\"4009405350535576:9F202AA481D270D57D6D0F6FFD9C1450F30E64700814DC2171992166F15850C5:3687:##400940535hDZP0vrIHUBTN93pIaSC\\\"}\"', 'Declinedpayment', '2020-03-06 06:43:01', '2020-05-13 13:18:47');
INSERT INTO `errors` (`id`, `json_string`, `model`, `created_at`, `updated_at`) VALUES
(43, '\"{\\\"TRANID\\\":null,\\\"PAResSyntaxOK\\\":\\\"true\\\",\\\"Filce\\\":\\\"\\\\u015ei\\\\u015fli\\\",\\\"firmaadi\\\":\\\"EnBiosis\\\",\\\"islemtipi\\\":\\\"Auth\\\",\\\"total1\\\":\\\"7.50\\\",\\\"lang\\\":\\\"tr\\\",\\\"merchantID\\\":\\\"400940535\\\",\\\"maskedCreditCard\\\":\\\"5355 76** **** 8256\\\",\\\"amount\\\":\\\"672.17\\\",\\\"sID\\\":\\\"2\\\",\\\"ACQBIN\\\":\\\"510138\\\",\\\"itemnumber1\\\":\\\"a1\\\",\\\"Ecom_Payment_Card_ExpDate_Year\\\":\\\"23\\\",\\\"storekey\\\":\\\"ENDIET19\\\",\\\"MaskedPan\\\":\\\"535576***8256\\\",\\\"Fadres\\\":\\\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\\\u015ei\\\\u015fli\\\\\\/Istanbul\\\",\\\"clientIp\\\":\\\"78.186.63.129\\\",\\\"iReqDetail\\\":null,\\\"okUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"md\\\":\\\"535576:F3AB20AD276F72A3FA20EAD4A281B1AF206A0E090B2AF2B77F8B7710BC6D3E87:3951:##400940535\\\",\\\"clientId\\\":\\\"400940535\\\",\\\"vendorCode\\\":null,\\\"taksit\\\":\\\"3\\\",\\\"productcode1\\\":\\\"a2\\\",\\\"Fil\\\":\\\"Istanbul\\\",\\\"Ecom_Payment_Card_ExpDate_Month\\\":\\\"08\\\",\\\"storetype\\\":\\\"3d_pay\\\",\\\"iReqCode\\\":null,\\\"tulkekod\\\":\\\"tr\\\",\\\"mdErrorMsg\\\":\\\"Not authenticated\\\",\\\"PAResVerified\\\":\\\"false\\\",\\\"cavv\\\":null,\\\"id1\\\":\\\"a5\\\",\\\"digest\\\":\\\"digest\\\",\\\"callbackCall\\\":\\\"true\\\",\\\"failUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"cavvAlgorithm\\\":null,\\\"Fpostakodu\\\":\\\"postakod34367\\\",\\\"price1\\\":\\\"6.25\\\",\\\"faturaFirma\\\":\\\"faturaFirma\\\",\\\"xid\\\":\\\"aWzDxXnYGJqGx0C\\\\\\/bg51LpW3cpg=\\\",\\\"encoding\\\":\\\"ISO-8859-9\\\",\\\"oid\\\":null,\\\"mdStatus\\\":\\\"0\\\",\\\"dsId\\\":\\\"2\\\",\\\"eci\\\":null,\\\"version\\\":\\\"2.0\\\",\\\"Fadres2\\\":\\\"XXX\\\",\\\"Fismi\\\":\\\"is\\\",\\\"qty1\\\":\\\"3\\\",\\\"desc1\\\":\\\"a4 desc\\\",\\\"txstatus\\\":\\\"N\\\",\\\"_charset_\\\":\\\"UTF-8\\\",\\\"HASH\\\":\\\"IQ\\\\\\/07tTYK7qbNMdfzeqeXWbVyU4=\\\",\\\"rnd\\\":\\\"QNlvjOlIHo2rkOv7uosn\\\",\\\"HASHPARAMS\\\":\\\"clientid:oid:mdStatus:cavv:eci:md:rnd:\\\",\\\"HASHPARAMSVAL\\\":\\\"4009405350535576:F3AB20AD276F72A3FA20EAD4A281B1AF206A0E090B2AF2B77F8B7710BC6D3E87:3951:##400940535QNlvjOlIHo2rkOv7uosn\\\"}\"', 'Declinedpayment', '2020-03-06 06:44:47', '2020-05-13 13:18:47'),
(44, '\"{\\\"TRANID\\\":null,\\\"PAResSyntaxOK\\\":\\\"true\\\",\\\"Filce\\\":\\\"\\\\u015ei\\\\u015fli\\\",\\\"firmaadi\\\":\\\"EnBiosis\\\",\\\"islemtipi\\\":\\\"Auth\\\",\\\"total1\\\":\\\"7.50\\\",\\\"lang\\\":\\\"tr\\\",\\\"merchantID\\\":\\\"400940535\\\",\\\"maskedCreditCard\\\":\\\"5355 76** **** 8256\\\",\\\"amount\\\":\\\"672.17\\\",\\\"sID\\\":\\\"2\\\",\\\"ACQBIN\\\":\\\"510138\\\",\\\"itemnumber1\\\":\\\"a1\\\",\\\"Ecom_Payment_Card_ExpDate_Year\\\":\\\"23\\\",\\\"storekey\\\":\\\"ENDIET19\\\",\\\"MaskedPan\\\":\\\"535576***8256\\\",\\\"Fadres\\\":\\\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\\\u015ei\\\\u015fli\\\\\\/Istanbul\\\",\\\"clientIp\\\":\\\"78.186.63.129\\\",\\\"iReqDetail\\\":null,\\\"okUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"md\\\":\\\"535576:AA8CB88137F9870CB5FB9A2B6EEEF7FDE49DAEBE23E41E9781EB1B088A998716:4591:##400940535\\\",\\\"clientId\\\":\\\"400940535\\\",\\\"vendorCode\\\":null,\\\"taksit\\\":\\\"3\\\",\\\"productcode1\\\":\\\"a2\\\",\\\"Fil\\\":\\\"Istanbul\\\",\\\"Ecom_Payment_Card_ExpDate_Month\\\":\\\"08\\\",\\\"storetype\\\":\\\"3d_pay\\\",\\\"iReqCode\\\":null,\\\"tulkekod\\\":\\\"tr\\\",\\\"mdErrorMsg\\\":\\\"Not authenticated\\\",\\\"PAResVerified\\\":\\\"false\\\",\\\"cavv\\\":null,\\\"id1\\\":\\\"a5\\\",\\\"digest\\\":\\\"digest\\\",\\\"callbackCall\\\":\\\"true\\\",\\\"failUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"cavvAlgorithm\\\":null,\\\"Fpostakodu\\\":\\\"postakod34367\\\",\\\"price1\\\":\\\"6.25\\\",\\\"faturaFirma\\\":\\\"faturaFirma\\\",\\\"xid\\\":\\\"dDJDJgFRfbT25o9FoKHRLbK+O\\\\\\/A=\\\",\\\"encoding\\\":\\\"ISO-8859-9\\\",\\\"oid\\\":null,\\\"mdStatus\\\":\\\"0\\\",\\\"dsId\\\":\\\"2\\\",\\\"eci\\\":null,\\\"version\\\":\\\"2.0\\\",\\\"Fadres2\\\":\\\"XXX\\\",\\\"Fismi\\\":\\\"is\\\",\\\"qty1\\\":\\\"3\\\",\\\"desc1\\\":\\\"a4 desc\\\",\\\"txstatus\\\":\\\"N\\\",\\\"_charset_\\\":\\\"UTF-8\\\",\\\"HASH\\\":\\\"hCdYWPh8qEjG4YR2VF1axWT5Cb8=\\\",\\\"rnd\\\":\\\"V3uZurgbpau+1VrXg8H1\\\",\\\"HASHPARAMS\\\":\\\"clientid:oid:mdStatus:cavv:eci:md:rnd:\\\",\\\"HASHPARAMSVAL\\\":\\\"4009405350535576:AA8CB88137F9870CB5FB9A2B6EEEF7FDE49DAEBE23E41E9781EB1B088A998716:4591:##400940535V3uZurgbpau+1VrXg8H1\\\"}\"', 'Declinedpayment', '2020-03-06 07:16:47', '2020-05-13 13:18:47'),
(45, '\"{\\\"ReturnOid\\\":\\\"ORDER-20092XDXI04027903\\\",\\\"TRANID\\\":null,\\\"PAResSyntaxOK\\\":\\\"true\\\",\\\"Filce\\\":\\\"\\\\u015ei\\\\u015fli\\\",\\\"firmaadi\\\":\\\"EnBiosis\\\",\\\"islemtipi\\\":\\\"Auth\\\",\\\"total1\\\":\\\"7.50\\\",\\\"lang\\\":\\\"tr\\\",\\\"merchantID\\\":\\\"400940535\\\",\\\"maskedCreditCard\\\":\\\"4836 12** **** 2417\\\",\\\"amount\\\":\\\"0.10\\\",\\\"sID\\\":\\\"1\\\",\\\"ACQBIN\\\":\\\"440293\\\",\\\"itemnumber1\\\":\\\"a1\\\",\\\"Ecom_Payment_Card_ExpDate_Year\\\":\\\"24\\\",\\\"EXTRA_CARDBRAND\\\":\\\"VISA\\\",\\\"storekey\\\":\\\"ENDIET19\\\",\\\"MaskedPan\\\":\\\"483612***2417\\\",\\\"acqStan\\\":\\\"000107\\\",\\\"Fadres\\\":\\\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\\\u015ei\\\\u015fli\\\\\\/Istanbul\\\",\\\"clientIp\\\":\\\"88.248.135.154\\\",\\\"protocol\\\":\\\"3DS1.0.2\\\",\\\"iReqDetail\\\":null,\\\"okUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"md\\\":\\\"483612:F0C70FB034CCACF9C8D18BEC2922DD5649EABAA3FB9FCCF5B7C9EB298AEB2D96:5216:##400940535\\\",\\\"ProcReturnCode\\\":\\\"54\\\",\\\"clientId\\\":\\\"400940535\\\",\\\"payResults_dsId\\\":\\\"1\\\",\\\"vendorCode\\\":null,\\\"TransId\\\":\\\"20092XDXJ04027905\\\",\\\"EXTRA_TRXDATE\\\":\\\"20200401 23:03:23\\\",\\\"productcode1\\\":\\\"a2\\\",\\\"paresTxStatus\\\":\\\"Y\\\",\\\"Fil\\\":\\\"Istanbul\\\",\\\"signature\\\":\\\"V3kWnHYZSTJ9cLtiuxCIFoRD5S2OQ+bM2QRVIUpBuHE\\\\\\/we3TEe53ZvgCbtxDp9VHK0XxZNsZDe0Zt+rcLys8qMZvSX1hsC8\\\\\\/Nqjh7KVNnd12dWSvXvdLNM0IVejdbJyEmD+bdMQYb5Vx6zdEmei3CHr819Kkzp6E3nmpCPKrmlu9qYf0ClyC7tKveDf3\\\\\\/eBBIn8cKasDiEOsvIdW5a8X7DL0XEQEKfGPqNbuvH\\\\\\/lXLHCIJfX3EQtkxPNWxL7Hv1ode6l5Is+97va18OMhIau1Fo9V0N5lH5PunVFfZaCaFObc4YVMoKkHH1KNX2taCihc5Yeqw8HZIrfkjLmf8KXmg==\\\",\\\"Ecom_Payment_Card_ExpDate_Month\\\":\\\"04\\\",\\\"storetype\\\":\\\"3d_pay\\\",\\\"iReqCode\\\":null,\\\"veresEnrolledStatus\\\":\\\"Y\\\",\\\"Response\\\":\\\"Declined\\\",\\\"tulkekod\\\":\\\"tr\\\",\\\"SettleId\\\":\\\"175\\\",\\\"mdErrorMsg\\\":\\\"Authenticated\\\",\\\"ErrMsg\\\":\\\"Kartin son kullanma tarihi hatali.\\\",\\\"EXTRA_SGKTUTAR\\\":\\\"0.1\\\",\\\"PAResVerified\\\":\\\"false\\\",\\\"cavv\\\":\\\"AAABBjWHdQAAAXZzYYd1AAAAAAA=\\\",\\\"id1\\\":\\\"a5\\\",\\\"digest\\\":\\\"digest\\\",\\\"HostRefNum\\\":\\\"009223854332\\\",\\\"callbackCall\\\":\\\"true\\\",\\\"AuthCode\\\":null,\\\"failUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"cavvAlgorithm\\\":\\\"2\\\",\\\"Fpostakodu\\\":\\\"postakod34367\\\",\\\"price1\\\":\\\"6.25\\\",\\\"faturaFirma\\\":\\\"faturaFirma\\\",\\\"xid\\\":\\\"nzfR\\\\\\/kC5+pkkO0Dp02JMQy8rzl8=\\\",\\\"encoding\\\":\\\"UTF-8\\\",\\\"oid\\\":null,\\\"mdStatus\\\":\\\"1\\\",\\\"dsId\\\":\\\"1\\\",\\\"eci\\\":\\\"05\\\",\\\"version\\\":\\\"4.0\\\",\\\"Fadres2\\\":\\\"XXX\\\",\\\"Fismi\\\":\\\"is\\\",\\\"qty1\\\":\\\"3\\\",\\\"desc1\\\":\\\"a4 desc\\\",\\\"EXTRA_CARDISSUER\\\":\\\"T. VAKIFLAR BANKASI T.A.O.\\\",\\\"clientid\\\":\\\"400940535\\\",\\\"_charset_\\\":\\\"UTF-8\\\",\\\"HASH\\\":\\\"nrg7fYfTknUwBNth3tk9r5RgVbw=\\\",\\\"rnd\\\":\\\"s5A7BCqPxl7Gnti6X6zX\\\",\\\"HASHPARAMS\\\":\\\"clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:\\\",\\\"HASHPARAMSVAL\\\":\\\"40094053554Declined1AAABBjWHdQAAAXZzYYd1AAAAAAA=05483612:F0C70FB034CCACF9C8D18BEC2922DD5649EABAA3FB9FCCF5B7C9EB298AEB2D96:5216:##400940535s5A7BCqPxl7Gnti6X6zX\\\"}\"', 'Declinedpayment', '2020-04-01 17:03:29', '2020-05-13 13:18:47'),
(46, '\"{\\\"ReturnOid\\\":\\\"ORDER-20092XKAE04028746\\\",\\\"TRANID\\\":null,\\\"PAResSyntaxOK\\\":\\\"true\\\",\\\"Filce\\\":\\\"\\\\u015ei\\\\u015fli\\\",\\\"firmaadi\\\":\\\"EnBiosis\\\",\\\"islemtipi\\\":\\\"Auth\\\",\\\"total1\\\":\\\"7.50\\\",\\\"lang\\\":\\\"tr\\\",\\\"merchantID\\\":\\\"400940535\\\",\\\"maskedCreditCard\\\":\\\"4836 12** **** 2417\\\",\\\"amount\\\":\\\"0.10\\\",\\\"sID\\\":\\\"1\\\",\\\"ACQBIN\\\":\\\"440293\\\",\\\"itemnumber1\\\":\\\"a1\\\",\\\"Ecom_Payment_Card_ExpDate_Year\\\":\\\"24\\\",\\\"EXTRA_CARDBRAND\\\":\\\"VISA\\\",\\\"storekey\\\":\\\"ENDIET19\\\",\\\"MaskedPan\\\":\\\"483612***2417\\\",\\\"acqStan\\\":\\\"000108\\\",\\\"Fadres\\\":\\\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\\\u015ei\\\\u015fli\\\\\\/Istanbul\\\",\\\"clientIp\\\":\\\"88.248.135.154\\\",\\\"protocol\\\":\\\"3DS1.0.2\\\",\\\"iReqDetail\\\":null,\\\"okUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"md\\\":\\\"483612:5816449AC369B41BBDEE74AFC2FD11157929B7A6217E909C0215E02F5DE52D3F:3736:##400940535\\\",\\\"ProcReturnCode\\\":\\\"54\\\",\\\"clientId\\\":\\\"400940535\\\",\\\"payResults_dsId\\\":\\\"1\\\",\\\"vendorCode\\\":null,\\\"TransId\\\":\\\"20092XKAF04028748\\\",\\\"EXTRA_TRXDATE\\\":\\\"20200401 23:10:00\\\",\\\"productcode1\\\":\\\"a2\\\",\\\"paresTxStatus\\\":\\\"Y\\\",\\\"Fil\\\":\\\"Istanbul\\\",\\\"signature\\\":\\\"oVfBvqT0FypP+jX1AGBpgd0VUnI6d5ARmU3TsRoXv5e3WtmRFsWHqWUpzCzs\\\\\\/LTqDB4JSem1CT\\\\\\/I9GlHArqDIHTVI4FNhZRJhIYZBkqwfJamzHJnMoIfWM9gT7gKh3m3kZ32FoNdadcTfopRLe5nQ7M3kpPwob+l+pKBUpSHrSh3+5OfY\\\\\\/0lGLypkYfhO+KveREqEA8zDn92VXexdsAahjey2g\\\\\\/dTUEFmz7xNjkNH+ht82HIy8SxgoDfUp0HivyCcoN7NgSZRz4BvoOAXe0xSqdXQi\\\\\\/WfUNoxRxmtTLk93hO7WcPa55DPu13HSw2bHPq8NEYNQEROomVWYEfqY1Jag==\\\",\\\"Ecom_Payment_Card_ExpDate_Month\\\":\\\"04\\\",\\\"storetype\\\":\\\"3d_pay\\\",\\\"iReqCode\\\":null,\\\"veresEnrolledStatus\\\":\\\"Y\\\",\\\"Response\\\":\\\"Declined\\\",\\\"tulkekod\\\":\\\"tr\\\",\\\"SettleId\\\":\\\"175\\\",\\\"mdErrorMsg\\\":\\\"Authenticated\\\",\\\"ErrMsg\\\":\\\"Kartin son kullanma tarihi hatali.\\\",\\\"EXTRA_SGKTUTAR\\\":\\\"0.1\\\",\\\"PAResVerified\\\":\\\"false\\\",\\\"cavv\\\":\\\"AAABBUREGAAAAXZzZ0QYAAAAAAA=\\\",\\\"id1\\\":\\\"a5\\\",\\\"digest\\\":\\\"digest\\\",\\\"HostRefNum\\\":\\\"009223854665\\\",\\\"callbackCall\\\":\\\"true\\\",\\\"AuthCode\\\":null,\\\"failUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"cavvAlgorithm\\\":\\\"2\\\",\\\"Fpostakodu\\\":\\\"postakod34367\\\",\\\"price1\\\":\\\"6.25\\\",\\\"faturaFirma\\\":\\\"faturaFirma\\\",\\\"xid\\\":\\\"8cukq\\\\\\/44nPNPuexLOtjIwNtKIQ4=\\\",\\\"encoding\\\":\\\"UTF-8\\\",\\\"oid\\\":null,\\\"mdStatus\\\":\\\"1\\\",\\\"dsId\\\":\\\"1\\\",\\\"eci\\\":\\\"05\\\",\\\"version\\\":\\\"4.0\\\",\\\"Fadres2\\\":\\\"XXX\\\",\\\"Fismi\\\":\\\"is\\\",\\\"qty1\\\":\\\"3\\\",\\\"desc1\\\":\\\"a4 desc\\\",\\\"EXTRA_CARDISSUER\\\":\\\"T. VAKIFLAR BANKASI T.A.O.\\\",\\\"clientid\\\":\\\"400940535\\\",\\\"_charset_\\\":\\\"UTF-8\\\",\\\"HASH\\\":\\\"A3z1GN5snpeefYQ082DpDFMM2Lg=\\\",\\\"rnd\\\":\\\"ggvFF+aoenU8A9kZpT3I\\\",\\\"HASHPARAMS\\\":\\\"clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:\\\",\\\"HASHPARAMSVAL\\\":\\\"40094053554Declined1AAABBUREGAAAAXZzZ0QYAAAAAAA=05483612:5816449AC369B41BBDEE74AFC2FD11157929B7A6217E909C0215E02F5DE52D3F:3736:##400940535ggvFF+aoenU8A9kZpT3I\\\"}\"', 'Declinedpayment', '2020-04-01 17:10:06', '2020-05-13 13:18:47'),
(47, '\"{\\\"ReturnOid\\\":\\\"ORDER-20092Xc4F04017112\\\",\\\"Filce\\\":\\\"\\\\u015ei\\\\u015fli\\\",\\\"firmaadi\\\":\\\"EnBiosis\\\",\\\"islemtipi\\\":\\\"Auth\\\",\\\"total1\\\":\\\"7.50\\\",\\\"lang\\\":\\\"tr\\\",\\\"amount\\\":\\\"0.10\\\",\\\"itemnumber1\\\":\\\"a1\\\",\\\"Ecom_Payment_Card_ExpDate_Year\\\":\\\"20\\\",\\\"storekey\\\":\\\"ENDIET19\\\",\\\"Fadres\\\":\\\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\\\u015ei\\\\u015fli\\\\\\/Istanbul\\\",\\\"clientIp\\\":\\\"88.248.135.154\\\",\\\"okUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"ProcReturnCode\\\":\\\"99\\\",\\\"clientId\\\":\\\"400940535\\\",\\\"payResults_dsId\\\":\\\"0\\\",\\\"TransId\\\":\\\"20092Xc4F04017114\\\",\\\"productcode1\\\":\\\"a2\\\",\\\"EXTRA_TRXDATE\\\":\\\"20200401 23:28:54\\\",\\\"Fil\\\":\\\"Istanbul\\\",\\\"Ecom_Payment_Card_ExpDate_Month\\\":\\\"01\\\",\\\"storetype\\\":\\\"3d_pay\\\",\\\"Response\\\":\\\"Declined\\\",\\\"tulkekod\\\":\\\"tr\\\",\\\"SettleId\\\":null,\\\"ErrMsg\\\":\\\"Kredi karti numarasi gecerli formatta degil.\\\",\\\"id1\\\":\\\"a5\\\",\\\"HostRefNum\\\":null,\\\"callbackCall\\\":\\\"true\\\",\\\"AuthCode\\\":null,\\\"failUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"Fpostakodu\\\":\\\"postakod34367\\\",\\\"price1\\\":\\\"6.25\\\",\\\"faturaFirma\\\":\\\"faturaFirma\\\",\\\"xid\\\":\\\"F+Xa+xIF4ATAgPMfm6O1NwyTvIw=\\\",\\\"encoding\\\":\\\"UTF-8\\\",\\\"oid\\\":null,\\\"dsId\\\":\\\"0\\\",\\\"Fadres2\\\":\\\"XXX\\\",\\\"Fismi\\\":\\\"is\\\",\\\"qty1\\\":\\\"3\\\",\\\"desc1\\\":\\\"a4 desc\\\",\\\"HASH\\\":\\\"wg9UQOfmvG1Fo7EZ\\\\\\/Be8+Jdpuoo=\\\",\\\"rnd\\\":\\\"\\\\\\/66MKV2IuLf1F5MSPQR6\\\",\\\"HASHPARAMS\\\":\\\"clientid:oid:AuthCode:ProcReturnCode:Response:rnd:\\\",\\\"HASHPARAMSVAL\\\":\\\"40094053599Declined\\\\\\/66MKV2IuLf1F5MSPQR6\\\"}\"', 'Declinedpayment', '2020-04-01 17:28:58', '2020-05-13 13:18:47'),
(48, '\"{\\\"TRANID\\\":null,\\\"PAResSyntaxOK\\\":\\\"true\\\",\\\"Filce\\\":\\\"\\\\u015ei\\\\u015fli\\\",\\\"firmaadi\\\":\\\"EnBiosis\\\",\\\"islemtipi\\\":\\\"Auth\\\",\\\"total1\\\":\\\"7.50\\\",\\\"lang\\\":\\\"tr\\\",\\\"merchantID\\\":\\\"400940535\\\",\\\"maskedCreditCard\\\":\\\"4836 12** **** 2417\\\",\\\"amount\\\":\\\"12990\\\",\\\"sID\\\":\\\"1\\\",\\\"ACQBIN\\\":\\\"440293\\\",\\\"itemnumber1\\\":\\\"a1\\\",\\\"Ecom_Payment_Card_ExpDate_Year\\\":\\\"24\\\",\\\"storekey\\\":\\\"ENDIET19\\\",\\\"MaskedPan\\\":\\\"483612***2417\\\",\\\"Fadres\\\":\\\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\\\u015ei\\\\u015fli\\\\\\/Istanbul\\\",\\\"clientIp\\\":\\\"88.248.135.154\\\",\\\"protocol\\\":\\\"3DS1.0.2\\\",\\\"iReqDetail\\\":null,\\\"okUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"md\\\":\\\"483612:B1F4CB10D0813F620E09598BA058EF93B9AF8E2E5A719CAE74DEDE7507407A90:4118:##400940535\\\",\\\"clientId\\\":\\\"400940535\\\",\\\"vendorCode\\\":null,\\\"productcode1\\\":\\\"a2\\\",\\\"paresTxStatus\\\":\\\"N\\\",\\\"Fil\\\":\\\"Istanbul\\\",\\\"signature\\\":\\\"Kss39QJuAsJSWuRaYb0n1pTVMW68huq++4glBZ67Y1oNvbxb120sTp5XY\\\\\\/tX0hEfwegATq2PN6CHV0oMspEPw\\\\\\/gbZEzzkgMQdleTNcryzy5ysb8vKFprdxYnBSwQbMv1znthw6zBbNYtC+p4G\\\\\\/Hhvq2NIYNuRRn2GtFHKHi1bSjbJt8dz87Q9qxHAHWUdBrR\\\\\\/65z+BO84FmpHqcbmd\\\\\\/W5H4HaHvqc2NyQGKlTyewquhvaE9zJViGg3G1w4GScMpeZTWefc5SsbM6aCgafaEFJk2nbxLXxTZepfOod5rNGAyI\\\\\\/Soj\\\\\\/gBRGjgRZ+lwv3422A5rtoa\\\\\\/yXvbJyFphQsKNg==\\\",\\\"Ecom_Payment_Card_ExpDate_Month\\\":\\\"01\\\",\\\"storetype\\\":\\\"3d_pay\\\",\\\"iReqCode\\\":null,\\\"veresEnrolledStatus\\\":\\\"Y\\\",\\\"tulkekod\\\":\\\"tr\\\",\\\"mdErrorMsg\\\":\\\"Not authenticated\\\",\\\"PAResVerified\\\":\\\"false\\\",\\\"cavv\\\":null,\\\"id1\\\":\\\"a5\\\",\\\"digest\\\":\\\"digest\\\",\\\"callbackCall\\\":\\\"true\\\",\\\"failUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"cavvAlgorithm\\\":null,\\\"Fpostakodu\\\":\\\"postakod34367\\\",\\\"price1\\\":\\\"6.25\\\",\\\"faturaFirma\\\":\\\"faturaFirma\\\",\\\"xid\\\":\\\"ZHS2Q5tV5CKFpu9iB6zUBl15a60=\\\",\\\"encoding\\\":\\\"UTF-8\\\",\\\"oid\\\":null,\\\"mdStatus\\\":\\\"0\\\",\\\"dsId\\\":\\\"1\\\",\\\"eci\\\":null,\\\"version\\\":\\\"4.0\\\",\\\"Fadres2\\\":\\\"XXX\\\",\\\"Fismi\\\":\\\"is\\\",\\\"qty1\\\":\\\"3\\\",\\\"desc1\\\":\\\"a4 desc\\\",\\\"_charset_\\\":\\\"UTF-8\\\",\\\"HASH\\\":\\\"c8LwRjWZXkMRW4RBUB5XZ1\\\\\\/0JVs=\\\",\\\"rnd\\\":\\\"49gEHTCuROIvNMUkJEHv\\\",\\\"HASHPARAMS\\\":\\\"clientid:oid:mdStatus:cavv:eci:md:rnd:\\\",\\\"HASHPARAMSVAL\\\":\\\"4009405350483612:B1F4CB10D0813F620E09598BA058EF93B9AF8E2E5A719CAE74DEDE7507407A90:4118:##40094053549gEHTCuROIvNMUkJEHv\\\"}\"', 'Declinedpayment', '2020-04-01 22:15:24', '2020-05-13 13:18:47'),
(49, '\"{\\\"TRANID\\\":null,\\\"PAResSyntaxOK\\\":\\\"true\\\",\\\"Filce\\\":\\\"\\\\u015ei\\\\u015fli\\\",\\\"firmaadi\\\":\\\"EnBiosis\\\",\\\"islemtipi\\\":\\\"Auth\\\",\\\"total1\\\":\\\"7.50\\\",\\\"lang\\\":\\\"tr\\\",\\\"merchantID\\\":\\\"400940535\\\",\\\"maskedCreditCard\\\":\\\"4836 12** **** 2417\\\",\\\"amount\\\":\\\"10392\\\",\\\"sID\\\":\\\"1\\\",\\\"ACQBIN\\\":\\\"440293\\\",\\\"itemnumber1\\\":\\\"a1\\\",\\\"Ecom_Payment_Card_ExpDate_Year\\\":\\\"24\\\",\\\"storekey\\\":\\\"ENDIET19\\\",\\\"MaskedPan\\\":\\\"483612***2417\\\",\\\"Fadres\\\":\\\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\\\u015ei\\\\u015fli\\\\\\/Istanbul\\\",\\\"clientIp\\\":\\\"88.248.135.154\\\",\\\"protocol\\\":\\\"3DS1.0.2\\\",\\\"iReqDetail\\\":null,\\\"okUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"md\\\":\\\"483612:89CEF1CCA0A3F220A5BB890A208BC99061659A3C31CE4CA7601D894F3B0B5925:3847:##400940535\\\",\\\"clientId\\\":\\\"400940535\\\",\\\"vendorCode\\\":null,\\\"productcode1\\\":\\\"a2\\\",\\\"paresTxStatus\\\":\\\"N\\\",\\\"Fil\\\":\\\"Istanbul\\\",\\\"signature\\\":\\\"S34wQi0Q0gW+pY\\\\\\/cpbfsDODKqEFthP1FfmfOjKQVFNBCa6j5nFvHs2YH1OTUsU5wdFQ9HQQXWcJgdAGEUN6j9uUAHHjYTio1aMydU7f+75y2yM6QngwIe6Z\\\\\\/cb\\\\\\/xH53SB1tbolB05y7vxSjeIxNcPYXSv2jbFu98Qv7dbW7Hb2jV9hlAMGkV07NQk6t7D64o\\\\\\/FnrWnWSKLzzQzaaLzspUpNOxE4y65KyPU+P2Hh\\\\\\/g58zIrgpA0fX0TaV5LYxkdGIZLKmPKhRk3\\\\\\/IwpbBnha7CeEf4ukmM2BceuCFvnPu+p3RIMgCEdADVTjEPQQk3sQyeEbHn14JVSwUlx4llzr\\\\\\/Qw==\\\",\\\"Ecom_Payment_Card_ExpDate_Month\\\":\\\"01\\\",\\\"storetype\\\":\\\"3d_pay\\\",\\\"iReqCode\\\":null,\\\"veresEnrolledStatus\\\":\\\"Y\\\",\\\"tulkekod\\\":\\\"tr\\\",\\\"mdErrorMsg\\\":\\\"Not authenticated\\\",\\\"PAResVerified\\\":\\\"false\\\",\\\"cavv\\\":null,\\\"id1\\\":\\\"a5\\\",\\\"digest\\\":\\\"digest\\\",\\\"callbackCall\\\":\\\"true\\\",\\\"failUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"cavvAlgorithm\\\":null,\\\"Fpostakodu\\\":\\\"postakod34367\\\",\\\"price1\\\":\\\"6.25\\\",\\\"faturaFirma\\\":\\\"faturaFirma\\\",\\\"xid\\\":\\\"osSJCkzxOM5v5ZYt\\\\\\/lWIarN0x54=\\\",\\\"encoding\\\":\\\"UTF-8\\\",\\\"oid\\\":null,\\\"mdStatus\\\":\\\"0\\\",\\\"dsId\\\":\\\"1\\\",\\\"eci\\\":null,\\\"version\\\":\\\"4.0\\\",\\\"Fadres2\\\":\\\"XXX\\\",\\\"Fismi\\\":\\\"is\\\",\\\"qty1\\\":\\\"3\\\",\\\"desc1\\\":\\\"a4 desc\\\",\\\"_charset_\\\":\\\"UTF-8\\\",\\\"HASH\\\":\\\"neyxBSiuvyPdDbyyn6QF28vNBFM=\\\",\\\"rnd\\\":\\\"DtUfI+M5JryluKfSctjv\\\",\\\"HASHPARAMS\\\":\\\"clientid:oid:mdStatus:cavv:eci:md:rnd:\\\",\\\"HASHPARAMSVAL\\\":\\\"4009405350483612:89CEF1CCA0A3F220A5BB890A208BC99061659A3C31CE4CA7601D894F3B0B5925:3847:##400940535DtUfI+M5JryluKfSctjv\\\"}\"', 'Declinedpayment', '2020-04-01 22:16:40', '2020-05-13 13:18:47'),
(50, '\"{\\\"EXTRA_TRXDATE\\\":\\\"20200402 09:45:00\\\",\\\"acqStan\\\":\\\"000111\\\",\\\"digest\\\":\\\"digest\\\",\\\"clientIp\\\":\\\"88.248.135.154\\\",\\\"Fpostakodu\\\":\\\"postakod34367\\\",\\\"callbackCall\\\":\\\"true\\\",\\\"storekey\\\":\\\"ENDIET19\\\",\\\"EXTRA_SGKTUTAR\\\":\\\"0.1\\\",\\\"faturaFirma\\\":\\\"faturaFirma\\\",\\\"clientId\\\":\\\"400940535\\\",\\\"okUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"cavvAlgorithm\\\":\\\"2\\\",\\\"EXTRA_TEBACIKLAMA\\\":\\\"B132493-TR:=Hatal\\\\u0131 taksit tutar\\\\u0131;;EN:=\\\",\\\"sID\\\":\\\"1\\\",\\\"Fil\\\":\\\"Istanbul\\\",\\\"ProcReturnCode\\\":\\\"13\\\",\\\"eci\\\":\\\"05\\\",\\\"id1\\\":\\\"a5\\\",\\\"failUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"SettleId\\\":\\\"175\\\",\\\"PAResSyntaxOK\\\":\\\"true\\\",\\\"version\\\":\\\"4.0\\\",\\\"maskedCreditCard\\\":\\\"4836 12** **** 2417\\\",\\\"MaskedPan\\\":\\\"483612***2417\\\",\\\"iReqCode\\\":null,\\\"amount\\\":\\\"0.10\\\",\\\"_charset_\\\":\\\"UTF-8\\\",\\\"taksit\\\":\\\"3\\\",\\\"Response\\\":\\\"Declined\\\",\\\"ACQBIN\\\":\\\"440293\\\",\\\"TransId\\\":\\\"20093JtAF04014888\\\",\\\"lang\\\":\\\"en\\\",\\\"total1\\\":\\\"7.50\\\",\\\"qty1\\\":\\\"3\\\",\\\"EXTRA_CARDBRAND\\\":\\\"VISA\\\",\\\"ReturnOid\\\":\\\"ORDER-20093JtAE04014886\\\",\\\"mdStatus\\\":\\\"1\\\",\\\"AuthCode\\\":null,\\\"HostRefNum\\\":\\\"009309880532\\\",\\\"storetype\\\":\\\"3d_pay\\\",\\\"merchantID\\\":\\\"400940535\\\",\\\"vendorCode\\\":null,\\\"Filce\\\":\\\"\\\\u015ei\\\\u015fli\\\",\\\"Ecom_Payment_Card_ExpDate_Month\\\":\\\"01\\\",\\\"mdErrorMsg\\\":\\\"Authenticated\\\",\\\"protocol\\\":\\\"3DS1.0.2\\\",\\\"dsId\\\":\\\"1\\\",\\\"Fadres2\\\":\\\"XXX\\\",\\\"xid\\\":\\\"R4iEM6GFkD0YbMj\\\\\\/upb3Ob9IRjk=\\\",\\\"itemnumber1\\\":\\\"a1\\\",\\\"TRANID\\\":null,\\\"Fismi\\\":\\\"is\\\",\\\"ErrMsg\\\":\\\"Invalid amount.\\\",\\\"signature\\\":\\\"fEA3zJ8yf0hKC+Vw07eeOZUzHlNgE881HI0syShTO+dd9MfdHnJ9Hf2fA7tkY3Ga6aX6B0N+nvm2uU88AoZzvEoO366\\\\\\/3PcyXdKsEf30S6xSUfGgFU5FlFhY2xu2yFoDYSVgGCYwqoj65iM0tin63pHhTi\\\\\\/4b3mYITSrIkjHO41AltZPXkI6i7wxiUgvb+4DM65k2N8dSd\\\\\\/i\\\\\\/J4S6TGA7x4+gw0Q9KDJOFANSubDMhkpFkkemOj+HI3Lixvvt9toHX4fCi6c0FbcNcnkQs148OboZd1UD+ZTKTfsDxZbt+OolT3412xI7AkwLzwB7L9al2HSc2sXcoushRumz0JIlg==\\\",\\\"tulkekod\\\":\\\"tr\\\",\\\"Fadres\\\":\\\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\\\u015ei\\\\u015fli\\\\\\/Istanbul\\\",\\\"cavv\\\":\\\"AAABAJmFgQAAAXZ0mIWBAAAAAAA=\\\",\\\"md\\\":\\\"483612:30217A07B24E8BA5797DEA1884DBF9A6719D502C8EF7FCA7726A1B59F220C5B9:4234:##400940535\\\",\\\"Ecom_Payment_Card_ExpDate_Year\\\":\\\"24\\\",\\\"firmaadi\\\":\\\"EnBiosis\\\",\\\"productcode1\\\":\\\"a2\\\",\\\"desc1\\\":\\\"a4 desc\\\",\\\"payResults_dsId\\\":\\\"1\\\",\\\"PAResVerified\\\":\\\"false\\\",\\\"veresEnrolledStatus\\\":\\\"Y\\\",\\\"islemtipi\\\":\\\"Auth\\\",\\\"EXTRA_CARDISSUER\\\":\\\"T. VAKIFLAR BANKASI T.A.O.\\\",\\\"clientid\\\":\\\"400940535\\\",\\\"oid\\\":null,\\\"iReqDetail\\\":null,\\\"price1\\\":\\\"6.25\\\",\\\"paresTxStatus\\\":\\\"Y\\\",\\\"encoding\\\":\\\"UTF-8\\\",\\\"HASH\\\":\\\"VCZ7UjpbJgvLkWe4AA\\\\\\/c1mpILaY=\\\",\\\"rnd\\\":\\\"V940loJmgbIAMAgTPGqa\\\",\\\"HASHPARAMS\\\":\\\"clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:\\\",\\\"HASHPARAMSVAL\\\":\\\"40094053513Declined1AAABAJmFgQAAAXZ0mIWBAAAAAAA=05483612:30217A07B24E8BA5797DEA1884DBF9A6719D502C8EF7FCA7726A1B59F220C5B9:4234:##400940535V940loJmgbIAMAgTPGqa\\\"}\"', 'Declinedpayment', '2020-04-02 03:45:05', '2020-05-13 13:18:47'),
(51, '\"{\\\"TRANID\\\":null,\\\"PAResSyntaxOK\\\":\\\"true\\\",\\\"Filce\\\":\\\"\\\\u015ei\\\\u015fli\\\",\\\"firmaadi\\\":\\\"EnBiosis\\\",\\\"islemtipi\\\":\\\"Auth\\\",\\\"total1\\\":\\\"7.50\\\",\\\"lang\\\":\\\"en\\\",\\\"merchantID\\\":\\\"400940535\\\",\\\"maskedCreditCard\\\":\\\"4836 12** **** 2417\\\",\\\"amount\\\":\\\"1344.34\\\",\\\"sID\\\":\\\"1\\\",\\\"ACQBIN\\\":\\\"440293\\\",\\\"itemnumber1\\\":\\\"a1\\\",\\\"Ecom_Payment_Card_ExpDate_Year\\\":\\\"24\\\",\\\"storekey\\\":\\\"ENDIET19\\\",\\\"MaskedPan\\\":\\\"483612***2417\\\",\\\"Fadres\\\":\\\"Harbiye, Atiye Sokak. Kat: 2 No:8, \\\\u015ei\\\\u015fli\\\\\\/Istanbul\\\",\\\"clientIp\\\":\\\"88.248.135.154\\\",\\\"protocol\\\":\\\"3DS1.0.2\\\",\\\"iReqDetail\\\":null,\\\"okUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"md\\\":\\\"483612:78C632B3E5ECA961FDD093F372941ABD210C566F4D45D7AC10AB24DD94D3B27C:4486:##400940535\\\",\\\"clientId\\\":\\\"400940535\\\",\\\"vendorCode\\\":null,\\\"taksit\\\":\\\"3\\\",\\\"productcode1\\\":\\\"a2\\\",\\\"paresTxStatus\\\":\\\"N\\\",\\\"Fil\\\":\\\"Istanbul\\\",\\\"signature\\\":\\\"CLTEJUOP2WkUF3BMZD5PIce\\\\\\/soJNarVZWCNJlOaphN7hQ8YsAXRvJ\\\\\\/GXFLC5fDSnXLDXtAo7UTYJGJLgToCxi6xmUyjf1kAybYmBJKSHzQCBj05ulfBuRMdMf64binYI+uOujT0+iYMSmWYf4cnfqzQdAKtHffbIm\\\\\\/o+uQ3cuBzBTwszPPgMh1eWI3OAjvLfCLyO2x3hvNE3bVyOTh82BSOw6oU59tPHEPFKpVLAapHuc1fPnns3JMlcsWzQLbZ48jbIWN3JO7Vang1JP0SvE\\\\\\/Uv08V4jXU8\\\\\\/fReYBoVmFi8sAaPaat\\\\\\/\\\\\\/wkUp7xWqrrrCrTJI2hJcykhIG7E4WgSLw==\\\",\\\"Ecom_Payment_Card_ExpDate_Month\\\":\\\"01\\\",\\\"storetype\\\":\\\"3d_pay\\\",\\\"iReqCode\\\":null,\\\"veresEnrolledStatus\\\":\\\"Y\\\",\\\"tulkekod\\\":\\\"tr\\\",\\\"mdErrorMsg\\\":\\\"Not authenticated\\\",\\\"PAResVerified\\\":\\\"false\\\",\\\"cavv\\\":null,\\\"id1\\\":\\\"a5\\\",\\\"digest\\\":\\\"digest\\\",\\\"callbackCall\\\":\\\"true\\\",\\\"failUrl\\\":\\\"http:\\\\\\/\\\\\\/localhost\\\\\\/enbiosis\\\\\\/public\\\\\\/checkout-result\\\",\\\"cavvAlgorithm\\\":null,\\\"Fpostakodu\\\":\\\"postakod34367\\\",\\\"price1\\\":\\\"6.25\\\",\\\"faturaFirma\\\":\\\"faturaFirma\\\",\\\"xid\\\":\\\"JYHLX9dJwTRuhM4RR0zCy3MkL50=\\\",\\\"encoding\\\":\\\"UTF-8\\\",\\\"oid\\\":null,\\\"mdStatus\\\":\\\"0\\\",\\\"dsId\\\":\\\"1\\\",\\\"eci\\\":null,\\\"version\\\":\\\"4.0\\\",\\\"Fadres2\\\":\\\"XXX\\\",\\\"Fismi\\\":\\\"is\\\",\\\"qty1\\\":\\\"3\\\",\\\"desc1\\\":\\\"a4 desc\\\",\\\"_charset_\\\":\\\"UTF-8\\\",\\\"HASH\\\":\\\"\\\\\\/eLoa23nwkhWxjqapnhCvD0Uhpg=\\\",\\\"rnd\\\":\\\"00Y3nR6KAwLM4\\\\\\/IbjTkK\\\",\\\"HASHPARAMS\\\":\\\"clientid:oid:mdStatus:cavv:eci:md:rnd:\\\",\\\"HASHPARAMSVAL\\\":\\\"4009405350483612:78C632B3E5ECA961FDD093F372941ABD210C566F4D45D7AC10AB24DD94D3B27C:4486:##40094053500Y3nR6KAwLM4\\\\\\/IbjTkK\\\"}\"', 'Declinedpayment', '2020-04-02 03:49:41', '2020-05-13 13:18:47');

-- --------------------------------------------------------

--
-- Table structure for table `kits`
--

CREATE TABLE `kits` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kit_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `microbiome` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `food` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `order_detail_id` bigint(20) DEFAULT NULL,
  `survey_id` bigint(20) DEFAULT NULL,
  `registered` tinyint(1) NOT NULL DEFAULT 0,
  `registered_at` timestamp NULL DEFAULT NULL,
  `delevered_to_user_at` timestamp NULL DEFAULT NULL,
  `received_from_user_at` timestamp NULL DEFAULT NULL,
  `send_to_lab_at` timestamp NULL DEFAULT NULL,
  `uploaded_at` timestamp NULL DEFAULT NULL,
  `survey_filled_at` timestamp NULL DEFAULT NULL,
  `kit_status` tinyint(1) NOT NULL DEFAULT 0,
  `sell_status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kits`
--

INSERT INTO `kits` (`id`, `kit_code`, `microbiome`, `food`, `user_id`, `order_detail_id`, `survey_id`, `registered`, `registered_at`, `delevered_to_user_at`, `received_from_user_at`, `send_to_lab_at`, `uploaded_at`, `survey_filled_at`, `kit_status`, `sell_status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'omar123', NULL, NULL, NULL, 16, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-16 07:44:47', '2020-02-26 05:59:23', NULL),
(2, NULL, NULL, NULL, NULL, 16, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-16 07:44:47', '2020-02-12 12:27:24', NULL),
(3, NULL, NULL, NULL, NULL, 16, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-16 07:44:47', '2020-02-19 12:02:29', NULL),
(4, NULL, NULL, NULL, NULL, 16, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-16 07:44:47', '2020-02-19 12:03:15', NULL),
(5, NULL, NULL, NULL, NULL, 16, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-16 07:44:47', '2020-01-16 07:44:47', NULL),
(6, NULL, NULL, NULL, NULL, 16, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-16 07:44:47', '2020-01-16 07:44:47', NULL),
(7, NULL, NULL, NULL, NULL, 16, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-16 07:44:47', '2020-01-16 07:44:47', NULL),
(8, NULL, NULL, NULL, NULL, 16, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-16 07:44:47', '2020-01-16 07:44:47', NULL),
(9, NULL, NULL, NULL, NULL, 16, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-16 07:44:47', '2020-01-16 07:44:47', NULL),
(10, NULL, NULL, NULL, NULL, 16, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-16 07:44:47', '2020-01-16 07:44:47', NULL),
(11, NULL, NULL, NULL, NULL, 16, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-16 07:44:47', '2020-01-16 07:44:47', NULL),
(12, NULL, NULL, NULL, NULL, 16, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-16 07:44:47', '2020-01-16 07:44:47', NULL),
(13, NULL, NULL, NULL, NULL, 17, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-16 07:44:47', '2020-01-16 07:44:47', NULL),
(14, NULL, NULL, NULL, NULL, 17, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-16 07:44:47', '2020-01-16 07:44:47', NULL),
(15, 'laithatik1', NULL, NULL, NULL, 17, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-16 07:44:47', '2020-04-08 13:20:06', NULL),
(16, NULL, NULL, NULL, NULL, 17, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-16 07:44:47', '2020-01-16 07:44:47', NULL),
(17, NULL, NULL, NULL, NULL, 17, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-16 07:44:47', '2020-01-16 07:44:47', NULL),
(18, NULL, NULL, NULL, NULL, 17, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-16 07:44:47', '2020-01-16 07:44:47', NULL),
(19, NULL, NULL, NULL, NULL, 18, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-16 07:44:47', '2020-01-16 07:44:47', NULL),
(20, NULL, NULL, NULL, NULL, 18, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-16 07:44:47', '2020-01-16 07:44:47', NULL),
(21, NULL, NULL, NULL, NULL, 18, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-16 07:44:47', '2020-01-16 07:44:47', NULL),
(22, NULL, NULL, NULL, NULL, 18, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-16 07:44:47', '2020-01-16 07:44:47', NULL),
(23, NULL, NULL, NULL, NULL, 18, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-16 07:44:47', '2020-01-16 07:44:47', NULL),
(24, NULL, NULL, NULL, NULL, 18, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-16 07:44:47', '2020-01-16 07:44:47', NULL),
(527, NULL, NULL, NULL, NULL, 28, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-21 10:05:26', '2020-02-19 08:25:17', NULL),
(528, NULL, NULL, NULL, NULL, 28, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-21 10:05:26', '2020-02-19 08:16:56', NULL),
(529, NULL, NULL, NULL, NULL, 28, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-21 10:05:26', '2020-02-19 08:17:37', NULL),
(530, NULL, NULL, NULL, NULL, 28, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-21 10:05:26', '2020-01-21 10:05:26', NULL),
(531, NULL, NULL, NULL, NULL, 28, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-21 10:05:26', '2020-01-21 10:05:26', NULL),
(532, NULL, NULL, NULL, NULL, 28, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-21 10:05:26', '2020-01-21 10:05:26', NULL),
(533, NULL, NULL, NULL, NULL, 28, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-21 10:05:26', '2020-01-21 10:05:26', NULL),
(534, NULL, NULL, NULL, NULL, 28, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-21 10:05:26', '2020-01-21 10:05:26', NULL),
(535, NULL, NULL, NULL, NULL, 28, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-21 10:05:26', '2020-01-21 10:05:26', NULL),
(536, NULL, NULL, NULL, NULL, 28, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-21 10:05:26', '2020-01-21 10:05:26', NULL),
(537, NULL, NULL, NULL, NULL, 28, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-21 10:05:26', '2020-01-21 10:05:26', NULL),
(538, NULL, NULL, NULL, NULL, 28, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-21 10:05:26', '2020-01-21 10:05:26', NULL),
(539, NULL, NULL, NULL, NULL, 29, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-21 10:05:26', '2020-01-21 10:05:26', NULL),
(540, NULL, NULL, NULL, NULL, 29, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-21 10:05:26', '2020-01-21 10:05:26', NULL),
(541, NULL, NULL, NULL, NULL, 29, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-21 10:05:26', '2020-01-21 10:05:26', NULL),
(542, NULL, NULL, NULL, NULL, 29, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-21 10:05:26', '2020-01-21 10:05:26', NULL),
(543, NULL, NULL, NULL, NULL, 29, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-21 10:05:26', '2020-01-21 10:05:26', NULL),
(544, NULL, NULL, NULL, NULL, 29, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-21 10:05:26', '2020-01-21 10:05:26', NULL),
(545, NULL, NULL, NULL, NULL, 29, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-21 10:05:26', '2020-01-21 10:05:26', NULL),
(546, NULL, NULL, NULL, NULL, 29, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-21 10:05:26', '2020-01-21 10:05:26', NULL),
(547, NULL, NULL, NULL, NULL, 29, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-21 10:05:26', '2020-01-21 10:05:26', NULL),
(548, NULL, NULL, NULL, NULL, 29, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-21 10:05:26', '2020-01-21 10:05:26', NULL),
(549, NULL, NULL, NULL, NULL, 30, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-21 10:05:26', '2020-01-21 10:05:26', NULL),
(550, NULL, NULL, NULL, NULL, 30, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-21 10:05:26', '2020-01-21 10:05:26', NULL),
(551, NULL, NULL, NULL, NULL, 30, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-21 10:05:26', '2020-01-21 10:05:26', NULL),
(552, NULL, NULL, NULL, NULL, 30, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-21 10:05:27', '2020-01-21 10:05:27', NULL),
(553, 'qaswed', NULL, NULL, NULL, 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-21 03:36:47', '2020-02-28 05:44:27', NULL),
(554, '12', '{\"Mikrobiyom \\u00c7e\\u015fitlili\\u011finiz\":\"6\",\"\\u00d6nemli Organizmalar\\u0131n\\u0131z\":{\"ORGANIZMA\":[\"KULLANICI\",\"TOPLUM\"],\"Alistipes\":[\"0\",\"76\"],\"Anaerostipes\":[\"0\",\"74\"],\"Butyrivibrio\":[\"0\",\"90\"],\"Phascolarctobacterium\":[\"0\",\"70\"],\"Escherichia\":[\"0\",\"92\"],\"Bacteroides\":[\"3\",\"54\"],\"Blautia\":[\"7\",\"64\"],\"Lachnospiraceae\":[\"8\",\"56\"],\"Ruminococcus\":[\"9\",\"64\"],\"Parabacteroides\":[\"11\",\"65\"],\"Coprococcus\":[\"15\",\"62\"],\"Dorea\":[\"16\",\"70\"],\"Bifidobacterium\":[\"21\",\"77\"],\"Clostridium\":[\"22\",\"72\"],\"Roseburia\":[\"25\",\"70\"],\"Akkermansia\":[\"29\",\"80\"],\"Haemophilus\":[\"32\",\"83\"],\"Eubacterium\":[\"47\",\"79\"],\"Collinsella\":[\"51\",\"73\"],\"Anaerotruncus\":[\"55\",\"78\"],\"Ruminococcaceae\":[\"61\",\"53\"],\"Faecalibacterium\":[\"75\",\"61\"],\"Streptococcus\":[\"78\",\"81\"],\"Desulfovibrio\":[\"90\",\"74\"],\"Catenibacterium\":[\"91\",\"91\"],\"Prevotella\":[\"96\",\"81\"]},\"Taksonomik Analiz\":{\"Taksonomik Analiz (Cins Seviyesi)\":{\"TAX\":[\"KULLANICI\",\"TOPLUM\",\"SAGLIKLI\"],\"Prevotella\":[\"4.444819602546787\",\"0.1755526608462995\",\"0.18805477790579875\"],\"Succinivibrio\":[\"13.055903916650587\",\"0.06243439885384906\",\"0.0868518692885645\"],\"Ruminococcaceae\":[\"9.897742620104188\",\"8.59439235986533\",\"9.066843166957936\"],\"Faecalibacterium\":[\"9.35148562608528\",\"6.514846244369883\",\"6.706931790107228\"],\"Clostridiales\":[\"5.65792012348061\",\"4.955044777367546\",\"5.690149576974722\"],\"Di\\u011fer\":[\"22.847530387806287\",\"79.52217689785078\",\"78.07311404085995\"]},\"Taksonomik Analiz (Aile seviyesi)\":{\"TAX\":[\"KULLANICI\",\"TOPLUM\",\"SAGLIKLI\"],\"Prevotellaceae\":[\"34.74459772332626\",\"4.464190420269295\",\"5.339408930792739\"],\"Ruminococcaceae\":[\"20.984468454562993\",\"18.593607440392915\",\"19.173199559772755\"],\"Succinivibrionaceae\":[\"13.063139108624346\",\"0.06278135179239813\",\"0.08729209740417539\"],\"Clostridiales\":[\"5.65792012348061\",\"4.955044777367546\",\"5.690149576974722\"],\"Paraprevotellaceae\":[\"4.651022573798958\",\"0.7241708123085702\",\"0.8064155859179338\"],\"Di\\u011fer\":[\"20.898852016206845\",\"71.20020519786927\",\"68.90353424913768\"]},\"Taksonomik Analiz (\\u015eube Seviyesi)\":{\"TAX\":[\"KULLANICI\",\"TOPLUM\",\"SAGLIKLI\"],\"Bacteroidetes\":[\"40.990980127339384\",\"37.866835633649075\",\"37.89050589756257\"],\"Firmicutes\":[\"40.33740111904302\",\"46.73062860310761\",\"47.58803695446815\"],\"Di\\u011fer\":[\"18.671618753617594\",\"15.402535763243321\",\"14.521457147969272\"]}},\"Yak\\u0131n Profiller\":{\"PROFIL\":[\"YUZDE\"],\"fazla kilolu\":[\"20\"],\"cilt rahats\\u0131zl\\u0131klar\\u0131na sahip\":[\"19\"],\"laktoz hassasiyeti problemi var\":[\"14\"],\"akci\\u011fer rahats\\u0131zl\\u0131\\u011f\\u0131na sahip\":[\"10\"],\"migren rahats\\u0131zl\\u0131\\u011f\\u0131 ya\\u015f\\u0131yor\":[\"10\"],\"vejetaryen, ancak deniz \\u00fcr\\u00fcnleri t\\u00fcketiyor\":[\"10\"],\"gluten hassasiyeti veya alerjisine sahip\":[\"8\"],\"zaman zaman ishal problemi ya\\u015f\\u0131yor\":[\"8\"],\"dikkat bozuklu\\u011fu rahats\\u0131zl\\u0131\\u011f\\u0131 ya\\u015f\\u0131yor\":[\"7\"],\"g\\u0131da alerjisi bulunuyor\":[\"6\"],\"k\\u0131rm\\u0131z\\u0131 et yemiyor\":[\"6\"],\"hayat\\u0131n\\u0131n bir d\\u00f6neminde kanser te\\u015fhisi alm\\u0131\\u015f\":[\"6\"],\"vejetaryen\":[\"5\"],\"huzursuz ba\\u011f\\u0131rsak sendromu ya\\u015f\\u0131yor\":[\"5\"],\"karaci\\u011fer rahats\\u0131zl\\u0131\\u011f\\u0131na sahip\":[\"5\"],\"tiroid rahats\\u0131zl\\u0131\\u011f\\u0131na sahip\":[\"4\"],\"kalp rahats\\u0131zl\\u0131\\u011f\\u0131 ya\\u015f\\u0131yor\":[\"3\"]},\"Ba\\u011f\\u0131rsak Skorlar\\u0131\":{\"V\\u00fccut K\\u00fctle Endeksi-Mikrobiyom \\u0130li\\u015fkiniz\":[\"19\",\"15\",\"72\",\"35\"],\"Karbonhidrat Metabolizman\\u0131z\":[\"46\",\"\",\"\",\"48\"],\"Protein Metabolizman\\u0131z\":[\"1\",\"\",\"\",\"29\"],\"Ya\\u011f Metabolizman\\u0131z\":[\"2\",\"\",\"\",\"29\"],\"Probiyotik Mikroorganizma \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"39\",\"17\",\"78\",\"43\"],\"Uyku Kaliteniz\":[\"77\",\"23\",\"84\",\"74\"],\"Ba\\u011f\\u0131rsak Hareketlilik \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"19\",\"29\",\"69\",\"35\"],\"Gluten Hassasiyeti \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"19\",\"16\",\"64\",\"24\"],\"Laktoz Hassasiyeti \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"12\",\"15\",\"82\",\"27\"],\"Vitamin Senteziniz\":[\"83\",\"56\",\"73\",\"67\"],\"Mikrobiyom Ya\\u015f\\u0131n\\u0131z\":[\"27\",\"\",\"\",\"\"],\"Yapay Tatland\\u0131r\\u0131c\\u0131 Hasar\\u0131n\\u0131z\":[\"58\",\"27\",\"58\",\"32\"],\"\\u015eeker T\\u00fcketim Skorunuz\":[\"49\",\"7\",\"81\",\"59\"],\"Antibiyotik Hasar \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"14\",\"27\",\"74\",\"34\"],\"Otoimm\\u00fcnite Endeksiniz\":[\"9\",\"23\",\"62\",\"29\"]}}', '{\"Ada\\u00e7ay\\u0131\":5,\"Ahududu\":6,\"Alabal\\u0131k\":7,\"Amaranth\":3,\"Ananas\":4,\"Antep F\\u0131st\\u0131\\u011f\\u0131\":3,\"Armut\":4,\"Armut Suyu\":6,\"Arpa\":8,\"Asma Yapra\\u011f\\u0131\":4,\"Aspir Ya\\u011f\\u0131\":5,\"Avokado\":5,\"Ay\\u00e7ekirde\\u011fi\":3,\"Ay\\u00e7i\\u00e7ek Ya\\u011f\\u0131\":5,\"\\u0130nek S\\u00fct\\u00fc (Az Ya\\u011fl\\u0131)\":5,\"A\\u00e7ai \\u00dcz\\u00fcm\\u00fc\":7,\"Badem \":5,\"Bakla\":8,\"Balkaba\\u011f\\u0131\":5,\"Bal\\u0131k Ya\\u011f\\u0131 Destekleri\":7,\"Barbunya\":7,\"Beyaz Dut\":5,\"Beyaz Ekmek\":5,\"Beyaz Peynir (Tam Ya\\u011fl\\u0131)\":7,\"Beyaz Un\":3,\"Bezelye\":5,\"Bira\":7,\"Brokoli\":4,\"Br\\u00fcksel Lahanas\\u0131\":5,\"Bulgur\":4,\"Bu\\u011fday\":4,\"Bu\\u011fday Ru\\u015feymi\":8,\"Bu\\u011fday Unu\":5,\"B\\u00f6r\\u00fclce\":5,\"B\\u00f6\\u011f\\u00fcrtlen\":4,\"Ceviz\":5,\"Cheddar\":6,\"Chia Tohumu\":6,\"Dana Eti\":4,\"Domates\":0,\"Dut Kurusu\":5,\"Ebeg\\u00fcmeci\":5,\"Elma\":3,\"Elma Suyu\":3,\"Enginar\":4,\"Erik\":4,\"Eski Ka\\u015far\":6,\"Esmer Pirin\\u00e7\":5,\"Ezine Peyniri\":6,\"Frambuaz Suyu\":4,\"F\\u0131nd\\u0131k\":3,\"F\\u0131nd\\u0131k Ya\\u011f\\u0131\":4,\"Grayver\":6,\"Greyfurt \":3,\"Greyfurt Suyu\":4,\"Hamsi\":6,\"Havu\\u00e7\":3,\"Havu\\u00e7 Suyu\":6,\"Hindi Eti\":3,\"Hindiba\":3,\"Hindistan Cevizi\":5,\"Taze Hurma\":4,\"Kabak \\u00c7ekirde\\u011fi\":5,\"Kahve\":4,\"Kaju\":5,\"Kakao\":4,\"Kale\":4,\"Kanola Ya\\u011f\\u0131\":4,\"Kapari\":2,\"Karabu\\u011fday \":3,\"Karadut\":5,\"Karalahana\":4,\"Karides\":8,\"Karnabahar\":6,\"Kavun\":4,\"Kaymak\":5,\"Taze Kay\\u0131s\\u0131\":3,\"Kay\\u0131s\\u0131 Suyu\":6,\"Ka\\u015far Peyniri\":5,\"Kefir \":5,\"Kepekli Ekmek\":5,\"Kepekli Makarna\":5,\"Kereviz\":6,\"Kestane\":4,\"Keten Tohumu\":10,\"Ke\\u00e7i Eti\":4,\"Ke\\u00e7i Peyniri\":6,\"Ke\\u00e7i S\\u00fct\\u00fc\":5,\"Kinoa\":3,\"Kivi \":6,\"Koyun Eti \":3,\"Koyun Peyniri\":6,\"Koyun S\\u00fct\\u00fc\":5,\"Krem Peynir\":6,\"Kril Ya\\u011f\\u0131 Destekleri\":8,\"Kuru Fasulye\":5,\"Kuru Hurma\":4,\"Kuru Kay\\u0131s\\u0131\":5,\"Kuru \\u00dcz\\u00fcm\":9,\"Kuyruk Ya\\u011f\\u0131\":4,\"Kuzu Eti \":7,\"Ku\\u015fkonmaz\":5,\"K\\u0131l\\u0131\\u00e7 Bal\\u0131\\u011f\\u0131\":5,\"K\\u0131rm\\u0131z\\u0131-Mor \\u00dcz\\u00fcm\":8,\"K\\u0131rm\\u0131z\\u0131 Biber\":3,\"Frenk \\u00dcz\\u00fcm\\u00fc\":8,\"K\\u0131rm\\u0131z\\u0131 Pancar\":4,\"K\\u0131rm\\u0131z\\u0131 \\u015earap\":4,\"K\\u0131z\\u0131lc\\u0131k\":5,\"Beyaz Lahana\":7,\"Lava\\u015f\":6,\"Levrek\":5,\"Limon\":3,\"Lor Peyniri\":7,\"L\\u00fcfer\":5,\"Maden Suyu\":5,\"Makarna\":5,\"Manda S\\u00fct\\u00fc\":4,\"Mandalina \":3,\"Mandalina Suyu\":3,\"Mango\":4,\"Mantar\":9,\"Margarin\":6,\"Maydanoz\":3,\"Mercimek\":6,\"Midye \":7,\"Mor Lahana\":4,\"Morina Bal\\u0131\\u011f\\u0131\":5,\"Muz\":5,\"M\\u0131s\\u0131r\":4,\"M\\u0131s\\u0131r Gevre\\u011fi\":5,\"M\\u0131s\\u0131r Ke\\u011fe\\u011fi\":7,\"M\\u0131s\\u0131r Ni\\u015fastas\\u0131\":7,\"M\\u0131s\\u0131r Unu\":4,\"M\\u0131s\\u0131r Ya\\u011f\\u0131\":5,\"M\\u0131s\\u0131r\\u00f6z\\u00fc Ya\\u011f\\u0131\":5,\"Taze Nane\":4,\"Nar Suyu\":5,\"Nar\":5,\"Nektarin\":3,\"Nohut\":6,\"Noodle\":5,\"Oolong \\u00c7ay\\u0131\":6,\"Otlu Peynir\":6,\"Palamut\":6,\"Papaya\":4,\"\\u0130nek S\\u00fct\\u00fc (Tam Ya\\u011fl\\u0131)\":4,\"Patates\":4,\"Patl\\u0131can\":4,\"Pembe \\u015earap\":3,\"Pirin\\u00e7\":5,\"Pirin\\u00e7 Unu\":3,\"Portakal\":4,\"Portakal Suyu\":4,\"Protein Barlar\":5,\"Ricotta\":6,\"Roka\":3,\"Rokfor\":6,\"Salep\":6,\"Sardalya\":6,\"Sar\\u0131msak\":5,\"Semizotu \":5,\"Siyah Havu\\u00e7\":4,\"Siyah \\u00c7ay\":4,\"Somon\":6,\"Soya Fasulyesi\":3,\"Soya Filizi\":2,\"Soya Unu\":2,\"Soya Ya\\u011f\\u0131\":5,\"So\\u011fan\":5,\"Tere\":4,\"Susam\":4,\"S\\u00fctl\\u00fc kremalar\":6,\"S\\u0131\\u011f\\u0131r Eti\":4,\"Tam Bu\\u011fday Unu\":5,\"Tam Tah\\u0131ll\\u0131 Ekmek\":8,\"Tam Tah\\u0131ll\\u0131 Gevrekler\":7,\"Tam Tah\\u0131l Unu\":6,\"Tatl\\u0131 Patates\":4,\"Tavuk Eti\":5,\"Tereya\\u011f\\u0131\":5,\"Tofu\":2,\"Ton Bal\\u0131\\u011f\\u0131\":5,\"Tulum Peyniri\":6,\"Tuna Bal\\u0131\\u011f\\u0131\":5,\"Turp\":6,\"Uskumru\":5,\"Vi\\u015fne \":5,\"Vi\\u015fne Suyu\":6,\"Whey Protein Destekleri\":5,\"Yaban Mersini\":6,\"Yo\\u011furt (Az Ya\\u011fl\\u0131)\":5,\"Yenge\\u00e7\":7,\"Yer Elmas\\u0131\":3,\"Yer F\\u0131st\\u0131\\u011f\\u0131\":5,\"Taze Fasulye\":5,\"Ye\\u015fil \\u00c7ay\":4,\"Yo\\u011furt (Tam Ya\\u011fl\\u0131)\":4,\"Yulaf\":8,\"Yulaf Kepe\\u011fi\":4,\"Yulafl\\u0131 Ekmek\":6,\"Yumurta\":4,\"Yumurta Beyaz\\u0131\":5,\"Yumurta Sar\\u0131s\\u0131\":7,\"Zeytin\":5,\"Zeytin Ezmesi\":4,\"Zeytinya\\u011f\\u0131\":4,\"\\u00c7am F\\u0131st\\u0131\\u011f\\u0131\":4,\"\\u00c7avdar\":6,\"\\u00c7avdar Ekme\\u011fi\":5,\"\\u00c7avdar Unu\":5,\"\\u00c7ilek\":1,\"\\u00c7ipura\":6,\"\\u00c7\\u00f6kelek\":5,\"\\u00dcz\\u00fcm Suyu \":6,\"\\u015ealgam\":4,\"\\u015eeftali\":4}', 124, 2, 16, 1, '2020-05-21 16:17:42', NULL, NULL, NULL, '2020-04-03 08:45:45', '2020-03-04 12:46:43', 4, 1, '2020-01-21 03:36:47', '2020-05-21 16:17:42', NULL),
(555, NULL, NULL, NULL, NULL, 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-01-21 03:36:47', '2020-02-19 12:16:04', NULL),
(556, NULL, NULL, NULL, NULL, 31, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-02-19 05:15:06', '2020-02-19 12:11:09', NULL),
(562, '123123', NULL, NULL, NULL, 32, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2020-02-20 08:28:27', '2020-04-08 16:31:50', NULL),
(563, 'qwe', NULL, NULL, NULL, 32, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-02-20 08:28:27', '2020-03-02 12:58:44', NULL),
(564, '123', '{\"Mikrobiyom \\u00c7e\\u015fitlili\\u011finiz\":\"6\",\"\\u00d6nemli Organizmalar\\u0131n\\u0131z\":{\"ORGANIZMA\":[\"KULLANICI\",\"TOPLUM\"],\"Alistipes\":[\"0\",\"76\"],\"Anaerostipes\":[\"0\",\"74\"],\"Butyrivibrio\":[\"0\",\"90\"],\"Phascolarctobacterium\":[\"0\",\"70\"],\"Escherichia\":[\"0\",\"92\"],\"Bacteroides\":[\"3\",\"54\"],\"Blautia\":[\"7\",\"64\"],\"Lachnospiraceae\":[\"8\",\"56\"],\"Ruminococcus\":[\"9\",\"64\"],\"Parabacteroides\":[\"11\",\"65\"],\"Coprococcus\":[\"15\",\"62\"],\"Dorea\":[\"16\",\"70\"],\"Bifidobacterium\":[\"21\",\"77\"],\"Clostridium\":[\"22\",\"72\"],\"Roseburia\":[\"25\",\"70\"],\"Akkermansia\":[\"29\",\"80\"],\"Haemophilus\":[\"32\",\"83\"],\"Eubacterium\":[\"47\",\"79\"],\"Collinsella\":[\"51\",\"73\"],\"Anaerotruncus\":[\"55\",\"78\"],\"Ruminococcaceae\":[\"61\",\"53\"],\"Faecalibacterium\":[\"75\",\"61\"],\"Streptococcus\":[\"78\",\"81\"],\"Desulfovibrio\":[\"90\",\"74\"],\"Catenibacterium\":[\"91\",\"91\"],\"Prevotella\":[\"96\",\"81\"]},\"Taksonomik Analiz\":{\"Taksonomik Analiz (Cins Seviyesi)\":{\"TAX\":[\"KULLANICI\",\"TOPLUM\",\"SAGLIKLI\"],\"Prevotella\":[\"4.444819602546787\",\"0.1755526608462995\",\"0.18805477790579875\"],\"Succinivibrio\":[\"13.055903916650587\",\"0.06243439885384906\",\"0.0868518692885645\"],\"Ruminococcaceae\":[\"9.897742620104188\",\"8.59439235986533\",\"9.066843166957936\"],\"Faecalibacterium\":[\"9.35148562608528\",\"6.514846244369883\",\"6.706931790107228\"],\"Clostridiales\":[\"5.65792012348061\",\"4.955044777367546\",\"5.690149576974722\"],\"Di\\u011fer\":[\"22.847530387806287\",\"79.52217689785078\",\"78.07311404085995\"]},\"Taksonomik Analiz (Aile seviyesi)\":{\"TAX\":[\"KULLANICI\",\"TOPLUM\",\"SAGLIKLI\"],\"Prevotellaceae\":[\"34.74459772332626\",\"4.464190420269295\",\"5.339408930792739\"],\"Ruminococcaceae\":[\"20.984468454562993\",\"18.593607440392915\",\"19.173199559772755\"],\"Succinivibrionaceae\":[\"13.063139108624346\",\"0.06278135179239813\",\"0.08729209740417539\"],\"Clostridiales\":[\"5.65792012348061\",\"4.955044777367546\",\"5.690149576974722\"],\"Paraprevotellaceae\":[\"4.651022573798958\",\"0.7241708123085702\",\"0.8064155859179338\"],\"Di\\u011fer\":[\"20.898852016206845\",\"71.20020519786927\",\"68.90353424913768\"]},\"Taksonomik Analiz (\\u015eube Seviyesi)\":{\"TAX\":[\"KULLANICI\",\"TOPLUM\",\"SAGLIKLI\"],\"Bacteroidetes\":[\"40.990980127339384\",\"37.866835633649075\",\"37.89050589756257\"],\"Firmicutes\":[\"40.33740111904302\",\"46.73062860310761\",\"47.58803695446815\"],\"Di\\u011fer\":[\"18.671618753617594\",\"15.402535763243321\",\"14.521457147969272\"]}},\"Yak\\u0131n Profiller\":{\"PROFIL\":[\"YUZDE\"],\"fazla kilolu\":[\"20\"],\"cilt rahats\\u0131zl\\u0131klar\\u0131na sahip\":[\"19\"],\"laktoz hassasiyeti problemi var\":[\"14\"],\"akci\\u011fer rahats\\u0131zl\\u0131\\u011f\\u0131na sahip\":[\"10\"],\"migren rahats\\u0131zl\\u0131\\u011f\\u0131 ya\\u015f\\u0131yor\":[\"10\"],\"vejetaryen, ancak deniz \\u00fcr\\u00fcnleri t\\u00fcketiyor\":[\"10\"],\"gluten hassasiyeti veya alerjisine sahip\":[\"8\"],\"zaman zaman ishal problemi ya\\u015f\\u0131yor\":[\"8\"],\"dikkat bozuklu\\u011fu rahats\\u0131zl\\u0131\\u011f\\u0131 ya\\u015f\\u0131yor\":[\"7\"],\"g\\u0131da alerjisi bulunuyor\":[\"6\"],\"k\\u0131rm\\u0131z\\u0131 et yemiyor\":[\"6\"],\"hayat\\u0131n\\u0131n bir d\\u00f6neminde kanser te\\u015fhisi alm\\u0131\\u015f\":[\"6\"],\"vejetaryen\":[\"5\"],\"huzursuz ba\\u011f\\u0131rsak sendromu ya\\u015f\\u0131yor\":[\"5\"],\"karaci\\u011fer rahats\\u0131zl\\u0131\\u011f\\u0131na sahip\":[\"5\"],\"tiroid rahats\\u0131zl\\u0131\\u011f\\u0131na sahip\":[\"4\"],\"kalp rahats\\u0131zl\\u0131\\u011f\\u0131 ya\\u015f\\u0131yor\":[\"3\"]},\"Ba\\u011f\\u0131rsak Skorlar\\u0131\":{\"V\\u00fccut K\\u00fctle Endeksi-Mikrobiyom \\u0130li\\u015fkiniz\":[\"19\",\"15\",\"72\",\"35\"],\"Karbonhidrat Metabolizman\\u0131z\":[\"46\",\"\",\"\",\"48\"],\"Protein Metabolizman\\u0131z\":[\"1\",\"\",\"\",\"29\"],\"Ya\\u011f Metabolizman\\u0131z\":[\"2\",\"\",\"\",\"29\"],\"Probiyotik Mikroorganizma \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"39\",\"17\",\"78\",\"43\"],\"Uyku Kaliteniz\":[\"77\",\"23\",\"84\",\"74\"],\"Ba\\u011f\\u0131rsak Hareketlilik \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"19\",\"29\",\"69\",\"35\"],\"Gluten Hassasiyeti \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"19\",\"16\",\"64\",\"24\"],\"Laktoz Hassasiyeti \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"12\",\"15\",\"82\",\"27\"],\"Vitamin Senteziniz\":[\"83\",\"56\",\"73\",\"67\"],\"Mikrobiyom Ya\\u015f\\u0131n\\u0131z\":[\"27\",\"\",\"\",\"\"],\"Yapay Tatland\\u0131r\\u0131c\\u0131 Hasar\\u0131n\\u0131z\":[\"58\",\"27\",\"58\",\"32\"],\"\\u015eeker T\\u00fcketim Skorunuz\":[\"49\",\"7\",\"81\",\"59\"],\"Antibiyotik Hasar \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"14\",\"27\",\"74\",\"34\"],\"Otoimm\\u00fcnite Endeksiniz\":[\"9\",\"23\",\"62\",\"29\"]}}', '{\"Ada\\u00e7ay\\u0131\":5,\"Ahududu\":6,\"Alabal\\u0131k\":7,\"Amaranth\":3,\"Ananas\":4,\"Antep F\\u0131st\\u0131\\u011f\\u0131\":3,\"Armut\":4,\"Armut Suyu\":6,\"Arpa\":8,\"Asma Yapra\\u011f\\u0131\":4,\"Aspir Ya\\u011f\\u0131\":5,\"Avokado\":5,\"Ay\\u00e7ekirde\\u011fi\":3,\"Ay\\u00e7i\\u00e7ek Ya\\u011f\\u0131\":5,\"\\u0130nek S\\u00fct\\u00fc (Az Ya\\u011fl\\u0131)\":5,\"A\\u00e7ai \\u00dcz\\u00fcm\\u00fc\":7,\"Badem \":5,\"Bakla\":8,\"Balkaba\\u011f\\u0131\":5,\"Bal\\u0131k Ya\\u011f\\u0131 Destekleri\":7,\"Barbunya\":7,\"Beyaz Dut\":5,\"Beyaz Ekmek\":5,\"Beyaz Peynir (Tam Ya\\u011fl\\u0131)\":7,\"Beyaz Un\":3,\"Bezelye\":5,\"Bira\":7,\"Brokoli\":4,\"Br\\u00fcksel Lahanas\\u0131\":5,\"Bulgur\":4,\"Bu\\u011fday\":4,\"Bu\\u011fday Ru\\u015feymi\":8,\"Bu\\u011fday Unu\":5,\"B\\u00f6r\\u00fclce\":5,\"B\\u00f6\\u011f\\u00fcrtlen\":4,\"Ceviz\":5,\"Cheddar\":6,\"Chia Tohumu\":6,\"Dana Eti\":4,\"Domates\":0,\"Dut Kurusu\":5,\"Ebeg\\u00fcmeci\":5,\"Elma\":3,\"Elma Suyu\":3,\"Enginar\":4,\"Erik\":4,\"Eski Ka\\u015far\":6,\"Esmer Pirin\\u00e7\":5,\"Ezine Peyniri\":6,\"Frambuaz Suyu\":4,\"F\\u0131nd\\u0131k\":3,\"F\\u0131nd\\u0131k Ya\\u011f\\u0131\":4,\"Grayver\":6,\"Greyfurt \":3,\"Greyfurt Suyu\":4,\"Hamsi\":6,\"Havu\\u00e7\":3,\"Havu\\u00e7 Suyu\":6,\"Hindi Eti\":3,\"Hindiba\":3,\"Hindistan Cevizi\":5,\"Taze Hurma\":4,\"Kabak \\u00c7ekirde\\u011fi\":5,\"Kahve\":4,\"Kaju\":5,\"Kakao\":4,\"Kale\":4,\"Kanola Ya\\u011f\\u0131\":4,\"Kapari\":2,\"Karabu\\u011fday \":3,\"Karadut\":5,\"Karalahana\":4,\"Karides\":8,\"Karnabahar\":6,\"Kavun\":4,\"Kaymak\":5,\"Taze Kay\\u0131s\\u0131\":3,\"Kay\\u0131s\\u0131 Suyu\":6,\"Ka\\u015far Peyniri\":5,\"Kefir \":5,\"Kepekli Ekmek\":5,\"Kepekli Makarna\":5,\"Kereviz\":6,\"Kestane\":4,\"Keten Tohumu\":10,\"Ke\\u00e7i Eti\":4,\"Ke\\u00e7i Peyniri\":6,\"Ke\\u00e7i S\\u00fct\\u00fc\":5,\"Kinoa\":3,\"Kivi \":6,\"Koyun Eti \":3,\"Koyun Peyniri\":6,\"Koyun S\\u00fct\\u00fc\":5,\"Krem Peynir\":6,\"Kril Ya\\u011f\\u0131 Destekleri\":8,\"Kuru Fasulye\":5,\"Kuru Hurma\":4,\"Kuru Kay\\u0131s\\u0131\":5,\"Kuru \\u00dcz\\u00fcm\":9,\"Kuyruk Ya\\u011f\\u0131\":4,\"Kuzu Eti \":7,\"Ku\\u015fkonmaz\":5,\"K\\u0131l\\u0131\\u00e7 Bal\\u0131\\u011f\\u0131\":5,\"K\\u0131rm\\u0131z\\u0131-Mor \\u00dcz\\u00fcm\":8,\"K\\u0131rm\\u0131z\\u0131 Biber\":3,\"Frenk \\u00dcz\\u00fcm\\u00fc\":8,\"K\\u0131rm\\u0131z\\u0131 Pancar\":4,\"K\\u0131rm\\u0131z\\u0131 \\u015earap\":4,\"K\\u0131z\\u0131lc\\u0131k\":5,\"Beyaz Lahana\":7,\"Lava\\u015f\":6,\"Levrek\":5,\"Limon\":3,\"Lor Peyniri\":7,\"L\\u00fcfer\":5,\"Maden Suyu\":5,\"Makarna\":5,\"Manda S\\u00fct\\u00fc\":4,\"Mandalina \":3,\"Mandalina Suyu\":3,\"Mango\":4,\"Mantar\":9,\"Margarin\":6,\"Maydanoz\":3,\"Mercimek\":6,\"Midye \":7,\"Mor Lahana\":4,\"Morina Bal\\u0131\\u011f\\u0131\":5,\"Muz\":5,\"M\\u0131s\\u0131r\":4,\"M\\u0131s\\u0131r Gevre\\u011fi\":5,\"M\\u0131s\\u0131r Ke\\u011fe\\u011fi\":7,\"M\\u0131s\\u0131r Ni\\u015fastas\\u0131\":7,\"M\\u0131s\\u0131r Unu\":4,\"M\\u0131s\\u0131r Ya\\u011f\\u0131\":5,\"M\\u0131s\\u0131r\\u00f6z\\u00fc Ya\\u011f\\u0131\":5,\"Taze Nane\":4,\"Nar Suyu\":5,\"Nar\":5,\"Nektarin\":3,\"Nohut\":6,\"Noodle\":5,\"Oolong \\u00c7ay\\u0131\":6,\"Otlu Peynir\":6,\"Palamut\":6,\"Papaya\":4,\"\\u0130nek S\\u00fct\\u00fc (Tam Ya\\u011fl\\u0131)\":4,\"Patates\":4,\"Patl\\u0131can\":4,\"Pembe \\u015earap\":3,\"Pirin\\u00e7\":5,\"Pirin\\u00e7 Unu\":3,\"Portakal\":4,\"Portakal Suyu\":4,\"Protein Barlar\":5,\"Ricotta\":6,\"Roka\":3,\"Rokfor\":6,\"Salep\":6,\"Sardalya\":6,\"Sar\\u0131msak\":5,\"Semizotu \":5,\"Siyah Havu\\u00e7\":4,\"Siyah \\u00c7ay\":4,\"Somon\":6,\"Soya Fasulyesi\":3,\"Soya Filizi\":2,\"Soya Unu\":2,\"Soya Ya\\u011f\\u0131\":5,\"So\\u011fan\":5,\"Tere\":4,\"Susam\":4,\"S\\u00fctl\\u00fc kremalar\":6,\"S\\u0131\\u011f\\u0131r Eti\":4,\"Tam Bu\\u011fday Unu\":5,\"Tam Tah\\u0131ll\\u0131 Ekmek\":8,\"Tam Tah\\u0131ll\\u0131 Gevrekler\":7,\"Tam Tah\\u0131l Unu\":6,\"Tatl\\u0131 Patates\":4,\"Tavuk Eti\":5,\"Tereya\\u011f\\u0131\":5,\"Tofu\":2,\"Ton Bal\\u0131\\u011f\\u0131\":5,\"Tulum Peyniri\":6,\"Tuna Bal\\u0131\\u011f\\u0131\":5,\"Turp\":6,\"Uskumru\":5,\"Vi\\u015fne \":5,\"Vi\\u015fne Suyu\":6,\"Whey Protein Destekleri\":5,\"Yaban Mersini\":6,\"Yo\\u011furt (Az Ya\\u011fl\\u0131)\":5,\"Yenge\\u00e7\":7,\"Yer Elmas\\u0131\":3,\"Yer F\\u0131st\\u0131\\u011f\\u0131\":5,\"Taze Fasulye\":5,\"Ye\\u015fil \\u00c7ay\":4,\"Yo\\u011furt (Tam Ya\\u011fl\\u0131)\":4,\"Yulaf\":8,\"Yulaf Kepe\\u011fi\":4,\"Yulafl\\u0131 Ekmek\":6,\"Yumurta\":4,\"Yumurta Beyaz\\u0131\":5,\"Yumurta Sar\\u0131s\\u0131\":7,\"Zeytin\":5,\"Zeytin Ezmesi\":4,\"Zeytinya\\u011f\\u0131\":4,\"\\u00c7am F\\u0131st\\u0131\\u011f\\u0131\":4,\"\\u00c7avdar\":6,\"\\u00c7avdar Ekme\\u011fi\":5,\"\\u00c7avdar Unu\":5,\"\\u00c7ilek\":1,\"\\u00c7ipura\":6,\"\\u00c7\\u00f6kelek\":5,\"\\u00dcz\\u00fcm Suyu \":6,\"\\u015ealgam\":4,\"\\u015eeftali\":4}', NULL, 32, NULL, 0, NULL, NULL, NULL, NULL, '2020-04-03 08:44:14', NULL, 4, 1, '2020-02-20 08:28:27', '2020-04-08 11:27:20', NULL),
(565, 'AAAAA', '{\"Mikrobiyom \\u00c7e\\u015fitlili\\u011finiz\":\"6\",\"\\u00d6nemli Organizmalar\\u0131n\\u0131z\":{\"ORGANIZMA\":[\"KULLANICI\",\"TOPLUM\"],\"Alistipes\":[\"0\",\"76\"],\"Anaerostipes\":[\"0\",\"74\"],\"Butyrivibrio\":[\"0\",\"90\"],\"Phascolarctobacterium\":[\"0\",\"70\"],\"Escherichia\":[\"0\",\"92\"],\"Bacteroides\":[\"3\",\"54\"],\"Blautia\":[\"7\",\"64\"],\"Lachnospiraceae\":[\"8\",\"56\"],\"Ruminococcus\":[\"9\",\"64\"],\"Parabacteroides\":[\"11\",\"65\"],\"Coprococcus\":[\"15\",\"62\"],\"Dorea\":[\"16\",\"70\"],\"Bifidobacterium\":[\"21\",\"77\"],\"Clostridium\":[\"22\",\"72\"],\"Roseburia\":[\"25\",\"70\"],\"Akkermansia\":[\"29\",\"80\"],\"Haemophilus\":[\"32\",\"83\"],\"Eubacterium\":[\"47\",\"79\"],\"Collinsella\":[\"51\",\"73\"],\"Anaerotruncus\":[\"55\",\"78\"],\"Ruminococcaceae\":[\"61\",\"53\"],\"Faecalibacterium\":[\"75\",\"61\"],\"Streptococcus\":[\"78\",\"81\"],\"Desulfovibrio\":[\"90\",\"74\"],\"Catenibacterium\":[\"91\",\"91\"],\"Prevotella\":[\"96\",\"81\"]},\"Taksonomik Analiz\":{\"Taksonomik Analiz (Cins Seviyesi)\":{\"TAX\":[\"KULLANICI\",\"TOPLUM\",\"SAGLIKLI\"],\"Prevotella\":[\"4.444819602546787\",\"0.1755526608462995\",\"0.18805477790579875\"],\"Succinivibrio\":[\"13.055903916650587\",\"0.06243439885384906\",\"0.0868518692885645\"],\"Ruminococcaceae\":[\"9.897742620104188\",\"8.59439235986533\",\"9.066843166957936\"],\"Faecalibacterium\":[\"9.35148562608528\",\"6.514846244369883\",\"6.706931790107228\"],\"Clostridiales\":[\"5.65792012348061\",\"4.955044777367546\",\"5.690149576974722\"],\"Di\\u011fer\":[\"22.847530387806287\",\"79.52217689785078\",\"78.07311404085995\"]},\"Taksonomik Analiz (Aile seviyesi)\":{\"TAX\":[\"KULLANICI\",\"TOPLUM\",\"SAGLIKLI\"],\"Prevotellaceae\":[\"34.74459772332626\",\"4.464190420269295\",\"5.339408930792739\"],\"Ruminococcaceae\":[\"20.984468454562993\",\"18.593607440392915\",\"19.173199559772755\"],\"Succinivibrionaceae\":[\"13.063139108624346\",\"0.06278135179239813\",\"0.08729209740417539\"],\"Clostridiales\":[\"5.65792012348061\",\"4.955044777367546\",\"5.690149576974722\"],\"Paraprevotellaceae\":[\"4.651022573798958\",\"0.7241708123085702\",\"0.8064155859179338\"],\"Di\\u011fer\":[\"20.898852016206845\",\"71.20020519786927\",\"68.90353424913768\"]},\"Taksonomik Analiz (\\u015eube Seviyesi)\":{\"TAX\":[\"KULLANICI\",\"TOPLUM\",\"SAGLIKLI\"],\"Bacteroidetes\":[\"40.990980127339384\",\"37.866835633649075\",\"37.89050589756257\"],\"Firmicutes\":[\"40.33740111904302\",\"46.73062860310761\",\"47.58803695446815\"],\"Di\\u011fer\":[\"18.671618753617594\",\"15.402535763243321\",\"14.521457147969272\"]}},\"Yak\\u0131n Profiller\":{\"PROFIL\":[\"YUZDE\"],\"fazla kilolu\":[\"20\"],\"cilt rahats\\u0131zl\\u0131klar\\u0131na sahip\":[\"19\"],\"laktoz hassasiyeti problemi var\":[\"14\"],\"akci\\u011fer rahats\\u0131zl\\u0131\\u011f\\u0131na sahip\":[\"10\"],\"migren rahats\\u0131zl\\u0131\\u011f\\u0131 ya\\u015f\\u0131yor\":[\"10\"],\"vejetaryen, ancak deniz \\u00fcr\\u00fcnleri t\\u00fcketiyor\":[\"10\"],\"gluten hassasiyeti veya alerjisine sahip\":[\"8\"],\"zaman zaman ishal problemi ya\\u015f\\u0131yor\":[\"8\"],\"dikkat bozuklu\\u011fu rahats\\u0131zl\\u0131\\u011f\\u0131 ya\\u015f\\u0131yor\":[\"7\"],\"g\\u0131da alerjisi bulunuyor\":[\"6\"],\"k\\u0131rm\\u0131z\\u0131 et yemiyor\":[\"6\"],\"hayat\\u0131n\\u0131n bir d\\u00f6neminde kanser te\\u015fhisi alm\\u0131\\u015f\":[\"6\"],\"vejetaryen\":[\"5\"],\"huzursuz ba\\u011f\\u0131rsak sendromu ya\\u015f\\u0131yor\":[\"5\"],\"karaci\\u011fer rahats\\u0131zl\\u0131\\u011f\\u0131na sahip\":[\"5\"],\"tiroid rahats\\u0131zl\\u0131\\u011f\\u0131na sahip\":[\"4\"],\"kalp rahats\\u0131zl\\u0131\\u011f\\u0131 ya\\u015f\\u0131yor\":[\"3\"]},\"Ba\\u011f\\u0131rsak Skorlar\\u0131\":{\"V\\u00fccut K\\u00fctle Endeksi-Mikrobiyom \\u0130li\\u015fkiniz\":[\"19\",\"15\",\"72\",\"35\"],\"Karbonhidrat Metabolizman\\u0131z\":[\"46\",\"\",\"\",\"48\"],\"Protein Metabolizman\\u0131z\":[\"1\",\"\",\"\",\"29\"],\"Ya\\u011f Metabolizman\\u0131z\":[\"2\",\"\",\"\",\"29\"],\"Probiyotik Mikroorganizma \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"39\",\"17\",\"78\",\"43\"],\"Uyku Kaliteniz\":[\"77\",\"23\",\"84\",\"74\"],\"Ba\\u011f\\u0131rsak Hareketlilik \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"19\",\"29\",\"69\",\"35\"],\"Gluten Hassasiyeti \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"19\",\"16\",\"64\",\"24\"],\"Laktoz Hassasiyeti \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"12\",\"15\",\"82\",\"27\"],\"Vitamin Senteziniz\":[\"83\",\"56\",\"73\",\"67\"],\"Mikrobiyom Ya\\u015f\\u0131n\\u0131z\":[\"27\",\"\",\"\",\"\"],\"Yapay Tatland\\u0131r\\u0131c\\u0131 Hasar\\u0131n\\u0131z\":[\"58\",\"27\",\"58\",\"32\"],\"\\u015eeker T\\u00fcketim Skorunuz\":[\"49\",\"7\",\"81\",\"59\"],\"Antibiyotik Hasar \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"14\",\"27\",\"74\",\"34\"],\"Otoimm\\u00fcnite Endeksiniz\":[\"9\",\"23\",\"62\",\"29\"]}}', '{\"Ada\\u00e7ay\\u0131\":5,\"Ahududu\":6,\"Alabal\\u0131k\":7,\"Amaranth\":3,\"Ananas\":4,\"Antep F\\u0131st\\u0131\\u011f\\u0131\":3,\"Armut\":4,\"Armut Suyu\":6,\"Arpa\":8,\"Asma Yapra\\u011f\\u0131\":4,\"Aspir Ya\\u011f\\u0131\":5,\"Avokado\":5,\"Ay\\u00e7ekirde\\u011fi\":3,\"Ay\\u00e7i\\u00e7ek Ya\\u011f\\u0131\":5,\"\\u0130nek S\\u00fct\\u00fc (Az Ya\\u011fl\\u0131)\":5,\"A\\u00e7ai \\u00dcz\\u00fcm\\u00fc\":7,\"Badem \":5,\"Bakla\":8,\"Balkaba\\u011f\\u0131\":5,\"Bal\\u0131k Ya\\u011f\\u0131 Destekleri\":7,\"Barbunya\":7,\"Beyaz Dut\":5,\"Beyaz Ekmek\":5,\"Beyaz Peynir (Tam Ya\\u011fl\\u0131)\":7,\"Beyaz Un\":3,\"Bezelye\":5,\"Bira\":7,\"Brokoli\":4,\"Br\\u00fcksel Lahanas\\u0131\":5,\"Bulgur\":4,\"Bu\\u011fday\":4,\"Bu\\u011fday Ru\\u015feymi\":8,\"Bu\\u011fday Unu\":5,\"B\\u00f6r\\u00fclce\":5,\"B\\u00f6\\u011f\\u00fcrtlen\":4,\"Ceviz\":5,\"Cheddar\":6,\"Chia Tohumu\":6,\"Dana Eti\":4,\"Domates\":0,\"Dut Kurusu\":5,\"Ebeg\\u00fcmeci\":5,\"Elma\":3,\"Elma Suyu\":3,\"Enginar\":4,\"Erik\":4,\"Eski Ka\\u015far\":6,\"Esmer Pirin\\u00e7\":5,\"Ezine Peyniri\":6,\"Frambuaz Suyu\":4,\"F\\u0131nd\\u0131k\":3,\"F\\u0131nd\\u0131k Ya\\u011f\\u0131\":4,\"Grayver\":6,\"Greyfurt \":3,\"Greyfurt Suyu\":4,\"Hamsi\":6,\"Havu\\u00e7\":3,\"Havu\\u00e7 Suyu\":6,\"Hindi Eti\":3,\"Hindiba\":3,\"Hindistan Cevizi\":5,\"Taze Hurma\":4,\"Kabak \\u00c7ekirde\\u011fi\":5,\"Kahve\":4,\"Kaju\":5,\"Kakao\":4,\"Kale\":4,\"Kanola Ya\\u011f\\u0131\":4,\"Kapari\":2,\"Karabu\\u011fday \":3,\"Karadut\":5,\"Karalahana\":4,\"Karides\":8,\"Karnabahar\":6,\"Kavun\":4,\"Kaymak\":5,\"Taze Kay\\u0131s\\u0131\":3,\"Kay\\u0131s\\u0131 Suyu\":6,\"Ka\\u015far Peyniri\":5,\"Kefir \":5,\"Kepekli Ekmek\":5,\"Kepekli Makarna\":5,\"Kereviz\":6,\"Kestane\":4,\"Keten Tohumu\":10,\"Ke\\u00e7i Eti\":4,\"Ke\\u00e7i Peyniri\":6,\"Ke\\u00e7i S\\u00fct\\u00fc\":5,\"Kinoa\":3,\"Kivi \":6,\"Koyun Eti \":3,\"Koyun Peyniri\":6,\"Koyun S\\u00fct\\u00fc\":5,\"Krem Peynir\":6,\"Kril Ya\\u011f\\u0131 Destekleri\":8,\"Kuru Fasulye\":5,\"Kuru Hurma\":4,\"Kuru Kay\\u0131s\\u0131\":5,\"Kuru \\u00dcz\\u00fcm\":9,\"Kuyruk Ya\\u011f\\u0131\":4,\"Kuzu Eti \":7,\"Ku\\u015fkonmaz\":5,\"K\\u0131l\\u0131\\u00e7 Bal\\u0131\\u011f\\u0131\":5,\"K\\u0131rm\\u0131z\\u0131-Mor \\u00dcz\\u00fcm\":8,\"K\\u0131rm\\u0131z\\u0131 Biber\":3,\"Frenk \\u00dcz\\u00fcm\\u00fc\":8,\"K\\u0131rm\\u0131z\\u0131 Pancar\":4,\"K\\u0131rm\\u0131z\\u0131 \\u015earap\":4,\"K\\u0131z\\u0131lc\\u0131k\":5,\"Beyaz Lahana\":7,\"Lava\\u015f\":6,\"Levrek\":5,\"Limon\":3,\"Lor Peyniri\":7,\"L\\u00fcfer\":5,\"Maden Suyu\":5,\"Makarna\":5,\"Manda S\\u00fct\\u00fc\":4,\"Mandalina \":3,\"Mandalina Suyu\":3,\"Mango\":4,\"Mantar\":9,\"Margarin\":6,\"Maydanoz\":3,\"Mercimek\":6,\"Midye \":7,\"Mor Lahana\":4,\"Morina Bal\\u0131\\u011f\\u0131\":5,\"Muz\":5,\"M\\u0131s\\u0131r\":4,\"M\\u0131s\\u0131r Gevre\\u011fi\":5,\"M\\u0131s\\u0131r Ke\\u011fe\\u011fi\":7,\"M\\u0131s\\u0131r Ni\\u015fastas\\u0131\":7,\"M\\u0131s\\u0131r Unu\":4,\"M\\u0131s\\u0131r Ya\\u011f\\u0131\":5,\"M\\u0131s\\u0131r\\u00f6z\\u00fc Ya\\u011f\\u0131\":5,\"Taze Nane\":4,\"Nar Suyu\":5,\"Nar\":5,\"Nektarin\":3,\"Nohut\":6,\"Noodle\":5,\"Oolong \\u00c7ay\\u0131\":6,\"Otlu Peynir\":6,\"Palamut\":6,\"Papaya\":4,\"\\u0130nek S\\u00fct\\u00fc (Tam Ya\\u011fl\\u0131)\":4,\"Patates\":4,\"Patl\\u0131can\":4,\"Pembe \\u015earap\":3,\"Pirin\\u00e7\":5,\"Pirin\\u00e7 Unu\":3,\"Portakal\":4,\"Portakal Suyu\":4,\"Protein Barlar\":5,\"Ricotta\":6,\"Roka\":3,\"Rokfor\":6,\"Salep\":6,\"Sardalya\":6,\"Sar\\u0131msak\":5,\"Semizotu \":5,\"Siyah Havu\\u00e7\":4,\"Siyah \\u00c7ay\":4,\"Somon\":6,\"Soya Fasulyesi\":3,\"Soya Filizi\":2,\"Soya Unu\":2,\"Soya Ya\\u011f\\u0131\":5,\"So\\u011fan\":5,\"Tere\":4,\"Susam\":4,\"S\\u00fctl\\u00fc kremalar\":6,\"S\\u0131\\u011f\\u0131r Eti\":4,\"Tam Bu\\u011fday Unu\":5,\"Tam Tah\\u0131ll\\u0131 Ekmek\":8,\"Tam Tah\\u0131ll\\u0131 Gevrekler\":7,\"Tam Tah\\u0131l Unu\":6,\"Tatl\\u0131 Patates\":4,\"Tavuk Eti\":5,\"Tereya\\u011f\\u0131\":5,\"Tofu\":2,\"Ton Bal\\u0131\\u011f\\u0131\":5,\"Tulum Peyniri\":6,\"Tuna Bal\\u0131\\u011f\\u0131\":5,\"Turp\":6,\"Uskumru\":5,\"Vi\\u015fne \":5,\"Vi\\u015fne Suyu\":6,\"Whey Protein Destekleri\":5,\"Yaban Mersini\":6,\"Yo\\u011furt (Az Ya\\u011fl\\u0131)\":5,\"Yenge\\u00e7\":7,\"Yer Elmas\\u0131\":3,\"Yer F\\u0131st\\u0131\\u011f\\u0131\":5,\"Taze Fasulye\":5,\"Ye\\u015fil \\u00c7ay\":4,\"Yo\\u011furt (Tam Ya\\u011fl\\u0131)\":4,\"Yulaf\":8,\"Yulaf Kepe\\u011fi\":4,\"Yulafl\\u0131 Ekmek\":6,\"Yumurta\":4,\"Yumurta Beyaz\\u0131\":5,\"Yumurta Sar\\u0131s\\u0131\":7,\"Zeytin\":5,\"Zeytin Ezmesi\":4,\"Zeytinya\\u011f\\u0131\":4,\"\\u00c7am F\\u0131st\\u0131\\u011f\\u0131\":4,\"\\u00c7avdar\":6,\"\\u00c7avdar Ekme\\u011fi\":5,\"\\u00c7avdar Unu\":5,\"\\u00c7ilek\":1,\"\\u00c7ipura\":6,\"\\u00c7\\u00f6kelek\":5,\"\\u00dcz\\u00fcm Suyu \":6,\"\\u015ealgam\":4,\"\\u015eeftali\":4}', NULL, 32, NULL, 0, NULL, '2020-02-20 09:17:51', '2020-02-20 09:20:04', '2020-02-20 09:22:26', '2020-04-03 08:39:35', NULL, 4, 1, '2020-02-20 08:28:27', '2020-04-03 08:39:35', NULL),
(566, NULL, NULL, NULL, NULL, 32, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-02-20 08:28:27', '2020-02-20 08:28:27', NULL),
(567, NULL, NULL, NULL, NULL, 32, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-02-20 08:28:27', '2020-02-20 08:28:27', NULL),
(568, NULL, NULL, NULL, NULL, 32, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-02-20 08:28:27', '2020-02-20 08:28:27', NULL),
(569, NULL, NULL, NULL, NULL, 32, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-02-20 08:28:27', '2020-02-20 08:28:27', NULL),
(570, NULL, NULL, NULL, NULL, 32, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-02-20 08:28:27', '2020-02-20 08:28:27', NULL),
(571, '1qw2', NULL, '{\"Ada\\u00e7ay\\u0131\":5,\"Ahududu\":6,\"Alabal\\u0131k\":7,\"Amaranth\":3,\"Ananas\":4,\"Antep F\\u0131st\\u0131\\u011f\\u0131\":3,\"Armut\":4,\"Armut Suyu\":6,\"Arpa\":8,\"Asma Yapra\\u011f\\u0131\":4,\"Aspir Ya\\u011f\\u0131\":5,\"Avokado\":5,\"Ay\\u00e7ekirde\\u011fi\":3,\"Ay\\u00e7i\\u00e7ek Ya\\u011f\\u0131\":5,\"\\u0130nek S\\u00fct\\u00fc (Az Ya\\u011fl\\u0131)\":5,\"A\\u00e7ai \\u00dcz\\u00fcm\\u00fc\":7,\"Badem \":5,\"Bakla\":8,\"Balkaba\\u011f\\u0131\":5,\"Bal\\u0131k Ya\\u011f\\u0131 Destekleri\":7,\"Barbunya\":7,\"Beyaz Dut\":5,\"Beyaz Ekmek\":5,\"Beyaz Peynir (Tam Ya\\u011fl\\u0131)\":7,\"Beyaz Un\":3,\"Bezelye\":5,\"Bira\":7,\"Brokoli\":4,\"Br\\u00fcksel Lahanas\\u0131\":5,\"Bulgur\":4,\"Bu\\u011fday\":4,\"Bu\\u011fday Ru\\u015feymi\":8,\"Bu\\u011fday Unu\":5,\"B\\u00f6r\\u00fclce\":5,\"B\\u00f6\\u011f\\u00fcrtlen\":4,\"Ceviz\":5,\"Cheddar\":6,\"Chia Tohumu\":6,\"Dana Eti\":4,\"Domates\":0,\"Dut Kurusu\":5,\"Ebeg\\u00fcmeci\":5,\"Elma\":3,\"Elma Suyu\":3,\"Enginar\":4,\"Erik\":4,\"Eski Ka\\u015far\":6,\"Esmer Pirin\\u00e7\":5,\"Ezine Peyniri\":6,\"Frambuaz Suyu\":4,\"F\\u0131nd\\u0131k\":3,\"F\\u0131nd\\u0131k Ya\\u011f\\u0131\":4,\"Grayver\":6,\"Greyfurt \":3,\"Greyfurt Suyu\":4,\"Hamsi\":6,\"Havu\\u00e7\":3,\"Havu\\u00e7 Suyu\":6,\"Hindi Eti\":3,\"Hindiba\":3,\"Hindistan Cevizi\":5,\"Taze Hurma\":4,\"Kabak \\u00c7ekirde\\u011fi\":5,\"Kahve\":4,\"Kaju\":5,\"Kakao\":4,\"Kale\":4,\"Kanola Ya\\u011f\\u0131\":4,\"Kapari\":2,\"Karabu\\u011fday \":3,\"Karadut\":5,\"Karalahana\":4,\"Karides\":8,\"Karnabahar\":6,\"Kavun\":4,\"Kaymak\":5,\"Taze Kay\\u0131s\\u0131\":3,\"Kay\\u0131s\\u0131 Suyu\":6,\"Ka\\u015far Peyniri\":5,\"Kefir \":5,\"Kepekli Ekmek\":5,\"Kepekli Makarna\":5,\"Kereviz\":6,\"Kestane\":4,\"Keten Tohumu\":10,\"Ke\\u00e7i Eti\":4,\"Ke\\u00e7i Peyniri\":6,\"Ke\\u00e7i S\\u00fct\\u00fc\":5,\"Kinoa\":3,\"Kivi \":6,\"Koyun Eti \":3,\"Koyun Peyniri\":6,\"Koyun S\\u00fct\\u00fc\":5,\"Krem Peynir\":6,\"Kril Ya\\u011f\\u0131 Destekleri\":8,\"Kuru Fasulye\":5,\"Kuru Hurma\":4,\"Kuru Kay\\u0131s\\u0131\":5,\"Kuru \\u00dcz\\u00fcm\":9,\"Kuyruk Ya\\u011f\\u0131\":4,\"Kuzu Eti \":7,\"Ku\\u015fkonmaz\":5,\"K\\u0131l\\u0131\\u00e7 Bal\\u0131\\u011f\\u0131\":5,\"K\\u0131rm\\u0131z\\u0131-Mor \\u00dcz\\u00fcm\":8,\"K\\u0131rm\\u0131z\\u0131 Biber\":3,\"Frenk \\u00dcz\\u00fcm\\u00fc\":8,\"K\\u0131rm\\u0131z\\u0131 Pancar\":4,\"K\\u0131rm\\u0131z\\u0131 \\u015earap\":4,\"K\\u0131z\\u0131lc\\u0131k\":5,\"Beyaz Lahana\":7,\"Lava\\u015f\":6,\"Levrek\":5,\"Limon\":3,\"Lor Peyniri\":7,\"L\\u00fcfer\":5,\"Maden Suyu\":5,\"Makarna\":5,\"Manda S\\u00fct\\u00fc\":4,\"Mandalina \":3,\"Mandalina Suyu\":3,\"Mango\":4,\"Mantar\":9,\"Margarin\":6,\"Maydanoz\":3,\"Mercimek\":6,\"Midye \":7,\"Mor Lahana\":4,\"Morina Bal\\u0131\\u011f\\u0131\":5,\"Muz\":5,\"M\\u0131s\\u0131r\":4,\"M\\u0131s\\u0131r Gevre\\u011fi\":5,\"M\\u0131s\\u0131r Ke\\u011fe\\u011fi\":7,\"M\\u0131s\\u0131r Ni\\u015fastas\\u0131\":7,\"M\\u0131s\\u0131r Unu\":4,\"M\\u0131s\\u0131r Ya\\u011f\\u0131\":5,\"M\\u0131s\\u0131r\\u00f6z\\u00fc Ya\\u011f\\u0131\":5,\"Taze Nane\":4,\"Nar Suyu\":5,\"Nar\":5,\"Nektarin\":3,\"Nohut\":6,\"Noodle\":5,\"Oolong \\u00c7ay\\u0131\":6,\"Otlu Peynir\":6,\"Palamut\":6,\"Papaya\":4,\"\\u0130nek S\\u00fct\\u00fc (Tam Ya\\u011fl\\u0131)\":4,\"Patates\":4,\"Patl\\u0131can\":4,\"Pembe \\u015earap\":3,\"Pirin\\u00e7\":5,\"Pirin\\u00e7 Unu\":3,\"Portakal\":4,\"Portakal Suyu\":4,\"Protein Barlar\":5,\"Ricotta\":6,\"Roka\":3,\"Rokfor\":6,\"Salep\":6,\"Sardalya\":6,\"Sar\\u0131msak\":5,\"Semizotu \":5,\"Siyah Havu\\u00e7\":4,\"Siyah \\u00c7ay\":4,\"Somon\":6,\"Soya Fasulyesi\":3,\"Soya Filizi\":2,\"Soya Unu\":2,\"Soya Ya\\u011f\\u0131\":5,\"So\\u011fan\":5,\"Tere\":4,\"Susam\":4,\"S\\u00fctl\\u00fc kremalar\":6,\"S\\u0131\\u011f\\u0131r Eti\":4,\"Tam Bu\\u011fday Unu\":5,\"Tam Tah\\u0131ll\\u0131 Ekmek\":8,\"Tam Tah\\u0131ll\\u0131 Gevrekler\":7,\"Tam Tah\\u0131l Unu\":6,\"Tatl\\u0131 Patates\":4,\"Tavuk Eti\":5,\"Tereya\\u011f\\u0131\":5,\"Tofu\":2,\"Ton Bal\\u0131\\u011f\\u0131\":5,\"Tulum Peyniri\":6,\"Tuna Bal\\u0131\\u011f\\u0131\":5,\"Turp\":6,\"Uskumru\":5,\"Vi\\u015fne \":5,\"Vi\\u015fne Suyu\":6,\"Whey Protein Destekleri\":5,\"Yaban Mersini\":6,\"Yo\\u011furt (Az Ya\\u011fl\\u0131)\":5,\"Yenge\\u00e7\":7,\"Yer Elmas\\u0131\":3,\"Yer F\\u0131st\\u0131\\u011f\\u0131\":5,\"Taze Fasulye\":5,\"Ye\\u015fil \\u00c7ay\":4,\"Yo\\u011furt (Tam Ya\\u011fl\\u0131)\":4,\"Yulaf\":8,\"Yulaf Kepe\\u011fi\":4,\"Yulafl\\u0131 Ekmek\":6,\"Yumurta\":4,\"Yumurta Beyaz\\u0131\":5,\"Yumurta Sar\\u0131s\\u0131\":7,\"Zeytin\":5,\"Zeytin Ezmesi\":4,\"Zeytinya\\u011f\\u0131\":4,\"\\u00c7am F\\u0131st\\u0131\\u011f\\u0131\":4,\"\\u00c7avdar\":6,\"\\u00c7avdar Ekme\\u011fi\":5,\"\\u00c7avdar Unu\":5,\"\\u00c7ilek\":1,\"\\u00c7ipura\":6,\"\\u00c7\\u00f6kelek\":5,\"\\u00dcz\\u00fcm Suyu \":6,\"\\u015ealgam\":4,\"\\u015eeftali\":4}', NULL, 33, NULL, 1, NULL, '2020-04-01 14:07:01', NULL, NULL, NULL, NULL, 1, 1, '2020-02-28 10:08:31', '2020-04-03 08:26:44', NULL),
(572, '123123123', NULL, NULL, NULL, 34, NULL, 0, NULL, '2020-04-08 10:54:06', '2020-04-08 10:54:47', '2020-04-08 10:54:54', NULL, NULL, 3, 1, '2020-02-28 13:40:06', '2020-04-08 16:31:44', NULL),
(574, 'NewOne', NULL, NULL, NULL, NULL, NULL, 1, '2020-03-05 07:28:48', NULL, NULL, NULL, NULL, NULL, 4, 2, '2020-03-04 10:19:50', '2020-03-05 07:28:48', NULL),
(575, 'laith', NULL, NULL, 7, NULL, 16, 1, NULL, NULL, NULL, NULL, NULL, '2020-03-10 12:36:49', 4, 2, '2020-03-05 04:28:23', '2020-03-10 12:36:49', NULL),
(576, 'ERU3', '{\"Mikrobiyom \\u00c7e\\u015fitlili\\u011finiz\":\"9\",\"\\u00d6nemli Organizmalar\\u0131n\\u0131z\":{\"ORGANIZMA\":[\"KULLANICI\",\"TOPLUM\"],\"Haemophilus\":[\"0\",\"83\"],\"Desulfovibrio\":[\"0\",\"74\"],\"Butyrivibrio\":[\"0\",\"90\"],\"Catenibacterium\":[\"0\",\"91\"],\"Escherichia\":[\"0\",\"92\"],\"Prevotella\":[\"15\",\"81\"],\"Phascolarctobacterium\":[\"22\",\"70\"],\"Streptococcus\":[\"44\",\"81\"],\"Roseburia\":[\"47\",\"70\"],\"Alistipes\":[\"49\",\"76\"],\"Anaerostipes\":[\"51\",\"74\"],\"Parabacteroides\":[\"57\",\"65\"],\"Anaerotruncus\":[\"58\",\"78\"],\"Bacteroides\":[\"59\",\"54\"],\"Blautia\":[\"65\",\"64\"],\"Ruminococcus\":[\"65\",\"64\"],\"Eubacterium\":[\"70\",\"79\"],\"Lachnospiraceae\":[\"72\",\"56\"],\"Ruminococcaceae\":[\"74\",\"53\"],\"Akkermansia\":[\"77\",\"80\"],\"Faecalibacterium\":[\"80\",\"61\"],\"Clostridium\":[\"85\",\"72\"],\"Coprococcus\":[\"89\",\"62\"],\"Bifidobacterium\":[\"92\",\"77\"],\"Dorea\":[\"96\",\"70\"],\"Collinsella\":[\"99\",\"73\"]},\"Taksonomik Analiz\":{\"Taksonomik Analiz (Cins Seviyesi)\":{\"TAX\":[\"KULLANICI\",\"TOPLUM\",\"SAGLIKLI\"],\"Bacteroides\":[\"28.888307113787278\",\"26.36581355734437\",\"25.35405933303747\"],\"Faecalibacterium\":[\"10.633196087297998\",\"6.514846244369883\",\"6.706931790107228\"],\"Ruminococcaceae\":[\"10.041768807863484\",\"8.59439235986533\",\"9.066843166957936\"],\"Lachnospiraceae\":[\"7.567770188257134\",\"7.11711465936931\",\"7.260400822181206\"],\"Coprococcus\":[\"4.4178546778684815\",\"2.13217518121622\",\"2.1864993968471707\"],\"Bifidobacterium\":[\"3.4920389366208915\",\"1.2213977847244806\",\"0.9226336179105874\"],\"Lachnospira\":[\"3.1386105623914133\",\"1.0500766460544306\",\"0.9840398332972775\"],\"Ruminococcus\":[\"2.999381202846467\",\"0.0005002447296400818\",\"0.00046093110629444214\"],\"Clostridiales\":[\"2.829211985624866\",\"4.955044777367546\",\"5.690149576974722\"],\"Di\\u011fer\":[\"25.991860437442\",\"42.04863854495879\",\"41.82798153158011\"]},\"Taksonomik Analiz (Aile seviyesi)\":{\"TAX\":[\"KULLANICI\",\"TOPLUM\",\"SAGLIKLI\"],\"Bacteroidaceae\":[\"28.888307113787278\",\"26.36711369744144\",\"25.354459755148163\"],\"Ruminococcaceae\":[\"25.54382749839351\",\"18.593607440392915\",\"19.173199559772755\"],\"Lachnospiraceae\":[\"20.32332151272104\",\"15.562550243855489\",\"15.435685293116789\"],\"Bifidobacteriaceae\":[\"3.493823928409929\",\"1.2328588424903582\",\"0.9366230521039208\"],\"Di\\u011fer\":[\"21.75071994668825\",\"38.243869775819796\",\"39.10003233985837\"]},\"Taksonomik Analiz (\\u015eube Seviyesi)\":{\"TAX\":[\"KULLANICI\",\"TOPLUM\",\"SAGLIKLI\"],\"Firmicutes\":[\"53.68541304709999\",\"46.73062860310761\",\"47.58803695446815\"],\"Bacteroidetes\":[\"35.407692124616226\",\"37.866835633649075\",\"37.89050589756257\"],\"Di\\u011fer\":[\"10.906894828283782\",\"15.402535763243321\",\"14.521457147969272\"]}},\"Yak\\u0131n Profiller\":{\"PROFIL\":[\"YUZDE\"],\"fazla kilolu\":[\"24\"],\"cilt rahats\\u0131zl\\u0131klar\\u0131na sahip\":[\"23\"],\"laktoz hassasiyeti problemi var\":[\"21\"],\"gluten hassasiyeti veya alerjisine sahip\":[\"16\"],\"migren rahats\\u0131zl\\u0131\\u011f\\u0131 ya\\u015f\\u0131yor\":[\"15\"],\"tiroid rahats\\u0131zl\\u0131\\u011f\\u0131na sahip\":[\"11\"],\"huzursuz ba\\u011f\\u0131rsak sendromu ya\\u015f\\u0131yor\":[\"10\"],\"g\\u0131da alerjisi bulunuyor\":[\"10\"],\"zaman zaman ishal problemi ya\\u015f\\u0131yor\":[\"9\"],\"dikkat bozuklu\\u011fu rahats\\u0131zl\\u0131\\u011f\\u0131 ya\\u015f\\u0131yor\":[\"9\"],\"akci\\u011fer rahats\\u0131zl\\u0131\\u011f\\u0131na sahip\":[\"8\"],\"k\\u0131rm\\u0131z\\u0131 et yemiyor\":[\"7\"],\"vejetaryen, ancak deniz \\u00fcr\\u00fcnleri t\\u00fcketiyor\":[\"7\"],\"kab\\u0131zl\\u0131k problemi ya\\u015f\\u0131yor\":[\"7\"],\"otoimm\\u00fcn bozuklu\\u011fa sahip\":[\"6\"],\"hayat\\u0131n\\u0131n bir d\\u00f6neminde kanser te\\u015fhisi alm\\u0131\\u015f\":[\"5\"],\"iltihapl\\u0131 ba\\u011f\\u0131rsak hastal\\u0131\\u011f\\u0131 te\\u015fhisi alm\\u0131\\u015f\":[\"5\"],\"mantar problemleri ya\\u015f\\u0131yor\":[\"4\"],\"vejetaryen\":[\"3\"]},\"Ba\\u011f\\u0131rsak Skorlar\\u0131\":{\"V\\u00fccut K\\u00fctle Endeksi-Mikrobiyom \\u0130li\\u015fkiniz\":[\"53\",\"15\",\"72\",\"35\"],\"Karbonhidrat Metabolizman\\u0131z\":[\"52\",\"\",\"\",\"48\"],\"Protein Metabolizman\\u0131z\":[\"34\",\"\",\"\",\"29\"],\"Ya\\u011f Metabolizman\\u0131z\":[\"34\",\"\",\"\",\"29\"],\"Probiyotik Mikroorganizma \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"12\",\"17\",\"78\",\"43\"],\"Uyku Kaliteniz\":[\"61\",\"23\",\"84\",\"74\"],\"Ba\\u011f\\u0131rsak Hareketlilik \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"29\",\"29\",\"69\",\"35\"],\"Gluten Hassasiyeti \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"6\",\"16\",\"64\",\"24\"],\"Laktoz Hassasiyeti \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"10\",\"15\",\"82\",\"27\"],\"Vitamin Senteziniz\":[\"82\",\"56\",\"73\",\"67\"],\"Mikrobiyom Ya\\u015f\\u0131n\\u0131z\":[\"34\",\"\",\"\",\"\"],\"Yapay Tatland\\u0131r\\u0131c\\u0131 Hasar\\u0131n\\u0131z\":[\"29\",\"27\",\"58\",\"32\"],\"\\u015eeker T\\u00fcketim Skorunuz\":[\"70\",\"7\",\"81\",\"59\"],\"Antibiyotik Hasar \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"36\",\"27\",\"74\",\"34\"],\"Otoimm\\u00fcnite Endeksiniz\":[\"7\",\"23\",\"62\",\"29\"]}}', NULL, NULL, NULL, NULL, 0, NULL, '2020-04-03 08:59:12', '2020-04-03 08:59:16', '2020-04-03 08:59:19', NULL, NULL, 3, 2, '2020-03-05 11:20:14', '2020-04-03 08:59:19', NULL);
INSERT INTO `kits` (`id`, `kit_code`, `microbiome`, `food`, `user_id`, `order_detail_id`, `survey_id`, `registered`, `registered_at`, `delevered_to_user_at`, `received_from_user_at`, `send_to_lab_at`, `uploaded_at`, `survey_filled_at`, `kit_status`, `sell_status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(577, 'ERU4', '{\"Mikrobiyom \\u00c7e\\u015fitlili\\u011finiz\":\"6\",\"\\u00d6nemli Organizmalar\\u0131n\\u0131z\":{\"ORGANIZMA\":[\"KULLANICI\",\"TOPLUM\"],\"Haemophilus\":[\"0\",\"83\"],\"Desulfovibrio\":[\"0\",\"74\"],\"Alistipes\":[\"0\",\"76\"],\"Butyrivibrio\":[\"0\",\"90\"],\"Catenibacterium\":[\"0\",\"91\"],\"Escherichia\":[\"0\",\"92\"],\"Prevotella\":[\"7\",\"81\"],\"Ruminococcus\":[\"14\",\"64\"],\"Akkermansia\":[\"15\",\"80\"],\"Coprococcus\":[\"21\",\"62\"],\"Anaerostipes\":[\"30\",\"74\"],\"Streptococcus\":[\"34\",\"81\"],\"Parabacteroides\":[\"44\",\"65\"],\"Phascolarctobacterium\":[\"49\",\"70\"],\"Ruminococcaceae\":[\"54\",\"53\"],\"Anaerotruncus\":[\"55\",\"78\"],\"Eubacterium\":[\"61\",\"79\"],\"Dorea\":[\"65\",\"70\"],\"Lachnospiraceae\":[\"70\",\"56\"],\"Roseburia\":[\"73\",\"70\"],\"Bacteroides\":[\"75\",\"54\"],\"Faecalibacterium\":[\"79\",\"61\"],\"Clostridium\":[\"88\",\"72\"],\"Bifidobacterium\":[\"92\",\"77\"],\"Blautia\":[\"92\",\"64\"],\"Collinsella\":[\"99\",\"73\"]},\"Taksonomik Analiz\":{\"Taksonomik Analiz (Cins Seviyesi)\":{\"TAX\":[\"KULLANICI\",\"TOPLUM\",\"SAGLIKLI\"],\"Bacteroides\":[\"38.96060100792476\",\"26.36581355734437\",\"25.35405933303747\"],\"Faecalibacterium\":[\"10.313430878468253\",\"6.514846244369883\",\"6.706931790107228\"],\"Lachnospiraceae\":[\"8.852127320756132\",\"7.11711465936931\",\"7.260400822181206\"],\"Ruminococcaceae\":[\"6.346366412499766\",\"8.59439235986533\",\"9.066843166957936\"],\"Blautia\":[\"6.006332315416753\",\"2.617733227390829\",\"2.5023072110449895\"],\"Bifidobacterium\":[\"3.344136987841205\",\"1.2213977847244806\",\"0.9226336179105874\"],\"Sutterella\":[\"2.9610131704666807\",\"0.6364564443847679\",\"0.675715900151596\"],\"Di\\u011fer\":[\"23.215991906626442\",\"46.93224572255102\",\"47.51110815860899\"]},\"Taksonomik Analiz (Aile seviyesi)\":{\"TAX\":[\"KULLANICI\",\"TOPLUM\",\"SAGLIKLI\"],\"Bacteroidaceae\":[\"38.96060100792476\",\"26.36711369744144\",\"25.354459755148163\"],\"Lachnospiraceae\":[\"19.44751484721884\",\"15.562550243855489\",\"15.435685293116789\"],\"Ruminococcaceae\":[\"18.921070873222547\",\"18.593607440392915\",\"19.173199559772755\"],\"Di\\u011fer\":[\"22.670813271633847\",\"39.476728618310155\",\"40.03665539196229\"]},\"Taksonomik Analiz (\\u015eube Seviyesi)\":{\"TAX\":[\"KULLANICI\",\"TOPLUM\",\"SAGLIKLI\"],\"Bacteroidetes\":[\"43.88969031605372\",\"37.866835633649075\",\"37.89050589756257\"],\"Firmicutes\":[\"43.55152968507035\",\"46.73062860310761\",\"47.58803695446815\"],\"Di\\u011fer\":[\"12.558779998875934\",\"15.402535763243321\",\"14.521457147969272\"]}},\"Yak\\u0131n Profiller\":{\"PROFIL\":[\"YUZDE\"],\"fazla kilolu\":[\"27\"],\"cilt rahats\\u0131zl\\u0131klar\\u0131na sahip\":[\"24\"],\"laktoz hassasiyeti problemi var\":[\"17\"],\"migren rahats\\u0131zl\\u0131\\u011f\\u0131 ya\\u015f\\u0131yor\":[\"12\"],\"g\\u0131da alerjisi bulunuyor\":[\"12\"],\"zaman zaman ishal problemi ya\\u015f\\u0131yor\":[\"8\"],\"gluten hassasiyeti veya alerjisine sahip\":[\"8\"],\"akci\\u011fer rahats\\u0131zl\\u0131\\u011f\\u0131na sahip\":[\"8\"],\"k\\u0131rm\\u0131z\\u0131 et yemiyor\":[\"6\"],\"vejetaryen\":[\"6\"],\"dikkat bozuklu\\u011fu rahats\\u0131zl\\u0131\\u011f\\u0131 ya\\u015f\\u0131yor\":[\"5\"],\"vejetaryen, ancak deniz \\u00fcr\\u00fcnleri t\\u00fcketiyor\":[\"5\"],\"hayat\\u0131n\\u0131n bir d\\u00f6neminde kanser te\\u015fhisi alm\\u0131\\u015f\":[\"5\"],\"kab\\u0131zl\\u0131k problemi ya\\u015f\\u0131yor\":[\"5\"],\"vegan besleniyor\":[\"5\"],\"huzursuz ba\\u011f\\u0131rsak sendromu ya\\u015f\\u0131yor\":[\"4\"],\"tiroid rahats\\u0131zl\\u0131\\u011f\\u0131na sahip\":[\"4\"],\"otoimm\\u00fcn bozuklu\\u011fa sahip\":[\"3\"],\"karaci\\u011fer rahats\\u0131zl\\u0131\\u011f\\u0131na sahip\":[\"3\"],\"b\\u00f6brek rahats\\u0131zl\\u0131\\u011f\\u0131 ya\\u015f\\u0131yor\":[\"3\"]},\"Ba\\u011f\\u0131rsak Skorlar\\u0131\":{\"V\\u00fccut K\\u00fctle Endeksi-Mikrobiyom \\u0130li\\u015fkiniz\":[\"68\",\"15\",\"72\",\"35\"],\"Karbonhidrat Metabolizman\\u0131z\":[\"55\",\"\",\"\",\"48\"],\"Protein Metabolizman\\u0131z\":[\"44\",\"\",\"\",\"29\"],\"Ya\\u011f Metabolizman\\u0131z\":[\"43\",\"\",\"\",\"29\"],\"Probiyotik Mikroorganizma \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"21\",\"17\",\"78\",\"43\"],\"Uyku Kaliteniz\":[\"82\",\"23\",\"84\",\"74\"],\"Ba\\u011f\\u0131rsak Hareketlilik \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"44\",\"29\",\"69\",\"35\"],\"Gluten Hassasiyeti \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"31\",\"16\",\"64\",\"24\"],\"Laktoz Hassasiyeti \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"29\",\"15\",\"82\",\"27\"],\"Vitamin Senteziniz\":[\"77\",\"56\",\"73\",\"67\"],\"Mikrobiyom Ya\\u015f\\u0131n\\u0131z\":[\"34\",\"\",\"\",\"\"],\"Yapay Tatland\\u0131r\\u0131c\\u0131 Hasar\\u0131n\\u0131z\":[\"55\",\"27\",\"58\",\"32\"],\"\\u015eeker T\\u00fcketim Skorunuz\":[\"65\",\"7\",\"81\",\"59\"],\"Antibiyotik Hasar \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"33\",\"27\",\"74\",\"34\"],\"Otoimm\\u00fcnite Endeksiniz\":[\"70\",\"23\",\"62\",\"29\"]}}', '{\"Ada\\u00e7ay\\u0131\":7,\"Ahududu\":9,\"Alabal\\u0131k\":6,\"Amaranth\":7,\"Ananas\":9,\"Antep F\\u0131st\\u0131\\u011f\\u0131\":5,\"Armut\":9,\"Armut Suyu\":9,\"Arpa\":4,\"Asma Yapra\\u011f\\u0131\":8,\"Aspir Ya\\u011f\\u0131\":8,\"Avokado\":7,\"Ay\\u00e7ekirde\\u011fi\":7,\"Ay\\u00e7i\\u00e7ek Ya\\u011f\\u0131\":8,\"\\u0130nek S\\u00fct\\u00fc (Az Ya\\u011fl\\u0131)\":5,\"A\\u00e7ai \\u00dcz\\u00fcm\\u00fc\":9,\"Badem \":4,\"Bakla\":8,\"Balkaba\\u011f\\u0131\":7,\"Bal\\u0131k Ya\\u011f\\u0131 Destekleri\":8,\"Barbunya\":8,\"Beyaz Dut\":10,\"Beyaz Ekmek\":9,\"Beyaz Peynir (Tam Ya\\u011fl\\u0131)\":7,\"Beyaz Un\":9,\"Bezelye\":6,\"Bira\":8,\"Brokoli\":8,\"Br\\u00fcksel Lahanas\\u0131\":9,\"Bulgur\":7,\"Bu\\u011fday\":6,\"Bu\\u011fday Ru\\u015feymi\":6,\"Bu\\u011fday Unu\":8,\"B\\u00f6r\\u00fclce\":9,\"B\\u00f6\\u011f\\u00fcrtlen\":9,\"Ceviz\":5,\"Cheddar\":7,\"Chia Tohumu\":8,\"Dana Eti\":7,\"Domates\":6,\"Dut Kurusu\":10,\"Ebeg\\u00fcmeci\":9,\"Elma\":8,\"Elma Suyu\":8,\"Enginar\":6,\"Erik\":9,\"Eski Ka\\u015far\":7,\"Esmer Pirin\\u00e7\":7,\"Ezine Peyniri\":7,\"Frambuaz Suyu\":10,\"F\\u0131nd\\u0131k\":6,\"F\\u0131nd\\u0131k Ya\\u011f\\u0131\":8,\"Grayver\":8,\"Greyfurt \":6,\"Greyfurt Suyu\":7,\"Hamsi\":7,\"Havu\\u00e7\":8,\"Havu\\u00e7 Suyu\":9,\"Hindi Eti\":8,\"Hindiba\":6,\"Hindistan Cevizi\":7,\"Taze Hurma\":9,\"Kabak \\u00c7ekirde\\u011fi\":9,\"Kahve\":8,\"Kaju\":7,\"Kakao\":8,\"Kale\":9,\"Kanola Ya\\u011f\\u0131\":8,\"Kapari\":8,\"Karabu\\u011fday \":6,\"Karadut\":10,\"Karalahana\":8,\"Karides\":7,\"Karnabahar\":9,\"Kavun\":8,\"Kaymak\":8,\"Taze Kay\\u0131s\\u0131\":8,\"Kay\\u0131s\\u0131 Suyu\":9,\"Ka\\u015far Peyniri\":8,\"Kefir \":7,\"Kepekli Ekmek\":6,\"Kepekli Makarna\":7,\"Kereviz\":8,\"Kestane\":8,\"Keten Tohumu\":8,\"Ke\\u00e7i Eti\":9,\"Ke\\u00e7i Peyniri\":8,\"Ke\\u00e7i S\\u00fct\\u00fc\":5,\"Kinoa\":5,\"Kivi \":9,\"Koyun Eti \":10,\"Koyun Peyniri\":7,\"Koyun S\\u00fct\\u00fc\":5,\"Krem Peynir\":8,\"Kril Ya\\u011f\\u0131 Destekleri\":8,\"Kuru Fasulye\":5,\"Kuru Hurma\":8,\"Kuru Kay\\u0131s\\u0131\":7,\"Kuru \\u00dcz\\u00fcm\":10,\"Kuyruk Ya\\u011f\\u0131\":8,\"Kuzu Eti \":10,\"Ku\\u015fkonmaz\":7,\"K\\u0131l\\u0131\\u00e7 Bal\\u0131\\u011f\\u0131\":7,\"K\\u0131rm\\u0131z\\u0131-Mor \\u00dcz\\u00fcm\":9,\"K\\u0131rm\\u0131z\\u0131 Biber\":9,\"Frenk \\u00dcz\\u00fcm\\u00fc\":9,\"K\\u0131rm\\u0131z\\u0131 Pancar\":9,\"K\\u0131rm\\u0131z\\u0131 \\u015earap\":5,\"K\\u0131z\\u0131lc\\u0131k\":9,\"Beyaz Lahana\":9,\"Lava\\u015f\":8,\"Levrek\":7,\"Limon\":7,\"Lor Peyniri\":7,\"L\\u00fcfer\":7,\"Maden Suyu\":8,\"Makarna\":8,\"Manda S\\u00fct\\u00fc\":5,\"Mandalina \":7,\"Mandalina Suyu\":9,\"Mango\":8,\"Mantar\":8,\"Margarin\":9,\"Maydanoz\":9,\"Mercimek\":6,\"Midye \":7,\"Mor Lahana\":9,\"Morina Bal\\u0131\\u011f\\u0131\":7,\"Muz\":6,\"M\\u0131s\\u0131r\":7,\"M\\u0131s\\u0131r Gevre\\u011fi\":8,\"M\\u0131s\\u0131r Ke\\u011fe\\u011fi\":8,\"M\\u0131s\\u0131r Ni\\u015fastas\\u0131\":9,\"M\\u0131s\\u0131r Unu\":8,\"M\\u0131s\\u0131r Ya\\u011f\\u0131\":8,\"M\\u0131s\\u0131r\\u00f6z\\u00fc Ya\\u011f\\u0131\":8,\"Taze Nane\":8,\"Nar Suyu\":8,\"Nar\":8,\"Nektarin\":9,\"Nohut\":7,\"Noodle\":8,\"Oolong \\u00c7ay\\u0131\":8,\"Otlu Peynir\":8,\"Palamut\":7,\"Papaya\":8,\"\\u0130nek S\\u00fct\\u00fc (Tam Ya\\u011fl\\u0131)\":5,\"Patates\":9,\"Patl\\u0131can\":9,\"Pembe \\u015earap\":8,\"Pirin\\u00e7\":8,\"Pirin\\u00e7 Unu\":7,\"Portakal\":6,\"Portakal Suyu\":7,\"Protein Barlar\":7,\"Ricotta\":7,\"Roka\":9,\"Rokfor\":8,\"Salep\":9,\"Sardalya\":7,\"Sar\\u0131msak\":6,\"Semizotu \":9,\"Siyah Havu\\u00e7\":9,\"Siyah \\u00c7ay\":8,\"Somon\":6,\"Soya Fasulyesi\":7,\"Soya Filizi\":7,\"Soya Unu\":6,\"Soya Ya\\u011f\\u0131\":8,\"So\\u011fan\":5,\"Tere\":9,\"Susam\":7,\"S\\u00fctl\\u00fc kremalar\":8,\"S\\u0131\\u011f\\u0131r Eti\":9,\"Tam Bu\\u011fday Unu\":6,\"Tam Tah\\u0131ll\\u0131 Ekmek\":8,\"Tam Tah\\u0131ll\\u0131 Gevrekler\":8,\"Tam Tah\\u0131l Unu\":7,\"Tatl\\u0131 Patates\":8,\"Tavuk Eti\":8,\"Tereya\\u011f\\u0131\":8,\"Tofu\":6,\"Ton Bal\\u0131\\u011f\\u0131\":7,\"Tulum Peyniri\":8,\"Tuna Bal\\u0131\\u011f\\u0131\":7,\"Turp\":9,\"Uskumru\":7,\"Vi\\u015fne \":9,\"Vi\\u015fne Suyu\":9,\"Whey Protein Destekleri\":6,\"Yaban Mersini\":8,\"Yo\\u011furt (Az Ya\\u011fl\\u0131)\":8,\"Yenge\\u00e7\":8,\"Yer Elmas\\u0131\":7,\"Yer F\\u0131st\\u0131\\u011f\\u0131\":8,\"Taze Fasulye\":9,\"Ye\\u015fil \\u00c7ay\":8,\"Yo\\u011furt (Tam Ya\\u011fl\\u0131)\":7,\"Yulaf\":6,\"Yulaf Kepe\\u011fi\":8,\"Yulafl\\u0131 Ekmek\":8,\"Yumurta\":7,\"Yumurta Beyaz\\u0131\":8,\"Yumurta Sar\\u0131s\\u0131\":7,\"Zeytin\":9,\"Zeytin Ezmesi\":8,\"Zeytinya\\u011f\\u0131\":7,\"\\u00c7am F\\u0131st\\u0131\\u011f\\u0131\":8,\"\\u00c7avdar\":4,\"\\u00c7avdar Ekme\\u011fi\":7,\"\\u00c7avdar Unu\":7,\"\\u00c7ilek\":9,\"\\u00c7ipura\":7,\"\\u00c7\\u00f6kelek\":8,\"\\u00dcz\\u00fcm Suyu \":9,\"\\u015ealgam\":9,\"\\u015eeftali\":9}', 7, NULL, 16, 1, '2020-03-09 14:23:07', NULL, NULL, NULL, NULL, '2020-03-10 06:15:16', 0, 2, '2020-03-05 12:03:43', '2020-03-10 06:15:16', NULL),
(578, 'AB1227', '{\"Mikrobiyom \\u00c7e\\u015fitlili\\u011finiz\":\"6\",\"\\u00d6nemli Organizmalar\\u0131n\\u0131z\":{\"ORGANIZMA\":[\"KULLANICI\",\"TOPLUM\"],\"Alistipes\":[\"0\",\"76\"],\"Anaerostipes\":[\"0\",\"74\"],\"Butyrivibrio\":[\"0\",\"90\"],\"Phascolarctobacterium\":[\"0\",\"70\"],\"Escherichia\":[\"0\",\"92\"],\"Bacteroides\":[\"3\",\"54\"],\"Blautia\":[\"7\",\"64\"],\"Lachnospiraceae\":[\"8\",\"56\"],\"Ruminococcus\":[\"9\",\"64\"],\"Parabacteroides\":[\"11\",\"65\"],\"Coprococcus\":[\"15\",\"62\"],\"Dorea\":[\"16\",\"70\"],\"Bifidobacterium\":[\"21\",\"77\"],\"Clostridium\":[\"22\",\"72\"],\"Roseburia\":[\"25\",\"70\"],\"Akkermansia\":[\"29\",\"80\"],\"Haemophilus\":[\"32\",\"83\"],\"Eubacterium\":[\"47\",\"79\"],\"Collinsella\":[\"51\",\"73\"],\"Anaerotruncus\":[\"55\",\"78\"],\"Ruminococcaceae\":[\"61\",\"53\"],\"Faecalibacterium\":[\"75\",\"61\"],\"Streptococcus\":[\"78\",\"81\"],\"Desulfovibrio\":[\"90\",\"74\"],\"Catenibacterium\":[\"91\",\"91\"],\"Prevotella\":[\"96\",\"81\"]},\"Taksonomik Analiz\":{\"Taksonomik Analiz (Cins Seviyesi)\":{\"TAX\":[\"KULLANICI\",\"TOPLUM\",\"SAGLIKLI\"],\"Prevotella\":[\"4.444819602546787\",\"0.1755526608462995\",\"0.18805477790579875\"],\"Succinivibrio\":[\"13.055903916650587\",\"0.06243439885384906\",\"0.0868518692885645\"],\"Ruminococcaceae\":[\"9.897742620104188\",\"8.59439235986533\",\"9.066843166957936\"],\"Faecalibacterium\":[\"9.35148562608528\",\"6.514846244369883\",\"6.706931790107228\"],\"Clostridiales\":[\"5.65792012348061\",\"4.955044777367546\",\"5.690149576974722\"],\"Di\\u011fer\":[\"22.847530387806287\",\"79.52217689785078\",\"78.07311404085995\"]},\"Taksonomik Analiz (Aile seviyesi)\":{\"TAX\":[\"KULLANICI\",\"TOPLUM\",\"SAGLIKLI\"],\"Prevotellaceae\":[\"34.74459772332626\",\"4.464190420269295\",\"5.339408930792739\"],\"Ruminococcaceae\":[\"20.984468454562993\",\"18.593607440392915\",\"19.173199559772755\"],\"Succinivibrionaceae\":[\"13.063139108624346\",\"0.06278135179239813\",\"0.08729209740417539\"],\"Clostridiales\":[\"5.65792012348061\",\"4.955044777367546\",\"5.690149576974722\"],\"Paraprevotellaceae\":[\"4.651022573798958\",\"0.7241708123085702\",\"0.8064155859179338\"],\"Di\\u011fer\":[\"20.898852016206845\",\"71.20020519786927\",\"68.90353424913768\"]},\"Taksonomik Analiz (\\u015eube Seviyesi)\":{\"TAX\":[\"KULLANICI\",\"TOPLUM\",\"SAGLIKLI\"],\"Bacteroidetes\":[\"40.990980127339384\",\"37.866835633649075\",\"37.89050589756257\"],\"Firmicutes\":[\"40.33740111904302\",\"46.73062860310761\",\"47.58803695446815\"],\"Di\\u011fer\":[\"18.671618753617594\",\"15.402535763243321\",\"14.521457147969272\"]}},\"Yak\\u0131n Profiller\":{\"PROFIL\":[\"YUZDE\"],\"fazla kilolu\":[\"20\"],\"cilt rahats\\u0131zl\\u0131klar\\u0131na sahip\":[\"19\"],\"laktoz hassasiyeti problemi var\":[\"14\"],\"akci\\u011fer rahats\\u0131zl\\u0131\\u011f\\u0131na sahip\":[\"10\"],\"migren rahats\\u0131zl\\u0131\\u011f\\u0131 ya\\u015f\\u0131yor\":[\"10\"],\"vejetaryen, ancak deniz \\u00fcr\\u00fcnleri t\\u00fcketiyor\":[\"10\"],\"gluten hassasiyeti veya alerjisine sahip\":[\"8\"],\"zaman zaman ishal problemi ya\\u015f\\u0131yor\":[\"8\"],\"dikkat bozuklu\\u011fu rahats\\u0131zl\\u0131\\u011f\\u0131 ya\\u015f\\u0131yor\":[\"7\"],\"g\\u0131da alerjisi bulunuyor\":[\"6\"],\"k\\u0131rm\\u0131z\\u0131 et yemiyor\":[\"6\"],\"hayat\\u0131n\\u0131n bir d\\u00f6neminde kanser te\\u015fhisi alm\\u0131\\u015f\":[\"6\"],\"vejetaryen\":[\"5\"],\"huzursuz ba\\u011f\\u0131rsak sendromu ya\\u015f\\u0131yor\":[\"5\"],\"karaci\\u011fer rahats\\u0131zl\\u0131\\u011f\\u0131na sahip\":[\"5\"],\"tiroid rahats\\u0131zl\\u0131\\u011f\\u0131na sahip\":[\"4\"],\"kalp rahats\\u0131zl\\u0131\\u011f\\u0131 ya\\u015f\\u0131yor\":[\"3\"]},\"Ba\\u011f\\u0131rsak Skorlar\\u0131\":{\"V\\u00fccut K\\u00fctle Endeksi-Mikrobiyom \\u0130li\\u015fkiniz\":[\"19\",\"15\",\"72\",\"35\"],\"Karbonhidrat Metabolizman\\u0131z\":[\"46\",\"\",\"\",\"48\"],\"Protein Metabolizman\\u0131z\":[\"1\",\"\",\"\",\"29\"],\"Ya\\u011f Metabolizman\\u0131z\":[\"2\",\"\",\"\",\"29\"],\"Probiyotik Mikroorganizma \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"39\",\"17\",\"78\",\"43\"],\"Uyku Kaliteniz\":[\"77\",\"23\",\"84\",\"74\"],\"Ba\\u011f\\u0131rsak Hareketlilik \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"19\",\"29\",\"69\",\"35\"],\"Gluten Hassasiyeti \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"19\",\"16\",\"64\",\"24\"],\"Laktoz Hassasiyeti \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"12\",\"15\",\"82\",\"27\"],\"Vitamin Senteziniz\":[\"83\",\"56\",\"73\",\"67\"],\"Mikrobiyom Ya\\u015f\\u0131n\\u0131z\":[\"27\",\"\",\"\",\"\"],\"Yapay Tatland\\u0131r\\u0131c\\u0131 Hasar\\u0131n\\u0131z\":[\"58\",\"27\",\"58\",\"32\"],\"\\u015eeker T\\u00fcketim Skorunuz\":[\"49\",\"7\",\"81\",\"59\"],\"Antibiyotik Hasar \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"14\",\"27\",\"74\",\"34\"],\"Otoimm\\u00fcnite Endeksiniz\":[\"9\",\"23\",\"62\",\"29\"]}}', '{\"Ada\\u00e7ay\\u0131\":4,\"Ahududu\":7,\"Alabal\\u0131k\":4,\"Amaranth\":4,\"Ananas\":7,\"Antep F\\u0131st\\u0131\\u011f\\u0131\":5,\"Armut\":7,\"Armut Suyu\":7,\"Arpa\":5,\"Asma Yapra\\u011f\\u0131\":6,\"Aspir Ya\\u011f\\u0131\":6,\"Avokado\":5,\"Ay\\u00e7ekirde\\u011fi\":4,\"Ay\\u00e7i\\u00e7ek Ya\\u011f\\u0131\":4,\"\\u0130nek S\\u00fct\\u00fc (Az Ya\\u011fl\\u0131)\":3,\"A\\u00e7ai \\u00dcz\\u00fcm\\u00fc\":7,\"Badem \":0,\"Bakla\":6,\"Balkaba\\u011f\\u0131\":4,\"Bal\\u0131k Ya\\u011f\\u0131 Destekleri\":3,\"Barbunya\":4,\"Beyaz Dut\":8,\"Beyaz Ekmek\":5,\"Beyaz Peynir (Tam Ya\\u011fl\\u0131)\":2,\"Beyaz Un\":6,\"Bezelye\":1,\"Bira\":5,\"Brokoli\":4,\"Br\\u00fcksel Lahanas\\u0131\":6,\"Bulgur\":4,\"Bu\\u011fday\":4,\"Bu\\u011fday Ru\\u015feymi\":3,\"Bu\\u011fday Unu\":5,\"B\\u00f6r\\u00fclce\":5,\"B\\u00f6\\u011f\\u00fcrtlen\":7,\"Ceviz\":1,\"Cheddar\":4,\"Chia Tohumu\":4,\"Dana Eti\":4,\"Domates\":6,\"Dut Kurusu\":8,\"Ebeg\\u00fcmeci\":6,\"Elma\":7,\"Elma Suyu\":6,\"Enginar\":5,\"Erik\":7,\"Eski Ka\\u015far\":5,\"Esmer Pirin\\u00e7\":6,\"Ezine Peyniri\":5,\"Frambuaz Suyu\":8,\"F\\u0131nd\\u0131k\":3,\"F\\u0131nd\\u0131k Ya\\u011f\\u0131\":6,\"Grayver\":5,\"Greyfurt \":5,\"Greyfurt Suyu\":6,\"Hamsi\":2,\"Havu\\u00e7\":7,\"Havu\\u00e7 Suyu\":7,\"Hindi Eti\":4,\"Hindiba\":5,\"Hindistan Cevizi\":5,\"Taze Hurma\":7,\"Kabak \\u00c7ekirde\\u011fi\":6,\"Kahve\":6,\"Kaju\":6,\"Kakao\":5,\"Kale\":7,\"Kanola Ya\\u011f\\u0131\":4,\"Kapari\":6,\"Karabu\\u011fday \":1,\"Karadut\":8,\"Karalahana\":6,\"Karides\":3,\"Karnabahar\":6,\"Kavun\":6,\"Kaymak\":6,\"Taze Kay\\u0131s\\u0131\":5,\"Kay\\u0131s\\u0131 Suyu\":7,\"Ka\\u015far Peyniri\":6,\"Kefir \":4,\"Kepekli Ekmek\":6,\"Kepekli Makarna\":5,\"Kereviz\":6,\"Kestane\":6,\"Keten Tohumu\":3,\"Ke\\u00e7i Eti\":5,\"Ke\\u00e7i Peyniri\":5,\"Ke\\u00e7i S\\u00fct\\u00fc\":4,\"Kinoa\":2,\"Kivi \":7,\"Koyun Eti \":8,\"Koyun Peyniri\":6,\"Koyun S\\u00fct\\u00fc\":3,\"Krem Peynir\":5,\"Kril Ya\\u011f\\u0131 Destekleri\":0,\"Kuru Fasulye\":2,\"Kuru Hurma\":6,\"Kuru Kay\\u0131s\\u0131\":5,\"Kuru \\u00dcz\\u00fcm\":7,\"Kuyruk Ya\\u011f\\u0131\":6,\"Kuzu Eti \":6,\"Ku\\u015fkonmaz\":5,\"K\\u0131l\\u0131\\u00e7 Bal\\u0131\\u011f\\u0131\":6,\"K\\u0131rm\\u0131z\\u0131-Mor \\u00dcz\\u00fcm\":7,\"K\\u0131rm\\u0131z\\u0131 Biber\":8,\"Frenk \\u00dcz\\u00fcm\\u00fc\":7,\"K\\u0131rm\\u0131z\\u0131 Pancar\":7,\"K\\u0131rm\\u0131z\\u0131 \\u015earap\":4,\"K\\u0131z\\u0131lc\\u0131k\":6,\"Beyaz Lahana\":5,\"Lava\\u015f\":5,\"Levrek\":4,\"Limon\":5,\"Lor Peyniri\":6,\"L\\u00fcfer\":6,\"Maden Suyu\":5,\"Makarna\":3,\"Manda S\\u00fct\\u00fc\":6,\"Mandalina \":5,\"Mandalina Suyu\":8,\"Mango\":6,\"Mantar\":5,\"Margarin\":7,\"Maydanoz\":7,\"Mercimek\":2,\"Midye \":3,\"Mor Lahana\":6,\"Morina Bal\\u0131\\u011f\\u0131\":6,\"Muz\":5,\"M\\u0131s\\u0131r\":4,\"M\\u0131s\\u0131r Gevre\\u011fi\":5,\"M\\u0131s\\u0131r Ke\\u011fe\\u011fi\":6,\"M\\u0131s\\u0131r Ni\\u015fastas\\u0131\":6,\"M\\u0131s\\u0131r Unu\":4,\"M\\u0131s\\u0131r Ya\\u011f\\u0131\":6,\"M\\u0131s\\u0131r\\u00f6z\\u00fc Ya\\u011f\\u0131\":5,\"Taze Nane\":7,\"Nar Suyu\":6,\"Nar\":10,\"Nektarin\":5,\"Nohut\":3,\"Noodle\":5,\"Oolong \\u00c7ay\\u0131\":5,\"Otlu Peynir\":5,\"Palamut\":3,\"Papaya\":6,\"\\u0130nek S\\u00fct\\u00fc (Tam Ya\\u011fl\\u0131)\":3,\"Patates\":9,\"Patl\\u0131can\":7,\"Pembe \\u015earap\":4,\"Pirin\\u00e7\":5,\"Pirin\\u00e7 Unu\":4,\"Portakal\":4,\"Portakal Suyu\":6,\"Protein Barlar\":4,\"Ricotta\":5,\"Roka\":6,\"Rokfor\":5,\"Salep\":5,\"Sardalya\":3,\"Sar\\u0131msak\":5,\"Semizotu \":4,\"Siyah Havu\\u00e7\":6,\"Siyah \\u00c7ay\":4,\"Somon\":0,\"Soya Fasulyesi\":5,\"Soya Filizi\":5,\"Soya Unu\":4,\"Soya Ya\\u011f\\u0131\":4,\"So\\u011fan\":5,\"Tere\":5,\"Susam\":4,\"S\\u00fctl\\u00fc kremalar\":6,\"S\\u0131\\u011f\\u0131r Eti\":7,\"Tam Bu\\u011fday Unu\":3,\"Tam Tah\\u0131ll\\u0131 Ekmek\":7,\"Tam Tah\\u0131ll\\u0131 Gevrekler\":5,\"Tam Tah\\u0131l Unu\":5,\"Tatl\\u0131 Patates\":7,\"Tavuk Eti\":6,\"Tereya\\u011f\\u0131\":5,\"Tofu\":4,\"Ton Bal\\u0131\\u011f\\u0131\":1,\"Tulum Peyniri\":3,\"Tuna Bal\\u0131\\u011f\\u0131\":6,\"Turp\":6,\"Uskumru\":0,\"Vi\\u015fne \":7,\"Vi\\u015fne Suyu\":7,\"Whey Protein Destekleri\":4,\"Yaban Mersini\":6,\"Yo\\u011furt (Az Ya\\u011fl\\u0131)\":5,\"Yenge\\u00e7\":4,\"Yer Elmas\\u0131\":6,\"Yer F\\u0131st\\u0131\\u011f\\u0131\":3,\"Taze Fasulye\":5,\"Ye\\u015fil \\u00c7ay\":3,\"Yo\\u011furt (Tam Ya\\u011fl\\u0131)\":4,\"Yulaf\":6,\"Yulaf Kepe\\u011fi\":6,\"Yulafl\\u0131 Ekmek\":6,\"Yumurta\":5,\"Zeytin\":8,\"Zeytin Ezmesi\":7,\"Zeytinya\\u011f\\u0131\":5,\"\\u00c7am F\\u0131st\\u0131\\u011f\\u0131\":6,\"\\u00c7avdar\":3,\"\\u00c7avdar Ekme\\u011fi\":4,\"\\u00c7avdar Unu\":5,\"\\u00c7ilek\":6,\"\\u00c7ipura\":3,\"\\u00c7\\u00f6kelek\":5,\"\\u00dcz\\u00fcm Suyu \":7,\"\\u015ealgam\":5,\"\\u015eeftali\":6}', 7, NULL, 16, 1, '2020-03-09 14:23:11', '2020-03-17 18:38:16', '2020-03-30 15:18:35', '2020-03-30 15:19:02', '2020-04-03 08:28:10', '2020-03-11 11:54:12', 4, 2, '2020-03-06 04:22:05', '2020-04-03 08:28:10', NULL),
(579, 'AB1574', '{\"Mikrobiyom \\u00c7e\\u015fitlili\\u011finiz\":\"8\",\"\\u00d6nemli Organizmalar\\u0131n\\u0131z\":{\"ORGANIZMA\":[\"KULLANICI\",\"TOPLUM\"],\"Alistipes\":[\"0\",\"76\"],\"Butyrivibrio\":[\"0\",\"90\"],\"Escherichia\":[\"0\",\"92\"],\"Bacteroides\":[\"13\",\"54\"],\"Blautia\":[\"24\",\"64\"],\"Akkermansia\":[\"32\",\"80\"],\"Clostridium\":[\"35\",\"72\"],\"Haemophilus\":[\"44\",\"83\"],\"Parabacteroides\":[\"47\",\"65\"],\"Anaerotruncus\":[\"54\",\"78\"],\"Bifidobacterium\":[\"55\",\"77\"],\"Anaerostipes\":[\"57\",\"74\"],\"Ruminococcus\":[\"61\",\"64\"],\"Lachnospiraceae\":[\"61\",\"56\"],\"Dorea\":[\"77\",\"70\"],\"Faecalibacterium\":[\"80\",\"61\"],\"Coprococcus\":[\"81\",\"62\"],\"Roseburia\":[\"81\",\"70\"],\"Streptococcus\":[\"82\",\"81\"],\"Ruminococcaceae\":[\"85\",\"53\"],\"Catenibacterium\":[\"87\",\"91\"],\"Prevotella\":[\"93\",\"81\"],\"Eubacterium\":[\"93\",\"79\"],\"Phascolarctobacterium\":[\"97\",\"70\"],\"Desulfovibrio\":[\"98\",\"74\"],\"Collinsella\":[\"98\",\"73\"]},\"Taksonomik Analiz\":{\"Taksonomik Analiz (Cins Seviyesi)\":{\"TAX\":[\"KULLANICI\",\"TOPLUM\",\"SAGLIKLI\"],\"Prevotella\":[\"26.085632823786902\",\"0.1755526608462995\",\"0.18805477790579875\"],\"Ruminococcaceae\":[\"15.181780388540265\",\"8.59439235986533\",\"9.066843166957936\"],\"Faecalibacterium\":[\"10.860783564157487\",\"6.514846244369883\",\"6.706931790107228\"],\"Lachnospiraceae\":[\"6.741764937910864\",\"7.11711465936931\",\"7.260400822181206\"],\"Bacteroides\":[\"5.66430036090735\",\"26.36581355734437\",\"25.35405933303747\"],\"Clostridiales\":[\"4.165057771830373\",\"4.955044777367546\",\"5.690149576974722\"],\"Phascolarctobacterium\":[\"4.112335526814196\",\"0.6239314639417413\",\"0.6291177123847058\"],\"Coprococcus\":[\"3.360115243391537\",\"2.13217518121622\",\"2.1864993968471707\"],\"Di\\u011fer\":[\"23.82822938266102\",\"43.52112909567929\",\"42.91794342360377\"]},\"Taksonomik Analiz (Aile seviyesi)\":{\"TAX\":[\"KULLANICI\",\"TOPLUM\",\"SAGLIKLI\"],\"Ruminococcaceae\":[\"30.603409886758303\",\"18.593607440392915\",\"19.173199559772755\"],\"Prevotellaceae\":[\"26.085632823786902\",\"4.464190420269295\",\"5.339408930792739\"],\"Lachnospiraceae\":[\"16.709983084694297\",\"15.562550243855489\",\"15.435685293116789\"],\"Bacteroidaceae\":[\"5.66430036090735\",\"26.36711369744144\",\"25.354459755148163\"],\"Di\\u011fer\":[\"20.936673843853143\",\"35.01253819804086\",\"34.697246461169556\"]},\"Taksonomik Analiz (\\u015eube Seviyesi)\":{\"TAX\":[\"KULLANICI\",\"TOPLUM\",\"SAGLIKLI\"],\"Firmicutes\":[\"58.85882321433713\",\"46.73062860310761\",\"47.58803695446815\"],\"Bacteroidetes\":[\"34.68232987757353\",\"37.866835633649075\",\"37.89050589756257\"],\"Di\\u011fer\":[\"6.458846908089342\",\"15.402535763243321\",\"14.521457147969272\"]}},\"Yak\\u0131n Profiller\":{\"PROFIL\":[\"YUZDE\"],\"fazla kilolu\":[\"24\"],\"cilt rahats\\u0131zl\\u0131klar\\u0131na sahip\":[\"19\"],\"gluten hassasiyeti veya alerjisine sahip\":[\"15\"],\"huzursuz ba\\u011f\\u0131rsak sendromu ya\\u015f\\u0131yor\":[\"11\"],\"g\\u0131da alerjisi bulunuyor\":[\"10\"],\"migren rahats\\u0131zl\\u0131\\u011f\\u0131 ya\\u015f\\u0131yor\":[\"10\"],\"laktoz hassasiyeti problemi var\":[\"9\"],\"zaman zaman ishal problemi ya\\u015f\\u0131yor\":[\"9\"],\"vejetaryen\":[\"8\"],\"vejetaryen, ancak deniz \\u00fcr\\u00fcnleri t\\u00fcketiyor\":[\"7\"],\"akci\\u011fer rahats\\u0131zl\\u0131\\u011f\\u0131na sahip\":[\"6\"],\"otoimm\\u00fcn bozuklu\\u011fa sahip\":[\"6\"],\"tiroid rahats\\u0131zl\\u0131\\u011f\\u0131na sahip\":[\"5\"],\"hayat\\u0131n\\u0131n bir d\\u00f6neminde kanser te\\u015fhisi alm\\u0131\\u015f\":[\"5\"],\"mantar problemleri ya\\u015f\\u0131yor\":[\"4\"],\"k\\u0131rm\\u0131z\\u0131 et yemiyor\":[\"4\"],\"dikkat bozuklu\\u011fu rahats\\u0131zl\\u0131\\u011f\\u0131 ya\\u015f\\u0131yor\":[\"4\"],\"kalp rahats\\u0131zl\\u0131\\u011f\\u0131 ya\\u015f\\u0131yor\":[\"3\"],\"karaci\\u011fer rahats\\u0131zl\\u0131\\u011f\\u0131na sahip\":[\"3\"]},\"Ba\\u011f\\u0131rsak Skorlar\\u0131\":{\"V\\u00fccut K\\u00fctle Endeksi-Mikrobiyom \\u0130li\\u015fkiniz\":[\"34\",\"15\",\"72\",\"35\"],\"Karbonhidrat Metabolizman\\u0131z\":[\"54\",\"\",\"\",\"48\"],\"Protein Metabolizman\\u0131z\":[\"7\",\"\",\"\",\"29\"],\"Ya\\u011f Metabolizman\\u0131z\":[\"7\",\"\",\"\",\"29\"],\"Probiyotik Mikroorganizma \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"68\",\"17\",\"78\",\"43\"],\"Uyku Kaliteniz\":[\"67\",\"23\",\"84\",\"74\"],\"Ba\\u011f\\u0131rsak Hareketlilik \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"27\",\"29\",\"69\",\"35\"],\"Gluten Hassasiyeti \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"15\",\"16\",\"64\",\"24\"],\"Laktoz Hassasiyeti \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"18\",\"15\",\"82\",\"27\"],\"Vitamin Senteziniz\":[\"94\",\"56\",\"73\",\"67\"],\"Mikrobiyom Ya\\u015f\\u0131n\\u0131z\":[\"34\",\"\",\"\",\"\"],\"Yapay Tatland\\u0131r\\u0131c\\u0131 Hasar\\u0131n\\u0131z\":[\"21\",\"27\",\"58\",\"32\"],\"\\u015eeker T\\u00fcketim Skorunuz\":[\"54\",\"7\",\"81\",\"59\"],\"Antibiyotik Hasar \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"61\",\"27\",\"74\",\"34\"],\"Otoimm\\u00fcnite Endeksiniz\":[\"42\",\"23\",\"62\",\"29\"]}}', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 2, '2020-03-06 05:11:26', '2020-04-08 12:56:50', NULL),
(580, 'AB1581', '{\"Mikrobiyom \\u00c7e\\u015fitlili\\u011finiz\":\"7\",\"\\u00d6nemli Organizmalar\\u0131n\\u0131z\":{\"ORGANIZMA\":[\"KULLANICI\",\"TOPLUM\"],\"Alistipes\":[\"0\",\"76\"],\"Eubacterium\":[\"0\",\"79\"],\"Butyrivibrio\":[\"0\",\"90\"],\"Escherichia\":[\"0\",\"92\"],\"Bacteroides\":[\"9\",\"54\"],\"Parabacteroides\":[\"22\",\"65\"],\"Anaerostipes\":[\"25\",\"74\"],\"Blautia\":[\"27\",\"64\"],\"Ruminococcus\":[\"31\",\"64\"],\"Dorea\":[\"32\",\"70\"],\"Desulfovibrio\":[\"34\",\"74\"],\"Phascolarctobacterium\":[\"40\",\"70\"],\"Roseburia\":[\"45\",\"70\"],\"Coprococcus\":[\"49\",\"62\"],\"Lachnospiraceae\":[\"50\",\"56\"],\"Anaerotruncus\":[\"53\",\"78\"],\"Bifidobacterium\":[\"57\",\"77\"],\"Akkermansia\":[\"61\",\"80\"],\"Ruminococcaceae\":[\"70\",\"53\"],\"Haemophilus\":[\"73\",\"83\"],\"Clostridium\":[\"77\",\"72\"],\"Faecalibacterium\":[\"82\",\"61\"],\"Streptococcus\":[\"89\",\"81\"],\"Collinsella\":[\"94\",\"73\"],\"Prevotella\":[\"95\",\"81\"],\"Catenibacterium\":[\"96\",\"91\"]},\"Taksonomik Analiz\":{\"Taksonomik Analiz (Cins Seviyesi)\":{\"TAX\":[\"KULLANICI\",\"TOPLUM\",\"SAGLIKLI\"],\"Prevotella\":[\"7.561596352148696\",\"0.1755526608462995\",\"0.18805477790579875\"],\"Faecalibacterium\":[\"11.299793975784468\",\"6.514846244369883\",\"6.706931790107228\"],\"Ruminococcaceae\":[\"10.17822451037097\",\"8.59439235986533\",\"9.066843166957936\"],\"Lachnospiraceae\":[\"9.251662707721701\",\"7.11711465936931\",\"7.260400822181206\"],\"Victivallaceae\":[\"7.077933292027887\",\"0.0176533850080952\",\"0.023799359008268666\"],\"Clostridiales\":[\"3.754723701125082\",\"4.955044777367546\",\"5.690149576974722\"],\"Di\\u011fer\":[\"21.38290173542775\",\"72.44984325232724\",\"70.87576572895904\"]},\"Taksonomik Analiz (Aile seviyesi)\":{\"TAX\":[\"KULLANICI\",\"TOPLUM\",\"SAGLIKLI\"],\"Prevotellaceae\":[\"29.4938982175107\",\"4.464190420269295\",\"5.339408930792739\"],\"Ruminococcaceae\":[\"24.031847572748145\",\"18.593607440392915\",\"19.173199559772755\"],\"Lachnospiraceae\":[\"14.175330623456212\",\"15.562550243855489\",\"15.435685293116789\"],\"Paraprevotellaceae\":[\"7.5652688127349546\",\"0.7241708123085702\",\"0.8064155859179338\"],\"Di\\u011fer\":[\"24.733654773549986\",\"60.655481083173726\",\"59.245290630399786\"]},\"Taksonomik Analiz (\\u015eube Seviyesi)\":{\"TAX\":[\"KULLANICI\",\"TOPLUM\",\"SAGLIKLI\"],\"Firmicutes\":[\"46.66008072236066\",\"46.73062860310761\",\"47.58803695446815\"],\"Bacteroidetes\":[\"41.345663014346826\",\"37.866835633649075\",\"37.89050589756257\"],\"Di\\u011fer\":[\"11.994256263292513\",\"15.402535763243321\",\"14.521457147969272\"]}},\"Yak\\u0131n Profiller\":{\"PROFIL\":[\"YUZDE\"],\"fazla kilolu\":[\"20\"],\"cilt rahats\\u0131zl\\u0131klar\\u0131na sahip\":[\"20\"],\"gluten hassasiyeti veya alerjisine sahip\":[\"16\"],\"huzursuz ba\\u011f\\u0131rsak sendromu ya\\u015f\\u0131yor\":[\"15\"],\"laktoz hassasiyeti problemi var\":[\"15\"],\"migren rahats\\u0131zl\\u0131\\u011f\\u0131 ya\\u015f\\u0131yor\":[\"14\"],\"g\\u0131da alerjisi bulunuyor\":[\"11\"],\"vejetaryen\":[\"11\"],\"zaman zaman ishal problemi ya\\u015f\\u0131yor\":[\"10\"],\"akci\\u011fer rahats\\u0131zl\\u0131\\u011f\\u0131na sahip\":[\"8\"],\"k\\u0131rm\\u0131z\\u0131 et yemiyor\":[\"7\"],\"vejetaryen, ancak deniz \\u00fcr\\u00fcnleri t\\u00fcketiyor\":[\"6\"],\"dikkat bozuklu\\u011fu rahats\\u0131zl\\u0131\\u011f\\u0131 ya\\u015f\\u0131yor\":[\"6\"],\"kab\\u0131zl\\u0131k problemi ya\\u015f\\u0131yor\":[\"5\"],\"hayat\\u0131n\\u0131n bir d\\u00f6neminde kanser te\\u015fhisi alm\\u0131\\u015f\":[\"4\"],\"otoimm\\u00fcn bozuklu\\u011fa sahip\":[\"4\"],\"tiroid rahats\\u0131zl\\u0131\\u011f\\u0131na sahip\":[\"3\"],\"karaci\\u011fer rahats\\u0131zl\\u0131\\u011f\\u0131na sahip\":[\"3\"],\"kalp rahats\\u0131zl\\u0131\\u011f\\u0131 ya\\u015f\\u0131yor\":[\"3\"]},\"Ba\\u011f\\u0131rsak Skorlar\\u0131\":{\"V\\u00fccut K\\u00fctle Endeksi-Mikrobiyom \\u0130li\\u015fkiniz\":[\"29\",\"15\",\"72\",\"35\"],\"Karbonhidrat Metabolizman\\u0131z\":[\"46\",\"\",\"\",\"48\"],\"Protein Metabolizman\\u0131z\":[\"6\",\"\",\"\",\"29\"],\"Ya\\u011f Metabolizman\\u0131z\":[\"5\",\"\",\"\",\"29\"],\"Probiyotik Mikroorganizma \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"57\",\"17\",\"78\",\"43\"],\"Uyku Kaliteniz\":[\"80\",\"23\",\"84\",\"74\"],\"Ba\\u011f\\u0131rsak Hareketlilik \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"24\",\"29\",\"69\",\"35\"],\"Gluten Hassasiyeti \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"54\",\"16\",\"64\",\"24\"],\"Laktoz Hassasiyeti \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"9\",\"15\",\"82\",\"27\"],\"Vitamin Senteziniz\":[\"74\",\"56\",\"73\",\"67\"],\"Mikrobiyom Ya\\u015f\\u0131n\\u0131z\":[\"63\",\"\",\"\",\"\"],\"Yapay Tatland\\u0131r\\u0131c\\u0131 Hasar\\u0131n\\u0131z\":[\"16\",\"27\",\"58\",\"32\"],\"\\u015eeker T\\u00fcketim Skorunuz\":[\"50\",\"7\",\"81\",\"59\"],\"Antibiyotik Hasar \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"61\",\"27\",\"74\",\"34\"],\"Otoimm\\u00fcnite Endeksiniz\":[\"62\",\"23\",\"62\",\"29\"]}}', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 2, '2020-03-06 05:11:46', '2020-03-06 05:11:58', NULL),
(581, 'AB1122', '{\"Mikrobiyom \\u00c7e\\u015fitlili\\u011finiz\":\"7\",\"\\u00d6nemli Organizmalar\\u0131n\\u0131z\":{\"ORGANIZMA\":[\"KULLANICI\",\"TOPLUM\"],\"Desulfovibrio\":[\"0\",\"74\"],\"Alistipes\":[\"0\",\"76\"],\"Butyrivibrio\":[\"0\",\"90\"],\"Catenibacterium\":[\"0\",\"91\"],\"Escherichia\":[\"0\",\"92\"],\"Anaerostipes\":[\"25\",\"74\"],\"Parabacteroides\":[\"26\",\"65\"],\"Eubacterium\":[\"30\",\"79\"],\"Bifidobacterium\":[\"36\",\"77\"],\"Phascolarctobacterium\":[\"39\",\"70\"],\"Akkermansia\":[\"44\",\"80\"],\"Roseburia\":[\"49\",\"70\"],\"Bacteroides\":[\"54\",\"54\"],\"Dorea\":[\"54\",\"70\"],\"Prevotella\":[\"55\",\"81\"],\"Anaerotruncus\":[\"56\",\"78\"],\"Haemophilus\":[\"64\",\"83\"],\"Blautia\":[\"66\",\"64\"],\"Streptococcus\":[\"72\",\"81\"],\"Coprococcus\":[\"72\",\"62\"],\"Ruminococcus\":[\"76\",\"64\"],\"Lachnospiraceae\":[\"77\",\"56\"],\"Clostridium\":[\"80\",\"72\"],\"Ruminococcaceae\":[\"86\",\"53\"],\"Collinsella\":[\"91\",\"73\"],\"Faecalibacterium\":[\"94\",\"61\"]},\"Taksonomik Analiz\":{\"Taksonomik Analiz (Cins Seviyesi)\":{\"TAX\":[\"KULLANICI\",\"TOPLUM\",\"SAGLIKLI\"],\"Bacteroides\":[\"26.169902710942395\",\"26.36581355734437\",\"25.35405933303747\"],\"Faecalibacterium\":[\"18.72767948080984\",\"6.514846244369883\",\"6.706931790107228\"],\"Lachnospiraceae\":[\"10.423422684458505\",\"7.11711465936931\",\"7.260400822181206\"],\"Ruminococcaceae\":[\"7.419646156631697\",\"8.59439235986533\",\"9.066843166957936\"],\"Lachnospira\":[\"5.544723120275319\",\"1.0500766460544306\",\"0.9840398332972775\"],\"Ruminococcus\":[\"4.236279302934247\",\"0.0005002447296400818\",\"0.00046093110629444214\"],\"Clostridiales\":[\"3.979721682892217\",\"4.955044777367546\",\"5.690149576974722\"],\"Di\\u011fer\":[\"23.498624861055774\",\"45.402211510899484\",\"44.93711454633787\"]},\"Taksonomik Analiz (Aile seviyesi)\":{\"TAX\":[\"KULLANICI\",\"TOPLUM\",\"SAGLIKLI\"],\"Ruminococcaceae\":[\"31.385205860292448\",\"18.593607440392915\",\"19.173199559772755\"],\"Bacteroidaceae\":[\"26.169902710942395\",\"26.36711369744144\",\"25.354459755148163\"],\"Lachnospiraceae\":[\"22.14092196290234\",\"15.562550243855489\",\"15.435685293116789\"],\"Di\\u011fer\":[\"20.30396946586282\",\"39.476728618310155\",\"40.03665539196229\"]},\"Taksonomik Analiz (\\u015eube Seviyesi)\":{\"TAX\":[\"KULLANICI\",\"TOPLUM\",\"SAGLIKLI\"],\"Firmicutes\":[\"62.33426377189545\",\"46.73062860310761\",\"47.58803695446815\"],\"Bacteroidetes\":[\"29.331718739096406\",\"37.866835633649075\",\"37.89050589756257\"],\"Di\\u011fer\":[\"8.334017489008147\",\"15.402535763243321\",\"14.521457147969272\"]}},\"Yak\\u0131n Profiller\":{\"PROFIL\":[\"YUZDE\"],\"migren rahats\\u0131zl\\u0131\\u011f\\u0131 ya\\u015f\\u0131yor\":[\"25\"],\"cilt rahats\\u0131zl\\u0131klar\\u0131na sahip\":[\"23\"],\"gluten hassasiyeti veya alerjisine sahip\":[\"22\"],\"fazla kilolu\":[\"22\"],\"laktoz hassasiyeti problemi var\":[\"18\"],\"huzursuz ba\\u011f\\u0131rsak sendromu ya\\u015f\\u0131yor\":[\"14\"],\"g\\u0131da alerjisi bulunuyor\":[\"12\"],\"zaman zaman ishal problemi ya\\u015f\\u0131yor\":[\"11\"],\"tiroid rahats\\u0131zl\\u0131\\u011f\\u0131na sahip\":[\"11\"],\"akci\\u011fer rahats\\u0131zl\\u0131\\u011f\\u0131na sahip\":[\"11\"],\"otoimm\\u00fcn bozuklu\\u011fa sahip\":[\"10\"],\"vejetaryen\":[\"9\"],\"dikkat bozuklu\\u011fu rahats\\u0131zl\\u0131\\u011f\\u0131 ya\\u015f\\u0131yor\":[\"8\"],\"hayat\\u0131n\\u0131n bir d\\u00f6neminde kanser te\\u015fhisi alm\\u0131\\u015f\":[\"8\"],\"k\\u0131rm\\u0131z\\u0131 et yemiyor\":[\"6\"],\"vejetaryen, ancak deniz \\u00fcr\\u00fcnleri t\\u00fcketiyor\":[\"5\"],\"mantar problemleri ya\\u015f\\u0131yor\":[\"5\"],\"kab\\u0131zl\\u0131k problemi ya\\u015f\\u0131yor\":[\"5\"],\"iltihapl\\u0131 ba\\u011f\\u0131rsak hastal\\u0131\\u011f\\u0131 te\\u015fhisi alm\\u0131\\u015f\":[\"4\"],\"kalp rahats\\u0131zl\\u0131\\u011f\\u0131 ya\\u015f\\u0131yor\":[\"3\"]},\"Ba\\u011f\\u0131rsak Skorlar\\u0131\":{\"V\\u00fccut K\\u00fctle Endeksi-Mikrobiyom \\u0130li\\u015fkiniz\":[\"44\",\"15\",\"72\",\"35\"],\"Karbonhidrat Metabolizman\\u0131z\":[\"40\",\"\",\"\",\"48\"],\"Protein Metabolizman\\u0131z\":[\"28\",\"\",\"\",\"29\"],\"Ya\\u011f Metabolizman\\u0131z\":[\"27\",\"\",\"\",\"29\"],\"Probiyotik Mikroorganizma \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"22\",\"17\",\"78\",\"43\"],\"Uyku Kaliteniz\":[\"67\",\"23\",\"84\",\"74\"],\"Ba\\u011f\\u0131rsak Hareketlilik \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"52\",\"29\",\"69\",\"35\"],\"Gluten Hassasiyeti \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"63\",\"16\",\"64\",\"24\"],\"Laktoz Hassasiyeti \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"67\",\"15\",\"82\",\"27\"],\"Vitamin Senteziniz\":[\"52\",\"56\",\"73\",\"67\"],\"Mikrobiyom Ya\\u015f\\u0131n\\u0131z\":[\"44\",\"\",\"\",\"\"],\"Yapay Tatland\\u0131r\\u0131c\\u0131 Hasar\\u0131n\\u0131z\":[\"22\",\"27\",\"58\",\"32\"],\"\\u015eeker T\\u00fcketim Skorunuz\":[\"16\",\"7\",\"81\",\"59\"],\"Antibiyotik Hasar \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"21\",\"27\",\"74\",\"34\"],\"Otoimm\\u00fcnite Endeksiniz\":[\"36\",\"23\",\"62\",\"29\"]}}', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 2, '2020-03-11 05:40:14', '2020-03-12 12:15:57', NULL),
(582, 'AA0056', '{\"Mikrobiyom \\u00c7e\\u015fitlili\\u011finiz\":\"6\",\"\\u00d6nemli Organizmalar\\u0131n\\u0131z\":{\"ORGANIZMA\":[\"KULLANICI\",\"TOPLUM\"],\"Alistipes\":[\"0\",\"76\"],\"Butyrivibrio\":[\"0\",\"90\"],\"Catenibacterium\":[\"0\",\"91\"],\"Escherichia\":[\"0\",\"92\"],\"Eubacterium\":[\"29\",\"79\"],\"Phascolarctobacterium\":[\"31\",\"70\"],\"Desulfovibrio\":[\"34\",\"74\"],\"Streptococcus\":[\"38\",\"81\"],\"Dorea\":[\"38\",\"70\"],\"Akkermansia\":[\"41\",\"80\"],\"Haemophilus\":[\"45\",\"83\"],\"Roseburia\":[\"45\",\"70\"],\"Anaerostipes\":[\"47\",\"74\"],\"Clostridium\":[\"49\",\"72\"],\"Prevotella\":[\"53\",\"81\"],\"Blautia\":[\"56\",\"64\"],\"Ruminococcus\":[\"56\",\"64\"],\"Anaerotruncus\":[\"61\",\"78\"],\"Bifidobacterium\":[\"64\",\"77\"],\"Faecalibacterium\":[\"64\",\"61\"],\"Bacteroides\":[\"65\",\"54\"],\"Parabacteroides\":[\"65\",\"65\"],\"Coprococcus\":[\"67\",\"62\"],\"Lachnospiraceae\":[\"72\",\"56\"],\"Ruminococcaceae\":[\"78\",\"53\"],\"Collinsella\":[\"97\",\"73\"]},\"Taksonomik Analiz\":{\"Taksonomik Analiz (Cins Seviyesi)\":{\"TAX\":[\"KULLANICI\",\"TOPLUM\",\"SAGLIKLI\"],\"Bacteroides\":[\"32.28796328337891\",\"26.36581355734437\",\"25.35405933303747\"],\"Ruminococcaceae\":[\"15.946302654454078\",\"8.59439235986533\",\"9.066843166957936\"],\"Lachnospiraceae\":[\"10.905934532054253\",\"7.11711465936931\",\"7.260400822181206\"],\"Faecalibacterium\":[\"6.979108453952549\",\"6.514846244369883\",\"6.706931790107228\"],\"Clostridiales\":[\"4.256191954546849\",\"4.955044777367546\",\"5.690149576974722\"],\"Lachnospira\":[\"4.046325254227563\",\"1.0500766460544306\",\"0.9840398332972775\"],\"YS2\":[\"2.627792769856573\",\"0.13041948038096782\",\"0.1593879042265837\"],\"Di\\u011fer\":[\"22.950381097529217\",\"45.27229227524816\",\"44.778187573217586\"]},\"Taksonomik Analiz (Aile seviyesi)\":{\"TAX\":[\"KULLANICI\",\"TOPLUM\",\"SAGLIKLI\"],\"Bacteroidaceae\":[\"32.28796328337891\",\"26.36711369744144\",\"25.354459755148163\"],\"Ruminococcaceae\":[\"27.592418635950178\",\"18.593607440392915\",\"19.173199559772755\"],\"Lachnospiraceae\":[\"20.23935506529856\",\"15.562550243855489\",\"15.435685293116789\"],\"Di\\u011fer\":[\"19.88026301537235\",\"39.476728618310155\",\"40.03665539196229\"]},\"Taksonomik Analiz (\\u015eube Seviyesi)\":{\"TAX\":[\"KULLANICI\",\"TOPLUM\",\"SAGLIKLI\"],\"Firmicutes\":[\"53.026717877689435\",\"46.73062860310761\",\"47.58803695446815\"],\"Bacteroidetes\":[\"38.68800609296301\",\"37.866835633649075\",\"37.89050589756257\"],\"Di\\u011fer\":[\"8.285276029347557\",\"15.402535763243321\",\"14.521457147969272\"]}},\"Yak\\u0131n Profiller\":{\"PROFIL\":[\"YUZDE\"],\"fazla kilolu\":[\"27\"],\"cilt rahats\\u0131zl\\u0131klar\\u0131na sahip\":[\"24\"],\"gluten hassasiyeti veya alerjisine sahip\":[\"19\"],\"laktoz hassasiyeti problemi var\":[\"17\"],\"migren rahats\\u0131zl\\u0131\\u011f\\u0131 ya\\u015f\\u0131yor\":[\"16\"],\"huzursuz ba\\u011f\\u0131rsak sendromu ya\\u015f\\u0131yor\":[\"14\"],\"g\\u0131da alerjisi bulunuyor\":[\"12\"],\"k\\u0131rm\\u0131z\\u0131 et yemiyor\":[\"9\"],\"akci\\u011fer rahats\\u0131zl\\u0131\\u011f\\u0131na sahip\":[\"9\"],\"otoimm\\u00fcn bozuklu\\u011fa sahip\":[\"9\"],\"dikkat bozuklu\\u011fu rahats\\u0131zl\\u0131\\u011f\\u0131 ya\\u015f\\u0131yor\":[\"8\"],\"tiroid rahats\\u0131zl\\u0131\\u011f\\u0131na sahip\":[\"7\"],\"zaman zaman ishal problemi ya\\u015f\\u0131yor\":[\"7\"],\"kab\\u0131zl\\u0131k problemi ya\\u015f\\u0131yor\":[\"6\"],\"hayat\\u0131n\\u0131n bir d\\u00f6neminde kanser te\\u015fhisi alm\\u0131\\u015f\":[\"5\"],\"vejetaryen, ancak deniz \\u00fcr\\u00fcnleri t\\u00fcketiyor\":[\"5\"],\"kalp rahats\\u0131zl\\u0131\\u011f\\u0131 ya\\u015f\\u0131yor\":[\"4\"],\"mantar problemleri ya\\u015f\\u0131yor\":[\"3\"],\"iltihapl\\u0131 ba\\u011f\\u0131rsak hastal\\u0131\\u011f\\u0131 te\\u015fhisi alm\\u0131\\u015f\":[\"3\"],\"b\\u00f6brek rahats\\u0131zl\\u0131\\u011f\\u0131 ya\\u015f\\u0131yor\":[\"3\"]},\"Ba\\u011f\\u0131rsak Skorlar\\u0131\":{\"V\\u00fccut K\\u00fctle Endeksi-Mikrobiyom \\u0130li\\u015fkiniz\":[\"67\",\"15\",\"72\",\"35\"],\"Karbonhidrat Metabolizman\\u0131z\":[\"55\",\"\",\"\",\"48\"],\"Protein Metabolizman\\u0131z\":[\"34\",\"\",\"\",\"29\"],\"Ya\\u011f Metabolizman\\u0131z\":[\"34\",\"\",\"\",\"29\"],\"Probiyotik Mikroorganizma \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"28\",\"17\",\"78\",\"43\"],\"Uyku Kaliteniz\":[\"22\",\"23\",\"84\",\"74\"],\"Ba\\u011f\\u0131rsak Hareketlilik \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"38\",\"29\",\"69\",\"35\"],\"Gluten Hassasiyeti \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"51\",\"16\",\"64\",\"24\"],\"Laktoz Hassasiyeti \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"62\",\"15\",\"82\",\"27\"],\"Vitamin Senteziniz\":[\"75\",\"56\",\"73\",\"67\"],\"Mikrobiyom Ya\\u015f\\u0131n\\u0131z\":[\"55\",\"\",\"\",\"\"],\"Yapay Tatland\\u0131r\\u0131c\\u0131 Hasar\\u0131n\\u0131z\":[\"40\",\"27\",\"58\",\"32\"],\"\\u015eeker T\\u00fcketim Skorunuz\":[\"42\",\"7\",\"81\",\"59\"],\"Antibiyotik Hasar \\u00d6l\\u00e7\\u00fcm\\u00fcn\\u00fcz\":[\"48\",\"27\",\"74\",\"34\"],\"Otoimm\\u00fcnite Endeksiniz\":[\"50\",\"23\",\"62\",\"29\"]}}', '{\"Ada\\u00e7ay\\u0131\":7,\"Ahududu\":9,\"Alabal\\u0131k\":4,\"Amaranth\":7,\"Ananas\":8,\"Antep F\\u0131st\\u0131\\u011f\\u0131\":4,\"Armut\":9,\"Armut Suyu\":9,\"Arpa\":8,\"Asma Yapra\\u011f\\u0131\":8,\"Aspir Ya\\u011f\\u0131\":7,\"Avokado\":7,\"Ay\\u00e7ekirde\\u011fi\":6,\"Ay\\u00e7i\\u00e7ek Ya\\u011f\\u0131\":6,\"\\u0130nek S\\u00fct\\u00fc (Az Ya\\u011fl\\u0131)\":5,\"A\\u00e7ai \\u00dcz\\u00fcm\\u00fc\":9,\"Badem \":3,\"Bakla\":8,\"Balkaba\\u011f\\u0131\":7,\"Bal\\u0131k Ya\\u011f\\u0131 Destekleri\":8,\"Barbunya\":7,\"Beyaz Dut\":10,\"Beyaz Ekmek\":8,\"Beyaz Peynir (Tam Ya\\u011fl\\u0131)\":4,\"Beyaz Un\":9,\"Bezelye\":6,\"Bira\":8,\"Brokoli\":7,\"Br\\u00fcksel Lahanas\\u0131\":8,\"Bulgur\":7,\"Bu\\u011fday\":7,\"Bu\\u011fday Ru\\u015feymi\":6,\"Bu\\u011fday Unu\":7,\"B\\u00f6r\\u00fclce\":8,\"B\\u00f6\\u011f\\u00fcrtlen\":8,\"Ceviz\":3,\"Cheddar\":5,\"Chia Tohumu\":7,\"Dana Eti\":4,\"Domates\":7,\"Dut Kurusu\":10,\"Ebeg\\u00fcmeci\":8,\"Elma\":9,\"Elma Suyu\":9,\"Enginar\":8,\"Erik\":8,\"Eski Ka\\u015far\":5,\"Esmer Pirin\\u00e7\":7,\"Ezine Peyniri\":5,\"Frambuaz Suyu\":9,\"F\\u0131nd\\u0131k\":4,\"F\\u0131nd\\u0131k Ya\\u011f\\u0131\":7,\"Grayver\":5,\"Greyfurt \":7,\"Greyfurt Suyu\":7,\"Hamsi\":5,\"Havu\\u00e7\":8,\"Havu\\u00e7 Suyu\":9,\"Hindi Eti\":6,\"Hindiba\":7,\"Hindistan Cevizi\":7,\"Taze Hurma\":8,\"Kabak \\u00c7ekirde\\u011fi\":7,\"Kahve\":8,\"Kaju\":7,\"Kakao\":7,\"Kale\":8,\"Kanola Ya\\u011f\\u0131\":6,\"Kapari\":8,\"Karabu\\u011fday \":6,\"Karadut\":10,\"Karalahana\":7,\"Karides\":7,\"Karnabahar\":8,\"Kavun\":8,\"Kaymak\":8,\"Taze Kay\\u0131s\\u0131\":9,\"Kay\\u0131s\\u0131 Suyu\":9,\"Ka\\u015far Peyniri\":6,\"Kefir \":7,\"Kepekli Ekmek\":7,\"Kepekli Makarna\":7,\"Kereviz\":8,\"Kestane\":8,\"Keten Tohumu\":8,\"Ke\\u00e7i Eti\":7,\"Ke\\u00e7i Peyniri\":5,\"Ke\\u00e7i S\\u00fct\\u00fc\":4,\"Kinoa\":6,\"Kivi \":9,\"Koyun Eti \":8,\"Koyun Peyniri\":6,\"Koyun S\\u00fct\\u00fc\":4,\"Krem Peynir\":6,\"Kril Ya\\u011f\\u0131 Destekleri\":7,\"Kuru Fasulye\":6,\"Kuru Hurma\":8,\"Kuru Kay\\u0131s\\u0131\":7,\"Kuru \\u00dcz\\u00fcm\":10,\"Kuyruk Ya\\u011f\\u0131\":6,\"Kuzu Eti \":9,\"Ku\\u015fkonmaz\":8,\"K\\u0131l\\u0131\\u00e7 Bal\\u0131\\u011f\\u0131\":5,\"K\\u0131rm\\u0131z\\u0131-Mor \\u00dcz\\u00fcm\":9,\"K\\u0131rm\\u0131z\\u0131 Biber\":8,\"Frenk \\u00dcz\\u00fcm\\u00fc\":10,\"K\\u0131rm\\u0131z\\u0131 Pancar\":8,\"K\\u0131rm\\u0131z\\u0131 \\u015earap\":6,\"K\\u0131z\\u0131lc\\u0131k\":8,\"Beyaz Lahana\":8,\"Lava\\u015f\":8,\"Levrek\":5,\"Limon\":7,\"Lor Peyniri\":4,\"L\\u00fcfer\":5,\"Maden Suyu\":8,\"Makarna\":7,\"Manda S\\u00fct\\u00fc\":5,\"Mandalina \":7,\"Mandalina Suyu\":8,\"Mango\":8,\"Mantar\":8,\"Margarin\":7,\"Maydanoz\":8,\"Mercimek\":6,\"Midye \":7,\"Mor Lahana\":8,\"Morina Bal\\u0131\\u011f\\u0131\":5,\"Muz\":7,\"M\\u0131s\\u0131r\":8,\"M\\u0131s\\u0131r Gevre\\u011fi\":8,\"M\\u0131s\\u0131r Ke\\u011fe\\u011fi\":8,\"M\\u0131s\\u0131r Ni\\u015fastas\\u0131\":9,\"M\\u0131s\\u0131r Unu\":8,\"M\\u0131s\\u0131r Ya\\u011f\\u0131\":7,\"M\\u0131s\\u0131r\\u00f6z\\u00fc Ya\\u011f\\u0131\":7,\"Taze Nane\":7,\"Nar Suyu\":9,\"Nar\":9,\"Nektarin\":8,\"Nohut\":7,\"Noodle\":8,\"Oolong \\u00c7ay\\u0131\":8,\"Otlu Peynir\":6,\"Palamut\":5,\"Papaya\":8,\"\\u0130nek S\\u00fct\\u00fc (Tam Ya\\u011fl\\u0131)\":4,\"Patates\":8,\"Patl\\u0131can\":8,\"Pembe \\u015earap\":7,\"Pirin\\u00e7\":8,\"Pirin\\u00e7 Unu\":7,\"Portakal\":7,\"Portakal Suyu\":7,\"Protein Barlar\":7,\"Ricotta\":5,\"Roka\":8,\"Rokfor\":5,\"Salep\":8,\"Sardalya\":5,\"Sar\\u0131msak\":8,\"Semizotu \":8,\"Siyah Havu\\u00e7\":8,\"Siyah \\u00c7ay\":7,\"Somon\":2,\"Soya Fasulyesi\":5,\"Soya Filizi\":7,\"Soya Unu\":6,\"Soya Ya\\u011f\\u0131\":6,\"So\\u011fan\":8,\"Tere\":8,\"Susam\":7,\"S\\u00fctl\\u00fc kremalar\":8,\"S\\u0131\\u011f\\u0131r Eti\":7,\"Tam Bu\\u011fday Unu\":7,\"Tam Tah\\u0131ll\\u0131 Ekmek\":10,\"Tam Tah\\u0131ll\\u0131 Gevrekler\":8,\"Tam Tah\\u0131l Unu\":8,\"Tatl\\u0131 Patates\":8,\"Tavuk Eti\":6,\"Tereya\\u011f\\u0131\":6,\"Tofu\":6,\"Ton Bal\\u0131\\u011f\\u0131\":5,\"Tulum Peyniri\":5,\"Tuna Bal\\u0131\\u011f\\u0131\":5,\"Turp\":9,\"Uskumru\":4,\"Vi\\u015fne \":9,\"Vi\\u015fne Suyu\":9,\"Whey Protein Destekleri\":6,\"Yaban Mersini\":8,\"Yo\\u011furt (Az Ya\\u011fl\\u0131)\":8,\"Yenge\\u00e7\":7,\"Yer Elmas\\u0131\":8,\"Yer F\\u0131st\\u0131\\u011f\\u0131\":6,\"Taze Fasulye\":8,\"Ye\\u015fil \\u00c7ay\":7,\"Yo\\u011furt (Tam Ya\\u011fl\\u0131)\":6,\"Yulaf\":9,\"Yulaf Kepe\\u011fi\":8,\"Yulafl\\u0131 Ekmek\":9,\"Yumurta\":5,\"Zeytin\":9,\"Zeytin Ezmesi\":8,\"Zeytinya\\u011f\\u0131\":6,\"\\u00c7am F\\u0131st\\u0131\\u011f\\u0131\":7,\"\\u00c7avdar\":6,\"\\u00c7avdar Ekme\\u011fi\":7,\"\\u00c7avdar Unu\":7,\"\\u00c7ilek\":8,\"\\u00c7ipura\":5,\"\\u00c7\\u00f6kelek\":7,\"\\u00dcz\\u00fcm Suyu \":9,\"\\u015ealgam\":8,\"\\u015eeftali\":9}', 118, NULL, 16, 1, '2020-04-09 09:25:39', '2020-03-30 15:09:13', NULL, NULL, '2020-05-13 14:09:44', '2020-05-06 19:25:20', 4, 2, '2020-03-22 12:54:00', '2020-05-13 14:09:44', NULL),
(583, NULL, NULL, NULL, NULL, 36, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-04-01 17:14:06', '2020-04-01 17:14:06', NULL),
(584, NULL, NULL, NULL, NULL, 36, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-04-01 17:14:06', '2020-04-01 17:14:06', NULL),
(585, NULL, NULL, NULL, NULL, 36, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-04-01 17:14:06', '2020-04-01 17:14:06', NULL),
(586, NULL, NULL, NULL, NULL, 36, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-04-01 17:14:06', '2020-04-01 17:14:06', NULL),
(587, NULL, NULL, NULL, NULL, 36, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-04-01 17:14:06', '2020-04-01 17:14:06', NULL),
(588, NULL, NULL, NULL, NULL, 36, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-04-01 17:14:06', '2020-04-01 17:14:06', NULL),
(589, NULL, NULL, NULL, NULL, 36, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-04-01 17:14:06', '2020-04-01 17:14:06', NULL),
(590, NULL, NULL, NULL, NULL, 36, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-04-01 17:14:06', '2020-04-01 17:14:06', NULL),
(591, NULL, NULL, NULL, NULL, 36, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-04-01 17:14:06', '2020-04-01 17:14:06', NULL),
(592, NULL, NULL, NULL, NULL, 36, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-04-01 17:14:06', '2020-04-01 17:14:06', NULL),
(593, NULL, NULL, NULL, NULL, 37, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-04-01 17:33:20', '2020-04-01 17:33:20', NULL),
(594, '123321', NULL, NULL, NULL, 38, NULL, 0, NULL, '2020-04-03 09:20:27', '2020-04-03 09:27:52', '2020-04-03 09:28:35', NULL, NULL, 3, 1, '2020-04-02 12:04:46', '2020-04-03 09:28:35', NULL),
(595, NULL, NULL, NULL, NULL, 38, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-04-02 12:04:46', '2020-04-02 12:04:46', NULL),
(596, NULL, NULL, NULL, NULL, 38, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-04-02 12:04:46', '2020-04-02 12:04:46', NULL),
(597, NULL, NULL, NULL, NULL, 38, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-04-02 12:04:46', '2020-04-02 12:04:46', NULL),
(598, NULL, NULL, NULL, NULL, 38, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-04-02 12:04:46', '2020-04-02 12:04:46', NULL),
(599, NULL, NULL, NULL, NULL, 38, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-04-02 12:04:46', '2020-04-02 12:04:46', NULL),
(600, NULL, NULL, NULL, NULL, 38, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-04-02 12:04:46', '2020-04-02 12:04:46', NULL),
(601, 'laithatik', NULL, NULL, NULL, 38, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-04-02 12:04:46', '2020-04-08 12:13:40', NULL),
(602, '123ewq', NULL, NULL, NULL, 38, NULL, 0, NULL, '2020-04-03 10:53:25', '2020-04-03 10:54:25', NULL, NULL, NULL, 2, 1, '2020-04-02 12:04:46', '2020-04-08 13:22:44', NULL),
(603, '1', NULL, NULL, 17, 38, NULL, 1, '2020-04-08 13:22:35', NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-04-02 12:04:46', '2020-04-08 13:22:35', NULL),
(604, NULL, NULL, NULL, NULL, 39, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-04-06 09:46:01', '2020-04-06 09:46:01', NULL),
(605, '123ewq1', NULL, NULL, 17, 39, NULL, 1, '2020-04-08 12:50:24', '2020-04-08 08:33:00', NULL, NULL, NULL, NULL, 1, 1, '2020-04-06 09:46:01', '2020-04-08 16:37:14', NULL),
(606, 'wwww', NULL, NULL, 17, 39, NULL, 1, '2020-04-09 07:58:54', NULL, NULL, NULL, NULL, NULL, 0, 1, '2020-04-06 09:46:01', '2020-04-09 07:58:54', NULL),
(607, 'TEST', NULL, NULL, 124, NULL, 16, 1, '2020-05-13 18:25:27', NULL, NULL, NULL, NULL, '2020-05-13 18:30:29', 0, 2, '2020-04-09 09:19:19', '2020-05-13 18:30:29', NULL);
INSERT INTO `kits` (`id`, `kit_code`, `microbiome`, `food`, `user_id`, `order_detail_id`, `survey_id`, `registered`, `registered_at`, `delevered_to_user_at`, `received_from_user_at`, `send_to_lab_at`, `uploaded_at`, `survey_filled_at`, `kit_status`, `sell_status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(608, 'TEST1', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 2, '2020-04-09 09:41:28', '2020-04-09 09:41:28', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(22, '2014_10_12_000000_create_users_table', 1),
(23, '2014_10_12_100000_create_password_resets_table', 1),
(24, '2019_12_22_192724_create_admins_table', 1),
(25, '2019_12_22_193237_create_dietitians_table', 1),
(26, '2019_12_22_213809_create_photos_table', 1),
(27, '2019_12_30_140141_create_kits_table', 1),
(28, '2020_01_02_111745_create_products_table', 1),
(29, '2020_01_09_095840_create_coupons_table', 1),
(30, '2020_01_13_130519_create_payments_table', 1),
(31, '2020_01_13_130537_create_orders_table', 1),
(32, '2020_01_14_110718_create_order_details_table', 1),
(33, '2020_01_14_165144_create_errors_table', 1),
(34, '2020_01_22_140200_create_activity_log_table', 1),
(35, '2020_01_28_120836_create_surveys_table', 1),
(36, '2020_01_30_094055_create_questions_table', 1),
(37, '2020_01_30_115520_create_question_langs_table', 1),
(38, '2020_01_30_140349_create_answers_table', 1),
(39, '2020_01_31_091844_create_survey_langs_table', 1),
(40, '2020_01_31_102241_create_survey_sections_table', 1),
(41, '2020_01_31_103144_create_survey_section_langs_table', 1),
(42, '2020_02_04_104911_create_countries_table', 1),
(43, '2020_02_13_143239_create_sessions_table', 2),
(44, '2016_06_01_000001_create_oauth_auth_codes_table', 3),
(45, '2016_06_01_000002_create_oauth_access_tokens_table', 3),
(46, '2016_06_01_000003_create_oauth_refresh_tokens_table', 3),
(47, '2016_06_01_000004_create_oauth_clients_table', 3),
(48, '2016_06_01_000005_create_oauth_personal_access_clients_table', 3),
(51, '2020_03_04_090700_add_vegetarian_column_to_users_tables', 4),
(52, '2020_03_04_113355_add_new_column_to_kits_tables', 5),
(56, '2020_03_25_125148_create_resources_table', 6),
(58, '2020_03_27_145449_create_suggestions_table', 7),
(62, '2020_04_01_134025_create_cities_table', 8),
(63, '2020_04_01_135208_create_counties_table', 8),
(64, '2020_04_01_194459_add_new_columns_to_orders', 9),
(65, '2020_04_07_140021_add_new_columns_to_users_table', 10),
(66, '2020_04_13_122222_add_new_columns_to_admin', 11),
(67, '2020_05_12_110235_add_birth_to_users_table', 12),
(68, '2020_05_15_111122_add_duration_to_survey_sections_table', 13),
(69, '2020_05_18_150550_create_contacts_table', 13),
(70, '2020_05_28_153606_add_new_column_to_dietitian', 14);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('01ec33a7d54aeecf54e1c272ecd3e2174605592fce0b220c80b4d2f8427644e0271c9bc7dc1926d4', 7, 7, 'TutsForWeb', '[]', 0, '2020-03-09 08:13:08', '2020-03-09 08:13:08', '2020-09-09 11:13:07'),
('04a929bc5f19823c2a2b5a38313c72e8072361cd744ff5606849be9e24cde5fdd639bfa434f29eed', 118, 9, 'TutsForWeb', '[]', 0, '2020-05-13 18:21:43', '2020-05-13 18:21:43', '2020-11-13 21:21:43'),
('0ba38a3a8f40c749695a766641ca9b4376318401ae28e904ffa2b7cab63801196a15748186249da3', 7, 7, 'TutsForWeb', '[]', 0, '2020-04-07 13:53:20', '2020-04-07 13:53:20', '2020-10-07 16:53:19'),
('0f00c89276c6b9f4bce2eacb080cf671e563b5e17bf8910be41b5e6183049bf6560c554a1753ad28', 7, 7, 'TutsForWeb', '[]', 0, '2020-02-28 05:31:00', '2020-02-28 05:31:00', '2020-08-28 08:30:57'),
('0ff3d2df221fec48f28318b6a042e162a17733e21c72465b07cc26710cfab3c471a8eb79329373a3', 7, 7, 'TutsForWeb', '[]', 0, '2020-03-03 10:44:00', '2020-03-03 10:44:00', '2020-09-03 13:43:59'),
('1019dfc12228b6154919e3d323b33b3d619234f15a582844c334374c754d55b991aa4ce352e72dca', 7, 7, 'TutsForWeb', '[]', 0, '2020-03-09 08:08:10', '2020-03-09 08:08:10', '2020-09-09 11:08:10'),
('10e66e1104dfac0e70004bbedb1914907863c1c8ba4f964caa620c4181156273fff36852f789442b', 7, 7, 'TutsForWeb', '[]', 0, '2020-03-09 08:16:12', '2020-03-09 08:16:12', '2020-09-09 11:16:12'),
('14650651a97ac3edd99eb7dd226eb47ad1f0fed4594e991e45c97a98296030743f8cc136cde7ca86', 7, 7, 'TutsForWeb', '[]', 0, '2020-02-26 06:14:10', '2020-02-26 06:14:10', '2020-08-26 09:14:10'),
('164236797f051c7cd3b5906f46aaa3cdde0393d629b24ea42ee64269ea298ecd0737ddf0f34837b8', 118, 7, 'TutsForWeb', '[]', 0, '2020-04-09 11:50:33', '2020-04-09 11:50:33', '2020-10-09 14:50:33'),
('1b0279739e17ab84ccc61fd4505f055f8e52bb6226101bca6f1cc9d8b99484e39d5f3105110c0d7b', 7, 11, 'TutsForWeb', '[]', 0, '2020-05-27 15:09:06', '2020-05-27 15:09:06', '2020-11-27 18:09:05'),
('1bfc6b824dcff4f1572023d17f05079cac3d9b468b7d8b94814043c05baa87f59762cb220f4ecab0', 119, 7, 'TutsForWeb', '[]', 0, '2020-04-09 09:37:06', '2020-04-09 09:37:06', '2020-10-09 12:37:06'),
('1ea348c10afd58b7776e7b80c558e000f3b6130eb46f4ce6b343c99315806ff47385d207b8b33e8f', 7, 7, 'TutsForWeb', '[]', 0, '2020-04-07 16:40:53', '2020-04-07 16:40:53', '2020-10-07 19:40:53'),
('1ffcb4dd89c501cb440a1b729663991fae903e63993b4013803ae95839da18ab286596231dcd3dd3', 7, 7, 'TutsForWeb', '[]', 0, '2020-02-26 06:44:30', '2020-02-26 06:44:30', '2020-08-26 09:44:30'),
('21596119b834e57c67a863c1400efd97352b08ad881e1bc0dbbc2ed0c651b09136d96434cea99e3f', 7, 7, 'TutsForWeb', '[]', 0, '2020-03-09 08:05:15', '2020-03-09 08:05:15', '2020-09-09 11:05:14'),
('2367d4516481d0b123cc0fe3f8b4782430cbc4fd90a3f165402b0c0a4f588816223872fd9b33366c', 118, 7, 'TutsForWeb', '[]', 0, '2020-04-27 12:04:38', '2020-04-27 12:04:38', '2020-10-27 15:04:38'),
('24acd9f1a0f8809932f19424d303c4454ae14f94825608f45214bd8fe2dc04f405aab239550da3e8', 118, 7, 'TutsForWeb', '[]', 0, '2020-04-09 14:01:53', '2020-04-09 14:01:53', '2020-10-09 17:01:53'),
('2a409125efcda863837512edb6c69696e3fb25225c1412c034cdcefd931ea6e63d5c054903f2aca8', 7, 7, 'TutsForWeb', '[]', 0, '2020-03-02 05:57:58', '2020-03-02 05:57:58', '2020-09-02 08:57:58'),
('2bbfd6f0d91a36682cf4928fcaadeae96c0346af5ffc1cf0516db34cb8dd9c8987189f7405a53832', 124, 11, 'TutsForWeb', '[]', 0, '2020-05-21 16:16:12', '2020-05-21 16:16:12', '2020-11-21 19:16:11'),
('2c82c7bca83ac82ff5adeec994ab5c977c5ed857f014d4bd0b38d22b0d95865e33f65010c805ff6d', 7, 7, 'TutsForWeb', '[]', 0, '2020-03-09 08:04:31', '2020-03-09 08:04:31', '2020-09-09 11:04:30'),
('30333d45d4864d84f71e2575710e2d730f5c529859957858ae6c8cb8221e12f7ccb483d7ec5abd44', 118, 7, 'TutsForWeb', '[]', 0, '2020-04-13 13:51:09', '2020-04-13 13:51:09', '2020-10-13 16:51:08'),
('3118f84ab4ea83678fade2cd09de6f0d1d6c723d6462ba83b6bd585dbce9a106e9dd68bba2c60d1e', 7, 7, 'TutsForWeb', '[]', 0, '2020-03-09 08:07:11', '2020-03-09 08:07:11', '2020-09-09 11:07:10'),
('34c0f3b106dac560598355dad0bcb847489bd84d6f58a88ca8ab7afcf11d12987882f7edca9d5747', 118, 7, 'TutsForWeb', '[]', 0, '2020-04-13 18:35:14', '2020-04-13 18:35:14', '2020-10-13 21:35:14'),
('362a3d3e5ad4cec07c6b8b167f35a97d87f51a513e09b11223796499a8c785336f5357e5bda6dca2', 7, 7, 'TutsForWeb', '[]', 0, '2020-02-26 08:03:48', '2020-02-26 08:03:48', '2020-08-26 11:03:48'),
('36bef049e4980197a2a4651ada684a9d202196f7e5ae8ee051fa664db89b104397a47a2529b4ce78', 7, 7, 'TutsForWeb', '[]', 0, '2020-03-02 10:35:19', '2020-03-02 10:35:19', '2020-09-02 13:35:16'),
('391b4754763b0b56e3d831ccbf3b10fff7e0fb1ddac976ff412fd0baaa3847427851ae20ff51cd09', 7, 7, 'TutsForWeb', '[]', 0, '2020-03-09 08:16:44', '2020-03-09 08:16:44', '2020-09-09 11:16:44'),
('399bd696437bfd2dbf1d4f6e0dff8ed33cb0da0a172dbe5fe118a64507497419fcc09bb355996803', 7, 7, 'TutsForWeb', '[]', 0, '2020-03-09 08:11:46', '2020-03-09 08:11:46', '2020-09-09 11:11:46'),
('39a58cce47feb4376299db8d7f6b03390eac945042dd027a8d23b4720124b9325dd77b9f23a15248', 12, 7, 'TutsForWeb', '[]', 0, '2020-02-26 09:31:37', '2020-02-26 09:31:37', '2020-08-26 12:31:37'),
('3ee4c790c16cb8b60f98ea197b1048c9564e3d6093eec7122f114890981778dcaaaab458daa6f095', 124, 11, 'TutsForWeb', '[]', 0, '2020-05-21 16:21:25', '2020-05-21 16:21:25', '2020-11-21 19:21:25'),
('4547830a5ccf9ccc027263be6b30af7ff6686accd28dfee644be5859accc8d3d421b866c0b3a4dab', 7, 7, 'TutsForWeb', '[]', 0, '2020-03-03 09:18:40', '2020-03-03 09:18:40', '2020-09-03 12:18:40'),
('46c8897f4a3314ad599ae9388a709f508383025c38a1ddca4525ade32940efb38a340b03ade8f9da', 7, 7, 'TutsForWeb', '[]', 0, '2020-02-26 09:11:16', '2020-02-26 09:11:16', '2020-08-26 12:11:16'),
('4724ea873d3bbe234f575f0e0e453bf97f1ec34f860a086cd76cf3159bb2170f98af4ec934862f6b', 118, 7, 'TutsForWeb', '[]', 0, '2020-04-25 00:33:41', '2020-04-25 00:33:41', '2020-10-25 03:33:40'),
('4a9d5ff413a8ed8e861dcaa14da8dea030a424c4aed33ccc1c3542abb9b4ba3f6a46d16628bd5d32', 122, 7, 'TutsForWeb', '[]', 0, '2020-04-09 14:13:41', '2020-04-09 14:13:41', '2020-10-09 17:13:40'),
('4caa70fe631ce93de6ba01ebd92217c30d91e1bedf046144d1f9323b642f37d9774fe6bf90dfe2ad', 7, 7, 'TutsForWeb', '[]', 0, '2020-03-09 08:11:33', '2020-03-09 08:11:33', '2020-09-09 11:11:33'),
('4d0a4228cd856460e0a235655b962495b3da772ff6ede90a2b73937a88997202e1b2b05df8eefadb', 13, 7, 'TutsForWeb', '[]', 0, '2020-02-26 09:54:40', '2020-02-26 09:54:40', '2020-08-26 12:54:40'),
('4fbcdad1b7b274148a0047fd802b27dda8b46fadf75adf9ceb37ec1a8cadd925bd8dc3e4eff5453b', 7, 7, 'TutsForWeb', '[]', 0, '2020-03-09 08:24:45', '2020-03-09 08:24:45', '2020-09-09 11:24:45'),
('5365254a615fde7d130ba137b1b1f5598d00af6b75fba1c59ff9d4c92740c3bae1ed0cc40c282357', 7, 7, 'TutsForWeb', '[]', 0, '2020-03-09 08:12:40', '2020-03-09 08:12:40', '2020-09-09 11:12:40'),
('5d452f62aca6ccd229d633cf325a85722b50e2d69b70029fcf18b7cdea533556d32566ad557bd9a2', 121, 7, 'TutsForWeb', '[]', 0, '2020-04-09 10:20:23', '2020-04-09 10:20:23', '2020-10-09 13:20:23'),
('65abf2315ce214b7d467a517fd9b338041fc087aa72f9caea1c9d973003506a5bf295dac63c0aed9', 7, 7, 'TutsForWeb', '[]', 0, '2020-03-09 08:13:20', '2020-03-09 08:13:20', '2020-09-09 11:13:20'),
('68c978a708adb14ffe56ae1efb170a3db9db0391bcf03dd8f8127705ba7623a883531139447fb3b0', 7, 7, 'TutsForWeb', '[]', 0, '2020-03-03 11:06:14', '2020-03-03 11:06:14', '2020-09-03 14:06:14'),
('69a89b375da7b1fc2b34f451c95550b834e043c0e1c517f7a79fb637a5d8720b20a50f5f78511735', 123, 7, 'TutsForWeb', '[]', 0, '2020-04-24 22:48:18', '2020-04-24 22:48:18', '2020-10-25 01:48:18'),
('6af4400e8dd9662698a131f893f840e103d3142346956c1aa3baf63c0191aefb7f62bf634e9ed844', 7, 7, 'TutsForWeb', '[]', 0, '2020-03-09 08:15:22', '2020-03-09 08:15:22', '2020-09-09 11:15:22'),
('6e3c9aaf98478373e8da5343d3a79876e0ae6b6ccabafb656f73fccb648806a26d48cfae141c0cb3', 7, 7, 'TutsForWeb', '[]', 0, '2020-03-02 05:53:41', '2020-03-02 05:53:41', '2020-09-02 08:53:40'),
('771330eed0ddfcd237f07ce73e758536fceff8d2cd6b25de466b20bd19600abbc802ccd91204abd1', 118, 9, 'TutsForWeb', '[]', 0, '2020-05-13 13:27:24', '2020-05-13 13:27:24', '2020-11-13 16:27:23'),
('7e38f18b06c7c6312d047bc0024d4f9c51705b70b6d0495bb07dc90f7271f532b9f0b94ead15bf04', 118, 7, 'TutsForWeb', '[]', 0, '2020-04-09 09:25:40', '2020-04-09 09:25:40', '2020-10-09 12:25:39'),
('7e8cbf9fa2b95747c26e41fb20a63b3b2f906613e57b7f36f090e653cf60c9a10318fc3df56e95ff', 118, 7, 'TutsForWeb', '[]', 0, '2020-04-09 09:32:41', '2020-04-09 09:32:41', '2020-10-09 12:32:41'),
('80f3b63feaa1346a72fa238a1bc05365b8c3924b58849f9f58ea2f09d55093af93e67e115bb2dd1b', 118, 7, 'TutsForWeb', '[]', 0, '2020-04-09 11:48:38', '2020-04-09 11:48:38', '2020-10-09 14:48:38'),
('819a7386dc30571408489fbe0cd6bf8b72a8c5382617d1176fac2c71aaeaa1a232d02f5b32e6898e', 7, 7, 'TutsForWeb', '[]', 0, '2020-03-02 05:58:14', '2020-03-02 05:58:14', '2020-09-02 08:58:14'),
('832a15836ec4c969b958973d6492b2b1617fa7410c02ba66a3c32df0bf4397ce8154e6209424f0b0', 7, 7, 'TutsForWeb', '[]', 0, '2020-03-09 08:21:33', '2020-03-09 08:21:33', '2020-09-09 11:21:33'),
('84e33f1c0d0a60c1da355e4ae8ff9ef26cf97031c4674b0978fae8b5775c02b0e9d1da49efd3e21e', 7, 7, 'TutsForWeb', '[]', 0, '2020-03-09 08:15:50', '2020-03-09 08:15:50', '2020-09-09 11:15:50'),
('8831236972eb5c344e137c96de4a64ef249b09b56de5a94d66255a0a275364b841f74b05267fe294', 118, 9, 'TutsForWeb', '[]', 0, '2020-05-14 13:37:09', '2020-05-14 13:37:09', '2020-11-14 16:37:08'),
('89677ca60cbd5137cad6be7a566015d51b6b90b3ef2af00e59fb2edfe3c3b6737cd972227382f740', 122, 7, 'TutsForWeb', '[]', 0, '2020-04-09 14:17:19', '2020-04-09 14:17:19', '2020-10-09 17:17:19'),
('8a048efd7fb89947008f31a47db67cd26e8293dae45d674bdf45b993312d544be4be2e418f9204d4', 118, 7, 'TutsForWeb', '[]', 0, '2020-05-07 19:25:07', '2020-05-07 19:25:07', '2020-11-07 22:25:06'),
('8d97e1266dd0db13a6d55fd6442189e93b6e86513b82e213f59525a3ad771f8099995598195cb471', 7, 7, 'TutsForWeb', '[]', 0, '2020-03-09 08:12:01', '2020-03-09 08:12:01', '2020-09-09 11:12:01'),
('8f87c6494d77fae2d0f7593910a7618479191b3f48aae1a6d02fe0cfd07ac7887753c970f367815e', 7, 7, 'TutsForWeb', '[]', 0, '2020-03-02 10:35:19', '2020-03-02 10:35:19', '2020-09-02 13:35:19'),
('90aab46eb236b182a52fc5ba6aeace4c96fba15682c680e17aba1d5cf70ac87addf57289750887c0', 7, 7, 'TutsForWeb', '[]', 0, '2020-03-09 08:14:53', '2020-03-09 08:14:53', '2020-09-09 11:14:53'),
('93fb65b5748799de12474f24d1edf495d10597fe8a412e1d1ac6301c863ccfe6d23758425c9a9d6c', 7, 7, 'TutsForWeb', '[]', 0, '2020-03-09 08:20:36', '2020-03-09 08:20:36', '2020-09-09 11:20:35'),
('96244d4536b67c9a9e11eb0250e59a5c43f5935a2d540952964ec690a821dbd45f1fb9fdca44e9c4', 7, 7, 'TutsForWeb', '[]', 0, '2020-02-26 06:44:55', '2020-02-26 06:44:55', '2020-08-26 09:44:54'),
('96b8771b7c44b0e14f573b90e769332383bed5ea359564c10c75fb8338d27b98423888c58c129bc0', 7, 7, 'TutsForWeb', '[]', 0, '2020-03-02 10:38:07', '2020-03-02 10:38:07', '2020-09-02 13:38:07'),
('97b852ba150a1e54294056c4901687fefe9c3fd1d1d0396c57bf4a3581a9d414e84e701d65302367', 16, 7, 'TutsForWeb', '[]', 0, '2020-03-03 11:06:37', '2020-03-03 11:06:37', '2020-09-03 14:06:37'),
('9fd57f94b71daae04b6e50f7e55a1f0fe44065f09d26a96d8cedb65161990b87fe1656e2e0d76702', 7, 7, 'TutsForWeb', '[]', 0, '2020-03-09 08:12:23', '2020-03-09 08:12:23', '2020-09-09 11:12:23'),
('a0d5f4ecef8427c33dfc8ab17542a4748db8588c790ecf96954ca649167f4a8ac24fa24231e72d06', 7, 7, 'TutsForWeb', '[]', 0, '2020-02-26 06:55:25', '2020-02-26 06:55:25', '2020-08-26 09:55:24'),
('a5728eb361f950ecb84d6141e206b490155b6f8495e6099810ec8da95c6f712de375d3319a580eec', 123, 7, 'TutsForWeb', '[]', 0, '2020-04-24 22:49:13', '2020-04-24 22:49:13', '2020-10-25 01:49:12'),
('a5f0f5c66068a2683cfdba3cba9e7f7d756454f0a995f2eb9e7ecaa9abae6d6c02f1f3af0a435f4d', 7, 7, 'TutsForWeb', '[]', 0, '2020-03-09 08:22:52', '2020-03-09 08:22:52', '2020-09-09 11:22:51'),
('a5f2c2e5fec72d201426d3ba63d2cdb5a3ef9e1e7cd185c587a5a1e856084818ab05f0d78733891b', 7, 7, 'TutsForWeb', '[]', 0, '2020-03-04 12:34:14', '2020-03-04 12:34:14', '2020-09-04 15:34:13'),
('a91e8370f7782ad5105bdc5aae6113b36a840bfee2ddc3d620441e1be5efc7ebacb15020e7097a2c', 7, 7, 'TutsForWeb', '[]', 0, '2020-03-09 08:08:51', '2020-03-09 08:08:51', '2020-09-09 11:08:51'),
('ae6a731482b229a9c8c82ebdd3c0479a15a01a51ba193b2e92f914b6831148d5dbe2a4eb12cc8ef3', 7, 7, 'TutsForWeb', '[]', 0, '2020-03-09 08:23:58', '2020-03-09 08:23:58', '2020-09-09 11:23:58'),
('afe122bd5fea6644adefb0a07dba503fad2a2228bb9d7e8bf5cfcd1937db04a0041fb7bfba17851f', 118, 9, 'TutsForWeb', '[]', 0, '2020-05-13 19:06:29', '2020-05-13 19:06:29', '2020-11-13 22:06:29'),
('b001cdda132f0fcaa4f9e184962e3f533a1a6add93817611e7faca1070454afb4253f33d18690207', 7, 7, 'TutsForWeb', '[]', 0, '2020-02-26 12:37:35', '2020-02-26 12:37:35', '2020-08-26 15:37:34'),
('b0755e51b16323a33d78da4d0f1d58c1c1be3db049d0da4abb5ba5ee26e26a5d7c1b624d820e7d7e', 122, 7, 'TutsForWeb', '[]', 0, '2020-04-09 10:24:57', '2020-04-09 10:24:57', '2020-10-09 13:24:57'),
('b1f6bfd8250d6b1e10be9574362bd90a009d3b42085d248a81ded3cbf014bc2a260fa28d276afaef', 124, 9, 'TutsForWeb', '[]', 0, '2020-05-13 18:25:27', '2020-05-13 18:25:27', '2020-11-13 21:25:27'),
('b8712f2ce017275e81a396132d0cdcc9aa464046bb02a8f9dbdc3e76791957db4718c96acf58c7d7', 7, 7, 'TutsForWeb', '[]', 0, '2020-02-28 05:31:35', '2020-02-28 05:31:35', '2020-08-28 08:31:35'),
('bd58c654157ab24d65a3fd2c8f1b27684f9b43114b77f29626c6b524c703118b7f5d369cbb19ff35', 11, 7, 'TutsForWeb', '[]', 0, '2020-02-26 09:07:03', '2020-02-26 09:07:03', '2020-08-26 12:07:03'),
('c816f5c552ea9b6145322d5c5df479b889ab6929e920ee2423970c5d80911786f1e6a3d438fe39b2', 120, 7, 'TutsForWeb', '[]', 0, '2020-04-09 09:44:18', '2020-04-09 09:44:18', '2020-10-09 12:44:18'),
('ca36c8b8b2ba4d75a51975d8814000bbb10e1917b988922b89bf9f0338cde4e2f01bd88cb93716c5', 7, 7, 'TutsForWeb', '[]', 0, '2020-03-02 05:58:08', '2020-03-02 05:58:08', '2020-09-02 08:58:08'),
('d9026165408569d134aa1e0801480b741b58b8bbf7f61b3f7d12dcfd0f7516999a6436dfd8093d01', 7, 7, 'TutsForWeb', '[]', 0, '2020-02-28 05:31:15', '2020-02-28 05:31:15', '2020-08-28 08:31:15'),
('dad9a6b53a4924e6c23436f65cdb875d07a53d95a54216d7329084dce0d07c7ec1a9addd902c1fa4', 7, 7, 'TutsForWeb', '[]', 0, '2020-03-09 08:12:21', '2020-03-09 08:12:21', '2020-09-09 11:12:20'),
('e6e5ee94c6b6b24ea3551cea903453d6c79fec78e7054a52a2e8ac3391daca3643c02d0a59910b3f', 7, 7, 'TutsForWeb', '[]', 0, '2020-03-04 11:34:45', '2020-03-04 11:34:45', '2020-09-04 14:34:44'),
('e9ffc5e270f557ed4a4e6569037516f81e69400654a1c3e48a1e925cabb7bedf328f3ac65b5a9840', 118, 7, 'TutsForWeb', '[]', 0, '2020-04-09 17:15:13', '2020-04-09 17:15:13', '2020-10-09 20:15:13'),
('ebf66224bea692b9be68ca8942875a34e103729472c98d2dfd18499215d94b902b949b685400d3da', 118, 7, 'TutsForWeb', '[]', 0, '2020-05-06 19:22:12', '2020-05-06 19:22:12', '2020-11-06 22:22:09'),
('ecc41f843a86dfa22add3a58185ecf50c3e7c218ce5a99767e72131d55a5b207ff446990c5fa392c', 7, 7, 'TutsForWeb', '[]', 0, '2020-03-09 08:14:35', '2020-03-09 08:14:35', '2020-09-09 11:14:35'),
('f1d831cfc1050f9cdc77585edfacd1f09baeccc1111e4df6aa37e63ad24941e27a62c5dcf8dba3aa', 7, 7, 'TutsForWeb', '[]', 0, '2020-02-26 06:44:36', '2020-02-26 06:44:36', '2020-08-26 09:44:36'),
('f332e54b586ca3d71e3991af91f23095f091c27de58388242eca84e87a960ff7fa93fbc03bcc6cf1', 7, 7, 'TutsForWeb', '[]', 0, '2020-03-03 11:06:30', '2020-03-03 11:06:30', '2020-09-03 14:06:30'),
('f883c5c36c7c3684b25d63656baa69b7bb8d16b429110dd5c8c70cacdae5080e83ea5551c25de083', 7, 7, 'TutsForWeb', '[]', 0, '2020-04-07 16:41:55', '2020-04-07 16:41:55', '2020-10-07 19:41:55'),
('f9375011b1bc0f86268b7c9828a2717e13a41ec670f3aab95a8840605c6a902ae03b62f238790c8f', 118, 7, 'TutsForWeb', '[]', 0, '2020-04-09 09:46:20', '2020-04-09 09:46:20', '2020-10-09 12:46:20'),
('fdc6c2bda7504282f555669fb70a299b3fbfbcb188219286b5e9aa56f494145858aae02e8ba4d63b', 7, 7, 'TutsForWeb', '[]', 0, '2020-02-26 06:44:19', '2020-02-26 06:44:19', '2020-08-26 09:44:19');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(7, NULL, 'Laravel Personal Access Client', 'U6QavUPBiuRYDWxWv68f5dzPyARo2R5VzFDmbfa1', 'http://localhost', 1, 0, 0, '2020-02-25 09:02:38', '2020-02-25 09:02:38'),
(8, NULL, 'Laravel Password Grant Client', 'eRFZrmVHUFec1RgHDKdbWS6CQ2kuF89mWYhXpAL6', 'http://localhost', 0, 1, 0, '2020-02-25 09:02:38', '2020-02-25 09:02:38'),
(9, NULL, 'Laravel Personal Access Client', '7Wq83Ct4Oa1jV8rJLnEAJzELM5pbSzQgSuATHHy8', 'http://localhost', 1, 0, 0, '2020-05-13 13:27:19', '2020-05-13 13:27:19'),
(10, NULL, 'Laravel Password Grant Client', 'CUFYj9MDTsrJd8rhS8qU7cZaUZBwaeyTxLbeWdC3', 'http://localhost', 0, 1, 0, '2020-05-13 13:27:19', '2020-05-13 13:27:19'),
(11, NULL, 'Laravel Personal Access Client', '7G1JqTls5VX8UcyzmuAFXJIj8LhKunB2b3SGHEQ0', 'http://localhost', 1, 0, 0, '2020-05-21 16:16:09', '2020-05-21 16:16:09'),
(12, NULL, 'Laravel Password Grant Client', 'ymU7eYbZcquyXfWPqqqicdPiVpUPNOsAj5EtljNp', 'http://localhost', 0, 1, 0, '2020-05-21 16:16:09', '2020-05-21 16:16:09');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-02-25 09:00:47', '2020-02-25 09:00:47'),
(2, 3, '2020-02-25 09:01:54', '2020-02-25 09:01:54'),
(3, 5, '2020-02-25 09:02:08', '2020-02-25 09:02:08'),
(4, 7, '2020-02-25 09:02:38', '2020-02-25 09:02:38'),
(5, 9, '2020-05-13 13:27:19', '2020-05-13 13:27:19'),
(6, 11, '2020-05-21 16:16:09', '2020-05-21 16:16:09');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `payment_id` bigint(20) DEFAULT NULL,
  `coupon_id` bigint(20) DEFAULT NULL,
  `tracking` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `city_id` bigint(20) DEFAULT NULL,
  `county_id` bigint(20) DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fcity_id` bigint(20) DEFAULT NULL,
  `fcounty_id` bigint(20) DEFAULT NULL,
  `faddress` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dietitian_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `checkbox` tinyint(1) NOT NULL DEFAULT 1,
  `cc_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiration_month` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiration_year` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cvc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `payment_id`, `coupon_id`, `tracking`, `status`, `city_id`, `county_id`, `address`, `fcity_id`, `fcounty_id`, `faddress`, `name`, `email`, `dietitian_code`, `phone`, `price`, `checkbox`, `cc_name`, `card_number`, `expiration_month`, `expiration_year`, `cvc`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, '20015NnxJ04020559', 0, 1, 2, 'Bağlar Mh Hakan Kurtulan Sk No 8/3', NULL, NULL, NULL, 'Laith Atik', 'laythateek@gmail.com', 'wfZf5zaV3x', '05348447726', 0.10, 1, 'LAith ATIK', '5355761528128256', '08', '23', '252', '2020-01-14 19:39:53', '2020-01-14 19:39:53'),
(2, 2, NULL, '20015Ns2G04021496', 0, NULL, NULL, 'Bağlar Mh Hakan Kurtulan Sk No 8/3', NULL, NULL, NULL, 'Laith Atik', 'laythateek@gmail.com', 'wfZf5zaV3x', '05348447726', 0.10, 1, 'LAITH ATIK', '5355761528128256', '08', '23', '252', '2020-01-14 19:44:55', '2020-01-14 19:44:55'),
(3, 3, NULL, '20015RvwH04017251', 0, NULL, NULL, 'Bağlar Mh Hakan Kurtulan Sk No 8/3', NULL, NULL, NULL, 'Laith Atik', 'laythateek@gmail.com', 'wfZf5zaV3x', '05348447726', 0.10, 1, 'LAith ATIK', '5355761528128256', '08', '23', '252', '2020-01-14 23:47:50', '2020-01-14 23:47:50'),
(10, 10, 20, '20016MGTG04017560', 0, NULL, NULL, 'Bağlar Mh Hakan Kurtulan Sk No 8/3', NULL, NULL, NULL, 'Laith Atik', 'laythateek@gmail.com', '123321', '05348447726', 0.10, 1, 'LAITH ATIK', '5355761528128256', '08', '23', '252', '2020-01-16 07:44:47', '2020-01-16 07:44:47'),
(15, 16, 2, '20021S6dB04019532', 0, NULL, NULL, 'Bağlar Mh Hakan Kurtulan Sk No 8/3', NULL, NULL, NULL, 'Laith Atik', 'laythateek@gmail.com', NULL, '05348447726', 0.10, 1, 'LAITH ATIK', '5355761528128256', '08', '23', '252', '2020-01-21 10:05:26', '2020-01-21 10:05:26'),
(16, 17, 16, '20050LPEE04023565', 0, NULL, NULL, 'Bağlar Mh Hakan Kurtulan Sk No 8/3', NULL, NULL, NULL, 'Laith Atik', 'laythateek@gmail.com', 'awdawd', '+905348447726', 0.10, 1, 'LAith ATIK', '5355761528128256', '08', '23', '252', '2020-02-19 05:15:06', '2020-02-19 05:15:06'),
(17, 18, NULL, '20051OcYH04028884', 0, NULL, NULL, 'Bağlar Mh Hakan Kurtulan Sk No 8/3', NULL, NULL, NULL, 'Laith Atik', 'laythateek@gmail.com', NULL, '05348447726', 0.10, 1, 'LAith ATIK', '5355761528128256', '08', '23', '252', '2020-02-20 08:28:27', '2020-02-20 08:28:27'),
(18, 19, NULL, '20059QIYE04010332', 0, NULL, NULL, 'Bağlar Mh Hakan Kurtulan Sk No 8/3', NULL, NULL, NULL, 'Laith Atik', 'laythateek@gmail.com', NULL, '05348447726', 0.10, 1, 'LAith ATIK', '5355761528128256', '08', '23', '252', '2020-02-28 10:08:31', '2020-02-28 10:08:31'),
(19, 20, NULL, '20059ToEJ04013325', 0, NULL, NULL, 'Bağlar Mh Hakan Kurtulan Sk No 8/3', NULL, NULL, NULL, 'Laith Atik', 'laythateek@gmail.com', NULL, '05348447726', 0.10, 1, 'LAith ATIK', '5355761528128256', '08', '23', '252', '2020-02-28 13:40:06', '2020-02-28 13:40:06'),
(20, 21, NULL, '20062KoND04019099', 0, NULL, NULL, 'Bağlar Mh Hakan Kurtulan Sk No 8/3', NULL, NULL, NULL, 'Laith Atik', 'laythateek@gmail.com', NULL, '05348447726', 3.00, 1, 'ARIF KAZANCI', '5406699689186011', '02', '23', '409', '2020-03-02 04:40:15', '2020-03-02 04:40:15'),
(21, 25, NULL, '20092XLuE04029006', 0, 1, 1, 'Bağlar Mh Hakan Kurtulan Sk No 8/3', 19, 182, 'Adresiniz', 'Laith Atik', 'laythateek@gmail.com', NULL, '+905348447726', 0.10, 1, 'LAITH ATIK', '4836124760112417', '01', '24', '153', '2020-04-01 17:14:06', '2020-04-01 17:14:06'),
(22, 28, NULL, '20092XeoD04021052', 0, 67, 755, 'Bağlar Mh Hakan Kurtulan Sk No 8/3', NULL, NULL, NULL, 'Laith Atik', 'laythateek@gmail.com', 'laith atik', '+905348447726', 0.10, 1, 'LAITH ATIK', '4836124760112417', '01', '24', '153', '2020-04-01 17:33:20', '2020-04-01 17:33:20'),
(23, 29, 21, '20093SEsH04026728', 0, 37, 386, 'Bağlar Mh Hakan Kurtulan Sk No 8/3', NULL, NULL, NULL, 'Laith Atik', 'laythateek@gmail.com', NULL, '+905348447726', 0.10, 1, 'LAith ATIK', '4836124760112417', '01', '24', '153', '2020-04-02 12:04:45', '2020-04-02 12:04:45'),
(24, 30, 22, '20097Mt8E04011117', 0, 67, 755, 'Bağlar Mh Hakan Kurtulan Sk No 8/3', 1, 3, 'laith atik', 'Laith Atik', 'laythateek@gmail.com', NULL, '+905348447726', 0.39, 1, 'LAITH ATIK', '4836124760112417', '01', '24', '153', '2020-05-23 09:46:01', '2020-04-06 09:46:01');

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`id`, `order_id`, `product_id`, `name`, `quantity`, `price`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Çift Paketi', 1, 1099.89, '2020-01-14 19:39:53', '2020-01-14 19:39:53'),
(2, 2, 2, 'Tekli Paket', 1, 267.5, '2020-01-14 19:44:55', '2020-01-14 19:44:55'),
(3, 2, 1, 'Çift Paketi', 1, 1099.89, '2020-01-14 19:44:56', '2020-01-14 19:44:56'),
(4, 3, 2, 'Tekli Paket', 5, 267.5, '2020-01-14 23:47:50', '2020-01-14 23:47:50'),
(5, 3, 1, 'Çift Paketi', 7, 1099.89, '2020-01-14 23:47:50', '2020-01-14 23:47:50'),
(12, 7, 3, 'Üçlü Diyet Paketi', 4, 2925, '2020-01-16 00:03:58', '2020-01-16 00:03:58'),
(13, 7, 2, 'Tekli Paket', 6, 1299, '2020-01-16 00:03:58', '2020-01-16 00:03:58'),
(14, 7, 1, 'Çift Paketi', 3, 2295, '2020-01-16 00:03:58', '2020-01-16 00:03:58'),
(16, 10, 3, 'Üçlü Diyet Paketi', 4, 2925, '2020-01-16 07:44:47', '2020-01-16 07:44:47'),
(17, 10, 2, 'Tekli Paket', 6, 1299, '2020-01-16 07:44:47', '2020-01-16 07:44:47'),
(18, 10, 1, 'Çift Paketi', 3, 2295, '2020-01-16 07:44:47', '2020-01-16 07:44:47'),
(28, 15, 3, 'Üçlü Diyet Paketi', 4, 2925, '2020-01-21 10:05:26', '2020-01-21 10:05:26'),
(29, 15, 1, 'Çift Paketi', 5, 2295, '2020-01-21 10:05:26', '2020-01-21 10:05:26'),
(30, 15, 2, 'Tekli Paket', 4, 1299, '2020-01-21 10:05:26', '2020-01-21 10:05:26'),
(31, 16, 2, 'Tekli Paket', 1, 1299, '2020-02-19 05:15:06', '2020-02-19 05:15:06'),
(32, 17, 87, 'Dörtlü Takip Paketi', 1, 3825, '2020-02-20 08:28:27', '2020-02-20 08:28:27'),
(33, 18, 2, 'Tekli Paket', 1, 1299, '2020-02-28 10:08:31', '2020-02-28 10:08:31'),
(34, 19, 2, 'Tekli Paket', 1, 1299, '2020-02-28 13:40:06', '2020-02-28 13:40:06'),
(35, 20, 2, 'Tekli Paket', 1, 1299, '2020-03-02 04:40:15', '2020-03-02 04:40:15'),
(36, 21, 87, 'Dörtlü Takip Paketi', 1, 3060, '2020-04-01 17:14:06', '2020-04-01 17:14:06'),
(37, 22, 2, 'Tekli Paket', 1, 1299, '2020-04-01 17:33:20', '2020-04-01 17:33:20'),
(38, 23, 2, 'Tekli Paket', 10, 1299, '2020-04-02 12:04:46', '2020-04-02 12:04:46'),
(39, 24, 2, 'Tekli Paket', 3, 1299, '2020-04-06 09:46:01', '2020-04-06 09:46:01');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `eee` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ReturnOid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `TRANID` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PAResSyntaxOK` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Filce` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firmaadi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `islemtipi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `merchantID` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `maskedCreditCard` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sID` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ACQBIN` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `itemnumber1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Ecom_Payment_Card_ExpDate_Year` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `EXTRA_CARDBRAND` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `storekey` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MaskedPan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `acqStan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Fadres` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `EXTRA_CAVVRESULTCODE` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `clientIp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iReqDetail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `okUrl` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `md` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ProcReturnCode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payResults_dsId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendorCode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `taksit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `TransId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `EXTRA_TRXDATE` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `productcode1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Fil` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Ecom_Payment_Card_ExpDate_Month` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `storetype` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iReqCode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Response` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tulkekod` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SettleId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mdErrorMsg` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ErrMsg` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `EXTRA_SGKTUTAR` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PAResVerified` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cavv` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `digest` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `HostRefNum` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `callbackCall` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AuthCode` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `failUrl` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cavvAlgorithm` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Fpostakodu` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `faturaFirma` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `xid` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `encoding` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `oid` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mdStatus` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dsId` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `eci` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `version` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Fadres2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `EXTRA_CARDHOLDERNAME` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Fismi` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `EXTRA_CARDISSUER` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `clientid` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `txstatus` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_charset_` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `HASH` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rnd` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `HASHPARAMS` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `HASHPARAMSVAL` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `ReturnOid`, `TRANID`, `PAResSyntaxOK`, `Filce`, `firmaadi`, `islemtipi`, `total1`, `lang`, `merchantID`, `maskedCreditCard`, `amount`, `sID`, `ACQBIN`, `itemnumber1`, `Ecom_Payment_Card_ExpDate_Year`, `EXTRA_CARDBRAND`, `storekey`, `MaskedPan`, `acqStan`, `Fadres`, `EXTRA_CAVVRESULTCODE`, `clientIp`, `iReqDetail`, `okUrl`, `md`, `ProcReturnCode`, `payResults_dsId`, `vendorCode`, `taksit`, `TransId`, `EXTRA_TRXDATE`, `productcode1`, `Fil`, `Ecom_Payment_Card_ExpDate_Month`, `storetype`, `iReqCode`, `Response`, `tulkekod`, `SettleId`, `mdErrorMsg`, `ErrMsg`, `EXTRA_SGKTUTAR`, `PAResVerified`, `cavv`, `id1`, `digest`, `HostRefNum`, `callbackCall`, `AuthCode`, `failUrl`, `cavvAlgorithm`, `Fpostakodu`, `price1`, `faturaFirma`, `xid`, `encoding`, `oid`, `mdStatus`, `dsId`, `eci`, `version`, `Fadres2`, `EXTRA_CARDHOLDERNAME`, `Fismi`, `qty1`, `desc1`, `EXTRA_CARDISSUER`, `clientid`, `txstatus`, `_charset_`, `HASH`, `rnd`, `HASHPARAMS`, `HASHPARAMSVAL`, `created_at`, `updated_at`) VALUES
(1, 'ORDER-20015NnxJ04020559', NULL, 'true', 'Şişli', 'EnBiosis', 'Auth', '7.50', 'tr', '400940535', '5355 76** **** 8256', '0.10', '2', '510138', 'a1', '23', 'MASTERCARD', 'ENDIET19', '535576***8256', '000036', 'Harbiye, Atiye Sokak. Kat: 2 No:8, Şişli/Istanbul', '2', '85.101.203.12', NULL, 'http://localhost/enbiosis/public/checkout-result', '535576:3636BD4903F711BE60AE4A166234EA199E6D2720451D21E2BF048446333B5ECE:3104:##400940535', '00', '2', NULL, NULL, '20015NnxJ04020561', '20200115 13:39:49', 'a2', 'Istanbul', '08', '3d_pay', NULL, 'Approved', 'tr', '97', 'Authenticated', NULL, '0.1', 'false', 'jOfWN9WLoNcoCREARmUgARMAAAA=', 'a5', 'digest', '001513920897', 'true', '267243', 'http://localhost/enbiosis/public/checkout-result', '3', 'postakod34367', '6.25', 'faturaFirma', 'sJOTd4OLyXbmD+mPjIifZChN0Bo=', 'ISO-8859-9', NULL, '1', '2', '02', '2.0', 'XXX', 'LA* AT*', 'is', '3', 'a4 desc', 'T. VAKIFLAR BANKASI T.A.O.', '400940535', 'Y', 'UTF-8', 'jse3NfiMxWKbuW1n7LprpAwouDs=', 'ucPoMmm4p5O3pF2Av4ml', 'clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:', '40094053526724300Approved1jOfWN9WLoNcoCREARmUgARMAAAA=02535576:3636BD4903F711BE60AE4A166234EA199E6D2720451D21E2BF048446333B5ECE:3104:##400940535ucPoMmm4p5O3pF2Av4ml', '2020-01-14 19:39:53', '2020-01-14 19:39:53'),
(2, 'ORDER-20015Ns2G04021496', NULL, 'true', 'Şişli', 'EnBiosis', 'Auth', '7.50', 'tr', '400940535', '5355 76** **** 8256', '0.10', '2', '510138', 'a1', '23', 'MASTERCARD', 'ENDIET19', '535576***8256', '000037', 'Harbiye, Atiye Sokak. Kat: 2 No:8, Şişli/Istanbul', '2', '85.101.203.12', NULL, 'http://localhost/enbiosis/public/checkout-result', '535576:F9FEC158100AAA89B72232B20FB62D0E0DB52BBAFF54B3AA855150AC2AB2CBA2:3980:##400940535', '00', '2', NULL, NULL, '20015Ns2G04021498', '20200115 13:44:52', 'a2', 'Istanbul', '08', '3d_pay', NULL, 'Approved', 'tr', '97', 'Authenticated', NULL, '0.1', 'false', 'jOfWN9WLoNcoCREAR07uBQYAAAA=', 'a5', 'digest', '001513921299', 'true', '271480', 'http://localhost/enbiosis/public/checkout-result', '3', 'postakod34367', '6.25', 'faturaFirma', '8n39VD9bTptJpYt+fkDDG56DAA0=', 'ISO-8859-9', NULL, '1', '2', '02', '2.0', 'XXX', 'LA* AT*', 'is', '3', 'a4 desc', 'T. VAKIFLAR BANKASI T.A.O.', '400940535', 'Y', 'UTF-8', '0077rY+AkfdodqY50AQhKxxNwnE=', '+K1PhZVBhjbur3u4NNdi', 'clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:', '40094053527148000Approved1jOfWN9WLoNcoCREAR07uBQYAAAA=02535576:F9FEC158100AAA89B72232B20FB62D0E0DB52BBAFF54B3AA855150AC2AB2CBA2:3980:##400940535+K1PhZVBhjbur3u4NNdi', '2020-01-14 19:44:55', '2020-01-14 19:44:55'),
(3, 'ORDER-20015RvwH04017251', NULL, 'true', 'Şişli', 'EnBiosis', 'Auth', '7.50', 'tr', '400940535', '5355 76** **** 8256', '0.10', '2', '510138', 'a1', '23', 'MASTERCARD', 'ENDIET19', '535576***8256', '000038', 'Harbiye, Atiye Sokak. Kat: 2 No:8, Şişli/Istanbul', '2', '85.101.203.12', NULL, 'http://localhost/enbiosis/public/checkout-result', '535576:EC80A5ED7A617F2DB6CC204D7E5F274B6AA591166A701B4F5A178EA4FBEF0B93:3816:##400940535', '00', '2', NULL, NULL, '20015RvwH04017253', '20200115 17:47:48', 'a2', 'Istanbul', '08', '3d_pay', NULL, 'Approved', 'tr', '97', 'Authenticated', NULL, '0.1', 'false', 'jOfWN9WLoNcoCREAcnG+BQMAAAA=', 'a5', 'digest', '001517941537', 'true', '509710', 'http://localhost/enbiosis/public/checkout-result', '3', 'postakod34367', '6.25', 'faturaFirma', 'UmrpsGil3tmWmeu8oH9T33XCNis=', 'ISO-8859-9', NULL, '1', '2', '02', '2.0', 'XXX', 'LA* AT*', 'is', '3', 'a4 desc', 'T. VAKIFLAR BANKASI T.A.O.', '400940535', 'Y', 'UTF-8', '0usDI+xoB4Xyz/0Zdhev+eWRoyE=', 'MNpKpIE/CBj8Vf9GRxod', 'clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:', '40094053550971000Approved1jOfWN9WLoNcoCREAcnG+BQMAAAA=02535576:EC80A5ED7A617F2DB6CC204D7E5F274B6AA591166A701B4F5A178EA4FBEF0B93:3816:##400940535MNpKpIE/CBj8Vf9GRxod', '2020-01-14 23:47:50', '2020-01-14 23:47:50'),
(9, 'ORDER-20016MGTG04017560', NULL, 'true', 'Şişli', 'EnBiosis', 'Auth', '7.50', 'tr', '400940535', '5355 76** **** 8256', '0.10', '2', '510138', 'a1', '23', 'MASTERCARD', 'ENDIET19', '535576***8256', '000040', 'Harbiye, Atiye Sokak. Kat: 2 No:8, Şişli/Istanbul', '2', '78.186.63.129', NULL, 'http://localhost/enbiosis/public/checkout-result', '535576:E68D825E9EADFDFD2D696B28305C46854E7626068B3AF59DF94FF0B738A6CD2D:4134:##400940535', '00', '2', NULL, NULL, '20016MGTG04017562', '20200116 12:06:19', 'a2', 'Istanbul', '08', '3d_pay', NULL, 'Approved', 'tr', '98', 'Authenticated', NULL, '0.1', 'false', 'jOfWN9WLoNcoCREAz0CYCQYAAAA=', 'a5', 'digest', '001612997554', 'true', '134448', 'http://localhost/enbiosis/public/checkout-result', '3', 'postakod34367', '6.25', 'faturaFirma', '0PHqL1/218R576L8A3ewtRRw74A=', 'ISO-8859-9', NULL, '1', '2', '02', '2.0', 'XXX', 'LA* AT*', 'is', '3', 'a4 desc', 'T. VAKIFLAR BANKASI T.A.O.', '400940535', 'Y', 'UTF-8', '0688gId4yXDAXmRpn/pRJgAIWNY=', '22jWsZr07SPPg9UH0OXR', 'clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:', '40094053513444800Approved1jOfWN9WLoNcoCREAz0CYCQYAAAA=02535576:E68D825E9EADFDFD2D696B28305C46854E7626068B3AF59DF94FF0B738A6CD2D:4134:##40094053522jWsZr07SPPg9UH0OXR', '2020-01-16 06:58:33', '2020-01-16 06:58:33'),
(10, 'ORDER-20016MGTG04017560', NULL, 'true', 'Şişli', 'EnBiosis', 'Auth', '7.50', 'tr', '400940535', '5355 76** **** 8256', '0.10', '2', '510138', 'a1', '23', 'MASTERCARD', 'ENDIET19', '535576***8256', '000040', 'Harbiye, Atiye Sokak. Kat: 2 No:8, Şişli/Istanbul', '2', '78.186.63.129', NULL, 'http://localhost/enbiosis/public/checkout-result', '535576:E68D825E9EADFDFD2D696B28305C46854E7626068B3AF59DF94FF0B738A6CD2D:4134:##400940535', '00', '2', NULL, NULL, '20016MGTG04017562', '20200116 12:06:19', 'a2', 'Istanbul', '08', '3d_pay', NULL, 'Approved', 'tr', '98', 'Authenticated', NULL, '0.1', 'false', 'jOfWN9WLoNcoCREAz0CYCQYAAAA=', 'a5', 'digest', '001612997554', 'true', '134448', 'http://localhost/enbiosis/public/checkout-result', '3', 'postakod34367', '6.25', 'faturaFirma', '0PHqL1/218R576L8A3ewtRRw74A=', 'ISO-8859-9', NULL, '1', '2', '02', '2.0', 'XXX', 'LA* AT*', 'is', '3', 'a4 desc', 'T. VAKIFLAR BANKASI T.A.O.', '400940535', 'Y', 'UTF-8', '0688gId4yXDAXmRpn/pRJgAIWNY=', '22jWsZr07SPPg9UH0OXR', 'clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:', '40094053513444800Approved1jOfWN9WLoNcoCREAz0CYCQYAAAA=02535576:E68D825E9EADFDFD2D696B28305C46854E7626068B3AF59DF94FF0B738A6CD2D:4134:##40094053522jWsZr07SPPg9UH0OXR', '2020-01-16 07:44:47', '2020-01-16 07:44:47'),
(13, 'ORDER-20021S6dB04019532', NULL, 'true', 'Şişli', 'EnBiosis', 'Auth', '7.50', 'tr', '400940535', '5355 76** **** 8256', '0.10', '2', '510138', 'a1', '23', 'MASTERCARD', 'ENDIET19', '535576***8256', '000048', 'Harbiye, Atiye Sokak. Kat: 2 No:8, Şişli/Istanbul', '2', '78.186.63.129', NULL, 'http://localhost/enbiosis/public/checkout-result', '535576:65F52C2982178102A01C71B3FE5CA5638B699CF040013679637CF75EDA9C087A:3753:##400940535', '00', '2', NULL, NULL, '20021S6dB04019534', '20200121 18:56:29', 'a2', 'Istanbul', '08', '3d_pay', NULL, 'Approved', 'tr', '103', 'Authenticated', NULL, '0.1', 'false', 'jOfWN9WLoNcoCREDoMZJAhIAAAA=', 'a5', 'digest', '002118411592', 'true', '459495', 'http://localhost/enbiosis/public/checkout-result', '3', 'postakod34367', '6.25', 'faturaFirma', 'GOzHNPSjJhvhUuRliwl9IWwSKvw=', 'ISO-8859-9', NULL, '1', '2', '02', '2.0', 'XXX', 'LA* AT*', 'is', '3', 'a4 desc', 'T. VAKIFLAR BANKASI T.A.O.', '400940535', 'Y', 'UTF-8', 'Ibhdy27lkoazpwFLxLOjVhrX0GA=', 'N4Jl1qugYF7GNI5de6HH', 'clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:', '40094053545949500Approved1jOfWN9WLoNcoCREDoMZJAhIAAAA=02535576:65F52C2982178102A01C71B3FE5CA5638B699CF040013679637CF75EDA9C087A:3753:##400940535N4Jl1qugYF7GNI5de6HH', '2020-01-21 09:57:41', '2020-01-21 09:57:41'),
(16, 'ORDER-20021S6dB04019532', NULL, 'true', 'Şişli', 'EnBiosis', 'Auth', '7.50', 'tr', '400940535', '5355 76** **** 8256', '0.10', '2', '510138', 'a1', '23', 'MASTERCARD', 'ENDIET19', '535576***8256', '000048', 'Harbiye, Atiye Sokak. Kat: 2 No:8, Şişli/Istanbul', '2', '78.186.63.129', NULL, 'http://localhost/enbiosis/public/checkout-result', '535576:65F52C2982178102A01C71B3FE5CA5638B699CF040013679637CF75EDA9C087A:3753:##400940535', '00', '2', NULL, NULL, '20021S6dB04019534', '20200121 18:56:29', 'a2', 'Istanbul', '08', '3d_pay', NULL, 'Approved', 'tr', '103', 'Authenticated', NULL, '0.1', 'false', 'jOfWN9WLoNcoCREDoMZJAhIAAAA=', 'a5', 'digest', '002118411592', 'true', '459495', 'http://localhost/enbiosis/public/checkout-result', '3', 'postakod34367', '6.25', 'faturaFirma', 'GOzHNPSjJhvhUuRliwl9IWwSKvw=', 'ISO-8859-9', NULL, '1', '2', '02', '2.0', 'XXX', 'LA* AT*', 'is', '3', 'a4 desc', 'T. VAKIFLAR BANKASI T.A.O.', '400940535', 'Y', 'UTF-8', 'Ibhdy27lkoazpwFLxLOjVhrX0GA=', 'N4Jl1qugYF7GNI5de6HH', 'clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:', '40094053545949500Approved1jOfWN9WLoNcoCREDoMZJAhIAAAA=02535576:65F52C2982178102A01C71B3FE5CA5638B699CF040013679637CF75EDA9C087A:3753:##400940535N4Jl1qugYF7GNI5de6HH', '2020-01-21 10:05:26', '2020-01-21 10:05:26'),
(17, 'ORDER-20050LPEE04023565', NULL, 'true', 'Şişli', 'EnBiosis', 'Auth', '7.50', 'en', '400940535', '5355 76** **** 8256', '0.10', '2', '510138', 'a1', '23', 'MASTERCARD', 'ENDIET19', '535576***8256', '000064', 'Harbiye, Atiye Sokak. Kat: 2 No:8, Şişli/Istanbul', '2', '78.186.63.129', NULL, 'http://localhost/enbiosis/public/checkout-result', '535576:AB87BA48BD7842774CA4C54A2FF7B7B18692B5861B1C0E5C53482646C2AA18FF:3885:##400940535', '00', '2', NULL, NULL, '20050LPEF04023567', '20200219 11:15:04', 'a2', 'Istanbul', '08', '3d_pay', NULL, 'Approved', 'tr', '132', 'Authenticated', NULL, '0.1', 'false', 'jOfWN9WLoNcoCREAcBXICEIAAAA=', 'a5', 'digest', '005011294272', 'true', '098852', 'http://localhost/enbiosis/public/checkout-result', '3', 'postakod34367', '6.25', 'faturaFirma', 'V8CLpEZjVa80yjARRYABCdhivoo=', 'ISO-8859-9', NULL, '1', '2', '02', '4.0', 'XXX', 'LA* AT*', 'is', '3', 'a4 desc', 'T. VAKIFLAR BANKASI T.A.O.', '400940535', NULL, 'UTF-8', 'PavXfOgO3d65X2CulBbj+ppVfAo=', 'hLd3s8R8Gr0y8vVqiLaf', 'clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:', '40094053509885200Approved1jOfWN9WLoNcoCREAcBXICEIAAAA=02535576:AB87BA48BD7842774CA4C54A2FF7B7B18692B5861B1C0E5C53482646C2AA18FF:3885:##400940535hLd3s8R8Gr0y8vVqiLaf', '2020-02-19 05:15:06', '2020-02-19 05:15:06'),
(18, 'ORDER-20051OcYH04028884', NULL, 'true', 'Şişli', 'EnBiosis', 'Auth', '7.50', 'en', '400940535', '5355 76** **** 8256', '0.10', '2', '510138', 'a1', '23', 'MASTERCARD', 'ENDIET19', '535576***8256', '000065', 'Harbiye, Atiye Sokak. Kat: 2 No:8, Şişli/Istanbul', '2', '78.186.63.129', NULL, 'http://localhost/enbiosis/public/checkout-result', '535576:882B0C5C208C89ACB3309F4BFAC866EAC3CA628E154429C82FA347C97B4D2EA0:3866:##400940535', '00', '2', NULL, NULL, '20051OcYI04028886', '20200220 14:28:24', 'a2', 'Istanbul', '08', '3d_pay', NULL, 'Approved', 'tr', '133', 'Authenticated', NULL, '0.1', 'false', 'jOfWN9WLoNcoCREBFvToBnAAAAA=', 'a5', 'digest', '005114365087', 'true', '245296', 'http://localhost/enbiosis/public/checkout-result', '3', 'postakod34367', '6.25', 'faturaFirma', 'ukP4woTBOYOAnwQTQXG7XgSDljc=', 'ISO-8859-9', NULL, '1', '2', '02', '4.0', 'XXX', 'LA* AT*', 'is', '3', 'a4 desc', 'T. VAKIFLAR BANKASI T.A.O.', '400940535', NULL, 'UTF-8', 'nnTUNtDymkyLHGVwdgS1XxPeHs4=', 'Qji61suIr6XS4VOpR5EV', 'clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:', '40094053524529600Approved1jOfWN9WLoNcoCREBFvToBnAAAAA=02535576:882B0C5C208C89ACB3309F4BFAC866EAC3CA628E154429C82FA347C97B4D2EA0:3866:##400940535Qji61suIr6XS4VOpR5EV', '2020-02-20 08:28:27', '2020-02-20 08:28:27'),
(19, 'ORDER-20059QIYE04010332', NULL, 'true', 'Şişli', 'EnBiosis', 'Auth', '7.50', 'en', '400940535', '5355 76** **** 8256', '0.10', '2', '510138', 'a1', '23', 'MASTERCARD', 'ENDIET19', '535576***8256', '000091', 'Harbiye, Atiye Sokak. Kat: 2 No:8, Şişli/Istanbul', '2', '78.186.63.129', NULL, 'http://localhost/enbiosis/public/checkout-result', '535576:BB94B5753F3191EA7B4C7C770312EAD629FCF546DA49F8F76CC086861D8819E8:4419:##400940535', '00', '2', NULL, NULL, '20059QIYE04010334', '20200228 16:08:24', 'a2', 'Istanbul', '08', '3d_pay', NULL, 'Approved', 'tr', '141', 'Authenticated', NULL, '0.1', 'false', 'jOfWN9WLoNcoCREFKKzzAGIAAAA=', 'a5', 'digest', '005916825047', 'true', '313346', 'http://localhost/enbiosis/public/checkout-result', '3', 'postakod34367', '6.25', 'faturaFirma', 'Ow3WaLC89eXW1o000Vo8KZtGbOI=', 'ISO-8859-9', NULL, '1', '2', '02', '2.0', 'XXX', 'LA* AT*', 'is', '3', 'a4 desc', 'T. VAKIFLAR BANKASI T.A.O.', '400940535', 'Y', 'UTF-8', 'pGkKM/rXiYgO5sS8BV3th/B6Tbs=', '+0fF1tjCw+9eY/MoONsc', 'clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:', '40094053531334600Approved1jOfWN9WLoNcoCREFKKzzAGIAAAA=02535576:BB94B5753F3191EA7B4C7C770312EAD629FCF546DA49F8F76CC086861D8819E8:4419:##400940535+0fF1tjCw+9eY/MoONsc', '2020-02-28 10:08:31', '2020-02-28 10:08:31'),
(20, 'ORDER-20059ToEJ04013325', NULL, 'true', 'Şişli', 'EnBiosis', 'Auth', '7.50', 'en', '400940535', '5355 76** **** 8256', '0.10', '2', '510138', 'a1', '23', 'MASTERCARD', 'ENDIET19', '535576***8256', '000094', 'Harbiye, Atiye Sokak. Kat: 2 No:8, Şişli/Istanbul', '2', '78.186.63.129', NULL, 'http://localhost/enbiosis/public/checkout-result', '535576:045228D1324B66E59ACE864ACE0AA0281554A1600F24F8474F1746C7072F7AEC:3290:##400940535', '00', '2', NULL, NULL, '20059ToEJ04013327', '20200228 19:40:04', 'a2', 'Istanbul', '08', '3d_pay', NULL, 'Approved', 'tr', '141', 'Authenticated', NULL, '0.1', 'false', 'jOfWN9WLoNcoCREFRHs1AUcAAAA=', 'a5', 'digest', '005919841913', 'true', '520535', 'http://localhost/enbiosis/public/checkout-result', '3', 'postakod34367', '6.25', 'faturaFirma', '5nTGBaI+KDt4a0zT/XpjylMJBJk=', 'ISO-8859-9', NULL, '1', '2', '02', '2.0', 'XXX', 'LA* AT*', 'is', '3', 'a4 desc', 'T. VAKIFLAR BANKASI T.A.O.', '400940535', 'Y', 'UTF-8', 'V6EtoF8iNn+VO48S6cbvax+S09I=', 'H7A4E3MsOEwvRWl84abX', 'clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:', '40094053552053500Approved1jOfWN9WLoNcoCREFRHs1AUcAAAA=02535576:045228D1324B66E59ACE864ACE0AA0281554A1600F24F8474F1746C7072F7AEC:3290:##400940535H7A4E3MsOEwvRWl84abX', '2020-02-28 13:40:06', '2020-02-28 13:40:06'),
(21, 'ORDER-20062KoND04019099', NULL, 'true', 'Şişli', 'EnBiosis', 'Auth', '7.50', 'tr', '400940535', '5406 69** **** 6011', '3', '2', '510138', 'a1', '23', 'MASTERCARD', 'ENDIET19', '540669***6011', '000097', 'Harbiye, Atiye Sokak. Kat: 2 No:8, Şişli/Istanbul', '2', '78.186.63.129', NULL, 'http://localhost/enbiosis/public/checkout-result', '540669:50EF8ABDA1ADCD5B066592A4B204F1BC888EA4D8C2E4E1B00B9FB474EF0E6F4E:4693:##400940535', '00', '2', NULL, '2', '20062KoNE04019101', '20200302 10:40:13', 'a2', 'Istanbul', '02', '3d_pay', NULL, 'Approved', 'tr', '144', 'Authenticated', NULL, '3.0', 'false', 'jAIBAwQDCQYHAwQBCQcFipLc2cw=', 'a5', 'digest', '006210969613', 'true', '272838', 'http://localhost/enbiosis/public/checkout-result', '3', 'postakod34367', '6.25', 'faturaFirma', '4IexN+z3UQtY5iSj4Je85rYZfaI=', 'ISO-8859-9', NULL, '1', '2', '02', '2.0', 'XXX', NULL, 'is', '3', 'a4 desc', 'T. GARANTI BANKASI A.S.', '400940535', 'Y', 'UTF-8', 'cDHvNBz46hvfDYBWtwMjRY8LgoI=', '23ZAOUi/Noi1+PaacXyL', 'clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:', '40094053527283800Approved1jAIBAwQDCQYHAwQBCQcFipLc2cw=02540669:50EF8ABDA1ADCD5B066592A4B204F1BC888EA4D8C2E4E1B00B9FB474EF0E6F4E:4693:##40094053523ZAOUi/Noi1+PaacXyL', '2020-03-02 04:40:15', '2020-03-02 04:40:15'),
(22, 'ORDER-20092XLuE04029006', NULL, 'true', 'Şişli', 'EnBiosis', 'Auth', '7.50', 'tr', '400940535', '4836 12** **** 2417', '0.10', '1', '440293', 'a1', '24', 'VISA', 'ENDIET19', '483612***2417', '000109', 'Harbiye, Atiye Sokak. Kat: 2 No:8, Şişli/Istanbul', '2', '88.248.135.154', NULL, 'http://localhost/enbiosis/public/checkout-result', '483612:8582B3A825C939033027CCB4DEA4CB3EE14C9A017EA9DC5181DF7EC649DDCAFC:4506:##400940535', '00', '1', NULL, NULL, '20092XLuE04029008', '20200401 23:11:46', 'a2', 'Istanbul', '01', '3d_pay', NULL, 'Approved', 'tr', '175', 'Authenticated', NULL, '0.1', 'false', 'AAABCWmBZQAAAXZzaIFlAAAAAAA=', 'a5', 'digest', '009223854769', 'true', '440883', 'http://localhost/enbiosis/public/checkout-result', '2', 'postakod34367', '6.25', 'faturaFirma', 'LCUZrLb5zsjv8CPZmYREZsOSRTk=', 'UTF-8', NULL, '1', '1', '05', '4.0', 'XXX', 'LA* AT*', 'is', '3', 'a4 desc', 'T. VAKIFLAR BANKASI T.A.O.', '400940535', NULL, 'UTF-8', 'b2oNbRrxM67nXpD0ve/P4T5e8Ec=', 'NxCu543jPYfdUhsuS0dg', 'clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:', '40094053544088300Approved1AAABCWmBZQAAAXZzaIFlAAAAAAA=05483612:8582B3A825C939033027CCB4DEA4CB3EE14C9A017EA9DC5181DF7EC649DDCAFC:4506:##400940535NxCu543jPYfdUhsuS0dg', '2020-04-01 17:11:47', '2020-04-01 17:11:47'),
(23, 'ORDER-20092XLuE04029006', NULL, 'true', 'Şişli', 'EnBiosis', 'Auth', '7.50', 'tr', '400940535', '4836 12** **** 2417', '0.10', '1', '440293', 'a1', '24', 'VISA', 'ENDIET19', '483612***2417', '000109', 'Harbiye, Atiye Sokak. Kat: 2 No:8, Şişli/Istanbul', '2', '88.248.135.154', NULL, 'http://localhost/enbiosis/public/checkout-result', '483612:8582B3A825C939033027CCB4DEA4CB3EE14C9A017EA9DC5181DF7EC649DDCAFC:4506:##400940535', '00', '1', NULL, NULL, '20092XLuE04029008', '20200401 23:11:46', 'a2', 'Istanbul', '01', '3d_pay', NULL, 'Approved', 'tr', '175', 'Authenticated', NULL, '0.1', 'false', 'AAABCWmBZQAAAXZzaIFlAAAAAAA=', 'a5', 'digest', '009223854769', 'true', '440883', 'http://localhost/enbiosis/public/checkout-result', '2', 'postakod34367', '6.25', 'faturaFirma', 'LCUZrLb5zsjv8CPZmYREZsOSRTk=', 'UTF-8', NULL, '1', '1', '05', '4.0', 'XXX', 'LA* AT*', 'is', '3', 'a4 desc', 'T. VAKIFLAR BANKASI T.A.O.', '400940535', NULL, 'UTF-8', 'b2oNbRrxM67nXpD0ve/P4T5e8Ec=', 'NxCu543jPYfdUhsuS0dg', 'clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:', '40094053544088300Approved1AAABCWmBZQAAAXZzaIFlAAAAAAA=05483612:8582B3A825C939033027CCB4DEA4CB3EE14C9A017EA9DC5181DF7EC649DDCAFC:4506:##400940535NxCu543jPYfdUhsuS0dg', '2020-04-01 17:12:35', '2020-04-01 17:12:35'),
(24, 'ORDER-20092XLuE04029006', NULL, 'true', 'Şişli', 'EnBiosis', 'Auth', '7.50', 'tr', '400940535', '4836 12** **** 2417', '0.10', '1', '440293', 'a1', '24', 'VISA', 'ENDIET19', '483612***2417', '000109', 'Harbiye, Atiye Sokak. Kat: 2 No:8, Şişli/Istanbul', '2', '88.248.135.154', NULL, 'http://localhost/enbiosis/public/checkout-result', '483612:8582B3A825C939033027CCB4DEA4CB3EE14C9A017EA9DC5181DF7EC649DDCAFC:4506:##400940535', '00', '1', NULL, NULL, '20092XLuE04029008', '20200401 23:11:46', 'a2', 'Istanbul', '01', '3d_pay', NULL, 'Approved', 'tr', '175', 'Authenticated', NULL, '0.1', 'false', 'AAABCWmBZQAAAXZzaIFlAAAAAAA=', 'a5', 'digest', '009223854769', 'true', '440883', 'http://localhost/enbiosis/public/checkout-result', '2', 'postakod34367', '6.25', 'faturaFirma', 'LCUZrLb5zsjv8CPZmYREZsOSRTk=', 'UTF-8', NULL, '1', '1', '05', '4.0', 'XXX', 'LA* AT*', 'is', '3', 'a4 desc', 'T. VAKIFLAR BANKASI T.A.O.', '400940535', NULL, 'UTF-8', 'b2oNbRrxM67nXpD0ve/P4T5e8Ec=', 'NxCu543jPYfdUhsuS0dg', 'clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:', '40094053544088300Approved1AAABCWmBZQAAAXZzaIFlAAAAAAA=05483612:8582B3A825C939033027CCB4DEA4CB3EE14C9A017EA9DC5181DF7EC649DDCAFC:4506:##400940535NxCu543jPYfdUhsuS0dg', '2020-04-01 17:13:42', '2020-04-01 17:13:42'),
(25, 'ORDER-20092XLuE04029006', NULL, 'true', 'Şişli', 'EnBiosis', 'Auth', '7.50', 'tr', '400940535', '4836 12** **** 2417', '0.10', '1', '440293', 'a1', '24', 'VISA', 'ENDIET19', '483612***2417', '000109', 'Harbiye, Atiye Sokak. Kat: 2 No:8, Şişli/Istanbul', '2', '88.248.135.154', NULL, 'http://localhost/enbiosis/public/checkout-result', '483612:8582B3A825C939033027CCB4DEA4CB3EE14C9A017EA9DC5181DF7EC649DDCAFC:4506:##400940535', '00', '1', NULL, NULL, '20092XLuE04029008', '20200401 23:11:46', 'a2', 'Istanbul', '01', '3d_pay', NULL, 'Approved', 'tr', '175', 'Authenticated', NULL, '0.1', 'false', 'AAABCWmBZQAAAXZzaIFlAAAAAAA=', 'a5', 'digest', '009223854769', 'true', '440883', 'http://localhost/enbiosis/public/checkout-result', '2', 'postakod34367', '6.25', 'faturaFirma', 'LCUZrLb5zsjv8CPZmYREZsOSRTk=', 'UTF-8', NULL, '1', '1', '05', '4.0', 'XXX', 'LA* AT*', 'is', '3', 'a4 desc', 'T. VAKIFLAR BANKASI T.A.O.', '400940535', NULL, 'UTF-8', 'b2oNbRrxM67nXpD0ve/P4T5e8Ec=', 'NxCu543jPYfdUhsuS0dg', 'clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:', '40094053544088300Approved1AAABCWmBZQAAAXZzaIFlAAAAAAA=05483612:8582B3A825C939033027CCB4DEA4CB3EE14C9A017EA9DC5181DF7EC649DDCAFC:4506:##400940535NxCu543jPYfdUhsuS0dg', '2020-04-01 17:14:06', '2020-04-01 17:14:06'),
(26, 'ORDER-20092XeoD04021052', NULL, 'true', 'Şişli', 'EnBiosis', 'Auth', '7.50', 'tr', '400940535', '4836 12** **** 2417', '0.10', '1', '440293', 'a1', '24', 'VISA', 'ENDIET19', '483612***2417', '000110', 'Harbiye, Atiye Sokak. Kat: 2 No:8, Şişli/Istanbul', '2', '88.248.135.154', NULL, 'http://localhost/enbiosis/public/checkout-result', '483612:FD93BC15641EC76CB4C14F54C826870747EF74C0C781482213206751372E0E4C:3440:##400940535', '00', '1', NULL, NULL, '20092XeoD04021054', '20200401 23:30:40', 'a2', 'Istanbul', '01', '3d_pay', NULL, 'Approved', 'tr', '175', 'Authenticated', NULL, '0.1', 'false', 'AAABCXJ3NQAAAXZzgHc1AAAAAAA=', 'a5', 'digest', '009223855651', 'true', '442405', 'http://localhost/enbiosis/public/checkout-result', '2', 'postakod34367', '6.25', 'faturaFirma', 'mjc2rtFYjJWxafeCxCAKVEaohrY=', 'UTF-8', NULL, '1', '1', '05', '4.0', 'XXX', 'LA* AT*', 'is', '3', 'a4 desc', 'T. VAKIFLAR BANKASI T.A.O.', '400940535', NULL, 'UTF-8', 'qFlblktUPGsLcjbjQkHTYLAnlEU=', 'SEti0wZhaj+29B/Gc5P/', 'clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:', '40094053544240500Approved1AAABCXJ3NQAAAXZzgHc1AAAAAAA=05483612:FD93BC15641EC76CB4C14F54C826870747EF74C0C781482213206751372E0E4C:3440:##400940535SEti0wZhaj+29B/Gc5P/', '2020-04-01 17:30:42', '2020-04-01 17:30:42'),
(27, 'ORDER-20092XeoD04021052', NULL, 'true', 'Şişli', 'EnBiosis', 'Auth', '7.50', 'tr', '400940535', '4836 12** **** 2417', '0.10', '1', '440293', 'a1', '24', 'VISA', 'ENDIET19', '483612***2417', '000110', 'Harbiye, Atiye Sokak. Kat: 2 No:8, Şişli/Istanbul', '2', '88.248.135.154', NULL, 'http://localhost/enbiosis/public/checkout-result', '483612:FD93BC15641EC76CB4C14F54C826870747EF74C0C781482213206751372E0E4C:3440:##400940535', '00', '1', NULL, NULL, '20092XeoD04021054', '20200401 23:30:40', 'a2', 'Istanbul', '01', '3d_pay', NULL, 'Approved', 'tr', '175', 'Authenticated', NULL, '0.1', 'false', 'AAABCXJ3NQAAAXZzgHc1AAAAAAA=', 'a5', 'digest', '009223855651', 'true', '442405', 'http://localhost/enbiosis/public/checkout-result', '2', 'postakod34367', '6.25', 'faturaFirma', 'mjc2rtFYjJWxafeCxCAKVEaohrY=', 'UTF-8', NULL, '1', '1', '05', '4.0', 'XXX', 'LA* AT*', 'is', '3', 'a4 desc', 'T. VAKIFLAR BANKASI T.A.O.', '400940535', NULL, 'UTF-8', 'qFlblktUPGsLcjbjQkHTYLAnlEU=', 'SEti0wZhaj+29B/Gc5P/', 'clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:', '40094053544240500Approved1AAABCXJ3NQAAAXZzgHc1AAAAAAA=05483612:FD93BC15641EC76CB4C14F54C826870747EF74C0C781482213206751372E0E4C:3440:##400940535SEti0wZhaj+29B/Gc5P/', '2020-04-01 17:32:16', '2020-04-01 17:32:16'),
(28, 'ORDER-20092XeoD04021052', NULL, 'true', 'Şişli', 'EnBiosis', 'Auth', '7.50', 'tr', '400940535', '4836 12** **** 2417', '0.10', '1', '440293', 'a1', '24', 'VISA', 'ENDIET19', '483612***2417', '000110', 'Harbiye, Atiye Sokak. Kat: 2 No:8, Şişli/Istanbul', '2', '88.248.135.154', NULL, 'http://localhost/enbiosis/public/checkout-result', '483612:FD93BC15641EC76CB4C14F54C826870747EF74C0C781482213206751372E0E4C:3440:##400940535', '00', '1', NULL, NULL, '20092XeoD04021054', '20200401 23:30:40', 'a2', 'Istanbul', '01', '3d_pay', NULL, 'Approved', 'tr', '175', 'Authenticated', NULL, '0.1', 'false', 'AAABCXJ3NQAAAXZzgHc1AAAAAAA=', 'a5', 'digest', '009223855651', 'true', '442405', 'http://localhost/enbiosis/public/checkout-result', '2', 'postakod34367', '6.25', 'faturaFirma', 'mjc2rtFYjJWxafeCxCAKVEaohrY=', 'UTF-8', NULL, '1', '1', '05', '4.0', 'XXX', 'LA* AT*', 'is', '3', 'a4 desc', 'T. VAKIFLAR BANKASI T.A.O.', '400940535', NULL, 'UTF-8', 'qFlblktUPGsLcjbjQkHTYLAnlEU=', 'SEti0wZhaj+29B/Gc5P/', 'clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:', '40094053544240500Approved1AAABCXJ3NQAAAXZzgHc1AAAAAAA=05483612:FD93BC15641EC76CB4C14F54C826870747EF74C0C781482213206751372E0E4C:3440:##400940535SEti0wZhaj+29B/Gc5P/', '2020-04-01 17:33:20', '2020-04-01 17:33:20'),
(29, 'ORDER-20093SEsH04026728', NULL, 'true', 'Şişli', 'EnBiosis', 'Auth', '7.50', 'en', '400940535', '4836 12** **** 2417', '0.10', '1', '440293', 'a1', '24', 'VISA', 'ENDIET19', '483612***2417', '000112', 'Harbiye, Atiye Sokak. Kat: 2 No:8, Şişli/Istanbul', '2', '88.248.135.154', NULL, 'http://localhost/enbiosis/public/checkout-result', '483612:40E0E4D5313A97ABBFE11451079254169DB4342A1C54D0712CC8F2CA6BD729B6:4031:##400940535', '00', '1', NULL, NULL, '20093SEsH04026730', '20200402 18:04:44', 'a2', 'Istanbul', '01', '3d_pay', NULL, 'Approved', 'tr', '175', 'Authenticated', NULL, '0.1', 'false', 'AAABA2FiggAAAXZ5RmKCAAAAAAA=', 'a5', 'digest', '009318908155', 'true', '324603', 'http://localhost/enbiosis/public/checkout-result', '2', 'postakod34367', '6.25', 'faturaFirma', '+xrBKA3No4LzUzSmVNwMoL+McsY=', 'UTF-8', NULL, '1', '1', '05', '4.0', 'XXX', 'LA* AT*', 'is', '3', 'a4 desc', 'T. VAKIFLAR BANKASI T.A.O.', '400940535', NULL, 'UTF-8', 'p8EvQml3AnxXpU62O2B/pbLWw5s=', 'gbIELV3elCtdMm1txEIa', 'clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:', '40094053532460300Approved1AAABA2FiggAAAXZ5RmKCAAAAAAA=05483612:40E0E4D5313A97ABBFE11451079254169DB4342A1C54D0712CC8F2CA6BD729B6:4031:##400940535gbIELV3elCtdMm1txEIa', '2020-04-02 12:04:45', '2020-04-02 12:04:45'),
(30, 'ORDER-20097Mt8E04011117', NULL, 'true', 'Şişli', 'EnBiosis', 'Auth', '7.50', 'tr', '400940535', '4836 12** **** 2417', '0.39', '1', '440293', 'a1', '24', 'VISA', 'ENDIET19', '483612***2417', '000116', 'Harbiye, Atiye Sokak. Kat: 2 No:8, Şişli/Istanbul', '2', '88.248.135.154', NULL, 'http://localhost/enbiosis/public/checkout-result', '483612:DAD00E92BC9EC90E93C4A2545D5B1FB1347D25E40F2B976339F81E3878B2DB16:3808:##400940535', '00', '1', NULL, NULL, '20097Mt8F04011119', '20200406 12:45:58', 'a2', 'Istanbul', '01', '3d_pay', NULL, 'Approved', 'tr', '179', 'Authenticated', NULL, '0.39', 'false', 'AAABCSkHhQAAAXcHkAeFAAAAAAA=', 'a5', 'digest', '009712136898', 'true', '101164', 'http://localhost/enbiosis/public/checkout-result', '2', 'postakod34367', '6.25', 'faturaFirma', 'YbhsS8gY0O0zK5+99F6URlECNzE=', 'UTF-8', NULL, '1', '1', '05', '4.0', 'XXX', 'LA* AT*', 'is', '3', 'a4 desc', 'T. VAKIFLAR BANKASI T.A.O.', '400940535', NULL, 'UTF-8', '+A68/2nMrEeijtI8RCx4ILzl5Xw=', 'WYAXiad5uFtZdlQLva72', 'clientid:oid:AuthCode:ProcReturnCode:Response:mdStatus:cavv:eci:md:rnd:', '40094053510116400Approved1AAABCSkHhQAAAXcHkAeFAAAAAAA=05483612:DAD00E92BC9EC90E93C4A2545D5B1FB1347D25E40F2B976339F81E3878B2DB16:3808:##400940535WYAXiad5uFtZdlQLva72', '2020-04-06 09:46:00', '2020-04-06 09:46:00');

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE `photos` (
  `id` int(10) UNSIGNED NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imageable_id` int(11) NOT NULL,
  `imageable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `photos`
--

INSERT INTO `photos` (`id`, `path`, `imageable_id`, `imageable_type`, `created_at`, `updated_at`) VALUES
(4, 'uclu-diyet-paketi.jpeg', 3, 'App\\Product', '2020-02-18 12:00:26', '2020-02-18 12:00:26'),
(5, 'tekli-paket.jpeg', 2, 'App\\Product', '2020-02-18 12:00:41', '2020-02-18 12:00:41'),
(6, 'cift-paketi.jpeg', 1, 'App\\Product', '2020-02-18 12:00:49', '2020-02-18 12:00:49'),
(7, 'dortlu-takip-paketi.webp', 87, 'App\\Product', '2020-02-19 05:56:14', '2020-02-19 05:56:14'),
(26, 'https://img.youtube.com/vi/AB-YtkGqEUU/maxresdefault.jpg', 4, 'App\\Resource', '2020-03-25 15:04:22', '2020-03-25 15:04:22'),
(28, 'https://img.youtube.com/vi/AB-YtkGqEUU/maxresdefault.jpg', 6, 'App\\Resource', '2020-03-26 12:23:34', '2020-03-26 12:23:34'),
(30, 'https://img.youtube.com/vi/AB-YtkGqEUU/maxresdefault.jpg', 8, 'App\\Resource', '2020-03-27 10:46:31', '2020-03-27 10:46:31'),
(31, 'https://img.youtube.com/vi/AB-YtkGqEUU/maxresdefault.jpg', 9, 'App\\Resource', '2020-03-31 09:53:27', '2020-03-31 09:53:27');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `discount_price` double(8,2) DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `stok` int(11) NOT NULL,
  `admin_id` bigint(20) DEFAULT NULL,
  `number_of_kit` int(11) NOT NULL DEFAULT 1,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `discount_price`, `slug`, `description`, `stok`, `admin_id`, `number_of_kit`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Çift Paketi', 2295.00, NULL, 'cift-paketi', '<p>Bağırsak mikrobiyom profilinizi g&ouml;r&uuml;nt&uuml;lemek i&ccedil;in bir kez test edin.</p><p>Bağırsak mikrobiyom sıralama raporunuz sayesinde i&ccedil; d&uuml;nyanız hakkında ayrıntılı bilgi sahibi olun!</p>', 5000, 1, 2, NULL, '2020-01-14 19:08:47', '2020-01-15 23:55:44'),
(2, 'Tekli Paket', 1299.00, NULL, 'tekli-paket', '<p>Bağırsak mikrobiyom profilini g&ouml;r&uuml;nt&uuml;lemek i&ccedil;in bir kez test et.</p><p>Bağırsak mikrobiyom sıralama raporun sayesinde i&ccedil; d&uuml;nyan hakkında ayrıntılı bilgi sahibi ol!</p>', 5000, 1, 1, NULL, '2020-01-14 19:43:42', '2020-04-01 19:41:31'),
(3, 'Üçlü Diyet Paketi', 2925.00, NULL, 'uclu-diyet-paketi', '<p>Bağırsak mikrobiyom profilini &uuml;&ccedil; kez test et: diyet veya yaşam tarzı değişikliklerinden &ouml;nce, sırasında ve sonrasında!</p><p>Takipli yapacağın testler, sana zaman i&ccedil;inde bağırsak mikrobiyom profilinin nasıl değiştiğini daha kapsamlı bir dok&uuml;manla sunar.</p><p>Fonksiyonel aktif mikrobiyom değişikliklerini takip etmen i&ccedil;in yılda birka&ccedil; kez veya gerektiği sıklıkta tekrar test etmeni &ouml;neririz.</p>', 5000, 1, 3, NULL, '2020-01-15 23:56:27', '2020-01-15 23:56:27'),
(87, 'Dörtlü Takip Paketi', 3825.00, 20.00, 'dortlu-takip-paketi', '<p>Bağırsak mikrobiyom profilini d&ouml;rt kez test et: diyet veya yaşam tarzı değişikliklerinden &ouml;nce, sırasında, sonrasında ve kontrol&uuml;nde!Takipli yapacağın testler, sana zaman i&ccedil;inde bağırsak mikrobiyom profilinin nasıl değiştiğini daha kapsamlı bir dok&uuml;manla sunar.Fonksiyonel aktif mikrobiyom değişikliklerini takip etmen i&ccedil;in yılda birka&ccedil; kez veya gerektiği sıklıkta tekrar test etmeni &ouml;neririz.</p>', 9999, 1, 4, NULL, '2020-02-19 05:56:14', '2020-04-01 17:16:01');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `survey_section_id` bigint(20) UNSIGNED NOT NULL,
  `question_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'text',
  `question_order` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `survey_section_id`, `question_type`, `question_order`, `created_at`, `updated_at`, `deleted_at`) VALUES
(67, 55, 'date', 1, '2020-03-02 13:24:24', '2020-04-08 18:14:14', NULL),
(68, 55, 'text', 2, '2020-03-02 13:24:57', '2020-03-02 13:24:57', NULL),
(69, 55, 'number', 3, '2020-03-02 13:25:10', '2020-03-02 13:25:10', NULL),
(70, 55, 'number', 4, '2020-03-02 13:25:25', '2020-04-09 14:44:17', NULL),
(71, 55, 'radio', 5, '2020-03-02 13:26:02', '2020-03-02 13:26:20', NULL),
(72, 55, 'text', 6, '2020-03-02 13:26:38', '2020-03-02 13:26:38', NULL),
(73, 55, 'text', 7, '2020-03-02 13:26:51', '2020-03-02 13:26:51', NULL),
(74, 55, 'radio', 8, '2020-03-02 13:28:48', '2020-03-02 13:28:48', NULL),
(75, 55, 'radio', 9, '2020-03-02 13:29:23', '2020-03-02 13:29:23', NULL),
(76, 55, 'radio', 10, '2020-03-02 13:30:18', '2020-03-02 13:30:18', NULL),
(77, 55, 'text', 11, '2020-03-02 13:30:46', '2020-03-02 13:30:46', NULL),
(78, 55, 'radio', 12, '2020-03-02 13:31:35', '2020-03-02 13:31:35', NULL),
(79, 55, 'radio', 13, '2020-03-02 13:32:37', '2020-03-02 13:33:34', NULL),
(80, 55, 'radio', 14, '2020-03-02 13:35:45', '2020-03-02 13:35:45', NULL),
(81, 55, 'radio', 15, '2020-03-02 13:38:24', '2020-03-02 13:38:24', NULL),
(82, 55, 'radio', 16, '2020-03-02 13:39:32', '2020-03-02 13:39:32', NULL),
(83, 55, 'radio', 17, '2020-03-02 13:40:19', '2020-03-02 13:40:19', NULL),
(84, 55, 'radio', 18, '2020-03-02 13:40:58', '2020-03-02 13:40:58', NULL),
(85, 55, 'radio', 19, '2020-03-02 13:41:47', '2020-03-02 13:43:35', NULL),
(86, 55, 'radio', 20, '2020-03-02 13:42:54', '2020-03-02 13:43:23', NULL),
(87, 55, 'radio', 21, '2020-03-02 13:45:31', '2020-03-02 13:45:31', NULL),
(88, 55, 'radio', 22, '2020-03-02 13:46:18', '2020-03-02 13:46:18', NULL),
(89, 55, 'radio', 23, '2020-03-02 13:47:13', '2020-03-02 13:47:13', NULL),
(90, 55, 'radio', 24, '2020-03-02 13:48:17', '2020-03-02 13:48:17', NULL),
(91, 56, 'radio', 1, '2020-03-02 13:54:58', '2020-03-02 13:54:58', NULL),
(92, 56, 'radio', 2, '2020-03-02 13:56:51', '2020-03-02 13:56:51', NULL),
(93, 56, 'radio', 3, '2020-03-02 13:58:28', '2020-03-02 13:58:40', NULL),
(94, 56, 'radio', 4, '2020-03-02 13:59:47', '2020-03-03 06:29:06', '2020-03-03 06:29:06'),
(95, 55, 'date', 1, '2020-03-03 04:41:04', '2020-03-05 04:38:47', '2020-03-05 04:38:47'),
(96, 56, 'radio', 4, '2020-03-03 06:32:12', '2020-03-03 06:32:12', NULL),
(97, 56, 'radio', 5, '2020-03-03 06:35:54', '2020-03-03 06:35:54', NULL),
(98, 56, 'radio', 6, '2020-03-03 06:37:24', '2020-03-03 06:37:24', NULL),
(99, 56, 'radio', 7, '2020-03-03 06:40:13', '2020-03-03 06:40:21', NULL),
(100, 56, 'radio', 8, '2020-03-03 06:41:03', '2020-03-03 06:41:03', NULL),
(101, 56, 'radio', 9, '2020-03-03 06:45:22', '2020-03-03 06:45:22', NULL),
(102, 56, 'radio', 10, '2020-03-03 06:47:20', '2020-03-03 06:47:20', NULL),
(103, 56, 'radio', 11, '2020-03-03 06:48:38', '2020-03-03 06:48:38', NULL),
(104, 56, 'radio', 12, '2020-03-03 06:52:07', '2020-03-03 06:52:07', NULL),
(105, 56, 'radio', 13, '2020-03-03 06:53:33', '2020-03-03 06:53:33', NULL),
(106, 56, 'radio', 14, '2020-03-03 06:55:02', '2020-03-03 06:55:02', NULL),
(107, 56, 'radio', 15, '2020-03-03 06:55:51', '2020-03-03 06:55:51', NULL),
(108, 56, 'radio', 16, '2020-03-03 06:56:43', '2020-03-03 06:56:43', NULL),
(109, 56, 'radio', 17, '2020-03-03 06:57:24', '2020-03-03 07:00:57', NULL),
(110, 56, 'radio', 19, '2020-03-03 06:58:58', '2020-03-03 06:58:58', NULL),
(111, 56, 'radio', 21, '2020-03-03 07:00:29', '2020-03-03 07:00:29', NULL),
(112, 56, 'textarea', 22, '2020-03-03 07:00:45', '2020-03-03 07:00:45', NULL),
(113, 57, 'checkbox', 1, '2020-03-03 07:03:08', '2020-03-03 07:03:08', NULL),
(114, 57, 'checkbox', 2, '2020-03-03 07:10:53', '2020-03-03 07:10:53', NULL),
(115, 57, 'radio', 3, '2020-03-03 07:12:46', '2020-03-03 07:12:46', NULL),
(116, 58, 'radio', 1, '2020-03-03 07:15:00', '2020-03-03 07:15:00', NULL),
(117, 58, 'radio', 2, '2020-03-03 07:22:32', '2020-03-03 07:22:32', NULL),
(118, 58, 'radio', 3, '2020-03-03 07:23:21', '2020-03-03 07:23:21', NULL),
(119, 58, 'radio', 4, '2020-03-03 07:24:11', '2020-03-03 07:24:11', NULL),
(120, 58, 'radio', 5, '2020-03-03 07:25:09', '2020-03-03 07:25:36', NULL),
(121, 58, 'radio', 6, '2020-03-03 07:26:27', '2020-03-03 07:26:40', NULL),
(122, 58, 'radio', 7, '2020-03-03 07:31:33', '2020-03-03 07:31:33', NULL),
(123, 58, 'radio', 8, '2020-03-03 07:33:20', '2020-03-03 07:33:36', NULL),
(124, 58, 'radio', 9, '2020-03-03 07:35:14', '2020-03-03 07:35:14', NULL),
(125, 59, 'radio', 1, '2020-03-03 07:37:05', '2020-03-03 07:37:05', NULL),
(126, 59, 'radio', 2, '2020-03-03 07:38:39', '2020-03-03 07:38:39', NULL),
(127, 59, 'radio', 3, '2020-03-03 07:51:43', '2020-03-03 07:51:43', NULL),
(128, 59, 'radio', 4, '2020-03-03 07:53:41', '2020-03-03 07:53:41', NULL),
(129, 59, 'radio', 5, '2020-03-03 07:55:26', '2020-03-03 07:55:26', NULL),
(130, 59, 'radio', 6, '2020-03-03 07:57:06', '2020-03-03 07:57:06', NULL),
(131, 59, 'radio', 7, '2020-03-03 07:58:11', '2020-03-03 07:58:11', NULL),
(132, 59, 'radio', 8, '2020-03-03 08:00:02', '2020-03-03 08:00:02', NULL),
(133, 59, 'radio', 9, '2020-03-03 08:06:56', '2020-03-03 08:06:56', NULL),
(134, 59, 'radio', 10, '2020-03-03 08:08:16', '2020-03-03 08:08:31', NULL),
(135, 59, 'radio', 11, '2020-03-03 08:09:15', '2020-03-03 08:09:15', NULL),
(136, 59, 'radio', 12, '2020-03-03 08:09:54', '2020-03-03 08:09:54', NULL),
(137, 59, 'radio', 13, '2020-03-03 08:10:30', '2020-03-03 08:10:30', NULL),
(138, 59, 'radio', 14, '2020-03-03 08:11:14', '2020-03-03 08:11:14', NULL),
(139, 59, 'radio', 15, '2020-03-03 08:12:00', '2020-03-03 08:12:12', NULL),
(140, 59, 'radio', 16, '2020-03-03 08:13:05', '2020-03-03 08:13:05', NULL),
(141, 59, 'radio', 17, '2020-03-03 08:14:01', '2020-03-03 08:14:01', NULL),
(142, 59, 'radio', 18, '2020-03-03 08:15:34', '2020-03-03 08:15:34', NULL),
(143, 59, 'radio', 19, '2020-03-03 08:20:28', '2020-03-03 08:20:28', NULL),
(144, 59, 'radio', 20, '2020-03-03 08:21:30', '2020-03-03 08:21:30', NULL),
(145, 59, 'radio', 21, '2020-03-03 08:47:00', '2020-03-03 08:47:00', NULL),
(146, 59, 'radio', 22, '2020-03-03 08:48:03', '2020-03-03 08:48:03', NULL),
(153, 55, 'radio', 1, '2020-03-10 12:14:07', '2020-03-10 12:14:14', '2020-03-10 12:14:14');

-- --------------------------------------------------------

--
-- Table structure for table `question_langs`
--

CREATE TABLE `question_langs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `lang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'tr',
  `question_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `option_name` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `question_langs`
--

INSERT INTO `question_langs` (`id`, `lang`, `question_id`, `title`, `option_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(63, 'tr', 67, 'Doğum Tarihiniz', NULL, '2020-03-02 13:24:24', '2020-03-02 13:24:24', NULL),
(64, 'tr', 68, 'Mesleğiniz', NULL, '2020-03-02 13:24:57', '2020-03-02 13:24:57', NULL),
(65, 'tr', 69, 'Kilonuz', NULL, '2020-03-02 13:25:10', '2020-03-02 13:25:10', NULL),
(66, 'tr', 70, 'Boyunuz', NULL, '2020-03-02 13:25:25', '2020-03-02 13:25:25', NULL),
(67, 'tr', 71, 'Doğum şekliniz', '[{\"title\":\"Normal do\\u011fum\",\"is_if\":null},{\"title\":\"Sezeryan\",\"is_if\":null}]', '2020-03-02 13:26:02', '2020-03-02 13:26:02', NULL),
(68, 'tr', 72, 'Yaşadığınız şehir', NULL, '2020-03-02 13:26:38', '2020-03-02 13:26:38', NULL),
(69, 'tr', 73, 'Ülke', NULL, '2020-03-02 13:26:51', '2020-03-02 13:26:51', NULL),
(70, 'tr', 74, 'Hiç farklı bir ülkede yaşadınız mı ?', '[{\"title\":\"Evet\",\"is_if\":\"Hangi y\\u0131llar aras\\u0131nda nerede ya\\u015fad\\u0131n\\u0131z ?\"},{\"title\":\"Hay\\u0131r\",\"is_if\":null}]', '2020-03-02 13:28:48', '2020-03-02 13:28:48', NULL),
(71, 'tr', 75, 'Şu anda yaşadığınız bölge', '[{\"title\":\"K\\u0131rsal\",\"is_if\":null},{\"title\":\"Kentsel\",\"is_if\":null}]', '2020-03-02 13:29:23', '2020-03-02 13:29:23', NULL),
(72, 'tr', 76, 'Yaşadığınız konut tipi', '[{\"title\":\"Apartman Dairesi\",\"is_if\":null},{\"title\":\"M\\u00fcstakil bah\\u00e7eli ev\",\"is_if\":null}]', '2020-03-02 13:30:18', '2020-03-02 13:30:18', NULL),
(73, 'tr', 77, 'Günde ortalama kaç adım yürürsünüz', NULL, '2020-03-02 13:30:46', '2020-03-02 13:30:46', NULL),
(74, 'tr', 78, 'Gün içerisinde genellikle', '[{\"title\":\"oturarak \\u00e7al\\u0131\\u015f\\u0131r\\u0131m\",\"is_if\":null},{\"title\":\"zaman\\u0131m ayakta ge\\u00e7er\",\"is_if\":null},{\"title\":\"fiziksel aktivite gerektiren faliyetlerde bulunurum\",\"is_if\":null}]', '2020-03-02 13:31:35', '2020-03-02 13:31:35', NULL),
(75, 'tr', 79, 'Spor yapma sıklığınız', '[{\"title\":\"hi\\u00e7\",\"is_if\":null},{\"title\":\"nadir\",\"is_if\":null},{\"title\":\"Haftada 1-2 kere\",\"is_if\":null},{\"title\":\"Haftada 3-5 kere\",\"is_if\":null},{\"title\":\"her g\\u00fcn\",\"is_if\":null}]', '2020-03-02 13:32:37', '2020-03-02 13:32:37', NULL),
(76, 'tr', 80, 'Uykusuzluk problemi çeker misiniz ?', '[{\"title\":\"Evet\",\"is_if\":\"Ortalama uyku s\\u00fcreniz nedir ?\"},{\"title\":\"Hay\\u0131r\",\"is_if\":\"Ortalama uyku s\\u00fcreniz nedir ?\"}]', '2020-03-02 13:35:45', '2020-03-02 13:35:45', NULL),
(77, 'tr', 81, 'Yatağa girince 30 dakika içerisinde uykuya dalar mısınız ?', '[{\"title\":\"Evet\",\"is_if\":null},{\"title\":\"Hay\\u0131r\",\"is_if\":null}]', '2020-03-02 13:38:25', '2020-03-02 13:38:25', NULL),
(78, 'tr', 82, 'Uykunun ortasında ya da sabah çok erken uyanır mısınız ?', '[{\"title\":\"Evet\",\"is_if\":null},{\"title\":\"Hay\\u0131r\",\"is_if\":null}]', '2020-03-02 13:39:32', '2020-03-02 13:39:32', NULL),
(79, 'tr', 83, 'Günlük uyku süreniz', '[{\"title\":\"5 saatten az\",\"is_if\":null},{\"title\":\"5-6 saat\",\"is_if\":null},{\"title\":\"7-8 saat\",\"is_if\":null},{\"title\":\"8 saatten fazla\",\"is_if\":null}]', '2020-03-02 13:40:19', '2020-03-02 13:40:19', NULL),
(80, 'tr', 84, 'Sigara kullanır mısınız ?', '[{\"title\":\"hay\\u0131r\",\"is_if\":null},{\"title\":\"haftada birka\\u00e7\",\"is_if\":null},{\"title\":\"g\\u00fcnde birka\\u00e7\",\"is_if\":null},{\"title\":\"g\\u00fcnde 1 paket\",\"is_if\":null}]', '2020-03-02 13:40:58', '2020-03-02 13:40:58', NULL),
(81, 'tr', 85, 'Alkol kullanır mısınız ?', '[{\"title\":\"hay\\u0131r\",\"is_if\":null},{\"title\":\"ayda birka\\u00e7 kez\",\"is_if\":null},{\"title\":\"haftada birka\\u00e7 kaz\",\"is_if\":null},{\"title\":\"g\\u00fcnl\\u00fck\",\"is_if\":null}]', '2020-03-02 13:41:47', '2020-03-02 13:41:47', NULL),
(82, 'tr', 86, 'Evinizde evcil hayvan', '[{\"title\":\"yok\",\"is_if\":null},{\"title\":\"kedi\",\"is_if\":null},{\"title\":\"k\\u00f6pek\",\"is_if\":null},{\"title\":\"akvaryum\\/kafes havyan\\u0131\",\"is_if\":null},{\"title\":\"di\\u011fer\",\"is_if\":\"nedir ?\"}]', '2020-03-02 13:42:54', '2020-03-02 13:42:54', NULL),
(83, 'tr', 87, 'Yaşam alanınızda/ konutunuzda yalnız mı yaşıyorsunuz ?', '[{\"title\":\"Evet\",\"is_if\":null},{\"title\":\"Hay\\u0131r\",\"is_if\":\"Ka\\u00e7 ki\\u015fi ile ya\\u015f\\u0131yorsunuz ?\"}]', '2020-03-02 13:45:31', '2020-03-02 13:45:31', NULL),
(84, 'tr', 88, 'Genel olarak kendimi', '[{\"title\":\"Yorgun hissederim\",\"is_if\":null},{\"title\":\"enerjik hissederim\",\"is_if\":null}]', '2020-03-02 13:46:18', '2020-03-02 13:46:18', NULL),
(85, 'tr', 89, 'İş, sosyal aktivite ve/veya eğlence sebebiyle bir gün boyunca mavi ışığa (telefon, bilgisyar, televizyon gibi) maruz kalma süreniz', '[{\"title\":\"3 saatten daha az\",\"is_if\":null},{\"title\":\"3-5 saat aras\\u0131\",\"is_if\":null},{\"title\":\"5-8 saat aras\\u0131\",\"is_if\":null},{\"title\":\"8 saatten fazla\",\"is_if\":null}]', '2020-03-02 13:47:13', '2020-03-02 13:47:13', NULL),
(86, 'tr', 90, 'Yemek yedikten sonra kendinizi nasıl hissedersiniz ?', '[{\"title\":\"Rahatlam\\u0131\\u015f\",\"is_if\":null},{\"title\":\"\\u015ei\\u015fkin\",\"is_if\":null},{\"title\":\"Uykum gelir\",\"is_if\":null},{\"title\":\"Daha fazla yemek isterim\",\"is_if\":null},{\"title\":\"Pi\\u015fmanl\\u0131k\",\"is_if\":null}]', '2020-03-02 13:48:17', '2020-03-02 13:48:17', NULL),
(87, 'tr', 91, 'Diyabet (şeker hastalığı) şikayetiniz var mı ?', '[{\"title\":\"Hay\\u0131r\",\"is_if\":null},{\"title\":\"Evet, hekim taraf\\u0131ndan te\\u015fhis edildi\",\"is_if\":\"Diyabet tipiniz ( Tip 1 diyabet - tip 2 diyabet -  gebelik \\u015fekeri  - bilmiyorum)\"},{\"title\":\"te\\u015fhis edilmedi, ama \\u015f\\u00fcpheleniyorum\",\"is_if\":null}]', '2020-03-02 13:54:58', '2020-03-02 13:54:58', NULL),
(88, 'tr', 92, 'Böbrek hastalığı şikayetiniz varmı ?', '[{\"title\":\"Hay\\u0131r\",\"is_if\":null},{\"title\":\"Evet, hekim taraf\\u0131ndan te\\u015fhis edildi\",\"is_if\":null},{\"title\":\"te\\u015fhis edilmedi, ama \\u015f\\u00fcpheleniyorum\",\"is_if\":null}]', '2020-03-02 13:56:51', '2020-03-02 13:56:51', NULL),
(89, 'tr', 93, 'Kanser teşhisi aldınız mı ?', '[{\"title\":\"Hay\\u0131r\",\"is_if\":null},{\"title\":\"Evet, hekim taraf\\u0131ndan te\\u015fhis edildi\",\"is_if\":\"Kanser \\u00e7e\\u015fidi\"}]', '2020-03-02 13:58:28', '2020-03-02 13:58:28', NULL),
(90, 'tr', 94, 'Cilt probleminiz var mı ?', '[{\"title\":\"Hay\\u0131r\",\"is_if\":null},{\"title\":\"Evet\",\"is_if\":\"Cilt problem tipi (  sedef - kepek\\/d\\u00f6k\\u00fcnt\\u00fc -  mantar \\u00fcremesi  - di\\u011fer  )\"}]', '2020-03-02 13:59:47', '2020-03-02 13:59:47', NULL),
(91, 'tr', 95, 'Doğum Tarihimiz', NULL, '2020-03-03 04:41:04', '2020-03-03 04:41:04', NULL),
(92, 'tr', 96, 'Cilt probleminiz', '[{\"title\":\"Sedef\",\"is_if\":null},{\"title\":\"Kepek \\/ D\\u00f6k\\u00fcnt\\u00fc\",\"is_if\":null},{\"title\":\"Mantar \\u00fcremesi\",\"is_if\":null},{\"title\":\"Di\\u011fer\",\"is_if\":\"Yaz\\u0131n\\u0131z\"},{\"title\":\"problem yok\",\"is_if\":null}]', '2020-03-03 06:32:12', '2020-03-03 06:32:12', NULL),
(93, 'tr', 97, 'Kronik bağırsak probleminiz', '[{\"title\":\"Kronik kab\\u0131zl\\u0131k\",\"is_if\":null},{\"title\":\"Kronik ishal\",\"is_if\":null},{\"title\":\"Gaz\\/\\u015fi\\u015fkinlik\",\"is_if\":null},{\"title\":\"Di\\u011fer\",\"is_if\":\"Yaz\\u0131n\\u0131z\"},{\"title\":\"Problem yok\",\"is_if\":null}]', '2020-03-03 06:35:54', '2020-03-03 06:35:54', NULL),
(94, 'tr', 98, 'Laktoz intoleransnız var mı ?', '[{\"title\":\"Hay\\u0131r\",\"is_if\":null},{\"title\":\"Evet\",\"is_if\":\"Hangi test ile tan\\u0131mland\\u0131\"}]', '2020-03-03 06:37:24', '2020-03-03 06:37:24', NULL),
(95, 'tr', 99, 'Akciğer hastalığınız var mı ?', '[{\"title\":\"Hay\\u0131r\",\"is_if\":null},{\"title\":\"Evet, hekim taraf\\u0131ndan te\\u015fhis edildi\",\"is_if\":null},{\"title\":\"Te\\u015fhis edilmedi, ama \\u015f\\u00fcpheleniyorum\",\"is_if\":null}]', '2020-03-03 06:40:13', '2020-03-03 06:40:13', NULL),
(96, 'tr', 100, 'Karaciğer hastalıınız var mı ?', '[{\"title\":\"Hay\\u0131r\",\"is_if\":null},{\"title\":\"Evet, hekim taraf\\u0131ndan te\\u015fhis edildi\",\"is_if\":null},{\"title\":\"Te\\u015fhis edilmedi, ama \\u015f\\u00fcpheleniyorum\",\"is_if\":null}]', '2020-03-03 06:41:03', '2020-03-03 06:41:03', NULL),
(97, 'tr', 101, 'Kalp probleminiz var mı?', '[{\"title\":\"Hay\\u0131r\",\"is_if\":null},{\"title\":\"Evet, hekim taraf\\u0131ndan te\\u015fhis edildi\",\"is_if\":null},{\"title\":\"Te\\u015fhis edilmedi, ama \\u015f\\u00fcpheleniyorum\",\"is_if\":null}]', '2020-03-03 06:45:22', '2020-03-03 06:45:22', NULL),
(98, 'tr', 102, 'Depresyon şikayetiniz var mı ?', '[{\"title\":\"Hay\\u0131r\",\"is_if\":null},{\"title\":\"Evet, hekim taraf\\u0131ndan te\\u015fhis edildi\",\"is_if\":null},{\"title\":\"Te\\u015fhis edilmedi, ama \\u015f\\u00fcpheleniyorum\",\"is_if\":null}]', '2020-03-03 06:47:20', '2020-03-03 06:47:20', NULL),
(99, 'tr', 103, 'Migren şikayetiniz var mı ?', '[{\"title\":\"Hay\\u0131r\",\"is_if\":null},{\"title\":\"Evet, hekim taraf\\u0131ndan te\\u015fhis edildi\",\"is_if\":null},{\"title\":\"Te\\u015fhis edilmedi, ama \\u015f\\u00fcpheleniyorum\",\"is_if\":null}]', '2020-03-03 06:48:38', '2020-03-03 06:48:38', NULL),
(100, 'tr', 104, 'Reflü, gastrit ya da ülser şikayetleriniz var mı ?', '[{\"title\":\"Hay\\u0131r\",\"is_if\":null},{\"title\":\"Evet, hekim taraf\\u0131ndan te\\u015fhis edildi\",\"is_if\":\"te\\u015fhisi nedir ?\"},{\"title\":\"Te\\u015fhis edilmedi, ama \\u015f\\u00fcpheleniyorum\",\"is_if\":\"\\u015f\\u00fcphelendi\\u011finiz nedir ?\"}]', '2020-03-03 06:52:07', '2020-03-03 06:52:07', NULL),
(101, 'tr', 105, 'Gluten hassasiyeti probleminiz var mı ?', '[{\"title\":\"Hay\\u0131r\",\"is_if\":null},{\"title\":\"Evet, hekim taraf\\u0131ndan te\\u015fhis edildi\",\"is_if\":null},{\"title\":\"Te\\u015fhis edilmedi, ama \\u015f\\u00fcpheleniyorum\",\"is_if\":null}]', '2020-03-03 06:53:33', '2020-03-03 06:53:33', NULL),
(102, 'tr', 106, 'Tiroid probleminiz', '[{\"title\":\"Tiroid probleminiz\",\"is_if\":null},{\"title\":\"Hipertroidi\",\"is_if\":null},{\"title\":\"Hipotroidi\",\"is_if\":null},{\"title\":\"Di\\u011fer\",\"is_if\":\"Yaz\\u0131n\\u0131z\"},{\"title\":\"Hay\\u0131r\",\"is_if\":null}]', '2020-03-03 06:55:02', '2020-03-03 06:55:02', NULL),
(103, 'tr', 107, 'Romatizma probleminiz var mı ?', '[{\"title\":\"Hay\\u0131r\",\"is_if\":null},{\"title\":\"Evet, hekim taraf\\u0131ndan te\\u015fhis edildi\",\"is_if\":null},{\"title\":\"Te\\u015fhis edilmedi, ama \\u015f\\u00fcpheleniyorum\",\"is_if\":null}]', '2020-03-03 06:55:51', '2020-03-03 06:55:51', NULL),
(104, 'tr', 108, 'Son iki yıl içerisinde hastanede yatarak tedavi gördünüz mü ya da 3 günden uzun süreli olarak refakatçi kaldınız mı ?', '[{\"title\":\"Hay\\u0131r\",\"is_if\":null},{\"title\":\"Evet\",\"is_if\":\"Hangi sa\\u011fl\\u0131k sorunu sebebiyle tedavi g\\u00f6rd\\u00fcn\\u00fcz ? Ka\\u00e7 g\\u00fcn hastanede kald\\u0131n\\u0131z ?\"}]', '2020-03-03 06:56:43', '2020-03-03 06:56:43', NULL),
(105, 'tr', 109, 'Şimdiye kadar hiç operasyon geçirdiniz mi?', '[{\"title\":\"Hay\\u0131r\",\"is_if\":null},{\"title\":\"Evet\",\"is_if\":\"Operasyonlar\\u0131 ve tarihlerini l\\u00fctfen yaz\\u0131n\\u0131z\"}]', '2020-03-03 06:57:24', '2020-03-03 06:57:24', NULL),
(106, 'tr', 110, 'Haftalık bağırsak faaliyeti', '[{\"title\":\"Haftada ortalama1 kez tuvalete \\u00e7\\u0131kar\\u0131m\",\"is_if\":null},{\"title\":\"Haftada ortalama 2-3 kez tuvalete \\u00e7\\u0131kar\\u0131m\",\"is_if\":null},{\"title\":\"G\\u00fcnl\\u00fck ortalama 1 kez tuvalete \\u00e7\\u0131kar\\u0131m\",\"is_if\":null},{\"title\":\"G\\u00fcnl\\u00fck ortalama 2 kez tuvalete \\u00e7\\u0131kar\\u0131m\",\"is_if\":null},{\"title\":\"G\\u00fcnl\\u00fck ortalama 3 kez veya daha fazla tuvalete \\u00e7\\u0131kar\\u0131m\",\"is_if\":null},{\"title\":\"Yukar\\u0131dakilerden farkl\\u0131 olarak\",\"is_if\":\"Ka\\u00e7 kez tuvalete \\u00e7\\u0131kars\\u0131n\\u0131z\"}]', '2020-03-03 06:58:58', '2020-03-03 06:58:58', NULL),
(107, 'tr', 111, 'Bağırsak problemi yaşıyor musunuz ?', '[{\"title\":\"Hay\\u0131r\",\"is_if\":null},{\"title\":\"Genellikle ishal problemi ya\\u015famaktay\\u0131m\",\"is_if\":null},{\"title\":\"Genellikle kab\\u0131zl\\u0131k problemi ya\\u015famaktay\\u0131m\",\"is_if\":null}]', '2020-03-03 07:00:29', '2020-03-03 07:00:29', NULL),
(108, 'tr', 112, 'Yukarıda belirtilenler dışında hekim tarafından tanısı konulmuş hastalıklarınızı aşağıya ekleyebilirsiniz.', NULL, '2020-03-03 07:00:45', '2020-03-03 07:00:45', NULL),
(109, 'tr', 113, 'Gıda alerjiniz var mı ?', '[{\"title\":\"F\\u0131st\\u0131k\\/a\\u011fa\\u00e7 yemi\\u015fleri\",\"is_if\":null},{\"title\":\"Deniz \\u00fcr\\u00fcnleri\",\"is_if\":null},{\"title\":\"Yumurta\",\"is_if\":null},{\"title\":\"Gluten\",\"is_if\":null},{\"title\":\"Di\\u011fer\",\"is_if\":\"Yaz\\u0131n\\u0131z\"},{\"title\":\"G\\u0131da alerjim yok\",\"is_if\":null}]', '2020-03-03 07:03:08', '2020-03-03 07:03:08', NULL),
(110, 'tr', 114, 'Gıda dışı alerji şikayetiniz var mı ?', '[{\"title\":\"G\\u00fcne\\u015f\",\"is_if\":null},{\"title\":\"Hayvan t\\u00fcy\\u00fc\",\"is_if\":null},{\"title\":\"Antibiyotik\",\"is_if\":null},{\"title\":\"Polen\\/toz\",\"is_if\":null},{\"title\":\"Di\\u011fer\",\"is_if\":\"Yaz\\u0131n\\u0131z\"},{\"title\":\"G\\u0131da d\\u0131\\u015f\\u0131 alerjim yok\",\"is_if\":null}]', '2020-03-03 07:10:53', '2020-03-03 07:10:53', NULL),
(111, 'tr', 115, 'Alerji olarak sınıflandırılamayacak tahmin ettiğiniz bir besin intoleransınız var mı ?', '[{\"title\":\"Hay\\u0131r\",\"is_if\":null},{\"title\":\"Evet\",\"is_if\":\"Hangi besini t\\u00fcketince nas\\u0131l bir sorun ya\\u015fad\\u0131\\u011f\\u0131n\\u0131z\\u0131 belirtiniz.\"}]', '2020-03-03 07:12:47', '2020-03-03 07:12:47', NULL),
(112, 'tr', 116, 'Son altı ay içerisinde antibiyotik kullandınız mı ?', '[{\"title\":\"Hay\\u0131r\",\"is_if\":null},{\"title\":\"Evet, son 1 ay i\\u00e7inde\",\"is_if\":null},{\"title\":\"Evet, 3 ay \\u00f6nce\",\"is_if\":null},{\"title\":\"Evet 6 ay \\u00f6nce\",\"is_if\":null}]', '2020-03-03 07:15:00', '2020-03-03 07:15:00', NULL),
(113, 'tr', 117, 'Son altı ayda mide koruyucu (proton pompa inhibitor benzeri) ilaçlar kullandınız mı?', '[{\"title\":\"Hay\\u0131r\",\"is_if\":null},{\"title\":\"Evet\",\"is_if\":null}]', '2020-03-03 07:22:32', '2020-03-03 07:22:32', NULL),
(114, 'tr', 118, 'Son grip aşısı olma tarihiniz', '[{\"title\":\"son 6 ay i\\u00e7erisinde\",\"is_if\":null},{\"title\":\"son 12 ay i\\u00e7erisinde\",\"is_if\":null},{\"title\":\"son 1 y\\u0131l i\\u00e7erisinde grip a\\u015f\\u0131s\\u0131 olmad\\u0131m\",\"is_if\":null}]', '2020-03-03 07:23:21', '2020-03-03 07:23:21', NULL),
(115, 'tr', 119, 'Multivitamin takviyesi', '[{\"title\":\"kullanm\\u0131yorum\",\"is_if\":null},{\"title\":\"g\\u00fcnl\\u00fck olarak kullan\\u0131yorum\",\"is_if\":null},{\"title\":\"haftada 1-2 kez\",\"is_if\":null},{\"title\":\"haftada 3-5 kez\",\"is_if\":null},{\"title\":\"nadir olarak\",\"is_if\":null}]', '2020-03-03 07:24:11', '2020-03-03 07:24:11', NULL),
(116, 'tr', 120, 'D vitamini takviyesi', '[{\"title\":\"kullanm\\u0131yorum\",\"is_if\":null},{\"title\":\"g\\u00fcnl\\u00fck olarak kullan\\u0131yorum\",\"is_if\":null},{\"title\":\"haftada 1-2 kez\",\"is_if\":null},{\"title\":\"haftada 3-5 kez\",\"is_if\":null},{\"title\":\"nadir olarak\",\"is_if\":null}]', '2020-03-03 07:25:10', '2020-03-03 07:25:10', NULL),
(117, 'tr', 121, 'B vitamini takviyesi', '[{\"title\":\"kullanm\\u0131yorum\",\"is_if\":null},{\"title\":\"g\\u00fcnl\\u00fck olarak kullan\\u0131yorum\",\"is_if\":null},{\"title\":\"haftada 1-2 kez\",\"is_if\":null},{\"title\":\"haftada 3-5 kez\",\"is_if\":null},{\"title\":\"nadir olarak\",\"is_if\":null}]', '2020-03-03 07:26:27', '2020-03-03 07:26:27', NULL),
(118, 'tr', 122, 'Probiyotik takviyesi', '[{\"title\":\"kullanm\\u0131yorum\",\"is_if\":null},{\"title\":\"g\\u00fcnl\\u00fck olarak kullan\\u0131yorum\",\"is_if\":null},{\"title\":\"haftada 1-2 kez\",\"is_if\":null},{\"title\":\"haftada 3-5 kez\",\"is_if\":null},{\"title\":\"nadir olarak\",\"is_if\":null}]', '2020-03-03 07:31:33', '2020-03-03 07:31:33', NULL),
(119, 'tr', 123, 'Eğer probiyotik takviyesi kullanıyorsanız kullandığınız probiyotik çeşidi', '[{\"title\":\"kullanm\\u0131yorum\",\"is_if\":null},{\"title\":\"Bilmiyorum\",\"is_if\":null},{\"title\":\"birden\\u00e7oketkenli\",\"is_if\":null},{\"title\":\"bifidobacterium\",\"is_if\":null},{\"title\":\"Di\\u011fer\",\"is_if\":\"kulland\\u0131\\u011f\\u0131n\\u0131z probiyotik \\u00e7e\\u015fidi\"}]', '2020-03-03 07:33:20', '2020-03-03 07:33:20', NULL),
(120, 'tr', 124, 'Yukarıdakiler dışında düzenli kullandığınız ilaç/takviye var mı ?', '[{\"title\":\"Hay\\u0131r\",\"is_if\":null},{\"title\":\"Evet\",\"is_if\":\"Kulland\\u0131\\u011f\\u0131n\\u0131zila\\u00e7\\/takviye\\/kimyasal t\\u00fcr\\u00fc\"}]', '2020-03-03 07:35:14', '2020-03-03 07:35:14', NULL),
(121, 'tr', 125, 'Beslenme tipiniz ?', '[{\"title\":\"Et yiyorum\",\"is_if\":null},{\"title\":\"Yaln\\u0131zca k\\u0131rm\\u0131z\\u0131 et yemiyorum\",\"is_if\":null},{\"title\":\"Deniz \\u00fcr\\u00fcnleri d\\u0131\\u015f\\u0131nda et yemiyorum\",\"is_if\":null},{\"title\":\"Vejetaryenim\",\"is_if\":null},{\"title\":\"Vegan\\u0131m\",\"is_if\":null}]', '2020-03-03 07:37:05', '2020-03-03 07:37:05', NULL),
(122, 'tr', 126, 'Takip ettiğiniz bir diyet var mı ?', '[{\"title\":\"Hay\\u0131r\",\"is_if\":null},{\"title\":\"Evet\",\"is_if\":\"Hangi diyet takip ediyorsunuz\"}]', '2020-03-03 07:38:39', '2020-03-03 07:38:39', NULL),
(123, 'tr', 127, 'Günde kaç ana öğün yemek yiyorsunuz ?', '[{\"title\":\"1\",\"is_if\":null},{\"title\":\"2\",\"is_if\":null},{\"title\":\"3\",\"is_if\":null},{\"title\":\"3\\u2019ten fazla\",\"is_if\":null}]', '2020-03-03 07:51:43', '2020-03-03 07:51:43', NULL),
(124, 'tr', 128, 'Öğün aralarında atıştırmalık gıdalar tüketir misiniz ?', '[{\"title\":\"Hay\\u0131r\",\"is_if\":null},{\"title\":\"Meyve\",\"is_if\":null},{\"title\":\"Kuru yemi\\u015f\",\"is_if\":null},{\"title\":\"Bisk\\u00fcvi\",\"is_if\":null},{\"title\":\"Di\\u011fer\",\"is_if\":\"Yaz\\u0131n\\u0131z\"}]', '2020-03-03 07:53:41', '2020-03-03 07:53:41', NULL),
(125, 'tr', 129, 'Şeker içerikli ürünler (çikolata, sütlü/hamurlu tatlılar vs) ya da yapay tatlandırıcı (diyet/zero/ligth içecekler/ürünler dahil) kullanıyor musunuz?', '[{\"title\":\"Hay\\u0131r\",\"is_if\":null},{\"title\":\"Evet\",\"is_if\":\"hangi s\\u0131kl\\u0131kta t\\u00fcketti\\u011finizi l\\u00fctfen belirtiniz.\"}]', '2020-03-03 07:55:26', '2020-03-03 07:55:26', NULL),
(126, 'tr', 130, 'Hazır içeçekleri (gazlı içecekler, hazır meyve suyu ve soğuk çay vs) tüketiyor musunuz ?', '[{\"title\":\"Hay\\u0131r\",\"is_if\":null},{\"title\":\"Evet\",\"is_if\":\"Hangi \\u00fcr\\u00fcnden ne s\\u0131kl\\u0131kta t\\u00fcketti\\u011finizi l\\u00fctfen belirtiniz.\"}]', '2020-03-03 07:57:06', '2020-03-03 07:57:06', NULL),
(127, 'tr', 131, 'Maden suyu tüketiyor musunuz ?', '[{\"title\":\"Hay\\u0131r\",\"is_if\":null},{\"title\":\"Evet\",\"is_if\":\"Haftada ortalama ka\\u00e7 ml ?\"}]', '2020-03-03 07:58:11', '2020-03-03 07:58:11', NULL),
(128, 'tr', 132, 'Haftalık sebze tüketim sıklığınız nedir ?', '[{\"title\":\"nadir\",\"is_if\":null},{\"title\":\"haftada 1-2 kez\",\"is_if\":\"En \\u00e7ok hangi sebze \\u00fcr\\u00fcn\\u00fcn\\u00fc t\\u00fcketti\\u011finizi l\\u00fctfen belirtiniz.\"},{\"title\":\"haftada 3-5 kez\",\"is_if\":\"En \\u00e7ok hangi sebze \\u00fcr\\u00fcn\\u00fcn\\u00fc t\\u00fcketti\\u011finizi l\\u00fctfen belirtiniz.\"},{\"title\":\"her g\\u00fcn\",\"is_if\":\"En \\u00e7ok hangi sebze \\u00fcr\\u00fcn\\u00fcn\\u00fc t\\u00fcketti\\u011finizi l\\u00fctfen belirtiniz.\"}]', '2020-03-03 08:00:02', '2020-03-03 08:00:02', NULL),
(129, 'tr', 133, 'Haftalık meyve tüketim sıklığınız nedir ?', '[{\"title\":\"nadir\",\"is_if\":null},{\"title\":\"haftada 1-2 kez\",\"is_if\":\"En \\u00e7ok hangi meyve \\u00fcr\\u00fcn\\u00fcn\\u00fc t\\u00fcketti\\u011finizi l\\u00fctfen belirtiniz.\"},{\"title\":\"haftada 3-5 kez\",\"is_if\":\"En \\u00e7ok hangi meyve \\u00fcr\\u00fcn\\u00fcn\\u00fc t\\u00fcketti\\u011finizi l\\u00fctfen belirtiniz.\"},{\"title\":\"her g\\u00fcn\",\"is_if\":\"En \\u00e7ok hangi meyve \\u00fcr\\u00fcn\\u00fcn\\u00fc t\\u00fcketti\\u011finizi l\\u00fctfen belirtiniz.\"}]', '2020-03-03 08:06:56', '2020-03-03 08:06:56', NULL),
(130, 'tr', 134, 'Kırmızı et tüketim sıklığınız nedir ?', '[{\"title\":\"t\\u00fcketmiyorum\",\"is_if\":null},{\"title\":\"nadir\",\"is_if\":null},{\"title\":\"haftada 1-2 kez\",\"is_if\":null},{\"title\":\"haftada 3-5 kez\",\"is_if\":null},{\"title\":\"her g\\u00fcn\",\"is_if\":null}]', '2020-03-03 08:08:16', '2020-03-03 08:08:16', NULL),
(131, 'tr', 135, 'Tavuk etit tüketim sıklığınız nedir ?', '[{\"title\":\"t\\u00fcketmiyorum\",\"is_if\":null},{\"title\":\"nadir\",\"is_if\":null},{\"title\":\"haftada 1-2 kez\",\"is_if\":null},{\"title\":\"haftada 3-5 kez\",\"is_if\":null},{\"title\":\"her g\\u00fcn\",\"is_if\":null}]', '2020-03-03 08:09:15', '2020-03-03 08:09:15', NULL),
(132, 'tr', 136, 'Deniz ürünü tüketim sıklığınız nedir ?', '[{\"title\":\"t\\u00fcketmiyorum\",\"is_if\":null},{\"title\":\"nadir\",\"is_if\":null},{\"title\":\"haftada 1-2 kez\",\"is_if\":null},{\"title\":\"haftada 3-5 kez\",\"is_if\":null},{\"title\":\"her g\\u00fcn\",\"is_if\":null}]', '2020-03-03 08:09:54', '2020-03-03 08:09:54', NULL),
(133, 'tr', 137, 'Süt tüketim sıklığınız nedir?', '[{\"title\":\"t\\u00fcketmiyorum\",\"is_if\":null},{\"title\":\"nadir\",\"is_if\":null},{\"title\":\"haftada 1-2 kez\",\"is_if\":null},{\"title\":\"haftada 3-5 kez\",\"is_if\":null},{\"title\":\"her g\\u00fcn\",\"is_if\":null}]', '2020-03-03 08:10:30', '2020-03-03 08:10:30', NULL),
(134, 'tr', 138, 'Yoğurt tüketim sıklığınız nedir?', '[{\"title\":\"t\\u00fcketmiyorum\",\"is_if\":null},{\"title\":\"nadir\",\"is_if\":null},{\"title\":\"haftada 1-2 kez\",\"is_if\":null},{\"title\":\"haftada 3-5 kez\",\"is_if\":null},{\"title\":\"her g\\u00fcn\",\"is_if\":null}]', '2020-03-03 08:11:14', '2020-03-03 08:11:14', NULL),
(135, 'tr', 139, 'Peynir tüketim sıklığınız nedir?', '[{\"title\":\"t\\u00fcketmiyorum\",\"is_if\":null},{\"title\":\"nadir\",\"is_if\":null},{\"title\":\"haftada 1-2 kez\",\"is_if\":null},{\"title\":\"haftada 3-5 kez\",\"is_if\":null},{\"title\":\"her g\\u00fcn\",\"is_if\":null}]', '2020-03-03 08:12:00', '2020-03-03 08:12:00', NULL),
(136, 'tr', 140, 'Öğünleri evde yeme sıklığınız nedir ?', '[{\"title\":\"nadir\",\"is_if\":null},{\"title\":\"haftada 1-2 kez\",\"is_if\":null},{\"title\":\"haftada 3-5 kez\",\"is_if\":null},{\"title\":\"her g\\u00fcn (ak\\u015fam)\",\"is_if\":null}]', '2020-03-03 08:13:05', '2020-03-03 08:13:05', NULL),
(137, 'tr', 141, 'Günlük 1 litreden fazla su tüketim sıklığınız nedir?', '[{\"title\":\"hi\\u00e7bir zaman\",\"is_if\":null},{\"title\":\"her g\\u00fcn\",\"is_if\":null},{\"title\":\"haftada 1-2 kez\",\"is_if\":null},{\"title\":\"haftada 3-5 kez\",\"is_if\":null},{\"title\":\"nadir olarak\",\"is_if\":null}]', '2020-03-03 08:14:01', '2020-03-03 08:14:01', NULL),
(138, 'tr', 142, 'Günlük siyah çay tüketimi (çay bardağı) sıklığınız nedir ?', '[{\"title\":\"t\\u00fcketmem\",\"is_if\":null},{\"title\":\"1-2 bardak\",\"is_if\":null},{\"title\":\"3-5 bardak\",\"is_if\":null},{\"title\":\"5-8 bardak aras\\u0131\",\"is_if\":null},{\"title\":\"8 bardaktan fazla\",\"is_if\":null}]', '2020-03-03 08:15:34', '2020-03-03 08:15:34', NULL),
(139, 'tr', 143, 'Haftalık kahve tüketim sıklığınız nedir ?', '[{\"title\":\"nadir\",\"is_if\":null},{\"title\":\"haftada 1-2 kez\",\"is_if\":null},{\"title\":\"haftada 3-5 kez\",\"is_if\":null},{\"title\":\"her g\\u00fcn\",\"is_if\":\"g\\u00fcnl\\u00fck hahve t\\u00fcketim s\\u0131kl\\u0131\\u011f\\u0131n\\u0131z nedir ( ka\\u00e7 bardak )?\"}]', '2020-03-03 08:20:28', '2020-03-03 08:20:28', NULL),
(140, 'tr', 144, 'Bitki çayı tüketir misiniz ?', '[{\"title\":\"Hay\\u0131r\",\"is_if\":null},{\"title\":\"Evet\",\"is_if\":\"hangi bitki \\u00e7aylar\\u0131n\\u0131 ne s\\u0131kl\\u0131kta t\\u00fcketti\\u011finizi l\\u00fctfen belirtiniz.\"}]', '2020-03-03 08:21:30', '2020-03-03 08:21:30', NULL),
(141, 'tr', 145, 'Şimdiye kadar kilo almak/vermek için herhangi bir yol denediniz mi ?', '[{\"title\":\"Hay\\u0131r\",\"is_if\":null},{\"title\":\"Evet\",\"is_if\":\"hangi y\\u00f6ntemi kulland\\u0131n\\u0131z ?\"}]', '2020-03-03 08:47:00', '2020-03-03 08:47:00', NULL),
(142, 'tr', 146, 'başarılı olabildiniz mi ?', '[{\"title\":\"Hay\\u0131r\",\"is_if\":\"Sizce sebebi ne olabilir ?\"},{\"title\":\"Evet\",\"is_if\":null}]', '2020-03-03 08:48:03', '2020-03-03 08:48:03', NULL),
(149, 'tr', 153, 'awd', '[{\"title\":\"wadawd wdd\",\"is_if\":\"awd adw adwdwa dawd\"},{\"title\":\"awd wdw dw d ddddddddddddddddd\",\"is_if\":\"dwddddddddddddddddddddddddddd\"},{\"title\":\"awd awdw adwada da daw\",\"is_if\":\"ddddddddddddddd222222222222\"}]', '2020-03-10 12:14:07', '2020-03-10 12:14:07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `resources`
--

CREATE TABLE `resources` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `video` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `embed_video` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `resources`
--

INSERT INTO `resources` (`id`, `video`, `embed_video`, `description`, `deleted_at`, `created_at`, `updated_at`) VALUES
(4, 'https://www.youtube.com/watch?v=AB-YtkGqEUU', 'https://www.youtube.com/embed/AB-YtkGqEUU', NULL, '2020-03-26 09:35:46', '2020-03-25 15:04:22', '2020-03-26 09:35:46'),
(5, 'https://www.youtube.com/watch?v=AB-YtkGqEUU', 'https://www.youtube.com/embed/AB-YtkGqEUU', '123321', '2020-03-26 09:20:52', '2020-03-26 09:20:30', '2020-03-26 09:20:52'),
(6, 'https://www.youtube.com/watch?v=AB-YtkGqEUU', 'https://www.youtube.com/embed/AB-YtkGqEUU', NULL, '2020-03-26 12:23:38', '2020-03-26 12:23:34', '2020-03-26 12:23:38'),
(7, 'https://www.youtube.com/watch?v=AB-YtkGqEUU', 'https://www.youtube.com/embed/AB-YtkGqEUU', NULL, '2020-03-27 10:46:25', '2020-03-26 12:32:33', '2020-03-27 10:46:25'),
(8, 'https://www.youtube.com/watch?v=AB-YtkGqEUU', 'https://www.youtube.com/embed/AB-YtkGqEUU', NULL, '2020-03-27 10:46:37', '2020-03-27 10:46:31', '2020-03-27 10:46:37'),
(9, 'https://www.youtube.com/watch?v=AB-YtkGqEUU', 'https://www.youtube.com/embed/AB-YtkGqEUU', NULL, '2020-03-31 09:53:35', '2020-03-31 09:53:27', '2020-03-31 09:53:35');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('J3WSZdq0IbKZNDBUNdAp0rb6JjfNJk6k0c2DI9XJ', NULL, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.100 Safari/537.36', 'YTozOntzOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjY6Il90b2tlbiI7czo0MDoiMHViUldUWEdZa1pDMXNSSnN0Mld0aDlNSTBZUWp5bkwySW5FWnhSaCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzI6Imh0dHA6Ly9sb2NhbGhvc3QvZW5iaW9zaXMvcHVibGljIjt9fQ==', 1581605498),
('km8mjxkF0ijsIIivgOcGYuy78GX7ukbuJFFWbjMd', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.100 Safari/537.36', 'YTo1OntzOjY6Il90b2tlbiI7czo0MDoia3Jqdnh0SjNnRXFyM1JWdzVjOHc1RmVxZDgycUZIdndFb05FMUFzNyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NDU6Imh0dHA6Ly9sb2NhbGhvc3QvZW5iaW9zaXMvcHVibGljL2VtYWlsL3ZlcmlmeSI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fXM6MzoidXJsIjthOjE6e3M6ODoiaW50ZW5kZWQiO3M6Mzc6Imh0dHA6Ly9sb2NhbGhvc3QvZW5iaW9zaXMvcHVibGljL3VzZXIiO31zOjUwOiJsb2dpbl93ZWJfNTliYTM2YWRkYzJiMmY5NDAxNTgwZjAxNGM3ZjU4ZWE0ZTMwOTg5ZCI7aToxO30=', 1581605342);

-- --------------------------------------------------------

--
-- Table structure for table `suggestions`
--

CREATE TABLE `suggestions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kit_id` bigint(20) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `suggestions`
--

INSERT INTO `suggestions` (`id`, `title`, `description`, `kit_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'wdawd', 'awawdadwddwdwdawawdadwddwdwdawawdadwddwdwdawawdadwddwdwdawawdadwddwdwdawawdadwddwdwdawawdadwddwdwdawawdadwddwdwdawawdadwddwdwdawawdadwddwdwdawawdadwddwdwdawawdadwddwdwdawawdadwddwdwdawawdadwddwdwdawawdadwddwdwdawawdadwddwdwdawawdadwddwdwdawawdadwddwdwdawawdadwddwdwdawawdadwddwdwdawawdadwddwdwd', 582, '2020-03-30 13:19:59', '2020-03-30 13:10:27', '2020-03-30 13:19:59'),
(2, 'awdawda wdaw dwa d', 'awdawdawdwadwadwaawdawdawdawdwadawdwadwadwadadwawdwadwadawdawdwada', 582, '2020-03-30 13:20:48', '2020-03-30 13:20:10', '2020-03-30 13:20:48'),
(3, 'rgrgdrg', 'dwdddwdwrrgrgrgddsdrgrgd', 582, '2020-03-30 13:22:51', '2020-03-30 13:20:59', '2020-03-30 13:22:51'),
(4, 'wad awda wd', 'awdawdwadwadawd', 582, '2020-03-30 13:22:53', '2020-03-30 13:21:36', '2020-03-30 13:22:53'),
(5, 'awdawdaw', '<p>wad awd awda dawd awd wadrgf ergrh tht jt j</p><p>tj tjtjht hjt jty j</p><p>tyj tyj tyj tyj t egerger g er</p><p>ge ge rge rge grerge g&nbsp;</p><p>erge rge rger gerg erg erg hrthr</p><p>&nbsp;</p>', 582, '2020-03-30 13:22:56', '2020-03-30 13:22:09', '2020-03-30 13:22:56'),
(6, 'daw daw d', '<p>adawdwa dwa dwad waawd awd rgerg regdrg rdg</p>', 582, '2020-03-30 13:24:48', '2020-03-30 13:23:02', '2020-03-30 13:24:48'),
(7, 'wadwadawd', '<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</p>', 582, '2020-03-30 13:24:24', '2020-03-30 13:24:15', '2020-03-30 13:24:24'),
(8, 'wadawdawd', '<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>', 582, '2020-03-30 13:24:45', '2020-03-30 13:24:34', '2020-03-30 13:24:45'),
(9, 'aw daw dw d', '<p>wd awd wad wadwd awd ad</p>', 582, NULL, '2020-03-30 14:51:05', '2020-03-30 14:51:05'),
(10, '1233321', '<p>123333212e eq 3eq eq qe qw q w</p>', 582, '2020-03-31 10:33:52', '2020-03-31 10:33:48', '2020-03-31 10:33:52'),
(11, 'www', '<p>wwwwwwwwwww</p>', 581, NULL, '2020-03-31 11:30:57', '2020-03-31 11:30:57'),
(12, 'awdawd', '<p>awd awdwad&nbsp; dwad awdaw d aw</p>', 581, NULL, '2020-03-31 11:36:58', '2020-03-31 11:36:58'),
(13, 'awd wadwadawd', '<p>awd awd awdaw d</p>', 581, NULL, '2020-03-31 11:37:03', '2020-03-31 11:37:03'),
(14, '1', '<p>1</p>', 578, NULL, '2020-03-31 11:45:20', '2020-03-31 11:45:20'),
(15, 'wdwwd', '<p>wadadw dawd w</p>', 572, '2020-04-08 09:16:40', '2020-04-08 09:16:28', '2020-04-08 09:16:40'),
(16, 'wwwww', '<p>wdw dada wd awd&nbsp;</p>', 606, '2020-04-08 10:51:58', '2020-04-08 10:51:55', '2020-04-08 10:51:58');

-- --------------------------------------------------------

--
-- Table structure for table `surveys`
--

CREATE TABLE `surveys` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `surveys`
--

INSERT INTO `surveys` (`id`, `admin_id`, `publish`, `deleted_at`, `created_at`, `updated_at`) VALUES
(16, 1, 1, NULL, '2020-03-02 13:22:15', '2020-03-03 12:41:00');

-- --------------------------------------------------------

--
-- Table structure for table `survey_langs`
--

CREATE TABLE `survey_langs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `survey_id` bigint(20) UNSIGNED NOT NULL,
  `lang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'tr',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `survey_langs`
--

INSERT INTO `survey_langs` (`id`, `survey_id`, `lang`, `title`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(29, 16, 'tr', 'ENBIOSIS KULLANICI ANKETİ', '<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>', '2020-03-02 13:22:15', '2020-03-30 12:30:33', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `survey_sections`
--

CREATE TABLE `survey_sections` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `survey_id` bigint(20) UNSIGNED NOT NULL,
  `section_order` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `duration` int(10) UNSIGNED NOT NULL DEFAULT 5,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `survey_sections`
--

INSERT INTO `survey_sections` (`id`, `survey_id`, `section_order`, `duration`, `created_at`, `updated_at`, `deleted_at`) VALUES
(55, 16, 1, 5, '2020-03-02 13:22:17', '2020-03-02 13:22:17', NULL),
(56, 16, 2, 5, '2020-03-02 13:52:00', '2020-03-05 04:44:26', NULL),
(57, 16, 3, 5, '2020-03-03 07:01:28', '2020-03-05 04:45:06', NULL),
(58, 16, 4, 5, '2020-03-03 07:13:50', '2020-03-05 04:49:07', NULL),
(59, 16, 5, 5, '2020-03-03 07:35:43', '2020-03-05 04:49:12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `survey_section_langs`
--

CREATE TABLE `survey_section_langs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `survey_section_id` bigint(20) UNSIGNED NOT NULL,
  `lang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'tr',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `survey_section_langs`
--

INSERT INTO `survey_section_langs` (`id`, `survey_section_id`, `lang`, `title`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(47, 55, 'tr', 'YAŞAM TARZI', '<p>YAŞAM TARZI</p>', '2020-03-02 13:22:17', '2020-03-02 13:22:31', NULL),
(48, 56, 'tr', 'SAĞLIK', '<p><strong>SAĞLIK</strong></p>', '2020-03-02 13:52:00', '2020-03-02 13:52:00', NULL),
(49, 57, 'tr', 'ALERJİ', '<p><strong>ALERJİ</strong></p>', '2020-03-03 07:01:28', '2020-03-03 07:01:28', NULL),
(50, 58, 'tr', 'İLAÇ', '<p>İLA&Ccedil;</p>', '2020-03-03 07:13:50', '2020-03-03 07:13:50', NULL),
(51, 59, 'tr', 'BESLENME ALIŞKANLIKLARI', '<p><strong>BESLENME ALIŞKANLIKLARI</strong></p>', '2020-03-03 07:35:43', '2020-03-03 07:35:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `dietitian_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` tinyint(1) NOT NULL DEFAULT 0,
  `date_of_birth` date DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vegetarian` tinyint(1) NOT NULL DEFAULT 0,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `verify_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `dietitian_id`, `name`, `email`, `email_verified_at`, `password`, `gender`, `date_of_birth`, `address`, `phone`, `vegetarian`, `remember_token`, `deleted_at`, `created_at`, `updated_at`, `verify_code`, `expires_at`) VALUES
(3, NULL, 'Emad Menkar', 'Emenkar@enbiosis.com', NULL, '$2y$10$86vKsnNAlobPYy7uwq5dFeu.OIkLwkoCF2P5EZwTfM9pZyMFbtXPy', 0, NULL, NULL, NULL, 0, NULL, NULL, '2020-02-05 06:09:47', '2020-02-05 06:09:47', NULL, NULL),
(4, NULL, 'LAith', 'lalla', NULL, 'wadwadwa\n', 2, NULL, NULL, NULL, 0, NULL, NULL, '2020-02-12 11:40:05', '2020-02-12 11:40:05', NULL, NULL),
(5, NULL, 'la@la.com', 'la@la.com', NULL, '$2y$10$g72s9R.XSpRi0yHbA7dw7OxMRgujPrZNUF7UnJm15Y25QFLxnAwYq', 1, NULL, 'latik@enbiosis.com', '12345678901', 0, NULL, NULL, '2020-02-12 12:23:01', '2020-02-12 12:23:01', NULL, NULL),
(6, NULL, 'Laith Atik', 'lak@gmail.com', NULL, '$2y$10$8etIvZrapsau6KHBJDnSSeCI.Dz7vFo0TjBIA4z7iFfWrG3Gz2t9S', 1, NULL, 'Bağlar Mh Hakan Kurtulan Sk No 8/3', '05348447726', 0, NULL, NULL, '2020-02-12 12:27:24', '2020-02-13 12:18:22', NULL, NULL),
(7, NULL, 'Laith Atik', 'laythateek@gmail.com', '2020-04-07 15:21:49', '$2y$10$19FclctY9r/1iEA7D8wJMut1JDF3jauIJoxPPMs08ZVTXbgj.fvJi', 1, '2020-05-07', 'Bağlar Mh Hakan Kurtulan Sk No 8/3', '05348447726', 0, 'zMMd6zccztXWwbqW1GqLyB7x49b4xFXTwFPMPhMXIFUaxzuLD7nBFGtNL45K', NULL, '2020-02-17 05:18:03', '2020-05-27 15:08:58', NULL, NULL),
(9, NULL, 'Laith Atik', 'laithateek@gmail.com', '2020-02-20 09:19:33', '$2y$10$umbTV1Sqq5WvBUuskO39b.9hB0zBj1XJBxmj89/prd1gKuuC8ZPcu', 1, NULL, 'Bağlar Mh Hakan Kurtulan Sk No 8/3', '05348447726', 0, NULL, NULL, '2020-02-20 09:19:06', '2020-02-20 09:19:33', NULL, NULL),
(10, NULL, 'Laith Atik', 'laythateek@gmail.comwwww', NULL, '$2y$10$p8g3k43qSbQm4e4uJdqwh.kZyvCG6goLWt/PotbKxpMYAu00VtGOa', 1, NULL, 'Bağlar Mh Hakan Kurtulan Sk No 8/3', '05348447726', 0, NULL, NULL, '2020-02-26 08:45:24', '2020-02-26 08:45:24', NULL, NULL),
(13, NULL, 'laith', 'laythateek11@gmail.com', NULL, '$2y$10$BYhBAOUZNyi6mhWxxAU2Zu477Zsv8NfuKSqyTTPtGHZiHtUVXsm/u', 0, NULL, 'laith atik', '12345678910', 0, NULL, NULL, '2020-02-26 09:54:40', '2020-02-26 09:54:40', NULL, NULL),
(15, NULL, 'Laith Atik', 'latik@aisolt.com', '2020-03-02 11:32:00', '$2y$10$/z2sUEUdG9AIvWPHVxxWfusv4oj7.qNSmh/QsLxSJ5X/kWof8mweu', 1, NULL, 'Bağlar Mh Hakan Kurtulan Sk No 8/3', '05348447726', 0, NULL, NULL, '2020-03-02 11:30:52', '2020-03-02 11:32:00', NULL, NULL),
(16, NULL, 'laith', 'laythateek@atikla.com', NULL, '$2y$10$3HSrUm4G1qtKP/5wqnVChOu4SO3znABBmJKCFgPVyqZQ06nMEOXKG', 0, NULL, 'laith atik', '12345678910', 0, NULL, NULL, '2020-03-03 11:06:37', '2020-04-08 16:29:20', NULL, NULL),
(17, NULL, 'Laith Atik1', 'laithatik@gmail.com', '2020-03-05 07:29:12', '$2y$10$Xk3RNaM3RYk3u5c0Bfn2SOwOkLxfSsN/gW7zB9L0o5QpdcEz3Yepy', 1, NULL, 'Bağlar Mh Hakan Kurtulan Sk No 8/3', '12332112331', 1, NULL, NULL, '2020-03-05 07:28:48', '2020-04-08 11:18:05', NULL, NULL),
(118, NULL, 'Omar Taha', 'ot.themechanic95@gmail.com', '2020-04-09 09:33:21', '$2y$10$c6yCj6MP2Terp31FrTld8eN9Rt1MbZBMbc.wwhfimqhZanZ43h97q', 0, NULL, 'ISTANBUL', '45345345345345', 0, NULL, NULL, '2020-04-09 09:25:39', '2020-04-09 09:33:21', NULL, NULL),
(124, NULL, 'Omar Taha', 'otaha@aisolt.com', '2020-05-13 18:26:28', '$2y$10$mKpXtUeXNDqAY2Blj7EQKugeWiMy3aycAvi0vj9ovWn2wUadQQnYO', 1, '1999-10-20', '12333211', '1231423534534', 0, NULL, NULL, '2020-05-13 18:25:27', '2020-05-21 16:15:42', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_log`
--
ALTER TABLE `activity_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activity_log_log_name_index` (`log_name`),
  ADD KEY `subject` (`subject_id`,`subject_type`),
  ADD KEY `causer` (`causer_id`,`causer_type`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `answers_kit_id_index` (`kit_id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `counties`
--
ALTER TABLE `counties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `coupons_code_unique` (`code`);

--
-- Indexes for table `dietitians`
--
ALTER TABLE `dietitians`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `dietitians_email_unique` (`email`),
  ADD UNIQUE KEY `dietitians_reference_code_unique` (`reference_code`);

--
-- Indexes for table `errors`
--
ALTER TABLE `errors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kits`
--
ALTER TABLE `kits`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kits_kit_code_unique` (`kit_code`),
  ADD KEY `survey_id` (`survey_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `orders_tracking_unique` (`tracking`),
  ADD UNIQUE KEY `orders_payment_id_unique` (`payment_id`),
  ADD UNIQUE KEY `orders_coupon_id_unique` (`coupon_id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`eee`),
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `products_slug_unique` (`slug`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `questions_survey_section_id_index` (`survey_section_id`);

--
-- Indexes for table `question_langs`
--
ALTER TABLE `question_langs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `question_langs_question_id_index` (`question_id`);

--
-- Indexes for table `resources`
--
ALTER TABLE `resources`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `suggestions`
--
ALTER TABLE `suggestions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `suggestions_kit_id_index` (`kit_id`);

--
-- Indexes for table `surveys`
--
ALTER TABLE `surveys`
  ADD PRIMARY KEY (`id`),
  ADD KEY `surveys_admin_id_index` (`admin_id`);

--
-- Indexes for table `survey_langs`
--
ALTER TABLE `survey_langs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `survey_langs_survey_id_index` (`survey_id`);

--
-- Indexes for table `survey_sections`
--
ALTER TABLE `survey_sections`
  ADD PRIMARY KEY (`id`),
  ADD KEY `survey_sections_survey_id_index` (`survey_id`);

--
-- Indexes for table `survey_section_langs`
--
ALTER TABLE `survey_section_langs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `survey_section_langs_survey_section_id_index` (`survey_section_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=350;

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `answers`
--
ALTER TABLE `answers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2430;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `counties`
--
ALTER TABLE `counties`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=897;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=185;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `dietitians`
--
ALTER TABLE `dietitians`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `errors`
--
ALTER TABLE `errors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `kits`
--
ALTER TABLE `kits`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=609;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `password_resets`
--
ALTER TABLE `password_resets`
  MODIFY `eee` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `photos`
--
ALTER TABLE `photos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=154;

--
-- AUTO_INCREMENT for table `question_langs`
--
ALTER TABLE `question_langs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=150;

--
-- AUTO_INCREMENT for table `resources`
--
ALTER TABLE `resources`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `suggestions`
--
ALTER TABLE `suggestions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `surveys`
--
ALTER TABLE `surveys`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `survey_langs`
--
ALTER TABLE `survey_langs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `survey_sections`
--
ALTER TABLE `survey_sections`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `survey_section_langs`
--
ALTER TABLE `survey_section_langs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
