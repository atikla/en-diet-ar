<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Survey;


class Survey extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'admin_id'
    ];

    public function admin()
    {
        return $this->belongsTo('App\Admin');
    }

    public function surveySection()
    {
        return $this->hasMany('App\SurveySection')->orderBy('section_order');
    }

    public function surveyLang()
    {
        return $this->hasMany('App\SurveyLang');
    }

    public function getNumberOfSectionAttribute()
    {
        return $this->surveySection()->count();
    }

    public function kits()
    {
        return $this->hasMany('App\kit');
    }

    public function getDefaultLanguageAttribute()
    {
        $default = SurveyLang::where('survey_id', '=', $this->id)
            ->where('lang', '=', 'tr')
            ->first();
        return $default;
    }

    public function getSurveyAttribute()
    {
        $survey = new Survey();
        $survey->publisher = $this->admin->name;
        $surveyLang = $this->surveyLang
            ->where('lang', '=', app()->getlocale())
            ->first();

        if ( $surveyLang == null ) 
            $surveyLang = $this->defaultLanguage;

        $survey->lang = $surveyLang->lang;
        $survey->title = $surveyLang->title;
        $survey->description = strip_tags( $surveyLang->description );
        $survey->sections = $this->surveySection
            ->filter(function ($section) {

            $surveySectionLang = $section->surveySectionLang
                ->where('lang', '=', app()->getlocale())
                ->first();

            if ( $surveySectionLang == null ) # if local lang does not exist in db will be get default lang
                $surveySectionLang = $section->defaultLanguage;

            $section->lang = $surveySectionLang->lang;
            $section->title = $surveySectionLang->title;
            $section->description = strip_tags( $surveySectionLang->description );
            unset($section->surveySectionLang);
            if ( $section->question->count() == 0  )
                return null;
            $section->questions =   $section->question
                ->filter(function ($question) {

                $questionLang = $question->questionLang
                    ->where('lang', '=', app()->getlocale())
                    ->first();

                if ( $questionLang == null ) # if local lang does not exist in db will be get default lang
                    $questionLang = $question->defaultLanguage;

                $question->lang = $questionLang->lang;
                $question->title = $questionLang->title;
                $question->option_name = $questionLang->option_name;
                $question->Answer = null;
                if ( $question->answer->where('kit_id', '=', $this->kitid)->first() )
                    $question->Answer = $question->answer->where('kit_id', '=', $this->kitid)->first()->answer;
                unset( $question->questionLang );
                unset( $question->answer );
                return $question;
            });
            unset( $section->question );
            return $section;
        });
        return $survey;
    }

    public function getDietitianSurveyAttribute()
    {
        $data = $this->Survey;
        foreach ($data['sections'] as $sectionKey => $section) {
            foreach ( $section['questions'] as $questionKey => $question ) {
                if ( $question->option_name ) {
                    $newAnswer = [];
                    foreach( $question->option_name as $key => $option ) {
                        if (!$question['Answer'])
                            continue;
                        foreach( $question['Answer'] as  $answerKey => $answerValue) {
                            if ( array_key_exists( '"'. $key . '"', $answerValue) ) {
                                $answerArray = [
                                    'title' => $option['title'],
                                    'is_if' => null
                                ];
                                if ( $answerValue['"'. $key . '"'] )
                                    $answerArray['is_if'] = ['is_if_title' => $option['is_if'], 'is_if_ans' =>  $answerValue['"'. $key . '"']];
                                array_push($newAnswer, $answerArray);
                                
                            }
                        }
                    }
                    $question['Answer'] = $newAnswer;
                }
                unset($question['option_name']);
            }
        }
        return $data;
    }

}
