<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Coupon extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'discount', 'used', 'note', 'reference_code', 'campaign', 'expires_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'campaign'      => 'boolean',
        'expires_at'    => 'datetime',
    ];
    
    protected $primarykey='id';

    public function Order()
    {
        return $this->hasMany('App\Order')->orderBy('id', 'DESC');
    }

    public function dietitian()
    {
        return $this->hasOne('App\Dietitian', 'reference_code', 'reference_code');
    }

    public function getReferenceAttribute()
    {
        if ( $this->dietitian )
            return $this->dietitian->kind . '. ' . $this->dietitian->name . ' - ' . $this->dietitian->reference_code;
        else 
            return null;
    }
    public function getStatusAttribute()
    {
        if ( $this->used == 0 ) {
            return 'Not Used Yet!';
        } else {
            return '<a href="' . route('admin.orders.show', [ $this->order->first()->id ]) .'">used in this order</a>';
        } 
    }

}
