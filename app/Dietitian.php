<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;

class Dietitian extends Authenticatable
{
    use SoftDeletes, HasApiTokens;
    
    protected $guard = 'dietitian';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'address', 'password', 'phone', 'email_verified_at', 'reference_code', 'city_id', 'type', 'verify_code', 'expires_at', 'selling_team_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'verify_code', 'expires_at'
    ];

      /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'expires_at' => 'datetime',
    ];

    public function photo()
    {
        return $this->morphOne('App\Photo', 'imageable');
    }

    public function kits()
    {
        return $this->hasMany('App\Kit');
    }

    public function orders()
    {
        return $this->hasMany('App\Order', 'dietitian_code', 'reference_code')->orderBy('id', 'DESC');
    }

    public function city()
    {
        return $this->belongsTo('App\City');
    }

    public function sellingTeam()
    {
        return $this->belongsTo('App\SellingTeam');
    }


    public function getIsLeaderAttribute()
    {
        if ( $this->sellingTeam && $this->sellingTeam->leader_id == $this->id)
            return true;
        else 
            return false;
    }


    public function getStrTeamAttribute()
    {
       if ( $this->sellingTeam ) {
            if ( $this->isLeader ) {
                return 'leader for ' . $this->sellingTeam->name . ' team';
            }else {
                return 'member in ' . $this->sellingTeam->name . ' team';
            }
       }else {
           return 'not belogs to any team';
       }
    }
    
    public function getKindAttribute()
    {
        if ( $this->type  == 0 ) {
            return 'Dietitian';
        } elseif ( $this->type  == 1 ) {
            return 'Doctor';
        } elseif ( $this->type  == 2 ) {
            return 'Pharmacist';
        } else {
            return 'Dietitian';
        }
    }

    public function setReferenceCodeAttribute($value)
    {
        $this->attributes['reference_code'] = strtoupper($value);
    }

    public function coupon()
    {
        return $this->belongsTo('App\Coupon', 'reference_code', 'reference_code');
    }

    public function getStrRoleAttribute()
    {
        return $this->kind;
    }
}
