<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Resource extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'video', 'embed_video', 'description'
    ];

    public function photo()
    {
        return $this->morphOne('App\Photo', 'imageable');
    }
}
