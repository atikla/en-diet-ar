<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use SoftDeletes;
    protected $guard = 'admin';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'phone', 'password', 'role', 'active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'active' => 'boolean',
    ];
    public function photo()
    {
        return $this->morphOne('App\Photo', 'imageable');
    }

    public function products ()
    {
        return $this->hasMany('App\Product');
    }

    public function survey()
    {
        return $this->hasMany('App\Survey');
    }

    public function getStrRoleAttribute()
    {
        if ($this->role == 0 )
            return 'Super Admin';
        else if ( $this->role == 1 )
            return 'Operation Admin';
        else if ( $this->role == 2 )
            return 'Laboratory Admin';
        else 
        return 'Draft Admin';
    }
            
}
