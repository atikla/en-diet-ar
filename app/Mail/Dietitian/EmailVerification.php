<?php

namespace App\Mail\Dietitian;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Dietitian;

class EmailVerification extends Mailable
{
    use Queueable, SerializesModels;

    /**
    * The Kit instance.
    *
    * @var user
    */
    protected $dietitian;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Dietitian $dietitian)
    {
        $this->dietitian = $dietitian;
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.user.EmailVerification')
            ->subject("E-posta Adresini Doğrula")
            ->with([ 'verify_code' => $this->dietitian->verify_code ]);
    }
}
