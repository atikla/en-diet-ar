<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;

class EmailVerification extends Mailable
{
    use Queueable, SerializesModels;

    /**
    * The Kit instance.
    *
    * @var user
    */
    protected $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.user.EmailVerification')
            ->subject("E-posta Adresini Doğrula")
            ->with([ 'verify_code' => $this->user->verify_code ]);
    }
}
