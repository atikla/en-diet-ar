<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendPdf extends Mailable
{
    use Queueable, SerializesModels;

    protected $food;
    protected $microbiome;
    protected $kit_code;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($food, $microbiome, $kit_code)
    {
        $this->food = $food;
        $this->microbiome = $microbiome;
        $this->kit_code = $kit_code;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.kit.SendPdf')
                ->subject("ENBIOSIS Mikrobiyom Analiz Sonuçlarınız")
                ->attach($this->food, [
                    'as' => __('food.file_name') . '.pdf',
                    'mime' => 'application/pdf',
                ])
                ->attach($this->microbiome, [
                    'as' => __('mikrobyom.file_name') . '.pdf', 
                    'mime' => 'application/pdf',
                ])
                ->with([
                    'kit_code' => $this->kit_code,
                ]);
    }
}
