<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Order;

class OrderShipped extends Mailable
{
    use Queueable, SerializesModels;

    /**
    * The order instance.
    *
    * @var Order
    */
     protected $order;

    /**
    * Create a new message instance.
    *
    * @return void
    */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
    * Build the message.
    *
    * @return $this
    */
    public function build()
    {
        $city = '';
        $county = '';
        if ($this->order->city_id)
            $city = $this->order->city->city;

        if ($this->order->county_id)
            $county = $this->order->county->county;

        $address = $this->order->address . ' - ' . $county . ' - ' . $city;
        
        return $this->view('mail.order.index')
                    ->subject('تم اكمال طلبك بنجاح')
                    ->with(['order' => $this->order]);
    }
}
