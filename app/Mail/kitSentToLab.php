<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Kit;


class kitSentToLab extends Mailable
{
    use Queueable, SerializesModels;

    /**
    * The Kit instance.
    *
    * @var Kit
    */
    protected $kit;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Kit $kit)
    {
        $this->kit = $kit;
    }

     /**
    * Build the message.
    *
    * @return $this
    */
    public function build()
    {   
        
        return $this->view('mail.kit.kitsentyolab')
                    ->subject("Kitiniz Analizde")
                    ->with([
                        'kit_code' => $this->kit->kit_code,
                    ]);
    }
}
