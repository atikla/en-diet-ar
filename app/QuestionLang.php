<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuestionLang extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'question_id', 'option_name', 'lang', 'title'
    ];

    protected $casts = [
      'option_name' => 'array',
  ];

    public function Question()
    {
        return $this->belongsTo('App\Question');
    }

    public function getLanguageAttribute()
    {   $country = Country::where('code', '=', $this->lang)
            ->first();
        return $country->name;
    }
}
