<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Question extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $fillable = [
        'question_type', 'survey_section_id', 'question_order'
    ];

    public function surveySection()
    {
        return $this->belongsTo('App\SurveySection');
    }

    public function questionLang()
    {
        return $this->hasMany('App\QuestionLang');
    }

    public function answer()
    {
        return $this->hasMany('App\Answer');
    }

    public function getDefaultLanguageAttribute()
    {
        $default = QuestionLang::where('question_id', '=', $this->id)
            ->where('lang', '=', 'tr')
            ->first();
        return $default;
    }
}
