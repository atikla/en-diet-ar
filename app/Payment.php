<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{

     /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $fillable = [
        'ReturnOid', 'TRANID', 'PAResSyntaxOK', 'Filce', 'firmaadi', 'islemtipi', 'total1', 'lang', 'merchantID',
        'maskedCreditCard', 'amount', 'sID', 'ACQBIN', 'itemnumber1', 'Ecom_Payment_Card_ExpDate_Year', 'EXTRA_CARDBRAND',
        'storekey', 'MaskedPan', 'acqStan', 'Fadres', 'EXTRA_CAVVRESULTCODE', 'clientIp', 'iReqDetail', 'okUrl', 'md', 'ProcReturnCode',
        'payResults_dsId', 'vendorCode', 'taksit', 'TransId', 'EXTRA_TRXDATE', 'productcode1', 'Fil', 'Ecom_Payment_Card_ExpDate_Month',
        'storetype', 'iReqCode', 'Response', 'tulkekod', 'SettleId', 'mdErrorMsg', 'ErrMsg', 'EXTRA_SGKTUTAR', 'PAResVerified', 'cavv', 'id1',
        'digest', 'HostRefNum', 'callbackCall', 'AuthCode', 'failUrl', 'cavvAlgorithm', 'Fpostakodu', 'price1', 'faturaFirma', 'xid', 'encoding',
        'oid', 'mdStatus', 'dsId', 'eci', 'version', 'Fadres2', 'EXTRA_CARDHOLDERNAME', 'Fismi', 'qty1', 'desc1', 'EXTRA_CARDISSUER', 'clientid',
        'txstatus', '_charset_', 'HASH', 'rnd', 'HASHPARAMS', 'HASHPARAMSVAL'
    ];

    /**
     * Get the Order that owns the Payment.
     */
    public function order()
    {
        return $this->hasOne('App\Order');
    }

}
