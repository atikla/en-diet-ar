<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SurveySectionLang extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'survey_section_id', 'lang', 'title', 'description'
    ];

    public function surveySection()
    {
        return $this->belongsTo('App\SurveySection');
    }
    
    public function getLanguageAttribute()
    {   $country = Country::where('code', '=', $this->lang)
            ->first();
        return $country->name;
    }
}
