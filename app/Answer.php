<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Answer extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'kit_id', 'answer', 'question_id'
    ];

    protected $casts = [
      'answer' => 'array',
    ];

    public function question()
    {
        return $this->belongsTo('App\Question');
    }

    public function kit()
    {
        return $this->belongsTo('App\Kit');
    }
}
