<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Dietitian;

class SellingTeam extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'leader_id', 'note'
    ];

    public function cities()
    {
        return $this->belongsToMany('App\City', 'selling_team_city');
    }

    public function dietitians()
    {
        return $this->hasMany('App\Dietitian');
    }

    public function getLeaderAttribute()
    {
        if ( $this->leader_id ) {
            $leader = Dietitian::whereId( $this->leader_id )->first();
            if ( $leader )
                return $leader;
            else 
                return null;
        } else
            return null;
    }
}
