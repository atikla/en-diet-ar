<?php

if (!function_exists('colorScale')) {

    /**
     * description
     *
     * @param string $title
     * @return array
     */
    function colorScale($title)
    {
        $limitValues = config()->get('limitvalues.' . $title);
        $colors = config()->get('limitvalues.colors');
        if (!$limitValues) {
            $data = [
                ['color' => 'rgba(89,196,217,1)', 'rate' => 0],
                ['color' => 'rgba(237,209,161,1)', 'rate' => 35],
                ['color' => 'rgba(238,175,130,1)', 'rate' => 70],
                ['color' => 'rgba(227,99,79,1)', 'rate' => 100]
            ];
            return $data;
        }
        $data = [];
        foreach ($limitValues as $key => $value) {
            array_push($data, ['color' => $colors[$value], 'rate' => ( int ) $key ]);
        }
        return $data;
    }
}
