<?php

if (!function_exists('CloseProfleFilter')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function closeProfleFilter($closeProfles)
    {
        $data = array();
        if ( isset( $closeProfles['fazla kilolu'] ) )
            unset( $closeProfles['fazla kilolu'] );
        if ( isset( $closeProfles['vejetaryen'] ) )
            unset( $closeProfles['vejetaryen'] );
        if ( isset( $closeProfles['vejetaryen, ancak deniz ürünleri tüketiyor'] ) )
            unset( $closeProfles['vejetaryen, ancak deniz ürünleri tüketiyor'] );
        if ( isset( $closeProfles['kırmızı et yemiyor'] ) )
            unset( $closeProfles['kırmızı et yemiyor'] );
        if ( isset( $closeProfles['vegan besleniyor'] ) )
            unset( $closeProfles['vegan besleniyor'] );

        foreach ($closeProfles as $key => $value) {
            array_push($data, [ 'title'=> __('mikrobyom.'.$key), 'value' => ( int ) $value[0] ]);
        }
        return  $data ;
    }
}
