<?php
namespace App\Helpers;

use Illuminate\Http\Request;

class Payment
{
    public static function paymentOptions()
    {
        $options = new \Iyzipay\Options();
        // ForTest
        // $options->setApiKey("sandbox-9I4mNBWYXb72Q7NtSESBfmL2GFSWd1ly");
        // $options->setSecretKey("sandbox-h83nmH7HzzqktTLux74XSY82HRKwvdIr");
        // $options->setBaseUrl("https://sandbox-api.iyzipay.com");
        // For Live
        $options->setApiKey("SWX3E6Dj6qAS02AUfFchA0OKyPeysXss");
        $options->setSecretKey("iIc4n1hvB1oayt3OR6IHIhFKukl5tMs3");
        $options->setBaseUrl("https://api.iyzipay.com");
        return $options;
    }

    public static function paymentCard($cardHolderName, $cardNumber, $expireMonth, $expireYear, $cvc)
    {
        $paymentCard = new \Iyzipay\Model\PaymentCard();
        $paymentCard->setCardHolderName($cardHolderName);
        $paymentCard->setCardNumber($cardNumber);
        $paymentCard->setExpireMonth($expireMonth);
        $paymentCard->setExpireYear($expireYear);
        $paymentCard->setCvc($cvc);
        $paymentCard->setRegisterCard(0);
        return $paymentCard;
    }

    public static function buyer($name, $surname, $phone, $email, $address, $county, $city)
    {
        $buyer = new \Iyzipay\Model\Buyer();
        $buyer->setId(str_random(5));
        $buyer->setName($name);
        $buyer->setSurname($surname);
        $buyer->setGsmNumber($phone);
        $buyer->setEmail($email);
        $buyer->setIdentityNumber(11111111111);
        $buyer->setRegistrationAddress($address);
        $buyer->setIp(request()->ip());
        $city = \App\City::findOrFail($city);
        $country = 'Turkey';
        if ( $city ) {
            $buyer->setCity($city->city);
            $country = config('cities.' . $city->country_code .'.name');
        }else 
            $buyer->setCity('Istanbul');
        $buyer->setCountry($country);
        return $buyer;
    }

    public static function shippingAddress($name, $country, $city, $county, $address)
    {
        $shippingAddress = new \Iyzipay\Model\Address();
        $shippingAddress->setContactName($name);
        $city = \App\City::findOrFail($city);
        $shippingAddress->setCity($city->city . ' - ' . $city->county->where('id', '=', $county)->first()->county);
        $shippingAddress->setCountry(config('cities.' . $city->country_code .'.name'));
        $shippingAddress->setAddress($address);
        return $shippingAddress;
    }

    public static function billingAddress($name, $country, $city, $county, $address)
    {
        $billingAddress = new \Iyzipay\Model\Address();
        $billingAddress->setContactName($name);
        $city = \App\City::findOrFail($city);
        $billingAddress->setCity($city->city . ' - ' . $city->county->where('id', '=', $county)->first()->county);
        $billingAddress->setCountry(config('cities.' . $city->country_code .'.name'));
        $billingAddress->setAddress($address);
        return $billingAddress;
    }

    public static function basketItems($session, $slug, $name)
    {
        $basketItems = [];
        $BasketItem = new \Iyzipay\Model\BasketItem();
        $BasketItem->setId(session($session)['slug']);
        $BasketItem->setName(session($session)['name']);
        $BasketItem->setCategory1("biyoteknoloji");
        $BasketItem->setItemType(\Iyzipay\Model\BasketItemType::PHYSICAL);
        $BasketItem->setPrice(session($session)['price']);
        for ($i = 0; $i < session($session)['quantity']; $i++) {
            array_push($basketItems, $BasketItem);
        }
        return $basketItems;
    }
}