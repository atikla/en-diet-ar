<?php

if (!function_exists('finalTaksonomik')) {

    /**
     * description
     *
     * @param array $data
     * @return array
     */
    function finalTaksonomik($data)
    {
        foreach ( $data as $lv1key =>$taksonomik ) {
            foreach ( $taksonomik['items'] as $lv2key => $item ) {
                $temp = array_chunk($item['scores'], ( sizeof($item['scores']) / 2 ) + 1 );
                array_values($temp[1]);
                $data[$lv1key]['items'][$lv2key]['scores'] = [];
                array_multisort( array_map( function($i) {
                    return $i['value'];
                }, $temp[1] ), SORT_DESC, $temp[1] );
                foreach ($temp[0] as $key => $value) {
                    array_push($data[$lv1key]['items'][$lv2key]['scores'], $value);
                    if ( isset($temp[1][$key] ) )
                        array_push($data[$lv1key]['items'][$lv2key]['scores'], $temp[1][$key]);
                }
            }
        }
        return $data;
    }
}
