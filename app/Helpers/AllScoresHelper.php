<?php

if (!function_exists('allScores')) {  

    /**
     * description
     *
     * @return array
     */
    function allScores()
    {
        return [
            [
                'title' => __('api.iyi_Skor'),
                'analizType' => 'iyi',
                'items' => []
            ],
            [
                'title' => __('api.orta_Skor'),
                'analizType' => 'orta',
                'items' => []
            ],
            [
                'title' => __('api.kotu_Skor'),
                'analizType' => 'kotu',
                'items' => []
            ]
        ];
    }
}
