<?php
use Illuminate\Support\Str;
if (!function_exists('slug')) {

    /**
     * description
     *
     * @param string string
     * @param string separator
     * @return string 
     */
    function slug(string $string, string $separator = '_') : string
    {
        return Str::slug($string, $separator);
    }
}
