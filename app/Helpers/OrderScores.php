<?php

if (!function_exists('orderScores')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function orderScores($scores)
    {
        $data = [];
        $data['Vücut Kütle Endeksi-Mikrobiyom İlişkiniz']   = $scores['Vücut Kütle Endeksi-Mikrobiyom İlişkiniz'];
        $data['Karbonhidrat Metabolizmanız']                = $scores['Karbonhidrat Metabolizmanız'];
        $data['Protein Metabolizmanız']                     = $scores['Protein Metabolizmanız'];
        $data['Yağ Metabolizmanız']                         = $scores['Yağ Metabolizmanız'];
        $data['Vitamin Senteziniz']                         = $scores['Vitamin Senteziniz'];
        $data['Laktoz Hassasiyeti Ölçümünüz']               = $scores['Laktoz Hassasiyeti Ölçümünüz'];
        $data['Gluten Hassasiyeti Ölçümünüz']               = $scores['Gluten Hassasiyeti Ölçümünüz'];
        $data['Şeker Tüketim Skorunuz']                     = $scores['Şeker Tüketim Skorunuz'];
        $data['Yapay Tatlandırıcı Hasarınız']               = $scores['Yapay Tatlandırıcı Hasarınız'];
        $data['Probiyotik Mikroorganizma Ölçümünüz']        = $scores['Probiyotik Mikroorganizma Ölçümünüz'];
        $data['Bağırsak Hareketlilik Ölçümünüz']            = $scores['Bağırsak Hareketlilik Ölçümünüz'];
        $data['Antibiyotik Hasar Ölçümünüz']                = $scores['Antibiyotik Hasar Ölçümünüz'];
        $data['Otoimmünite Endeksiniz']                     = $scores['Otoimmünite Endeksiniz'];
        $data['Uyku Kaliteniz']                             = $scores['Uyku Kaliteniz'];
        return $data;
    }
}
