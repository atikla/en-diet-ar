<?php

if (!function_exists('scoreKeys')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function scoreKeys($title)
    {
        $data = config()->get('limitvalues.' . $title);
        $temp = [];
        foreach ($data as $d => $key)
            array_push($temp, [$key => $d]);
        $data= [
            'title' =>  __('mikrobyom.score_key', [ 'skor' => __('mikrobyom.' . $title . '.0') ]),
            'desc' =>  __('mikrobyom.score_key_decs'),
            'keys' => []

        ];
        if ( isset($temp[0]['iyi']) ) {
            array_push($data['keys'],
                [
                    'title' =>  __('mikrobyom.iyi_anahtar'),
                    'desc' =>  __('mikrobyom.rate_key',
                        [
                            'first' => $temp[0]['iyi'],
                            'second' => $temp[1]['orta_1']
                        ])
                ],
                [
                    'title' => __('mikrobyom.orta_anahtar'),
                    'desc' =>  __('mikrobyom.rate_key',
                        [
                            'first' => $temp[1]['orta_1']+1,
                            'second' => $temp[2]['orta_2']
                        ])
                ],
                [
                    'title' => __('mikrobyom.kotu_anahtar'),
                    'desc' =>  __('mikrobyom.rate_key',
                        [
                            'first' => $temp[2]['orta_2']+1,
                            'second' => $temp[3]['kotu']
                        ])
                ]
            );

        } else {
            array_push($data['keys'],
                [
                    'title' => __('mikrobyom.iyi_anahtar'),
                    'desc' =>  __('mikrobyom.rate_key',
                        [
                            'first' => $temp[2]['orta_1']+1,
                            'second' => $temp[3]['iyi']
                        ])
                ],
                [
                    'title' => __('mikrobyom.orta_anahtar'),
                    'desc' =>  __('mikrobyom.rate_key',
                        [
                            'first' => $temp[1]['orta_2']+1,
                            'second' => $temp[2]['orta_1']
                        ])
                ],
                [
                    'title' =>  __('mikrobyom.kotu_anahtar'),
                    'desc' =>  __('mikrobyom.rate_key',
                        [
                            'first' => $temp[0]['kotu'],
                            'second' => $temp[1]['orta_2']
                        ])
                ]
            );
        }
        return $data;
    }
}
