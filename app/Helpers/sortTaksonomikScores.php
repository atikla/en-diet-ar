<?php

if (!function_exists('sortTaksonomikScores')) {

    /**
     * description
     *
     * @param array $data 
     * @return array
     */
    function sortTaksonomikScores($data)
    {
        foreach ( $data as $lv1key => $taksonomik ) {
            foreach ( $taksonomik['items'] as $lv2key => $item ) {
                array_multisort( array_map( function($i) {
                        return $i['value'];
                }, $data[$lv1key]['items'][$lv2key]['scores'] ), SORT_DESC, $data[$lv1key]['items'][$lv2key]['scores'] );
            }
        }
        return $data;
    }
}
