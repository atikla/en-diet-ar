<?php

if (!function_exists('GetFoodCategoriesName')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function GetFoodCategoriesName($data)
    {
        foreach( $data as $key => $value ) {
            $data[$key]['foodType'] =(__('food.' . $data[$key]['id']));
        }
        return $data;
    }
}
