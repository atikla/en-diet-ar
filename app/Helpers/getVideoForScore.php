<?php

if (!function_exists('getVideoForScore')) {

    /**
     * description
     *
     * @param score string
     * @return array
     */
    function getVideoForScore(string $score)
    {
        $videos = [
            'Vücut Kütle Endeksi-Mikrobiyom İlişkiniz' => [
                'title' => 'Beden Kitle İndeksi Skoru Ne Anlatır?',
                'imageurl' => 'https://img.youtube.com/vi/JpDolJYgKjw/maxresdefault.jpg',
                'videoUrl' => 'https://www.youtube.com/embed/b0QNhwnnc78'
            ],
            'Karbonhidrat Metabolizmanız' => [
                'title' => 'Karbonhidrat Metabolizması Skoru Ne Anlatır?',
                'imageurl' => 'https://img.youtube.com/vi/JpDolJYgKjw/maxresdefault.jpg',
                'videoUrl' => 'https://www.youtube.com/embed/rinpCxXmpDk'
            ],
            'Protein Metabolizmanız' => [
                'title' => 'Protein Metabolizması Skoru Ne Anlatır?',
                'imageurl' => 'https://img.youtube.com/vi/JpDolJYgKjw/maxresdefault.jpg',
                'videoUrl' => 'https://www.youtube.com/embed/GpurwzLax-Q'
            ],
            'Yağ Metabolizmanız' => [
                'title' => 'Yağ Metabolizması Skoru Ne Anlatır?',
                'imageurl' => 'https://img.youtube.com/vi/JpDolJYgKjw/maxresdefault.jpg',
                'videoUrl' => 'https://www.youtube.com/embed/tu3DibKvruE'
            ],
            'Vitamin Senteziniz' => [
                'title' => 'Vitamin Sentezi Skoru Ne Anlatır?',
                'imageurl' => 'https://img.youtube.com/vi/JpDolJYgKjw/maxresdefault.jpg',
                'videoUrl' => 'https://www.youtube.com/embed/3JhET6V79bY'
            ],
            'Laktoz Hassasiyeti Ölçümünüz' => [
                'title' => 'Laktoz Hassasiyeti Skoru Ne Anlatır?',
                'imageurl' => 'https://img.youtube.com/vi/JpDolJYgKjw/maxresdefault.jpg',
                'videoUrl' => 'https://www.youtube.com/embed/r86zOhIliNA'
            ],
            'Gluten Hassasiyeti Ölçümünüz' => [
                'title' => 'Glüten Hassasiyeti Skoru Ne Anlatır?',
                'imageurl' => 'https://img.youtube.com/vi/JpDolJYgKjw/maxresdefault.jpg',
                'videoUrl' => 'https://www.youtube.com/embed/nj2HUNWZVM0'
            ],
            'Şeker Tüketim Skorunuz' => [
                'title' => 'Şeker Tüketimi Skoru Ne Anlatır?',
                'imageurl' => 'https://img.youtube.com/vi/JpDolJYgKjw/maxresdefault.jpg',
                'videoUrl' => 'https://www.youtube.com/embed/BUTm-WsRvFo'
            ],
            'Yapay Tatlandırıcı Hasarınız' => [
                'title' => 'Yapay Tatlandırıcı Hasarı Skoru Ne Anlatır?',
                'imageurl' => 'https://img.youtube.com/vi/JpDolJYgKjw/maxresdefault.jpg',
                'videoUrl' => 'https://www.youtube.com/embed/W6i_1S65ciE'
            ],
            'Probiyotik Mikroorganizma Ölçümünüz' => [
                'title' => 'Probiyotik Mikroorganizmalar Skoru Ne Anlatır?',
                'imageurl' => 'https://img.youtube.com/vi/JpDolJYgKjw/maxresdefault.jpg',
                'videoUrl' => 'https://www.youtube.com/embed/gG8UC7TzBII'
            ],
            'Bağırsak Hareketlilik Ölçümünüz' => [
                'title' => 'Bağırsak Hareketliliği Skoru Ne Anlatır?',
                'imageurl' => 'https://img.youtube.com/vi/JpDolJYgKjw/maxresdefault.jpg',
                'videoUrl' => 'https://www.youtube.com/embed/GKHFdk7zsJg'
            ],
            'Antibiyotik Hasar Ölçümünüz' => [
                'title' => 'Antibiyotik Hasar Skoru Ne Anlatır?',
                'imageurl' => 'https://img.youtube.com/vi/JpDolJYgKjw/maxresdefault.jpg',
                'videoUrl' => 'https://www.youtube.com/embed/ZKFdScJxUSI'
            ],
            'Otoimmünite Endeksiniz' => [
                'title' => 'Otoimmünite Skoru Ne Anlatır?',
                'imageurl' => 'https://img.youtube.com/vi/JpDolJYgKjw/maxresdefault.jpg',
                'videoUrl' => 'https://www.youtube.com/embed/0_Cfflz-8rE'
            ],
            'Uyku Kaliteniz' => [
                'title' => 'Uyku Kalitesi Ne Anlatır?',
                'imageurl' => 'https://img.youtube.com/vi/JpDolJYgKjw/maxresdefault.jpg',
                'videoUrl' => 'https://www.youtube.com/embed/51HlbvLwauI'
            ],
        ];
        return isset($videos[$score]) ? $videos[$score] : null;
    }
}
