<?php

if (!function_exists('getCurrentGuard')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function getCurrentGuard()
    {
        if(\Auth::guard('admin')->check())
        {
            return auth()->guard('admin')->user()->strRole;
        }
        elseif(\Auth::guard('dietitian')->check())
            {return "dietitian";}
         else
            {return "client";}
    }
}
