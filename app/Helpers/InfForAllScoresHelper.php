<?php

if (!function_exists('scores')) {

    /**
     * description
     *
     * @param string $title
     * @param array $scores
     * @return array 
     */
    function scores($title, $scores)
    {
        if ( sizeof($scores) == 4 ) {
            $t = [];
            $t[0]  = $scores[0];
            $t[1]  = $scores[3];
            $t[2]  = $scores[1];
            $t[3]  = $scores[2];
            $scores = $t;
        }
        $temp = config()->get('scores.' . $title);
        if ( !$temp )
            return null;
        $data = [];
        $isMy = true;
        foreach ( $temp as $key => $value ) {

            array_push($data,[
                "title" =>  __('mikrobyom.' . $value),
                "value" =>  $scores[$key],
                "isMy"  =>  $isMy
            ]);
            $isMy = false;
        }
        return $data;
    }
}
