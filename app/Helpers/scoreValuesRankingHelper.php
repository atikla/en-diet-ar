
<?php

if (!function_exists('scoreValuesRanking')) {

    /**
     * description
     *
     * @param string $title
     * @param int $score
     * @return  int
     */
    function scoreValuesRanking($title, $score)
    {
        $limits = config()->get('limitvalues.' . $title );
        
        if ( $limits[0] == 'iyi') {

            $orta_1 = array_search('orta_1', $limits);
            $orta_2 = array_search('orta_2', $limits);
            if ( $score <  $orta_1 ) {

                return 0;

            } else if ( $score >= $orta_1 && $score <= $orta_2 ) {

                return 1;

            } else if ( $score > $orta_2 ) {

                return 2;

            }
            return 0;

        } else if ( $limits[0] == 'kotu') {

            $orta_1 = array_search('orta_2', $limits);
            $orta_2 = array_search('orta_1', $limits);
            if ( $score <  $orta_1 ) {

                return 2;

            } else if ( $score >= $orta_1 && $score <= $orta_2 ) {

                return 1;

            } else if ( $score > $orta_2 ) {

                return 0;

            }
            return 0;
            
        }
        return 0;
    }
}
