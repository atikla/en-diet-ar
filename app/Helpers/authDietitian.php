<?php
use Illuminate\Http\Request;
use App\Dietitian;

if (!function_exists('authD')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function authD()
    {
        $token = str_replace('Bearer ', '', request()->header('Authorization') );
        $oauth = DB::table('oauth_access_tokens')->whereId($token)->whereName('dietitian')->first();
        if ( $oauth )
            return $oauth->user_id;
        else 
            return null;
    }
}
