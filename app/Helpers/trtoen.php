<?php

if (!function_exists('trtoen')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function trtoen($text)
    {
        $search = array('Ç','ç','Ğ','ğ','ı','İ','Ö','ö','Ş','ş','Ü','ü',' ');
        $replace = array('c','c','g','g','i','i','o','o','s','s','u','u','-');
        return str_replace($search,$replace,$text);
        
    }
}
