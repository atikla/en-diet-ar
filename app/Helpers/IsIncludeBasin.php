<?php

if (!function_exists('IsIncludeBasin')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function IsIncludeBasin()
    {
        if
        (     \Route::current()->getName() == '/'
            ||  \Route::current()->getName() == 'bagirsak-hastaliklari'
            ||  \Route::current()->getName() == 'ibs'
            ||  \Route::current()->getName() == 'kisisellestirilmis-beslenme'
            ||  \Route::current()->getName() == 'mikrobiyom_testleri'
            ||  \Route::current()->getName() == 'hakkimizda'
        )
            return true;
        return false;
    }
}
