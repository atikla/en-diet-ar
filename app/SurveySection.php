<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SurveySection extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'survey_id', 'section_order', 'duration'
    ];

    public function survey()
    {
        return $this->belongsTo('App\Survey');
    }

    public function surveySectionLang()
    {
        return $this->hasMany('App\SurveySectionLang');
    }

    public function question()
    {
        return $this->hasMany('App\Question')->orderBy('question_order');
    }

    public function getDefaultLanguageAttribute()
    {
        $default = SurveySectionLang::where('survey_section_id', '=', $this->id)
            ->where('lang', '=', 'tr')
            ->first();
        return $default;
    }
}
