<?php

namespace App\Exports;

use App\Kit;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithDrawings;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class KitsExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize, WithDrawings
{
    protected $index = 0 ;
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {   
        $kits = Kit::orderBy('id', 'DESC')->get();
        return $kits;
    }

    /**
    * @var Kits $kits
    */
    public function map($kits): array
    {   
        $orderName = '*****';
        $orderPhone = '*****';
        $orderAddress ='*****';
        $orderEmail = '*****';
        // ++$this->index;
        if ( $kits->user ) {
            $orderName = $kits->user->name;
            $orderAddress = $kits->user->address;
            $orderPhone = $kits->user->phone;
            $orderEmail = $kits->user->email;

        } else if ($kits->orderDetail) {
            $orderName = $kits->orderDetail->order->name;
            $orderAddress = $kits->orderDetail->order->address;
            $orderPhone = $kits->orderDetail->order->phone;
            $orderEmail = $kits->orderDetail->order->email;
        }
            
        return [
            ++$this->index,
            $kits->kit_code,
            $kits->status,
            $kits->kitStatuse,
            $orderName,
            $orderPhone,
            $orderEmail,
            $orderAddress,
            $kits->created_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss'),
        ];
    }
    public function headings(): array
    {
        return [
            '#',
            'Kit Code',
            'Kit Satış Durumu',
            'Kit Durumu',
            'Müşteri Adı',
            'Müşteri Tel',
            'Müşteri Eposta',
            'Müşteri Adres',
            'Kit code oluşturma zamanı',
        ];
    }
    public function drawings()
    {
        $drawing = new Drawing();
        $drawing->setName('Logo');
        $drawing->setDescription('This is my logo');
        $drawing->setPath(public_path('/img/header-logo.png'));
        $drawing->setHeight(50);
        $drawing->setCoordinates('J1');

        return $drawing;
    }
}