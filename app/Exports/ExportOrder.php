<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithDrawings;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use App\Order;
class ExportOrder implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize
{
    protected $index = 0 ;
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $orders = Order::orderBy('id', 'DESC')->get();
        return $orders;
    }
    /**
    * @var Kits $kits
    */
    public function map($orders): array
    {   
        $orderQuantity = 0;
        $fullname = explode(' ', $orders->name);
        $name = $fullname[0];
        unset($fullname[0]);
        $surname = implode(' ', $fullname);
        foreach ($orders->orderDetail as $detail)
            $orderQuantity += $detail->quantity;
        $address = '';
        if ($orders->city)
        $address .= trtoen($orders->city->city) . ' - ';
        if ($orders->county)
        $address .= trtoen($orders->county->county) . ' - ';
        $address .= trtoen($orders->address);
        return [
            ++$this->index,
            $orders->created_at->isoFormat(' DD MMM ,ddd Y - HH:mm:ss'),
            $orderQuantity,
            $orders->price . ' TL',
            trtoen($name),
            trtoen($surname),
            $orders->phone,
            $orders->email,
            $address,
        ];
    }
    public function headings(): array
    {
        return [
            '#',
            trtoen('Siparis Tarihi'),
            trtoen('Siparis Adedi'),
            trtoen('Siparis Tutari'),
            trtoen('isim'),
            trtoen('Soyisim'),
            trtoen('Telefon Numarasi'),
            trtoen('Email Adresi'),
            trtoen('Adres'),
        ];
    }
}
