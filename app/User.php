<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable, SoftDeletes, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'address', 'phone', 'gender', 'vegetarian', 'verify_code', 'expires_at', 'date_of_birth', 'email_verified_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'verify_code', 'expires_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'expires_at' => 'datetime',
        'date_of_birth' => 'date:Y-m-d',
        'vegetarian' => 'boolean',

    ];
    
    public function photo()
    {
        return $this->morphOne('App\Photo', 'imageable');
    }

    // User Can HAve A mmulti Kit 
    public function Kits()
    {
        return $this->hasMany('App\Kit')->orderBy('registered_at', 'DESC');   
    }

    //Get Gender
    public function getUserGenderAttribute()
    {
        if($this->gender == 0)
            return __('app.belirtilmemiş');
        elseif($this->gender == 1)
            return __('app.erkek');
        elseif($this->gender == 2)
            return __('app.kadin');
        else
            return __('app.belirtilmemiş');
    }

    // // Define an Accessor
    // public function getVegetarianAttribute($value)
    // {
    //     if ( $value ) {
    //         return 'yes';
    //     } else {
    //         return 'no';
    //     }
    // }
    public function getAgeAttribute()
    {
        if ( $this->date_of_birth ){
            $years = \Carbon\Carbon::parse($this->date_of_birth)->age;
            return $years;
        } else {
            return null;
        }
    }
    public function getStrRoleAttribute()
    {
        return 'Normal User';
    }
}
