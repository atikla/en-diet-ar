<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Product extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'price', 'discount_price', 'slug', 'description', 'stok', 'number_of_kit'
    ];

    public function photo()
    {
        return $this->morphOne('App\Photo', 'imageable');
    }

    public function admin()
    {
        return $this->belongsTo('App\Admin');
    }

    public function OrderDetail()
    {
        return $this->hasMany('App\OrderDetail');
    }

    public function getDiscountAttribute()
    {
        if($this->discount_price == NULL)
            return '<td class="text-danger"> Do Not Have A Discount </td>';
        else
            return '<td class="text-success">' . (double) ( ( ( 100 - $this->discount_price ) * $this->price ) / 100 ) . ' / ' . $this->discount_price .  ' % </td>'; 

    }
}
