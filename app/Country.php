<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $fillable = [
        'name', 'code'
    ];

    public function setCodeAttribute($value)
    {
        $this->attributes['code'] = strtolower($value);
    }
}
