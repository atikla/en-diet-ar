<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Order;
use Product;

class OrderDetail extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'price', 'quantity', 'product_id', 'order_id', 'name'
     ];



    public function order()
    {
        // return Order::findOrfail($this->order_id);
        return $this->belongsTo('App\Order', 'order_id', 'id');
    }

    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id', 'id');
    }
    
    public function kit()
    {
        return $this->hasMany('App\Kit');
    }
    
}
