<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\City;
use App\County;

class Order extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tracking', 'payment_id', 'coupon_id', 'status', 'address',
        'name', 'email', 'dietitian_code', 'phone', 'price', 'checkbox',
        'cc_name', 'card_number', 'expiration_month', 'expiration_year', 'cvc',
        'city', 'county', 'fcity_id', 'fcounty_id', 'faddress'
    ];

     /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'cc_name', 'card_number', 'expiration_month', 'expiration_year', 'cvc'
    ];

    /**
     * Get the payment record associated with the Order.
     */
    public function payment()
    {
        return $this->belongsTo('App\Payment');
    }

    public function coupon()
    {
        return $this->belongsTo('App\Coupon');
    }

    public function orderDetail()
    {
        return $this->hasMany('App\OrderDetail', 'order_id');
    }

    public function city(){
        return $this->belongsTo('App\City');
    }

    public function county(){
        return $this->belongsTo('App\County');
    }

    public function dietitian()
    {
        return $this->belongsTo('App\Dietitian', 'dietitian_code', 'reference_code');
    }

    public function getBiCityAttribute()
    {
        if ( $this->fcity_id ) {
            return City::find($this->fcity_id);
        }
    }
    public function getBiCountyAttribute()
    {
        if ( $this->fcounty_id ) {
            return County::find($this->fcounty_id);
        }
    }
    public function getOrderStatusAttribute()
    {
        if($this->status == 0)
            return '<span class="text-dark"> order reserved </span>';
    }

    public function setDietitianCodeAttribute($value)
    {
        $this->attributes['dietitian_code'] = strtoupper($value);
    }
}
