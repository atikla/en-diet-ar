<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Country;

class SurveyLang extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'survey_id', 'lang', 'title', 'description'
    ];

    public function survey()
    {
        return $this->belongsTo('App\Survey');
    }

    public function getLanguageAttribute()
    {   $country = Country::where('code', '=', $this->lang)
            ->first();
        return $country->name;
    }
}
