<?php

namespace App\Http\Requests;

use LVR\CreditCard\CardCvc;
use LVR\CreditCard\CardNumber;
use LVR\CreditCard\CardExpirationYear;
use LVR\CreditCard\CardExpirationMonth;
use Illuminate\Foundation\Http\FormRequest;

class BankCardRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'card_number' => ['required', new CardNumber],
            'expiration_year' => ['required', new CardExpirationYear($this->get('expiration_month'))],
            'expiration_month' => ['required', new CardExpirationMonth($this->get('expiration_year'))],
            'cvc' => ['required', new CardCvc($this->get('card_number'))],
            'cc_name' => 'required',
            'name' => 'required|max:255',
            'email' => 'required|max:255',
            'phone' => 'required|max:15|min:1',
            'address' => 'required',
            'country' => 'required',
            'city' => 'required|numeric|min:1|',
            'county' => 'required|numeric|min:1|',
            // 'fcity' => 'numeric|min:1|nullable',
            // 'fcounty' => 'required_with:fcity|numeric|min:1|nullable',
            // 'faddress' => 'required_with:fcity|min:1|nullable'
            // 'Mesafeli_Satış_Sözleşmesi' => 'required',
            // 'Ön_Bilgilendirme_Formu' => 'required',
            // 'Aydınlatma_Formu' => 'required',
        ];
    }

	// public function messages()
    // {
    //     return [
	// 		'address.required' => 'العنوان مطلوب',
	// 		'card_number.required' =>'Lütfen kredi kartı alanını doldurunuz.',
	// 		'expiration_year.required' =>'Lütfen kredi kartı yılını doldurunuz.',
	// 		'expiration_month.required' =>'Lütfen kredi kartı ayını doldurunuz.',
	// 		'cvc.required' =>'Lütfen cvc alanını doldurunuz.',
	// 		'cc-name.required' =>'Lütfen kredi kartı sahibi alanını doldurunuz.',
	// 		'name.required' => 'Lütfen isim alanını doldurunuz.',
	// 		'email.required' => 'Lütfen e-posta alanını doldurunuz.',
	// 		'phone.required' => 'Lütfen telefon alanını doldurunuz.',
	// 		'city.required' => 'Lütfen şehir alanını doldurunuz.',
	// 		'county.required' => 'Lütfen ilçe alanını doldurunuz.',
	// 		'Mesafeli_Satış_Sözleşmesi.required' => 'Lütfen mesafeli satış sözleşmesi alanını seçiniz.',
	// 		'Ön_Bilgilendirme_Formu.required' => 'Lütfen ön bilgilendirme formu alanını seçiniz.',
	// 		'Aydınlatma_Formu.required' => 'Lütfen aydınlatma formu alanını seçiniz.',
	// 	];
    // }
}
