<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;
use Alert;
use App\Kit;
use App\Answer;

class AdminSurveyKitAnswerController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
     public function __construct()
    {
        $this->middleware(['auth:admin', 'admin', 'superAdmin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kits = Kit::where('survey_id', '!=', NULL)
            ->orderBy('id', 'DESC')
            ->paginate(5);
        $this->travelLogging('go to answers index', auth()->user(), 'superAdmin');
        return \view('admin.answer.index', \compact('kits'));
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kit = Kit::findOrFail($id);
        $survey = $kit->survey;
        $this->travelLogging('go to answers show', auth()->user(), 'superAdmin');
        return \view('admin.answer.show', \compact('kit', 'survey'));
    }

    /**
     * Export Answers for  Kit  As json
     * @return json file
     */
    public function export(Request $request) 
    {
        $data = array();
        if ( $request->all ) {
            $kits = Kit::where('survey_id', '!=', NULL) ->orderBy('id', 'DESC')->get();
            foreach ( $kits as $kit ) {
                $tempanswer = [];
                $tempanswer['previous_kit'] = $kit->oldOne;
                foreach ( $kit->answer as $answer) {

                    $question = $answer->question()->withTrashed()->first();

                    if ( $question->defaultLanguage->option_name ) {
                        $tempdata = [] ;
                        $options = $question->defaultLanguage->option_name;
                        foreach ($options as $key => $value) {
                            foreach ($answer->answer as  $nestedValue) {
                                if ( array_key_exists( '"'. $key . '"', ( array ) $nestedValue) ) {
                                    $tempdata[ $value['title'] ] = null;
                                    if ( $nestedValue['"'. $key . '"'] ) {
                                        $tempdata[ $value['title'] ] = $nestedValue[ '"'. $key . '"' ];
                                    }
                                }
                            }
                        }
                        $tempanswer[$question->DefaultLanguage->title] = ( array ) $tempdata;
                    } else {
                        $tempanswer[$question->DefaultLanguage->title] = ( array ) $answer->answer;
                    }
                }
                $data[$kit->kit_code] = $tempanswer;
            }
            $file = time() . '_' . rand() . '_answers.json';

            $path = storage_path() ."/jsonFiles/answerFiles/";

            if ( !is_dir( $path ) ) mkdir($path,0777,true);

            File::put($path . $file,json_encode($data));

            $headers = array('Content-Type: application/json',);
            $this->logging('All answers exported', null, array_keys($data), auth()->user(), new Answer, 'superAdmin');

            return response()->download($path . $file, 'answers.json', $headers);
        } else {
            $kits = array_slice( explode('-', $request->kits), 0, -1 );
            foreach ( $kits as $kitcode ) {
                $kit = Kit::where('kit_code', '=', $kitcode) ->orderBy('id', 'DESC')->first();
                $tempanswer = [];
                $tempanswer['previous_kit'] = $kit->oldOne;
                foreach ( $kit->answer as $answer) {

                    $question = $answer->question()->withTrashed()->first();

                    if ( $question->defaultLanguage->option_name ) {
                        $tempdata = [] ;
                        $options = $question->defaultLanguage->option_name;
                        foreach ($options as $key => $value) {
                            foreach ($answer->answer as  $nestedValue) {
                                if ( array_key_exists( '"'. $key . '"', ( array ) $nestedValue) ) {
                                    $tempdata[ $value['title'] ] = null;
                                    if ( $nestedValue['"'. $key . '"'] ) {
                                        $tempdata[ $value['title'] ] = $nestedValue[ '"'. $key . '"' ];
                                    }
                                }
                            }
                        }
                        $tempanswer[$question->DefaultLanguage->title] = ( array ) $tempdata;
                    } else {
                        $tempanswer[$question->DefaultLanguage->title] = ( array ) $answer->answer;
                    }
                }
                $data[$kit->kit_code] = $tempanswer;
            }
            $file = time() . '_' . rand() . '_answers.json';

            $path = storage_path() ."/jsonFiles/answerFiles/";

            if ( !is_dir( $path ) ) mkdir($path,0777,true);

            File::put($path . $file,json_encode($data));

            $headers = array('Content-Type: application/json',);
            $this->logging('some kit answers exported', null, array_keys($data), auth()->user(), new Answer, 'superAdmin');

            return response()->download($path . $file, 'answers.json', $headers);
        }
    }

    public function getSurveyTemplate($id)
    {
        $surveys = \App\Survey::findOrFail($id);
        $survey = $surveys->Survey;
        $file = time() . '_' . rand() . '_anket.json';
        $path = storage_path() ."/jsonFiles/anket/";
        if ( !is_dir( $path ) ) mkdir($path,0777,true);
        $data = json_encode($survey);
        File::put($path . $file, $data);
        $headers = array('Content-Type: application/json',);
        $this->logging('Survey Template Exported', null, null, auth()->user(), new Answer, 'superAdmin');
        return response()->download($path . $file, $file, $headers);
    }
    /**
     * search in resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $this->validate($request, [
            'search' => 'required|max:50|min:1',
        ]);
        $search = $request->search;
        $kits = Kit::where('survey_id', '!=', NULL)
            ->orderBy('id', 'DESC')
            ->where('kit_code', 'LIKE', '%' . $search . '%')
            ->get();
        Alert::alert('Search Result', $kits->count() . ' items Found', 'info');
        $this->logging('Answer searched for ' . $request->search, null, $request->search, auth()->user(), new Answer, 'superAdmin');
        return \view('admin.answer.search', \compact('kits', 'search'));
    }

}
