<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Spatie\Activitylog\Models\Activity;
use App\User;
use App\Kit;
use App\Order;
use App\Coupon;

class AdminDashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth:admin', 'admin', 'superAdmin']);
    }


    /**
     * Display a listing of the availableCoupons.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        $kits = Kit::all();
        $orders = Order::all();
        $coupons = Coupon::all();
        $logs = $this->getLogs();
        $this->travelLogging('go to admin Dashboard page', auth()->user(), 'superAdmin');
        return \view('admin.index', compact('users', 'kits', 'orders', 'coupons', 'logs') );
    }


    public function getLogs()
    {
        $templogs = Activity::orderBy('created_at', 'DESC')
            ->get()
            ->groupBy( function ( $item ) {
                return $item->created_at->format('Y-m-d');
            })->take(7)->reverse();
        $logs = [];
        foreach ( $templogs as $date => $day) {
            $logs[$date] = [
                'apiUser' => 0,
                'opAdmin' => 0,
                'labAdmin' => 0,
                'superAdmin' => 0,
                'apiDietitian' => 0
            ];
            foreach ( $day as $key => $value  ) {
                if ( $value->log_name != 'adminlogin' && $value->log_name != 'adminlogout') {
                    if (!isset($logs[$date][$value->log_name])) {
                        $logs[$date][$value->log_name] = 0;
                    }
                    $logs[$date][$value->log_name]++;
                }
            }
        }
        $data = ['log' => [], 'line1' => [], 'line2' => [] ];
        $i = 1;
        foreach ($logs as $day => $value) {
            if ( $i == 1 )
                array_push($data['log'], $day);
            if ( $i == sizeof($logs) )
                array_push($data['log'], $day);
            array_push($data['line1'], [$day, $value['superAdmin'], $value['opAdmin'], $value['labAdmin']]);
            array_push($data['line2'], [$day, $value['apiUser'], $value['apiDietitian']]);
            ++$i;
        }
        return $data;
    }
}
