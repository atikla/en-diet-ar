<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Coupon;
use App\Dietitian;
use Alert;

class AdminCampaignController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth:admin', 'admin', 'superAdmin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $campaigns = Coupon::where('expires_at', '>=', date('Y-m-d'))
            ->whereCampaign(true)
            ->orderBy('id', 'DESC')
            ->paginate(5);
        $dietitians = Dietitian::whereNotNull('reference_code')->orderBy('id', 'DESC')->get();
        $dietitians->filter( function ($dietitian) {
            $dietitian->name = $dietitian->kind . '. ' . $dietitian->name . ' - ' . $dietitian->reference_code;
        });
        $dietitians = $dietitians->pluck('name', 'reference_code');
        $this->travelLogging('go to available coupon page', auth()->user(), 'superAdmin');
        return view('admin.campaign.index', compact('campaigns', 'dietitians'))->with(['title' => 'Available campaigns']);
    }

    /**
     * Display a listing of the NotavailableCoupons.
     *
     * @return \Illuminate\Http\Response
     */
    public function notAvailableCampaigns()
    {
        $campaigns = Coupon::where('expires_at', '<', date('Y-m-d'))
            ->whereCampaign(true)
            ->orderBy('id', 'DESC')
            ->paginate(5);
        $dietitians = Dietitian::whereNotNull('reference_code')->orderBy('id', 'DESC')->get();
        $dietitians->filter( function ($dietitian) {
            $dietitian->name = $dietitian->kind . '. ' . $dietitian->name . ' - ' . $dietitian->reference_code;
        });
        $dietitians = $dietitians->pluck('name', 'reference_code');
        $this->travelLogging('go to not available coupon page', auth()->user(), 'superAdmin');
        return view('admin.campaign.index', compact('campaigns', 'dietitians'))->with(['title' => 'Not Available campaigns']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'code' => 'required|max:10|unique:coupons',
            'discount' => 'required|numeric|min:1|max:100',
            'expires_at' => 'required|date|after:'. now(),
        ]);
        $input = $request->all();
        $input['campaign'] = true;
        $input['code'] = strtoupper(trtoen($input['code']));
        $campaign = Coupon::create($input);
        Alert::success('Done :)', ' Campaign Has Been Created');
        $this->logging('New Campaign Created - ', null, $input, auth()->user(), $campaign, 'superAdmin');
        return \redirect()->route('admin.campaigns.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($code)
    {
        $campaign = Coupon::whereCampaign(true)
            ->whereCode($code)
            ->firstOrFail();
        $dietitians = Dietitian::whereNotNull('reference_code')->orderBy('id', 'DESC')->get();
        $dietitians->filter( function ($dietitian) {
            $dietitian->name = $dietitian->kind . '. ' . $dietitian->name . ' - ' . $dietitian->reference_code;
        });
        $dietitians = $dietitians->pluck('name', 'reference_code');
        return view('admin.campaign.show', compact('campaign', 'dietitians'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $code)
    {
        $this->validate($request, [
            'expires_at' => 'required|date|after:'. now(),
            'note' => 'required'
        ]);
        $campaign = Coupon::whereCampaign(true)->whereCode($code)
            ->firstOrFail();
        $temp = $campaign->toArray();
        $campaign->update([
            'expires_at' => $request->expires_at,
            'note' => $request->note,
        ]);
        $this->logging('campaign updated', $temp, $request->all(), auth()->user(), $campaign, 'superAdmin');
        return \redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $campaign = Coupon::findOrfail($id); 
        $campaign->delete();
        $this->logging('campaign deleted', null, $campaign->toArray(), auth()->user(), $campaign, 'superAdmin');
        Alert::info('deleted', 'The campaign with code ( ' . $campaign->code . ' ) Has Been deleted');
        return \redirect()->back();
    }
}
