<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Survey;
use App\SurveySection;
use App\SurveySectionLang;
use App\Country;
use Alert;

class AdminSurveySectionsController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
     public function __construct()
    {
        $this->middleware(['auth:admin', 'admin', 'superAdmin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($surveyId)
    {
        $survey = Survey::findOrFail($surveyId);
        $sections = $survey->surveySection;
        if(sizeof($sections) != 0){
            $sections->filter( function ($section) {
                $section->surveySectionLang;
            });
            $countres = Country::pluck('name','code');
            return view('admin.survey.section.index', compact('sections', 'countres'));
        }else {
            $section = $survey->surveySection()
                ->create([
                    'section_order' => 1
                ]);
            $section->surveySectionLang()->create(
                [
                    'title' => 'baslik',
                    'description' =>  'aciklama',
                ]
            );
            Alert::alert('info', 'this survey Dont Have any section We Create A one For You', 'info')
                ->autoclose(10000);
            return redirect()->route('admin.sections.index', $surveyId);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create($surveyId)
    // {
        //
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $surveyId)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'description' => 'required|max:1000',
            'section_order' => 'required|numeric|max:100',
        ]);
        $survey = Survey::findOrFail($surveyId);
        $surveySection = $survey->surveySection()->create([
            'section_order' => $request->section_order
        ]);
        $surveySection->surveySectionLang()->create([
            'title' => $request->title,
            'description' => $request->description,
            'lang' => 'tr',
        ]);
        Alert::alert('Section Created', 'The Section Has Been Created and add title, description for default lang (tr)', 'success');
        return redirect()->route('admin.sections.index', $surveyId);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function storeLang(Request $request, $surveyId, $sectionId)
    {
        $this->validate($request, [
            '*title' => 'required|max:255',
            '*description' => 'required|max:1000',
            '*lang' => 'required',
        ]);
        $section = SurveySection::findOrFail($sectionId);
        if( $section->surveySectionLang->contains('lang', $request['section_id_' . $sectionId . '_lang'] ) ) {
            $lang = $section->surveySectionLang->where('lang', '=', $request['section_id_' . $sectionId . '_lang'])->first();
            $lang->title = $request['section_id_' . $sectionId . '_title'];
            $lang->description = $request['section_id_' . $sectionId . '_description'];
            $lang->save();
            Alert::alert('section updated', 'The section Has Been Updated for  lang ( ' . $lang->language .' )', 'success')
                ->autoclose(10000);
            return redirect()->route('admin.sections.index', $surveyId);

        }else{
            $lang = $section->surveySectionLang()->create(
                [
                    'title' => $request['section_id_' . $sectionId . '_title'],
                    'description' =>  $request['section_id_' . $sectionId . '_description'],
                    'lang' => $request['section_id_' . $sectionId . '_lang']
                ]
            );
            Alert::alert('section Lang Created', 'The section Lang Has Been Created for  lang ( ' . $lang->language .' )', 'success')
                ->autoclose(10000);
            return redirect()->route('admin.sections.index', $surveyId);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function show($surveyId, $id)
    // {
        //
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function edit($surveyId, $id)
    // {
         //
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $surveyId,  $sectionId)
    {
        $this->validate($request, [
            '*section_order' => 'required|numeric|max:100|min:1',
            '*section_duration' => 'required|numeric|max:10|min:1',
        ]);
        $section = SurveySection::findOrFail($sectionId);
        $section->section_order = $request['update_' . $section->id . 'section_order'];
        $section->duration = $request['update_' . $section->id . 'duration'];
        $section->save();
        Alert::alert('section Order Updated', 'The section Order Has Been Updated', 'success')
                ->autoclose(10000);
        return redirect()->route('admin.sections.index', $surveyId);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function updateLang(Request $request, $surveyId,  $sectionId, $SectionLangId)
    {
        $this->validate($request, [
            '*title' => 'required|max:255',
            '*description' => 'required|max:1000',
        ]);
        $section = SurveySection::findOrFail($sectionId);
        $sectionLang = $section->surveySectionLang()
            ->where('id', '=', $SectionLangId)
            ->first();
        if ( $sectionLang ) {

            $sectionLang->title = $request['update_lang_' . $SectionLangId . '_title'];
            $sectionLang->description = $request['update_lang_' . $SectionLangId . '_description'];
            $sectionLang->save();
            Alert::alert('section updated', 'The section Has Been Updated for  lang ( ' . $sectionLang->language .' )', 'success')
                ->autoclose(10000);
            return redirect()->route('admin.sections.index', $surveyId); 
        } else {
            Alert::alert('section Not updated', 'Error Occurred', 'error')
                ->autoclose(10000);
            return redirect()->route('admin.sections.index', $surveyId);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($surveyId, $sectionId)
    {
        $section = SurveySection::findOrFail($sectionId);
        $section->delete();
        Alert::alert('Section Deleted', 'The Section Has Been Deleted', 'error');
        return redirect()->route('admin.sections.index', $surveyId);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $surveyId, $sectionId, $sectionLangId
     * @return \Illuminate\Http\Response
     */
     public function destroyLang($surveyId, $sectionId, $sectionLangId)
    {
        $section = SurveySection::findOrFail($sectionId);
        $sectionLang = $section->surveySectionLang()
            ->where('id', '=', $sectionLangId)
            ->first();
        if ( $sectionLang ) {
            if($sectionLang->lang == 'tr'){
                Alert::alert('Error', 'You Can Not Delete The default Lang', 'warning');
                return redirect()->back();
            }
            $sectionLang->delete();
            Alert::alert('Section Lang Deleted', 'The Section Lang Has Been Deleted ', 'success');
            return redirect()->back();
        } else {
            Alert::alert('Error', 'Error Occurred', 'error');
            return redirect()->back();
        }
    }
}
