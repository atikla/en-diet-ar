<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Order;
use Alert;
use App\Kit;
use Mail;
use App\Mail\KitDeleverToUser;
use App\Mail\KitReceivedFromUser;
use App\Mail\kitSentToLab;
use App\Exports\ExportOrder;
use Maatwebsite\Excel\Facades\Excel;

class OperationAdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth:admin', 'admin', 'op-admin']);
    }


    public function indexRoute()
    {
        return view('admin-operation.index');
    }

    public function export()
    {
        app()->setlocale('en');
        return Excel::download(new ExportOrder, 'orders.csv');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::orderBy('id', 'DESC')
            ->paginate(10);
        $this->travelLogging('go to order index', auth()->user(), 'opAdmin');
        return \view('admin-operation.order.index', \compact('orders'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::findOrFail($id);
        $this->logging('go to orders show', null, $order->toArray(), auth()->user(),  $order, 'opAdmin');
        return \view('admin-operation.order.show', \compact('order'));
    }

    /**
     * search in resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $this->validate($request, [
            'search' => 'required|max:50|min:3',
        ]);
        $search = $request->search;
        $orders = Order::where('tracking', 'LIKE', '%' . $search . '%')
            ->orWhere('address', 'LIKE', '%' . $search . '%')
            ->orWhere('name', 'LIKE', '%' . $search . '%')
            ->orWhere('email', 'LIKE', '%' . $search . '%')
            ->orWhere('dietitian_code', 'LIKE', '%' . $search . '%')
            ->orWhere('phone', 'LIKE', '%' . $search . '%')
            ->orWhere('cc_name', 'LIKE', '%' . $search . '%')
            ->orWhere('card_number', 'LIKE', '%' . $search . '%')
            ->get();
        Alert::alert('Search Result', $orders->count() . ' items Found', 'info');
        $this->logging('order searched for ' . $request->search , null, $request->search, auth()->user(), new Order, 'opAdmin');
        return view('admin-operation.order.search', compact('orders'));
    }
    
    public function kit()
    {
        $kits = Kit::whereNotNull('kit_code')->orderBy('id', 'DESC')->paginate(10);
        return \view('admin-operation.kit.index', \compact('kits'));
    }

    public function Kitsearch(Request $request)
    {
        $this->validate($request, [
            'search' => 'required|max:50|min:3',
        ]);
        $search = $request->search;
        $kits = Kit::where('kit_code', 'LIKE', '%' . $search . '%')
            ->get();
        Alert::alert('Search Result', $kits->count() . ' items Found', 'info');
        $this->logging('kit searched for ' . $request->search , null, $request->search, auth()->user(), new kit, 'opAdmin');
        return \view('admin-operation.kit.serach', \compact('kits', 'search'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateKit(Request $request, $id)
    {
        //
        $kit = Kit::findOrFail($id);
        $request['id_' . $kit->id . 'kit_code'] = str_replace(' ', '', $request['id_' . $kit->id . 'kit_code']);
        $this->validate($request, [
            "*kit_code"    => "required|unique:kits,kit_code|max:10|min:1",
            "*kit_id"  => "required",
        ]);

        $kit->kit_code = $request['id_' . $kit->id . 'kit_code'];
        $kit->save();
        Alert::alert('Done :)', 'Kit Code Has Been Created', 'success');
        $this->logging('kit updated', $request->except('_method', '_token'), $kit->toArray(), auth()->user(), $kit, 'opAdmin');
        return \redirect()->back();
    }

    /**
     * Update Kit Status  
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function kitDeleverToUser(Request $request) 
    {
        $this->validate($request, [
            "kit_code"    => "required",
            "kit_id"  => "required",
        ]);
        $kit = Kit::find($request->kit_id);
        if ( $kit && $kit->kit_code == $request->kit_code){
            $kit->delevered_to_user_at = now();
            $kit->kit_status = 1;
            $kit->save();
            $this->logging('kit delever to user', null, $kit->toArray(), auth()->user(), $kit, 'opAdmin');
            if( $kit->user ){
                Mail::to($kit->user->email)->locale(app()->getLocale())->send(new KitDeleverToUser($kit));
                Alert::alert('Done :)', ' Mail Send to user ', 'success');
                return response()->json([
                    'mes' => 'ok from user',
                    'code' => '200'
                    ]
                );
            }elseif( $kit->orderDetail ){
                Mail::to($kit->orderDetail->order->email)->locale(app()->getLocale())->send(new KitDeleverToUser($kit));
                Alert::alert('Done :)', ' Mail Send to order mail ', 'success');
                return response()->json([
                    'mes' => 'ok from order',
                    'code' => '200'
                    ]
                );
            }else {

                Alert::alert('Done :)', ' Kit Status Updated But No Mail Sending ', 'success');
                return response()->json([
                    'mes' => 'ok',
                    'code' => '200'
                    ]
                );
            }
        }else {
            Alert::alert('Error', ' error occurred ', 'error');

            return response()->json([
                    'mes' => 'error',
                    'code' => '400'
                ]
            );
        }
    }

    /**
     * Export Kit Code list As xlsx file 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function kitReceived(Request $request) 
    {
        $this->validate($request, [
            "kit_code"    => "required",
            "kit_id"  => "required",
        ]);
        $kit = Kit::find($request->kit_id);
        if ( $kit && $kit->kit_code == $request->kit_code){
            $kit->received_from_user_at = now();
            $kit->kit_status = 2;
            $kit->save();
            $this->logging('kit received from user', null, $kit->toArray(), auth()->user(), $kit, 'opAdmin');
            if( $kit->user ){
                Mail::to($kit->user->email)->locale(app()->getLocale())->send(new KitReceivedFromUser($kit));
                Alert::alert('Done :)', ' Mail Send to user ', 'success');
                return response()->json([
                    'mes' => 'ok from user',
                    'code' => '200'
                    ]
                );
            }elseif( $kit->orderDetail ){
                Mail::to($kit->orderDetail->order->email)->locale(app()->getLocale())->send(new KitReceivedFromUser($kit));
                Alert::alert('Done :)', ' Mail Send to order mail', 'success');
                return response()->json([
                    'mes' => 'ok from order',
                    'code' => '200'
                    ]
                );
            }else {

                Alert::alert('Done :)', ' Kit Status Updated But No Mail Sending ', 'success');
                return response()->json([
                    'mes' => 'ok',
                    'code' => '200'
                    ]
                );
            }
        }else {
            Alert::alert('Error', ' error occurred ', 'error');

            return response()->json([
                    'mes' => 'error',
                    'code' => '400'
                ]
            );
        }
    }

    public function kitSentToLab(Request $request) 
    {
        $this->validate($request, [
            "kit_code"    => "required",
            "kit_id"  => "required",
        ]);
        $kit = Kit::find($request->kit_id);
        if ( $kit && $kit->kit_code == $request->kit_code){
            $kit->send_to_lab_at = now();
            $kit->kit_status = 3;
            $kit->save();
            $this->logging('kit sent to lab', null, $kit->toArray(), auth()->user(), $kit, 'opAdmin');
            if( $kit->user ){
                Mail::to($kit->user->email)->locale(app()->getLocale())->send(new kitSentToLab($kit));
                Alert::alert('Done :)', ' Mail Send to user ', 'success');
                return response()->json([
                    'mes' => 'ok from user',
                    'code' => '200'
                    ]
                );
            }elseif( $kit->orderDetail ){
                Mail::to($kit->orderDetail->order->email)->locale(app()->getLocale())->send(new kitSentToLab($kit));
                Alert::alert('Done :)', ' Mail Send to order mail ', 'success');
                return response()->json([
                    'mes' => 'ok from order',
                    'code' => '200'
                    ]
                );
            }else {

                Alert::alert('Done :)', ' Kit Status Updated But No Mail Sending ', 'success');
                return response()->json([
                    'mes' => 'ok',
                    'code' => '200'
                    ]
                );
            }
        }else {
            Alert::alert('Error', ' error occurred ', 'error');

            return response()->json([
                    'mes' => 'error',
                    'code' => '400'
                ]
            );
        }
    }
}
