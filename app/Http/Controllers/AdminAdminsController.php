<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Admin;
use Alert;

class AdminAdminsController extends Controller
{

     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth:admin', 'admin', 'superAdmin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->travelLogging('go to admins index', auth()->user(), 'superAdmin');
        $admins = Admin::where('deleted_at', null)
                    ->orderBy('id', 'DESC')
                    ->paginate(5);
        return view('admin.admin.index', compact('admins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|unique:admins|max:255',
            'phone' => 'required|max:15|min:11',
            'active' => 'required|numeric|max:1',
            'role' => 'required|numeric|max:3',
            'photo' => 'image|nullable|max:2500',
            'password' => 'required|min:6',
            'password_confirmation' => 'required_with:password|same:password|min:6'
        ]);
        // Get all Inputs From Reqest 
        $input = $request->except('photo_id');
        // bcrypt Password to store it in databasde
        $input['password'] = bcrypt($request->password);
        $admin = Admin::create($input);
        if( $file = $request->file('photo') ){

            $name = time() . $input['email'] . '.' . $file->getClientOriginalExtension();
            $file->move('img/private', $name);
            $path = $name;
            $admin->photo()->create( ['path' => $path] );
        }
        $this->logging('New Admin Created',null, $admin->toArray(), auth()->user(), $admin, 'superAdmin');
        Alert::alert('Admin Created', 'The Admin Has Been Created', 'success');
        return redirect()->route('admin.admins.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $admin = Admin::findOrFail($id);
        $this->travelLogging('go to admins edit', auth()->user(), 'superAdmin');
        return view( 'admin.admin.edit', compact('admin') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|unique:admins,email,'.$id,
            'phone' => 'required|max:15|min:11',
            'active' => 'required|numeric|max:1',
            'role' => 'required|numeric|max:3',
            'photo' => 'image|nullable|max:2500',
            'password' => 'nullable|min:6',
            'password_confirmation' => 'required_with:password|same:password|min:6|nullable'
        ]);
        $admin = Admin::findOrFail($id);
        $oldAdmin = $admin;
        if ( $admin->id == 1 ) {
            Alert::alert('This Admin Can Not be Updated', '', 'error');
            return redirect()->route('admin.admins.index');
        }
        $input = $request->except('photo');
        
        if(trim($request->password) == ''){

            $input = $request->except('password', 'photo');

        }else{

            $input = $request->all();
            $input['password'] = bcrypt($request->password);

        }

        if( $file = $request->file('photo') ){

            if($admin->photo){

                if(file_exists('img/private/' . $admin->photo->path))
                    unlink('img/private/' . $admin->photo->path);
                $admin->photo->delete();
            }
            $path = time() . $input['email'] . '.' . $file->getClientOriginalExtension();
            $file->move('img/private', $path);
            $admin->photo()->create([ 'path' => $path ]);

        }
        $admin->update($input);
        // Session::flash('updated', 'The admin Has Been Updated');
        Alert::alert('Admin Updated', 'The Admin Has Been Updated', 'success');
        $this->logging('Admin updated', $oldAdmin->toArray(), $request->except('password_confirmation', 'password', 'photo', '_method', '_token'), auth()->user(), $admin, 'superAdmin');
        return redirect()->route('admin.admins.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ( $request->soft ) {
            $admin = Admin::withTrashed()
                ->where('id', $id)
                ->first();
            if ( $admin->photo ) {
                if( file_exists( 'img/private/' . $admin->photo->path ) )
                    unlink( 'img/private/' . $admin->photo->path );

                $admin->photo->delete();
            }
            $admin->forceDelete();
            $this->logging('Admin deleted', null, $admin->toArray(), auth()->user(), $admin, 'superAdmin');
            Alert::alert('Admin Deleted', 'The Admin Has Been Deleted Permanently', 'error');
            return redirect()->route('admin.admins.trash');
        }
        $admin = Admin::findOrFail($id);
        if ( $admin->id == 1 ) {
            Alert::alert('This Admin Can Not be Trashed', '', 'error');
            return redirect()->route('admin.admins.index');
        }
        $admin->active = 0;
        $admin->role = 3;
        $admin->save();
        $admin->delete();
        $this->logging('Admin trashed', null, $admin->toArray(), auth()->user(), $admin, 'superAdmin');
        Alert::alert('Admin trashed', 'The Admin Has Been Moved To trash', 'warning');
        return redirect()->route('admin.admins.index');
    }

    /**
     * show trashed from storage.
     * @return \Illuminate\Http\Response
     */
    public function trash()
    {
        $admins = Admin::onlyTrashed()
                    ->orderBy('id', 'DESC')
                    ->paginate(5);
        $this->travelLogging('go to admins trash', auth()->user(), 'superAdmin');
        return view('admin.admin.trash', compact('admins') );
    }

    /**
     * restore trashed from storage.
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $admin = Admin::withTrashed()->find($id);
        $admin->restore();
        Alert::alert('Admin Restored', 'The Admin Has Been Restored', 'info');
        $this->logging('Admin restored', null, $admin->toArray(), auth()->user(), $admin, 'superAdmin');
        return redirect()->route('admin.admins.trash');
    }
}
