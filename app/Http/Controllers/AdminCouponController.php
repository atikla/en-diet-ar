<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Coupon;
use App\Dietitian;
use Alert;
use DB;

class AdminCouponController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth:admin', 'admin', 'superAdmin']);
    }


    /**
     * Display a listing of the availableCoupons.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coupons = Coupon::where('used', 0)
            ->whereCampaign(false)
            ->orderBy('id', 'DESC')
            ->paginate(5);
        $dietitians = Dietitian::whereNotNull('reference_code')->orderBy('id', 'DESC')->get();
        $dietitians->filter( function ($dietitian) {
            $dietitian->name = $dietitian->kind . '. ' . $dietitian->name . ' - ' . $dietitian->reference_code;
        });
        $dietitians = $dietitians->pluck('name', 'reference_code');
        $this->travelLogging('go to available coupon page', auth()->user(), 'superAdmin');
        return view('admin.coupon.index', compact('coupons', 'dietitians'))->with(['title' => 'Available Coupons']);
    }

    /**
     * Display a listing of the NotavailableCoupons.
     *
     * @return \Illuminate\Http\Response
     */
    public function awardedCoupons()
    {
        return 1;
    }

    /**
     * Display a listing of the NotavailableCoupons.
     *
     * @return \Illuminate\Http\Response
     */
    public function notAvailableCoupons()
    {
        $coupons = Coupon::where('used', 1)
            ->whereCampaign(false)
            ->orderBy('id', 'DESC')
            ->paginate(5);
        $dietitians = Dietitian::whereNotNull('reference_code')->orderBy('id', 'DESC')->get();
        $dietitians->filter( function ($dietitian) {
            $dietitian->name = $dietitian->kind . '. ' . $dietitian->name . ' - ' . $dietitian->reference_code;
        });
        $this->travelLogging('go to used coupon page', auth()->user(), 'superAdmin');
        return view('admin.coupon.index', compact('coupons', 'dietitians'))->with(['title' => 'Used Coupons']);
    }

    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $i = $request->items;
        $array = array();
        while($i != 0){
            $code = str_random(10);
            $existcode = Coupon::where('code', '=', $code)
                                ->first();
            if($existcode == null){
                $coupon = Coupon::create(['code' => $code, 'discount' => $request->discount, 'note' => $request->note, 'reference_code' => $request->reference]);
                array_push($array, $code . ' - ' . $request->discount . ' %');
                $i--;
            }else{
                continue;
            } 
        }
        Alert::success('Done :)', sizeof($array) . ' Coupon Has Been Created');
        $this->logging('New Coupons Created - ' .sizeof($array) . ' item -',null, $array, auth()->user(), $coupon, 'superAdmin');
        return \redirect()->route('admin.coupons.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'discount' => 'required|min:1|max:100|numeric',
            'note' => 'required|min:1|max:100'
        ]);
        // return $request->all();
        $coupon = Coupon::findOrFail($id);
        $oldcoupon = $coupon;
        $coupon->discount = $request->discount;
        $coupon->note = $request->note;
        $coupon->reference_code = $request->reference;
        $coupon->save();
        Alert::success('Done :)', 'The Coupon Has Been Updated');
        $this->logging('coupon updated', $oldcoupon->toArray(), $coupon->toArray(), auth()->user(), $coupon, 'superAdmin');
        return \redirect()->route('admin.coupons.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $coupon = Coupon::findOrfail($id); 
        $coupon->delete();
        $this->logging('Coupon deleted', null, $coupon->toArray(), auth()->user(), $coupon, 'superAdmin');
        Alert::info('deleted', 'The Coupon with code ( ' . $coupon->code . ' ) Has Been deleted');
        return \redirect()->back();
    }

    /**
     * search in resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $this->validate($request, [
            'search' => 'required|max:50',
        ]);
        $search = $request->search;
        $coupons = Coupon::where('code', 'LIKE', '%' . $search . '%')
            ->orWhere('note', 'LIKE', '%' . $search . '%')
            ->orWhere('discount', 'LIKE', '%' . $search . '%')
            ->orWhereHas('dietitian', function( $query ) use ($search) 
            {
                $query->where('reference_code', 'LIKE', '%'.$search.'%');
            })
            ->get();
        $coupons = $coupons->filter(function($coupon) { if (!$coupon->campaign) return $coupon; });
        Alert::alert('Search Result', $coupons->count() . ' items Found', 'info');
        $this->logging('Coupon searched', null, $request->search, auth()->user(), new Coupon, 'superAdmin');
        return view('admin.coupon.search', compact('coupons'));
    }
}
