<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Spatie\Activitylog\Models\Activity;

class AdminActivityLogController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth:admin', 'admin', 'superAdmin']);
    }

    public function index()
    {
        $logs = Activity::orderBy('created_at', 'DESC')
            ->get()
            ->groupBy( function ( $item ) {
                return $item->created_at->format('Y-m-d');
            });
        return view('admin.activity.index', compact('logs'));
    }
    public function show($date)
    {
        $logs = Activity::orderBy('created_at', 'DESC')
            ->get()
            ->groupBy( function ( $item ) {
                return $item->created_at->format('Y-m-d');
            });
        $selecteflog = Activity::orderBy('created_at', 'DESC')
            ->whereDate('created_at', $date)
            ->get();
        if ( $selecteflog->count() > 0 ) {
            return view('admin.activity.show', compact('logs', 'selecteflog', 'date'));
        } else {
            abort(404);
        }
    }
}
