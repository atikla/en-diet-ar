<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Requests;
use  Alert;
use App\User;
use App\Photo;
use App\Kit;

class AdminUsersController extends Controller
{


     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth:admin', 'admin', 'superAdmin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::latest()
            ->paginate(5);
        $this->travelLogging('go to users index', auth()->user(), 'superAdmin');
        return view('admin.user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->travelLogging('go to users create', auth()->user(), 'superAdmin');
        return view('admin.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|unique:users|max:255',
            // 'phone' => 'required|max:15|min:1',
            'phone' => 'required|max:15|min:11',
            'address' => 'required',
            // 'lab_id' => 'unique:users|nullable',
            'date_of_birth' => 'required|date|date_format:Y-m-d',
            'gender' => 'required',
            'password' => 'required|min:6',
            'password_confirmation' => 'required_with:password|same:password|min:6'
        ]);

        
            // Get all Inputs From Reqest 
            $input = $request->except('photo_id', 'email_verified_at');
            // bcrypt Password to store it in databasde
            $input['password'] = bcrypt($request->password);
            if ( $request->email_verified_at )
                $input['email_verified_at'] = now();

            $user = User::create($input);

            

            if($file = $request->file('photo_id')){

                // $name = time() . $file->getClientOriginalName() ;
                $name = time() . $input['email'] . '.' . $file->getClientOriginalExtension();
    
                $file->move('img/private', $name);
    
                $photo = new Photo();
                $photo->path = $name;
                $user->photo()
                    ->save($photo);
            }

            // Session::flash('created', 'The user Has Been Created');
            Alert::alert('User Created', 'The user Has Been Created', 'success');
            $this->logging('new user created',null, $user->toArray(), auth()->user(), $user, 'superAdmin');
            return redirect()->route('admin.users.index');

        // return 'store';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $user = User::findOrFail($id);
        $kits = Kit::where('user_id', null)
            ->where('registered', '=', 0)
            ->where('registered_at', null)
            ->where('kit_code', '!=', null)
            ->orderBy('id', 'DESC')
            ->pluck('kit_code', 'id');
        $this->travelLogging('go to user show', auth()->user(), 'superAdmin');
        return view('admin.user.show', compact('user', 'kits'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $this->travelLogging('go to users edit', auth()->user(), 'superAdmin');
        return view('admin.user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'unique:users,email,'.$id,
            'date_of_birth' => 'required|date|date_format:Y-m-d',
            'gender' => 'required',
            'phone' => 'required|max:15|min:11',
            'address' => 'required',
            'password_confirmation' => 'required_with:password|same:password'
        ]);
        $user = User::findOrFail($id);
        $old = $user->toArray();
        $input = $request->except('photo_id', 'email_verified_at');
        
        if(trim($request->password) == ''){

            $input = $request->except('password', 'photo_id');

        }else{

            $input = $request->all();
            $input['password'] = bcrypt($request->password);

        }

        if($file = $request->file('photo_id')){

            if($user->photo){

                if(file_exists('img/private/' . $user->photo->path))
                    unlink('img/private/' . $user->photo->path);
                $user->photo->delete();
            }
            $name = time() . $input['email'] . '.' . $file->getClientOriginalExtension();

            $file->move('img/private', $name);

            $photo = new Photo();
            $photo->path = $name;
            $user->photo()
                ->save($photo);
        }

        if ( $request->email_verified_at )
            $input['email_verified_at'] = now();

        $user->update($input);
        // Session::flash('updated', 'The user Has Been Updated');
        Alert::alert('User Updated', 'The user Has Been Updated', 'success');
        $this->logging('user updated', $old, $user->toArray(), auth()->user(), $user, 'superAdmin');
        return redirect()->route('admin.users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
        if ( isset($_GET['soft'] ) ) {
            $user = User::withTrashed()
                ->where('id', $id)
                ->first();
            if ( $user->photo ) {

                if( file_exists('img/private/' . $user->photo->path) )
                    unlink('img/private/' . $user->photo->path);
                $user->photo->delete();
            }
            $user->forceDelete();
            // Session::flash('deleted', 'The user Has Been Deleted Permanently');
            Alert::alert('User Deleted', 'The User Has Been Deleted Permanently', 'error');
            $this->logging('user deleted', null, $user->toArray(), auth()->user(), $user, 'superAdmin');
            return redirect()->route('admin.users.trash');
        }
        $user = User::findOrFail($id);
        // $user->is_admin = 0;
        // $user->save();
        $user->delete();
        // Session::flash('deleted_soft', 'The user Has Been Moved To trash');
        Alert::alert('User trashed', 'The User Has Been Moved To trash', 'warning');
        $this->logging('User trashed', null, $user->toArray(), auth()->user(), $user, 'superAdmin');
        return redirect()->route('admin.users.index');

    }

    public function trash()
    {    
        $users = User::onlyTrashed()
            ->orderBy('id', 'DESC')
            ->paginate(5);
        $this->travelLogging('go to users trash', auth()->user(), 'superAdmin');
        return view('admin.user.restore', compact('users'));   
    }
    
    public function restore($id)
    {
        $user = User::withTrashed()->find($id);
        $user->restore();
        // Session::flash('restored', 'The user Has Been Restored');
        Alert::alert('User Restored', 'The User Has Been Restored', 'info');
        $this->logging('user restored', null, $user->toArray(), auth()->user(), $user, 'superAdmin');
        return redirect()->route('admin.users.index');
        // return redirect('/admin/users');
    }

    /**
     * search in resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $this->validate($request, [
            'search' => 'required|min:3|max:50',
        ]);
        $search = $request->search;
        $users = User::where('name', 'LIKE', '%' . $search . '%')
            ->orWhere('email', 'LIKE', '%' . $search . '%')
            ->orWhere('address', 'LIKE', '%' . $search . '%')
            ->orWhere('phone', 'LIKE', '%' . $search . '%')
            ->get();
        Alert::alert('Search Result', $users->count() . ' items Found', 'info');
        $this->logging('user searched for ' . $request->search , null, $request->search, auth()->user(), new User, 'superAdmin');
        return view('admin.user.search', compact('users'));
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function link(Request $request, $id)
    {
        $this->validate($request, [
            "kit"  => "required",
        ]);
        $kit = Kit::findOrfail($request->kit);
        $kit->user_id = $id;
        $kit->registered  = 1;
        $kit->registered_at  = now();
        $kit->save();
        Alert::alert('Done :)', 'kit with kit code = ' . $kit->kit_code . ' Has Been link to user', 'success')->autoclose(10000);
        $this->travelLogging('kit with  code = ' . $kit->kit_code . ' Has Been link to user - user email: ' . $kit->user->email . ' -', auth()->user(), 'superAdmin');
        return \redirect()->back();
    }
}