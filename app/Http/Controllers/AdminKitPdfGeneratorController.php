<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kit;
use Alert;
use GuzzleHttp\Client;
use App;

class AdminKitPdfGeneratorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth:admin', 'admin', 'superAdmin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kits = Kit::whereNotNull('food')
            ->whereNotNull('microbiome')
            ->orderBy('id', 'DESC')
            ->get();
        return view('admin.kit.generate', compact('kits'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function Generate(Request $request, $kitCode)
    {
        $savedlocal = App::getLocale();

        $kit = Kit::where('kit_code', '=', $kitCode)->first();
        if ( $kit ) {
            if ( $kit->food && $kit->microbiome ) {
                $locale = $request->lang;
                if ( !array_key_exists( $request->lang, config()->get('app.supported_languages' ) ) )
                    $locale = 'tr';
                App::setLocale($locale);
                $client = new Client();
                $data = $kit->pdf;
                try {
                    $result = $client->post('https://enbiosis-pdf-generator.herokuapp.com/api/pdf/generate', [
                        'headers' => [
                            'Content-Type' => 'application/json',
                            'Content-Language' => \App::getLocale()],
                        'body' => json_encode($data),
                    ]);
                    if( $result->getStatusCode() == 200 ) {
                        $data = json_decode ($result->getBody()->getContents(), true);
                        App::setLocale($savedlocal);
                        $this->logging('generate a pdf for kit code' . $kit->kit_code . ' pdf lang is - ' . $locale, null, null, auth()->user(), $kit, 'superAdmin');
                        return view('admin.kit.pdf', ['food' => $data['food'], 'microbiome' => $data['microbiome'], 'kit' => $kit ] );        
                    } else {
                        Alert::alert('node server return error', '', 'info');
                        return redirect()->route('admin.kits.index');
                    }
                } catch (\Throwable $th) {
                    Alert::alert('while trying to get a pdf file from node server error occurred', '', 'info');
                    return redirect()->route('admin.kits.index');
                }
            } else {
                Alert::alert('Hata Olustu3', 'we dont have a result for this kit', 'info');
                return redirect()->route('admin.kits.index');
            }
        } else {
            abort(404);
        }
    }
}
