<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;
use App\SellingTeam;
use App\Dietitian;
use Alert;

class AdminSellingTeamsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teams = SellingTeam::latest()->paginate(5);
        return view('admin.selling-teams.index', compact('teams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cities = City::all()->pluck('city','id');
        return view('admin.selling-teams.create', compact('cities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createLeader($id)
    {
        $team = SellingTeam::findOrFail($id);
        if ( $team->leader ) {
            return redirect()->route('admin.selling-teams.show', $team->id);
        } else {
            $dietitians = Dietitian::whereNull('selling_team_id')->orWhere('selling_team_id', $team)
            ->orWhere('selling_team_id', $team->id)
            ->orderBy('id', 'DESC')
            ->get();
            $dietitians->filter( function ($dietitian) {
                $dietitian->name = $dietitian->kind . '. ' . $dietitian->name . ' - ' . $dietitian->reference_code;
            });
            $dietitians = $dietitians->pluck('name','id');
            return view('admin.selling-teams.assignleader', compact('team', 'dietitians'));
        }
        
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function createMember($id)
    {
        $team = SellingTeam::findOrFail($id);
        $dietitians = Dietitian::whereNull('selling_team_id')
            ->orderBy('id', 'DESC')
            ->get();
        $dietitians->filter( function ($dietitian) {
            $dietitian->name = $dietitian->kind . '. ' . $dietitian->name . ' - ' . $dietitian->reference_code;
        });
        $dietitians = $dietitians->pluck('name','id');
        if ( $team->leader ) {
            if ( $dietitians->isEmpty() )
                Alert::alert('You Don\'t Have A Members to add', 'Go to add new ' . $team->kind, 'info');

            return view('admin.selling-teams.addMember', compact('team', 'dietitians'));
        } else {
            Alert::alert('You Don\'t Have A Leader For This team', 'Before add member, you must assign a leader for this team', 'info');
            return view('admin.selling-teams.assignleader', compact('team', 'dietitians'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'note' => 'required|max:255',
            'cities' => 'required',
        ]);
        $input = $request->except('cities');
        $sellingTeam = SellingTeam::create($input);
        $cities = $request->cities;
        if ( $cities[0] == null)unset($cities[0]);
        $sellingTeam->cities()->attach( $cities );
        $this->logging('New Selling Teams Created',null, $sellingTeam->toArray(), auth()->user(), $sellingTeam, 'superAdmin');
        Alert::alert('selling teams Created', 'The selling teams Has Been Created', 'success');
        return redirect()->route('admin.selling-teams.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeLeader(Request $request, $id)
    {
        $this->validate($request, [
            'leader_id' => 'required',
        ]);
        $sellingTeam = SellingTeam::findOrFail($id);
        $dietitian = Dietitian::findOrFail($request->leader_id);
        $sellingTeam->update($request->all());
        $dietitian->update([ 'selling_team_id' => $sellingTeam->id] );
        $this->logging('assign leader ( ' . $dietitian->name . ' - ' .$dietitian->reference_code . ' ) for ' . $sellingTeam->name . ' team ',null, $sellingTeam->toArray(), auth()->user(), $sellingTeam, 'superAdmin');
        Alert::alert('Done', 'Leader Assign', 'success');
        return redirect()->route('admin.selling-teams.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function changeLeader(Request $request, $id)
    {
        $this->validate($request, [
            'old_leader_id' => 'required',
            'leader_id' => 'required|different:old_leader_id',
        ]);
        $sellingTeam = SellingTeam::findOrFail($id);
        $dietitian = $sellingTeam->dietitians()->findOrFail($request->leader_id);
        $oldDietitian = $sellingTeam->dietitians()->findOrFail($request->old_leader_id);
        $sellingTeam->update(['leader_id' => $request->leader_id]);
        $dietitian->selling_team_id = $sellingTeam->id;
        $dietitian->save();
        $this->logging('Remove ( ' . $oldDietitian->name . ' - ' .$oldDietitian->reference_code . ' ) form leading and add ( ' . $dietitian->name . ' - ' .$dietitian->reference_code . ' ) as a leader for '. $sellingTeam->name . ' team ',null, $sellingTeam->toArray(), auth()->user(), $sellingTeam, 'superAdmin');
        Alert::alert('Done', 'Leader Changed', 'success');
        return redirect()->route('admin.selling-teams.show', $sellingTeam->id);
    }
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function storeMember(Request $request, $id)
    {
        $this->validate($request, [
            'members' => 'required',
        ]);
        $sellingTeam = SellingTeam::findOrFail($id);
        $list = [];
        $list['team'] = $sellingTeam->name;
        foreach ( $request->members as $member ) {
            $dietitian = Dietitian::find($member);
            if ( $dietitian ) {
                $dietitian->selling_team_id = $sellingTeam->id;
                $dietitian->save();
                $list[$dietitian->id . ' - ' . $dietitian->kind] = $dietitian->name . ' - ' . $dietitian->reference_code;
            }
        }
        $this->logging('Added Members to ' . $sellingTeam->name . ' ' .$dietitian->kind . ' team ',null, $list, auth()->user(), $sellingTeam, 'superAdmin');
        Alert::alert('Done', 'Members Added', 'success');
        return redirect()->route('admin.selling-teams.show', $sellingTeam->id);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function storecity(Request $request, $id)
    {
        $this->validate($request, [
            'city' => 'required',
        ]);
        $team = SellingTeam::findOrFail($id);
        
        if ( $team->leader ) {
            $cities = [];
            foreach ( $request->city as $city ) {
                if ($temp = City::find($city))
                array_push($cities, $temp->city);
            }
            $team->cities()->attach($request->city);
            Alert::alert('Cities Added', '( ' . implode(' - ', $cities) . ' ) Added to ' . $team->name .' - ' . $team->kind . ' team', 'success');
            $this->logging('Cities ( ' . implode(' - ', $cities) . ' ) Added to ' . $team->name .' - ' . $team->kind . ' team' ,null, $cities, auth()->user(), $team, 'superAdmin');
            return redirect()->route('admin.selling-teams.show', $team->id);
        } else {
            Alert::alert('You Don\'t Have A Leader For This team', 'Before add member, you must assign a leader for this team', 'info');
            return view('admin.selling-teams.assignleader', compact('team', 'dietitians'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $team = SellingTeam::findOrFail($id);
        if ( $team->leader ) {
            $members = $team->dietitians;
            $members->filter( function ($dietitian) {
                $dietitian->name = $dietitian->kind . '. ' . $dietitian->name . ' - ' . $dietitian->reference_code;
            });
            $members = $members->pluck('name','id');
            $cities = City::all()->pluck('city','id');
            foreach($team->cities as $city){ unset($cities[$city->id]); }
            return view('admin.selling-teams.show', compact('team', 'members', 'cities'));
        } else {
            $dietitians = Dietitian::whereNull('selling_team_id')
            ->orderBy('id', 'DESC')
            ->get();
            $dietitians->filter( function ($dietitian) {
                $dietitian->name = $dietitian->kind . '. ' . $dietitian->name . ' - ' . $dietitian->reference_code;
            });
            $dietitians = $dietitians->pluck('name','id');
            Alert::alert('You Don\'t Have A Leader For This team', 'you must assign a leader for this team', 'info');
            return view('admin.selling-teams.assignleader', compact('team', 'dietitians'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            '*name' => 'required',
            '*note' => 'required',
        ]);
        $team = SellingTeam::findOrFail($id);
        $temp = $team->toArray();
        $team->update([
            'name' => $request[$id . '_name'],
            'note' => $request[$id . '_note'],
        ]);
        Alert::alert('Selling Team updated', 'The SellingTeam Has Been updated', 'success');
        $this->logging('Selling Team update name:' . $team->name , $temp, $team->toArray(), auth()->user(), $team, 'superAdmin');
        return redirect()->route('admin.selling-teams.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        
        $team = SellingTeam::findOrFail($id);
        if ($dietitians = $team->Leader) {
            $dietitians->update(['selling_team_id' => null]);
            $team->update(['leader_id' => null]);
        }
        $team->dietitians->filter( function($dietitian) {
            $dietitian->update(['selling_team_id' => null]);
        });
        unset($team->dietitians);
        $team->cities()->detach();
        if ($request->soft) {

        } else {
            $team->delete();
            Alert::alert('Selling Team trashed', 'The SellingTeam Has Been Moved To trash', 'warning');
            $this->logging('Selling Team trashed name:' . $team->name , null, $team->toArray(), auth()->user(), $team, 'superAdmin');
            return redirect()->route('admin.selling-teams.index');
        }
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyMember($id)
    {
        $member = Dietitian::whereId($id)->whereNotNull('selling_team_id')->firstOrFail();
        $team = $member->sellingTeam;
        if ( $team ) {
            if ( $team->leader->id == $member->id) {
                $team->leader_id = null;
                $team->save();
            }
            $member->selling_team_id = null;
            $member->save();
            $this->logging('Remove A member ( ' . $member->name . ' - ' .$member->reference_code . ' ) from ' . $team->name . ' team ',null, $team->toArray(), auth()->user(), $team, 'superAdmin');
            Alert::alert('Member Removed From Team', 'You Remove A member ( ' . $member->name . ' - ' .$member->reference_code . ' ) from ' . $team->name . ' team ', 'info');
            return redirect()->route('admin.selling-teams.show', $team->id);
        } else {
            abort(404);
        }
    }
         /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyCity($team, $city)
    {
        $team = SellingTeam::findOrFail($team);
        $old = $team->cities()->where('city_id', $city)->first();
        $team->cities()->detach($city);
        $this->logging('Remove A city ( ' . $old->city . ' ) from ' . $team->name . ' team ', $old, $team->toArray(), auth()->user(), $team, 'superAdmin');
        Alert::alert('Done', 'You Remove A city ( ' . $old->city . ' ) from ' . $team->name . ' team ', 'info');
        return redirect()->route('admin.selling-teams.show', $team->id);

    }
}
