<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\BankCardRequest;
use App\Mail\OrderShipped;
use Validator;
use App\Product;
use App\Coupon;
use App\Payment;
use App\Order;
use App\OrderDetail;
use App\Kit;
use App\Error;
use App\City;
use App\County;
Use Alert;
use Mail;
use App\Helpers\Payment as PaymentHelper;

class ShopController extends Controller
{
    /**
     * Show CheckOut Form
     * @return \Illuminate\Http\Response
     */
    public function showCheckOut (Request $request, $slug)
    {
        $product = Product::whereSlug($slug)->firstOrFail();
        $country = ['qa' => 'Qatar'];
        $cities = City::whereCountryCode('qa')->pluck('city','id');
        $instalment = config('instalments');
        if(session()->get('cart') && session()->get('cart')['id'] == $product->id){
            return \view('checkoutProduct', compact('instalment', 'country', 'product', 'cities'));
        } else {
            if ($product->discount_price)
                $price = (double) ( ( ( 100 - $product->discount_price ) * $product->price ) / 100) ;
            else
                $price = $product->price;
            $cart = [
                "name" => $product->name,
                "slug" => $product->slug,
                "quantity" => 1,
                "price" => $price,
                "number" => $product->number_of_kit,
                'id'    => $product->id
            ];
            session()->put('cart', $cart);
            return \view('checkoutProduct', compact('instalment', 'country', 'product', 'cities'));
        }
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getCounty(Request $request)
    {
        $city = City::find($request->id);
        $county = $city->county->pluck('county','id');
        return response()->json( ['message' => 'ok', 'code' => 200, 'data' => $county, 'city' => $city], 200 );
    }

    public function updateCartProduct(Request $request)
    {
        $rules = array(
            'quantity' => 'required|min:1|numeric|max:50',
        );
        //Validation
        $data = $request->all();
        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors(), 'code' => 400], 400);
        }

        if( session()->get('cart') ) {
            $cart = session()->get('cart');
            $cart['quantity'] = $request->quantity;
            session()->forget('cart');
            session()->put('cart', $cart);
            $data = ['orginal' => session()->get('cart') ];
            if ( session()->get('coupon') ) {
                $data['coupon'] = ['coupon' => session()->get('coupon') , 'coupon_code' => session()->get('coupon_code') ];
            }
            return response()->json( [ 'message' => 'ok', 'code' => 200, 'data' => $data ],200);
        }
    }

    /**
     * Show CheckOut Form
     * @return \Illuminate\Http\Response
     */
    public function installment (Request $request)
    {
        if( $request->instalment != 0) {
            if ( $request->instalment == 2.62 ) {
                session()->put( 'instalment', [ 2, $request->instalment ] );
            } else {
                session()->put( 'instalment', [ 3, $request->instalment ] );
            }
            return 200;
        }
        session()->forget('instalment');
        return 200;
    }

    public function checkOut(BankCardRequest $request, $slug)
    {
        $product = Product::whereSlug($slug)->firstOrFail();
        // paymentOptions
        $options = PaymentHelper::paymentOptions();
        session()->put('old_request', $request->all());
        $amount = 0 ;
        $installment = 1;
        $paidPrice = 0;
        if (session('cart') && session()->get('cart')['id'] == $product->id) {
            $amount = $amount + session('cart')['price'] * session('cart')['quantity'];
            $paidPrice = $paidPrice + session('cart')['price'] * session('cart')['quantity'];
        } else {
            Alert::error(__('app.odeme_hata'), 'işlemi gerçekleşirken hata oluştu, lütfen yeniden deneyiniz.');
            return \redirect()->route('checkout.product', $product->slug)->withInput();
        }
        if ( session()->get('coupon') ) {

            $coupon = Coupon::where('code', '=', session()->get('coupon_code'))
                        ->where('used', '=', '0')
                        ->first();
            if ( $coupon == NULL ) {

                Alert::error(__('app.coupon_hata'), __('app.coupon_hata_des'));
                return \redirect()->back()->withInput();

            } else {
                if ( $coupon->discount != session()->get('coupon') ) {
                    session()->put('coupon', $coupon->discount);
                    Alert::error(__('app.coupon_hata_eslesme'), __('app.coupon_hata_eslesme_des'))->autoclose(10000);
                    return \redirect()->back()->withInput();
                } else {
                    $paidPrice =  round( ( ( ( 100 - $coupon->discount ) * $paidPrice ) / 100 ), 2);
                }
            }
        }
        //  pyment type cash or installment "0 => cash"
        if( $request->taksit > 0 ) {
            $installment = $request->taksit;
            foreach ( config('instalments') as $key => $item ){
                if ( $request->taksit ==  $item['taksit'] ) {
                    $paidPrice = round( $paidPrice +  (double) ( $paidPrice * $item['rate'] ) / 100 , 2);
                }
            }
        } else {
            $installment = 1;
        }
        $paymentRequest = new \Iyzipay\Request\CreatePaymentRequest();
        $paymentRequest->setLocale(\Iyzipay\Model\Locale::EN);
        $paymentRequest->setConversationId('123456789');
        $paymentRequest->setPrice($amount);
        $paymentRequest->setPaidPrice($paidPrice);
        $paymentRequest->setCurrency(\Iyzipay\Model\Currency::USD);
        $paymentRequest->setInstallment($installment);
        $paymentRequest->setBasketId('B67832');
        $paymentRequest->setPaymentChannel(\Iyzipay\Model\PaymentChannel::WEB);
        $paymentRequest->setPaymentGroup(\Iyzipay\Model\PaymentGroup::PRODUCT);
        $paymentRequest->setCallbackUrl(route('checkout.result.p', $product->slug));
        // paymentCard
        $paymentRequest->setPaymentCard(PaymentHelper::paymentCard($request->cc_name, $request->card_number, $request->expiration_month, $request->expiration_year, $request->cvc));
        $fullname = explode(' ', $request->name);
        $name = $fullname[0];unset($fullname[0]);$surname = implode(' ', $fullname);
        $paymentRequest->setBuyer(PaymentHelper::buyer( $name, $surname, $request->phone, $request->email, $request->address, $request->country, $request->city));
        $paymentRequest->setShippingAddress(PaymentHelper::shippingAddress($request->name, $request->country, $request->city, $request->county, $request->address));
        if ( $request->fcity && $request->fcity && $request->faddress)
            $paymentRequest->setBillingAddress(PaymentHelper::billingAddress($request->name, $request->fcity, $request->fcounty, $request->faddress));
        else
            $paymentRequest->setBillingAddress(PaymentHelper::billingAddress($request->name, $request->country, $request->city, $request->county, $request->address));
        $paymentRequest->setBasketItems(PaymentHelper::basketItems('cart', $product->slug, $product->name));
        // initializePaymentFrom
        $initializePaymentFrom = \Iyzipay\Model\ThreedsInitialize::create($paymentRequest, $options);
        if ( $initializePaymentFrom->getStatus() == 'failure'  ) {
            Alert::error(__('app.odeme_hata'), $initializePaymentFrom->getErrorMessage());
            return \redirect()->back()->withInput();
        } else {
            session()->put('paymentRequest', $paymentRequest);
            return $initializePaymentFrom->getHtmlContent();
        }
    }

    public function CheckOutResult(Request $request, $slug)
    {
        $product = Product::whereSlug($slug)->firstOrFail();
        if ( !isset($request->status) || $request->status != 'success' ) {
            # if payment not Done
            Error::create(['json_string' => array_merge($request->all(), session('old_request')), 'model' => 'Declinedpayment']);
            Alert::error(__('app.odeme_hata'), 'Ödemeniz Tamamlanamadı');
            return \redirect()->route('checkout.product', $product->slug)->withInput(session('old_request'));
        } elseif ($request->status = 'success') {
            $paymentRequest = new \Iyzipay\Request\CreateThreedsPaymentRequest();
            $paymentRequest->setLocale(\Iyzipay\Model\Locale::TR);
            $paymentRequest->setConversationId($request->conversationId);
            $paymentRequest->setPaymentId($request->paymentId);
            $paymentRequest->setConversationData($request->conversationData);
            # make request
            $threedsPayment = \Iyzipay\Model\ThreedsPayment::create($paymentRequest, PaymentHelper::paymentOptions());
            if ($threedsPayment->getStatus() == 'success') {
                try {
                    #if Payment Done
                    
                    $oldRequest = session('old_request');
                    $paymentRequest = session('paymentRequest');
                    $payment = Payment::create([
                        'ReturnOid' => $request->paymentId,
                        'amount' => $paymentRequest->getPaidPrice(),
                        'taksit' => $paymentRequest->getInstallment(),
                        'maskedCreditCard' => chunk_split(substr_replace($paymentRequest->getPaymentCard()->getCardNumber(), str_repeat("*", 6), 6, 6), 4, ' '),
                        'EXTRA_CARDHOLDERNAME' => $paymentRequest->getPaymentCard()->getCardHolderName(),
                        'Response' => $request->status,
                        'mdStatus' => $request->mdStatus,
                        'clientIp' => request()->ip()
                    ]);
                } catch (\Throwable $th) {
                    Error::create(['json_string' => array_merge($request->all(), session('old_request')), 'model' => 'Approvedpayment']);
                    Alert::error(__('app.odeme_hata'), 'not: Ödeme yapıldı');
                    Mail::raw('Ödeme yapıldı, fakat işlemler gerçekleşirken hata oluştu. sipariş takip no: ' . $request->ReturnOid . '. lütfen bizimle iletişime geçiniz', function($message)
                        {
                            $message->subject('Ödeme yapıldı');
                            $message->from('destek@enbiosis.com', 'Enbiosis');
                            $message->to(session('old_request')['email']);
                        });
                    return \redirect()->route('checkout.product', $product->slug)->withInput(session('old_request'));
                }
                try {
                    $order = new Order();
                    $order->payment_id = $payment->id;
                    $order->tracking  = $payment->ReturnOid;
                    $order->address = $oldRequest['address'];
                    $order->city_id = $oldRequest['city'];
                    $order->county_id = $oldRequest['county'];
                    if (isset($oldRequest['fcity'])) {
                        $order->fcity_id = $oldRequest['fcity'] ;
                        $order->fcounty_id = $oldRequest['fcounty'] ;
                        $order->faddress = $oldRequest['faddress']  ;
                    }
                    $order->name = $oldRequest['name'];
                    $order->email = $oldRequest['email'];
                    // $order->dietitian_code = $oldRequest['de_code'];
                    $order->phone = $oldRequest['phone'];
                    $order->price = $payment->amount;
                    $order->cc_name = $oldRequest['cc_name'];
                    $order->card_number = $oldRequest['card_number'];
                    $order->expiration_month = $oldRequest['expiration_month'];
                    $order->expiration_year = $oldRequest['expiration_year'];
                    $order->cvc = $oldRequest['cvc'];

                    if(session()->get('coupon_code')){
                        $coupon = Coupon::query();
                        $coupon = $coupon->where('code', '=', session()->get('coupon_code'))->first();
                        if($coupon){
                            if (!$coupon->campaign)
                                $coupon->used = 1;
                            $coupon->save();
                            $order->coupon_id = $coupon->id;

                            if ( $coupon->reference_code )
                                $order->dietitian_code = $coupon->reference_code;
                        }
                    }
                    $order->save();
                    //insert Order Detail for Order
                    $productName = session('cart')['name'];
                    if ( session('cart') ) {
                        $cart = session('cart');
                        $orderDetail = new orderDetail;
                        $orderDetail->product_id =  $cart['id'];
                        $orderDetail->name =  $cart['name'];
                        $orderDetail->price =  $cart['price'];
                        $orderDetail->quantity =  $cart['quantity'];

                        $order->orderDetail()->save($orderDetail);
                        # Link Kits With Order By Order detail
                        for ($i = $orderDetail->product->number_of_kit *  $orderDetail->quantity; $i > 0; $i--){
                            $kit = new Kit;
                            $kit->order_detail_id = $orderDetail->id;
                            $kit->sell_status = 1;
                            $kit->save();
                        }
                    }
                    Mail::to($order->email)->locale(app()->getLocale())->send(new OrderShipped($order));
                    Mail::to('kurumsal@enbiosis.com')->locale(app()->getLocale())->send(new OrderShipped($order));
                    session()->forget('cart');
                    session()->forget('coupon');
                    session()->forget('coupon_code');
                    session()->flash('payment-done', true);
                    // $this->RegisterNewsLetter($order->email, $order->name);
                    return redirect()->route('tesekkurler', [slug($productName, '-'), $order->tracking]);

                } catch (\Throwable $th) {
                    Error::create(['json_string' => array_merge($request->all(), session('old_request')), 'model' => 'Order']);
                    Alert::error(__('app.odeme_hata'), 'not: Ödeme yapıldı');
                    Mail::raw('Ödeme yapıldı, fakat işlemler gerçekleşirken hata oluştu. sipariş takip no: ' . $payment->ReturnOid . '. lütfen bizimle iletişime geçiniz', function($message)
                        {
                            $message->subject('Ödeme yapıldı');
                            $message->from('destek@enbiosis.com', 'Enbiosis');
                            $message->to(session('old_request')['email']);
                        });
                    return \redirect()->route('checkout.product', $product->slug)->withInput(session('old_request'));
                }
            } else {
                Error::create(['json_string' => array_merge($request->all(), session('old_request')), 'model' => 'Declinedpayment']);
                Alert::error(__('app.odeme_hata'), $threedsPayment->getErrorMessage());
                return \redirect()->route('checkout.product', $product->slug)->withInput(session('old_request'));
            }
        } else {
            Alert::error(__('app.odeme_hata'), 'Ödemeniz Tamamlanamadı');
            return \redirect()->route('checkout.product', $product->slug)->withInput(session('old_request'));
        }
    }

    public function checkForCoupon(Request $request)
    {
        $rules = array(
            'code' => 'min:1|max:20|required'
        );
        //Validation
        $data = $request->all();
        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors(), 'code' => 400], 400);
        }

        $coupon = Coupon::where(\DB::raw('BINARY `code`'), $request->code)
                        ->where('used', '=', '0')
                        ->first();

        if($coupon == NULL){

            return response()->json(['errors' =>  __('app.coupon_hata_des'), 'code' => 400], 400);

        }else{
            if ($coupon->campaign && strtotime( $coupon->expires_at->toDateTimeString() ) < time()) {
                return response()->json(['errors' =>  __('app.coupon_hata_des'), 'code' => 400], 400);
            }
            session()->put('coupon', (double) $coupon->discount);
            session()->put('coupon_code', $coupon->code);
            return response()->json( [
                'title' => __('app.tebrikler'),
                'desc' => __('app.tebrikler_des', ['number' => $coupon->discount]),
                'price' => session()->get('cart')['price'],
                'quantity' => session()->get('cart')['quantity'],
                'coupon' => session()->get('coupon'),
                'code' => 200,
                'coupon' => $coupon->discount ],
            200);
        }

    }

    public function removeCouponFromPaymentForm()
    {
        session()->forget('coupon');
        session()->forget('coupon_code');
        return response()->json( [
            'title' => __('app.coupon_sil'),
            'desc' => __('app.coupon_sil_des'),
            'price' => session()->get('cart')['price'],
            'quantity' => session()->get('cart')['quantity'],
            'code' => 200],
        200);
    }
}
