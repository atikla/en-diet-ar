<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kit;
use App\Suggestion;
use Alert;

class AdminSuggestionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth:admin', 'admin', 'superAdmin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index()
    // {
    //     //
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
    //     //
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $kitId)
    {   
        $this->validate($request, [
            'description' => 'required',
            'title' => 'required',
        ]);
        $kit = Kit::findOrFail($kitId);
        $kit->suggestion()->create($request->all());
        Alert::alert('Suggestion Create', 'The suggestion Has Been Create', 'success');
        $this->logging('new suggestion created for kit code: ' . $kit->kit_code ,null, $request->except('_method', '_token'), auth()->user(), new Suggestion, 'superAdmin');
        return redirect()->route('admin.kits.show', $kit->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function show($id)
    // {
    //     //
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function edit($id)
    // {
    //     //
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $kitId, $id)
    {
        return $kitId;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($kitId, $id)
    {
        $kit = Kit::findOrFail($kitId);
        if ( $kit->suggestion->contains('id', $id) ) {
            $suggestion = $kit->suggestion->where('id', '=', $id)->first();
            $suggestion->delete();
            Alert::alert('Suggestion Deleted', 'The suggestion Has Been Deleted Permanently', 'success');
            $this->logging('suggestion deleted', null, $suggestion->toArray(), auth()->user(), $suggestion, 'superAdmin');

            return redirect()->route('admin.kits.show', $kit->id);
        }
        Alert::alert('Suggestion Can Not Deleted', 'The suggestion Has Not Been Deleted', 'error');
        return redirect()->route('admin.kits.show', $kit->id);
    }
}
