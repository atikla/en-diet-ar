<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Resource;
use App\Photo;
use Alert;

class AdminResourcesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth:admin', 'admin', 'superAdmin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $resources = Resource::where('deleted_at', null)
            ->orderBy('id', 'DESC')
            ->paginate(5);
        $this->travelLogging('go to resources index', auth()->user(), 'superAdmin');
        return view('admin.resource.index', compact('resources'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'video_url' => 'required',
            'imageUrl' => 'required',
            'embed_video' => 'required',
            'photo' => 'image|nullable|max:2500',
        ]);
        $resource = Resource::create([
            'video' => $request->video_url,
            'embed_video' => $request->embed_video,
            'description' => $request->description
            ]);
        if ( $file = $request->file('photo') ) {
            $name = time() . '-' . rand() . '.' . $file->getClientOriginalExtension();
            $file->move('img/videoimg', $name);
            $name = url('/') . '/img/videoimg/' . $name;
            $photo = new Photo();
            $photo->path = $name;
            $resource->photo()->save($photo);
        } else {
            $photo = new Photo();
            $photo->path = $request->imageUrl;
            $resource->photo()->save($photo);
        }
        Alert::alert('Resource Created', 'The Resource Has Been Created', 'success');
        $this->logging('new resource created',null, $resource->toArray(), auth()->user(), $resource, 'superAdmin');
        return redirect()->route('admin.resources.index');
    }

    // /**
    //  * Display the specified resource.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function show($id)
    // {
    //     //
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $resource = Resource::findOrFail($id);
        $path = $resource->photo->path;
        if ( strpos( $path, 'img/videoimg/' ) !== false ) {
            $path = explode('/', $path );
            $path = end( $path );
        
            if( file_exists( 'img/videoimg/' . $path ) )
                unlink( 'img/videoimg/' . $path );  
                $resource->photo->delete(); 
        }
        $resource->delete();
        Alert::alert('Resource Deleted', 'The Resource Has Been Deleted Permanently', 'error');
        $this->logging('resource deleted', null, $resource->toArray(), auth()->user(), $resource, 'superAdmin');
        return redirect()->route('admin.resources.index');
    }
}
