<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Requests;
use  Alert;
use App\Order;
use App\Dietitian;
use App\Exports\ExportOrder;
use Maatwebsite\Excel\Facades\Excel;

class AdminOrdersController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
     public function __construct()
    {
        $this->middleware(['auth:admin', 'admin', 'superAdmin']);
    }

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::orderBy('id', 'DESC')
                        ->paginate(5);
        $this->travelLogging('go to orders index', auth()->user(), 'superAdmin');
        return \view('admin.order.index', \compact('orders'));
    }

    public function export()
    {
        app()->setlocale('en');
        return Excel::download(new ExportOrder, 'orders.csv');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
    //     //
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(Request $request)
    // {
    //     //
    // }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::findOrFail($id);
        $dietitians = Dietitian::whereNotNull('reference_code')
        ->select( \DB::raw("CONCAT(name,' - ',reference_code) AS name"),'id' )
        ->orderBy('id', 'DESC')
        ->pluck('name','id');
        $this->logging('go to orders show ' . $order->tracking, null, $order->toArray(), auth()->user(),  $order, 'superAdmin');
        return \view('admin.order.show', \compact('order', 'dietitians'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function edit($id)
    // {
    //     //
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, $id)
    // {
    //     //
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    // {
    //     //
    // }
    /**
     * search in resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $this->validate($request, [
            'search' => 'required|max:50|min:3',
        ]);
        $search = $request->search;
        $orders = Order::where('tracking', 'LIKE', '%' . $search . '%')
            ->orWhere('address', 'LIKE', '%' . $search . '%')
            ->orWhere('name', 'LIKE', '%' . $search . '%')
            ->orWhere('email', 'LIKE', '%' . $search . '%')
            ->orWhere('dietitian_code', 'LIKE', '%' . $search . '%')
            ->orWhere('phone', 'LIKE', '%' . $search . '%')
            ->orWhere('cc_name', 'LIKE', '%' . $search . '%')
            ->orWhere('card_number', 'LIKE', '%' . $search . '%')
            ->orWhereHas('city', function( $query ) use ($search) 
            {
                $query->where('city', 'LIKE', '%'.$search.'%');
            })
            ->orWhereHas('county', function( $query ) use ($search) 
            {
                $query->where('county', 'LIKE', '%'.$search.'%');
            })
            ->get();
        Alert::alert('Search Result', $orders->count() . ' items Found', 'info');
        $this->logging('order searched for ' . $request->search , null, $request->search, auth()->user(), new Order, 'superAdmin');
        return view('admin.order.search', compact('orders'));
    }

    public function assignRefCode(Request $request, $id)
    {
        $this->validate($request, [
            'dietitian_id' => 'required',
        ]);

        $order = Order::findOrFail($id);
        $oldorder = Order::findOrFail($id);
        $dietitian = Dietitian::findOrFail($request->dietitian_id);
        $order->dietitian_code = $dietitian->reference_code;
        $order->save();
        $this->logging(' Order ( ' . $order->tracking . ' ) Reference Code Changed ( old: ' . $oldorder->dietitian_code . ', new: ' . $order->dietitian_code .  ' )' , ['reference_code' => $oldorder->dietitian_code], $order->toArray(), auth()->user(),  $order, 'superAdmin');
        Alert::alert('Reference Changed', ' Order Reference Code Changed ( old: ' . $oldorder->dietitian_code . ', new: ' . $order->dietitian_code .  ' )', 'success');
        return redirect()->route('admin.orders.show', $order->id);

    }
}
