<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Requests;
use  Alert;
use App\City;
use App\Kit;
use App\Dietitian;
// use App\User;
use App\Photo;

class AdminDietitiansController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
     public function __construct()
    {
        $this->middleware(['auth:admin', 'admin', 'superAdmin']);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->travelLogging('go to dietitians index', auth()->user(), 'superAdmin');
        $dietitians = Dietitian::orderBy('id', 'DESC')
            ->paginate(5);
        return \view('admin.dietitian.index', \compact('dietitians'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = ['0' => 'Dietitian', '1' => 'Doctor', '2'=> 'Pharmacist'];
        $cities = City::all()->pluck('city','id');
        $this->travelLogging('go to dietitians create', auth()->user(), 'superAdmin');
        return view('admin.dietitian.create', compact('types', 'cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|unique:dietitians|max:255',
            'city_id' => 'required',
            'address' => 'required',
            'type' => 'required',
            'reference_code' => 'required|max:15|unique:dietitians',
            'photo_id' => 'nullable|image|max:2500',
            'phone' => 'required|max:15|min:11',
            'password' => 'required|min:6',
            'password_confirmation' => 'required_with:password|same:password|min:6'
        ]);

        // Get all Inputs From Reqest 
        $input = $request->except('photo_id');
        // bcrypt Password to store it in databasde
        $input['password'] = bcrypt($request->password);

        $dietitian = Dietitian::create($input);

        if( $file = $request->file('photo_id') ) {

            $name = time() . $input['email'] . '.' . $file->getClientOriginalExtension();

            $file->move('img/private', $name);

            $photo = new Photo();
            $photo->path = $name;
            $dietitian->photo()->save($photo);
            }

        Alert::alert('Dietitian Created', 'The dietitian Has Been Created', 'success');
        $this->logging('new dietitian created ',null, $dietitian->toArray(), auth()->user(), $dietitian, 'superAdmin');
        return redirect()->route('admin.dietitians.index');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dietitian = Dietitian::findOrFail($id);
        $kits = Kit::whereNotNull('kit_code')->WhereNull('dietitian_id')->orderBy('id', 'DESC')->pluck('kit_code', 'id');
        $this->travelLogging('go to dietitians show', auth()->user(), 'superAdmin');
        return view('admin.dietitian.show', compact('dietitian', 'kits'));
    }

    public function kitLink(Request $request, $id)
    {
        $this->validate($request, [
            'kit' => 'required'
        ]);
        $dietitian = Dietitian::findOrFail($id);
        $kit = Kit::findOrFail($request->kit);
        $kit->dietitian_id = $dietitian->id;
        $kit->save();
        Alert::alert('Done :)', 'kit with kit code = ' . $kit->kit_code . ' Has Been link to dietitian - ' . $dietitian->email, 'success')->autoclose(10000);
        $this->logging('kit with  code = ' . $kit->kit_code . ' Has Been link to dietitian - dietitian email: ' . $kit->dietitian->email . ' -', null, null, auth()->user(), $kit,  'superAdmin');
        return \redirect()->back();
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dietitian = Dietitian::findOrFail($id);
        $types = ['0' => 'Dietitian', '1' => 'Doctor', '2'=> 'Pharmacist'];
        $cities = City::all()->pluck('city','id');
        $this->travelLogging('go to dietitians edit', auth()->user(), 'superAdmin');
        return view('admin.dietitian.edit', compact('dietitian', 'types', 'cities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'unique:dietitians,email,'.$id,
            'city_id' => 'required',
            'address' => 'required',
            'type' => 'required',
            'reference_code' => 'unique:dietitians,reference_code,'.$id,
            'photo_id' => 'nullable|image|max:2500',
            'phone' => 'required|max:15|min:11',
            'password_confirmation' => 'required_with:password|same:password'
        ]);

        $dietitian = Dietitian::findOrFail($id);
        $input = $request->except('photo_id');
        
        if(trim($request->password) == ''){

            $input = $request->except('password', 'photo_id');

        }else{

            $input = $request->all();
            $input['password'] = bcrypt($request->password);

        }

        if($file = $request->file('photo_id')){

            if($dietitian->photo){

                if(file_exists('img/private/' . $dietitian->photo->path))
                    unlink('img/private/' . $dietitian->photo->path);
                $dietitian->photo->delete();
            }
            $name = time() . $input['email'] . '.' . $file->getClientOriginalExtension();

            $file->move('img/private', $name);

            $photo = new Photo();
            $photo->path = $name;
            $dietitian->photo()
                ->save($photo);

        }
        $dietitian->update($input);
        Alert::alert('Dietitian Updated', 'The dietitian Has Been Updated', 'success');
        $this->logging('Dietitian updated', $request->except('password_confirmation', 'password', 'photo', '_method', '_token'), $dietitian->toArray(), auth()->user(), $dietitian, 'superAdmin');
        return redirect()->route('admin.dietitians.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(isset($_GET['soft'])){
            $dietitian = Dietitian::withTrashed()
                        ->where('id', $id)
                        ->first();
            if($dietitian->photo){

                if(file_exists('img/private/' . $dietitian->photo->path))
                    unlink('img/private/' . $dietitian->photo->path);
                $dietitian->photo
                    ->delete();
            }
            $dietitian->forceDelete();
            // Session::flash('deleted', 'The user Has Been Deleted Permanently');
            Alert::alert('dietitian Deleted', 'The dietitian Has Been Deleted Permanently', 'error');
            $this->logging('dietitian deleted', null, $dietitian->toArray(), auth()->user(), $dietitian, 'superAdmin');
            return redirect()->route('admin.dietitians.trash');
        }
        $dietitian = Dietitian::findOrFail($id);
        $dietitian->delete();
        Alert::alert('dietitian trashed', 'The dietitian Has Been Moved To trash', 'warning');
        $this->logging('dietitian trashed', null, $dietitian->toArray(), auth()->user(), $dietitian, 'superAdmin');
        return redirect()->route('admin.dietitians.index');

    }

    public function trash()
    {    
        $dietitians = Dietitian::onlyTrashed()
                                ->orderBy('id', 'DESC')
                                ->paginate(5);
        $this->travelLogging('go to dietitians trash', auth()->user(), 'superAdmin');
        return view('admin.dietitian.restore', compact('dietitians'));   
    }
    
    public function restore($id)
    {
        $dietitians = Dietitian::withTrashed()->find($id);
        $dietitians->restore();
        Alert::alert('Dietitians Restored', 'The Dietitian Has Been Restored', 'info');
        $this->logging('Dietitians restored', null, $dietitians->toArray(), auth()->user(), $dietitians, 'superAdmin');
        return redirect()->route('admin.dietitians.index');
    }
}
