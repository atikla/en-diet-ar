<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Kit;
use Alert;
use Auth;

class UserRegisterKitController extends Controller
{

    /**
    * Create a new controller instance.
    *
    * @return void
    */
     public function __construct()
    {
        $this->middleware('guest');
    }

    /**
    * Show the application registration Kit form.
    *
    * @return \Illuminate\Http\Response
    */
    public function showRegisterKitForm()
    {
        return \view('auth.registerKit');
    }

    /**
    * Handle a registration kit request for the application.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */

    public function RegisterKit(Request $request)
    {
        $this->validate($request, [
            'kit_code' => 'required|min:1|max:20',
        ]);
        $kit = Kit::where(\DB::raw('BINARY `kit_code`'), $request->kit_code)
                    ->where('registered', '=', '0')
                    ->first();
        if($kit == NULL){

            Alert::alert(__('app.kit_hata'), __('app.kit_hata_des'), 'error');
            return \redirect()->back()->withInput();

        }else{
            session()->put('kit_code', $kit->kit_code);
            return redirect()->route('register-kit-user.form');
        }
    }

    /**
    * Show the application registration user form after kit .
    *
    * @return \Illuminate\Http\Response
    */
    public function showRegisterKitWithUserForm()
    {
        return \view('auth.registerKitUser');
    }

    /**
    * Handle a registration user request for the application.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function registerKitWithUser(Request $request)
    {
        if(!$request->kit_code || $request->kit_code != session()->get('kit_code')){
            Alert::alert(__('app.odeme_hata'), __('app.genel_hata'), 'error');
            return redirect()->back();  
        }

        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|unique:users|max:255',
            'phone' => 'required|max:15|min:11',
            'address' => 'required',
            'kit_code' => 'required',
            'password' => 'required|min:6',
            'password_confirmation' => 'required_with:password|same:password|min:6',
            'Ön_Bilgilendirme_Formu' => 'required',
            'Aydınlatma_Formu' => 'required',
            'Üyelik-Sozleşmesi' => 'required',
            'Gizlilik-Politikası' => 'required',
            
        ]);
        
        $input = $request->except('kit_code');
        // bcrypt Password to store it in databasde
        
        $kit = Kit::where('kit_code', '=', session()->get('kit_code'))
                    ->where('registered', '=', '0')
                    ->first();
        if($kit){
            $input['password'] = bcrypt($request->password);
            $user = User::create($input);
            $kit->update(['user_id' => $user->id, 'registered' => 1, 'registered_at' => now()]);
            Auth::login($user);
            $user->sendEmailVerificationNotification();
            return redirect()->route('user.index');
        }else{
            session()->forget('kit_code');
            Alert::alert(__('app.odeme_hata'), __('app.genel_hata'), 'error');
            return redirect()->route('register-kit.form'); 
        }
    }

    /**
    * Handle a registration user request for the application.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function LoginUserAfterKitCodeMatch(Request $request)
    {   
        $this->validate($request, [
            'email_' => 'required|max:255|email',
            'password_' => 'required|min:6|string',
        ]);
        $user = User::where('email', '=', $request->email_)
                    ->first();
        if(Auth::guard('web')->check()){ # if auth user exist
            Auth::logout();
        }
        if ($user) { # if user exist
            if (Hash::check($request->password_, $user->password)) { # if password correct
                $kit = Kit::where('kit_code', '=', session()->get('kit_code'))
                            ->where('registered', '=', '0')
                            ->first();
                if ($kit) { #if kit available
                    $kit->update(['user_id' => $user->id, 'registered' => 1]);
                    Auth::login($user);
                    return redirect()->route('user.index');
                } else { #if kit not available
                    session()->forget('kit_code');
                    Alert::alert(__('app.odeme_hata'), __('app.genel_hata'), 'error');
                    return redirect()->route('register-kit.form'); 
                }
            } else {  # if password not correct
                Alert::alert(__('app.odeme_hata'), __('auth.failed'), 'error');
                return redirect()->back(); 
            }

        } else { # if user not exist
            Alert::alert(__('app.odeme_hata'), __('auth.failed'), 'error');
            return redirect()->back(); 
        }
    }
}
