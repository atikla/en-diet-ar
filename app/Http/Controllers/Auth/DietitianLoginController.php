<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Route;

class DietitianLoginController extends Controller
{
    public function __construct()
    {
      $this->middleware('guest:dietitian', ['except' => ['logout']]);
    }
    
    public function showLoginForm()
    {
      return view('dietitian.auth.login');
    }
    
    public function login(Request $request)
    {
      // Validate the form data
      $this->validate($request, [
        'email'   => 'required|email',
        'password' => 'required'
      ]);
      
      // Attempt to log the user in
      if (Auth::guard('dietitian')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
        $this->loginlogout('dietitianlogin', 'Dietitian with email: '. $request->email . ' login successful');
        // if successful, then redirect to their intended location
        return redirect()->intended(route('dietitian.index'));
      } 
      // if unsuccessful, then redirect back to the login with the form data
      return redirect()->back()->withInput($request->only('email', 'remember'))->withErrors(['password' => 'Password Or Email Uncurrecct' ]);
    }
    
    public function logout()
    {
        Auth::guard('dietitian')->logout();
        return redirect()->route('dietitian.index');
    }
}
