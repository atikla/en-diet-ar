<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Survey;
use App\SurveySection;
use App\SurveySectionLang;
use App\Question;
use App\QuestionLang;
use App\Country;
use Alert;

class AdminQuestionController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
     public function __construct()
    {
        $this->middleware(['auth:admin', 'admin', 'superAdmin']);
    }

    /**
     * Display a listing of the resource.
     * @param  int  $surveyId
     * @param  int  $sectionId
     * @return \Illuminate\Http\Response
     */
    public function index($surveyId, $sectionId)
    {
        $survey = Survey::findOrFail($surveyId);
        $section = $survey->surveySection
            ->where('id', '=', $sectionId)
            ->first();
        if ( $section ) {
            
            $questions = $section->question;
            
            if ( sizeof($questions) != 0  ) {
                $this->travelLogging('go to questions index', auth()->user(), 'superAdmin');
                return \view('admin.survey.section.question.index', compact('questions', 'surveyId', 'sectionId'));
            } else {
            
                Alert::alert('info', 'this Section Dont Have any Question Yet you Will Create One', 'info')
                    ->autoclose(10000);    
                return redirect()->route('admin.questions.create', [$surveyId,  $sectionId]);
            }
        }
        return abort(404);
    }

    /**
     * Show the form for creating a new resource.
     * @param  int  $surveyId
     * @param  int  $sectionId
     * @return \Illuminate\Http\Response
     */
    public function create($surveyId, $sectionId)
    {
        $survey = Survey::findOrFail($surveyId);
        $selectInput = array(
            'text' => 'text',
            'number' => 'number',
            'textarea' => 'textarea',
            'checkbox' => 'checkbox',
            'radio' => 'radio',
            'date' => 'date' 
        );
        if( $survey->surveySection->contains('id', $sectionId ) ) {
            $this->travelLogging('go to question create', auth()->user(), 'superAdmin');
            return \view('admin.survey.section.question.create', compact('surveyId', 'sectionId', 'selectInput'));
        }
        return abort(404);
    }
    /**
     * Show the form for creating a new resource.
     * @param  int  $surveyId
     * @param  int  $sectionId
     * @param  int  $questionId
     * @return \Illuminate\Http\Response
     */
     public function createLang($surveyId, $sectionId, $questionId)
    {
        $survey = Survey::findOrFail($surveyId);
        $section = $survey->surveySection->where('id', '=', $sectionId)->first();
        $countres = Country::pluck('name','code');
        $question = $section->question()->whereId($questionId)->firstOrFail();
        // return dd();
        if( $section && $question->defaultLanguage) {
            $question = $section->question->where('id', '=', $questionId)->first();
            $this->travelLogging('go to question createLang', auth()->user(), 'superAdmin');
            return \view('admin.survey.section.question.createLang', compact('surveyId', 'sectionId', 'questionId', 'question', 'countres'));
        }
        return abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $surveyId
     * @param  int  $sectionId
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $surveyId, $sectionId)
    {
        //
        $this->validate($request, [
            'title' => 'required|max:300',
            'question_type' => 'required',
            'question_order' => 'required|min:1|max:100|numeric'
        ]);
        $temp = $request->option_name;
        if($temp){
            $option_name = array();
            $data = $request->option_name['choices'];
            foreach ($data as $key => $value) {
                $is_if = NULL ;
                if ( isset($temp['if'][$key]) )
                    $is_if = $temp['if'][$key];
                array_push($option_name, array(
                    'title' => $value,
                    'is_if' => $is_if
                ));
            }
        }else{
            $option_name = NULL;
        }
        $survey = Survey::findOrFail($surveyId);
        $section = $survey->surveySection
            ->where('id', '=', $sectionId)
            ->first();
           
        if ( $section ){
            $question = $section->question()
                ->create([
                    'question_type'  => $request->question_type,
                    'question_order' => $request->question_order
                ]);
            
            $question->questionLang()
                ->create([
                    'title' => $request->title,
                    'option_name' => $option_name
                ]); 
            Alert::alert('Question Created', 'The Question Has Been Created with default lang (tr)', 'success')
            ->autoclose(10000);
            $this->logging('new question created',null, $question->toArray(), auth()->user(), $question, 'superAdmin');
            return redirect()->route('admin.questions.index', [$surveyId,  $sectionId]);
        }else {
            return abort(404);
        }
        return abort(404);
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $surveyId
     * @param  int  $sectionId
     * @param  int  $questionId
     * @return \Illuminate\Http\Response
     */
     public function storeLang(Request $request, $surveyId, $sectionId, $questionId)
    {
        //
        
        $this->validate($request, [
            'q_type' => 'required|max:300',
            'title' => 'required',
            'lang' => ['required', Rule::notIn(['tr'])],
            'type' => 'required',
            // 'question_order' => 'required|min:1|max:100|numeric'
        ]);
        $survey = Survey::findOrFail($surveyId);
        $section = $survey->surveySection()->whereId($sectionId)->firstOrFail();
        $question = $section->question()->whereId($questionId)->firstOrFail();
        $defaultLanguage = $question->defaultLanguage;
        $lang = $question->questionLang()->whereLang($request->lang)->first();
        $temp = null;
        if ( $defaultLanguage ) {
            if ( $request->type == 0 ) {
                if ( $lang ) {
                    $temp = $lang->toArray();
                    $lang->title = $request->title;
                    $lang->save();
                } else {
                    $lang = $question->questionLang()
                        ->create(['title' => $request->title, 'lang' => $request->lang ]);
                }
                Alert::alert('Question Lang Created', 'The Question Lang Has Been Created with lang ' . $request->lang , 'success');
                $this->logging('new question lang created - ' . $request->lang, $temp, $lang->toArray(), auth()->user(), $question, 'superAdmin');
                return  redirect()->route('admin.questions.index', [ $surveyId, $sectionId, $questionId ]);

            } else {
                if ( $defaultLanguage->option_name && sizeof($request->option_name['choices']) ==  sizeof($defaultLanguage->option_name) ) {
                    $option_name = [];
                    $data = $request->option_name['choices'];
                    foreach ($data as $key => $value) {
                        $is_if = NULL ;
                        if ( isset($request->option_name['if'][$key]) )
                            $is_if = $request->option_name['if'][$key];
                        array_push($option_name, array(
                            'title' => $value,
                            'is_if' => $is_if
                        ));
                    }
                    if ( $lang ) {
                        $temp = $lang->toArray();
                        $lang->title = $request->title;
                        $lang->option_name = $option_name;
                        $lang->save();
                    } else {
                       $lang =  $question->questionLang()
                            ->create(['title' => $request->title, 'lang' => $request->lang, 'option_name' => $option_name]);
                    }
                    Alert::alert('Question Lang Created', 'The Question Lang Has Been Created with lang ' . $request->lang , 'success');
                    $this->logging('new question lang created - ' . $request->lang, $temp, $lang, auth()->user(), $question, 'superAdmin');
                    return  redirect()->route('admin.questions.index', [ $surveyId, $sectionId, $questionId ]);
                } else {
                    Alert::alert('Error Occurred', 'some error occurred please try again' . $request->lang , 'success');
                    return  redirect()->route('admin.questions.index', [ $surveyId, $sectionId, $questionId ]);
                }
            }
        } else {
            return abort(404);
        }  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $surveyId
     * @param  int  $sectionId
     * @param  int  $questionId
     * @return \Illuminate\Http\Response
     */
    public function show($surveyId, $sectionId, $questionId)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $surveyId
     * @param  int  $sectionId
     * @param  int  $questionId
     * @return \Illuminate\Http\Response
     */
    public function edit($surveyId, $sectionId, $questionId)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $surveyId
     * @param  int  $sectionId
     * @param  int  $questionId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $surveyId, $sectionId, $questionId)
    {
        $this->validate($request, [
            '*question_order' => 'required|min:1|max:100|numeric',
            '*question_type' => 'required'
        ]);
        $question = Question::findOrFail($questionId);
        $temp = $question->toArray();
        $question->question_order = $request['update_' . $question->id . '_question_order'];
        $question->question_type = $request['update_' . $question->id . '_question_type'];
        $question->save();
        Alert::alert('Question Updated', 'The Question Has Been Updated', 'success');
        $this->logging('question updated', $temp, $question->toArray(), auth()->user(), $question, 'superAdmin');
        return redirect()->route('admin.questions.index', [$surveyId,  $sectionId]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $surveyId
     * @param  int  $sectionId
     * @param  int  $questionId
     * @return \Illuminate\Http\Response
     */
    public function destroy($surveyId, $sectionId, $questionId)
    {
        //
        $question = Question::findOrFail($questionId);
        $question->delete();
        Alert::alert('Deleted', 'The Question Has Been Deleted', 'warning');
        $this->logging('question deleted', null, $question->toArray(), auth()->user(), $question, 'superAdmin');
        return redirect()->route('admin.questions.index', [$surveyId,  $sectionId]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $surveyId, $sectionId, $sectionLangId
     * @return \Illuminate\Http\Response
     */
    public function destroyLang($surveyId, $sectionId, $questionId, $langid)
    {
        $survey = Survey::findOrFail($surveyId);
        $section = $survey->surveySection()->whereId($sectionId)->firstOrFail();
        $question = $section->question()->whereId($questionId)->firstOrFail();
        $questionLang = $question->questionLang()->whereId($langid)->firstOrFail();
        if($questionLang->lang == 'tr'){
            Alert::alert('Error', 'You Can Not Delete The default Lang', 'warning');
            return redirect()->back();
        }
        $questionLang->delete();
        $this->logging('question Lang deleted - ' . $questionLang->lang, null, $questionLang->toArray(), auth()->user(), $questionLang, 'superAdmin');
        Alert::alert('question Lang Deleted', 'The Section Lang Has Been Deleted ', 'success');
        return redirect()->back();
        
    }
}
