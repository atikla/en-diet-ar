<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Survey;
use App\Country;
use App\Admin;
use Auth;
use Alert;

class AdminSurveyController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
     public function __construct()
    {
        $this->middleware(['auth:admin', 'admin', 'superAdmin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $surveys = Survey::where('deleted_at', null)
                        ->orderBy('id', 'DESC')
                        ->paginate(2);
        $surveys->filter(function ($survey){
            $survey->surveyLang;
            return $survey;
        });
        $countres = Country::pluck('name','code');
        return \view('admin.survey.index', compact('surveys', 'countres'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countres = Country::pluck('name','code');
        return \view('admin.survey.create', \compact('countres'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'description' => 'required|max:1000',
        ]);

        $user = Auth::user();
        $survey = $user->survey()->create();
        $survey->surveyLang()->create([
            'title' => $request->title,
            'description' => $request->description

        ]);

        Alert::alert('surveys Created', 'The surveys Has Been Created and add title, description for default lang (tr)', 'success')
            ->autoclose(10000);
        return redirect()->route('admin.surveys.index');


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function storeLang(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'description' => 'required|max:1000',
            'lang' => 'required',
        ]);
        $survey = Survey::findOrFail($id);

        if( $survey->surveyLang->contains('lang', $request->lang) ){
            $lang = $survey->surveyLang->where('lang', '=', $request->lang)->first();
            $lang->title = $request->title;
            $lang->description = $request->description;
            $lang->save();
            Alert::alert('survey updated', 'The surveys Has Been Updated for  lang ( ' . $lang->language .' )', 'success')
                ->autoclose(10000);
            return redirect()->route('admin.surveys.index');
        }else{
            $lang = $survey->surveyLang()->create(
                [
                    'title' => $request->title,
                    'description' => $request->description,
                    'lang' => $request->lang,
                ]
            );
            Alert::alert('survey Created', 'The surveys Has Been Created for  lang ( ' . $lang->language .' )', 'success')
                ->autoclose(10000);
            return redirect()->route('admin.surveys.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function show($id)
    // {
    //     //
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function edit($id)
    // {
    //     //
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $survey = Survey::findOrFail($request->id);
        // $survey->update([
        //     'publish' => $request->publish
        // ]);
        $survey->publish = $request->publish;
        $survey->save();
        Alert::success('Done :)', 'survey Status Has Been Updated');
        return 200;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $surveyId
     * @param  int  $surveyLangId
     * @return \Illuminate\Http\Response
     */
     public function updateLang(Request $request, $surveyId, $surveyLangId)
    {
        $this->validate($request, [
            '*title' => 'required|max:255',
            '*description' => 'required|max:1000',
        ]);
        
        $survey = Survey::findOrFail($surveyId);
        $surveyLang = $survey->surveyLang()
            ->where('id', '=', $surveyLangId)
            ->first();
        if ( $surveyLang ) {

            $surveyLang->title = $request['id_' . $surveyLangId . 'title'];
            $surveyLang->description = $request['id_' . $surveyLangId . 'description'];
            $surveyLang->save();
            Alert::alert('survey updated', 'The surveys Has Been Updated for  lang ( ' . $surveyLang->language .' )', 'success')
                ->autoclose(10000);
            return redirect()->route('admin.surveys.index');
        } else {
            Alert::alert('survey Not updated', 'Error Occurred', 'error')
                ->autoclose(10000);
            return redirect()->route('admin.surveys.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $survey = Survey::findOrFail($id);
        $survey->delete();
        Alert::alert('Deleted', 'The surveys Has Been Deleted', 'warning');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $surveyId
     * @param  int  $surveyLangId
     * @return \Illuminate\Http\Response
     */
     public function destroyLang(Request $request, $surveyId, $surveyLangId)
    {
        $survey = Survey::findOrFail($surveyId);
        $surveyLang = $survey->surveyLang()
            ->where('id', '=', $surveyLangId)
            ->first();
        if ( $surveyLang ) {
            if($surveyLang->lang == 'tr'){
                Alert::alert('Error', 'You Can Not Delete The default Lang', 'warning');
                return redirect()->back();
            }
            $surveyLang->delete();
            Alert::alert('Survey Lang Deleted', 'The Survey Lang Has Been Deleted ', 'success');
            return redirect()->back();
        } else {
            Alert::alert('Error', 'Error Occurred', 'error');
            return redirect()->back();
        }
    }
}
