<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Newsletter;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function logging($log, $old, $attributes, $causer, $subject, $logName = 'default')
    {
        $lang = request()->header('Content-Language');
        if ( $lang )
            $lang = '( lang = ' . request()->header('Content-Language') . ' )';
        else
            $lang = '';
        

        activity($logName)
            ->causedBy($causer)
            ->performedOn($subject)
            ->withProperties(['ip' => request()->ip(),'attributes' => $attributes, 'old' => $old])
            ->log($log . ' ' . $lang );
    }

    public function travelLogging($log, $causer, $logName = 'default')
    {
        // activity($logName)
        //     ->causedBy($causer)
        //     ->withProperties(['ip' => request()->ip()])
        //     ->log($log);
    }

    public function loginlogout($logName, $log)
    {
        activity($logName)
            ->withProperties(['ip' => request()->ip()])
            ->log($log);
    }

    public function RegisterNewsLetter($email, $name = null)
    {
        // Newsletter::subscribeOrUpdate($email, ['FNAME'=>$name]);
    }
}
