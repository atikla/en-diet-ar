<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Error;
use Alert;

class AdminErrorsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth:admin', 'admin', 'superAdmin']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paymentErrors = Error::orderBy('id', 'DESC')->paginate(5);
        return view('admin.errors.index', compact('paymentErrors'));
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $this->validate($request, [
            'search' => 'required|max:50',
        ]);
        $search = $request->search;
        $paymentErrors = Error::where('model', 'LIKE', '%' . $search . '%')
            ->orWhere('json_string', 'LIKE', '%' . $search . '%')
            ->get();
        Alert::alert('Search Result', $paymentErrors->count() . ' items Found', 'info');
        return view('admin.errors.search', compact('paymentErrors'));
    }

}
