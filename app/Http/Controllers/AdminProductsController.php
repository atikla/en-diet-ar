<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Requests;
use Illuminate\Support\Str;
use Auth;
use  Alert;
// Our Models
use App\Product;
use App\Admin;
use App\Photo;

class AdminProductsController extends Controller
{


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth:admin', 'admin', 'superAdmin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::where('deleted_at', null)
            ->orderBy('id', 'DESC')
            ->paginate(5);
        $this->travelLogging('go to products index', auth()->user(), 'superAdmin');
        return view('admin.product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->travelLogging('go to product create', auth()->user(), 'superAdmin');
        return \view('admin.product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        // make a slug like first-second befor validation
        $request['slug'] = Str::slug($request->get('slug'), '-');

        $this->validate($request, [
            'name' => 'required|max:255',
            'slug' => 'required|unique:products|max:100',
            'price' => 'required|max:15|min:1',
            'discount_price' => 'nullable|max:15|min:1',
            'photo_id' => 'required|image',
            'stok' => 'required|max:5|min:1',
            'description' => 'required|min:30|max:2000',
            'number_of_kit' => 'required|min:1|max:20|numeric'
        ]);

        $input = $request->except('photo_id');
        $admin = auth('admin')->user();
        $product = $admin->products()
                        ->create($input);
        if($file = $request->file('photo_id')){

            $name = $input['slug'] . '.' . $file->getClientOriginalExtension();

            $file->move('img/private', $name);

            $photo = new Photo();
            $photo->path = $name;
            $product->photo()
                    ->save($photo);
        }

        // Session::flash('created', 'The product Has Been Created');
        Alert::alert('Product Created', 'The product Has Been Created', 'success');
        $this->logging('new Product created',null, $product->toArray(), auth()->user(), $product, 'superAdmin');
        return redirect()->route('admin.products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $slug)
    {
        //
        $product = Product::where('id', '=', $id)
            ->where('slug', '=', $slug)
            ->first();
        if ( !empty($product) ) {
            $this->travelLogging('go to product show', auth()->user(), 'superAdmin');
            return \view('admin.product.show', \compact('product'));
        } else {
            return \abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);
        $this->travelLogging('go to product edit', auth()->user(), 'superAdmin');
        return \view('admin.product.edit', \compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // make a slug like first-second befor validation
        $request['slug'] = Str::slug($request->get('slug'), '-');

        $this->validate($request, [
            'name' => 'required|max:255',
            'slug' => 'required|unique:products,slug, '. $id .'|max:100',
            'price' => 'required|max:15|min:1',
            'discount_price' => 'nullable|max:15|min:1',
            'stok' => 'required|max:5|min:1',
            'description' => 'required|min:30|max:2000',
            'number_of_kit' => 'required|min:1|max:20|numeric'
        ]);
        // return $request->all();
        $input = $request->except('photo_id');
        $admin = auth('admin')->user();
        $product = Product::findOrFail($id);
        $product->update($input);
        if($file = $request->file('photo_id')){

            if($product->photo){
                if(file_exists('img/private/' . $product->photo->path))
                    unlink('img/private/' . $product->photo->path);
                $product->photo->delete();
            }
            $name = $input['slug'] . '.' . $file->getClientOriginalExtension();

            $file->move('img/private', $name);

            $photo = new Photo();
            $photo->path = $name;
            $product->photo()
                    ->save($photo);
        }
        // Session::flash('updated', 'The Product Has Been Updated');
        Alert::alert('Product Updated', 'The product Has Been Created', 'success');
        $this->logging('Product updated', $request->except('_method', '_token'), $product->toArray(), auth()->user(), $product, 'superAdmin');
        return redirect()->route('admin.products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(isset($_GET['soft'])){
            $product = Product::withTrashed()
                            ->where('id', $id)
                            ->first();
            if($product->photo){

                if(file_exists('img/private/' . $product->photo->path))
                    unlink('img/private/' . $product->photo->path);
                $product->photo
                        ->delete();
            }
            $product->forceDelete();
            // Session::flash('deleted', 'The Product Has Been Deleted Permanently');
            $this->logging('product deleted', null, $product->toArray(), auth()->user(), $product, 'superAdmin');
            Alert::alert('Product Deleted', 'The Product Has Been Deleted Permanently', 'error');
            return redirect()->route('admin.products.trash');
        }
        $product = Product::findOrFail($id);
        $product->delete();
        $this->logging('product trashed', null, $product->toArray(), auth()->user(), $product, 'superAdmin');
        // Session::flash('deleted_soft', 'The Product Has Been Moved To trash');
        Alert::alert('Product trashed', 'The Product Has Been Moved To trash', 'warning');
        return redirect()->route('admin.products.index');
    }

    public function trash()
    {    
        $products = Product::onlyTrashed()
            ->orderBy('id', 'DESC')
            ->paginate(5);
        $this->travelLogging('go to products trash', auth()->user(), 'superAdmin');
        return view('admin.product.restore', compact('products'));   
    }
    
    public function restore($id)
    {
        $product = Product::withTrashed()->find($id);
        $product->restore();
        // Session::flash('restored', 'The Product Has Been Restored');
        Alert::alert('Product Restored', 'The Product Has Been Restored', 'info');
        $this->logging('product restored', null, $product->toArray(), auth()->user(), $product, 'superAdmin');
        return redirect()->route('admin.products.index');
        // return redirect('/admin/users');
    }
    /**
     * search in resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $this->validate($request, [
            'search' => 'required|min:3|max:50',
        ]);
        $search = $request->search;
        $products = Product::where('name', 'LIKE', '%' . $search . '%')
            ->orWhere('description', 'LIKE', '%' . $search . '%')
            ->orWhere('slug', 'LIKE', '%' . $search . '%')
            ->get();
        Alert::alert('Search Result', $products->count() . ' items Found', 'info');
        $this->logging('products searched for ' . $request->search , null, $request->search, auth()->user(), new Product, 'superAdmin');
        return view('admin.product.search', compact('products'));
    }
}
