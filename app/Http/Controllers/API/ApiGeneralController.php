<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Kit;

class ApiGeneralController extends Controller
{
    //
    public function getApi(Request $request)
    {
        return response()->json( [ 'status' => 400, 'success' => false, 'Error' => 'Bad Request' ], 400 );
    }

    public function testApi(Request $request)
    {
        return $request->all(); 
        return response()->json( [ '' => 400, 'success' => false, 'Error' => 'Bad Request' ], 400 );
    }
    public function testKit(Request $request)
    {
        $kit = Kit::where('kit_code', '=', 'AA0056')->first();

        if ( $kit == null )
            return response()->json( [ 'code' => 400, 'success' => false, 'Error' => 'Bad Request' ], 400 );

        $data = [
            'kit_code' => $kit->kit_code,
            'foods' => $kit->foods,
            'closeProfle' => $kit->closeProfle,
            'ageRange' => $kit->ageRange,
            'bacteria' => $kit->bacteria,
            'allScores' => $kit->allScores,
            'taksonomik' => $kit->taksonomik
        ];
        return response()->json( 
            [ 'message' => 'ok', 'code' => 200, 'data' => $data
        ], 200 );
    }
    public function testKit_aa(Request $request, $kitCode)
    {
        if ($request->enbiosis != '123321')
            return response()->json( [ 'code' => 400, 'success' => false, 'Error' => 'Bad Request' ], 400 );

        $kit = Kit::whereKitCode($kitCode)->first();

        if ( $kit == null  || !$kit->food || !$kit->microbiome)
            return response()->json( [ 'code' => 400, 'success' => false, 'Error' => 'Bad Request' ], 400 );

        return response()->json( 
            ['message' => 'ok', 'code' => 200, 'data' => $kit->pdf], 200 );
    }
    
}
