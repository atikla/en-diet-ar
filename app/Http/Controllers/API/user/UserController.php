<?php

namespace App\Http\Controllers\API\user;

use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use App\Survey;
use App\Kit;
use App\Answer;
use App\Resource;
use App\Mail\SendPdf;
use App\Mail\EmailVerification;
use Mail;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth:api']);
    }
    
    /**
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function sendEmailVerification(Request $request)
    {
        $user = $request->user();
        if ( $user->email_verified_at ) {
            return response()->json( [ 'message' => __('api.already_verified_email'), 'code' => 200 ], 200 );
        }
        $user->expires_at = strtotime('2 hour');
        $user->verify_code =  rand(10000, 99999);
        $user->save();
        try {
            Mail::to($user->email)->locale(app()->getLocale())->send( new EmailVerification( $user ) );
            $this->logging('Verification code sent to user email: ' . $user->email ,null, $user->toArray(), auth()->user(), $user, 'apiUser');
            return response()->json( [ 'message' => __('api.verification_email_send'),
                'code' => 200 ], 200 );
        } catch (\Throwable $th) {
            return response()->json([ 'errors' =>[ __('api.genel_hata') ], 'code' => 400 ], 400 );
        }
    }

    /**
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function checkForVerificationCode(Request $request)
    {
        $rules = array(  
            'verify_code' => 'required|max:6',
        );
        //Validation
        $data = $request->all();
        $validator = Validator::make($data, $rules);

        if ( $validator->fails() ) {
            return response()->json(['errors' => $validator->errors(), 'code' => 400], 400);  
        }
        $user = $request->user();
        if ( $request->verify_code == $user->verify_code ) {
            // dd($user->expires_at->toDateTimeString());
            if ( $user->expires_at && strtotime( $user->expires_at->toDateTimeString() ) >= time() ) {
                $user->email_verified_at = now();
                $user->verify_code = null;
                $user->expires_at = null;
                $user->save();
                $user->kits;
                $this->logging('The email address has been verified, email: ' . $user->email ,null, $user->toArray(), auth()->user(), $user, 'apiUser');
                return response()->json( ['message' => __('api.verification_email_done'), 'code' => 200, 'data' => $user], 200 );
            } else {
                return response()->json(['errors' => ['verify_code' => __('api.verification_email_expired')], 'code' => 400], 400);
            }
        } else {
            return response()->json(['errors' => ['verify_code' => __('api.verification_email_error')], 'code' => 400], 400);
        }
    }

    /**
     * get Auth User.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $user->kits;
        return response()->json( ['message' => 'ok', 'code' => 200, 'data' => $user], 200 );
    }

    /**
     * get Survey for Kit.
     * @param  int  $kitCode
     * @return \Illuminate\Http\Response
     */
    public function getSurvey(Request $request, $kitCode)
    {
        $user = $request->user();
        if ( $user->Kits->contains('kit_code', $kitCode) ) {
                $kit =  $user->kits->where('kit_code', $kitCode)->first();
                if ( $kit->answer->count() > 0  ) {
                    $surveys = Survey::where('publish', '=', 1)
                        ->where('id', '=', $kit->ReverseRelationship)
                        ->get();
                    $survey = $surveys->first();
                    $survey->kitid = $kit->id;
                    $data = $survey->Survey;
                    return response()->json( ['message' => 'ok', 'code' => 200, 'data' => $data], 200 );
                } else {
                    $surveys = Survey::where('publish', '=', 1)->get();
                    if ( $surveys->count() > 0 ) {
                        $survey = $surveys->first();
                        $data = $survey->Survey;
                        $this->logging('Get survey for user' . $user->email ,null, $user->toArray(), auth()->user(), $user, 'apiUser');
                        return response()->json(
                            [ 'message' => __('api.survey_information', ['code' => $kitCode]), 'code' => 200, 'data' => $data],
                            200
                        );
                    } else {
                        return response()->json([ 'errors' =>[ __('api.no_survey_desc') ], 'code' => 400 ], 400 );
                    }
                } 
        } else {
            return response()->json( [ 'errors' =>[ __('api.kit_error_desc') ], 'code' => 400 ],  400 );
        }
    }
     /**
     * get Auth User.
     *
     * @return \Illuminate\Http\Response
     */
    public function getKitFoods(Request $request)
    {
        $rules = array(  
            'kit_code' => 'required',
        );
        //Validation
        $data = $request->all();
        $validator = Validator::make($data, $rules); 

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors(), 'code' => 400], 400);  
        }
        $user = $request->user();
        if ( $kit = $user->kits()->where('kit_code', '=', $request->kit_code)->first() ) {

            if ( $kit->food ) {
                $data = $kit->foods;
                $this->logging('Get result - food - kit code: ' . $kit->kit_code ,null, $user->toArray(), auth()->user(), $user, 'apiUser');
                return response()->json( ['message' => 'ok', 'code' => 200, 'data' => $data], 200 );

            } else {

                return response()->json(['errors' => [ 'kit_code' => __('api.no_result_yet') ], 'code' => 400], 400);  
                
            }

        } else {
            return response()->json(['errors' => ['kit_code' => __('api.kit_hata') ], 'code' => 400], 400);  

        }
    }

    /**
     * get Auth User.
     *
     * @return \Illuminate\Http\Response
     */
    public function getKitCloseProfle(Request $request)
    {
        $rules = array(  
            'kit_code' => 'required',
        );
        //Validation
        $data = $request->all();
        $validator = Validator::make($data, $rules); 

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors(), 'code' => 400], 400);  
        }
        $user = $request->user();
        if ( $kit = $user->kits()->where('kit_code', '=', $request->kit_code)->first() ) {
            
            if ( $kit->closeProfle ) {
                $data = $kit->closeProfle;
                $this->logging('Get result - close profle - kit code: ' . $kit->kit_code ,null, $user->toArray(), auth()->user(), $user, 'apiUser');
                return response()->json( ['message' => 'ok', 'code' => 200, 'data' => $data], 200 );
            } else {
                return response()->json(['errors' => [ 'kit_code' => __('api.no_result_yet') ], 'code' => 400], 400);  
            }
        } else {
            return response()->json(['errors' => ['kit_code' => __('api.kit_hata')], 'code' => 400], 400);  
        }
    }

    /**
     * get Auth User.
     *
     * @return \Illuminate\Http\Response
     */
    public function getKitAgeRange(Request $request)
    {
        $rules = array(  
            'kit_code' => 'required',
        );
        //Validation
        $data = $request->all();
        $validator = Validator::make($data, $rules); 

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors(), 'code' => 400], 400);  
        }
        $user = $request->user();
        if ( $kit = $user->kits()->where('kit_code', '=', $request->kit_code)->first() ) {
            
            if ( $kit->ageRange ) {
                $data = $kit->ageRange;
                $this->logging('Get result - age range - kit code: ' . $kit->kit_code ,null, $user->toArray(), auth()->user(), $user, 'apiUser');
                return response()->json( ['message' => 'ok', 'code' => 200, 'data' => $data], 200 );
            } else {
                return response()->json(['errors' => [ 'kit_code' => __('api.no_result_yet') ], 'code' => 400], 400);
            }
        } else {
            return response()->json(['errors' => ['kit_code' => __('api.kit_hata')], 'code' => 400], 400);  
        }
    }

    /**
     * get Auth User.
     *
     * @return \Illuminate\Http\Response
     */
    public function getKitBacteria(Request $request)
    {
        $rules = array(  
            'kit_code' => 'required',
        );
        //Validation
        $data = $request->all();
        $validator = Validator::make($data, $rules); 

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors(), 'code' => 400], 400);  
        }
        $user = $request->user();
        if ( $kit = $user->kits()->where('kit_code', '=', $request->kit_code)->first() ) {
            
            if ( $kit->bacteria ) {
                $data = $kit->bacteria;
                $this->logging('Get result - bacteria - kit code: ' . $kit->kit_code ,null, $user->toArray(), auth()->user(), $user, 'apiUser');
                return response()->json( ['message' => 'ok', 'code' => 200, 'data' => $data], 200 );
            } else {
                return response()->json(['errors' => [ 'kit_code' => __('api.no_result_yet') ], 'code' => 400], 400);  
            }
        } else {
            return response()->json(['errors' => ['kit_code' => __('api.kit_hata')], 'code' => 400], 400);  
        }
    }

    /**
     * get Auth User.
     *
     * @return \Illuminate\Http\Response
     */
    public function getKitAllScores(Request $request, $pdf = null)
    {
        $rules = array(  
            'kit_code' => 'required',
        );
        //Validation
        $data = $request->all();
        $validator = Validator::make($data, $rules); 

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors(), 'code' => 400], 400);  
        }
        $user = $request->user();
        if ( $kit = $user->kits()->where('kit_code', '=', $request->kit_code)->first() ) {
            
            if ( $kit->allScores && $kit->pdfScores ) {
                $data = $kit->allScores;
                if ( $pdf )
                    $data = $kit->pdfScores;
                $this->logging('Get result - all scores - kit code: ' . $kit->kit_code ,null, $user->toArray(), auth()->user(), $user, 'apiUser');
                return response()->json( ['message' => 'ok', 'code' => 200, 'data' => $data], 200 );
            } else {
                return response()->json(['errors' => [ 'kit_code' => __('api.no_result_yet') ], 'code' => 400], 400);  
            }
        } else {
            return response()->json(['errors' => ['kit_code' => __('api.kit_hata')], 'code' => 400], 400);  
        }
    }

       /**
     * get Auth User.
     *
     * @return \Illuminate\Http\Response
     */
    public function getKitTaksonomik(Request $request, $type = null )
    {
        $rules = array(  
            'kit_code' => 'required',
        );
        //Validation
        $data = $request->all();
        $validator = Validator::make($data, $rules); 

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors(), 'code' => 400], 400);  
        }
        $user = $request->user();
        if ( $kit = $user->kits()->where('kit_code', '=', $request->kit_code)->first() ) {
            
            if ( $kit->taksonomik ) {
                $data = $kit->sortedTaksonomik;
                if ( $type )
                    $data = $kit->notSortedTaksonomik;
                $this->logging('Get result - taksonomik - kit code: ' . $kit->kit_code ,null, $user->toArray(), auth()->user(), $user, 'apiUser');
                return response()->json( ['message' => 'ok', 'code' => 200, 'data' => $data], 200 );
            } else {
                return response()->json(['errors' => [ 'kit_code' => __('api.no_result_yet') ], 'code' => 400], 400);  
            }
        } else {
            return response()->json(['errors' => ['kit_code' => __('api.kit_hata')], 'code' => 400], 400);  
        }
    }
      /**
     * get Auth User.
     *
     * @return \Illuminate\Http\Response
     */
    public function getsuggestion(Request $request)
    {
        $rules = array(  
            'kit_code' => 'required',
        );
        //Validation
        $data = $request->all();
        $validator = Validator::make($data, $rules); 

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors(), 'code' => 400], 400);  
        }
        $user = $request->user();
        if ( $kit = $user->kits()->where('kit_code', '=', $request->kit_code)->first() ) {
            
            if ( $kit->apiSuggestion ) {
                $data = $kit->apiSuggestion;
                $this->logging('Get kit suggestion kit code: ' . $kit->kit_code ,null, $user->toArray(), auth()->user(), $user, 'apiUser');
                return response()->json( ['message' => 'ok', 'code' => 200, 'data' => $data], 200 );
            } else {
                return response()->json(['errors' => [ 'kit_code' => __('api.no_kit_suggestion')], 'code' => 400], 400);
            }
        } else {
            return response()->json(['errors' => ['kit_code' => __('api.kit_hata') ], 'code' => 400], 400);  
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeSurvey(Request $request)
    {
        $rules = array(  
            'answers' => 'required',
            'kit_code' => 'required',
            'end' => 'required',
        );
        //Validation
        $data = $request->all();
        $validator = Validator::make($data, $rules); 

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors(), 'code' => 400], 400);  
        }

        $kit = Kit::where('kit_code', '=', $request->kit_code)
            ->where('user_id', '=', $request->user()->id)->first();
        if ( $kit ) {

            foreach ($request->answers as $key => $answer) {
                $kit->answer->where('question_id', '=', $key)->each->delete();

                Answer::create([
                    'question_id'   => $key,
                    'kit_id'        => $kit->id,
                    'answer'        => $answer
                ]);
            }
            if ( $request->end ) {
                $surveyId =  $kit->answer()->withTrashed()->first()->question->surveySection->survey->id;
                $kit->survey_id = $surveyId;
                $kit->survey_filled_at = now();
                $kit->save();
                $this->logging('survey filled at kit code: ' . $kit->kit_code ,null, null, auth()->user(), new Survey, 'apiUser');
            }
            return response()->json([ 'message' => __('Done'), 'code' => 200],);
        } else {
            return response()->json( [ 'errors' =>[ __('api.genel_hata') ], 'code' => 400 ],  400 );
        }
    }

    public function getresources()
    {
        $resources = Resource::all();
        if ( $resources->count() > 0 ) {
            $data = [];
            foreach ($resources as $resource) {
                array_push($data, [
                    'videoUrl' => $resource->embed_video,
                    'imageUrl' => $resource->photo->path,
                    'description' => $resource->description
                ]);
            }
            $this->loginlogout('apiUser', 'get resources');
            return response()->json( ['message' => 'ok', 'code' => 200, 'data' => $data], 200 );
        } else {
            return response()->json(['errors' => ['resource' => __('api.no_resource') ], 'code' => 400], 400);
        }
    }
    public function sendReport(Request $request)
    {
        $rules = array(  
            'foods' => 'required|mimes:pdf',
            'kit_code' => 'required',
            'microbiome' => 'required|mimes:pdf',
            'email' => 'required|email:rfc,dns',
        );

        //Validation
        $data = $request->all();
        $validator = Validator::make($data, $rules); 

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors(), 'code' => 400], 400);  
        }
        if( $microbiome = $request->file('microbiome') && $food = $request->file('foods') ) {

            $microbiome = time() . '-microbiome.' . $request->file('microbiome')->getClientOriginalExtension();
            $food = time() . '-food.' . $request->file('foods')->getClientOriginalExtension();
            $kit_code = $request->kit_code;

            $request->file('microbiome')->move(storage_path('/pdf'), $microbiome);
            $request->file('foods')->move(storage_path('/pdf'), $food);
            $microbiome = storage_path('/pdf/'. $microbiome);
            $food = storage_path('/pdf/'. $food);
            Mail::to($request->email)->locale(app()->getLocale())->send(new SendPdf($food, $microbiome, $kit_code));
            $this->logging('Send reports for kit code: ' . $kit_code . ' to ' . $request->email ,null, null, auth()->user(), new \App\User, 'apiUser');
            return response()->json( ['message' => __('api.send_pdf'),
                'code' => 200], 200 );
        }
        return response()->json(['errors' => ['dosya' => __('api.no_file') ], 'code' => 400], 400);
    }
}