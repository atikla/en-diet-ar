<?php

namespace App\Http\Controllers\API\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\Kit;
use Auth;
use App\User;

class UserRegisterKitController extends Controller
{

    /**
     * Handle a registration kit request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function RegisterKit(Request $request)
    {
        $rules = array(  
            'kit_code' => 'required|min:1|max:20',
        );
        // Validation
        $data = $request->all();
        $validator = Validator::make($data, $rules); 
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors(), 'code' => 400], 400);  
        } 
        $kit = Kit::where(\DB::raw('BINARY `kit_code`'), $request->kit_code)
            ->where('registered', '=', '0')
            ->first();
        if ( $kit == NULL ) {
            $this->loginlogout('apiUser', 'Trying to register kit with this code: '. $request->kit_code .', this code we don\'t have it in our records');
            return response()->json([ 'errors' => [ __('api.kit_erorr_desc') ], 'code' => 401, ], 401 );
        } else {
            $this->loginlogout('apiUser', 'Trying to register kit with this code: '. $request->kit_code);
            return response()->json([ 'message' => __('api.register_successfully'), 'code' =>200, 'data' => [ 'kit' => $kit ] ], 200 );
        }
    }

    /**
     * Handle a registration user request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function validatregisterUser(Request $request)
    {
        
        $rules = array(  
            'name' => 'required|max:255',
            'email' => 'required|unique:users|max:255',
            'phone' => 'required|max:15|min:11',
            'gender' => 'required',
            'date_of_birth' => 'required|date|date_format:Y-m-d',
            'kit_code' => 'required',
            'password' => 'required|min:6',
            'password_confirmation' => 'required_with:password|same:password|min:6',
        );
        // Validation
        $data = $request->all();
        $validator = Validator::make($data, $rules); 
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors(), 'code' => 400], 400);  
        }
        $this->RegisterNewsLetter($request->email, $request->name);
        return response()->json( [ 'message' => 'ok', 'code' => 200 ], 200 );
    }
    /**
     * Handle a registration user request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function registerUser(Request $request)
    {
        
        $rules = array(  
            'name' => 'required|max:255',
            'email' => 'required|unique:users|max:255',
            'phone' => 'required|max:15|min:11',
            'gender' => 'required',
            'date_of_birth' => 'required|date|date_format:Y-m-d',
            'kit_code' => 'required',
            'password' => 'required|min:6',
            'password_confirmation' => 'required_with:password|same:password|min:6',
        );
        // Validation
        $data = $request->all();
        $validator = Validator::make($data, $rules); 
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors(), 'code' => 400], 400);  
        }

        $input = $request->except('kit_code');
        // bcrypt Password to store it in databasde
        
        $kit = Kit::where('kit_code', '=', $request->kit_code)
            ->where('registered', '=', '0')
            ->first();
        if ( $kit ) {
            $this->RegisterNewsLetter($request->email, $request->name);
            $input['password'] = bcrypt($request->password);
            $user = User::create($input);
            $user = User::find($user->id);
            $kit->update(['user_id' => $user->id, 'registered' => 1, 'registered_at' => now()]);
            $token = $user->createToken('TutsForWeb')->accessToken;
            $user->kits;
            $this->logging('New user register successfully to this kit: ' . $request->kit_code ,null, $user->toArray(), auth()->user(), $user, 'apiUser');
            return response()->json( 
                [ 'message' => __('api.register_successfully'), 'code' => 200, 'data' => [ 'token' => $token, 'user' => $user,  ] ],
                200
            );
            
        } else {
            
            return response()->json( [ 'errors' =>[ __('api.genel_hata') ], 'code' => 401 ], 401 );
        }
    }

    /**
    * Handle a registration user request for the application.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function loginKitUser(Request $request)
    {   
        $rules = array(  
            'email' => 'required|max:255|email',
            'password' => 'required|min:6|string',
            'kit_code' => 'required',
        );
        // Validation
        $data = $request->all();
        $validator = Validator::make($data, $rules); 
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors(), 'code' => 400], 400);  
        }

        $user = User::where('email', '=', $request->email)
                    ->first();
        if ($user) { # if user exist
            $this->RegisterNewsLetter($user->email, $user->name);
            if (Hash::check($request->password, $user->password)) { # if password correct
                $kit = Kit::where('kit_code', '=', $request->kit_code)
                            ->where('registered', '=', '0')
                            ->first();
                if ($kit) { #if kit available
                    $kit->update(['user_id' => $user->id, 'registered' => 1, 'registered_at' => now()]);
                    $token = $user->createToken('TutsForWeb')->accessToken;
                    $user->kits;
                    $this->logging('exist user register new kit with code: ' . $request->kit_code ,null, $user->toArray(), auth()->user(), $user, 'apiUser');
                    return response()->json(
                        [ 'message' => __('api.register_successfully'), 'code' => 200, 'data' => ['token' => $token, 'user' => $user] ],
                        200
                    );
                } else { #if kit not available
                    $this->loginlogout('apiUser', 'Trying to register kit with this code: '. $request->kit_code .', but this kit not registrable');
                    return response()->json(['errors' =>  __('api.genel_hata'), 'code' => 400], 400);  
                }
            } else {  # if password not correct
                $this->loginlogout('apiUser', 'Trying to register kit with this code: '. $request->kit_code .', to exist user but the given info uncorrect');
                return response()->json(['errors' => 'Girdiğiniz kullanıcı bilgileri hatalıdır', 'code' => 400], 400);  
            }
        } else { # if user not exist
            $this->RegisterNewsLetter($request->email);
            $this->loginlogout('apiUser', 'Trying to register kit with this code: '. $request->kit_code .', to exist user but the given info uncorrect');
            return response()->json(['errors' => 'Girdiğiniz kullanıcı bilgileri hatalıdır', 'code' => 400], 400);  
        }
    }
}
