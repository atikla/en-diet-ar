<?php

namespace App\Http\Controllers\API\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Validator;

class AuthController extends Controller
{
    //
    use SendsPasswordResetEmails;
    /**
     * Handles Login Request
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    { 
        $rules = array(  
            'email' => 'required|email:rfc,dns',
            'password' => 'required|min:6', 
        );
        //Validation
        $data = $request->all();
        $validator = Validator::make($data, $rules); 

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors(), 'code' => 400], 400);  
        } 
 
        if (auth()->attempt($data)) {
            $token = auth()->user()->createToken('TutsForWeb')->accessToken;
            $user = auth()->user();
            $user->kits;
            $this->loginlogout('apiUser', 'user with email: '. $user->email . ' login successful');
            return response()->json(
                [ 'message' => __('api.login_successfully'), 'code' => 200, 'data' => [ 'token' => $token, 'user' => $user ] ],
                200
            );
        } else {
            $this->loginlogout('apiUser', 'user with email: '. $request->email . ' login unsuccessful');
            return response()->json( ['errors' => __('api.login_unsuccessful'), 'code' => 401 ], 401 );
        }
    } 
}
