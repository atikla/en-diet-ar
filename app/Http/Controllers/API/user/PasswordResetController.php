<?php

namespace App\Http\Controllers\API\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\ResetPassword;
use Validator;
use App\User;
use Hash;
use Mail;
use DB;

class PasswordResetController extends Controller
{
    /**
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function passwordreset(Request $request)
    {
        $rules = array(  
            'email' => 'required|email:rfc,dns',
        );
        //Validation
        $data = $request->all();
        $validator = Validator::make($data, $rules); 

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors(), 'code' => 400], 400);  
        }
        if ( $user = User::where('email', '=', $request->email)->first() ) {
            DB::table('password_resets')->whereEmail($request->email)->whereGuard('user')->delete();
            //Create Password Reset Token
            DB::table('password_resets')->insert([
                'email' => $request->email,
                'token' => str_replace('/', '', bcrypt( $request->email . time() ) ),
                'guard' => 'user',
                'created_at' => now()
            ]);
            //Get the token just created above
            $tokenData = DB::table('password_resets')
                ->where('email', $request->email)
                ->whereGuard('user')
                ->first();

            if ($this->sendResetEmail($user, $tokenData->token)) {
                $this->loginlogout('apiUser', 'Password reset url has been sended to this email: ' . $request->email);
                return response()->json( [ 'message' => __('api.send_reset_password'), 'code' => 200 ], 200 );
            } else {
                return response()->json([ 'errors' =>[ __('api.genel_hata') ], 'code' => 400 ], 400 );
            }
        } else {
            $this->loginlogout('apiUser', 'Trying to reset password on email that we don\'t have in our recorder');
            return response()->json(['errors' => [ 'email' => __('api.reset_password_error')], 'code' => 400], 400);  
        }
    }
    public function checkAndReset(Request $request)
    {
        
        $rules = array(
            'password' => 'required|min:6',
            'password_confirmation' => 'required_with:password|same:password|min:6',
            'token' => 'required'
        );
        //Validation
        $data = $request->all();
        $validator = Validator::make($data, $rules); 

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors(), 'code' => 400], 400); 
        }

        $tokenData = DB::table('password_resets')->where('token', urldecode( $request->token ))->first();
        if (!$tokenData)
            return response()->json(['errors' => [__('api.genel_hata')], 'code' => 400], 400);

        $user = User::where('email', $tokenData->email)->first();
        if (!$user)
            return response()->json(['errors' => [__('api.genel_hata')], 'code' => 400], 400);

        $user->password = Hash::make($request->password);
        $user->save(); 
    
        //Delete the token
        DB::table('password_resets')->whereEmail($user->email)->whereGuard('user')->delete();

        $this->loginlogout('apiUser', 'Password reset for this email: ' . $user->email . ' done successfully');
        return response()->json( ['message' => __('api.reset_password_done'), 'code' => 200], 200 );

    }

    private function sendResetEmail(User $user, $token)
    {
        //Generate, the password reset link. The token generated is embedded in the link
        $link = 'https://app.enbiosis.com/password/reset/' . urlencode ( $token );

        try {
            Mail::to($user->email)->locale(app()->getLocale())->send( new ResetPassword( $link ) );
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
}
