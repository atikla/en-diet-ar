<?php

namespace App\Http\Controllers\API\dietitian;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Dietitian;
use App\Survey;
use App\User;
use App\Kit;

class DietitianController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['api', 'dietitian-oauth']);
    }
    
    /**
     * get Auth User.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dietitian = Dietitian::find(authD());
        if ( $dietitian ) {
            return response()->json( ['message' => 'ok', 'code' => 200, 'data' => $dietitian], 200 );
        }
    }

    public function info()
    {
        $dietitian = Dietitian::find(authD());
        if ( $dietitian ) {
            $data = [];
            $data['clients'] = User::whereHas('kits', function($query) use ( $dietitian ) { $query->where('dietitian_id', $dietitian->id);  })->count();
            $data['kits'] = $dietitian->kits()->count();
            $data['kits_result_upload'] = $dietitian->kits()->whereKitStatus(4)->whereNotNull('uploaded_at')->count();
            $data['kits_result_not_upload'] = $dietitian->kits()->whereKitStatus('!=', 4)->whereNull('uploaded_at')->count();
            $data['kits_filled_surveys'] = $dietitian->kits()->whereNotNull('survey_id')->whereNotNull('survey_filled_at')->count();
            $data['kits_not_filled_surveys'] = $dietitian->kits()->whereNull('survey_id')->whereNull('survey_filled_at')->count();
            return response()->json( ['message' => 'ok', 'code' => 200, 'data' => $data], 200 );
        }
    }

    /**
     * get Auth User.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $dietitian = Dietitian::find(authD());
        if ( $dietitian ) {
            $rules = array(  
                'search' => 'required|min:3',
            );
            //Validation
            $data = $request->all();
            $validator = Validator::make($data, $rules);

            if ( $validator->fails() ) {
                return response()->json(['errors' => $validator->errors(), 'code' => 400], 400);  
            }
            $search = $request->search;
            $users = User::where('name', 'LIKE', '%' . $search .'%')
                ->whereHas('kits', function($query) use ( $dietitian ) {
                    $query->where('dietitian_id', $dietitian->id);
                })
                ->get();
            if ( $users->count() <= 0 ) {
                return response()->json( ['message' => __('api.no_result_found'), 'code' => 200, 'data' => null], 200 );
            } else {
                foreach( $users as $key => $user ) {
                    $kits =  $user->kits;
                    unset($user->kits);
                    $temp = [];
                    foreach( $kits as $key => $kit ) {
                        if ( $kit['dietitian_id'] != null )
                            array_push($temp, $kit);
                    }
                    $user['kits'] = $temp;
                }
                return response()->json( ['message' => 'ok', 'code' => 200, 'data' => $users], 200 );
            }
        }
    }

     /**
     * get Auth User.
     *
     * @return \Illuminate\Http\Response
     */
    public function getSurvey(Request $request, $kitCode)
    {
        $dietitian = Dietitian::find(authD());
        if ( $dietitian ) {
            $kit = Kit::whereKitCode($kitCode)->whereDietitianId($dietitian->id)->first();
            if ( $kit ) {
                if ( $kit->survey_id && $kit->survey_filled_at ) {
                    $survey = Survey::where('publish', '=', 1)->where('id', '=', $kit->ReverseRelationship)->first();
                    if ( $survey ) {
                            $survey->kitid = $kit->id;
                        $data = $survey->dietitianSurvey;
                        return response()->json( ['message' => 'ok', 'code' => 200, 'data' => $data], 200 );
                    } else {
                        return response()->json( ['errors' => 'Not found any survey please contact with system admin', 'code' => 401 ], 401 );
                    }
                } else {
                    return response()->json( ['errors' => 'kit Survey not filled yet!', 'code' => 401 ], 401 );
                }
            } else {
                return response()->json( ['errors' => 'kit not found', 'code' => 401 ], 401 );
            }
        } else {
            return response()->json( ['errors' => 'invalid token', 'code' => 401 ], 401 );
        }
    }
}
