<?php

namespace App\Http\Controllers\API\dietitian;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\Dietitian\EmailVerification;
use App\Dietitian;
use Validator;
use Mail;

class EmailVerificationController extends Controller
{

     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['api', 'dietitian-oauth']);
    }

    /**
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function sendEmailVerification(Request $request)
    {
        $dietitian = Dietitian::find(authD());
        if ( $dietitian && $dietitian->email_verified_at ) {
            return response()->json( [ 'message' => __('api.already_verified_email'), 'code' => 200 ], 200 );
        }
        $dietitian->expires_at = strtotime('2 hour');
        $dietitian->verify_code =  rand(10000, 99999);
        $dietitian->save();
        try {
            Mail::to($dietitian->email)->locale(app()->getLocale())->send( new EmailVerification( $dietitian ) );
            $this->logging('Verification code sent to dietitian email: ' . $dietitian->email ,null, $dietitian->toArray(), $dietitian, $dietitian, 'apiDietitian');
            return response()->json( [ 'message' => __('api.verification_email_send'),
                'code' => 200 ], 200 );
        } catch (\Throwable $th) {
            return response()->json([ 'errors' =>[ __('api.genel_hata') ], 'code' => 400 ], 400 );
        }
    }


    /**
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function checkForVerificationCode(Request $request)
    {
        $rules = array(  
            'verify_code' => 'required|max:6',
        );
        //Validation
        $data = $request->all();
        $validator = Validator::make($data, $rules);

        if ( $validator->fails() ) {
            return response()->json(['errors' => $validator->errors(), 'code' => 400], 400);  
        }
        $dietitian = Dietitian::find(authD());
        if ( $dietitian && $request->verify_code == $dietitian->verify_code ) {
            // dd($dietitian->expires_at->toDateTimeString());
            if ( $dietitian->expires_at && strtotime( $dietitian->expires_at->toDateTimeString() ) >= time() ) {
                $dietitian->update([
                    'email_verified_at' => now(),
                    'verify_code' => null,
                    'expires_at' => null
                ]);
                $dietitian->kits;
                $this->logging('The dietitian email address has been verified, email: ' . $dietitian->email ,null, $dietitian->toArray(), $dietitian, $dietitian, 'apiDietitian');
                return response()->json( ['message' => __('api.verification_email_done'), 'code' => 200, 'data' => $dietitian], 200 );
            } else {
                return response()->json(['errors' => ['verify_code' => __('api.verification_email_expired')], 'code' => 400], 400);
            }
        } else {
            return response()->json(['errors' => ['verify_code' => __('api.verification_email_error')], 'code' => 400], 400);
        }
    }

}
