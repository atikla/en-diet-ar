<?php

namespace App\Http\Controllers\API\dietitian;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Dietitian;
use Validator;

class DietitianKitController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['api', 'dietitian-oauth']);
    }

    public function getKitFoods(Request $request)
    {
        $dietitian = Dietitian::find(authD());
        if ( $dietitian ) {

            $rules = array(
                'kit_code' => 'required',
            );
            //Validation
            $data = $request->all();
            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors(), 'code' => 400], 400);
            }

            if ( $kit = $dietitian->kits()->where('kit_code', '=', $request->kit_code)->first() ) {
                if ( $kit->food ) {
                    $data = $kit->foods;
                    $this->logging('Get result - food - kit code: ' . $kit->kit_code ,null, null, $dietitian, $dietitian, 'apiDietitian');
                    return response()->json( ['message' => 'ok', 'code' => 200, 'data' => $data], 200 );
                } else {
                    return response()->json(['errors' => [ 'kit_code' => __('api.no_result_yet') ], 'code' => 400], 400);  
                }
            } else {
                return response()->json(['errors' => ['kit_code' => __('api.kit_hata') ], 'code' => 400], 400);
            }
        }
    }

    public function getKitCloseProfle(Request $request)
    {
        $dietitian = Dietitian::find(authD());
        if ( $dietitian ) {

            $rules = array(  
                'kit_code' => 'required',
            );
            //Validation
            $data = $request->all();
            $validator = Validator::make($data, $rules); 
    
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors(), 'code' => 400], 400);  
            }
            if ( $kit = $dietitian->kits()->where('kit_code', '=', $request->kit_code)->first() ) {
                
                if ( $kit->closeProfle ) {
                    $data = $kit->closeProfle;
                    $this->logging('Get result - close profle - kit code: ' . $kit->kit_code ,null, null, $dietitian, $dietitian, 'apiDietitian');
                    return response()->json( ['message' => 'ok', 'code' => 200, 'data' => $data], 200 );
                } else {
                    return response()->json(['errors' => [ 'kit_code' => __('api.no_result_yet') ], 'code' => 400], 400);  
                }
            } else {
                return response()->json(['errors' => ['kit_code' => __('api.kit_hata')], 'code' => 400], 400);  
            }
        }
    }

    public function getKitAgeRange(Request $request, $type = null )
    {
        $dietitian = Dietitian::find(authD());
        if ( $dietitian ) {
            $rules = array(  
                'kit_code' => 'required',
            );
            //Validation
            $data = $request->all();
            $validator = Validator::make($data, $rules); 

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors(), 'code' => 400], 400);  
            }
            if ( $kit = $dietitian->kits()->where('kit_code', '=', $request->kit_code)->first() ) {
                
                if ( $kit->ageRange ) {
                    $data = $kit->ageRange;
                    $this->logging('Get result - taksonomik - kit code: ' . $kit->kit_code ,null, null, $dietitian, $dietitian, 'apiDietitian');
                    return response()->json( ['message' => 'ok', 'code' => 200, 'data' => $data], 200 );
                } else {
                    return response()->json(['errors' => [ 'kit_code' => __('api.no_result_yet') ], 'code' => 400], 400);  
                }
            } else {
                return response()->json(['errors' => ['kit_code' => __('api.kit_hata')], 'code' => 400], 400);  
            }
        }
    }

    public function getKitBacteria(Request $request)
    {
        $dietitian = Dietitian::find(authD());
        if ( $dietitian ) {
            $rules = array(  
                'kit_code' => 'required',
            );
            //Validation
            $data = $request->all();
            $validator = Validator::make($data, $rules); 

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors(), 'code' => 400], 400);  
            }

            if ( $kit = $dietitian->kits()->where('kit_code', '=', $request->kit_code)->first() ) {
                
                if ( $kit->bacteria ) {
                    $data = $kit->bacteria;
                    $this->logging('Get result - bacteria - kit code: ' . $kit->kit_code ,null, null, $dietitian, $dietitian, 'apiDietitian');
                    return response()->json( ['message' => 'ok', 'code' => 200, 'data' => $data], 200 );
                } else {
                    return response()->json(['errors' => [ 'kit_code' => __('api.no_result_yet') ], 'code' => 400], 400);  
                }
            } else {
                return response()->json(['errors' => ['kit_code' => __('api.kit_hata')], 'code' => 400], 400);  
            }
        }
    }

    public function getKitAllScores(Request $request, $pdf = null)
    {
        $dietitian = Dietitian::find(authD());
        if ( $dietitian ) {

            $rules = array(  
                'kit_code' => 'required',
            );
            //Validation
            $data = $request->all();
            $validator = Validator::make($data, $rules); 

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors(), 'code' => 400], 400);  
            }

            if ( $kit = $dietitian->kits()->where('kit_code', '=', $request->kit_code)->first() ) {
                
                if ( $kit->allScores && $kit->pdfScores ) {
                    $data = $kit->allScores;
                    if ( $pdf )
                        $data = $kit->pdfScores;
                    $this->logging('Get result - all scores - kit code: ' . $kit->kit_code . ' - ' . $pdf ,null, null, $dietitian, $dietitian, 'apiDietitian');
                    return response()->json( ['message' => 'ok', 'code' => 200, 'data' => $data], 200 );
                } else {
                    return response()->json(['errors' => [ 'kit_code' => __('api.no_result_yet') ], 'code' => 400], 400);  
                }
            } else {
                return response()->json(['errors' => ['kit_code' => __('api.kit_hata')], 'code' => 400], 400);  
            }
        }
    }

    public function getKitTaksonomik(Request $request, $type = null )
    {
        $dietitian = Dietitian::find(authD());
        if ( $dietitian ) {
            $rules = array(  
                'kit_code' => 'required',
            );
            //Validation
            $data = $request->all();
            $validator = Validator::make($data, $rules); 

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors(), 'code' => 400], 400);  
            }

            if ( $kit = $dietitian->kits()->where('kit_code', '=', $request->kit_code)->first() ) {
                
                if ( $kit->taksonomik ) {
                    $data = $kit->sortedTaksonomik;
                    if ( $type )
                        $data = $kit->notSortedTaksonomik;
                    $this->logging('Get result - taksonomik - kit code: ' . $kit->kit_code . ' - ' . $type ,null, null, $dietitian, $dietitian, 'apiDietitian');
                    return response()->json( ['message' => 'ok', 'code' => 200, 'data' => $data], 200 );
                } else {
                    return response()->json(['errors' => [ 'kit_code' => __('api.no_result_yet') ], 'code' => 400], 400);  
                }
            } else {
                return response()->json(['errors' => ['kit_code' => __('api.kit_hata')], 'code' => 400], 400);  
            }
        }
    }
}
