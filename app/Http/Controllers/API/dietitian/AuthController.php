<?php

namespace App\Http\Controllers\API\dietitian;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class AuthController extends Controller
{
    /**
     * Handles Login Request
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $rules = array(  
            'email' => 'required|email:rfc,dns',
            'password' => 'required|min:6',
        );
        //Validation
        $data = $request->all();
        $validator = Validator::make($data, $rules); 

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors(), 'code' => 400], 400);
        } 

        if ( auth()->guard('dietitian')->attempt($data) ) {
            $token = auth()->guard('dietitian')->user()->createToken('dietitian')->token->id;
            $dietitian = auth()->guard('dietitian')->user();
            $dietitian->kits;
            $this->loginlogout('apiDietitian', 'dietitian with email: '. $dietitian->email . ' login successful');
            return response()->json( [ 'message' => __('api.login_successfully'), 'code' => 200, 'data' => [ 'token' => $token, 'dietitian' => $dietitian ] ], 200 );
        } else {
            $this->loginlogout('apiDietitian', 'dietitian with email: '. $request->email . ' login unsuccessful');
            return response()->json( ['errors' => __('api.login_unsuccessful'), 'code' => 401 ], 401 );
        }
    }
}
