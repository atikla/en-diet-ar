<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Payment;

class AdminPaymentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth:admin', 'admin', 'superAdmin']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $payments = Payment::orderBy('id', 'DESC')
            ->paginate(5);
        $sum = Payment::orderBy('id', 'DESC')->sum('amount');
        return view('admin.payment.index', compact('payments', 'sum') );
    }
}
