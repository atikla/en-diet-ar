<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use App\Exports\KitsExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ExcelImport;
use GuzzleHttp\Client;

use File;
use Alert;
use App\Kit;
use App\Answer;
use App\Mail\kitResultUploaded;
use Mail;


class LaboratoryAdminController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth:admin', 'admin', 'lab-admin']);
    }


    public function indexRoute()
    {
        return view('admin-lab.index');
    }

    /**
     * Display a listing of the Answers.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexAnswer()
    {
        $kits = Kit::where('survey_id', '!=', NULL)
            ->orderBy('id', 'DESC')
            ->paginate(5);
        $this->travelLogging('go to answers index', auth()->user(), 'labAdmin');
        return \view('admin-lab.answer.index', \compact('kits'));
    }

    /**
     * search in resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $this->validate($request, [
            'search' => 'required|max:50|min:1',
        ]);
        $search = $request->search;
        $kits = Kit::where('survey_id', '!=', NULL)
            ->orderBy('id', 'DESC')
            ->where('kit_code', 'LIKE', '%' . $search . '%')
            ->get();
        Alert::alert('Search Result', $kits->count() . ' items Found', 'info');
        $this->logging('Answer searched for ' . $request->search, null, $request->search, auth()->user(), new Answer, 'labAdmin');
        return \view('admin-lab.answer.search', \compact('kits', 'search'));
    }

    /**
     * Export Answers for  Kit  As json
     * @return json file
     */
    public function export(Request $request) 
    {
        $data = array();
        if ( $request->all ) {
            $kits = Kit::where('survey_id', '!=', NULL)->orderBy('id', 'DESC')->get();
            foreach ( $kits as $kit ) {
                $tempanswer = [];
                $tempanswer['previous_kit'] = $kit->oldOne;
                foreach ( $kit->answer as $answer) {

                    $question = $answer->question()->withTrashed()->first();

                    if ( $question->defaultLanguage->option_name ) {
                        $tempdata = [] ;
                        $options = $question->defaultLanguage->option_name;
                        foreach ($options as $key => $value) {
                            foreach ($answer->answer as  $nestedValue) {
                                if ( array_key_exists( '"'. $key . '"', ( array ) $nestedValue) ) {
                                    $tempdata[ $value['title'] ] = null;
                                    if ( $nestedValue['"'. $key . '"'] ) {
                                        $tempdata[ $value['title'] ] = $nestedValue[ '"'. $key . '"' ];
                                    }
                                }
                            }
                        }
                        $tempanswer[$question->DefaultLanguage->title] = ( array ) $tempdata;
                    } else {
                        $tempanswer[$question->DefaultLanguage->title] = ( array ) $answer->answer;
                    }
                }
                $data[$kit->kit_code] = $tempanswer;
            }
            $file = time() . '_' . rand() . '_answers.json';

            $path = storage_path() ."/jsonFiles/answerFiles/";

            if ( !is_dir( $path ) ) mkdir($path,0777,true);

            File::put($path . $file,json_encode($data));

            $headers = array('Content-Type: application/json',);
            $this->logging('All answers exported', null, array_keys($data), auth()->user(), new Answer, 'labAdmin');

            return response()->download($path . $file, 'answers.json', $headers);
        } else {
            $kits = array_slice( explode('-', $request->kits), 0, -1 );
            foreach ( $kits as $kitcode ) {
                $kit = Kit::where('kit_code', '=', $kitcode) ->orderBy('id', 'DESC')->first();
                $tempanswer = [];
                $tempanswer['previous_kit'] = $kit->oldOne;
                foreach ( $kit->answer as $answer) {

                    $question = $answer->question()->withTrashed()->first();

                    if ( $question->defaultLanguage->option_name ) {
                        $tempdata = [] ;
                        $options = $question->defaultLanguage->option_name;
                        foreach ($options as $key => $value) {
                            foreach ($answer->answer as  $nestedValue) {
                                if ( array_key_exists( '"'. $key . '"', ( array ) $nestedValue) ) {
                                    $tempdata[ $value['title'] ] = null;
                                    if ( $nestedValue['"'. $key . '"'] ) {
                                        $tempdata[ $value['title'] ] = $nestedValue[ '"'. $key . '"' ];
                                    }
                                }
                            }
                        }
                        $tempanswer[$question->DefaultLanguage->title] = ( array ) $tempdata;
                    } else {
                        $tempanswer[$question->DefaultLanguage->title] = ( array ) $answer->answer;
                    }
                }
                $data[$kit->kit_code] = $tempanswer;
            }
            $file = time() . '_' . rand() . '_answers.json';

            $path = storage_path() ."/jsonFiles/answerFiles/";

            if ( !is_dir( $path ) ) mkdir($path,0777,true);

            File::put($path . $file,json_encode($data));

            $headers = array('Content-Type: application/json',);
            $this->logging('some kit answers exported', null, array_keys($data), auth()->user(), new Answer, 'labAdmin');

            return response()->download($path . $file, 'answers.json', $headers);
        }
    }

    public function kitResultUploaded($kitcode) 
    {
        $kit = Kit::where('kit_code', '=', $kitcode)->first();
        if ( $kit->food && $kit->microbiome ) {

            $kit->uploaded_at = now();
            $kit->kit_status = 4;
            $kit->save();
            $this->logging('kit result uploaded', null, $kit->toArray(), auth()->user(), $kit, 'labAdmin');
            if( $kit->user ){
                Mail::to($kit->user->email)->locale(app()->getLocale())->send(new kitResultUploaded($kit));
                return 'mail have been sent to kit user';
                
            }elseif( $kit->orderDetail ){
                Mail::to($kit->orderDetail->order->email)->locale(app()->getLocale())->send(new kitResultUploaded($kit));
                return 'mail have been sent to kit order mail';

            }else {
                return 'mail have not been sent';
            }
        } else {
            return 'mail have not been sent because one of food or microbiome not exist';
        }
    }

    protected function kitResultUploadedInfo($kitcode) 
    {
        $kit = Kit::where('kit_code', '=', $kitcode)->first();
        if ( $kit->food && $kit->microbiome ) {
            $kit->uploaded_at = now();
            $kit->kit_status = 4;
            $kit->save();
            return 'both food and microbiome jsons uploaded';
        }
    }

    public function uploadMicrobiomeJsonFile(Request $request)
    {
        $this->validate($request, [
            'json-file' => 'required',
        ]);
        if ( $file = $request->file('json-file') ) {

            if( $file->getClientOriginalExtension() != 'json' )
                return redirect()->back()->withInput()->withErrors('file must be a json file');

            $fileName = time() . '-mikrobiyom-' . $request->file('json-file')->getClientOriginalName();
            // upload file to storage
            $file->move(storage_path('/jsonFiles/microbiomeFiles'), $fileName);
            // file get contents
            $microbiomes = json_decode(file_get_contents(storage_path('/jsonFiles/microbiomeFiles') . '/' . $fileName), true);
            $data = array(
                'valid'     => array(),
                'invalid'   => array(),
                'added'     => array(),
                'modified'  => array(),
                'denied'    => array()
            );
            if ( !is_array( $microbiomes ) )
                return redirect()->back()->withInput()->withErrors('Json Data it is not in format');
    
                // filter it between valid or not 
            foreach ( $microbiomes as $key => $microbiome ) {
                $index1 = isset( $microbiome['Mikrobiyom Çeşitliliğiniz'] ); 
                $index2 = isset( $microbiome['Önemli Organizmalarınız'] ); 
                $index3 = isset( $microbiome['Taksonomik Analiz'] ); 
                $index4 = isset( $microbiome['Yakın Profiller'] ); 
                $index5 = isset( $microbiome['Bağırsak Skorları'] ); 
                if ( $index1 && $index2 && $index3 && $index4 && $index5 ) {
                    $data['valid'][$key] = $microbiome;
                } else {
                    $data['unvalid'][$key] = $microbiome;
                }
            }
            // if size of valid data not 0,then will be process
            if ( sizeof( $data['valid'] ) > 0  ) {

                foreach ( $data['valid'] as $key => $microbiome) {
                    $kit = Kit::where('kit_code', '=', $key)->first();
                    // if kit exist
                    if ( $kit ) {
                        $this->logging('upload Microbiome for kitcode: ' . $kit->kit_code, null, $kit->toArray(), auth()->user(), $kit, 'labAdmin');
                        // if food data exist or not
                    if ( $kit->microbiome ) {
                        $kit->microbiome = $microbiome;
                        $kit->save();
                        $data['modified'][$key] = $this->kitResultUploadedInfo($kit->kit_code);
                    } else {
                        $kit->microbiome = $microbiome;
                        $kit->save();
                        $data['added'][$key] = $this->kitResultUploadedInfo($kit->kit_code);
                    }
                    // if kit not exist
                    } else {
                        $data['denied'][$key] = 'Not in our record';

                    }
                }

            //  if Json data it is not in format 
            } else {
                return redirect()->back()->withInput()->withErrors('Json Data it is not in format');
            }
            unset($data['valid']);
            session()->flash('jsonkit', $data);
            Alert::alert('Done', sizeof($data['modified']) +  sizeof($data['added']) . ' tane kit microbiome sonuclari yuklendi', 'info');
            return redirect()->route('lab.index');
        // if the file not json file
        } else {
            return redirect()->back()->withInput()->withErrors('file must be a json file');
        }
        return $request->all();
    }

    public function uploadFoodJsonFile(Request $request)
    {
        $this->validate($request, [
            'json-file' => 'required',
        ]);
        if ( $file = $request->file('json-file') ) {

            if( $file->getClientOriginalExtension() != 'json' )
                return redirect()->back()->withInput()->withErrors('file must be a json file');

            $fileName = time() . '-foods-' . $request->file('json-file')->getClientOriginalName();
            // upload file to storage
            $file->move(storage_path('/jsonFiles/foodsFiles'), $fileName);
            // get file contents
            $foods = Arr::sortRecursive(json_decode(file_get_contents(storage_path('/jsonFiles/foodsFiles') . '/' . $fileName), true));

            if ( !is_array( $foods ) )
                return redirect()->back()->withInput()->withErrors('Json Data it is not in format');
            // Get All Food Name Old And New
            $data = Arr::pluck(collect( Excel::toArray(new ExcelImport, storage_path().'/foods.xlsx')[0] ), '2', '0' );
            $names = array();
            foreach ($data as $key => $value)
                $names[str_replace(' ', '', mb_strtolower($key))] = $value;
            
            unset($data);

            $kitfoods = array();
            foreach ($foods as $key => $food) {
                foreach ( $food as $nestedkey => $item ) {
                    if ( isset( $names[ str_replace(' ', '', mb_strtolower($nestedkey) )  ] ) ) {
                        
                        $kitfoods[$key][$names[ str_replace(' ', '', mb_strtolower($nestedkey) )  ] ] = $item;
    
                    }
                }
            }
            $data = array(
                
                'added'     => array(),
                'modified'  => array(),
                'denied'    => array()
            );
            foreach ( $kitfoods as $key => $food) {
                $kit = Kit::where('kit_code', '=', $key)->first();
                // if kit exist
                if ( $kit ) {
                    $this->logging('upload food for kitcode: ' . $kit->kit_code, null, $kit->toArray(), auth()->user(), $kit, 'labAdmin');

                    // if food data exist or not
                    if ( $kit->food ) {
                        $kit->food = $food;
                        $kit->save();
                        $data['modified'][$key] = $this->kitResultUploadedInfo($kit->kit_code);
                    } else {
                        $kit->food = $food;
                        $kit->save();
                        $data['added'][$key] = $this->kitResultUploadedInfo($kit->kit_code);
                    }
                // if kit not exist
                } else {
                    $data['denied'][$key] = 'Not in our record';

                }
            }
            session()->flash('jsonkit', $data);
            Alert::alert('Done', sizeof($data['modified']) +  sizeof($data['added']) . ' tane kit gida sonuclari yuklendi', 'info');
            return redirect()->route('lab.index');
        } else {
            return redirect()->back()->withInput()->withErrors('file must be a json file');
        }
    }

    public function show($id)
    {
        $kit = Kit::findOrFail($id);
        $survey = $kit->survey;
        $this->logging('go to answers show ' . $kit->kit_code, null, ['kit_code', $kit->kit_code], auth()->user(),  $kit, 'labAdmin');
        return \view('admin-lab.answer.show', \compact('kit', 'survey'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function Generate(Request $request, $kitCode)
    {
        $savedlocal = app()->getLocale();

        $kit = Kit::where('kit_code', '=', $kitCode)->first();
        if ( $kit ) {
            if ( $kit->food && $kit->microbiome ) {
                $locale = $request->lang;
                if ( !array_key_exists( $request->lang, config()->get('app.supported_languages' ) ) )
                    $locale = 'tr';
                app()->setLocale($locale);
                $client = new Client();
                $data = [];
                $data['kitCode'] = $kit->kit_code;
                $data['userName'] = $kit->user ? $kit->user->name : $kit->kit_code;
                $data['registerDate'] = now()->isoFormat('Y-M-D');
                $data['taxForPDF'] = $kit->taksonomik;
                $data['closeProfiles'] = array_slice($kit->closeProfle, 0, 8);
                $data['allScoresPDF'] = $kit->pdfScores;
                $data['microTestResult'] = $kit->ageRange;
                $data['foodList'] = $kit->foods;
                $data['isBase64'] = true;

                try {
                    $result = $client->post('https://enbiosis-pdf-generator.herokuapp.com/api/pdf/generate', [
                        'headers' => [
                            'Content-Type' => 'application/json',
                            'Content-Language' => app()->getLocale()],
                        'body' => json_encode($data),
                    ]);
                    if( $result->getStatusCode() == 200 ) {
                        $data = json_decode ($result->getBody()->getContents(), true);
                        app()->setLocale($savedlocal);
                        $this->logging('generate a pdf for kit code' . $kit->kit_code . ' pdf lang is - ' . $locale, null, null, auth()->user(), $kit, 'labAdmin');
                        return view('admin-lab.pdf', ['food' => $data['food'], 'microbiome' => $data['microbiome'], 'kit' => $kit ] );        
                    } else {
                        Alert::alert('node server return error', '', 'info');
                        return redirect()->route('lab.answer.index');
                    }
                } catch (\Throwable $th) {
                    Alert::alert('while trying to get a pdf file from node server error occurred', '', 'info');
                    return redirect()->route('lab.answer.index');
                }
            } else {
                Alert::alert('Hata Olustu3', 'we dont have a result for this kit', 'info');
                return redirect()->route('lab.answer.index');
            }
        } else {
            abort(404);
        }
    }
}
