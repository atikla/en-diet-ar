<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kit;
use App\Survey;
use App\Answer;
use App\Question;
use Alert;
use Auth;

class UserController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    /**
     * show dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $kits = $user->kits;
        $notFilledSurvey = $kits->where('survey_id', '=', NULL)
            ->where('survey_filled_at', '=', NULL);
        if( $notFilledSurvey->count() == 0 ) {
            $user->photo;
            $user->dietitian;
            return \view('user.index', \compact('user'));
        }else{
            $kit = $notFilledSurvey->first();
            Alert::alert(__('user.bilgi'), __('user.anket_bilgi', ['code' => $kit->kit_code]), 'info')
                ->autoclose(10000);
            return redirect()->route('user.survey.create', [$kit->kit_code, $kit->id]);
        }
    }
     /**
     * Show the survey form for fill it by auth user
     * @param  int  $kitCode
     * @param  int  $kitId
     * @return \Illuminate\Http\Response
     */
     public function surveyView ($kitCode, $kitId)
    {
        $kit = auth()->user()
            ->kits
            ->where('kit_code', '=', $kitCode)
            ->where('id', '=', $kitId)
            ->first();
        if( $kit ){
            if( $kit->survey_id ){
                return redirect()->route('user.index');
            }
            $surveys = Survey::where('publish', '=', 1)->get();
            if ( $surveys->count() > 0 ) {
                $survey = $surveys->first();
                $survey->kitid = $kit->id;
                $surveys = $survey->Survey;
                return \view('user.survey.index', \compact('surveys', 'kitCode', 'kitId'));             

            } else {
                $user = Auth::user();
                $kits = $user->kits;
                $user->photo;
                $user->dietitian;
                Alert::alert(__('user.anket_yok'), __('user.anket_yok_des'), 'info')
                    ->autoclose(10000);
                return \view('user.index', \compact('user'));             
            }
        } else {
            return abort(404);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function storeSurvey(Request $request, $kitCode, $kitId)
    {
        
        $kit = auth()->user()
            ->kits
            ->where('kit_code', '=', $kitCode)
            ->where('id', '=', $kitId)
            ->first();
        if( $kit ){
            if( $kit->survey_id ){
                Alert::alert(__('user.bilgi'), __('user.hatali_url'), 'info')
                    ->autoclose(10000);
                return redirect()->route('user.index');
            }
        }
        $answers = array();
        $conditionals =  ( array ) json_decode( $request->conditionals, true);

        foreach ( $request->except(['_token', 'conditionals']) as $key => $value) {

            $id = explode("_", $key);
            $answers[ $id[2] ] = $value;
        }
        $kit = Kit::findOrFail($kitId);
        foreach ( $answers as $key => $answer ) {
            if ( is_array($answer) ) {
                $endanswer = [];
                foreach ( $answer as $option ) {

                    if( isset($conditionals[$key]) ) {

                        if( isset($conditionals[$key][$option]) ) {
                            
                            array_push($endanswer, [ '"'.$option.'"' => $conditionals[$key][$option] ]);

                        } else {

                            array_push($endanswer, [  '"'.$option.'"'  => null ]);

                        }
                    } else {

                        array_push($endanswer, [  '"'.$option.'"'  => null ]);

                    }
                }
            } else {
                $endanswer = $answer;
            }
            $exist = $kit->answer->where('question_id', '=', $key);
            if ( $exist->count() == 0 ){

                Answer::create([
                    'question_id'   => $key,
                    'kit_id'        => $kitId,
                    'answer'        => $endanswer
                ]);
            } else {
                $kit->answer
                ->where('question_id', '=', $key)
                ->each
                ->delete();
                Answer::create([
                    'question_id'   => $key,
                    'kit_id'        => $kitId,
                    'answer'        => $endanswer
                ]);
            }
            // $answers[$key] = $endanswer;
        }
        $kit = Kit::findOrFail($kitId);
        $surveyId =  $kit->answer()
            ->withTrashed()
            ->first()
            ->question
            ->surveySection
            ->survey
            ->id;
        $kit->survey_id = $surveyId;
        $kit->survey_filled_at = now();
        $kit->save();
        Alert::alert(__('user.bilgilendirme'), __('user.anket_dolduruldu', ['code' => $kit->kit_code]), 'info')
            ->autoclose(10000);
        return redirect()->route('user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function showAnswer($id)
    {
        $kit = Kit::findOrFail($id);
        $survey = $kit->survey;
        return \view('user.showanswer', \compact('kit', 'survey'));
    }
}
