<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin;
use Alert;

class AdminController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth:admin', 'admin']);
    }
    /**
     * show dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ( auth()->user()->role == 0  )
            return redirect()->route('admin.dashboard');
        else if ( auth()->user()->role == 1 )
            return redirect()->route('operation.index');
        else if ( auth()->user()->role == 2 )
            return redirect()->route('lab.index');
        else 
            return view('errors.selectPermission');
    }


    public function SuperAdminprofile()
    {
        $admin = auth()->user();
        return view('admin.profile', compact('admin'));
    }
    
    public function OperationAdminProfile()
    {
        $admin = auth()->user();
        return view('admin-operation.profile', compact('admin'));
    }

    public function LabAdminprofile()
    {
        $admin = auth()->user();
        return view('admin-lab.profile', compact('admin'));
    }
    
    public function updateProfile(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|unique:admins,email,'.auth()->user()->id,
            'phone' => 'required|max:15|min:11',
            'photo' => 'image|nullable|max:2500',
            'password' => 'nullable|min:6',
            'password_confirmation' => 'required_with:password|same:password|min:6|nullable'
        ]);
        $admin = auth()->user();
        
        $input = $request->except('photo');
        
        if(trim($request->password) == ''){

            $input = $request->except('password', 'photo');

        }else{

            $input = $request->all();
            $input['password'] = bcrypt($request->password);

        }

        if ( $file = $request->file('photo') ) {

            if ( $admin->photo ) {

                if ( file_exists('img/private/' . $admin->photo->path ) )
                    unlink('img/private/' . $admin->photo->path);
                $admin->photo->delete();
            }
            $path = time() . $input['email'] . '.' . $file->getClientOriginalExtension();
            $file->move('img/private', $path);
            $admin->photo()->create([ 'path' => $path ]);

        }
        $admin->update($input);
        Alert::alert('Your Profile Updated', '', 'success');
        return redirect()->route('admin.index');
    }
}
