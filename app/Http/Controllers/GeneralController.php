<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use App\Order;
use App\Contact;
use App\Mail\ContactUs;
use Mail;
use Alert;
use Newsletter;
// use Illuminate\Support\Arr;

class GeneralController extends Controller
{
    public function welcome()
    {
        return view('welcome');
    }

    public function setcookie(Request $request)
    {
        Cookie::queue('accept_cookies', true, 360000);
        return response()->json('',200);
    }

    public function cookiesPolicy()
    {
        return view('cookiesPolicy');
    }

    public function mesafeliSatisSozlesmesi()
    {
        return \view('mesafeli-satis-sozlesmesi');
    }
	public function mikrobiyom()
    {
        return \view('mikrobiyom');
    }
	public function mikrobiyom_analizi()
    {
        return \view('urunlerimiz');
    }
    public function onBilgilendirmeFormu()
    {
        return \view('on-bilgilendirme-formu');
    }
    public function aydinlatmaFormu()
    {
        return \view('aydinlatma-formu');
    }

    public function uyelikSozlesmesi()
    {
        return \view('uyeliksozlesmesi');
    }
	public function tesekkurler($slug, $tracking)
    {
        $order = Order::where('tracking', '=', $tracking)->firstOrFail();
        return \view('tesekkurler', compact('order') );
    }
    public function gizlilikPolitikasi()
    {
        return \view('gizlilikpolitikasi');
    }

    public function hakkimizda()
    {
        if (app()->getLocale() == 'tr')
            return \view('hakkimizda');
        return \view('hakkimizda_en');
    }

    public function bilim()
    {
        return \view('bilim');
    }

    public function enbiosisTest()
    {
        return \view('enbiosis-test');
    }

    public function sss()
    {
        return \view('sss');
    }

    public function profDrHakanAlagozlu()
    {
        return \view('cv.profDrHakanAlagozlu');
    }

    public function profDrTarkanKarakan()
    {
        return \view('cv.profDrTarkanKarakan');
    }

    public function profDrHalilCoskun()
    {
        return \view('cv.profDrHalilCoskun');
    }

    public function drOzkanUfukNalbantoglu()
    {
        return \view('cv.drOzkanUfukNalbantoglu');
    }

    public function docDrAycanGundogdu()
    {
        return \view('cv.docDrAycanGundogdu');
    }

    public function ceoOmerOzkan()
    {
        return \view('cv.ceoOmerOzkan');
    }

    public function iletisim()
    {
        return \view('iletisim');
    }

    public function newsletter(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|max:255',
        ]);
        $this->RegisterNewsLetter($request->email);
        Alert::alert('İşlem Tamamlandı', 'Bizimle ilgilendiginiz için teşekkür ederiz.', 'success');
        return redirect()->back();
    }

    public function sendMail(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'subject' => 'required|max:300|min:5',
            'mesaj' => 'required|max:1000|min:10',
        ]);
        $contact = Contact::create($request->all());
        try {
            $this->RegisterNewsLetter($contact->email, $contact->name);
            Mail::to($contact->email)->locale(app()->getLocale())->send( new ContactUs($contact));
            Mail::to('destek@enbiosis.com')->locale(app()->getLocale())->send( new ContactUs($contact));
            Alert::alert('Mesajınız Gönderildi', 'Bizimle iletişime geçtiğiniz için teşekkür ederiz.', 'success');
            return redirect()->to(url()->previous() . '?gonderildi=ok');
        } catch (\Throwable $th) {
            Alert::alert('Bir Hata Oluştu.', '', 'error');
            return redirect()->route('iletisim', '#')->withInput();
        }
    }

    public function locale($locale)
    {
        // session()->put('locale', $locale);
        return redirect()->back();
    }

    public function instalmentsTable(Request $request)
    {
        $instalment = config('instalments');
        $cart = session()->get('cart');
        $total = $cart['price'] * $cart['quantity'];
        if ( session()->get('coupon') )
            $total = round ( (double) ( ( ( 100 - session()->get('coupon') ) * $cart['price'] ) / 100 ), 2) * $cart['quantity'];
        $data = '';
        $first = 'checked';
        foreach ( $instalment as $key => $item )
        {
            $data .= '<tr>';

            $data .= '<td style="padding-left: 0;"><b><input type="radio" id="instalment" name="instalment" value="' . $item['taksit'] . '"' .  $first .' style="margin-right:10px;">' . __('pages/checkout.'.$item['title']) .' </b></td>';
            $data .= '<td> ' . $item['num'] . ' x ' . round( ( ( $total +   ( ( $total * $item['rate'] ) / 100 ) ) / $item['num'] ), 2) . '</td>';
			$data .= '<td> ' . ( $total +  round( ( ( $total * $item['rate'] ) / 100 ), 2) ) . '</td>';
            $data .= '</tr>';
            $first = '';
        }
        return $data;
    }

	public function kisisellestirilmis_beslenme()
    {
        return \view('landing.kisisellestirilmis-beslenme');
    }
	public function kilo_kontrolu()
    {
        return \view('landing.kilo-kontrolu');
    }
	public function bagirsak_hastaliklari()
    {
        return \view('landing.bagirsak-hastaliklari');
    }
	public function bagisiklik()
    {
        return \view('landing.bagisiklik');
    }
	public function mikrobiyom_testleri()
    {
        return \view('landing.mikrobiyom-testleri');
    }
    public function ibs()
    {
        return \view('landing.ibs');
    }

    public function colyak()
    {
        return \view('landing.colyak');
    }

    public function mikrobiyomDiyeti()
    {
        return \view('landing.mikrobiyomDiyeti');
    }

    public function actifit()
    {
        return \view('landing.actifit');
    }

    public function yatirimci()
    {
        app()->setLocale('en');
        session()->put('locale', 'en');
        return \view('landing.yatirimci');
    }

    public function test()
    {
        // $foods = Arr::sortRecursive(json_decode(file_get_contents(storage_path('first.json')), true));
        // return $foods;
        $contact = Contact::first();
        $order = \App\Order::first();
        return view('mail.order.index', compact('order'));
        Mail::to('latik@aisolt.com')->locale(app()->getLocale())->send( new ContactUs($contact));
       return 'test';
    }
}
