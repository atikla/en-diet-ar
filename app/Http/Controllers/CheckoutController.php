<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Coupon;

class CheckoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function campaign()
    {
        $coupon = Coupon::whereCampaign(true)
            ->where('expires_at', '>=', date('Y-m-d'))
            ->orderBy('expires_at', 'DESC')
            ->firstOrFail();
        return \redirect()->route('checkout.Campaign', $coupon->code);
    }

    /**
     * show dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::whereNumberOfKit(1)
            ->orderBy('updated_at', 'DESC')
            ->get();
        return  \view('checkout', compact('products'));
    }
    /**
     * show dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function campaignIndex($campaignCode)
    {
        $coupon = Coupon::whereCode($campaignCode)
            ->whereCampaign(true)
            ->where('expires_at', '>=', date('Y-m-d'))
            ->firstOrFail();
        $products = Product::whereNumberOfKit(1)
            ->orderBy('id', 'DESC')
            ->get();
        return  \view('campaignCheckout', compact('products', 'coupon'));
    }
    
}
