<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\KitsExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ExcelImport;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;
use App\Kit;
use App\User;
use App\Dietitian;
use Alert;
use Mail;
use App\Mail\KitDeleverToUser;
use App\Mail\KitReceivedFromUser;
use App\Mail\kitSentToLab;
use App\Mail\kitResultUploaded;


class AdminKitsController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
     public function __construct()
    {
        $this->middleware(['auth:admin', 'admin', 'superAdmin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kits = Kit::where('deleted_at', null)
            ->orderBy('id', 'DESC')
            ->paginate(5);
        $this->travelLogging('go to kits index', auth()->user(), 'superAdmin');
        return \view('admin.kit.index', \compact('kits'));

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function trash()
    {
        $kits = Kit::onlyTrashed()
            ->orderBy('id', 'DESC')
            ->paginate(5);
        $this->travelLogging('go to kits trash', auth()->user(), 'superAdmin');
        return \view('admin.kit.trash', \compact('kits'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'kit_code' => 'required|min:1|max:200|numeric'
        ]);

        $i = $request->kit_code;
        $array = array();
        while($i != 0){
            $code = str_random(10);
            $existcode = Kit::where('kit_code', '=', $code)
                                ->first();
            if(!$existcode){

                $kit = Kit::create(['kit_code' => $code]);
                $this->logging('New kit created',null, $kit->toArray(), auth()->user(), $kit, 'superAdmin');
                array_push($array, $code);
                $i--;
            }else{
                continue;
            }
        }
        Alert::alert('Done :)', sizeof($array) . ' Kit Codes Has Been Created', 'success');
        return \redirect()->route('admin.kits.index');
    }
    
     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function storeNewKit(Request $request)
    {
        $this->validate($request, [
            // 'kit_code.*' => 'required|unique:kits'
            "kit_code"    => "required|array|min:1|max:5|unique:kits",
            "kit_code.*"  => "required|string|distinct|min:3|max:10",
        ]);
        $kitCodes = $request->kit_code;
        $insertedKitCode = array();
        $notInsertedKitsCodes = array();
        foreach ($kitCodes as $kit_code) {
            $existcode = Kit::where('kit_code', '=', $kit_code)
                                ->first();
            if(!$existcode){
                $kit = Kit::create(['kit_code' => $kit_code, 'kit_status' => 0, 'sell_status' => 2]);
                $this->logging('New kit created',null, $kit->toArray(), auth()->user(), $kit, 'superAdmin');
                array_push($insertedKitCode, $kit_code);
            }else{
                array_push($notInsertedKitsCodes, $kit_code);
            }
        }
        $listInsertedKitCodes = '';
        $listNotInsertedKitCodes = '';
        foreach ($insertedKitCode as $kitcode)
            $listInsertedKitCodes .= '<li> '. $kitcode .' </li>';
        foreach ($notInsertedKitsCodes as $kitcode)
            $listNotInsertedKitCodes .= '<li> '. $kitcode .' </li>';
        // return 1;
        Alert::html('Done :)','
                    <div class="row">
                        <div class="col-6">
                            <span class="text-success mb-3"> inserted Kit Codes </span>
                            <ol>' . $listInsertedKitCodes . '</ol>
                        </div>
                        <div class="col-6">
                            <span class="text-danger mb-3"> not Inserted Kits Codes </span>
                            <ol>' . $listNotInsertedKitCodes . '</ol>
                        </div>
                    </div>
                ','success')->autoclose(10000);
        return \redirect()->route('admin.kits.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kit = Kit::findOrFail($id);
        $users = User::select(
                \DB::raw("CONCAT(name,' - ',email) AS name"),'id')
                ->orderBy('id', 'DESC')
                ->pluck('name','id');
        $dietitians = Dietitian::select(
                \DB::raw("CONCAT(name,' - ',reference_code) AS name"),'id')
                ->orderBy('id', 'DESC')
                ->pluck('name','id');
        $this->travelLogging('go to kits show', auth()->user(), 'superAdmin');
        return view( 'admin.kit.show', compact('kit', 'users', 'dietitians') );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function edit($id)
    // {
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $kit = Kit::findOrFail($id);
        $request['id_' . $kit->id . 'kit_code'] = str_replace(' ', '', $request['id_' . $kit->id . 'kit_code']);
        $this->validate($request, [
            "*kit_code"    => "required|unique:kits,kit_code|max:10|min:1",
            "*kit_id"  => "required",
        ]);

        $kit->kit_code = $request['id_' . $kit->id . 'kit_code'];
        $kit->save();
        Alert::alert('Done :)', 'Kit Code Has Been Created', 'success');
        $this->logging('kit updated', $request->except('_method', '_token'), $kit->toArray(), auth()->user(), $kit, 'superAdmin');
        return \redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function dislink(Request $request, $id)
    {
        $kit = Kit::findOrFail($id);
        $user = $kit->user;
        $kit->user_id = null;
        $kit->registered  = 0;
        $kit->registered_at  = null;
        // $kit->food = null;
        // $kit->microbiome = null;
        $kit->show = false;
        $kit->save();
        Alert::alert('Done :)', 'kit with kit code = ' . $kit->kit_code . ' Has Been Dislink this user', 'success')->autoclose(10000);
        $this->logging('kit with  code = ' . $kit->kit_code . ' Has Been Dislink from user - user email: ' . $user->email . ' -', null, null, auth()->user(), $kit,  'superAdmin');
        return \redirect()->back();
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function dietitianDislink(Request $request, $id)
    {
        $kit = Kit::findOrFail($id);
        $dietitian = $kit->dietitian;
        $kit->dietitian_id = null;
        $kit->save();
        Alert::alert('Done :)', 'kit with kit code = ' . $kit->kit_code . ' Has Been Dislink this dietitian', 'success')->autoclose(10000);
        $this->logging('kit with  code = ' . $kit->kit_code . ' Has Been Dislink from dietitian - dietitian email: ' . $dietitian->email . ' -', null, null, auth()->user(), $kit,  'superAdmin');
        return \redirect()->back();
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function link(Request $request, $id)
    {
        $this->validate($request, [
            "user"  => "required",
        ]);
        $kit = Kit::findOrfail($id);
        $kit->user_id = $request->user;
        $kit->registered  = 1;
        $kit->registered_at  = now();
        $kit->save();
        Alert::alert('Done :)', 'kit with kit code = ' . $kit->kit_code . ' Has Been link to user', 'success')->autoclose(10000);
        $this->logging('kit with  code = ' . $kit->kit_code . ' Has Been link to user - user email: ' . $kit->user->email . ' -', null, null, auth()->user(), $kit,  'superAdmin');
        return \redirect()->back();
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function dietitianLink(Request $request, $id)
    {
        $this->validate($request, [
            "dietitian"  => "required",
        ]);
        $kit = Kit::findOrfail($id);
        $kit->dietitian_id = $request->dietitian;
        $kit->save();
        Alert::alert('Done :)', 'kit with kit code = ' . $kit->kit_code . ' Has Been link to dietitian', 'success')->autoclose(10000);
        $this->logging('kit with  code = ' . $kit->kit_code . ' Has Been link to dietitian - dietitian email: ' . $kit->dietitian->email . ' -', null, null, auth()->user(), $kit,  'superAdmin');
        return \redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ( $request->soft ) {
            $kit = Kit::onlyTrashed()->findOrFail($id);
            $kit->forceDelete();
            $this->logging('kit deleted', null, $kit->toArray(), auth()->user(), $kit, 'superAdmin');
            Alert::alert('Kit Deleted', 'The Kit Has Been Deleted Permanently', 'error');
            return redirect()->route('admin.kits.trash');
        }
        $kit = Kit::findOrFail($id);
        $kit->delete();
        $this->logging('kit trashed', null, $kit->toArray(), auth()->user(), $kit, 'superAdmin');
        Alert::alert('Kit trashed', 'The Kit Has Been Moved To trash', 'warning');
        return redirect()->route('admin.kits.trash');
    }

    /**
     * Export Kit Code list As xlsx file 
     * @return xlsx file
     */
    public function export() 
    {
        // $temp = new KitsExport;
        // $la = $temp->collection();
        // return dd($la);
        return Excel::download(new KitsExport, 'kit.csv');
    }

    /**
     * Export Kit Code list As xlsx file 
     * @return xlsx file
     */
    public function restore($id) 
    {
        $kit = Kit::withTrashed()
            ->findOrFail($id);
        $kit->restore();
        Alert::alert('Kit Restored', 'The Kit with code " ' . $kit->kit_code . ' " Has Been Restored', 'info');
        $this->logging('Kit restored', null, $kit->toArray(), auth()->user(), $kit, 'superAdmin');
        return redirect()->route('admin.kits.index');
    }
    /**
     * Update Kit Status  
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function kitDeleverToUser(Request $request) 
    {
        $this->validate($request, [
            "kit_code"    => "required",
            "kit_id"  => "required",
        ]);
        $kit = Kit::find($request->kit_id);
        if ( $kit && $kit->kit_code == $request->kit_code){
            $kit->delevered_to_user_at = now();
            $kit->kit_status = 1;
            $kit->save();
            $this->logging('kit delever to user', null, $kit->toArray(), auth()->user(), $kit, 'superAdmin');
            if( $kit->user ){
                Mail::to($kit->user->email)->locale(app()->getLocale())->send(new KitDeleverToUser($kit));
                Alert::alert('Done :)', ' Mail Send to user ', 'success');
                return response()->json([
                    'mes' => 'ok from user',
                    'code' => '200'
                    ]
                );
            }elseif( $kit->orderDetail ){
                Mail::to($kit->orderDetail->order->email)->locale(app()->getLocale())->send(new KitDeleverToUser($kit));
                Alert::alert('Done :)', ' Mail Send to order mail ', 'success');
                return response()->json([
                    'mes' => 'ok from order',
                    'code' => '200'
                    ]
                );
            }else {

                Alert::alert('Done :)', ' Kit Status Updated But No Mail Sending ', 'success');
                return response()->json([
                    'mes' => 'ok',
                    'code' => '200'
                    ]
                );
            }
        }else {
            Alert::alert('Error', ' error occurred ', 'error');

            return response()->json([
                    'mes' => 'error',
                    'code' => '400'
                ]
            );
        }
    }


    /**
     * Export Kit Code list As xlsx file 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function kitReceived(Request $request) 
    {
        $this->validate($request, [
            "kit_code"    => "required",
            "kit_id"  => "required",
        ]);
        $kit = Kit::find($request->kit_id);
        if ( $kit && $kit->kit_code == $request->kit_code){
            $kit->received_from_user_at = now();
            $kit->kit_status = 2;
            $kit->save();
            $this->logging('kit received from user', null, $kit->toArray(), auth()->user(), $kit, 'superAdmin');
            if( $kit->user ){
                Mail::to($kit->user->email)->locale(app()->getLocale())->send(new KitReceivedFromUser($kit));
                Alert::alert('Done :)', ' Mail Send to user ', 'success');
                return response()->json([
                    'mes' => 'ok from user',
                    'code' => '200'
                    ]
                );
            }elseif( $kit->orderDetail ){
                Mail::to($kit->orderDetail->order->email)->locale(app()->getLocale())->send(new KitReceivedFromUser($kit));
                Alert::alert('Done :)', ' Mail Send to order mail', 'success');
                return response()->json([
                    'mes' => 'ok from order',
                    'code' => '200'
                    ]
                );
            }else {

                Alert::alert('Done :)', ' Kit Status Updated But No Mail Sending ', 'success');
                return response()->json([
                    'mes' => 'ok',
                    'code' => '200'
                    ]
                );
            }
        }else {
            Alert::alert('Error', ' error occurred ', 'error');

            return response()->json([
                    'mes' => 'error',
                    'code' => '400'
                ]
            );
        }
    }

    public function kitSentToLab(Request $request) 
    {
        $this->validate($request, [
            "kit_code"    => "required",
            "kit_id"  => "required",
        ]);
        $kit = Kit::find($request->kit_id);
        if ( $kit && $kit->kit_code == $request->kit_code){
            $kit->send_to_lab_at = now();
            $kit->kit_status = 3;
            $kit->save();
            $this->logging('kit sent to lab', null, $kit->toArray(), auth()->user(), $kit, 'superAdmin');
            if( $kit->user ){
                Mail::to($kit->user->email)->locale(app()->getLocale())->send(new kitSentToLab($kit));
                Alert::alert('Done :)', ' Mail Send to user ', 'success');
                return response()->json([
                    'mes' => 'ok from user',
                    'code' => '200'
                    ]
                );
            }elseif( $kit->orderDetail ){
                Mail::to($kit->orderDetail->order->email)->locale(app()->getLocale())->send(new kitSentToLab($kit));
                Alert::alert('Done :)', ' Mail Send to order mail ', 'success');
                return response()->json([
                    'mes' => 'ok from order',
                    'code' => '200'
                    ]
                );
            }else {

                Alert::alert('Done :)', ' Kit Status Updated But No Mail Sending ', 'success');
                return response()->json([
                    'mes' => 'ok',
                    'code' => '200'
                    ]
                );
            }
        }else {
            Alert::alert('Error', ' error occurred ', 'error');

            return response()->json([
                    'mes' => 'error',
                    'code' => '400'
                ]
            );
        }
    }

    protected function kitResultUploaded($kitcode) 
    {
        $kit = Kit::where('kit_code', '=', $kitcode)->first();
        if ( $kit->food && $kit->microbiome ) {

            $kit->uploaded_at = now();
            $kit->kit_status = 4;
            $kit->save();
            $this->logging('kit result uploaded', null, $kit->toArray(), auth()->user(), $kit, 'superAdmin');
            if( $kit->user ){
                Mail::to($kit->user->email)->locale(app()->getLocale())->send(new kitResultUploaded($kit));
                return 'mail have been sent to kit user';
                
            }elseif( $kit->orderDetail ){
                Mail::to($kit->orderDetail->order->email)->locale(app()->getLocale())->send(new kitResultUploaded($kit));
                return 'mail have been sent to kit order mail';

            }else {
                return 'mail have not been sent';
            }
        } else {
            return 'mail have not been sent because one of food or microbiome not exist';
        }
    }

    protected function kitResultUploadedInfo($kitcode) 
    {
        $kit = Kit::where('kit_code', '=', $kitcode)->first();
        if ( $kit->food && $kit->microbiome ) {
            $kit->uploaded_at = now();
            $kit->kit_status = 4;
            $kit->save();
            return 'both food and microbiome jsons uploaded';
        }
    }

    public function uploadMicrobiomeJsonFile(Request $request)
    {
        $this->validate($request, [
            'json-file' => 'required',
        ]);
        if ( $file = $request->file('json-file') ) {

            if( $file->getClientOriginalExtension() != 'json' )
                return redirect()->back()->withInput()->withErrors('file must be a json file');

            $fileName = time() . '-mikrobiyom-' . $request->file('json-file')->getClientOriginalName();
            // upload file to storage
            $file->move(storage_path('/jsonFiles/microbiomeFiles'), $fileName);
            // file get contents
            $microbiomes = json_decode(file_get_contents(storage_path('/jsonFiles/microbiomeFiles') . '/' . $fileName), true);
            $data = array(
                'valid'     => array(),
                'invalid'   => array(),
                'added'     => array(),
                'modified'  => array(),
                'denied'    => array()
            );
            if ( !is_array( $microbiomes ) )
                return redirect()->back()->withInput()->withErrors('Json Data it is not in format');
    
                // filter it between valid or not 
            foreach ( $microbiomes as $key => $microbiome ) {
                $index1 = isset( $microbiome['Mikrobiyom Çeşitliliğiniz'] ); 
                $index2 = isset( $microbiome['Önemli Organizmalarınız'] ); 
                $index3 = isset( $microbiome['Taksonomik Analiz'] ); 
                $index4 = isset( $microbiome['Yakın Profiller'] ); 
                $index5 = isset( $microbiome['Bağırsak Skorları'] ); 
                if ( $index1 && $index2 && $index3 && $index4 && $index5 ) {
                    $data['valid'][$key] = $microbiome;
                } else {
                    $data['unvalid'][$key] = $microbiome;
                }
            }
            // if size of valid data not 0,then will be process
            if ( sizeof( $data['valid'] ) > 0  ) {

                foreach ( $data['valid'] as $key => $microbiome) {
                    $kit = Kit::where('kit_code', '=', $key)->first();
                    // if kit exist
                    if ( $kit ) {
                        $this->logging('upload Microbiome for kitcode: ' . $kit->kit_code, null, $kit->toArray(), auth()->user(), $kit, 'superAdmin');
                        // if food data exist or not
                    if ( $kit->microbiome ) {
                        $kit->microbiome = $microbiome;
                        $kit->save();
                        $data['modified'][$key] = $this->kitResultUploadedInfo($kit->kit_code);
                    } else {
                        $kit->microbiome = $microbiome;
                        $kit->save();
                        $data['added'][$key] = $this->kitResultUploadedInfo($kit->kit_code);
                    }
                    // if kit not exist
                    } else {
                        $data['denied'][$key] = 'Not in our record';

                    }
                }

            //  if Json data it is not in format 
            } else {
                return redirect()->back()->withInput()->withErrors('Json Data it is not in format');
            }
            unset($data['valid']);
            session()->flash('jsonkit', $data);
            return redirect()->route('admin.kits.index');
        // if the file not json file
        } else {
            return redirect()->back()->withInput()->withErrors('file must be a json file');
        }
        return $request->all();
    }

    public function uploadFoodJsonFile(Request $request)
    {
        $this->validate($request, [
            'json-file' => 'required',
        ]);
        if ( $file = $request->file('json-file') ) {

            if( $file->getClientOriginalExtension() != 'json' )
                return redirect()->back()->withInput()->withErrors('file must be a json file');

            $fileName = time() . '-foods-' . $request->file('json-file')->getClientOriginalName();
            // upload file to storage
            $file->move(storage_path('/jsonFiles/foodsFiles'), $fileName);
            // get file contents
            $foods = Arr::sortRecursive(json_decode(file_get_contents(storage_path('/jsonFiles/foodsFiles') . '/' . $fileName), true));

            if ( !is_array( $foods ) )
                return redirect()->back()->withInput()->withErrors('Json Data it is not in format');
            // Get All Food Name Old And New
            $data = Arr::pluck(collect( Excel::toArray(new ExcelImport, storage_path().'/foods.xlsx')[0] ), '2', '0' );
            $names = array();
            foreach ($data as $key => $value)
                $names[str_replace(' ', '', mb_strtolower($key))] = $value;
            
            unset($data);

            $kitfoods = array();
            foreach ($foods as $key => $food) {
                foreach ( $food as $nestedkey => $item ) {
                    if ( isset( $names[ str_replace(' ', '', mb_strtolower($nestedkey) )  ] ) ) {
                        
                        $kitfoods[$key][$names[ str_replace(' ', '', mb_strtolower($nestedkey) )  ] ] = $item;
    
                    }
                }
            }
            $data = array(
                
                'added'     => array(),
                'modified'  => array(),
                'denied'    => array()
            );
            foreach ( $kitfoods as $key => $food) {
                $kit = Kit::where('kit_code', '=', $key)->first();
                // if kit exist
                if ( $kit ) {
                    $this->logging('upload food for kitcode: ' . $kit->kit_code, null, $kit->toArray(), auth()->user(), $kit, 'superAdmin');
                    // if food data exist or not
                    if ( $kit->food ) {
                        $kit->food = $food;
                        $kit->save();
                        $data['modified'][$key] = $this->kitResultUploadedInfo($kit->kit_code);
                    } else {
                        $kit->food = $food;
                        $kit->save();
                        $data['added'][$key] = $this->kitResultUploadedInfo($kit->kit_code);
                    }
                // if kit not exist
                } else {
                    $data['denied'][$key] = 'Not in our record';

                }
            }
            session()->flash('jsonkit', $data);
            return redirect()->route('admin.kits.index');
        } else {
            return redirect()->back()->withInput()->withErrors('file must be a json file');
        }
    }
    public function search(Request $request)
    {
        $this->validate($request, [
            'search' => 'required|max:50|min:3',
        ]);
        $search = $request->search;
        $kits = Kit::where('kit_code', 'LIKE', '%' . $search . '%')
            ->get();
        Alert::alert('Search Result', $kits->count() . ' items Found', 'info');
        $this->logging('kit searched for ' . $request->search , null, $request->search, auth()->user(), new kit, 'superAdmin');
        if ( $request->view == 'pdf')
            return view('admin.kit.generate', compact('kits'));
        else 
            return view('admin.kit.search', compact('kits'));
    }
    
    public function sendMail(Request $request, $kit_code)
    {
        $kit = Kit::whereKitCode($kit_code)->first();
        if ( $kit && $kit->food && $kit->microbiome ) {
            $this->logging('send result uplaoed mail kit code: ' . $kit->kit_code , null, $kit->toArray(), auth()->user(), new kit, 'superAdmin');
            Alert::alert('info', $this->kitResultUploaded($kit->kit_code) . ' - kit code: ' . $kit->kit_code, 'success');
            return redirect()->back();

        } else {
            Alert::alert('Error', 'error occurred', 'error');
            return redirect()->back();
        }
    }

    public function changeShow(Request $request, $kit_code)
    {
        $this->validate($request, [
            'show' => ['required', Rule::In([0, 1, '0', '1'])],
        ]);
        $kit = Kit::whereKitCode($kit_code)->firstOrFail();
        if ( $kit ) {
            $kit->show = $request->show;
            $kit->save();
            $this->logging('kit status changed ( ' . $kit->strShow . ' )' . $kit->kit_code , $request->except('_method', '_token'), $kit->toArray(), auth()->user(), new kit, 'superAdmin');
            Alert::alert('info', 'kit status changed kit code: ' . $kit->kit_code, 'success');
            return redirect()->route('admin.kits.show', $kit->id);
        } else {
            Alert::alert('Error', 'error occurred', 'error');
            return redirect()->back()->withInput();
        }
    }
}
