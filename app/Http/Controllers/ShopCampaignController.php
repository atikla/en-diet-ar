<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\BankCardRequest;
use App\Mail\OrderShipped;
use App\Coupon;
use App\Product;
use App\City;
use App\Error;
use App\Payment;
use App\Order;
use App\OrderDetail;
use App\Kit;
use Validator;
use Payment as PaymentHelper;
use Alert;
use Mail;

class ShopCampaignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($campaignCode, $slug)
    {
        $coupon = Coupon::whereCode($campaignCode)
            ->whereCampaign(true)
            ->where('expires_at', '>=', date('Y-m-d'))
            ->first();
        if (!$coupon)
            return redirect()->route('checkout');
        $product = Product::whereSlug($slug)->firstOrFail();
        $cities = City::all()->pluck('city','id');
        $instalment = config('instalments');
        if( session()->get(strtolower($coupon->code) . '_campaign_cart') && session()->get(strtolower($coupon->code) . '_campaign_cart')['id'] == $product->id ){
            $cart = session()->get($coupon->code . '_campaign_cart');
            return view('campaignCheckoutProduct', compact('instalment', 'cities', 'coupon', 'cart', 'product'));
        } else {
            if ($product->discount_price)
                $price = (double) ( ( ( 100 - $product->discount_price ) * $product->price ) / 100);
            else
                $price = $product->price;
            $cart = [
                "name" => $product->name,
                "slug" => $product->slug,
                "quantity" => 1,
                "price" => $price,
                "number" => $product->number_of_kit,
                'id'    => $product->id,
                'coupon' => $coupon->toArray()
            ];
            session()->put(strtolower($coupon->code) . '_campaign_cart', $cart);
            return view('campaignCheckoutProduct', compact('instalment', 'cities', 'coupon', 'cart', 'product'));
        }
    }
    public function updateQuantity(Request $request, $code)
    {
        $rules = array(
            'quantity' => 'required|min:1|numeric|max:50',
        );
        //Validation
        $data = $request->all();
        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors(), 'code' => 400], 400);
        }

        if( session()->get($code . '_campaign_cart') ) {
            $cart = session()->get($code . '_campaign_cart');
            $cart['quantity'] = $request->quantity;
            session()->forget($code . '_campaign_cart');
            session()->put($code . '_campaign_cart', $cart);
            $data = ['orginal' => session()->get($code . '_campaign_cart') ];
            $data['coupon'] = ['coupon' => session()->get($code . '_campaign_cart')['coupon']['discount'] , session()->get($code . '_campaign_cart')['coupon']['code'] ];
            return response()->json( [ 'message' => 'ok', 'code' => 200, 'data' => $data ],200);
        } else {
            return 1;
        }
    }

    public function campaignInstalmentsTable(Request $request, $code)
    {
        $instalment = config('instalments');
        $cart = session()->get($code. '_campaign_cart');
        $total = $cart['price'] * $cart['quantity'];
        if ( session()->get($code. '_campaign_cart') )
            $total = round ( (double) ( ( ( 100 - session()->get($code . '_campaign_cart')['coupon']['discount'] ) * $cart['price'] ) / 100 ), 2) * $cart['quantity'];
        $data = '';
        $first = 'checked';
        foreach ( $instalment as $key => $item )
        {
            $data .= '<tr>';

            $data .= '<td style="padding-left: 0;"><b><input type="radio" id="instalment" name="instalment" value="' . $item['taksit'] . '"' .  $first .' style="margin-right:10px;">' . __('pages/checkout.'.$item['title']) .' </b></td>';
            $data .= '<td> ' . $item['num'] . ' x ' . round( ( ( $total +   ( ( $total * $item['rate'] ) / 100 ) ) / $item['num'] ), 2) . '</td>';
			$data .= '<td> ' . ( $total +  round( ( ( $total * $item['rate'] ) / 100 ), 2) ) . '</td>';
            $data .= '</tr>';
            $first = '';
        }
        return $data;
    }
    public function checkOut(BankCardRequest $request, $code, $slug)
    {
        $product = Product::whereSlug($slug)->firstOrFail();
        $coupon = Coupon::whereCode($code)
            ->whereCampaign(true)
            ->where('expires_at', '>=', date('Y-m-d'))
            ->first();
        if (!$coupon) {
            Alert::error(__('app.odeme_hata'), 'Kampanya süresi sona ermiştir');
            return redirect()->route('checkout');
        }
        $options = PaymentHelper::paymentOptions();
        session()->put($code . '_old_request', $request->all());
        $amount = 0 ;
        $installment = 1;
        $paidPrice = 0;
        if ( session($code . '_campaign_cart') ) {
            $amount = $amount + session($code. '_campaign_cart')['price'] * session($code. '_campaign_cart')['quantity'];
            $paidPrice = $paidPrice + session($code. '_campaign_cart')['price'] * session($code. '_campaign_cart')['quantity'];
        } else {
            return \abort(404);
        }
        $paidPrice =  round( ( ( ( 100 - $coupon->discount ) * $paidPrice ) / 100 ), 2);
        //  pyment type cash or installment "0 => cash"
        if( $request->taksit > 0 ) {
            $installment = $request->taksit;
            foreach ( config('instalments') as $key => $item ){
                if ( $request->taksit ==  $item['taksit'] ) {
                    $paidPrice = round( $paidPrice +  (double) ( $paidPrice * $item['rate'] ) / 100 , 2);
                }
            }
        } else {
            $installment = 1;
        }
        $paymentRequest = new \Iyzipay\Request\CreatePaymentRequest();
        $paymentRequest->setLocale(\Iyzipay\Model\Locale::TR);
        $paymentRequest->setConversationId('123456789');
        $paymentRequest->setPrice($amount);
        $paymentRequest->setPaidPrice($paidPrice);
        $paymentRequest->setCurrency(\Iyzipay\Model\Currency::TL);
        $paymentRequest->setInstallment($installment);
        $paymentRequest->setBasketId('B67832');
        $paymentRequest->setPaymentChannel(\Iyzipay\Model\PaymentChannel::WEB);
        $paymentRequest->setPaymentGroup(\Iyzipay\Model\PaymentGroup::PRODUCT);
        $paymentRequest->setCallbackUrl(route('checkout.campaign.result', [$coupon->code, $product->slug]));
        // paymentCard
        $paymentRequest->setPaymentCard(PaymentHelper::paymentCard($request->cc_name, $request->card_number, $request->expiration_month, $request->expiration_year, $request->cvc));
        $fullname = explode(' ', $request->name);
        $name = $fullname[0];unset($fullname[0]);$surname = implode(' ', $fullname);
        $paymentRequest->setBuyer(PaymentHelper::buyer( $name, $surname, $request->phone, $request->email, $request->address, $request->city));
        $paymentRequest->setShippingAddress(PaymentHelper::shippingAddress($request->name, $request->city, $request->county, $request->address));
        if ( $request->fcity && $request->fcity && $request->faddress)
            $paymentRequest->setBillingAddress(PaymentHelper::billingAddress($request->name, $request->fcity, $request->fcounty, $request->faddress));
        else
            $paymentRequest->setBillingAddress(PaymentHelper::billingAddress($request->name, $request->city, $request->county, $request->address));
        $paymentRequest->setBasketItems(PaymentHelper::basketItems($code . '_campaign_cart', $product->slug, $product->name));
        // initializePaymentFrom
        $initializePaymentFrom = \Iyzipay\Model\ThreedsInitialize::create($paymentRequest, $options);
        if ( $initializePaymentFrom->getStatus() == 'failure'  ) {
            Alert::error(__('app.odeme_hata'), $initializePaymentFrom->getErrorMessage());
            return \redirect()->back()->withInput();
        } else {
            session()->put($code . '_paymentRequest', $paymentRequest);
            return $initializePaymentFrom->getHtmlContent();
        }
    }

    public function CheckOutResult(Request $request, $code, $slug)
    {
        $product = Product::whereSlug($slug)->firstOrFail();
        $coupon = Coupon::whereCode($code)
            ->whereCampaign(true)
            ->where('expires_at', '>=', date('Y-m-d'))
            ->first();
        if (!$coupon) {
            Alert::error(__('app.odeme_hata'), 'Kampanya süresi sona ermiştir');
            return redirect()->route('checkout');
        }

        if ( !isset($request->status) || $request->status != 'success' ) {
            # if payment not Done
            Error::create(['json_string' => array_merge($request->all(), session($code . '_old_request')), 'model' => 'Declinedpayment']);
            Alert::error(__('app.odeme_hata'), 'Ödemeniz Tamamlanamadı');
            return \redirect()->route('checkout.Campaign.index', [$code, $product->slug])->withInput(session($code . '_old_request'));
        } elseif ($request->status = 'success') {
            $paymentRequest = new \Iyzipay\Request\CreateThreedsPaymentRequest();
            $paymentRequest->setLocale(\Iyzipay\Model\Locale::TR);
            $paymentRequest->setConversationId($request->conversationId);
            $paymentRequest->setPaymentId($request->paymentId);
            $paymentRequest->setConversationData($request->conversationData);
            # make request
            $threedsPayment = \Iyzipay\Model\ThreedsPayment::create($paymentRequest, PaymentHelper::paymentOptions());
            if ($threedsPayment->getStatus() == 'success') {
                try {
                    #if Payment Done
                    $oldRequest = session($code . '_old_request');
                    $paymentRequest = session($code . '_paymentRequest');
                    $payment = Payment::create([
                        'ReturnOid' => $request->paymentId,
                        'amount' => $paymentRequest->getPaidPrice(),
                        'taksit' => $paymentRequest->getInstallment(),
                        'maskedCreditCard' => chunk_split(substr_replace($paymentRequest->getPaymentCard()->getCardNumber(), str_repeat("*", 6), 6, 6), 4, ' '),
                        'EXTRA_CARDHOLDERNAME' => $paymentRequest->getPaymentCard()->getCardHolderName(),
                        'Response' => $request->status,
                        'mdStatus' => $request->mdStatus,
                        'clientIp' => request()->ip()
                    ]);
                } catch (\Throwable $th)  {
                    Error::create(['json_string' => array_merge($request->all(), session($code . '_old_request')), 'model' => 'Approvedpayment']);
                    Alert::error(__('app.odeme_hata'), 'not: Ödeme yapıldı');
                    Mail::raw('Ödeme yapıldı, fakat işlemler gerçekleşirken hata oluştu. sipariş takip no: ' . $request->ReturnOid . '. lütfen bizimle iletişime geçiniz', function($message)
                        {
                            $message->subject('Ödeme yapıldı');
                            $message->from('destek@enbiosis.com', 'Enbiosis');
                            $message->to(session($code . '_old_request')['email']);
                        });
                    return \redirect()->route('checkout.Campaign.index', [$code, $product->slug])->withInput(session($code . '_old_request'));
                }
                try {
                    $order = new Order();
                    $order->payment_id = $payment->id;
                    $order->tracking  = $payment->ReturnOid;
                    $order->address = $oldRequest['address'];
                    $order->city_id = $oldRequest['city'];
                    $order->county_id = $oldRequest['county'];
                    if (isset($oldRequest['fcity'])) {
                        $order->fcity_id = $oldRequest['fcity'] ;
                        $order->fcounty_id = $oldRequest['fcounty'] ;
                        $order->faddress = $oldRequest['faddress']  ;
                    }
                    $order->name = $oldRequest['name'];
                    $order->email = $oldRequest['email'];
                    $order->dietitian_code = $oldRequest['de_code'];
                    $order->phone = $oldRequest['phone'];
                    $order->price = $payment->amount;
                    $order->cc_name = $oldRequest['cc_name'];
                    $order->card_number = $oldRequest['card_number'];
                    $order->expiration_month = $oldRequest['expiration_month'];
                    $order->expiration_year = $oldRequest['expiration_year'];
                    $order->cvc = $oldRequest['cvc'];
                    $order->coupon_id = $coupon->id;
                    if ( $coupon->reference_code )
                        $order->dietitian_code = $coupon->reference_code;
                    $order->save();
                    //insert Order Detail for Order
                    $productName = session($code. '_campaign_cart')['name'];
                    if ( session($code. '_campaign_cart') ) {
                        $cart = session($code. '_campaign_cart');
                        $orderDetail = new OrderDetail;
                        $orderDetail->product_id =  $cart['id'];
                        $orderDetail->name =  $cart['name'];
                        $orderDetail->price =  $cart['price'];
                        $orderDetail->quantity =  $cart['quantity'];

                        $order->orderDetail()->save($orderDetail);
                        # Link Kits With Order By Order detail
                        for ($i = $orderDetail->product->number_of_kit *  $orderDetail->quantity; $i > 0; $i--){
                            $kit = new Kit;
                            $kit->order_detail_id = $orderDetail->id;
                            $kit->sell_status = 1;
                            $kit->save();
                        }
                    }
                    Mail::to($order->email)->locale(app()->getLocale())->send(new OrderShipped($order));
                    Mail::to('kurumsal@enbiosis.com')->locale(app()->getLocale())->send(new OrderShipped($order));
                    session()->forget($code. '_campaign_cart');
                    session()->flash('payment-done', true);
                    $this->RegisterNewsLetter($order->email, $order->name);
                    return redirect()->route('tesekkurler', [slug($productName, '-'), $order->tracking]);
                } catch (\Throwable $th) {
                    Error::create(['json_string' => array_merge($request->all(), session($code . '_old_request')), 'model' => 'Order']);
                    Alert::error(__('app.odeme_hata'), 'not: Ödeme yapıldı');
                    Mail::raw('Ödeme yapıldı, fakat işlemler gerçekleşirken hata oluştu. sipariş takip no: ' . $payment->ReturnOid . '. lütfen bizimle iletişime geçiniz', function($message)
                        {
                            $message->subject('Ödeme yapıldı');
                            $message->from('destek@enbiosis.com', 'Enbiosis');
                            $message->to(session($code . '_old_request')['email']);
                        });
                    return \redirect()->route('checkout.Campaign.index', [$code, $product->slug])->withInput(session($code . '_old_request'));
                }
            } else {
                Error::create(['json_string' => array_merge($request->all(), session($code . '_old_request')), 'model' => 'Declinedpayment']);
                Alert::error(__('app.odeme_hata'), $threedsPayment->getErrorMessage());
                return \redirect()->route('checkout.Campaign.index', [$code, $product->slug])->withInput(session($code . '_old_request'));
            }
        } else {
            Alert::error(__('app.odeme_hata'), 'Ödemeniz Tamamlanamadı');
            return \redirect()->route('checkout.Campaign.index', [$code, $product->slug])->withInput(session($code . '_old_request'));
        }
    }
}
