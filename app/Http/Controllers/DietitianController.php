<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use App\Order;
use App\Kit;
use Alert;


class DietitianController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth:dietitian', 'dietitian']);
    }
    /**
     * show dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return  \view('dietitian.index');
    }

    public function team()
    {
        $team = auth()->user()->sellingTeam;
        return  \view('dietitian.team.index', compact('team'));
    }

    public function member($refCode)
    {
        $team = auth()->user()->sellingTeam;
        $member = $team->dietitians()->where('reference_code', '=', $refCode)->firstOrFail();
        return  \view('dietitian.team.show', compact('member'));
    }

    public function AllOrders()
    {
        $team = auth()->user()->sellingTeam;
        $dietitians = $team->dietitians;
        return  \view('dietitian.orders.index', compact('dietitians'));
    }

    public function showOrder($tracking)
    {
        $team = auth()->user()->sellingTeam;
        $order = Order::whereTracking($tracking)->firstOrFail();
        if ( $order->dietitian_code &&  $team->dietitians->contains('reference_code', $order->dietitian_code) ) 
            return \view('dietitian.orders.show', \compact('order') );
        else 
        abort(404);
    }

    public function myOrder($tracking)
    {
        $orders = auth()->user()->orders;
        if ($orders->contains('tracking', $tracking) ) {
            $order = Order::whereTracking($tracking)->firstOrFail();
            return \view('dietitian.orders.show', \compact('order') );
        } else
        abort(404);
    }

    public function allKits()
    {
        $team = auth()->user()->sellingTeam;
        $dietitians = $team->dietitians;
        return  \view('dietitian.kits.index', compact('dietitians'));
    }

    public function showKit($kitCode)
    {
        $team = auth()->user()->sellingTeam;
        $kit = Kit::whereKitCode($kitCode)->firstOrFail();
        if ( $team->dietitians->contains('id', $kit->dietitian_id) ) 
            return \view('dietitian.team.kits.show', \compact('kit') );
        else
        abort(404);
    }

    public function generateKit(Request $request,$kitCode)
    {
        $team = auth()->user()->sellingTeam;
        $kit = Kit::whereKitCode($kitCode)->firstOrFail();
        $savedlocal = app()->getLocale();
        if ($team->dietitians->contains('id', $kit->dietitian_id) ) {
            if ( $kit->food && $kit->microbiome ) {
                $locale = $request->lang;
                if ( !array_key_exists( $request->lang, config()->get('app.supported_languages' ) ) )
                    $locale = 'tr';
                app()->setLocale($locale);
                $client = new Client();
                $data = $kit->pdf;
                try {
                    $result = $client->post('https://enbiosis-pdf-generator.herokuapp.com/api/pdf/generate', [
                        'headers' => [
                            'Content-Type' => 'application/json',
                            'Content-Language' => app()->getLocale()],
                        'body' => json_encode($data),
                    ]);
                    if( $result->getStatusCode() == 200 ) {
                        $data = json_decode ($result->getBody()->getContents(), true);
                        app()->setLocale($savedlocal);
                        $this->logging($team->name .' team leader ( '.$team->Leader->name . ' ) generate a pdf for kit code' . $kit->kit_code . ' pdf lang is - ' . $locale, null, null, auth()->user(), $kit, 'apiDietitian');
                        return view('dietitian.kits.pdf', ['food' => $data['food'], 'microbiome' => $data['microbiome'], 'kit' => $kit ] );
                    } else {
                        Alert::alert('pdf server return error', '', 'info');
                        return redirect()->back();
                    }
                } catch (\Throwable $th) {
                    Alert::alert('while trying to get a pdf file from node server error occurred', '', 'info');
                    return redirect()->back();
                }
            } else {
                Alert::alert('Hata Olustu3', 'we dont have a result for this kit', 'info');
                return redirect()->back();
            }
            return \view('dietitian.kits.show', \compact('kit') );
        } else
        abort(404);

    }

    public function myKit($kitCode)
    {
        $kits = auth()->user()->kits;
        if ($kits->contains('kit_code', $kitCode) ) {
            $kit = Kit::whereKitCode($kitCode)->firstOrFail();
            return \view('dietitian.kits.show', \compact('kit') );
        } else
        abort(404);
    }

    public function generateMyKit(Request $request, $kitCode)
    {
        $kits = auth()->user()->kits;
        $savedlocal = app()->getLocale();
        if ($kits->contains('kit_code', $kitCode) ) {
            $kit = Kit::whereKitCode($kitCode)->firstOrFail();
            if ( $kit->food && $kit->microbiome ) {
                $locale = $request->lang;
                if ( !array_key_exists( $request->lang, config()->get('app.supported_languages' ) ) )
                    $locale = 'tr';
                app()->setLocale($locale);
                $client = new Client();
                $data = $kit->pdf;
                try {
                    $result = $client->post('https://enbiosis-pdf-generator.herokuapp.com/api/pdf/generate', [
                        'headers' => [
                            'Content-Type' => 'application/json',
                            'Content-Language' => app()->getLocale()],
                        'body' => json_encode($data),
                    ]);
                    if( $result->getStatusCode() == 200 ) {
                        $data = json_decode ($result->getBody()->getContents(), true);
                        app()->setLocale($savedlocal);
                        $this->logging($kit->dietitian->kind . '. ' . $kit->dietitian->name. ' generate a pdf for kit code' . $kit->kit_code . ' pdf lang is - ' . $locale, null, null, auth()->user(), $kit, 'apiDietitian');
                        return view('dietitian.kits.pdf', ['food' => $data['food'], 'microbiome' => $data['microbiome'], 'kit' => $kit ] );        
                    } else {
                        Alert::alert('pdf server return error', '', 'info');
                        return redirect()->back();
                    }
                } catch (\Throwable $th) {
                    Alert::alert('while trying to get a pdf file from node server error occurred', '', 'info');
                    return redirect()->back();
                }
            } else {
                Alert::alert('Hata Olustu3', 'we dont have a result for this kit', 'info');
                return redirect()->back();
            }
            return \view('dietitian.kits.show', \compact('kit') );
        } else
        abort(404);
    }
}
