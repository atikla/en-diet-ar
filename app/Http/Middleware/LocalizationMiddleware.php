<?php

namespace App\Http\Middleware;

use Closure;

class LocalizationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // if ( session()->has('locale') ) {
        //     $locale = session()->get('locale', \Config::get('app.locale'));
        // } else {
        //     $locale = 'tr';
        // }
        if(env('APP_ENV') == 'production')
            \URL::forcescheme('https');
        if ( session()->has('locale') ) {
            $locale = session()->get('locale', config()->get('app.locale'));
        } else {
            $locale = substr($request->server('HTTP_ACCEPT_LANGUAGE'), 0, 2);

            if ($locale != 'tr') {
                $locale = 'en';
            }
        }
        app()->setLocale($locale); 
        return $next($request);
    }
}
