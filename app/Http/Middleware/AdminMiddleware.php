<?php

namespace App\Http\Middleware;
use Auth;
use Closure;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( Auth::guard('admin')->check() && auth()->user()->active ) 
            return $next($request);

        Auth::logout();
        return redirect()->route('admin.login')->withErrors(['password' => 'this user not active']);
        
    }
}
