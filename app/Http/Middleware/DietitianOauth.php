<?php

namespace App\Http\Middleware;

use Closure;
use App\Dietitian;
use DB;

class DietitianOauth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('Authorization');
        if ( $token ) {
            $token = str_replace('Bearer ', '', $token);
            $oauth = DB::table('oauth_access_tokens')->whereId($token)->whereName('dietitian')->whereDate('expires_at', '>', date('y-m-d H:i:s'))->first();
            if ( $oauth ) {
                $dietitian = Dietitian::find($oauth->user_id);
                if ( $dietitian ) {
                    return $next($request);
                } else {
                    return response()->json( ['errors' => 'invalid token', 'code' => 401 ], 401 );
                }
            } else {
                return response()->json( ['errors' => 'invalid token', 'code' => 401 ], 401 );
            }
        } else {
            return response()->json( ['errors' => 'invalid token', 'code' => 401 ], 401 );
        }
    }
}
