<?php

namespace App\Http\Middleware;

use Closure;

class ApiContentType
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $Content = $request->header('Content-Type');
        if ( $Content && $Content == 'application/json' ) {
            return $next($request);
        } else {
            return response()->json( [ 'status' => 400, 'success' => false, 'Error' => 'Bad Request' ], 400 );
        }
    }
}
