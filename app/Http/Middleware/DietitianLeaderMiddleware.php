<?php

namespace App\Http\Middleware;

use Closure;
use Alert;

class DietitianLeaderMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if (auth()->guard('dietitian')->check() && auth()->user()->isLeader) 
            return $next($request);
            
        return redirect()->route('dietitian.login');
    }
}
