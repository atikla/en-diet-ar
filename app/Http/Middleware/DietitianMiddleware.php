<?php

namespace App\Http\Middleware;
use Auth;
use Closure;

class DietitianMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('dietitian')->check()) 
            return $next($request);

        return redirect()->route('dietitian.login');
    }
}
