<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Alert;

class LaboratoryAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( Auth::guard('admin')->check() && auth()->user()->role == 2 ) 
        return $next($request);

        Alert::info('İzin Verilmedi');
        return response()->view('errors.selectPermission');
    }
}
