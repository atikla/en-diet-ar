<?php

namespace App\Http\Middleware;

use Closure;
use \App\Kit;

class ApIKitShow
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( $request->kit_code ) {
            $kit = Kit::whereKitCode($request->kit_code)->whereShow(true)->first();
            if ( $kit ) {
                return $next($request);
            } else {
                return response()->json(['errors' => [ 'kit_code' => __('api.no_result_yet') ], 'code' => 400], 400);
            }
        } else {
            return response()->json( [ 'errors' =>[ __('api.genel_hata') ], 'code' => 400 ],  400 );
        }
    }
}
