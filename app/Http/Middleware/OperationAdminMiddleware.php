<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Alert;

class OperationAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( Auth::guard('admin')->check() && auth()->user()->role == 1 ) 
        return $next($request);

        Alert::info('İzin Verilmedi');
        return response()->view('errors.selectPermission');
    }
}
