<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'path', 'imageable_id', 'imageable_type'
    ];
    
    public function imageable()
    {
        return $this->morphTo();
    }
}
