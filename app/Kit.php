<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ExcelImport;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class Kit extends Model
{

    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'kit_code', 'user_id', 'dietitian_id', 'order_details', 'registered', 'registered_at', 'delevered_to_user_at', 'received_from_user_at',
        'send_to_lab_at', 'uploaded_at', 'sell_status', 'kit_status', 'food', 'microbiome', 'show'
    ];


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
     protected $casts = [
        'registered_at'             => 'datetime',
        'delevered_to_user_at'      => 'datetime',
        'received_from_user_at'     => 'datetime',
        'send_to_lab_at'            => 'datetime',
        'uploaded_at'               => 'datetime',
        'survey_filled_at'          => 'datetime',
        'food'                      => 'array',
        'microbiome'                => 'array',
        'show'                      => 'boolean',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'food', 'microbiome'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function dietitian()
    {
        return $this->belongsTo('App\Dietitian');
    }

    public function OrderDetail()
    {
        return $this->belongsTo('App\OrderDetail', 'order_detail_id', 'id');
    }

    public function answer()
    {
        return $this->hasMany('App\Answer');
    }

    public function survey()
    {
        return $this->belongsTo('App\Survey');
    }

    public function suggestion()
    {
        return $this->hasMany('App\Suggestion');
    }

    public function getPdfAttribute()
    {
        return [
            'kitCode'           => $this->kit_code,
            'userName'          => $this->user ? $this->user->name : $this->kit_code,
            'registerDate'      => now()->isoFormat('Y-M-D'),
            'taxForPDF'         => $this->taksonomik,
            'closeProfiles'     => array_slice($this->closeProfle, 0, 8),
            'allScoresPDF'      => $this->pdfScores,
            'microTestResult'   => $this->ageRange,
            'foodList'          => $this->foods,
            'isBase64'          => true
        ];
    }
    public function getStrShowAttribute()
    {
        if($this->show)
            return 'Active';
        else 
            return 'Not Active';   
    }
    public function getStatusAttribute()
    {
        if($this->sell_status == 0)
            return 'not sold Yet';
        elseif ($this->sell_status == 1)
            return 'sold online';
        else 
            return 'sold offline';   
    }

    public function getKitStatuseAttribute()
    {
        if($this->kit_status == 0)
            return 'order reseved';
        elseif ($this->kit_status == 1)
            return 'Delevered to user';
        elseif ($this->kit_status == 2)
            return 'received from user';
        elseif ($this->kit_status == 3)
            return 'sent to lab';
        else 
            return 'results uploaded';   
    }
    public function getReverseRelationshipAttribute()
    {
       return $this->answer()->withTrashed()->first()->question()->withTrashed()->first()->surveySection()->withTrashed()->first()->survey->id;
    }

    public function getFoodsAttribute()
    {
        if ( !$this->food ) { 
            return null;
        }
        $categories = Arr::pluck(collect( Excel::toArray(new ExcelImport, storage_path().'/foods.xlsx')[0] ), '3', '2' );
        $data = GetFoodCategoriesName(config()->get('foods.data'));
        $i = 0;
        foreach ($this->food as $key => $value) {

            $index = config()->get('foods.' . $categories[$key]);
            if ( file_exists('foods/' . $categories[$key] . '/' . mb_convert_case(__('food.' . str_replace(' ', '', mb_strtolower( $key ) ), [], 'tr' ), MB_CASE_TITLE, "UTF-8") . '.jpg') ){
                $img = url('/') . '/foods/' . $categories[$key] . '/' . mb_convert_case(__('food.' . str_replace(' ', '', mb_strtolower( $key ) ), [], 'tr' ), MB_CASE_TITLE, "UTF-8") . '.jpg';
            } else {
                $img = url('/') . '/img/enbiosis-new-log.png';
            } 
            $fooddata = [
                'foodId' => $i,
                'foodTitle' => __('food.' . str_replace(' ', '', mb_strtolower( $key ) ) ),
                'foodValue' => $value,
                'altTitle' =>  str_replace(' ', '', __('food.' . str_replace(' ', '', mb_strtolower( $key ) ) ) ),
                'imageUrl' => $img,
                'rank_icon' => $value > 7 ? 'iyi' : ( $value < 8 && $value > 3 ? 'orta' : 'kotu' )
            ];
            if ( !$index )
                $index = config()->get('foods.Diğer');
            if ( $value > 7 ) {

                array_push($data[0]['foodItems'][0]['items'], $fooddata);
                array_push($data[ $index ]['foodItems'][0]['items'], $fooddata);
            }
            else if ( $value < 8 && $value > 3 ) {

                array_push($data[0]['foodItems'][1]['items'], $fooddata);
                array_push($data[ $index ]['foodItems'][1]['items'], $fooddata);
            }
            else  {

                array_push($data[0]['foodItems'][2]['items'], $fooddata);
                array_push($data[ $index ]['foodItems'][2]['items'], $fooddata);
            }
            $i++;
        }
        return $data;
    }

    public function getCloseProfleAttribute()
    {
        if ( !$this->microbiome ) { 
            return null;
        }
        $microbiome = $this->microbiome;
        $closeProfles = $microbiome['Yakın Profiller'];
        unset($closeProfles['PROFIL']);
        return  closeProfleFilter($closeProfles);
    }

    public function getAgeRangeAttribute()
    {
        if ( !$this->microbiome ) { 
            return null;
        }
        $microbiome = $this->microbiome;
        $range  = $microbiome['Mikrobiyom Çeşitliliğiniz'];
        $age  = explode('.', $microbiome['Bağırsak Skorları']['Mikrobiyom Yaşınız'][0])[0];
        $note = __('mikrobyom.yaş_genc');
        $rank = 'iyi';
        if ( $this->user ) {
            if ( $this->user->age ) {
                if ($age == $this->user->age) {
                    $note = __('mikrobyom.yaş_esit');
                    $rank = 'orta';
                } elseif ($age > $this->user->age) {
                    $note = __('mikrobyom.yaş_yasli');
                    $rank = 'kotu';
                } else {
                    $note = __('mikrobyom.yaş_genc');
                    $rank = 'iyi';
                }
            }
        }
        $data = array(
            'range' => [
                'head' => __('mikrobyom.mikrobiyomçeşitliliğiniz'),
                'desc' =>  __('mikrobyom.mikrobyomce'),
                'note' =>  $range >= 8 ? __('mikrobyom.mikrobyomce_iyi') : ( $range < 8 && $range >= 5 ? __('mikrobyom.mikrobyomce_orta') : __('mikrobyom.mikrobyomce_kotu') ),
                'value' => $range,
                'rank' => $range >= 8 ? 'iyi' : ( $range < 8 && $range >= 5 ? 'orta' : 'kotu' ),
                'rank_title' => $range >= 8 ? __('mikrobyom.iyi') : ( $range < 8 && $range >= 5 ? __('mikrobyom.orta') : __('mikrobyom.kotu') )
            ],
            'Age' => [
                'head' => __('mikrobyom.Mikrobiyom Yaşınız.0'),
                'desc' => __('mikrobyom.Mikrobiyom Yaşınız.1'),
                'value' => $age,
                'note' => $note,
                'rank' => $rank,
                'title' =>  __('mikrobyom.Sizin Örneğiniz'),
            ]
        );
        return  $data ;
    }

    public function getBacteriaAttribute()
    {
        if ( !$this->microbiome ) { 
            return null;
        }
        $microbiome = $this->microbiome;
        $bacteria  = $microbiome['Önemli Organizmalarınız'];
        unset($bacteria['ORGANIZMA']);
        $data = array();
        foreach ($bacteria as $key => $value) {
            array_push($data, [
                'title'=> $key,
                'desc' => explode('.', __( 'mikrobyom.' . $key ) ),
                'scores' => [ 
                    ['title' => __('mikrobyom.Sizin Örneğiniz'),    'value' => round( $value[0], 0, PHP_ROUND_HALF_UP), 'isMy' => true ],
                    ['title' => __('mikrobyom.Toplum'),             'value' => round( $value[1], 0, PHP_ROUND_HALF_UP), 'isMy' => false ],
                ],
                'bacteriaFoods' => []
            ]);
        }
        return  $data ;
    }
    
    public function getAllScoresAttribute()
    {
        if ( !$this->microbiome ) { 
            return null;
        }
        $microbiome = $this->microbiome;
        $allScores  = $microbiome['Bağırsak Skorları'];
        unset($allScores['Mikrobiyom Yaşınız']);
        $data = allScores();
        foreach ($allScores as $key => $value) {
            $score = array_values(array_filter($value));
            array_push($data[ scoreValuesRanking($key, ( int ) $score[0]) ]['items'], [
                'head'=>  __( 'mikrobyom.' . $key . '.0'),
                'id' => __( 'mikrobyom.' . $key . '.0', [], 'tr'),
                'slug'=>  Str::slug( __( 'mikrobyom.' . $key . '.0' ), '-' ),
                'desc' => explode('.', __( 'mikrobyom.' . $key . '.1') ),
                "video" => getVideoForScore($key),
                'scores' => scores($key, $score),
                'colorScale' => colorScale($key),
                'bacteriaFoods' => [],
                'scoreKeys' => scoreKeys($key)
            ]);
        }
        return  $data ;
    }
    public function getPdfScoresAttribute()
    {
        if ( !$this->microbiome ) { 
            return null;
        }
        $microbiome = $this->microbiome;
        $allScores  = $microbiome['Bağırsak Skorları'];
        unset($allScores['Mikrobiyom Yaşınız']);
        $allScores = orderScores($allScores);
        $data = [];
        foreach ($allScores as $key => $value) {
            $score = array_values(array_filter($value));
            array_push($data, [
                'analizType' => scoreValuesRanking( $key, ( int ) $score[0] ) != 0 ?  scoreValuesRanking( $key, ( int ) $score[0] ) == 1 ? 'orta' : 'kotu' : 'iyi',
                'head'=>  __( 'mikrobyom.' . $key . '.0'),
                'id' => __( 'mikrobyom.' . $key . '.0', [], 'tr'),
                'slug'=>  Str::slug( __( 'mikrobyom.' . $key . '.0' ), '-' ),
                'desc' => explode('.', __( 'mikrobyom.' . $key . '.1') ),
                "video" => getVideoForScore($key),
                'scores' => scores($key, $score),
                'colorScale' => colorScale($key),
                'bacteriaFoods' => [],
                'scoreKeys' => scoreKeys($key)
            ]);
        }
        return  $data ;
    }

    public function getTaksonomikAttribute()
    {
        if ( !$this->microbiome ) { 
            return null;
        }
        $microbiome = $this->microbiome;
        $allTaksonomik  = $microbiome['Taksonomik Analiz'];
        $data = [];
        $index = 0 ;
        foreach ( $allTaksonomik as $key => $taksonomiks) {
            $temp = [] ;
            $temp['title'] = __('mikrobyom.' . explode(' ', $key)[0] . '.' . $index );
            $temp['desc'] = __('mikrobyom.' . $key );
            unset($taksonomiks['TAX']);
            $temp['items'] = [
                [ 'head' => __('mikrobyom.Sizin Örneğiniz'), 'isMy' => true, 'scores' => array()],
                [ 'head' => __('mikrobyom.turkeiye_ortalamas'), 'isMy' => false, 'scores' => array()],
                [ 'head' => __('mikrobyom.dunya_ortalamasi'), 'isMy' => false, 'scores' => array()]
            ];
            ksort($taksonomiks);
            foreach ( $taksonomiks as $nestedKey => $taksonomik ) {
                if($nestedKey == 'Diğer')
                    $nestedKey = __('mikrobyom.Diğer');
                array_push($temp['items'][0]['scores'], [
                    'name' => $nestedKey,
                    'value' => round($taksonomik[0], 5)
                ]);
                array_push($temp['items'][1]['scores'], [
                    'name' => $nestedKey,
                    'value' => round($taksonomik[1], 5)
                ]);
                array_push($temp['items'][2]['scores'], [
                    'name' => $nestedKey,
                    'value' => round($taksonomik[2], 5)
                ]);
            }
            array_push($data, $temp);
            $index++;
        }
        return array_reverse( $data );
    }

    public function getSortedTaksonomikAttribute()
    {
        $data = $this->taksonomik;
        if ( $data )
            return finalTaksonomik( sortTaksonomikScores( $data ) );
        else 
            return null;
    }

    public function getNotSortedTaksonomikAttribute()
    {
        $data = $this->taksonomik;
        if ( $data )
            return $data;
        else 
            return null;
    }

    public function getApiSuggestionAttribute()
    {
        if ( $this->suggestion->count() > 0 ) { 
            $data = [];
            foreach ( $this->suggestion as $suggestion ) {
                array_push($data, [
                    'title' => $suggestion->title,
                    'desc' => $suggestion->description
                ]);
            }
            return $data;
        } else {
            return  null;
        }
    }
    public function getOldOneAttribute()
    {
        if ( $this->user ) {
            $user = $this->user;
            $kit_code = $this->kit_code;
            $kits = $user->kits()
                ->whereNotNull('survey_id')
                ->whereNotNull('survey_filled_at')
                ->whereNotNull('registered_at')
                ->whereRegistered(1)
                ->whereKitStatus(4)
                ->orderBy('survey_filled_at', 'DESC')
                ->get();
            $index = $kits->search(function($kit, $key) use ($kit_code) { if ( $kit->kit_code ==  $kit_code) return $key; });
            if ( $index > 0 )
                return $kits[$index - 1]['kit_code'];
            else
                return  'none';
        } else {
            return  'none';
        }
    }

}