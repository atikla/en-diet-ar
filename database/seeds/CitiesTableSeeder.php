<?php

use Illuminate\Database\Seeder;
use App\City;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cities')->delete();
        $cities = [
            [ 'city' => 'Al Daayen', 'country_code' => 'qa', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s") ],
            [ 'city' => 'Al Khor', 'country_code' => 'qa', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s") ],
            [ 'city' => 'Al Rayyan Municipality', 'country_code' => 'qa', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s") ],
            [ 'city' => 'Al Wakrah', 'country_code' => 'qa', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s") ],
            [ 'city' => 'Al-Shahaniya', 'country_code' => 'qa', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s") ],
            [ 'city' => 'Doha', 'country_code' => 'qa', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s") ],
            [ 'city' => 'Madinat ash Shamal', 'country_code' => 'qa', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s") ],
            [ 'city' => 'Umm Salal Municipality', 'country_code' => 'qa', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s") ],
            [ 'city' => 'Capital Governorate', 'country_code' => 'bh', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s") ],
            [ 'city' => 'Central Governorate', 'country_code' => 'bh', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s") ],
            [ 'city' => 'Northern Governorate', 'country_code' => 'bh', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s") ],
            [ 'city' => 'Southern Governorate', 'country_code' => 'bh', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s") ],
        ];
        foreach ($cities as $city){
            City::create($city);
        }
    }
}
