<?php

use Illuminate\Database\Seeder;
use App\County;

class CountiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('counties')->delete();
        $counties = [
            [ 'city_id' => 1, 'county' => 'Center', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s")],
            [ 'city_id' => 2, 'county' => 'Al Ghuwayrīyah', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s")],
            [ 'city_id' => 2, 'county' => 'Al Khawr', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s")],
            [ 'city_id' => 3, 'county' => 'Al Rayyan', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s")],
            [ 'city_id' => 3, 'county' => 'Umm Bab', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s")],
            [ 'city_id' => 4, 'county' => 'Al Wakrah', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s")],
            [ 'city_id' => 4, 'county' => 'Al Wukayr', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s")],
            [ 'city_id' => 4, 'county' => 'Umm Said', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s")],
            [ 'city_id' => 5, 'county' => 'Al Jemailiya', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s")],
            [ 'city_id' => 5, 'county' => 'Al-Shahaniya', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s")],
            [ 'city_id' => 5, 'county' => 'Dukhan', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s")],
            [ 'city_id' => 6, 'county' => 'Doha', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s")],
            [ 'city_id' => 7, 'county' => 'Ar Ruways', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s")],
            [ 'city_id' => 7, 'county' => 'Fuwayriţ', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s")],
            [ 'city_id' => 7, 'county' => 'Madinat ash Shamal', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s")],
            [ 'city_id' => 8, 'county' => 'Umm Salal Mohammed', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s")],

        ];
        foreach ($counties as $county) {
            County::create($county);
        }
    }
}
