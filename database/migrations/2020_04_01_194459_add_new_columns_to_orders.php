<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumnsToOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->bigInteger('city_id')->after('status')->nullable();
            $table->bigInteger('county_id')->after('city_id')->nullable();
            $table->bigInteger('fcity_id')->after('address')->nullable();
            $table->bigInteger('fcounty_id')->after('fcity_id')->nullable();
            $table->string('faddress')->after('fcounty_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('city_id');
            $table->dropColumn('county_id');
            $table->dropColumn('fcity_id');
            $table->dropColumn('fcounty_id');
            $table->dropColumn('faddress');
        });
    }
}
