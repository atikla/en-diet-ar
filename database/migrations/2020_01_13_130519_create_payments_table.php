<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ReturnOid')->nullable();
            $table->string('TRANID')->nullable();
            $table->string('PAResSyntaxOK')->nullable();
            $table->string('Filce')->nullable();
            $table->string('firmaadi')->nullable();
            $table->string('islemtipi')->nullable();
            $table->string('total1')->nullable();
            $table->string('lang')->nullable();
            $table->string('merchantID')->nullable();
            $table->string('maskedCreditCard')->nullable();
            $table->string('amount')->nullable();
            $table->string('sID')->nullable();
            $table->string('ACQBIN')->nullable();
            $table->string('itemnumber1')->nullable();
            $table->string('Ecom_Payment_Card_ExpDate_Year')->nullable();
            $table->string('EXTRA_CARDBRAND')->nullable();
            $table->string('storekey')->nullable();
            $table->string('MaskedPan')->nullable();
            $table->string('acqStan')->nullable();
            $table->string('Fadres')->nullable();
            $table->string('EXTRA_CAVVRESULTCODE')->nullable();
            $table->string('clientIp')->nullable();
            $table->string('iReqDetail')->nullable();
            $table->string('okUrl')->nullable();
            $table->string('md')->nullable();
            $table->string('ProcReturnCode')->nullable();
            $table->string('payResults_dsId')->nullable();
            $table->string('vendorCode')->nullable();
            $table->string('taksit')->nullable();
            $table->string('TransId')->nullable();
            $table->string('EXTRA_TRXDATE')->nullable();
            $table->string('productcode1')->nullable();
            $table->string('Fil')->nullable();
            $table->string('Ecom_Payment_Card_ExpDate_Month')->nullable();
            $table->string('storetype')->nullable();
            $table->string('iReqCode')->nullable();
            $table->string('Response')->nullable();
            $table->string('tulkekod')->nullable();
            $table->string('SettleId')->nullable();
            $table->string('mdErrorMsg')->nullable();
            $table->string('ErrMsg')->nullable();
            $table->string('EXTRA_SGKTUTAR')->nullable();
            $table->string('PAResVerified')->nullable();
            $table->text('cavv')->nullable();
            $table->text('id1')->nullable();
            $table->text('digest')->nullable();
            $table->text('HostRefNum')->nullable();
            $table->text('callbackCall')->nullable();
            $table->text('AuthCode')->nullable();
            $table->text('failUrl')->nullable();
            $table->text('cavvAlgorithm')->nullable();
            $table->text('Fpostakodu')->nullable();
            $table->text('price1')->nullable();
            $table->text('faturaFirma')->nullable();
            $table->text('xid')->nullable();
            $table->text('encoding')->nullable();
            $table->text('oid')->nullable();
            $table->text('mdStatus')->nullable();
            $table->text('dsId')->nullable();
            $table->text('eci')->nullable();
            $table->text('version')->nullable();
            $table->text('Fadres2')->nullable();
            $table->text('EXTRA_CARDHOLDERNAME')->nullable();
            $table->text('Fismi')->nullable();
            $table->text('qty1')->nullable();
            $table->text('desc1')->nullable();
            $table->text('EXTRA_CARDISSUER')->nullable();
            $table->text('clientid')->nullable();
            $table->text('txstatus')->nullable();
            $table->text('_charset_')->nullable();
            $table->text('HASH')->nullable();
            $table->text('rnd')->nullable();
            $table->text('HASHPARAMS')->nullable();
            $table->text('HASHPARAMSVAL')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
