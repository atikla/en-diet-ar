<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumnToDietitian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dietitians', function (Blueprint $table) {
            $table->Integer('d_type')->default(0); # 0 = dietitian, 1 = doctor, 2 = pharmacist
            $table->bigInteger('city_id')->after('d_type')->nullable(); # il
            $table->Integer('d_permission')->default(0); # 0 = il sorumlusu degile , 1 = il sorumlusu
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dietitians', function (Blueprint $table) {
            $table->dropColumn('d_type');
            $table->dropColumn('city_id');
            $table->dropColumn('d_permission');
        });
    }
}