<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumnsToDietitians extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dietitians', function (Blueprint $table) {
            $table->Integer('type')->default(0); # 0 = dietitian, 1 = doctor, 2 = pharmacist
            $table->bigInteger('city_id')->after('address')->nullable(); # il
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dietitians', function (Blueprint $table) {
            $table->dropColumn('type');
            $table->dropColumn('city_id');
        });
    }
}
