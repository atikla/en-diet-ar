<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kit_code')->unique()->index()->nullable();
            $table->bigInteger('user_id')->nullable();
            $table->bigInteger('order_detail_id')->nullable();
            $table->bigInteger('survey_id')->nullable()->index();
            $table->boolean('registered')->default(0); # 0 = not registered yet - 1 = registered
            $table->timestamp('registered_at')->nullable(); # When User do register kit 
            $table->timestamp('delevered_to_user_at')->nullable(); # when we send the kit to user
            $table->timestamp('received_from_user_at')->nullable(); # when we receiv the kit and 
            $table->timestamp('send_to_lab_at')->nullable(); # send it to lab
            $table->timestamp('uploaded_at')->nullable(); # when receiv kit from lab and upload results
            $table->timestamp('survey_filled_at')->nullable(); # when user fill survey for kit
            $table->boolean('kit_status')->default(0); # 0 = order reseved - 1 = Delevered to user - 2 = received from user - 3 = sent to lab - 4 = results uploaded 
            $table->boolean('sell_status')->default(0); # ( 0 = avilabel - 1 = online - 2 = offline )sell
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kits');
    }
}
