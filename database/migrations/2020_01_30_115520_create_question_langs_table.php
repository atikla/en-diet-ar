<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionLangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_langs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('lang')->default('tr');
            $table->bigInteger('question_id')->unsigned()->index();
            $table->string('title');
            $table->longText('option_name')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_langs');
    }
}
