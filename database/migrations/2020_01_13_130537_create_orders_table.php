<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('payment_id')->unique()->nullable();
            $table->bigInteger('coupon_id')->unique()->nullable();
            $table->string('tracking')->unique();
            $table->boolean('status')->default(0);
            $table->string('address')->nullable();
            $table->string('name');
            $table->string('email');
            $table->string('dietitian_code')->nullable();
            $table->string('phone');
            $table->double('price', 8, 2);
            $table->boolean('checkbox')->default(1);
            $table->string('cc_name');
            $table->string('card_number');
            $table->string('expiration_month');
            $table->string('expiration_year');
            $table->string('cvc');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
