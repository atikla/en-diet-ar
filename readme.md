# Relases 
## First Release For Enbiosis v0.1.0
* in bitbucket https://bitbucket.org/tech-solt/enbiosis_api/commits/tag/v0.1.0
* in github https://github.com/atikla/enbiosis/releases/tag/v0.1.0
### user side
1. guest
   1.  can view landing page 
   1.  can view other information pages 
   1.  can place order  
   1.  register-kit 
        1.  register-kit  
        1.  make new user after ckech for kit code or link it with exist user 
1. Auth user 
    1.  can view kits 
    1.  can fill Questionnaire for each kit
    1.  can view Questionnaire Answer for each kit
1. Admin 
    1.  User Management
    1.  Product Management 
    1.  Coupon Management  
    1.  kits Management 
    1.  Order Management 
    1.   surveys Management

## v0.1.1 Release
## added features
### user side
1. installment payment method added
### Admin side
1. view surveys Answers
1. Export surveys Answers as json
### Api side
1. guest
     1. login
     1. register Kit
     1. register User
     1. validate register User
     1. login kit user
1. Auth
    1. Get Auth user
    1. Get Survey for kit
    1. insert Survey Answers for kit
    1. Get foods for kit
