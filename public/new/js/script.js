function openMenu(){
	$('.mobile-menu').toggleClass('d-flex');
}
$(document).ready(function(){
	if (typeof window.orientation !== 'undefined') {
		if(screen.orientation.type.indexOf('landscape') != -1){
			$('#turn').css('display','flex');
		}

	}

	$('.mobile-menu').click(function() {
		$('.mobile-menu').toggleClass('d-flex');
	});



	$('.form-send').click(function(){
		var form_adsoyad =  $("#form_adsoyad").val();
		var form_email =    $("#form_email").val();
		var form_baslik =  $("#form_baslik").val();
		var form_mesaj =     $("#form_mesaj").val();
		$(this).html('GÖNDERİLİYOR...');

		if(form_adsoyad == "" || form_email == "" || form_baslik == "" || form_mesaj == ""){
			alert('Lütfen tüm alanları doldurunuz.');
			$('.form-send').html('GÖNDER');
			return false;
		}
		$.ajax({
			type:'POST',
			url: "/mail/send.php",
			data:{
				'adsoyad' : form_adsoyad,
				'email' : form_email,
				'baslik' : form_baslik,
				'mesaj' : form_mesaj,
			},

			success: function(data)
			{
				alert("Mesajınız tarafımıza ulaşmıştır.");
				$('.form-send').html('GÖNDER');
			}
		});
	});
	$('.form_send_ik').click(function(){
		var form_adsoyad =  $("#form_adsoyad").val();
		var form_dogumyeri =    $("#form_dogumyeri").val();
		var form_dogumtarihi =  $("#form_dogumtarihi").val();
		var form_telefon =     $("#form_telefon").val();

		var form_adres =     $("#form_adres").val();
		var form_unvan =     $("#form_unvan").val();
		var form_tecrube =     $("#form_tecrube").val();
		var form_not =     $("#form_not").val();
		$(this).html('GÖNDERİLİYOR...');
		if(form_adsoyad == "" || form_dogumyeri == "" || form_dogumtarihi == "" || form_telefon == ""){
			alert('Lütfen (*) işaretli alanları doldurunuz.');
			$('.form_send_ik').html('GÖNDER');
			return false;
		}
		$.ajax({
			type:'POST',
			url: "/mail/send2.php",
			data:{
				'adsoyad' : form_adsoyad,
				'dogumyeri' : form_dogumyeri,
				'dogumtarihi' : form_dogumtarihi,
				'telefon' : form_telefon,
				'adres':form_adres,
				'unvan':form_unvan,
				'tecrube':form_tecrube,
				'not':form_not
			},
			success: function(data)
			{
				alert("Form tarafımıza ulaşmıştır.");
			}
		});
	});

});


window.addEventListener("orientationchange", function() {

	if(screen.orientation.type.indexOf('landscape') != -1){
		$('#turn').css('display','flex');
	}else{
		$('#turn').css('display','none');
	}
}, false);
