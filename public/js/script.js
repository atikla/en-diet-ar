
$(document).ready(function(){

	$('.end-carousel').owlCarousel({
	    loop:true,
		autoplay:true,
		autoplayTimeout:2500,
	    margin:20,
	    nav:false,
		dots:false,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:3
	        },
	        1000:{
	            items:5
	        }
	    }
	});
	$('.team-carousel').owlCarousel({
		loop:true,
		margin:20,
		dots:false,
		responsive:{
			0:{
				items:2,
				autoplay:true
			},
			600:{
				items:3,
				autoplay:true
			},
			1000:{
				items:5
			}
		}
	});
	$('.mobile-menu-toggle').click(function(){
		alert('dasd');
		$('.mobile-menu').css('display','flex');
	});
	$('.mm-close').click(function(){
		$('.mobile-menu').css('display','none');
	});
	$(".collapse").on('show.bs.collapse', function(){
        	$(this).prev(".card-header").find(".i").html('-');
        }).on('hide.bs.collapse', function(){
        	$(this).prev(".card-header").find(".i").html('+');
		});



		$('.form-send').click(function(){
			var form_adsoyad =  $("#form_adsoyad").val();
			var form_email =    $("#form_email").val();
			var form_baslik =  $("#form_baslik").val();
			var form_mesaj =     $("#form_mesaj").val();
			$(this).html('GÖNDERİLİYOR...');

			if(form_adsoyad == "" || form_email == "" || form_baslik == "" || form_mesaj == ""){
				alert('Lütfen tüm alanları doldurunuz.');
				$('.form-send').html('GÖNDER');
				return false;
			}
			$.ajax({
				type:'POST',
				url: "/mail/send.php",
				data:{
					'adsoyad' : form_adsoyad,
					'email' : form_email,
					'baslik' : form_baslik,
					'mesaj' : form_mesaj,
				},

				success: function(data)
				{
					alert("Mesajınız tarafımıza ulaşmıştır.");
					$('.form-send').html('GÖNDER');
				}
			});
		});
		$('.form_send_ik').click(function(){
			var form_adsoyad =  $("#form_adsoyad").val();
			var form_dogumyeri =    $("#form_dogumyeri").val();
			var form_dogumtarihi =  $("#form_dogumtarihi").val();
			var form_telefon =     $("#form_telefon").val();

			var form_adres =     $("#form_adres").val();
			var form_unvan =     $("#form_unvan").val();
			var form_tecrube =     $("#form_tecrube").val();
			var form_not =     $("#form_not").val();
			$(this).html('GÖNDERİLİYOR...');
			if(form_adsoyad == "" || form_dogumyeri == "" || form_dogumtarihi == "" || form_telefon == ""){
				alert('Lütfen (*) işaretli alanları doldurunuz.');
				$('.form_send_ik').html('GÖNDER');
				return false;
			}
			$.ajax({
				type:'POST',
				url: "/mail/send2.php",
				data:{
					'adsoyad' : form_adsoyad,
					'dogumyeri' : form_dogumyeri,
					'dogumtarihi' : form_dogumtarihi,
					'telefon' : form_telefon,
					'adres':form_adres,
					'unvan':form_unvan,
					'tecrube':form_tecrube,
					'not':form_not
				},
				success: function(data)
				{
					alert("Form tarafımıza ulaşmıştır.");
				}
			});
		});
});
$(window).scroll(function(){
	if($(window).scrollTop() > 108){
		$('.scrolling-offer').css('display','flex');
		$('.offer-carousel').owlCarousel({
			autoplay:true,
			autoplayTimeout:3000,
		    margin:20,
		    nav:false,
			loop:true,
			items:1,
			dots:false,
		    responsive:{
		        0:{
		            items:1
		        },
		        600:{
		            items:1
		        },
		        1000:{
		            items:1
		        }
		    }
		});
	}else{
		$('.scrolling-offer').css('display','none');
		$('.offer-carousel').owlCarousel('destroy');
	}
});
