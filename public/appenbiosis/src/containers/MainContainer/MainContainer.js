import React, {Fragment} from 'react'
import {connect} from 'react-redux'
import NavbarOuterDiv from '../../components/Navbar/NavbarOuterDiv'
import Navbar from '../../components/Navbar/Navbar'
import SideNavbarOuterDiv from '../../components/SideNavbar/SideNavbarOuterDiv'
import SideNavbar from '../../components/SideNavbar/SideNavbar'
import UserRoutes from '../../components/Routes/UserRoutes'
import NonActiveKitRoutes from '../../components/Routes/NonActiveKitRoutes'
import {toggleSideNavbar} from '../../store/actions/ui'
import {setActiveKit, logout} from '../../store/actions/auth'
import {setInitialFilters} from '../../store/actions/myFoods'

const MainContainer = ({
  activeItem,
  showSideNavbar,
  toggleSideNavbar,
  currentUser,
  setActiveKit,
  logout,
  setInitialFilters
}) => {
  return (
      <Fragment>
          <div className='u-background'></div>
          {currentUser.isAuthenticated ?
            <Fragment>
              <NavbarOuterDiv>
                <Navbar
                  showSideNavbar={showSideNavbar}
                  toggleSideNavbar={toggleSideNavbar}
                  currentUser={currentUser}
                  logout={logout}
                />
              </NavbarOuterDiv>
              <div className='main-container'>
                <SideNavbarOuterDiv>
                  <SideNavbar
                    activeItem={activeItem}
                    showSideNavbar={showSideNavbar}
                    toggleSideNavbar={toggleSideNavbar}
                    setInitialFilters={setInitialFilters}
                  />
                </SideNavbarOuterDiv>
                <div className='main-content'>
                    {currentUser.activeKit.survey_id === null ?
                      <NonActiveKitRoutes/> :
                      <UserRoutes/>
                    }
                </div>
              </div>
            </Fragment> :
            null
          }
      </Fragment>
  )
}

const mapStateToProps = (state) => {
  return {
    activeItem: state.ui.activeItem,
    showSideNavbar: state.ui.showSideNavbar,
    currentUser: state.currentUser
  }
}

export default connect(mapStateToProps, {
  logout,
  setInitialFilters,
  toggleSideNavbar,
  setActiveKit
})(MainContainer);
